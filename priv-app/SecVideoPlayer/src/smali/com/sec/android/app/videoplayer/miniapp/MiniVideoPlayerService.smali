.class public Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.super Lcom/sec/android/app/minimode/MiniModeService;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;,
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;,
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;
    }
.end annotation


# static fields
.field private static final BOTTOM_PERCENT_CAN_BE_OUTSIDE_OF_SCREEN:D = 0.6

.field private static final CURRENT_POSITION:I = 0x1

.field private static final DEFAULT_POSITION:I = 0x0

.field private static final DEFAULT_SUBTITLE_INDEX:I = 0x0

.field private static final HANDLE_ADJUST_X_Y_POSITION:I = 0xd

.field private static final HANDLE_NEXT:I = 0xa

.field private static final HANDLE_OPEN_DRM_VIDEO:I = 0xc

.field private static final HANDLE_PREVIOUS:I = 0xb

.field private static final INIT_POS:I = -0x270f

.field public static final LARGE_THUMBNAIL_DATA:Ljava/lang/String; = "large_poster_url"

.field private static final LEFT_PERCENT_CAN_BE_OUTSIDE_OF_SCREEN:D = 0.4

.field public static final PLAYBACK_DRM_STRING_DIVX_NOT_AUTHORIZED:I = 0x79

.field public static final PLAYBACK_DRM_STRING_DIVX_NOT_REGISTERED:I = 0x7a

.field public static final PLAYBACK_DRM_STRING_DIVX_RENTAL_EXPIRED:I = 0x7b

.field public static final PLAYBACK_DRM_STRING_DIVX_RENTAL_INFO:I = 0x7c

.field public static final PLAYBACK_DRM_STRING_FIRST_INTERVAL_RENDER:I = 0x77

.field public static final PLAYBACK_DRM_STRING_INVALID_CD:I = 0x75

.field public static final PLAYBACK_DRM_STRING_INVALID_SD:I = 0x76

.field public static final PLAYBACK_DRM_STRING_NULL:I = 0x73

.field public static final PLAYBACK_DRM_STRING_VALID_COUNT:I = 0x74

.field public static final PLAYBACK_DRM_STRING_WMDRM_INVALID:I = 0x78

.field protected static final RATIO:D = 1.5

.field private static final RIGHT_PERCENT_CAN_BE_OUTSIDE_OF_SCREEN:D = 0.6

.field private static final SCALE_MULTIPLIER_THRESOLD:I = 0x780

.field private static final TAG:Ljava/lang/String; = "MiniVideoPlayerService"

.field private static final TOP_PERCENT_CAN_BE_OUTSIDE_OF_SCREEN:D

.field private static mBlockSmartPause:Z

.field protected static mLarge:Z

.field private static mPausedbySmartPause:Z

.field private static mPrevNeedtoPause:I

.field private static mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

.field private static mSmartPause:Z


# instance fields
.field private final DRM_ACQUIRE_LICENSE:I

.field private final DRM_ERROR_FILE:I

.field private final DRM_EXPIRED_FILE:I

.field private final DRM_FILE_PLAY:I

.field private final DRM_FILE_SHOW_RENTAL:I

.field PrevSaveX:I

.field PrevSaveY:I

.field SaveX:I

.field SaveY:I

.field private bScreenScaling:Z

.field private bWatchON:Z

.field currentPosition:I

.field private displayName:Ljava/lang/String;

.field drm_uri:Ljava/lang/String;

.field private isCompletion:Z

.field private isStartFromList:Z

.field private mAIAHide:Z

.field mAm:Landroid/app/IActivityManager;

.field private mAudioOnly:Landroid/widget/RelativeLayout;

.field private mAudioOnlyText:Landroid/widget/TextView;

.field private mAudioTrack:I

.field private mBattLevel:I

.field private mBattScale:I

.field private mBattStatus:I

.field private final mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

.field private mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

.field private mBucketID:I

.field private final mCaptioningObserver:Landroid/database/ContentObserver;

.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCurrentProviderName:Ljava/lang/String;

.field private mDmsTitle:Ljava/lang/String;

.field mDownNextX:I

.field mDownNextY:I

.field private final mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

.field private final mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

.field mDrmPopupType:I

.field mDrmStringType:[I

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mEmptyLayout:Landroid/widget/RelativeLayout;

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mExitButton:Landroid/widget/ImageButton;

.field private mExitByMoviePlayer:Z

.field private mExitLayout:Landroid/widget/RelativeLayout;

.field private mForceResetSubtilteView:Z

.field final mForegroundToken:Landroid/os/IBinder;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private final mHandlerTemp:Landroid/os/Handler;

.field private mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mIsAudioClipOnlyWindowCreated:Z

.field private mIsCalledScalewindow:Z

.field private mIsMoved:Z

.field private mIsPlayingB4Hide:Z

.field private mIsResized:Z

.field private mIsfromMovieStore:Z

.field private mLastFile:Ljava/lang/String;

.field private mListType:I

.field private mLoadingLayout:Landroid/widget/RelativeLayout;

.field private mLoadingText:Landroid/widget/TextView;

.field private mMainView:Landroid/view/ViewGroup;

.field private mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

.field public mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

.field public mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

.field private mOldFolderState:I

.field private mOldOrientation:I

.field private mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

.field private mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

.field private mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

.field private mPlaySpeed:I

.field mPlaylist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResizeAnim:Landroid/view/animation/Animation;

.field private mRootLayout:Landroid/widget/RelativeLayout;

.field private mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field private mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

.field private mSavedcurrentPosition:I

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleFactor:F

.field mScaleMultiplier:I

.field private mScalewindowRect:Landroid/graphics/Rect;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSecureMode:Z

.field private mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

.field private mSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

.field private mUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

.field private mUrlUpdatedSKTcloudListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mVideoHeight:I

.field private mVideoID:J

.field private mVideoUri:Landroid/net/Uri;

.field private mVideoWidth:I

.field private mXpos:I

.field private mYpos:I

.field private miniMainView:Landroid/view/View;

.field private remainPercent:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    sput-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLarge:Z

    .line 223
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 224
    sput-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSmartPause:Z

    .line 225
    sput-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBlockSmartPause:Z

    .line 226
    sput-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPausedbySmartPause:Z

    .line 227
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevNeedtoPause:I

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v5, -0x1

    const/16 v4, -0x270f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/minimode/MiniModeService;-><init>()V

    .line 131
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMainView:Landroid/view/ViewGroup;

    .line 132
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    .line 133
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z

    .line 134
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSecureMode:Z

    .line 135
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isStartFromList:Z

    .line 137
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .line 140
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnly:Landroid/widget/RelativeLayout;

    .line 142
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingText:Landroid/widget/TextView;

    .line 143
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnlyText:Landroid/widget/TextView;

    .line 144
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitButton:Landroid/widget/ImageButton;

    .line 146
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    .line 148
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    .line 152
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 153
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 154
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 158
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    .line 159
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    .line 160
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I

    .line 161
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I

    .line 162
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    .line 163
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    .line 164
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    .line 165
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    .line 167
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    .line 168
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    .line 169
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    .line 170
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->displayName:Ljava/lang/String;

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDmsTitle:Ljava/lang/String;

    .line 175
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsCalledScalewindow:Z

    .line 176
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScalewindowRect:Landroid/graphics/Rect;

    .line 178
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCurrentProviderName:Ljava/lang/String;

    .line 180
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->remainPercent:I

    .line 183
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattScale:I

    .line 184
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattLevel:I

    .line 185
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattStatus:I

    .line 187
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    .line 188
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .line 189
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 190
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 192
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mGestureDetector:Landroid/view/GestureDetector;

    .line 196
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 197
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    .line 198
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bScreenScaling:Z

    .line 199
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    .line 200
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    .line 201
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z

    .line 204
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    .line 205
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    .line 206
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsMoved:Z

    .line 209
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 211
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    .line 212
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsPlayingB4Hide:Z

    .line 215
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSavedcurrentPosition:I

    .line 218
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bWatchON:Z

    .line 219
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsfromMovieStore:Z

    .line 220
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isCompletion:Z

    .line 236
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDownNextX:I

    .line 237
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDownNextY:I

    .line 240
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    .line 242
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    .line 244
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForegroundToken:Landroid/os/IBinder;

    .line 245
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAm:Landroid/app/IActivityManager;

    .line 246
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForceResetSubtilteView:Z

    .line 248
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 833
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 864
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1009
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    .line 1088
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 1115
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 1504
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$7;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    .line 1520
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$8;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    .line 1535
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    .line 1630
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 1682
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 1742
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    .line 2074
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$13;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    .line 2097
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mUrlUpdatedSKTcloudListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    .line 2529
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$15;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;

    .line 2827
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleMultiplier:I

    .line 3274
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->DRM_ERROR_FILE:I

    .line 3276
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->DRM_EXPIRED_FILE:I

    .line 3278
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->DRM_FILE_SHOW_RENTAL:I

    .line 3280
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->DRM_FILE_PLAY:I

    .line 3282
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->DRM_ACQUIRE_LICENSE:I

    .line 3296
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    .line 3298
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmStringType:[I

    .line 3427
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$16;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    .line 3448
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$17;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    .line 3714
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandlerTemp:Landroid/os/Handler;

    .line 3716
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandlerTemp:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCaptioningObserver:Landroid/database/ContentObserver;

    .line 3783
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    return-void

    .line 3298
    nop

    :array_0
    .array-data 4
        0x73
        0x74
        0x75
        0x76
        0x77
        0x78
        0x79
        0x7a
        0x7b
        0x7c
    .end array-data
.end method

.method private final StopSelf()V
    .locals 2

    .prologue
    .line 1083
    const-string v0, "video"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setBatteryADC(Ljava/lang/String;Z)V

    .line 1084
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->stopSelf()V

    .line 1085
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "Exit popup player"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setProgressBarVisibility(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattStatus:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattStatus:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattScale:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattScale:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattLevel:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattLevel:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSeekPosition()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->saveResumePos()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/db/SharedPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->playNext()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->playPrevious()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSubtitleSyncTime()V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/data/SideSyncInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # J

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendSideSyncLastPlayedItemBroadcast(J)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->canPlayNext()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isCompletion:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isShortSeekDisalbedCase()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$3300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$3402(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z

    return p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateSubtitleTextSize(II)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsMoved:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsMoved:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    return p1
.end method

.method static synthetic access$4402(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    return p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    return v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # F

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    return p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bScreenScaling:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->doPauseResume()V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilitySubtitleView(Z)V

    return-void
.end method

.method static synthetic access$5100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isRequiredSmoothing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mRootLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$5302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Landroid/view/animation/Animation;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic access$5400()I
    .locals 1

    .prologue
    .line 108
    sget v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevNeedtoPause:I

    return v0
.end method

.method static synthetic access$5402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 108
    sput p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevNeedtoPause:I

    return p0
.end method

.method static synthetic access$5500()Z
    .locals 1

    .prologue
    .line 108
    sget-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBlockSmartPause:Z

    return v0
.end method

.method static synthetic access$5600()Z
    .locals 1

    .prologue
    .line 108
    sget-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPausedbySmartPause:Z

    return v0
.end method

.method static synthetic access$5602(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 108
    sput-boolean p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPausedbySmartPause:Z

    return p0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->callMoviePlayer(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    return v0
.end method

.method private adjustXYposition(II)V
    .locals 21
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2596
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 2597
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isMovedSinceRotate()Z

    move-result v17

    if-eqz v17, :cond_d

    .line 2598
    iget v14, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2599
    .local v14, "w":I
    iget v3, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2600
    .local v3, "h":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    .line 2601
    .local v15, "x":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    move/from16 v16, v0

    .line 2602
    .local v16, "y":I
    move/from16 v12, p1

    .line 2603
    .local v12, "scH":I
    move/from16 v13, p2

    .line 2604
    .local v13, "scW":I
    const/4 v8, 0x0

    .line 2605
    .local v8, "newX":I
    const/4 v9, 0x0

    .line 2607
    .local v9, "newY":I
    sub-int v17, v13, v14

    if-eqz v17, :cond_0

    .line 2608
    mul-int v17, v15, v12

    mul-int v18, v15, v14

    sub-int v17, v17, v18

    sub-int v18, v13, v14

    div-int v8, v17, v18

    .line 2609
    :cond_0
    sub-int v17, v12, v3

    if-eqz v17, :cond_1

    .line 2610
    mul-int v17, v16, v13

    mul-int v18, v16, v3

    sub-int v17, v17, v18

    sub-int v18, v12, v3

    div-int v9, v17, v18

    .line 2612
    :cond_1
    iput v8, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2613
    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2615
    if-gez v15, :cond_2

    iput v15, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2616
    :cond_2
    sub-int v17, v13, v15

    sub-int v17, v17, v14

    if-gez v17, :cond_3

    sub-int v17, v12, v14

    sub-int v18, v13, v15

    sub-int v18, v18, v14

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2617
    :cond_3
    if-gez v16, :cond_4

    move/from16 v0, v16

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2618
    :cond_4
    sub-int v17, v12, v16

    sub-int v17, v17, v3

    if-gez v17, :cond_5

    sub-int v17, v13, v3

    sub-int v18, v12, v16

    sub-int v18, v18, v3

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2621
    :cond_5
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    move/from16 v17, v0

    move/from16 v0, v17

    move/from16 v1, p1

    if-gt v0, v1, :cond_6

    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    move/from16 v17, v0

    move/from16 v0, v17

    move/from16 v1, p2

    if-le v0, v1, :cond_c

    .line 2622
    :cond_6
    iget v10, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2623
    .local v10, "preWidth":I
    iget v11, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2624
    .local v11, "preheight":I
    const/4 v7, 0x0

    .local v7, "newWidth":I
    const/4 v6, 0x0

    .line 2626
    .local v6, "newHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v17, v0

    if-eqz v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVisibility()I

    move-result v17

    if-nez v17, :cond_7

    .line 2627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getWidth()I

    move-result v10

    .line 2628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getHeight()I

    move-result v11

    .line 2631
    :cond_7
    if-le v10, v11, :cond_a

    .line 2632
    move/from16 v7, p1

    .line 2633
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v10

    move/from16 v18, v0

    int-to-float v0, v11

    move/from16 v19, v0

    div-float v18, v18, v19

    div-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v6, v0

    .line 2634
    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2635
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    if-gez v17, :cond_8

    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2636
    :cond_8
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    sub-int v18, p2, v6

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_9

    sub-int v17, p2, v6

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2645
    :cond_9
    :goto_0
    iput v7, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2646
    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2648
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkAndUpdateCtrlLayout(I)V

    .line 2649
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateSubtitleTextSize(II)V

    .line 2651
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    .line 2652
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    .line 2659
    .end local v6    # "newHeight":I
    .end local v7    # "newWidth":I
    .end local v10    # "preWidth":I
    .end local v11    # "preheight":I
    :goto_1
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setXY(II)V

    .line 2680
    .end local v3    # "h":I
    .end local v8    # "newX":I
    .end local v9    # "newY":I
    .end local v12    # "scH":I
    .end local v13    # "scW":I
    .end local v14    # "w":I
    .end local v15    # "x":I
    .end local v16    # "y":I
    :goto_2
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    move/from16 v17, v0

    or-int/lit8 v17, v17, 0x8

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2681
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2682
    return-void

    .line 2638
    .restart local v3    # "h":I
    .restart local v6    # "newHeight":I
    .restart local v7    # "newWidth":I
    .restart local v8    # "newX":I
    .restart local v9    # "newY":I
    .restart local v10    # "preWidth":I
    .restart local v11    # "preheight":I
    .restart local v12    # "scH":I
    .restart local v13    # "scW":I
    .restart local v14    # "w":I
    .restart local v15    # "x":I
    .restart local v16    # "y":I
    :cond_a
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v11

    move/from16 v18, v0

    int-to-float v0, v10

    move/from16 v19, v0

    div-float v18, v18, v19

    div-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v7, v0

    .line 2639
    move/from16 v6, p2

    .line 2640
    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2641
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    if-gez v17, :cond_b

    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2642
    :cond_b
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    sub-int v18, p1, v7

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_9

    sub-int v17, p1, v7

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_0

    .line 2654
    .end local v6    # "newHeight":I
    .end local v7    # "newWidth":I
    .end local v10    # "preWidth":I
    .end local v11    # "preheight":I
    :cond_c
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    .line 2655
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    .line 2656
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    .line 2657
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    goto :goto_1

    .line 2661
    .end local v3    # "h":I
    .end local v8    # "newX":I
    .end local v9    # "newY":I
    .end local v12    # "scH":I
    .end local v13    # "scW":I
    .end local v14    # "w":I
    .end local v15    # "x":I
    .end local v16    # "y":I
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    move/from16 v17, v0

    const/16 v18, -0x270f

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    move/from16 v17, v0

    const/16 v18, -0x270f

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    move/from16 v17, v0

    const/16 v18, -0x270f

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    move/from16 v17, v0

    const/16 v18, -0x270f

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_f

    .line 2662
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    move/from16 v20, v0

    invoke-virtual/range {v17 .. v20}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInitAIAxyPosition(III)[I

    move-result-object v4

    .line 2663
    .local v4, "i":[I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    move/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(II)[I

    move-result-object v14

    .line 2664
    .local v14, "w":[I
    const/16 v17, 0x0

    aget v17, v14, v17

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2665
    const/16 v17, 0x1

    aget v17, v14, v17

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2666
    const/16 v17, 0x0

    aget v17, v4, v17

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2667
    const/16 v17, 0x1

    aget v17, v4, v17

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2668
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setXY(II)V

    .line 2669
    const-string v17, "MiniVideoPlayerService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "adjustXYposition - video->aia->port screen: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " / "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2671
    .end local v4    # "i":[I
    .end local v14    # "w":[I
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2672
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2673
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    .line 2674
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    .line 2675
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    .line 2676
    iget v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    goto/16 :goto_2
.end method

.method private callMiniPlayer(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1966
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "callMiniPlayer E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1968
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    .line 1969
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1970
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1971
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 1972
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnSizeChangeListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;)V

    .line 1973
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1975
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->canPlayNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isSupportNextPrev()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1976
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->createPlaylist(Landroid/net/Uri;)V

    .line 1979
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1980
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->updateLastPlayedItem(Landroid/net/Uri;)V

    .line 1983
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkSKTCloudUrl(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1990
    :cond_4
    :goto_0
    return-void

    .line 1985
    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkCloudUrl(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1988
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private callMoviePlayer(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1829
    iget-boolean v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    const/4 v12, 0x1

    if-ne v9, v12, :cond_0

    .line 1963
    :goto_0
    return-void

    .line 1832
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v9, v12}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v10

    .line 1834
    .local v10, "videoID":J
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1835
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoID()Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1837
    :cond_1
    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 1838
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v9, v12}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v10

    .line 1840
    :cond_2
    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-nez v9, :cond_4

    .line 1841
    const/4 v1, 0x0

    .line 1843
    .local v1, "filePath":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v9, :cond_3

    .line 1844
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v12, "file://"

    invoke-virtual {v9, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1845
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1850
    :cond_3
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1851
    const-string v9, "MiniVideoPlayerService"

    const-string v12, "callMoviePlayer() - streaming case. it\'s ok. continue"

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1863
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_4
    :goto_2
    new-instance v2, Landroid/content/Intent;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    const-class v12, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {v2, v9, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1864
    .local v2, "i":Landroid/content/Intent;
    const/4 v8, 0x0

    .line 1866
    .local v8, "tempIntent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGallerySecureLock()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1867
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "i":Landroid/content/Intent;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    const-class v12, Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;

    invoke-direct {v2, v9, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1870
    .restart local v2    # "i":Landroid/content/Intent;
    :cond_5
    iget-boolean v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsCalledScalewindow:Z

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScalewindowRect:Landroid/graphics/Rect;

    if-eqz v9, :cond_6

    .line 1871
    const/4 v9, 0x0

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScalewindowRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v9, v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    move-result-object v8

    .line 1874
    :cond_6
    if-eqz v8, :cond_7

    .line 1875
    move-object v2, v8

    .line 1878
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileBrowsableMode()Z

    move-result v9

    if-eqz v9, :cond_10

    .line 1879
    const/4 v7, 0x0

    .line 1880
    .local v7, "s":Ljava/lang/String;
    const-string v9, "bucket"

    const/4 v12, 0x0

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1881
    const-string v9, "search"

    invoke-virtual {v2, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1888
    .end local v7    # "s":Ljava/lang/String;
    :goto_3
    const-string v9, "subtitleLanguageIdx"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1890
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-boolean v9, v9, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    if-eqz v9, :cond_11

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-boolean v9, v9, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    if-eqz v9, :cond_11

    .line 1891
    const-string v9, "subtitleFile"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFile()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1892
    const-string v9, "subtitleActivation"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-boolean v12, v12, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1893
    const-string v9, "subttileSyncTime"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleSyncTime()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1894
    const-string v9, "subtitleIsMulti"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getIsMultiSubtitle()Z

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1895
    const-string v9, "subtitleMultiCount"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getMultiSelectSubtitleCount()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1896
    const-string v9, "subtitleMultiIndex"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 1915
    :goto_4
    const-string v9, "ListKey"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1916
    const-string v9, "WhereFrom"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getFrom()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1917
    const-string v9, "ListType"

    iget v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1918
    const-string v9, "filePath"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1919
    const-string v9, "playSpeed"

    iget v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1920
    const-string v9, "audioTrack"

    iget v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1922
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDmsTitle:Ljava/lang/String;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDmsTitle:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    .line 1923
    const-string v9, "title_name"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDmsTitle:Ljava/lang/String;

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1926
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v9

    if-eqz v9, :cond_13

    .line 1927
    const-string v9, "resumePos"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1932
    :goto_5
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1933
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v9, :cond_9

    .line 1934
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1935
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPathForAllShare(Ljava/lang/String;)J

    move-result-wide v4

    .line 1937
    .local v4, "id":J
    const-wide/16 v12, -0x1

    cmp-long v9, v4, v12

    if-lez v9, :cond_9

    .line 1938
    sget-object v9, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-static {v9, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    .line 1941
    .end local v4    # "id":J
    :cond_9
    const-string v9, "CurrentProviderName"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCurrentProviderName:Ljava/lang/String;

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1944
    :cond_a
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v9, :cond_b

    .line 1945
    const-string v9, "uri"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1947
    :cond_b
    const-string v9, "bucketid"

    iget v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1948
    const-string v9, "title"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1949
    const-string v9, "yosemite_enable"

    iget-boolean v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bWatchON:Z

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1950
    const-string v9, "isfromMovieStore"

    iget-boolean v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsfromMovieStore:Z

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1952
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isFromSideSync()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1953
    const-string v9, "title_of_sidesync"

    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->getTitleofSideSync()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1954
    const-string v9, "needSaveResumePos"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isNeedSaveResumePos()Z

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1957
    :cond_c
    const-string v9, "ActionVideoMinimodeService"

    invoke-virtual {v2, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1958
    const/high16 v9, 0x14000000

    invoke-virtual {v2, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1959
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1960
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    .line 1961
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    .line 1962
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    goto/16 :goto_0

    .line 1847
    .end local v2    # "i":Landroid/content/Intent;
    .end local v8    # "tempIntent":Landroid/content/Intent;
    .restart local v1    # "filePath":Ljava/lang/String;
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 1852
    :cond_e
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v9, :cond_f

    if-eqz v1, :cond_f

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->canRead()Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1853
    const-string v9, "MiniVideoPlayerService"

    const-string v12, "callMoviePlayer() - no id but file exist. it\'s ok. continue"

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1855
    :cond_f
    const-string v9, "MiniVideoPlayerService"

    const-string v12, "callMoviePlayer() - not found!! is it deleted?"

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1856
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    const v12, 0x7f0a00f7

    invoke-virtual {v9, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1857
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    .line 1858
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto/16 :goto_0

    .line 1883
    .end local v1    # "filePath":Ljava/lang/String;
    .restart local v2    # "i":Landroid/content/Intent;
    .restart local v8    # "tempIntent":Landroid/content/Intent;
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1884
    .local v6, "mUri":Landroid/net/Uri;
    const-string v9, "video/*"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 1898
    .end local v6    # "mUri":Landroid/net/Uri;
    :cond_11
    const/4 v3, 0x0

    .line 1900
    .local v3, "isSubtitleOptionOn":Z
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v12, "accessibility_sec_captioning_enabled"

    invoke-static {v9, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    const/4 v12, 0x1

    if-ne v9, v12, :cond_12

    .line 1901
    const/4 v3, 0x1

    .line 1909
    :goto_6
    const-string v9, "subtitleActivation"

    invoke-virtual {v2, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1910
    const-string v9, "subtitleIsMulti"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getIsMultiSubtitle()Z

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1911
    const-string v9, "subtitleMultiCount"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getMultiSelectSubtitleCount()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1912
    const-string v9, "subtitleMultiIndex"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    goto/16 :goto_4

    .line 1903
    :cond_12
    const/4 v3, 0x0

    goto :goto_6

    .line 1904
    :catch_0
    move-exception v0

    .line 1905
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const/4 v3, 0x0

    .line 1906
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_6

    .line 1929
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .end local v3    # "isSubtitleOptionOn":Z
    :cond_13
    const-string v9, "resumePos"

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getSeekPosition()I

    move-result v12

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_5
.end method

.method private canPlayNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1618
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1627
    :cond_0
    :goto_0
    return v1

    .line 1622
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "autoPlay"

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    .line 1623
    .local v0, "saved":Z
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFullBrowsableMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1624
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private checkCloudUrl(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 2086
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "content://cloud/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isServerOnlyContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->checkInvalidCloudId(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2088
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->showLoadingDialog(I)V

    .line 2089
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->setOnUrlUpdatedListener(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;)V

    .line 2090
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getUrlByUri(Landroid/net/Uri;)V

    .line 2091
    const/4 v0, 0x1

    .line 2094
    :cond_1
    return v0
.end method

.method private checkDRMFile(Landroid/net/Uri;)I
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/16 v9, 0xc

    const/16 v12, 0x1e

    const/16 v7, 0xd

    const/4 v11, 0x3

    const/16 v8, 0xa

    .line 3313
    if-nez p1, :cond_1

    .line 3424
    :cond_0
    :goto_0
    return v7

    .line 3316
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-eqz v10, :cond_0

    .line 3319
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 3321
    .local v6, "path":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->reset()V

    .line 3322
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v10, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v1

    .line 3323
    .local v1, "DrmType":I
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v10, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 3325
    .local v0, "DrmRigthsStatus":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3327
    :pswitch_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v10, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initOMADrmDeliveryType(Ljava/lang/String;)V

    .line 3329
    if-nez v0, :cond_3

    .line 3331
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v10, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initOMADrmConstraintsInfo(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 3332
    const-string v7, "MiniVideoPlayerService"

    const-string v9, "checkDRMFile - get DRM information error"

    invoke-static {v7, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 3333
    goto :goto_0

    .line 3335
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->setValidOMADrmMsg(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    .line 3336
    const-string v8, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkDRMFile - mDrmPopupType : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3338
    iget v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    if-eq v12, v8, :cond_0

    .line 3339
    const-string v7, "MiniVideoPlayerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkDRMFile : pop up type = "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmStringType:[I

    iget v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    add-int/lit8 v11, v11, -0x1e

    aget v10, v10, v11

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v9

    .line 3340
    goto :goto_0

    .line 3345
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->setInvalidOMADrmMsg()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    .line 3346
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkDRMFile - Invalid rights. mDrmPopupType : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3348
    iget v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    if-eq v12, v9, :cond_0

    .line 3349
    const-string v7, "MiniVideoPlayerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkDRMFile : pop up type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmStringType:[I

    iget v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    add-int/lit8 v11, v11, -0x1e

    aget v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 3350
    goto/16 :goto_0

    .line 3358
    :pswitch_1
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "checkDRMFile. VIDEO_DRM_PRDRM type."

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3360
    sget-boolean v9, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WMDRM_NOT_SUPPORT:Z

    if-eqz v9, :cond_4

    if-ne v1, v11, :cond_4

    move v7, v8

    .line 3361
    goto/16 :goto_0

    .line 3364
    :cond_4
    if-eq v0, v11, :cond_5

    const/4 v9, 0x2

    if-ne v0, v9, :cond_8

    .line 3365
    :cond_5
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->drm_uri:Ljava/lang/String;

    .line 3367
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsNetworkConnected()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 3368
    new-instance v3, Landroid/drm/DrmManagerClient;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 3369
    .local v3, "mDrmClient":Landroid/drm/DrmManagerClient;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3371
    .local v5, "mimeType":Ljava/lang/String;
    new-instance v4, Landroid/drm/DrmInfoRequest;

    invoke-direct {v4, v11, v5}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 3374
    .local v4, "mDrminfoRequest_DLA":Landroid/drm/DrmInfoRequest;
    const-string v7, "drm_path"

    invoke-virtual {v4, v7, v6}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3376
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 3377
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v2

    .line 3379
    .local v2, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    const-string v7, "user_guid"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getUserGuid()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3380
    const-string v7, "imei"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getImei()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3381
    const-string v7, "app_Id"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getAppID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3382
    const-string v7, "orderId"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getOrderID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3383
    const-string v7, "media_hub"

    const-string v8, "Media_hub"

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3384
    const-string v7, "mv_id"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getMVID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3385
    const-string v7, "svr_id"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getSvrID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3388
    .end local v2    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    invoke-virtual {v3, v7}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    .line 3389
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    invoke-virtual {v3, v7}, Landroid/drm/DrmManagerClient;->setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V

    .line 3390
    invoke-virtual {v3, v4}, Landroid/drm/DrmManagerClient;->acquireRights(Landroid/drm/DrmInfoRequest;)I

    .line 3391
    const-string v7, "MiniVideoPlayerService"

    const-string v8, "License Acquisitin has started"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3392
    const/16 v7, 0xe

    goto/16 :goto_0

    .line 3394
    .end local v3    # "mDrmClient":Landroid/drm/DrmManagerClient;
    .end local v4    # "mDrminfoRequest_DLA":Landroid/drm/DrmInfoRequest;
    .end local v5    # "mimeType":Ljava/lang/String;
    :cond_7
    const-string v7, "MiniVideoPlayerService"

    const-string v9, "No Network Connection"

    invoke-static {v7, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 3395
    goto/16 :goto_0

    .line 3397
    :cond_8
    const/4 v8, 0x1

    if-ne v0, v8, :cond_0

    .line 3398
    const/16 v7, 0x78

    goto/16 :goto_0

    .line 3403
    :pswitch_2
    const-string v10, "MiniVideoPlayerService"

    const-string v11, "checkDRMFile Divx type."

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3404
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v10, v0, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->divxPopupType(ILjava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    .line 3406
    iget v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    if-eq v10, v12, :cond_0

    .line 3407
    iget v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    const/16 v11, 0x24

    if-ne v10, v11, :cond_9

    .line 3408
    const-string v7, "MiniVideoPlayerService"

    const-string v9, "checkDRMFile Divx return false. expired"

    invoke-static {v7, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 3409
    goto/16 :goto_0

    .line 3410
    :cond_9
    iget v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    const/16 v10, 0x26

    if-ne v8, v10, :cond_a

    .line 3411
    const-string v7, "MiniVideoPlayerService"

    const-string v8, "checkDRMFile Divx return false. expired"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3412
    const/16 v7, 0xb

    goto/16 :goto_0

    .line 3413
    :cond_a
    iget v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    const/16 v10, 0x27

    if-ne v8, v10, :cond_0

    .line 3414
    const-string v7, "MiniVideoPlayerService"

    const-string v8, "checkDRMFile Divx return false. rental"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v9

    .line 3416
    goto/16 :goto_0

    .line 3325
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private checkSKTCloudUrl(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 2109
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "content://com.skp.tcloud/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2119
    :cond_0
    :goto_0
    return v0

    .line 2112
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isServerOnlyContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2113
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->showLoadingDialog(I)V

    .line 2114
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mUrlUpdatedSKTcloudListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->setOnUrlUpdatedSKTcloudListener(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;)V

    .line 2115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getUrlByUri(Landroid/net/Uri;)V

    .line 2116
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private createPlaylist(Landroid/net/Uri;)V
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2280
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileBrowsableMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2361
    :cond_0
    :goto_0
    return-void

    .line 2282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 2283
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    .line 2286
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2287
    const/4 v6, 0x0

    .line 2289
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2290
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_3

    .line 2291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 2294
    :cond_3
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    const/16 v2, 0x9

    if-ne v0, v2, :cond_7

    .line 2295
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    invoke-virtual {v0, v2, v1, v3, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v6

    .line 2296
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v6

    .line 2298
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_4

    .line 2299
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v6

    .line 2317
    :cond_4
    :goto_1
    if-eqz v6, :cond_e

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 2318
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2320
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_5

    .line 2321
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 2324
    :cond_5
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v8, v0, :cond_d

    .line 2325
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 2326
    .local v10, "id":J
    const/4 v9, 0x0

    .line 2328
    .local v9, "newUri":Landroid/net/Uri;
    const-wide/16 v2, 0x0

    cmp-long v0, v10, v2

    if-ltz v0, :cond_6

    .line 2329
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2330
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .line 2339
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2340
    const-string v0, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createPlaylist. video id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2324
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 2302
    .end local v8    # "i":I
    .end local v9    # "newUri":Landroid/net/Uri;
    .end local v10    # "id":J
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v6

    .line 2303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, v6, v3}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2305
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_4

    .line 2306
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, v6, v3}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_1

    .line 2309
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2310
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursorExternalType(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_1

    .line 2311
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2312
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/16 v5, 0xb

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZI)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_1

    .line 2331
    .restart local v8    # "i":I
    .restart local v9    # "newUri":Landroid/net/Uri;
    .restart local v10    # "id":J
    :cond_a
    :try_start_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2332
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    goto/16 :goto_3

    .line 2333
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2334
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    goto/16 :goto_3

    .line 2336
    :cond_c
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    goto/16 :goto_3

    .line 2344
    .end local v9    # "newUri":Landroid/net/Uri;
    .end local v10    # "id":J
    :cond_d
    const-string v0, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createPlaylist. size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2349
    .end local v8    # "i":I
    :cond_e
    if-eqz v6, :cond_f

    .line 2350
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2351
    const/4 v6, 0x0

    .line 2355
    :cond_f
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    .line 2357
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    if-ltz v0, :cond_10

    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 2358
    :cond_10
    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    goto/16 :goto_0

    .line 2346
    :catch_0
    move-exception v7

    .line 2347
    .local v7, "e":Ljava/lang/NumberFormatException;
    :try_start_2
    const-string v0, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createPlaylist - NumberFormatException :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2349
    if-eqz v6, :cond_f

    .line 2350
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2351
    const/4 v6, 0x0

    goto :goto_4

    .line 2349
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_11

    .line 2350
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2351
    const/4 v6, 0x0

    :cond_11
    throw v0
.end method

.method private doPauseResume()V
    .locals 4

    .prologue
    .line 1594
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "doPauseResume()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-nez v1, :cond_0

    .line 1615
    :goto_0
    return-void

    .line 1598
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1599
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    goto :goto_0

    .line 1601
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1602
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a0067

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_0

    .line 1604
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->IsPauseEnable()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1605
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "sound_effect"

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 1606
    .local v0, "saved":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSoundAliveMode(I)V

    .line 1607
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->start()V

    .line 1608
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSubtitleSyncTime()V

    goto :goto_0

    .line 1610
    .end local v0    # "saved":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->callMiniPlayer(Landroid/net/Uri;)V

    .line 1611
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->showLoadingDialog(I)V

    goto :goto_0
.end method

.method private exitThis()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 1040
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "onCompletion() - exitThis."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1044
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1046
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isBrowser(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1047
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendVHLastPlayedItemBroadcast(Z)V

    .line 1050
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->broadcastPausedPosition(Z)V

    .line 1052
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1053
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 1066
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    .line 1068
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v1, :cond_2

    .line 1069
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->setOnUrlUpdatedSKTcloudListener(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;)V

    .line 1070
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v1, :cond_3

    .line 1071
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->setOnUrlUpdatedListener(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;)V

    .line 1073
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 1075
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    if-eqz v1, :cond_4

    .line 1076
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->DeleteRemoteSubTitleFile()V

    .line 1079
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    .line 1080
    return-void

    .line 1057
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isFromSideSync()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1058
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendSideSyncLastPlayedItemBroadcast(J)V

    .line 1061
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1062
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    goto :goto_0
.end method

.method private getNextID(Landroid/database/Cursor;)J
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    .line 2420
    const-wide/16 v0, -0x1

    .line 2421
    .local v0, "videoID":J
    if-nez p1, :cond_0

    move-wide v2, v0

    .line 2439
    .end local v0    # "videoID":J
    .local v2, "videoID":J
    :goto_0
    return-wide v2

    .line 2423
    .end local v2    # "videoID":J
    .restart local v0    # "videoID":J
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2425
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2426
    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 2427
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 2428
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2429
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2430
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_1
    :goto_2
    move-wide v2, v0

    .line 2439
    .end local v0    # "videoID":J
    .restart local v2    # "videoID":J
    goto :goto_0

    .line 2432
    .end local v2    # "videoID":J
    .restart local v0    # "videoID":J
    :cond_2
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 2434
    goto :goto_2

    .line 2436
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1
.end method

.method private getNextPositionForPlaylist()I
    .locals 2

    .prologue
    .line 2364
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    .line 2365
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 2366
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    .line 2368
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    return v0
.end method

.method private getNextStoreUri()Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2242
    const/4 v2, 0x0

    .line 2244
    .local v2, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2245
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    .line 2246
    .local v0, "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->createLocalCursorOnDevice()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_0

    .line 2247
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "shiftContent() : DownloadList is empty"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2260
    .end local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :goto_0
    return-object v3

    .line 2251
    .restart local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :cond_0
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getFilePathOnDevice(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2252
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 2253
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "shiftContent() : getFilePathOnDevice() is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2256
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    .line 2258
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .end local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    .end local v1    # "path":Ljava/lang/String;
    :cond_2
    move-object v3, v2

    .line 2260
    goto :goto_0
.end method

.method private declared-synchronized getNextUri()Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2169
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getNextStoreUri()Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2213
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    .line 2171
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2172
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->updatePlayingdata(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V

    .line 2173
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->getUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 2176
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileBrowsableMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2178
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getVideoCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 2180
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_3

    .line 2181
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "getNextUri. Uri is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2169
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 2183
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_3
    if-eqz v0, :cond_4

    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gtz v3, :cond_4

    .line 2184
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "getNextUri. cursor.getCount()<=0"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2185
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2189
    :cond_4
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 2190
    if-eqz v0, :cond_5

    .line 2191
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2192
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 2194
    :catch_0
    move-exception v1

    .line 2195
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_4
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "getNextUri - maybe last file has just deleted."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2196
    if-eqz v0, :cond_0

    .line 2197
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 2199
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_1
    move-exception v3

    :try_start_5
    throw v3

    .line 2204
    :cond_6
    const/4 v2, 0x0

    .line 2207
    .local v2, "uri":Landroid/net/Uri;
    :cond_7
    sget-object v3, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getUriFromCursor(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Landroid/net/Uri;

    move-result-object v2

    .line 2208
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isDrmContent(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2210
    if-eqz v0, :cond_0

    .line 2211
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method private getPositionFromPlaylist(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)I
    .locals 3
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 1484
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_2

    .line 1485
    :cond_0
    const/4 v0, 0x0

    .line 1501
    :cond_1
    :goto_0
    return v0

    .line 1488
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1490
    .local v0, "position":I
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1491
    add-int/lit8 v0, v0, -0x1

    .line 1492
    if-gez v0, :cond_1

    .line 1493
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    goto :goto_0

    .line 1495
    :cond_3
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1496
    add-int/lit8 v0, v0, 0x1

    .line 1497
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1498
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPrevID(Landroid/database/Cursor;)J
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    .line 2442
    const-wide/16 v0, -0x1

    .line 2443
    .local v0, "videoID":J
    if-nez p1, :cond_0

    move-wide v2, v0

    .line 2459
    .end local v0    # "videoID":J
    .local v2, "videoID":J
    :goto_0
    return-wide v2

    .line 2445
    .end local v2    # "videoID":J
    .restart local v0    # "videoID":J
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 2447
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2448
    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 2449
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 2450
    invoke-interface {p1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2451
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 2453
    :cond_1
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_2
    move-wide v2, v0

    .line 2459
    .end local v0    # "videoID":J
    .restart local v2    # "videoID":J
    goto :goto_0

    .line 2456
    .end local v2    # "videoID":J
    .restart local v0    # "videoID":J
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    goto :goto_1
.end method

.method private getPrevPositionForPlaylist()I
    .locals 1

    .prologue
    .line 2372
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    .line 2373
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    if-gez v0, :cond_0

    .line 2374
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    .line 2376
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->currentPosition:I

    return v0
.end method

.method private getPrevStoreUri()Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2217
    const/4 v2, 0x0

    .line 2219
    .local v2, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2220
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    .line 2222
    .local v0, "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->createLocalCursorOnDevice()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_0

    .line 2223
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "shiftContent() : DownloadList is empty"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    .end local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :goto_0
    return-object v3

    .line 2227
    .restart local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :cond_0
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getFilePathOnDevice(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2229
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 2230
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "shiftContent() : getFilePathOnDevice() is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2233
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    .line 2236
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .end local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    .end local v1    # "path":Ljava/lang/String;
    :cond_2
    move-object v3, v2

    .line 2238
    goto :goto_0
.end method

.method private declared-synchronized getPrevUri()Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2123
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getPrevStoreUri()Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2165
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    .line 2125
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2126
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->updatePlayingdata(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V

    .line 2127
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->getUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 2130
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileBrowsableMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2132
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getVideoCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 2134
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_4

    .line 2135
    :cond_3
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "getPrevUri. cursor is NULL"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2136
    if-eqz v0, :cond_0

    .line 2137
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2123
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 2141
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_4
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 2142
    if-eqz v0, :cond_5

    .line 2143
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2144
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2146
    :catch_0
    move-exception v1

    .line 2147
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_3
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "getPrevID() - maybe last file has just deleted."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2148
    if-eqz v0, :cond_0

    .line 2149
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 2151
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_1
    move-exception v3

    :try_start_4
    throw v3

    .line 2156
    :cond_6
    const/4 v2, 0x0

    .line 2159
    .local v2, "uri":Landroid/net/Uri;
    :cond_7
    sget-object v3, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getUriFromCursor(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Landroid/net/Uri;

    move-result-object v2

    .line 2160
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isDrmContent(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2162
    if-eqz v0, :cond_0

    .line 2163
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method private getScaleMultiplier()I
    .locals 1

    .prologue
    .line 2806
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v0, :cond_1

    .line 2807
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2808
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVideoWidth()I

    move-result v0

    .line 2812
    :goto_0
    return v0

    .line 2810
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVideoHeight()I

    move-result v0

    goto :goto_0

    .line 2812
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleMultiplier:I

    goto :goto_0
.end method

.method private getUriFromCursor(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Landroid/net/Uri;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "playDirection"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 2404
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2405
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getNextID(Landroid/database/Cursor;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    .line 2409
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2410
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2416
    .local v0, "uri":Landroid/net/Uri;
    :goto_1
    return-object v0

    .line 2407
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getPrevID(Landroid/database/Cursor;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    goto :goto_0

    .line 2411
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2412
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "id"

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .restart local v0    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 2414
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .restart local v0    # "uri":Landroid/net/Uri;
    goto :goto_1
.end method

.method private getVideoCursor()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2462
    const/4 v6, 0x0

    .line 2464
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2465
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    const/16 v2, 0x9

    if-ne v0, v2, :cond_2

    .line 2466
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getBucketIdbyPath(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2, v1, v3, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v6

    .line 2479
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isDataNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2480
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, v6, v3}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2483
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isDataNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2484
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, v6, v3}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :cond_1
    move-object v3, v6

    .line 2487
    :goto_1
    return-object v3

    .line 2468
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    .line 2470
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2471
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursorExternalType(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    .line 2472
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2473
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/16 v5, 0xb

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZI)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    .line 2475
    :cond_5
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "getVideoCursor() ???? - why am I here?"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private initScaleMultiplier()V
    .locals 1

    .prologue
    .line 2793
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isRequiredSmoothing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2794
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->is1080pEquivalent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2795
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getScaleMultiplier()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleMultiplier:I

    .line 2803
    :goto_0
    return-void

    .line 2798
    :cond_0
    const/16 v0, 0x780

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleMultiplier:I

    goto :goto_0

    .line 2801
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getScaleMultiplier()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleMultiplier:I

    goto :goto_0
.end method

.method private initSubtitle()V
    .locals 2

    .prologue
    .line 3691
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "initSubtitle E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3693
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkExistSubtitle(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->isRemoteSubtitleFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3694
    :cond_0
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "initSubtitle() : outband or remote subtitle"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3699
    :goto_0
    return-void

    .line 3696
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 3697
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    goto :goto_0
.end method

.method private isBrowser(Ljava/lang/String;)Z
    .locals 1
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    .line 3580
    const-string v0, "http"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "rtsp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sshttp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3581
    :cond_0
    const/4 v0, 0x1

    .line 3583
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDrmContent(Landroid/net/Uri;)Z
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2380
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 2382
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 2383
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_DRM_AIA_FUNCTION:Z

    if-eqz v3, :cond_2

    .line 2385
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v2, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    :cond_0
    move v1, v2

    .line 2398
    :cond_1
    :goto_0
    return v1

    .line 2393
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    move v1, v2

    .line 2394
    goto :goto_0
.end method

.method private isHubContent(Landroid/net/Uri;)Z
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 2068
    if-nez p1, :cond_1

    .line 2071
    :cond_0
    :goto_0
    return v2

    .line 2069
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    .line 2070
    .local v0, "db":Lcom/sec/android/app/videoplayer/db/VideoDB;
    const-string v3, "pricing_type_code"

    invoke-virtual {v0, p1, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2071
    .local v1, "pricingCode":Ljava/lang/String;
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isMovedSinceRotate()Z
    .locals 1

    .prologue
    .line 2685
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRequiredSmoothing()Z
    .locals 1

    .prologue
    .line 2816
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isShortSeekDisalbedCase()Z
    .locals 3

    .prologue
    .line 1581
    const/4 v0, 0x0

    .line 1583
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGallerySecureLock()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1584
    :cond_0
    const/4 v0, 0x1

    .line 1590
    :cond_1
    :goto_0
    return v0

    .line 1585
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1586
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a0067

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1587
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSubtitleActivationOn()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1148
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFileTyp()I

    move-result v3

    const/16 v4, 0x69

    if-ne v3, v4, :cond_1

    .line 1149
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->isCaptionEnable()Z

    move-result v1

    .line 1158
    :cond_0
    :goto_0
    return v1

    .line 1150
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accessibility_sec_captioning_enabled"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eq v3, v1, :cond_0

    move v1, v2

    .line 1153
    goto :goto_0

    .line 1154
    :catch_0
    move-exception v0

    .line 1155
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    move v1, v2

    .line 1158
    goto :goto_0
.end method

.method private openVideo(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2026
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "openVideo E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2027
    if-nez p1, :cond_0

    .line 2050
    :goto_0
    return-void

    .line 2032
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2033
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sendBroadcastPlayedContentInfo()V

    .line 2036
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isBrowser(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2038
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendVHLastPlayedItemBroadcast(Z)V

    .line 2043
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateSecureMode(Landroid/net/Uri;)V

    .line 2045
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setVideoURI(Landroid/net/Uri;)V

    .line 2046
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestFocus()Z

    .line 2047
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->start()V

    .line 2049
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->saveRecentlyPlayedTime(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private playNext()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1269
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "playNext() E"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-nez v3, :cond_1

    .line 1272
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "playNext. mMiniPlayer is NULL"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    :cond_0
    :goto_0
    return-void

    .line 1276
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isCompletion:Z

    if-nez v3, :cond_2

    .line 1277
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->saveResumePos()V

    .line 1280
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    .line 1281
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 1283
    const/16 v2, 0xd

    .line 1284
    .local v2, "ret":I
    const/4 v1, 0x0

    .line 1285
    .local v1, "nextUri":Landroid/net/Uri;
    iput-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->drm_uri:Ljava/lang/String;

    .line 1288
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileBrowsableMode()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1289
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 1291
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getNextPositionForPlaylist()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nextUri":Landroid/net/Uri;
    check-cast v1, Landroid/net/Uri;

    .line 1292
    .restart local v1    # "nextUri":Landroid/net/Uri;
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isDrmContent(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1298
    :cond_5
    :goto_1
    const-string v3, "MiniVideoPlayerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "playNext. nextUri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    if-nez v1, :cond_7

    .line 1301
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->abandonAudioFocus()V

    .line 1302
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto :goto_0

    .line 1295
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getNextUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    .line 1306
    :cond_7
    const/4 v3, 0x5

    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I

    .line 1307
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I

    .line 1308
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    .line 1309
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getVideoCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 1310
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_8

    .line 1311
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getNextID(Landroid/database/Cursor;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    .line 1312
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1315
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1316
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1327
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateVideoWidget()V

    .line 1328
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->displayName:Ljava/lang/String;

    .line 1330
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkCloudUrl(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkSKTCloudUrl(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1333
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkDRMFile(Landroid/net/Uri;)I

    move-result v2

    .line 1335
    const/16 v3, 0xb

    if-eq v2, v3, :cond_3

    .line 1337
    sparse-switch v2, :sswitch_data_0

    .line 1370
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1371
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v4

    invoke-virtual {v3, v1, v4, v7, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->initRemoteSubtitle(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V

    .line 1374
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1375
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleLang()V

    .line 1376
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isSubtitleActivationOn()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 1377
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initSubtitle()V

    .line 1378
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    .line 1379
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1317
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1318
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    .line 1319
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->getResumePosition()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    goto :goto_2

    .line 1321
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v3

    if-nez v3, :cond_c

    .line 1322
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1324
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    goto/16 :goto_2

    .line 1339
    :sswitch_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const v4, 0x7f0a00ea

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1340
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->startVideoList()V

    .line 1341
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto/16 :goto_0

    .line 1345
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const v4, 0x7f0a01ab

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1346
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto/16 :goto_0

    .line 1350
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    iget v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getPopupString(IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(Ljava/lang/CharSequence;)V

    .line 1351
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V

    .line 1353
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1354
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v4

    invoke-virtual {v3, v1, v4, v7, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->initRemoteSubtitle(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V

    .line 1356
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1357
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleLang()V

    .line 1358
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isSubtitleActivationOn()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 1359
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initSubtitle()V

    .line 1360
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    goto/16 :goto_0

    .line 1365
    :sswitch_3
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->showLoadingDialog(I)V

    goto/16 :goto_0

    .line 1337
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xc -> :sswitch_2
        0xe -> :sswitch_3
        0x78 -> :sswitch_1
    .end sparse-switch
.end method

.method private playPrevious()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1162
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "playPrevious() E"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1164
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-nez v2, :cond_1

    .line 1165
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "playNext. mMiniPlayer is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1266
    :cond_0
    :goto_0
    return-void

    .line 1169
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->saveResumePos()V

    .line 1170
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    .line 1171
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 1173
    const/16 v1, 0xd

    .line 1174
    .local v1, "ret":I
    const/4 v0, 0x0

    .line 1175
    .local v0, "prevUri":Landroid/net/Uri;
    iput-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->drm_uri:Ljava/lang/String;

    .line 1178
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileBrowsableMode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1179
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 1181
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getPrevPositionForPlaylist()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "prevUri":Landroid/net/Uri;
    check-cast v0, Landroid/net/Uri;

    .line 1182
    .restart local v0    # "prevUri":Landroid/net/Uri;
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isDrmContent(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1187
    :cond_4
    :goto_1
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playNext. nextUri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    if-nez v0, :cond_6

    .line 1190
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->abandonAudioFocus()V

    .line 1191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto :goto_0

    .line 1185
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getPrevUri()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 1195
    :cond_6
    const/4 v2, 0x5

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I

    .line 1196
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I

    .line 1197
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    .line 1199
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1200
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1211
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateVideoWidget()V

    .line 1212
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->displayName:Ljava/lang/String;

    .line 1214
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkCloudUrl(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkSKTCloudUrl(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1217
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkDRMFile(Landroid/net/Uri;)I

    move-result v1

    .line 1219
    const/16 v2, 0xb

    if-eq v1, v2, :cond_2

    .line 1221
    sparse-switch v1, :sswitch_data_0

    .line 1254
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1255
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v3

    invoke-virtual {v2, v0, v3, v6, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->initRemoteSubtitle(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V

    .line 1258
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1259
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleLang()V

    .line 1260
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isSubtitleActivationOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 1261
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initSubtitle()V

    .line 1262
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    .line 1263
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1201
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1202
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    .line 1203
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getResumePosition()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    goto :goto_2

    .line 1205
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1206
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1208
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    goto/16 :goto_2

    .line 1223
    :sswitch_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a00ea

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1224
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->startVideoList()V

    .line 1225
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto/16 :goto_0

    .line 1229
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a01ab

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1230
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V

    goto/16 :goto_0

    .line 1234
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmPopupType:I

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getPopupString(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(Ljava/lang/CharSequence;)V

    .line 1235
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V

    .line 1237
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1238
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v3

    invoke-virtual {v2, v0, v3, v6, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->initRemoteSubtitle(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V

    .line 1240
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1241
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleLang()V

    .line 1242
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isSubtitleActivationOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 1243
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initSubtitle()V

    .line 1244
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    goto/16 :goto_0

    .line 1249
    :sswitch_3
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->showLoadingDialog(I)V

    goto/16 :goto_0

    .line 1221
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xc -> :sswitch_2
        0xe -> :sswitch_3
        0x78 -> :sswitch_1
    .end sparse-switch
.end method

.method private reCalculateScaleFactor(Landroid/util/TypedValue;)F
    .locals 4
    .param p1, "defaultValue"    # Landroid/util/TypedValue;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2844
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v2, :cond_2

    .line 2845
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 2846
    .local v0, "initWidth":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2847
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getWidth()I

    move-result v2

    if-gt v2, v0, :cond_1

    .line 2848
    invoke-virtual {p1}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    .line 2861
    .end local v0    # "initWidth":I
    :cond_0
    :goto_0
    return v1

    .line 2849
    .restart local v0    # "initWidth":I
    :cond_1
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    cmpl-float v2, v2, v1

    if-gtz v2, :cond_0

    .line 2861
    .end local v0    # "initWidth":I
    :cond_2
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    goto :goto_0

    .line 2853
    .restart local v0    # "initWidth":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getHeight()I

    move-result v2

    if-gt v2, v0, :cond_4

    .line 2854
    invoke-virtual {p1}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    goto :goto_0

    .line 2855
    :cond_4
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    goto :goto_0
.end method

.method private register()V
    .locals 5

    .prologue
    .line 767
    const-string v3, "MiniVideoPlayerService"

    const-string v4, "register()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 769
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 770
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 771
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 772
    const-string v3, "intent.stop.app-in-app"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 773
    const-string v3, "android.intent.action.PALM_DOWN"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 774
    const-string v3, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 777
    const-string v3, "AppInAppResumePositionRequest"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 778
    const-string v3, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 779
    const-string v3, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 780
    const-string v3, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 781
    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 782
    sget-object v3, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 783
    sget-object v3, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 784
    const-string v3, "com.android.internal.policy.impl.sec.glanceview.eventinfo"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 785
    const-string v3, "com.sec.android.app.videoplayer.SORT_BY"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 786
    const-string v3, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 787
    const-string v3, "com.android.settings.action.talkback_off"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 788
    const-string v3, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 790
    const-string v3, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 792
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 794
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 795
    .local v2, "mediaFilter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 796
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 797
    const-string v3, "file"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 798
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 800
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 801
    .local v1, "mediaBtnFilter":Landroid/content/IntentFilter;
    sget-object v3, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 802
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 804
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-nez v3, :cond_0

    .line 805
    new-instance v3, Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 807
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 808
    return-void
.end method

.method private registerObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3702
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "registerObserver E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3703
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_captioning_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCaptioningObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3704
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_sec_captioning_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCaptioningObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3705
    return-void
.end method

.method private runSmartPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3637
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSmartPauseOn(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSmartPause:Z

    .line 3638
    const-string v0, "MiniVideoPlayerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSmartPause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSmartPause:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3640
    sget-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSmartPause:Z

    if-eqz v0, :cond_1

    .line 3641
    sget-object v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-nez v0, :cond_0

    .line 3642
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceManager;->getSmartFaceManager()Lcom/samsung/android/smartface/SmartFaceManager;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 3645
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_2

    .line 3646
    sput v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevNeedtoPause:I

    .line 3647
    sget-object v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$18;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceManager;->setListener(Lcom/samsung/android/smartface/SmartFaceManager$SmartFaceInfoListener;)V

    .line 3680
    sget-object v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceManager;->start(I)Z

    .line 3687
    :cond_1
    :goto_0
    return-void

    .line 3683
    :cond_2
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "fail to get SmartFaceManager. Set mSmartPause as false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3684
    sput-boolean v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSmartPause:Z

    goto :goto_0
.end method

.method private saveRecentlyPlayedTime(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v0, :cond_0

    .line 2054
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2055
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->saveRecentlyPlayedTime(Landroid/net/Uri;)V

    .line 2065
    :cond_0
    :goto_0
    return-void

    .line 2056
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2058
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 2059
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isHubContent(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2060
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->saveRecentlyPlayedTime(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private saveResumePos()V
    .locals 4

    .prologue
    .line 3106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSavedcurrentPosition:I

    .line 3112
    :cond_0
    :goto_0
    return-void

    .line 3110
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    goto :goto_0
.end method

.method private sendSideSyncLastPlayedItemBroadcast(J)V
    .locals 5
    .param p1, "curPos"    # J

    .prologue
    .line 3493
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isNeedSaveResumePos()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3494
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "sendSideSyncLastPlayedItemBroadcast. no need to Save Resume Position"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3509
    :goto_0
    return-void

    .line 3498
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sidesync.common.MEDIA_SHARE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3500
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CONTENT"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3501
    const-string v1, "resumePos"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3502
    const-string v1, "UPDATEONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3505
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 3507
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendSideSyncLastPlayedItemBroadcast. mVideoUri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3508
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendSideSyncLastPlayedItemBroadcast. curPos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setProgressBarVisibility(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 3754
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->IsPauseEnable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3755
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v1

    .line 3756
    .local v1, "remainTime":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v0

    .line 3758
    .local v0, "durationTime":I
    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 3759
    mul-int/lit8 v2, v1, 0x64

    div-int/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->remainPercent:I

    .line 3760
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->remainPercent:I

    if-gtz v2, :cond_0

    .line 3761
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->remainPercent:I

    .line 3765
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    .line 3766
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->remainPercent:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 3767
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 3770
    .end local v0    # "durationTime":I
    .end local v1    # "remainTime":I
    :cond_1
    return-void
.end method

.method private setSeekPosition()V
    .locals 4

    .prologue
    .line 3128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v0, :cond_0

    .line 3129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3130
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSavedcurrentPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 3134
    :cond_0
    :goto_0
    return-void

    .line 3132
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    goto :goto_0
.end method

.method private setSubtitleSyncTime()V
    .locals 4

    .prologue
    .line 3115
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "setSubtitleSyncTime"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3117
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3118
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleSyncTime()I

    move-result v0

    .line 3119
    .local v0, "syncTime":I
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitleSyncTime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3121
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v1, :cond_0

    .line 3122
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSubtitleSyncTime(I)V

    .line 3125
    .end local v0    # "syncTime":I
    :cond_0
    return-void
.end method

.method private setVisibilitySubtitleView(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 2747
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v0, :cond_0

    .line 2748
    if-eqz p1, :cond_1

    .line 2749
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    if-eqz v0, :cond_0

    .line 2750
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    .line 2755
    :cond_0
    :goto_0
    return-void

    .line 2752
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupSideSyncStream(Landroid/content/Intent;)Z
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 3469
    const-string v5, "isFromSideSync"

    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 3470
    .local v0, "isFromSideSync":Z
    if-eqz v0, :cond_1

    .line 3471
    const-string v5, "title_of_sidesync"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3472
    .local v2, "titleName":Ljava/lang/String;
    const-string v5, "needSaveResumePos"

    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 3474
    .local v1, "needSaveResumePos":Z
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setNeedSaveResumePos(Z)V

    .line 3475
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setTitleofSideSync(Ljava/lang/String;)V

    .line 3477
    const-string v4, "MiniVideoPlayerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setupSideSyncStream() - needSaveResumePos = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3478
    const-string v4, "MiniVideoPlayerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setupSideSyncStream() - titleName = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3480
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSdp()Z

    move-result v4

    if-nez v4, :cond_0

    .line 3481
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 3484
    :cond_0
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setFromSideSync(Z)V

    .line 3489
    .end local v1    # "needSaveResumePos":Z
    .end local v2    # "titleName":Ljava/lang/String;
    :goto_0
    return v3

    .line 3488
    :cond_1
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setFromSideSync(Z)V

    move v3, v4

    .line 3489
    goto :goto_0
.end method

.method private showLoadingDialog(I)V
    .locals 7
    .param p1, "adjustPosition"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2572
    if-nez p1, :cond_0

    .line 2574
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2575
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(II)[I

    move-result-object v0

    .line 2577
    .local v0, "i":[I
    aget v2, v0, v5

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2578
    const/4 v2, 0x1

    aget v2, v0, v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2580
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2583
    .end local v0    # "i":[I
    .end local v1    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 2584
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleAudioOnlyView(I)V

    .line 2586
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 2587
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z

    .line 2588
    return-void
.end method

.method private startVideoList()V
    .locals 2

    .prologue
    .line 3464
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->launchVideoList(Landroid/content/Context;)V

    .line 3465
    return-void
.end method

.method private stringForTimeCur(I)Ljava/lang/String;
    .locals 11
    .param p1, "timeMs"    # I

    .prologue
    const/4 v10, 0x0

    .line 3548
    div-int/lit16 v6, p1, 0x3e8

    .line 3549
    .local v6, "totalSeconds":I
    rem-int/lit8 v5, v6, 0x3c

    .line 3550
    .local v5, "seconds":I
    div-int/lit8 v7, v6, 0x3c

    rem-int/lit8 v4, v7, 0x3c

    .line 3551
    .local v4, "minutes":I
    div-int/lit16 v1, v6, 0xe10

    .line 3553
    .local v1, "hours":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3554
    .local v2, "mFormatBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3556
    new-instance v3, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v3, v2, v7}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 3557
    .local v3, "mFormatter":Ljava/util/Formatter;
    const-string v7, "%02d:%02d:%02d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    const/4 v9, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v7, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3558
    .local v0, "CurTime":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 3560
    return-object v0
.end method

.method private stringForTimeEnd(I)Ljava/lang/String;
    .locals 11
    .param p1, "timeMs"    # I

    .prologue
    const/4 v10, 0x0

    .line 3564
    div-int/lit16 v6, p1, 0x3e8

    .line 3565
    .local v6, "totalSeconds":I
    rem-int/lit8 v5, v6, 0x3c

    .line 3566
    .local v5, "seconds":I
    div-int/lit8 v7, v6, 0x3c

    rem-int/lit8 v4, v7, 0x3c

    .line 3567
    .local v4, "minutes":I
    div-int/lit16 v1, v6, 0xe10

    .line 3569
    .local v1, "hours":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3570
    .local v2, "mFormatBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3572
    new-instance v3, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v3, v2, v7}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 3573
    .local v3, "mFormatter":Ljava/util/Formatter;
    const-string v7, " %02d:%02d:%02d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    const/4 v9, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v7, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3574
    .local v0, "EndTime":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 3576
    return-object v0
.end method

.method private unregister()V
    .locals 2

    .prologue
    .line 821
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "unregister()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 825
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->unregisterRemoteControlReceiver()V

    .line 826
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->unRegisterObserver()V

    .line 828
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 831
    :cond_0
    return-void
.end method

.method private updateLastPlayedPath()V
    .locals 3

    .prologue
    .line 2511
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 2512
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2513
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "lastPlayedItem"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 2514
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->broadcastLastFileName(Ljava/lang/String;)V

    .line 2516
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "lastPlayedItemOfAIA"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 2517
    return-void
.end method

.method private updateSecureMode(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1994
    const/4 v0, 0x0

    .line 1995
    .local v0, "isSecure":Z
    const/4 v1, 0x0

    .line 1997
    .local v1, "mfilePath":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1998
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 2003
    :goto_0
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateSecureMode. mfilePath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2005
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 2006
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "updateSecureMode. drm file!"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2007
    const/4 v0, 0x1

    .line 2010
    :cond_0
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateSecureMode. mSecureMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSecureMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isSecure :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSecureMode:Z

    if-eq v2, v0, :cond_1

    .line 2013
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSecureMode:Z

    .line 2015
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "updateSecureMode. mode change so re-set"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v2, :cond_1

    .line 2018
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setVisibility(I)V

    .line 2019
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSecureMode:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSecure(Z)V

    .line 2020
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setVisibility(I)V

    .line 2023
    :cond_1
    return-void

    .line 2000
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private updateSubtitleTextSize(II)V
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2730
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v4, :cond_2

    .line 2731
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 2732
    .local v0, "maxTextSize":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 2734
    .local v1, "minTextSize":I
    int-to-float v4, p1

    iget v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 2736
    .local v2, "ratio":F
    const/high16 v4, 0x41900000    # 18.0f

    mul-float/2addr v4, v2

    float-to-int v3, v4

    .line 2738
    .local v3, "textSize":I
    if-ge v3, v1, :cond_0

    move v3, v1

    .line 2739
    :cond_0
    if-le v3, v0, :cond_1

    move v3, v0

    .line 2741
    :cond_1
    const-string v4, "MiniVideoPlayerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateSubtitleTextSize() textSize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2742
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubtitleSize(I)V

    .line 2744
    .end local v0    # "maxTextSize":I
    .end local v1    # "minTextSize":I
    .end local v2    # "ratio":F
    .end local v3    # "textSize":I
    :cond_2
    return-void
.end method

.method private updateVideoWidget()V
    .locals 5

    .prologue
    .line 2491
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateLastPlayedPath()V

    .line 2492
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2493
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2494
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "videowidget.update"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2495
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2497
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ENABLE_MAGAZINE_HOME:Z

    if-eqz v3, :cond_0

    .line 2498
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2499
    .local v1, "intent2":Landroid/content/Intent;
    const-string v3, "videowidget.update"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2500
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2502
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2503
    .local v2, "intent3":Landroid/content/Intent;
    const-string v3, "com.samsung.everglades.video"

    const-string v4, "com.samsung.everglades.video.myvideo.list.FavoriteIconWidget"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2504
    const-string v3, "videowidget.update"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2505
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2508
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "intent2":Landroid/content/Intent;
    .end local v2    # "intent3":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public broadcastLastFileName(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2520
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.everglades.video.myvideo.LAST_FILE_NAME"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2521
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "filepath"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2523
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2527
    :goto_0
    return-void

    .line 2524
    :catch_0
    move-exception v0

    .line 2525
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ActivityNotFoundException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public broadcastPausedPosition(Z)V
    .locals 12
    .param p1, "exitPlayerByAppinApp"    # Z

    .prologue
    const-wide/16 v10, 0x1388

    .line 3608
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3609
    const-string v6, "MiniVideoPlayerService"

    const-string v7, "broadcastPausedPosition() launch from videoHub"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3610
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v6

    int-to-long v0, v6

    .line 3611
    .local v0, "curPos":J
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v6

    int-to-long v2, v6

    .line 3612
    .local v2, "duration":J
    const-string v6, "MiniVideoPlayerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastPausedPosition() - curPos : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", duration : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3614
    cmp-long v6, v0, v10

    if-gez v6, :cond_1

    .line 3615
    const-wide/16 v0, 0x0

    .line 3622
    :goto_0
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sdgtl.stamhubb.PAUSED_POSITION"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3623
    .local v5, "intent":Landroid/content/Intent;
    const-string v6, "paused_position"

    invoke-virtual {v5, v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3624
    const-string v6, "filepath"

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3625
    const-string v6, "exitPlayerByAppinApp"

    invoke-virtual {v5, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3626
    const-string v6, "MiniVideoPlayerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastPausedPosition() - postion : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exitPlayerByAppinApp : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3629
    :try_start_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3634
    .end local v0    # "curPos":J
    .end local v2    # "duration":J
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-void

    .line 3616
    .restart local v0    # "curPos":J
    .restart local v2    # "duration":J
    :cond_1
    sub-long v6, v2, v0

    cmp-long v6, v6, v10

    if-gtz v6, :cond_2

    .line 3617
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 3619
    :cond_2
    sub-long/2addr v0, v10

    goto :goto_0

    .line 3630
    .restart local v5    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v4

    .line 3631
    .local v4, "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "MiniVideoPlayerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ActivityNotFoundException occured :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public changeLoadingDialogText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 3265
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 3266
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 3267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0084

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingText:Landroid/widget/TextView;

    .line 3269
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3271
    :cond_1
    return-void
.end method

.method public checkAndUpdateCtrlLayout(I)V
    .locals 3
    .param p1, "winWidth"    # I

    .prologue
    .line 3005
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    if-nez v0, :cond_0

    .line 3012
    :goto_0
    return-void

    .line 3007
    :cond_0
    int-to-float v0, p1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080175

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 3008
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->updateLayout(Z)V

    goto :goto_0

    .line 3010
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->updateLayout(Z)V

    goto :goto_0
.end method

.method public createAudioOnlyClipWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3199
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "createAudioOnlyClipWindow()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3200
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z

    if-eqz v2, :cond_0

    .line 3201
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "createAudioOnlyClipWindow() Already Created!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3218
    :goto_0
    return-void

    .line 3205
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3206
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(II)[I

    move-result-object v0

    .line 3207
    .local v0, "i":[I
    aget v2, v0, v4

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 3208
    aget v2, v0, v5

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3209
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3211
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkAndUpdateCtrlLayout(I)V

    .line 3212
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 3215
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleAudioOnlyView(I)V

    .line 3216
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestLayout()V

    .line 3217
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z

    goto :goto_0
.end method

.method public getBtnController()Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    .locals 1

    .prologue
    .line 3235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    return-object v0
.end method

.method public getCurPlayingPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3239
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;
    .locals 12
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 1385
    const-string v0, " "

    .line 1386
    .local v0, "displayName":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    .line 1388
    .local v9, "uri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFullBrowsableMode()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1389
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    .line 1478
    :goto_0
    return-object v10

    .line 1392
    :cond_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ge v10, v11, :cond_2

    .line 1393
    :cond_1
    if-eqz v9, :cond_2

    .line 1394
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 1397
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v10, :cond_3

    .line 1398
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 1401
    :cond_3
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v10, :cond_4

    .line 1402
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 1406
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    if-eqz v10, :cond_5

    .line 1407
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaylist:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getPositionFromPlaylist(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "uri":Landroid/net/Uri;
    check-cast v9, Landroid/net/Uri;

    .line 1410
    .restart local v9    # "uri":Landroid/net/Uri;
    :cond_5
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_4

    :cond_6
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1412
    :cond_7
    if-eqz v9, :cond_15

    .line 1413
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isBrowser(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1414
    :cond_8
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1415
    .local v2, "filePath":Ljava/lang/String;
    const/16 v10, 0x2f

    invoke-virtual {v2, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1416
    .local v3, "lastIndex1":I
    const/16 v10, 0x2e

    invoke-virtual {v2, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 1418
    .local v4, "lastIndex2":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v3, v10, :cond_a

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v4, v10, :cond_a

    .line 1420
    add-int/lit8 v10, v3, 0x1

    :try_start_0
    invoke-virtual {v2, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1471
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "lastIndex1":I
    .end local v4    # "lastIndex2":I
    :cond_9
    :goto_1
    const-string v10, " "

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 1472
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "content://mms"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_15

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "content://gmail-ls"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_15

    .line 1474
    invoke-virtual {v9}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 1421
    .restart local v2    # "filePath":Ljava/lang/String;
    .restart local v3    # "lastIndex1":I
    .restart local v4    # "lastIndex2":I
    :catch_0
    move-exception v1

    .line 1422
    .local v1, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string v0, " "

    .line 1423
    goto :goto_1

    .line 1425
    .end local v1    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_a
    const-string v0, " "

    goto :goto_1

    .line 1426
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "lastIndex1":I
    .end local v4    # "lastIndex2":I
    :cond_b
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v10

    if-eqz v10, :cond_c

    .line 1427
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1428
    new-instance v8, Ljava/io/File;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1430
    .local v8, "temp":Ljava/io/File;
    if-eqz v8, :cond_9

    .line 1431
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1432
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 1436
    .end local v8    # "temp":Ljava/io/File;
    :cond_c
    const/4 v5, 0x0

    .line 1438
    .local v5, "newUri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v10

    if-nez v10, :cond_d

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v10

    if-eqz v10, :cond_10

    .line 1439
    :cond_d
    move-object v5, v9

    .line 1451
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1453
    if-nez v0, :cond_9

    .line 1454
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v10, :cond_e

    .line 1455
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 1458
    :cond_e
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v10, :cond_f

    .line 1459
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 1462
    :cond_f
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v10, :cond_13

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v10, v5}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 1463
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v11, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1440
    :cond_10
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 1441
    const-wide/16 v6, -0x1

    .line 1442
    .local v6, "mVideoDbId":J
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v6

    .line 1444
    const-wide/16 v10, -0x1

    cmp-long v10, v6, v10

    if-nez v10, :cond_11

    .line 1445
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v6

    .line 1447
    :cond_11
    sget-object v10, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v10, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 1448
    goto :goto_2

    .line 1449
    .end local v6    # "mVideoDbId":J
    :cond_12
    move-object v5, v9

    goto :goto_2

    .line 1464
    :cond_13
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v10, :cond_14

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v10, :cond_14

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v10, v5}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 1465
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v11, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1467
    :cond_14
    const-string v0, " "

    goto/16 :goto_1

    .end local v5    # "newUri":Landroid/net/Uri;
    :cond_15
    move-object v10, v0

    .line 1478
    goto/16 :goto_0
.end method

.method public getGestureDetector()Landroid/view/GestureDetector;
    .locals 4

    .prologue
    .line 2837
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v0, :cond_0

    .line 2838
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2840
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method public getScaleDetector()Landroid/view/ScaleGestureDetector;
    .locals 4

    .prologue
    .line 2830
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    if-nez v0, :cond_0

    .line 2831
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 2833
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method public getSchemeType()Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1

    .prologue
    .line 3231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method public hide()V
    .locals 5

    .prologue
    .line 3170
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v1, :cond_0

    .line 3171
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->saveResumePos()V

    .line 3172
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsPlayingB4Hide:Z

    .line 3174
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    .line 3175
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hide : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsPlayingB4Hide:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3176
    invoke-super {p0}, Lcom/sec/android/app/minimode/MiniModeService;->hide()V

    .line 3179
    :try_start_0
    const-string v1, "com.sec.android.app.minimode.MiniModeService"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "isFirstShow"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 3180
    .local v0, "isFirstShowMethod":Ljava/lang/reflect/Method;
    if-eqz v0, :cond_1

    .line 3181
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    .line 3183
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 3184
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "setProcessForeground : false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0

    .line 3196
    .end local v0    # "isFirstShowMethod":Ljava/lang/reflect/Method;
    :cond_1
    :goto_0
    return-void

    .line 3194
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3193
    :catch_1
    move-exception v1

    goto :goto_0

    .line 3192
    :catch_2
    move-exception v1

    goto :goto_0

    .line 3191
    :catch_3
    move-exception v1

    goto :goto_0

    .line 3190
    :catch_4
    move-exception v1

    goto :goto_0

    .line 3185
    .restart local v0    # "isFirstShowMethod":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v1

    goto :goto_0
.end method

.method public initMiniPlayerView(Landroid/content/Intent;)V
    .locals 20
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 421
    const-string v16, "startFromList"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isStartFromList:Z

    .line 423
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isStartFromList:Z

    move/from16 v16, v0

    if-eqz v16, :cond_5

    .line 424
    const-string v16, "filePath"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 425
    .local v6, "filePath":Ljava/lang/String;
    const-string v16, "currentID"

    const-wide/16 v18, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 427
    .local v4, "VideoID":J
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 428
    const-string v16, "MiniVideoPlayerService"

    const-string v17, "initMiniPlayerView : FILE PATH IS SAME AS CURRENT!!"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    .end local v4    # "VideoID":J
    .end local v6    # "filePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 430
    .restart local v4    # "VideoID":J
    .restart local v6    # "filePath":Ljava/lang/String;
    :cond_1
    const-wide/16 v16, -0x1

    cmp-long v16, v4, v16

    if-eqz v16, :cond_2

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    move-wide/from16 v16, v0

    cmp-long v16, v4, v16

    if-nez v16, :cond_2

    .line 431
    const-string v16, "MiniVideoPlayerService"

    const-string v17, "initMiniPlayerView : VideoID IS SAME AS CURRENT!!"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 435
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-virtual/range {v16 .. v19}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 437
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    move/from16 v16, v0

    if-lez v16, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    move/from16 v16, v0

    if-gtz v16, :cond_4

    .line 438
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    .line 440
    .local v15, "vutils":Lcom/sec/android/app/videoplayer/common/VUtils;
    if-eqz v15, :cond_4

    .line 441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v13

    .line 442
    .local v13, "screen":[I
    const/16 v16, 0x0

    aget v16, v13, v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    .line 443
    const/16 v16, 0x1

    aget v16, v13, v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    .line 469
    .end local v4    # "VideoID":J
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v13    # "screen":[I
    .end local v15    # "vutils":Lcom/sec/android/app/videoplayer/common/VUtils;
    :cond_4
    :goto_1
    const/16 v16, 0x0

    sput-boolean v16, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLarge:Z

    .line 471
    const-string v16, "filePath"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    .line 472
    const-string v16, "currentID"

    const-wide/16 v18, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    .line 474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v16, v0

    const-string v17, "resumePos"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    const-string v17, "ListKey"

    const/16 v18, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    const-string v17, "WhereFrom"

    const/16 v18, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setFrom(I)V

    .line 477
    const-string v16, "ListType"

    const/16 v17, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    .line 478
    const-string v16, "playSpeed"

    const/16 v17, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I

    .line 479
    const-string v16, "audioTrack"

    const/16 v17, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I

    .line 480
    const-string v16, "uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 483
    .local v10, "miniModeUri":Ljava/lang/String;
    if-nez v10, :cond_6

    .line 484
    const-string v16, "MiniVideoPlayerService"

    const-string v17, "initMiniPlayerView : MINIMODE_URI IS NULL!!"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 448
    .end local v10    # "miniModeUri":Ljava/lang/String;
    :cond_5
    const-string v16, "screenWidth"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    .line 449
    const-string v16, "screenHeight"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    .line 450
    const-string v16, "videoWidth"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    .line 451
    const-string v16, "videoHeight"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    .line 453
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(II)[I

    move-result-object v8

    .line 454
    .local v8, "i":[I
    const-string v16, "window.pos.x"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 455
    .local v11, "posX":I
    const-string v16, "window.pos.y"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 456
    .local v12, "posY":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setXY(II)V

    .line 457
    const/16 v16, 0x0

    aget v16, v8, v16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkAndUpdateCtrlLayout(I)V

    .line 458
    const/16 v16, -0x270f

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    .line 460
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v9

    .line 461
    .local v9, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v11, v9, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 462
    iput v12, v9, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 463
    const/16 v16, 0x0

    aget v16, v8, v16

    move/from16 v0, v16

    iput v0, v9, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 464
    const/16 v16, 0x1

    aget v16, v8, v16

    move/from16 v0, v16

    iput v0, v9, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 465
    iget v0, v9, Landroid/view/WindowManager$LayoutParams;->flags:I

    move/from16 v16, v0

    or-int/lit8 v16, v16, 0x8

    move/from16 v0, v16

    iput v0, v9, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 466
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_1

    .line 488
    .end local v8    # "i":[I
    .end local v9    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v11    # "posX":I
    .end local v12    # "posY":I
    .restart local v10    # "miniModeUri":Ljava/lang/String;
    :cond_6
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    .line 489
    const-string v16, "bucketid"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    .line 491
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBucketID:I

    move/from16 v16, v0

    if-eqz v16, :cond_7

    .line 492
    const/16 v16, 0x9

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    .line 495
    :cond_7
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView. mListType : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const-string v16, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->displayName:Ljava/lang/String;

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleSettings()V

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWebvttRendererVisibility(Z)V

    .line 503
    const-string v16, "calledScalewindow"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsCalledScalewindow:Z

    .line 504
    const-string v16, "ScalewindowRect"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Landroid/graphics/Rect;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScalewindowRect:Landroid/graphics/Rect;

    .line 506
    const-string v16, "subtitleActivation"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    .line 507
    .local v14, "subtitleActivation":Z
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView. subtitleActivation : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isStartFromList:Z

    move/from16 v16, v0

    if-eqz v16, :cond_13

    .line 510
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initSubtitle()V

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleSyncTime(I)V

    .line 513
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isSubtitleActivationOn()Z

    move-result v16

    if-eqz v16, :cond_8

    .line 514
    const/4 v14, 0x1

    .line 517
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForceResetSubtilteView:Z

    move/from16 v16, v0

    if-eqz v16, :cond_a

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_9

    .line 519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubtitleSettings()V

    .line 522
    :cond_9
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForceResetSubtilteView:Z

    .line 536
    :cond_a
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromDms()Z

    move-result v16

    if-eqz v16, :cond_b

    .line 540
    const-string v16, "title_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDmsTitle:Ljava/lang/String;

    .line 543
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v16

    if-eqz v16, :cond_c

    .line 544
    const-string v16, "CurrentProviderName"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCurrentProviderName:Ljava/lang/String;

    .line 548
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v16, v0

    if-eqz v16, :cond_d

    .line 549
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 551
    :cond_d
    new-instance v16, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v16

    if-eqz v16, :cond_e

    .line 554
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setupSideSyncStream(Landroid/content/Intent;)Z

    .line 559
    :cond_e
    const-string v16, "yosemite_enable"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bWatchON:Z

    .line 560
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView. bWatchON : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bWatchON:Z

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string v16, "isfromMovieStore"

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsfromMovieStore:Z

    .line 564
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    move/from16 v16, v0

    const/16 v17, 0xe

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_f

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    const/16 v17, 0x1e

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 568
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v16

    if-eqz v16, :cond_10

    .line 569
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->set(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v7

    .line 570
    .local v7, "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getData(Ljava/lang/String;Z)V

    .line 574
    .end local v7    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :cond_10
    sget-boolean v16, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v16, :cond_11

    .line 575
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView() - mLastFile:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView() - mVideoID:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoID:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView() - mListKey:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView() - mListType:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mListType:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    const-string v16, "MiniVideoPlayerService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "initMiniPlayerView() - mVideoUri:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isStartFromList:Z

    move/from16 v16, v0

    if-eqz v16, :cond_12

    .line 583
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateVideoWidget()V

    .line 585
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->callMiniPlayer(Landroid/net/Uri;)V

    .line 586
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    .line 587
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->showLoadingDialog(I)V

    .line 589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v16

    if-eqz v16, :cond_0

    .line 590
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/slink/SLink;->acquireWakeLockPopupPlayer(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 525
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const-string v17, "subtitleFile"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const-string v17, "subtitleLanguageIdx"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const-string v17, "subtitleIsMulti"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setMultiEnable(Z)V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const-string v17, "subtitleMultiCount"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setMultiSelectSubtitleCount(I)V

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const-string v17, "subtitleMultiIndex"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setMultiSelectedSubtitleIndex([I)V

    .line 530
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-object/from16 v16, v0

    const-string v17, "subttileSyncTime"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleSyncTime(I)V

    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubtitleSettings()V

    goto/16 :goto_2
.end method

.method public initScreenScaling()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2758
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bScreenScaling:Z

    .line 2761
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2762
    .local v0, "defaultValue":Landroid/util/TypedValue;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003b

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 2764
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isRequiredSmoothing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2765
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z

    if-nez v1, :cond_1

    .line 2766
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    .line 2774
    :goto_0
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initScreenScaling() - mScaleFactor = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2776
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilitySubtitleView(Z)V

    .line 2778
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initScaleMultiplier()V

    .line 2780
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v1, :cond_0

    .line 2781
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setBackgroundVisible(Ljava/lang/Boolean;)V

    .line 2790
    :cond_0
    return-void

    .line 2768
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->reCalculateScaleFactor(Landroid/util/TypedValue;)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    goto :goto_0

    .line 2771
    :cond_2
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F

    goto :goto_0
.end method

.method public is1080pEquivalent()Z
    .locals 5

    .prologue
    const/16 v4, 0x780

    const/16 v3, 0x438

    .line 2820
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVideoWidth()I

    move-result v1

    .line 2821
    .local v1, "videoWidth":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVideoHeight()I

    move-result v0

    .line 2823
    .local v0, "videoHeight":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v2

    if-eqz v2, :cond_0

    if-ge v1, v4, :cond_1

    if-ge v0, v3, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v2

    if-nez v2, :cond_2

    if-ge v0, v4, :cond_1

    if-lt v1, v3, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isBackgroundViewVisible()Z
    .locals 1

    .prologue
    .line 2591
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHEVCPDType()Z
    .locals 2

    .prologue
    .line 3588
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isBrowser(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3591
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "HEVC PD Type!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3592
    const/4 v0, 0x1

    .line 3594
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHEVCStreamingType()Z
    .locals 2

    .prologue
    .line 3598
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isBrowser(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3600
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "HEVC Streaming Type!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3601
    const/4 v0, 0x1

    .line 3603
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOutsideOfScreenLimit(Landroid/view/MotionEvent;)Z
    .locals 28
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 3019
    const/4 v13, 0x0

    .line 3020
    .local v13, "retVal":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getWidth()I

    move-result v18

    .line 3021
    .local v18, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getHeight()I

    move-result v9

    .line 3023
    .local v9, "height":I
    const/16 v16, 0x0

    .local v16, "screenWidth":I
    const/4 v15, 0x0

    .line 3025
    .local v15, "screenHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 3026
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    move/from16 v16, v0

    .line 3027
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    .line 3033
    :goto_0
    const-wide/16 v22, 0x0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide v26, 0x3fd999999999999aL    # 0.4

    mul-double v24, v24, v26

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v10, v0

    .line 3034
    .local v10, "leftLimit":I
    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v22, v0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide v26, 0x3fe3333333333333L    # 0.6

    mul-double v24, v24, v26

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v14, v0

    .line 3035
    .local v14, "rightLimit":I
    const-wide/16 v22, 0x0

    int-to-double v0, v9

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    mul-double v24, v24, v26

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v17, v0

    .line 3036
    .local v17, "topLimit":I
    int-to-double v0, v15

    move-wide/from16 v22, v0

    int-to-double v0, v9

    move-wide/from16 v24, v0

    const-wide v26, 0x3fe3333333333333L    # 0.6

    mul-double v24, v24, v26

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v8, v0

    .line 3038
    .local v8, "bottomLimit":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v21

    move-object/from16 v0, v21

    iget v11, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 3039
    .local v11, "nextX":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v21

    move-object/from16 v0, v21

    iget v12, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 3041
    .local v12, "nextY":I
    if-gt v11, v10, :cond_c

    const/4 v5, 0x1

    .line 3042
    .local v5, "blockLeft":Z
    :goto_1
    if-lt v11, v14, :cond_d

    const/4 v6, 0x1

    .line 3043
    .local v6, "blockRight":Z
    :goto_2
    move/from16 v0, v17

    if-gt v12, v0, :cond_e

    const/4 v7, 0x1

    .line 3044
    .local v7, "blockTop":Z
    :goto_3
    if-lt v12, v8, :cond_f

    const/4 v4, 0x1

    .line 3046
    .local v4, "blockBottom":Z
    :goto_4
    if-nez v5, :cond_0

    if-nez v6, :cond_0

    if-nez v7, :cond_0

    if-eqz v4, :cond_a

    .line 3047
    :cond_0
    const/4 v13, 0x1

    .line 3049
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v21

    packed-switch v21, :pswitch_data_0

    .line 3064
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDownNextX:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ne v11, v0, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDownNextY:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ne v12, v0, :cond_1

    .line 3065
    const/4 v13, 0x0

    .line 3067
    :cond_1
    move/from16 v19, v11

    .line 3068
    .local v19, "x":I
    move/from16 v20, v12

    .line 3070
    .local v20, "y":I
    if-eqz v5, :cond_3

    .line 3071
    if-le v11, v10, :cond_2

    .line 3072
    const/4 v13, 0x0

    .line 3074
    :cond_2
    move/from16 v19, v10

    .line 3077
    :cond_3
    if-eqz v6, :cond_5

    .line 3078
    if-ge v11, v14, :cond_4

    .line 3079
    const/4 v13, 0x0

    .line 3081
    :cond_4
    move/from16 v19, v14

    .line 3084
    :cond_5
    if-eqz v7, :cond_7

    .line 3085
    move/from16 v0, v17

    if-le v12, v0, :cond_6

    .line 3086
    const/4 v13, 0x0

    .line 3088
    :cond_6
    move/from16 v20, v17

    .line 3091
    :cond_7
    if-eqz v4, :cond_9

    .line 3092
    if-ge v12, v8, :cond_8

    .line 3093
    const/4 v13, 0x0

    .line 3095
    :cond_8
    move/from16 v20, v8

    .line 3098
    :cond_9
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setXY(II)V

    .line 3099
    if-eqz v13, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->move(II)V

    .line 3102
    .end local v19    # "x":I
    .end local v20    # "y":I
    :cond_a
    return v13

    .line 3029
    .end local v4    # "blockBottom":Z
    .end local v5    # "blockLeft":Z
    .end local v6    # "blockRight":Z
    .end local v7    # "blockTop":Z
    .end local v8    # "bottomLimit":I
    .end local v10    # "leftLimit":I
    .end local v11    # "nextX":I
    .end local v12    # "nextY":I
    .end local v14    # "rightLimit":I
    .end local v17    # "topLimit":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    move/from16 v16, v0

    .line 3030
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    goto/16 :goto_0

    .line 3041
    .restart local v8    # "bottomLimit":I
    .restart local v10    # "leftLimit":I
    .restart local v11    # "nextX":I
    .restart local v12    # "nextY":I
    .restart local v14    # "rightLimit":I
    .restart local v17    # "topLimit":I
    :cond_c
    const/4 v5, 0x0

    goto :goto_1

    .line 3042
    .restart local v5    # "blockLeft":Z
    :cond_d
    const/4 v6, 0x0

    goto :goto_2

    .line 3043
    .restart local v6    # "blockRight":Z
    :cond_e
    const/4 v7, 0x0

    goto :goto_3

    .line 3044
    .restart local v7    # "blockTop":Z
    :cond_f
    const/4 v4, 0x0

    goto :goto_4

    .line 3051
    .restart local v4    # "blockBottom":Z
    :pswitch_0
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDownNextX:I

    .line 3052
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDownNextY:I

    .line 3053
    const/4 v13, 0x0

    .line 3054
    goto :goto_5

    .line 3057
    :pswitch_1
    const/4 v13, 0x0

    .line 3058
    goto :goto_5

    .line 3049
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isScreenScaling()Z
    .locals 1

    .prologue
    .line 3015
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bScreenScaling:Z

    return v0
.end method

.method public makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "zoneInfo"    # I
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 2264
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 2266
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 2276
    :goto_0
    return-object v1

    .line 2267
    :catch_0
    move-exception v0

    .line 2268
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    .line 2275
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :cond_0
    :goto_1
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "makeMultiWindowIntent returns null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2276
    const/4 v1, 0x0

    goto :goto_0

    .line 2269
    :catch_1
    move-exception v0

    .line 2270
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 2271
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 2272
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected onClose(I)Z
    .locals 4
    .param p1, "reason"    # I

    .prologue
    const/4 v0, 0x1

    .line 639
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClose - reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    packed-switch p1, :pswitch_data_0

    .line 653
    :pswitch_0
    const/4 v0, 0x0

    :pswitch_1
    return v0

    .line 641
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x2

    .line 377
    invoke-super {p0, p1}, Lcom/sec/android/app/minimode/MiniModeService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 378
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 379
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I

    .line 381
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 382
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->adjustXYposition(II)V

    .line 388
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v1, :cond_1

    .line 389
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldFolderState:I

    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v1, v2, :cond_1

    .line 390
    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldFolderState:I

    .line 392
    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sub_lcd_auto_lock"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    .line 400
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 401
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateSubtitleTextSize(II)V

    .line 402
    return-void

    .line 383
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_0

    .line 384
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->adjustXYposition(II)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v13, 0x0

    .line 252
    invoke-super {p0}, Lcom/sec/android/app/minimode/MiniModeService;->onCreate()V

    .line 253
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "minivideo onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v11

    .line 256
    .local v11, "lp":Landroid/view/WindowManager$LayoutParams;
    const v0, 0x1030002

    iput v0, v11, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 257
    invoke-virtual {p0, v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 259
    invoke-virtual {p0, v13}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->enableDim(Z)V

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    .line 263
    .local v9, "appInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    iget v2, v9, Landroid/content/pm/ApplicationInfo;->theme:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    .line 265
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    .line 266
    .local v10, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f03000c

    invoke-virtual {v10, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d007c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const/4 v1, -0x1

    const v2, 0x7f0d007d

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setContentView(Landroid/view/View;II)V

    .line 270
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v0, :cond_0

    .line 271
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setBackgroundVisible(Ljava/lang/Boolean;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 278
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d0082

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d0080

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnly:Landroid/widget/RelativeLayout;

    .line 286
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d0085

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitButton:Landroid/widget/ImageButton;

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0149

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v1, 0x7f0d0088

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnPauseListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setOnStartListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 318
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setVUtilsData(Landroid/content/Context;)V

    .line 319
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/MiniVideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/MiniVideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 321
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->register()V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->registerObserver()V

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getMainView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMainView:Landroid/view/ViewGroup;

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5, v13, v13, v13}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 327
    iput-boolean v13, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForceResetSubtilteView:Z

    .line 328
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v12

    .line 329
    .local v12, "subUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isCheckNullSubtitleManager()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setContext(Landroid/content/Context;)V

    .line 331
    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleSetting()V

    .line 332
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForceResetSubtilteView:Z

    .line 335
    :cond_3
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    .line 336
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleView(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSubtitleUtil(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;)V

    .line 342
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v0, :cond_4

    .line 343
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mRootLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;Lcom/sec/android/app/videoplayer/type/SchemeType;Landroid/widget/ProgressBar;Landroid/widget/RelativeLayout;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setOnCommandListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;)V

    .line 347
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v0, :cond_5

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 350
    :cond_5
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_6

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 354
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 356
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setContext(Landroid/content/Context;)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldFolderState:I

    .line 362
    iput-boolean v13, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z

    .line 363
    iput-boolean v13, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z

    .line 365
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    .line 366
    invoke-virtual {p0, v13}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->enableDragRestriction(Z)V

    .line 368
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE_POPUP_PLAYER:Z

    if-eqz v0, :cond_7

    .line 369
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->runSmartPause()V

    .line 370
    :cond_7
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 595
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateVideoWidget()V

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    if-eqz v1, :cond_0

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->stopSubtitle(Z)V

    .line 601
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedTrackIndex:[I

    .line 604
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v1, :cond_1

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSoundAliveMode(I)V

    .line 606
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    .line 609
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 610
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->releaseWakeLockPopupPlayer()V

    .line 613
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->reset()V

    .line 615
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->unregister()V

    .line 617
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.finished.app-in-app"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 618
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 620
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z

    .line 621
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z

    .line 623
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE_POPUP_PLAYER:Z

    if-eqz v1, :cond_3

    .line 624
    sget-object v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v1, :cond_3

    .line 625
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSmartPause "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSmartPause:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "mSFManager.stop()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    sget-object v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v1}, Lcom/samsung/android/smartface/SmartFaceManager;->stop()V

    .line 631
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    if-eqz v1, :cond_4

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 635
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/minimode/MiniModeService;->onDestroy()V

    .line 636
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 405
    const/4 v0, 0x0

    .line 407
    .local v0, "retVal":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 408
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/minimode/MiniModeService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    .line 409
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "onStartCommand E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initMiniPlayerView(Landroid/content/Intent;)V

    .line 415
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->resetMode()V

    .line 416
    const-string v1, "video"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setBatteryADC(Ljava/lang/String;Z)V

    .line 417
    return v0

    .line 412
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public sendVHLastPlayedItemBroadcast(Z)V
    .locals 10
    .param p1, "isStart"    # Z

    .prologue
    .line 3517
    const-string v7, "MiniVideoPlayerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendVHLastPlayedItemBroadcast. isStart : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastFile : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3519
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v3

    .line 3521
    .local v3, "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 3522
    .local v6, "uri":Landroid/net/Uri;
    const-string v7, "MiniVideoPlayerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendVHLastPlayedItemBroadcast. isStart : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", uri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", postion : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", duration : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3525
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->stringForTimeCur(I)Ljava/lang/String;

    move-result-object v0

    .line 3526
    .local v0, "currentPos":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->stringForTimeEnd(I)Ljava/lang/String;

    move-result-object v1

    .line 3527
    .local v1, "duration":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const-string v8, "large_poster_url"

    invoke-virtual {v7, v6, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3529
    .local v5, "thumbnailUrl":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v2

    .line 3531
    .local v2, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 3532
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "com.sec.everglades.action.RECENT_DATA_CHANGED"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3533
    const-string v7, "from_where"

    const/16 v8, 0x45e

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3534
    const-string v7, "recent_thumbnail"

    invoke-virtual {v4, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3535
    const-string v7, "recent_title"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3536
    const-string v7, "recent_current_progress"

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3537
    const-string v7, "recent_total_progress"

    invoke-virtual {v4, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3538
    const-string v7, "recent_paused"

    invoke-virtual {v4, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3539
    const-string v7, "launch_player_productid"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getProductId()I

    move-result v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3540
    const-string v7, "launch_player_attributetype"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3542
    if-eqz v4, :cond_0

    .line 3543
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 3545
    :cond_0
    return-void
.end method

.method public setBackgroundVisible(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "visibility"    # Ljava/lang/Boolean;

    .prologue
    .line 3773
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->miniMainView:Landroid/view/View;

    const v2, 0x7f0d007d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 3774
    .local v0, "mMiniVideoSurfaceLayout":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 3775
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3776
    const v1, 0x7f020226

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 3781
    :cond_0
    :goto_0
    return-void

    .line 3778
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setContentView(Landroid/view/View;II)V
    .locals 6
    .param p1, "contentView"    # Landroid/view/View;
    .param p2, "close"    # I
    .param p3, "title"    # I

    .prologue
    const/4 v4, -0x1

    .line 373
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setContentView(Landroid/view/View;IIII)V

    .line 374
    return-void
.end method

.method public setPlaySpeed(I)V
    .locals 3
    .param p1, "speed"    # I

    .prologue
    .line 2563
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    add-int/lit8 v1, p1, 0x5

    int-to-float v1, v1

    const v2, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setPlaySpeed(F)V

    .line 2564
    return-void
.end method

.method public setQuarterPlaySpeed()V
    .locals 2

    .prologue
    .line 2567
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/high16 v1, 0x3e800000    # 0.25f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setPlaySpeed(F)V

    .line 2568
    return-void
.end method

.method public setVisibilityLoadingDialog(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 3243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 3244
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 3245
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3246
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "setVisibilityLoadingDialog : SET VISIBLE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3247
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 3262
    :cond_0
    :goto_0
    return-void

    .line 3248
    :cond_1
    if-eqz p1, :cond_0

    .line 3249
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "setVisibilityLoadingDialog : SET INVISIBLE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3250
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3251
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v0, :cond_0

    .line 3252
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestLayout()V

    .line 3253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->invalidate()V

    .line 3254
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isVideoRenderingStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3255
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "setVisibilityLoadingDialog : Video Rendering not started yet! we need to set visible empty view"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3256
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    goto :goto_0
.end method

.method public setVisibleAudioOnlyView(I)V
    .locals 3
    .param p1, "visible"    # I

    .prologue
    .line 3220
    const-string v0, "MiniVideoPlayerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisibleAudioOnlyView E : visible ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnly:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 3222
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnly:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3223
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnly:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0081

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnlyText:Landroid/widget/TextView;

    .line 3224
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioOnlyText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3228
    :goto_0
    return-void

    .line 3226
    :cond_0
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "setVisibleAudioOnly - mAudioOnly is null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVisibleEmptyView(I)V
    .locals 3
    .param p1, "visible"    # I

    .prologue
    .line 2713
    const-string v0, "MiniVideoPlayerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisibleEmptyView E : visible ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2714
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 2715
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2719
    :goto_0
    return-void

    .line 2717
    :cond_0
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "setVisibleEmptyView - mEmptyLayout is null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVisibleExitLayout(I)V
    .locals 3
    .param p1, "visible"    # I

    .prologue
    .line 2722
    const-string v0, "MiniVideoPlayerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisibleExitLayout E : visible ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2723
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 2724
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2726
    :cond_0
    return-void
.end method

.method public setXY(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/16 v6, 0xd

    .line 2689
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2690
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 2692
    :cond_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    .line 2693
    iput p2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    .line 2696
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveY:I

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->SaveX:I

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveY:I

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->PrevSaveX:I

    .line 2699
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 2700
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    .line 2701
    .local v1, "screenWidth":I
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    .line 2707
    .local v0, "screenHeight":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mXpos:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v2, v3

    if-gt v2, v1, :cond_1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mYpos:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v2, v3

    if-le v2, v0, :cond_2

    .line 2708
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2710
    :cond_2
    return-void

    .line 2703
    .end local v0    # "screenHeight":I
    .end local v1    # "screenWidth":I
    :cond_3
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I

    .line 2704
    .restart local v1    # "screenWidth":I
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I

    .restart local v0    # "screenHeight":I
    goto :goto_0
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3137
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "show : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsPlayingB4Hide:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3138
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    if-eqz v1, :cond_1

    .line 3139
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-eqz v1, :cond_0

    .line 3140
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSeekPosition()V

    .line 3141
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsPlayingB4Hide:Z

    if-eqz v1, :cond_0

    .line 3142
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->start()V

    .line 3144
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAIAHide:Z

    .line 3146
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/minimode/MiniModeService;->show()V

    .line 3150
    :try_start_0
    const-string v1, "com.sec.android.app.minimode.MiniModeService"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "isFirstShow"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 3151
    .local v0, "isFirstShowMethod":Ljava/lang/reflect/Method;
    if-eqz v0, :cond_2

    .line 3152
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_2

    .line 3154
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 3155
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "setProcessForeground : true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0

    .line 3167
    .end local v0    # "isFirstShowMethod":Ljava/lang/reflect/Method;
    :cond_2
    :goto_0
    return-void

    .line 3165
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3164
    :catch_1
    move-exception v1

    goto :goto_0

    .line 3163
    :catch_2
    move-exception v1

    goto :goto_0

    .line 3162
    :catch_3
    move-exception v1

    goto :goto_0

    .line 3161
    :catch_4
    move-exception v1

    goto :goto_0

    .line 3156
    .restart local v0    # "isFirstShowMethod":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v1

    goto :goto_0
.end method

.method public toggleController()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 811
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    if-nez v0, :cond_0

    .line 818
    :goto_0
    return-void

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->hide(Z)V

    goto :goto_0

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_0
.end method

.method public unRegisterObserver()V
    .locals 2

    .prologue
    .line 3708
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "unRegisterObserver E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3709
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCaptioningObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 3710
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mCaptioningObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 3712
    :cond_0
    return-void
.end method
