.class Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;
.super Ljava/lang/Object;
.source "VideoNfcDirectShare.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v4, 0x0

    .line 209
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNdefPushComplete mNdefStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$300(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$300(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 223
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->isSbeamOn()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$400(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onNdefPushComplete :: isSbeamOn !!"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 222
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    goto :goto_0
.end method
