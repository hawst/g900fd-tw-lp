.class public Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;
.super Ljava/lang/Object;
.source "SConnectMgr.java"


# static fields
.field private static final DLNA_EXTRA_KEY_DMR_ID:Ljava/lang/String; = "DEVICE"

.field private static final DLNA_EXTRA_KEY_URIS:Ljava/lang/String; = "android.intent.extra.STREAM"

.field private static TAG:Ljava/lang/String;

.field private static sSConnectMgr:Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;


# instance fields
.field private mUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->TAG:Ljava/lang/String;

    .line 21
    new-instance v0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->sSConnectMgr:Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->sSConnectMgr:Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;

    return-object v0
.end method


# virtual methods
.method public getStartUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->mUris:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUriList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->mUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setInfo(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 32
    const-string v3, "DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "dmrId":Ljava/lang/String;
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->mUris:Ljava/util/ArrayList;

    .line 35
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->mUris:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    const/4 v2, 0x0

    .line 44
    :goto_0
    return v2

    .line 37
    :cond_0
    sget-object v3, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dmrId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mUris size:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    .line 40
    .local v0, "asfUtil":Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setDmrUdn(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setDirectDmcMode(Z)V

    .line 43
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    const/16 v4, 0x25

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->setKeyType(I)V

    goto :goto_0
.end method
