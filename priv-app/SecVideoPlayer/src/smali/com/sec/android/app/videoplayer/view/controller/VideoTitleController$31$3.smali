.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

.field final synthetic val$Checkbox:Landroid/widget/CheckBox;

.field final synthetic val$mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;Landroid/widget/CheckBox;Lcom/sec/android/app/videoplayer/db/SharedPreference;)V
    .locals 0

    .prologue
    .line 2260
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->val$Checkbox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->val$mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 2262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->val$Checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2263
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->val$mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "showwifipopup_changeplayer"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    .line 2266
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2269
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->setContext(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1202(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 2270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;->this$1:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->createDevicePopup()V

    .line 2271
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2272
    return-void
.end method
