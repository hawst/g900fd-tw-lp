.class Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;
.super Landroid/os/AsyncTask;
.source "VideoTagBuddyInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateLocationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$1;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 182
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->findAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 182
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 189
    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateLocationTask - finished"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->sendTagBuddyInfoUpdateNotify()V

    .line 194
    :cond_0
    return-void
.end method
