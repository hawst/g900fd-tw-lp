.class Lcom/sec/android/app/videoplayer/videowall/TranscodeService$3;
.super Landroid/content/BroadcastReceiver;
.source "TranscodeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$3;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 363
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 364
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSIOPLevelReceiver - action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v2, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    const-string v2, "live_thumbnail_disable"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 368
    .local v1, "disableLIveThembnail":Z
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSIOPLevelReceiver - disableLIveThembnail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 370
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$3;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseTranscodeThread()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$900(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V

    .line 373
    .end local v1    # "disableLIveThembnail":Z
    :cond_0
    return-void
.end method
