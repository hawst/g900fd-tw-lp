.class Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;
.super Ljava/lang/Object;
.source "VideoTagBuddySetting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

.field final synthetic val$contextualData:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iput-object p2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->val$contextualData:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 142
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 144
    .local v2, "selectView":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$000(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagBuddyShowStatus()Z

    move-result v3

    .line 146
    .local v3, "showTagState":Z
    if-nez v3, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    if-eqz v2, :cond_0

    .line 151
    const v4, 0x7f0d0068

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 152
    .local v1, "itemCheckBox":Landroid/widget/CheckBox;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->val$contextualData:Ljava/util/List;

    invoke-interface {v4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "data":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 155
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 156
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 159
    const-string v4, "Location"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 160
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$100(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeLocationTagVisibility(I)V

    .line 166
    :cond_2
    const-string v4, "Date"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 167
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$200(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeDateTagVisibility(I)V

    .line 187
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$100(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$000(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v7

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$100(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v4

    if-nez v4, :cond_7

    move v4, v5

    :goto_2
    invoke-virtual {v7, v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setLocationTagShowStatus(Z)V

    .line 191
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$200(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 192
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$000(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$200(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_8

    :goto_3
    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setDateTagShowStatus(Z)V

    goto/16 :goto_0

    .line 173
    :cond_5
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 175
    const-string v4, "Location"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 176
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeLocationTagVisibility(I)V

    .line 179
    :cond_6
    const-string v4, "Date"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 180
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$200(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeDateTagVisibility(I)V

    goto :goto_1

    :cond_7
    move v4, v6

    .line 188
    goto :goto_2

    :cond_8
    move v5, v6

    .line 192
    goto :goto_3
.end method
