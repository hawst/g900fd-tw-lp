.class Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemCache"
.end annotation


# static fields
.field private static mLruCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createLruCache()Landroid/util/LruCache;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    # getter for: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$000()Landroid/content/Context;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    .line 116
    .local v1, "memClass":I
    const/high16 v2, 0x100000

    mul-int/2addr v2, v1

    div-int/lit8 v0, v2, 0x4

    .line 117
    .local v0, "cacheSize":I
    new-instance v2, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache$1;

    invoke-direct {v2, v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache$1;-><init>(I)V

    return-object v2
.end method

.method public static declared-synchronized get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 91
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->getCache()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-nez v0, :cond_0

    .line 108
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->createLruCache()Landroid/util/LruCache;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    .line 110
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    return-object v0
.end method

.method public static declared-synchronized put(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 99
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :goto_0
    monitor-exit v1

    return-void

    .line 102
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->getCache()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
