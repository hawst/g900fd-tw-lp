.class Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;
.super Ljava/lang/Object;
.source "AsfUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 3
    .param p1, "sprovider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IServiceConnectEventListener. onCreated. state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$9;->$SwitchMap$com$samsung$android$allshare$ServiceConnector$ServiceState:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 556
    .end local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 541
    .restart local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$102(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    check-cast p1, Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .end local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$202(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$400(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mServerRemovedListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$500(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->updatePlayerList()V

    goto :goto_0

    .line 536
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 3
    .param p1, "arg0"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IServiceConnectEventListener. onDeleted. mUseAllshare : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$102(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->connectAllShareFwk()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$600(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    .line 565
    :cond_0
    return-void
.end method
