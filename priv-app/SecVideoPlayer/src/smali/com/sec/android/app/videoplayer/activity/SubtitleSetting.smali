.class public Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;
.super Landroid/app/Activity;
.source "SubtitleSetting.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;


# instance fields
.field private mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private showDialog()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->updateDialog()V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setOnDialogDisappearListener(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->show()V

    .line 54
    :cond_1
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->mVideoSubtitleSettingDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->onOrientationChanged()V

    .line 37
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "saveInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->showDialog()V

    .line 18
    return-void
.end method

.method public onDisappear()V
    .locals 0

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->finish()V

    .line 42
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 23
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SubtitleSetting;->showDialog()V

    .line 29
    return-void
.end method
