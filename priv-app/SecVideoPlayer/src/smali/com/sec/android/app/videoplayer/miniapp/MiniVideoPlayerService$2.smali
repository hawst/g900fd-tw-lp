.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;
.super Landroid/content/BroadcastReceiver;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 833
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 836
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 837
    .local v0, "action":Ljava/lang/String;
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mReceiver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isLocalContents()Z

    move-result v1

    if-nez v1, :cond_1

    .line 840
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "mReceiver - NOT Local contents : SKIP media receiver action"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 844
    :cond_1
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 845
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "mMediaReceiver - Intent.ACTION_MEDIA_SCANNER_FINISHED."

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCurPlayingFileExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 847
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "mMediaReceiver - File removed, exit"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0

    .line 850
    :cond_2
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 851
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/extSdCard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 852
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a011d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 853
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 855
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLastFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 856
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "mMediaReceiver - Intent.ACTION_MEDIA_UNMOUNTED."

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0
.end method
