.class Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$3;
.super Ljava/lang/Object;
.source "ProgressbarPreviewHelp.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$3;->this$0:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 114
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 124
    :cond_0
    :goto_0
    return v4

    .line 116
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$3;->this$0:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->isEnabledProgressOptionByHoverType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$3;->this$0:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->access$000(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method
