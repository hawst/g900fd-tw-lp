.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;
.super Ljava/lang/Object;
.source "ChangePlayerPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 403
    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "showDevicePopup onClick"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableDLNAdisplay()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->clearDeviceIcon()V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->refreshPlayerList()V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->refreshWifiDisplays()V

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    const/16 v1, 0x2710

    # setter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1402(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)I

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshDevice:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;->this$1:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshDevice:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 420
    return-void
.end method
