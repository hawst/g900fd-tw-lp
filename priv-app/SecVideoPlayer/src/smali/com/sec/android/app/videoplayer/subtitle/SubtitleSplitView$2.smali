.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$2;
.super Ljava/lang/Object;
.source "SubtitleSplitView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 238
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 5
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    const/4 v4, 0x0

    .line 226
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged-scrollState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 228
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 232
    :cond_0
    return-void
.end method
