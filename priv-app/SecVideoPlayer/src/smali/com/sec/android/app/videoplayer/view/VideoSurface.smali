.class public Lcom/sec/android/app/videoplayer/view/VideoSurface;
.super Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;
.source "VideoSurface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoSurface"

.field private static mSurfaceCount:I

.field private static mSurfaceHeight:I

.field private static mSurfaceWidth:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSurfaceExists:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceSizeChangedListener:Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I

    .line 28
    sput v1, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    .line 29
    sput v1, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceSizeChangedListener:Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;

    .line 193
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mContext:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->initVideoView()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mContext:Landroid/content/Context;

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->initVideoView()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceSizeChangedListener:Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;

    .line 193
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mContext:Landroid/content/Context;

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->initVideoView()V

    .line 49
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/view/VideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoSurface;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I

    return v0
.end method

.method static synthetic access$108()I
    .locals 2

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I

    return v0
.end method

.method static synthetic access$110()I
    .locals 2

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/VideoSurface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/view/VideoSurface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoSurface;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z

    return p1
.end method

.method public static getSurfaceHeight()I
    .locals 1

    .prologue
    .line 228
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    return v0
.end method

.method public static getSurfaceWidth()I
    .locals 1

    .prologue
    .line 232
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    return v0
.end method

.method private initVideoView()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 169
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 171
    .local v0, "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 172
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 175
    return-void
.end method


# virtual methods
.method public getSurfaceExists()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z

    return v0
.end method

.method public getSurfaceHolder()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v8, 0x78

    const/4 v5, 0x1

    .line 52
    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    .line 53
    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    .line 55
    const/4 v4, 0x0

    .line 56
    .local v4, "videoWidth":I
    const/4 v3, 0x0

    .line 58
    .local v3, "videoHeight":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    .line 60
    .local v2, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v4

    .line 61
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v3

    .line 63
    if-nez v4, :cond_0

    if-nez v3, :cond_0

    .line 64
    const-string v5, "VideoSurface"

    const-string v6, "onMeasure() - videoWidth is 0 && videoHeight is 0 "

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v5, "VideoSurface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - caculated size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    sget v6, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    invoke-super {p0, v5, v6}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setMeasuredDimension(II)V

    .line 132
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-virtual {p0, v4, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v5

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    .line 70
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v5

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    .line 73
    const-string v5, "VideoSurface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - param size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v5, "VideoSurface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - real size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v5, "VideoSurface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - surfaceWidth : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", surfaceHeight : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v1

    .line 79
    .local v1, "screenMode":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 80
    const/4 v1, 0x0

    .line 83
    :cond_1
    if-lez v4, :cond_2

    if-lez v3, :cond_2

    .line 84
    packed-switch v1, :pswitch_data_0

    .line 126
    :cond_2
    :goto_1
    const-string v5, "VideoSurface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - caculated size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceSizeChangedListener:Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;

    if-eqz v5, :cond_3

    .line 128
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceSizeChangedListener:Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;

    invoke-interface {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;->onSurfaceSizeChanged()V

    .line 131
    :cond_3
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    sget v6, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    invoke-super {p0, v5, v6}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 86
    :pswitch_0
    const-string v5, "VideoSurface"

    const-string v6, "onMeasure() - VideoServiceUtil.KEEP_ASPECT_RATIO"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    mul-int/2addr v5, v4

    sget v6, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    mul-int/2addr v6, v3

    if-lt v5, v6, :cond_4

    .line 88
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    mul-int/2addr v5, v3

    div-int/2addr v5, v4

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    goto :goto_1

    .line 90
    :cond_4
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    mul-int/2addr v5, v4

    div-int/2addr v5, v3

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    goto :goto_1

    .line 94
    :pswitch_1
    if-lez v4, :cond_2

    if-lez v3, :cond_2

    .line 95
    const-string v5, "VideoSurface"

    const-string v6, "onMeasure() - VideoServiceUtil.ORIGINAL_SIZE"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    if-gt v4, v5, :cond_5

    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    if-le v3, v5, :cond_7

    .line 97
    :cond_5
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    mul-int/2addr v5, v4

    sget v6, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    mul-int/2addr v6, v3

    if-lt v5, v6, :cond_6

    .line 98
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    mul-int/2addr v5, v3

    div-int/2addr v5, v4

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    goto :goto_1

    .line 100
    :cond_6
    sget v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    mul-int/2addr v5, v4

    div-int/2addr v5, v3

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    goto :goto_1

    .line 102
    :cond_7
    sput v4, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    .line 103
    sput v3, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    goto :goto_1

    .line 109
    :pswitch_2
    const-string v5, "VideoSurface"

    const-string v6, "onMeasure() - VideoServiceUtil.FIT_TO_HEIGHT_RATIO"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v0

    .line 111
    .local v0, "screenHeight":I
    mul-int v5, v0, v4

    div-int/2addr v5, v3

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    .line 112
    sput v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    goto/16 :goto_1

    .line 116
    .end local v0    # "screenHeight":I
    :pswitch_3
    const-string v5, "VideoSurface"

    const-string v6, "onMeasure() - VideoServiceUtil.FULL_SCREEN_RATIO"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0, v4, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v5

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceWidth:I

    .line 118
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v5

    sput v5, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHeight:I

    goto/16 :goto_1

    .line 84
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 143
    move v0, p1

    .line 145
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 146
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 148
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 165
    :goto_0
    return v0

    .line 150
    :sswitch_0
    move v0, p1

    .line 151
    goto :goto_0

    .line 154
    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 155
    goto :goto_0

    .line 158
    :sswitch_2
    move v0, v2

    .line 159
    goto :goto_0

    .line 148
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public segSurfaceExists(Z)V
    .locals 0
    .param p1, "SurfaceExists"    # Z

    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z

    .line 191
    return-void
.end method

.method public setSurfaceHolder(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 183
    return-void
.end method

.method public setSurfaceSizeChangedListener(Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceSizeChangedListener:Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;

    .line 140
    return-void
.end method
