.class public Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;
.super Ljava/lang/Object;
.source "EditGuidePopup.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 7

    .prologue
    .line 31
    sget v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v2

    .line 32
    .local v2, "isAvailableSApps":Z
    if-eqz v2, :cond_0

    const v4, 0x7f0a0057

    .line 33
    .local v4, "title":I
    :goto_0
    if-eqz v2, :cond_1

    const v3, 0x7f0a01b3

    .line 34
    .local v3, "msg":I
    :goto_1
    if-eqz v2, :cond_2

    const v0, 0x7f0a00ae

    .line 36
    .local v0, "btn":I
    :goto_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 37
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 38
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 39
    new-instance v5, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;)V

    invoke-virtual {v1, v0, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 45
    const v5, 0x7f0a0026

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;)V

    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    .line 53
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 85
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 93
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 102
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 104
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->mDialog:Landroid/app/AlertDialog;

    return-object v5

    .line 32
    .end local v0    # "btn":I
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "msg":I
    .end local v4    # "title":I
    :cond_0
    const v4, 0x7f0a00a7

    goto :goto_0

    .line 33
    .restart local v4    # "title":I
    :cond_1
    const v3, 0x7f0a0107

    goto :goto_1

    .line 34
    .restart local v3    # "msg":I
    :cond_2
    const v0, 0x7f0a00dd

    goto :goto_2
.end method
