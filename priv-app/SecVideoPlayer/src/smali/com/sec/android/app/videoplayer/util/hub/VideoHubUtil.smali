.class public Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
.super Ljava/lang/Object;
.source "VideoHubUtil.java"


# static fields
.field private static sHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private bWathON:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentIdxOnDevice:I

.field private mLocalCursorOnDevice:Landroid/database/Cursor;

.field private mTotalCountOnDevice:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-class v0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 42
    return-void
.end method

.method private getCustomData()Z
    .locals 9

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "getCustomData()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const/4 v6, 0x0

    .line 364
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 366
    .local v8, "result":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getDrmCustomDataColumns()[Ljava/lang/String;

    move-result-object v2

    .line 369
    .local v2, "cols":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CUSTOM_DATA_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 374
    if-eqz v6, :cond_1

    .line 375
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->setCustomData(Landroid/database/Cursor;)V

    .line 377
    const/4 v8, 0x1

    .line 381
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 382
    const/4 v6, 0x0

    :goto_1
    move v0, v8

    .line 387
    :goto_2
    return v0

    .line 370
    :catch_0
    move-exception v7

    .line 371
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCustomData - SQLiteException :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const/4 v0, 0x0

    goto :goto_2

    .line 379
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 384
    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private getHubData(Ljava/lang/String;)V
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "getHubData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v6, 0x0

    .line 332
    .local v6, "c":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getVideoHubColumns()[Ljava/lang/String;

    move-result-object v2

    .line 334
    .local v2, "cols":[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 335
    .local v8, "where":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data = \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 343
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 344
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 345
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->setData(Landroid/database/Cursor;)V

    .line 348
    :cond_0
    if-eqz v6, :cond_1

    .line 349
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 350
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 351
    const/4 v6, 0x0

    .line 354
    :cond_1
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v7

    .line 339
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoCursor - SQLiteException :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    return-object v0
.end method

.method private getPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->bWathON:Z

    .line 50
    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    .line 57
    :cond_0
    return-void
.end method

.method private moveToFirst()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "moveToFirst()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 436
    :cond_0
    return-void
.end method

.method private moveToLast()V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "moveToLast()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 424
    :cond_0
    return-void
.end method

.method private moveToNext()V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "moveToNext()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 400
    :cond_0
    return-void
.end method

.method private moveToPrevious()V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "moveToPreivous()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 412
    :cond_0
    return-void
.end method


# virtual methods
.method public createLocalCursorOnDevice()I
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 188
    const-string v5, "title COLLATE LOCALIZED ASC"

    .line 189
    .local v5, "orderByClause":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getVideoHubColumns()[Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "cols":[Ljava/lang/String;
    const-string v3, "file_status = \'Downloaded\' or file_status = \'Downloading\'"

    .line 191
    .local v3, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 194
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 207
    iput-object v10, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    .line 210
    :cond_0
    if-nez v6, :cond_1

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "createLocalCursorOnDevice() - cursorVideo is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v9

    .line 222
    :goto_0
    return v0

    .line 195
    :catch_0
    move-exception v7

    .line 196
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createLocalCursorOnDevice() - SQLiteException :"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v6, 0x0

    move v0, v9

    .line 198
    goto :goto_0

    .line 199
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 200
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createLocalCursorOnDevice() - UnsupportedOperationException :"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const/4 v6, 0x0

    move v0, v9

    .line 202
    goto :goto_0

    .line 213
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "createLocalCursorOnDevice() - getCount() <= 0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 216
    goto :goto_0

    .line 218
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 219
    iput-object v6, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createLocalCursorOnDevice() -   mLocalCursorOnDevice.getCount() = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    goto :goto_0
.end method

.method public getCurrentIdxOnDevice(Ljava/lang/String;)V
    .locals 6
    .param p1, "currentPath"    # Ljava/lang/String;

    .prologue
    .line 273
    const/4 v1, -0x1

    .local v1, "idx":I
    const/4 v0, -0x1

    .line 274
    .local v0, "columnIndex":I
    const/4 v2, 0x0

    .line 276
    .local v2, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    const-string v4, "_data"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 279
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    add-int/lit8 v1, v1, 0x1

    .line 281
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 287
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    if-lt v1, v3, :cond_0

    .line 289
    :goto_0
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    if-ge v1, v3, :cond_2

    .line 290
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 294
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentIdxOnDevice() : mCurrentIdxOnDevice = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    return-void

    .line 284
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v4, "getCurrentIdxOnDevice() : find data!!"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 292
    :cond_2
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    goto :goto_1
.end method

.method public getData(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "drmCheck"    # Z

    .prologue
    .line 90
    if-eqz p2, :cond_0

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getCustomData()Z

    .line 93
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getHubData(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public getFilePathOnDevice(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;
    .param p2, "currentPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getLocalDeviceFilePath() : direction = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const/4 v0, 0x0

    .line 237
    .local v0, "path":Ljava/lang/String;
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-nez v1, :cond_1

    .line 238
    :cond_0
    iput v4, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    .line 239
    const/4 p2, 0x0

    .line 263
    .end local p2    # "currentPath":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 240
    .restart local p2    # "currentPath":Ljava/lang/String;
    :cond_1
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 241
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    goto :goto_0

    .line 244
    :cond_2
    invoke-virtual {p0, p2}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getCurrentIdxOnDevice(Ljava/lang/String;)V

    .line 246
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    if-eq v1, v4, :cond_4

    .line 247
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 248
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    if-nez v1, :cond_5

    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->moveToLast()V

    .line 260
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->setData(Landroid/database/Cursor;)V

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getPath()Ljava/lang/String;

    move-result-object v0

    :cond_4
    move-object p2, v0

    .line 263
    goto :goto_0

    .line 251
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->moveToPrevious()V

    goto :goto_1

    .line 253
    :cond_6
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 254
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mCurrentIdxOnDevice:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mTotalCountOnDevice:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_7

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->moveToFirst()V

    goto :goto_1

    .line 257
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->moveToNext()V

    goto :goto_1
.end method

.method public getHubUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 448
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getIdByPath(Ljava/lang/String;)J

    move-result-wide v0

    .line 450
    .local v0, "id":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 451
    sget-object v2, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 454
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getIdByPath(Ljava/lang/String;)J
    .locals 10
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 103
    const-wide/16 v8, -0x1

    .line 104
    .local v8, "id":J
    const/4 v6, 0x0

    .line 105
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 108
    .local v2, "cols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " escape \'!\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "selection":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 112
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 113
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 119
    :cond_0
    if-eqz v6, :cond_1

    .line 120
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 123
    :cond_1
    :goto_0
    return-wide v8

    .line 116
    :catch_0
    move-exception v7

    .line 117
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getHubIdByPath() path:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    if-eqz v6, :cond_1

    .line 120
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 119
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 120
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 11
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x0

    .line 133
    const-wide/16 v8, -0x1

    .line 134
    .local v8, "id":J
    const/4 v6, 0x0

    .line 135
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 138
    .local v2, "cols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " escape \'!\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, "selection":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 142
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 143
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 150
    :cond_0
    if-eqz v6, :cond_1

    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 155
    :cond_1
    :goto_0
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-eqz v0, :cond_3

    .line 156
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 159
    :goto_1
    return-object v0

    .line 147
    :catch_0
    move-exception v7

    .line 148
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getHubIdByPath() path:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    if-eqz v6, :cond_1

    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 150
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :cond_3
    move-object v0, v10

    .line 159
    goto :goto_1
.end method

.method public isWatchONEnable()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->bWathON:Z

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v1, "reset()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mLocalCursorOnDevice:Landroid/database/Cursor;

    .line 307
    :cond_0
    return-void
.end method

.method public sendBroadcastPlayedContentInfo()V
    .locals 3

    .prologue
    .line 313
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    const-string v2, "sendBroadcastPlayedContentInfo()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 315
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.everglades.video.ACTION_PLAYED_CONTENT_INFO"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    const-string v1, "product_id"

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getProductId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 317
    const-string v1, "video_attr_type_code_2"

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 320
    return-void
.end method

.method public set(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->init()V

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->mContext:Landroid/content/Context;

    .line 80
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    return-object v0
.end method

.method public setWatchONEnable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->bWathON:Z

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWatchONEnable. bYosemite : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->bWathON:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    return-void
.end method
