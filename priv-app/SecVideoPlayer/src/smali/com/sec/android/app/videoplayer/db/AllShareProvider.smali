.class public Lcom/sec/android/app/videoplayer/db/AllShareProvider;
.super Landroid/content/ContentProvider;
.source "AllShareProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field public static final ALLSHARE_TABLE_SCHEMA:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS video_allshare(_id INTEGER PRIMARY KEY, title TEXT NOT NULL, device TEXT NOT NULL, duration INTEGER, _size LONG, date_modified TEXT, mime_type TEXT, thumbnail TEXT, resolution TEXT, _data TEXT, seed TEXT, extension TEXT, seekmode TEXT );"

.field public static final ALLSHARE_VIDEO_DATE:Ljava/lang/String; = "date_modified"

.field public static final ALLSHARE_VIDEO_DB_URI:Ljava/lang/String; = "mediadburi"

.field public static final ALLSHARE_VIDEO_DEVICE:Ljava/lang/String; = "device"

.field public static final ALLSHARE_VIDEO_DURATION:Ljava/lang/String; = "duration"

.field public static final ALLSHARE_VIDEO_EXTENSION:Ljava/lang/String; = "extension"

.field public static final ALLSHARE_VIDEO_ID:Ljava/lang/String; = "_id"

.field public static final ALLSHARE_VIDEO_ITEM_SEED:Ljava/lang/String; = "seed"

.field public static final ALLSHARE_VIDEO_MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final ALLSHARE_VIDEO_RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final ALLSHARE_VIDEO_SEEKMODE:Ljava/lang/String; = "seekmode"

.field public static final ALLSHARE_VIDEO_SIZE:Ljava/lang/String; = "_size"

.field public static final ALLSHARE_VIDEO_THUMBNAIL_DATA:Ljava/lang/String; = "thumbnail"

.field public static final ALLSHARE_VIDEO_TITLE:Ljava/lang/String; = "title"

.field public static final ALLSHARE_VIDEO_URI_DATA:Ljava/lang/String; = "_data"

.field private static final ALL_SHARE_EXTERNAL_CONTENTS:I = 0x8

.field private static final ALL_SHARE_EXTERNAL_CONTENTS_ID:I = 0x9

.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.videoplayer.provider"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final DATABASE_NAME:Ljava/lang/String; = "video_remote_file.db"

.field private static final DATABASE_VERSION:I = 0x4

.field public static final DROP_TABLE_IF_EXISTS:Ljava/lang/String; = "DROP TABLE IF EXISTS "

.field private static final SCHEME:Ljava/lang/String; = "content://"

.field private static final SPECIPIC_MESSAGE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AllShareProvider"

.field public static final VIDEO_ALLSHARE_TABLE:Ljava/lang/String; = "video_allshare"

.field public static final VIDEO_LASTPLAYEDITEM_TABLE:Ljava/lang/String; = "video_lastplayitem"

.field public static final VIDEO_LASTPLAYEDITEM_TABLE_SCHEMA:Ljava/lang/String; = "CREATE TABLE video_lastplayitem(_id INTEGER PRIMARY KEY, title TEXT NOT NULL, duration INTEGER, _size INTEGER, date_modified TEXT, mime_type TEXT, resolution TEXT, _data TEXT );"


# instance fields
.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mOpenHelper:Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;

.field private mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "content://com.sec.android.app.videoplayer.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 115
    return-void
.end method

.method public static final getVideoProviderContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "video_lastplayitem"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 142
    const-string v4, "AllShareProvider"

    const-string v5, "bulkInsert"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v3, 0x0

    .line 144
    .local v3, "numInserted":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 147
    :try_start_0
    array-length v2, p2

    .line 149
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 150
    aget-object v4, p2, v1

    if-eqz v4, :cond_0

    .line 151
    aget-object v4, p2, v1

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 149
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    :cond_1
    move v3, v2

    .line 155
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 162
    .end local v1    # "i":I
    .end local v2    # "len":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 163
    return v3

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Landroid/database/SQLException;
    :try_start_1
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 304
    const-string v1, "AllShareProvider"

    const-string v2, "delete"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "video_lastplayitem"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 314
    .local v0, "count":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 317
    return v0

    .line 311
    .end local v0    # "count":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "video_allshare"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .restart local v0    # "count":I
    goto :goto_0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v8, 0x0

    .line 171
    const-string v5, "AllShareProvider"

    const-string v6, "insert E"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v1, 0x0

    .line 177
    .local v1, "values":Landroid/content/ContentValues;
    if-eqz p2, :cond_1

    .line 178
    move-object v1, p2

    .line 185
    :goto_0
    const-string v5, "date_modified"

    invoke-virtual {v1, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 188
    .local v0, "now":Ljava/lang/Long;
    const-string v5, "date_modified"

    invoke-virtual {v1, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 195
    .end local v0    # "now":Ljava/lang/Long;
    :cond_0
    const-wide/16 v2, 0x0

    .line 197
    .local v2, "rowId":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 198
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "video_lastplayitem"

    invoke-virtual {v5, v6, v8, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 204
    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-lez v5, :cond_4

    .line 205
    const/4 v4, 0x0

    .line 207
    .local v4, "videoUri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 208
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 213
    :goto_2
    const-string v5, "AllShareProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert. DB insert successded, videoUri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v4, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 217
    return-object v4

    .line 181
    .end local v2    # "rowId":J
    .end local v4    # "videoUri":Landroid/net/Uri;
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    .end local v1    # "values":Landroid/content/ContentValues;
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .restart local v1    # "values":Landroid/content/ContentValues;
    goto :goto_0

    .line 200
    .restart local v2    # "rowId":J
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "video_allshare"

    invoke-virtual {v5, v6, v8, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    goto :goto_1

    .line 210
    .restart local v4    # "videoUri":Landroid/net/Uri;
    :cond_3
    sget-object v5, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    goto :goto_2

    .line 221
    .end local v4    # "videoUri":Landroid/net/Uri;
    :cond_4
    new-instance v5, Landroid/database/SQLException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert row into "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    .line 225
    const-string v1, "AllShareProvider"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v1, Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mOpenHelper:Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;

    .line 230
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mOpenHelper:Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/db/AllShareProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 231
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.videoplayer.provider"

    const-string v3, "#"

    const/16 v4, 0x9

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.videoplayer.provider"

    const-string v3, "CREATE TABLE video_lastplayitem(_id INTEGER PRIMARY KEY, title TEXT NOT NULL, duration INTEGER, _size INTEGER, date_modified TEXT, mime_type TEXT, resolution TEXT, _data TEXT );"

    const/16 v4, 0x8

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.videoplayer.provider"

    const-string v3, "CREATE TABLE video_lastplayitem(_id INTEGER PRIMARY KEY, title TEXT NOT NULL, duration INTEGER, _size INTEGER, date_modified TEXT, mime_type TEXT, resolution TEXT, _data TEXT );/#"

    const/16 v4, 0x9

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "AllShareProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 244
    const-string v1, "AllShareProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "query E. uri = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 247
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v8, 0x0

    .line 249
    .local v8, "c":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 250
    const-string v1, "video_lastplayitem"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 251
    const-string v1, "AllShareProvider"

    const-string v2, "query. last played table"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 277
    :goto_1
    if-eqz v8, :cond_1

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 280
    :cond_1
    return-object v8

    .line 253
    :cond_2
    const-string v1, "video_allshare"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 267
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 284
    const-string v2, "AllShareProvider"

    const-string v3, "update"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/4 v0, 0x0

    .line 287
    .local v0, "count":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "video_lastplayitem"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 296
    :cond_0
    :goto_0
    const-string v2, "AllShareProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update X. count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 300
    return v0

    .line 290
    :catch_0
    move-exception v1

    .line 292
    .local v1, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0
.end method
