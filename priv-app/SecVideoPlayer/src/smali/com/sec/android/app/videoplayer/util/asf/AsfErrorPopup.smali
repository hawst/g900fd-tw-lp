.class public Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;
.super Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;
.source "AsfErrorPopup.java"


# static fields
.field private static sAsfErrorPopup:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDialog:Landroid/app/AlertDialog;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->sAsfErrorPopup:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;-><init>()V

    .line 19
    const-class v0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->TAG:Ljava/lang/String;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleAsfError()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleStateChanged()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private create()V
    .locals 3

    .prologue
    .line 162
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 164
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 167
    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$1;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 173
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    const/16 v2, 0x2db

    if-ne v1, v2, :cond_0

    .line 174
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$2;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 181
    :cond_0
    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 191
    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 240
    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$5;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 246
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    .line 247
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->sAsfErrorPopup:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    return-object v0
.end method

.method private getMessage()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 78
    const/4 v0, 0x0

    .line 79
    .local v0, "errorMsg":Ljava/lang/String;
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    sparse-switch v1, :sswitch_data_0

    .line 115
    :goto_0
    return-object v0

    .line 92
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a018e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    goto :goto_0

    .line 96
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0053

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getProvider()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 97
    goto :goto_0

    .line 100
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 101
    goto :goto_0

    .line 104
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0014

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getSelectedPlayer()Lcom/samsung/android/allshare/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 105
    goto :goto_0

    .line 108
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 109
    goto :goto_0

    .line 79
    nop

    :sswitch_data_0
    .sparse-switch
        0x2bc -> :sswitch_0
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_0
        0x2bf -> :sswitch_0
        0x2c0 -> :sswitch_0
        0x2c1 -> :sswitch_0
        0x2c2 -> :sswitch_0
        0x2c3 -> :sswitch_0
        0x2c4 -> :sswitch_0
        0x2c5 -> :sswitch_0
        0x2c6 -> :sswitch_0
        0x2c7 -> :sswitch_0
        0x2c8 -> :sswitch_2
        0x2ca -> :sswitch_1
        0x2db -> :sswitch_3
        0x2ee -> :sswitch_4
    .end sparse-switch
.end method

.method private getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "title":Ljava/lang/String;
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    sparse-switch v1, :sswitch_data_0

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    .line 122
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0184

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    goto :goto_0

    .line 126
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 127
    goto :goto_0

    .line 120
    :sswitch_data_0
    .sparse-switch
        0x2db -> :sswitch_0
        0x2ee -> :sswitch_1
    .end sparse-switch
.end method

.method private handleAsfError()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 143
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSelectedbyChangePlayer(Z)V

    .line 145
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    const/16 v2, 0x2ee

    if-ne v1, v2, :cond_0

    .line 159
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const/16 v2, 0x2ea

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->notify(II)V

    .line 149
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    .line 150
    .local v0, "schemetype":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    :cond_1
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    const/16 v2, 0x2ca

    if-ne v1, v2, :cond_2

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 154
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v4, v2, v3, v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->changePlayer(IJZ)V

    goto :goto_0

    .line 157
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method private handleStateChanged()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->notifyPlayerState()V

    .line 140
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 38
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 55
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 62
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 67
    :cond_0
    return-void
.end method

.method public show(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    if-eq v0, p1, :cond_1

    .line 71
    :cond_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I

    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->create()V

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->show()V

    .line 75
    return-void
.end method
