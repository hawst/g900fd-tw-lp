.class public Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;
.super Ljava/lang/Object;
.source "ZoomEventHandler.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;
    }
.end annotation


# static fields
.field private static final HD_FRAME_RECT:I = 0xe1000

.field private static final MIN_INTERVAL_FOR_DOUBLE_TAP_DETECTION:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "ZoomEventHandler"

.field private static ZOOM_RESET_MARGIN:I

.field private static bIsDefaultVideoCropSet:Z

.field private static mContext:Landroid/content/Context;

.field private static mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# instance fields
.field private mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

.field private mGestureDetectors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mLastPositionTapTime:J

.field private mLastPositionX:F

.field private mLastPositionY:F

.field private mLinearZoomDetector:Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

.field private mLongPressAnchor:Landroid/graphics/Point;

.field private mLongPressTapTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->ZOOM_RESET_MARGIN:I

    .line 46
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->bIsDefaultVideoCropSet:Z

    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLinearZoomDetector:Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    .line 41
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionTapTime:J

    .line 42
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressTapTime:J

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressAnchor:Landroid/graphics/Point;

    .line 44
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionX:F

    .line 45
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionY:F

    .line 54
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mHandler:Landroid/os/Handler;

    .line 57
    sput-object p1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mContext:Landroid/content/Context;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->ZOOM_RESET_MARGIN:I

    .line 69
    new-instance v0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLinearZoomDetector:Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLinearZoomDetector:Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    return-void
.end method

.method private CheckforLongPress(Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v10, 0x0

    const/high16 v8, 0x41200000    # 10.0f

    .line 163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 164
    .local v0, "action":I
    and-int/lit16 v1, v0, 0xff

    .line 166
    .local v1, "actionCode":I
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 168
    .local v5, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v7

    int-to-float v7, v7

    sub-float v2, v6, v7

    .line 169
    .local v2, "adustX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v7

    int-to-float v7, v7

    sub-float v3, v6, v7

    .line 171
    .local v3, "adustY":F
    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 173
    iget-wide v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressTapTime:J

    cmp-long v6, v6, v10

    if-nez v6, :cond_1

    .line 174
    new-instance v6, Landroid/graphics/Point;

    float-to-int v7, v2

    float-to-int v8, v3

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressAnchor:Landroid/graphics/Point;

    .line 175
    const-string v6, "ZoomEventHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LongPress SET MotionEvent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_1

    .line 181
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->onLongPress()V

    .line 180
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 185
    .end local v4    # "index":I
    :cond_0
    const/4 v6, 0x2

    if-ne v1, v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressAnchor:Landroid/graphics/Point;

    if-eqz v6, :cond_2

    .line 186
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressAnchor:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v8

    if-ltz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressAnchor:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float/2addr v6, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v8

    if-gez v6, :cond_2

    .line 195
    :cond_1
    :goto_1
    return-void

    .line 191
    :cond_2
    const-string v6, "ZoomEventHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LongPRessCancled MotionEvent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iput-wide v10, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressTapTime:J

    .line 193
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x64

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method private static _setVideoCrop(IIII)I
    .locals 6
    .param p0, "left"    # I
    .param p1, "top"    # I
    .param p2, "right"    # I
    .param p3, "bottom"    # I

    .prologue
    .line 220
    const/4 v0, -0x1

    .line 225
    .local v0, "lRetval":I
    sget-object v1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-static {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->getEvenCoOrdinate(I)I

    move-result v2

    invoke-static {p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->getEvenCoOrdinate(I)I

    move-result v3

    invoke-static {p2}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->getEvenCoOrdinate(I)I

    move-result v4

    invoke-static {p3}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->getEvenCoOrdinate(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setVideoCrop(IIII)V

    .line 229
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->checkZoomForPlayerPauseState()V

    .line 232
    const/4 v0, 0x0

    .line 233
    return v0
.end method

.method private static checkZoomForPlayerPauseState()V
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 207
    sget-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    sget-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->handleZoomOnPause()V

    .line 211
    :cond_0
    return-void
.end method

.method private static getEvenCoOrdinate(I)I
    .locals 3
    .param p0, "coordinate"    # I

    .prologue
    .line 237
    move v0, p0

    .line 239
    .local v0, "lRetval":I
    rem-int/lit8 v1, v0, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 240
    add-int/lit8 v0, v0, -0x1

    .line 242
    :cond_0
    return v0
.end method

.method private isZoomReset(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x0

    .line 126
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 127
    .local v0, "action":I
    and-int/lit16 v1, v0, 0xff

    .line 128
    .local v1, "actionCode":I
    const/4 v2, 0x0

    .line 136
    .local v2, "bReset":Z
    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 138
    .local v4, "currTapTime":J
    iget-wide v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionTapTime:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x1f4

    cmp-long v3, v6, v8

    if-gez v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    sub-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v6, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->ZOOM_RESET_MARGIN:I

    int-to-float v6, v6

    cmpg-float v3, v3, v6

    if-gez v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    sub-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v6, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->ZOOM_RESET_MARGIN:I

    int-to-float v6, v6

    cmpg-float v3, v3, v6

    if-gez v3, :cond_2

    .line 142
    const-string v3, "ZoomEventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isZoomReset : bReset = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    sget-boolean v3, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->bIsDefaultVideoCropSet:Z

    if-nez v3, :cond_0

    .line 145
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->setDefaultVideoCrop()I

    .line 148
    :cond_0
    const/4 v2, 0x1

    .line 149
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    .line 150
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionTapTime:J

    .line 151
    iput v10, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionX:F

    .line 152
    iput v10, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionY:F

    .line 159
    .end local v4    # "currTapTime":J
    :cond_1
    :goto_0
    return v2

    .line 154
    .restart local v4    # "currTapTime":J
    :cond_2
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionTapTime:J

    .line 155
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionX:F

    .line 156
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionY:F

    goto :goto_0
.end method

.method public static setDefaultVideoCrop()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 215
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->bIsDefaultVideoCropSet:Z

    .line 216
    sget-object v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-static {v2, v2, v0, v1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->_setVideoCrop(IIII)I

    move-result v0

    return v0
.end method

.method public static setVideoCrop(IIII)I
    .locals 1
    .param p0, "left"    # I
    .param p1, "top"    # I
    .param p2, "right"    # I
    .param p3, "bottom"    # I

    .prologue
    .line 199
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->bIsDefaultVideoCropSet:Z

    .line 200
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->_setVideoCrop(IIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method public bringToFront()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLinearZoomDetector:Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLinearZoomDetector:Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->bringToFront()V

    .line 81
    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 247
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_1

    .line 251
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->onLongPress()V

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLongPressTapTime:J

    .line 257
    .end local v0    # "index":I
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 84
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 85
    .local v2, "detectorCount":I
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->isZoomReset(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 86
    .local v1, "bReset":Z
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->CheckforLongPress(Landroid/view/MotionEvent;)V

    .line 87
    const/4 v0, 0x0

    .line 90
    .local v0, "bHandled":Z
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    if-ge v3, v2, :cond_3

    .line 91
    if-eqz v1, :cond_1

    .line 96
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 90
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 98
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->isGestureTracked()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 100
    const-string v4, "ZoomEventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Tracked Event Handled :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Tracked:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->isGestureTracked()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_2
    if-nez v0, :cond_3

    .line 105
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 107
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->isGestureTracked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 110
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    .line 118
    :cond_3
    const-string v4, "ZoomEventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ZoomEventHandler onTouchEvent:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    return v0
.end method

.method setZoomReset()V
    .locals 4

    .prologue
    .line 261
    const-string v1, "ZoomEventHandler"

    const-string v2, "setZoomReset"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->setDefaultVideoCrop()I

    .line 263
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mCurrTrackedDetector:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    .line 264
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mLastPositionTapTime:J

    .line 266
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 269
    :cond_0
    return-void
.end method
