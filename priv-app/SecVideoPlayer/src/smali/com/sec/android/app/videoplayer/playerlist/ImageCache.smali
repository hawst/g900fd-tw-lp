.class public Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;,
        Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;,
        Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;
    }
.end annotation


# static fields
.field private static final LEAST_SIZE_OF_DOWN_SCALE:I = 0x1e000

.field private static final MAX_THREADS:I = 0x8

.field private static final REFERENCE_LENGTH_OF_BITMAP:I = 0x1e0

.field private static mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private static mContext:Landroid/content/Context;

.field private static mHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

.field private static mOnUpdatedListener:Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;


# instance fields
.field private mBitmapWorker:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mHttpClient:Landroid/net/http/AndroidHttpClient;

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;-><init>(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mBitmapWorker:Ljava/lang/Runnable;

    .line 281
    const-string v0, "AllShare Sample 1/1"

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    .line 348
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$2;-><init>(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHandler:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500()Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;

    return-object v0
.end method

.method private downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/high16 v6, 0x43f00000    # 480.0f

    .line 318
    if-nez p1, :cond_0

    const/4 v2, 0x0

    .line 345
    :goto_0
    return-object v2

    .line 320
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v4

    const v5, 0x1e000

    if-le v4, v5, :cond_3

    .line 321
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v3, v4

    .line 322
    .local v3, "width":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v0, v4

    .line 324
    .local v0, "height":F
    const/4 v1, 0x0

    .line 325
    .local v1, "ratio":F
    cmpl-float v4, v3, v0

    if-lez v4, :cond_2

    .line 326
    cmpl-float v4, v3, v6

    if-lez v4, :cond_1

    .line 327
    div-float v1, v3, v6

    .line 328
    div-float v4, v0, v1

    float-to-int v4, v4

    int-to-float v0, v4

    .line 329
    const/high16 v3, 0x43f00000    # 480.0f

    .line 339
    :cond_1
    :goto_1
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-lez v4, :cond_3

    .line 340
    float-to-int v4, v3

    float-to-int v5, v0

    const/4 v6, 0x1

    invoke-static {p1, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 341
    .local v2, "resized":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 332
    .end local v2    # "resized":Landroid/graphics/Bitmap;
    :cond_2
    cmpl-float v4, v0, v6

    if-lez v4, :cond_1

    .line 333
    div-float v1, v0, v6

    .line 334
    div-float v4, v3, v1

    float-to-int v4, v4

    int-to-float v3, v4

    .line 335
    const/high16 v0, 0x43f00000    # 480.0f

    goto :goto_1

    .end local v0    # "height":F
    .end local v1    # "ratio":F
    .end local v3    # "width":F
    :cond_3
    move-object v2, p1

    .line 345
    goto :goto_0
.end method

.method private downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 285
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 288
    .local v3, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 289
    .end local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v4, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v8, v4}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 290
    .local v6, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    .line 292
    .local v7, "statusCode":I
    const/16 v8, 0xc8

    if-eq v7, v8, :cond_0

    const/4 v8, 0x0

    move-object v3, v4

    .line 314
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "statusCode":I
    .restart local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :goto_0
    return-object v8

    .line 294
    .end local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "response":Lorg/apache/http/HttpResponse;
    .restart local v7    # "statusCode":I
    :cond_0
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 296
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_2

    .line 297
    const/4 v5, 0x0

    .line 300
    .local v5, "inputStream":Ljava/io/InputStream;
    :try_start_2
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 301
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 303
    if-eqz v5, :cond_1

    .line 304
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 306
    :cond_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .end local v5    # "inputStream":Ljava/io/InputStream;
    :cond_2
    move-object v3, v4

    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "statusCode":I
    .restart local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :cond_3
    :goto_1
    move-object v8, v0

    .line 314
    goto :goto_0

    .line 303
    .end local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "response":Lorg/apache/http/HttpResponse;
    .restart local v7    # "statusCode":I
    :catchall_0
    move-exception v8

    if-eqz v5, :cond_4

    .line 304
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 306
    :cond_4
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    throw v8
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 309
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "statusCode":I
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 310
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :goto_2
    if-eqz v3, :cond_3

    .line 311
    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    goto :goto_1

    .line 309
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public static getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 57
    if-nez p0, :cond_1

    move-object v0, v1

    .line 67
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 60
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 63
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->QueueInProcessContain(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 64
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->put(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->getInstance()Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    move-result-object v2

    invoke-direct {v2}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->startWorkerThread()V

    :cond_2
    move-object v0, v1

    .line 67
    goto :goto_0
.end method

.method private static getInstance()Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mUniqueInstance:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mUniqueInstance:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mUniqueInstance:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    return-object v0
.end method

.method private getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 209
    const/4 v5, -0x1

    .line 210
    .local v5, "time":I
    const-wide/16 v2, -0x1

    .line 211
    .local v2, "duration":J
    const/4 v0, 0x0

    .line 212
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 214
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 216
    const/16 v6, 0x9

    invoke-virtual {v4, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 218
    const-wide/16 v6, 0x3a98

    cmp-long v6, v2, v6

    if-ltz v6, :cond_1

    .line 219
    const v5, 0xe4e1c0

    .line 224
    :cond_0
    :goto_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->setVideoDownSize(Landroid/media/MediaMetadataRetriever;)V

    .line 225
    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 233
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 236
    :goto_1
    return-object v0

    .line 220
    :cond_1
    const-wide/16 v6, 0x3e8

    cmp-long v6, v2, v6

    if-ltz v6, :cond_0

    .line 221
    const v5, 0xf4240

    goto :goto_0

    .line 226
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .line 228
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 229
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 233
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .line 230
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 231
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 233
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v6
.end method

.method private isRotatedMedia(Landroid/media/MediaMetadataRetriever;)Z
    .locals 4
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v2, 0x0

    .line 264
    const/16 v3, 0x18

    invoke-virtual {p1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .end local v1    # "value":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 267
    .local v0, "rotate":I
    sparse-switch v0, :sswitch_data_0

    .line 277
    :goto_1
    :sswitch_0
    return v2

    .line 265
    .end local v0    # "rotate":I
    .restart local v1    # "value":Ljava/lang/String;
    :cond_0
    const-string v1, "0"

    goto :goto_0

    .line 274
    .end local v1    # "value":Ljava/lang/String;
    .restart local v0    # "rotate":I
    :sswitch_1
    const/4 v2, 0x1

    goto :goto_1

    .line 267
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_1
    .end sparse-switch
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 52
    sput-object p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mContext:Landroid/content/Context;

    .line 54
    :cond_0
    return-void
.end method

.method public static setOnUpdatedListener(Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;

    .prologue
    .line 362
    sput-object p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;

    .line 363
    return-void
.end method

.method private setVideoDownSize(Landroid/media/MediaMetadataRetriever;)V
    .locals 6
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v5, 0x1

    .line 240
    const/high16 v1, -0x40800000    # -1.0f

    .line 241
    .local v1, "ratio":F
    const/4 v0, -0x1

    .line 242
    .local v0, "height":I
    const/16 v3, 0x12

    invoke-virtual {p1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 243
    .local v2, "width":I
    const/16 v3, 0x13

    invoke-virtual {p1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 245
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->isRotatedMedia(Landroid/media/MediaMetadataRetriever;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 246
    const/16 v2, 0xa0

    .line 247
    const/16 v0, 0x1e0

    .line 260
    :cond_0
    :goto_0
    invoke-virtual {p1, v2, v0, v5, v5}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 261
    return-void

    .line 249
    :cond_1
    if-le v2, v0, :cond_2

    .line 250
    const/16 v3, 0x1e0

    if-le v2, v3, :cond_0

    .line 251
    int-to-float v3, v2

    const/high16 v4, 0x43f00000    # 480.0f

    div-float v1, v3, v4

    .line 252
    int-to-float v3, v0

    div-float/2addr v3, v1

    float-to-int v0, v3

    .line 253
    const/16 v2, 0x1e0

    goto :goto_0

    .line 256
    :cond_2
    const/16 v0, 0xa0

    .line 257
    const/16 v2, 0x1e0

    goto :goto_0
.end method

.method private startWorkerThread()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v0, :cond_0

    .line 73
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 76
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    if-nez v0, :cond_1

    .line 77
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHubUtil:Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_2

    .line 81
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mBitmapWorker:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 85
    return-void
.end method
