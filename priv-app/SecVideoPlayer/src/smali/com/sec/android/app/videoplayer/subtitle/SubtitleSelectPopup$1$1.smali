.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public oSubtitleInfo(I)V
    .locals 4
    .param p1, "what"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 195
    sparse-switch p1, :sswitch_data_0

    .line 216
    :goto_0
    return-void

    .line 198
    :sswitch_0
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "setOnSubtitleInfoListener. MEDIA_INFO_UNSUPPORTED_SUBTITLE E or MEDIA_INFO_SUBTITLE_TIMED_OUT E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/io/File;)Ljava/io/File;

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Z)Z

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->createSelectSubtitlePopup()V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0092

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 209
    :sswitch_1
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "setOnSubtitleInfoListener. MEDIA_INFO_METADATA_UPDATE E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Z)Z

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x322 -> :sswitch_1
        0x385 -> :sswitch_0
        0x386 -> :sswitch_0
    .end sparse-switch
.end method
