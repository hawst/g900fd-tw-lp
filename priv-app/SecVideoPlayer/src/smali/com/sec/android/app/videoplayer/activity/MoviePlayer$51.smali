.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 6579
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 8
    .param p1, "displayId"    # I

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    .line 6582
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDisplayListener onDisplayAdded #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " added."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6584
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSymmetricMode()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6623
    :cond_0
    :goto_0
    return-void

    .line 6588
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v1, :cond_2

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3400()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 6589
    :cond_3
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mDisplayListener Not Extension mode!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6593
    :cond_4
    const/4 v0, 0x0

    .line 6595
    .local v0, "wasPlaying":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isExternalDisplay()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6596
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v1, :cond_5

    .line 6597
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v1

    const/16 v2, 0x321

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->notifyChanged(II)V

    .line 6600
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->checkExceptionalCaseDuringDlna()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 6601
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentResumePosition()J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-eqz v1, :cond_6

    .line 6602
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    .line 6606
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 6607
    const/4 v0, 0x1

    .line 6610
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopMoviPlay4Presentation()V

    .line 6611
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->EnablePresentationMode()V

    .line 6613
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->isWfdConnectedByOtherApp()Z

    move-result v1

    if-nez v1, :cond_a

    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_9

    if-nez v0, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_a

    :cond_9
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 6616
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 6617
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartSecondaryDisplay(Ljava/lang/Boolean;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$7000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 6619
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartSecondaryDisplay(Ljava/lang/Boolean;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$7000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Ljava/lang/Boolean;)V

    .line 6620
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xd2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method public onDisplayChanged(I)V
    .locals 3
    .param p1, "displayId"    # I

    .prologue
    .line 6627
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDisplayListener onDisplayChanged #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " changed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6628
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->dismissSubMenus()V

    .line 6630
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 6631
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->dismissPopupMenu()V

    .line 6635
    :cond_0
    :goto_0
    return-void

    .line 6632
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 6633
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->dismissPopupMenu()V

    goto :goto_0
.end method

.method public onDisplayRemoved(I)V
    .locals 5
    .param p1, "displayId"    # I

    .prologue
    const/4 v4, 0x0

    .line 6639
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDisplayListener onDisplayRemoved #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " removed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6641
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSymmetricMode()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6681
    :cond_0
    :goto_0
    return-void

    .line 6645
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 6646
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mDisplayListener Not Extension mode!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6650
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 6651
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v1, :cond_3

    .line 6652
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v1

    const/16 v2, 0x321

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->notifyChanged(II)V

    .line 6655
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopSecondaryDisplay()V

    .line 6657
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    .line 6659
    .local v0, "util":Lcom/sec/android/app/videoplayer/common/VUtils;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6661
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSelectedbyChangePlayer()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 6662
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mDisplayListener. selected case by change player"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6663
    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSelectedbyChangePlayer(Z)V

    .line 6664
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 6672
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopMoviPlay4Presentation()V

    .line 6673
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 6675
    sget-boolean v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v1, :cond_6

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3400()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 6676
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 6666
    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 6668
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    goto :goto_1

    .line 6678
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xd1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
