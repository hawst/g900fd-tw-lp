.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 3826
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 3828
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3829
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mTrimReceiver - onReceive"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3831
    const-string v3, "android.intent.action.START_VIDEO_FROM_TRIM"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3832
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mTrimReceiver. ACTION_VIDEO_FROM_TRIM"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3833
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 3834
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 3842
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 3836
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 3837
    .local v1, "trimmedFilePath":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTrimReceiver. Trimmed file path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3838
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleSettings()V

    .line 3839
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-wide/16 v4, -0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 3840
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    goto :goto_0
.end method
