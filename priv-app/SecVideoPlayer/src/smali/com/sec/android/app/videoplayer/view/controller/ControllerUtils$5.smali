.class Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;
.super Ljava/lang/Object;
.source "ControllerUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->createTalkBackOffDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 314
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$100(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 315
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "ControllerUtils"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->openPopupPlayer()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$500(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    .line 318
    return-void
.end method
