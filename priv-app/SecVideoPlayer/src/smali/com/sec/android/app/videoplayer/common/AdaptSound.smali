.class public Lcom/sec/android/app/videoplayer/common/AdaptSound;
.super Ljava/lang/Object;
.source "AdaptSound.java"


# static fields
.field public static final ACTION_INTENT_ADAPT_SOUND_CHECKED:Ljava/lang/String; = "com.sec.hearingadjust.checkvideo"

.field public static final ACTION_INTENT_LAUNCH_HEARING_ADJUST:Ljava/lang/String; = "com.sec.hearingadjust.launch"

.field private static final ADAPT_SOUND_EACH_SIDE_RESULT_LENGTH:I = 0x6

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final SETTING_ADAPTSOUND_CHECKED:Ljava/lang/String; = "hearing_videocheck"

.field private static final SETTING_ADAPTSOUND_DIAGNOSIS:Ljava/lang/String; = "hearing_diagnosis"

.field private static final SETTING_ADAPTSOUND_PARAMETERS:Ljava/lang/String; = "hearing_parameters"


# instance fields
.field private mAdaptSound:Landroid/media/audiofx/MySound;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/android/app/videoplayer/common/AdaptSound;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "priority"    # I
    .param p3, "audioSessionId"    # I

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->mContext:Landroid/content/Context;

    .line 104
    new-instance v0, Landroid/media/audiofx/MySound;

    invoke-direct {v0, p2, p3}, Landroid/media/audiofx/MySound;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->mAdaptSound:Landroid/media/audiofx/MySound;

    .line 105
    return-void
.end method

.method public static getAdaptSoundOn(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 194
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "hearing_videocheck"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 197
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private getAnalyzedGain()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 138
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "hearing_parameters"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static isAlreadyDiagnosed(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 174
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 176
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "hearing_diagnosis"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 179
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static setAdaptSoundOn(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bIschecked"    # Z

    .prologue
    const/4 v1, 0x1

    .line 210
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 211
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "hearing_videocheck"

    if-ne p1, v1, :cond_0

    :goto_0
    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 212
    return-void

    .line 211
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateGain()V
    .locals 10

    .prologue
    const/4 v9, 0x6

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->getAnalyzedGain()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "param":Ljava/lang/String;
    new-array v1, v9, [I

    .line 117
    .local v1, "left":[I
    new-array v4, v9, [I

    .line 119
    .local v4, "right":[I
    if-eqz v3, :cond_1

    .line 120
    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 122
    .local v5, "str":[Ljava/lang/String;
    array-length v2, v5

    .line 123
    .local v2, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 124
    if-ge v0, v9, :cond_0

    .line 125
    aget-object v6, v5, v0

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v1, v0

    .line 126
    sget-object v6, Lcom/sec/android/app/videoplayer/common/AdaptSound;->CLASSNAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    aget v8, v1, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    add-int/lit8 v6, v0, -0x6

    aget-object v7, v5, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v4, v6

    .line 129
    sget-object v6, Lcom/sec/android/app/videoplayer/common/AdaptSound;->CLASSNAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v8, v0, -0x6

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 133
    .end local v0    # "i":I
    .end local v2    # "len":I
    .end local v5    # "str":[Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->mAdaptSound:Landroid/media/audiofx/MySound;

    invoke-virtual {v6, v1, v4}, Landroid/media/audiofx/MySound;->setGain([I[I)V

    .line 134
    return-void
.end method


# virtual methods
.method public activate(Z)V
    .locals 1
    .param p1, "isEnable"    # Z

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->updateGain()V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->mAdaptSound:Landroid/media/audiofx/MySound;

    invoke-virtual {v0, p1}, Landroid/media/audiofx/MySound;->setEnabled(Z)I

    .line 152
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/AdaptSound;->mAdaptSound:Landroid/media/audiofx/MySound;

    invoke-virtual {v0}, Landroid/media/audiofx/MySound;->release()V

    .line 162
    return-void
.end method
