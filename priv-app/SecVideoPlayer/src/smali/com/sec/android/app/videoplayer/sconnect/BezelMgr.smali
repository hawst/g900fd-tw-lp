.class public Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;
.super Ljava/lang/Object;
.source "BezelMgr.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private bRegister:Z

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-class v0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->TAG:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 20
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mContext:Landroid/content/Context;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->bRegister:Z

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mContext:Landroid/content/Context;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mContext:Landroid/content/Context;

    const-string v1, "quickconnect"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 31
    new-instance v0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr$1;-><init>(Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/quickconnect/QuickConnectManager;->terminate()V

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 66
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 67
    return-void
.end method

.method public registerListener()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->bRegister:Z

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->TAG:Ljava/lang/String;

    const-string v1, "register Listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method public unregisterListener()V
    .locals 2

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->bRegister:Z

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 56
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->bRegister:Z

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->TAG:Ljava/lang/String;

    const-string v1, "unregister Listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    return-void
.end method
