.class public Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;
.super Ljava/lang/Object;
.source "VideoPlaySpeedPopup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_PLAYSPEED:I = 0x5

.field private static final LONGMINUSMSG:I = 0x2

.field private static final LONGPLUSMSG:I = 0x1

.field public static final LONG_PRESS_DELEY:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "VideoPlaySpeedPopup"

.field private static mIsQuarterSpeedMode:Z

.field private static mMinusButton:Landroid/widget/ImageButton;

.field private static mPlaySpeed:I

.field private static mPlusButton:Landroid/widget/ImageButton;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mMaxPlaySpeed:I

.field private mOnUpdatedListener:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

.field private mPlaySpeedPopup:Landroid/app/AlertDialog;

.field private mPlaySpeedText:Landroid/widget/TextView;

.field private mPlaySpeed_temp:I

.field private mSeekBar:Landroid/widget/SeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mIsQuarterSpeedMode:Z

    .line 39
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    .line 40
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    .line 41
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

    .line 348
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$9;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 410
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$10;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mHandler:Landroid/os/Handler;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setPlaySpeed(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->downPlaySpeed()V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 29
    sput p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->sendMessage(IJ)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->removeMessage(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->changePlaySpeed(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->upPlaySpeed()V

    return-void
.end method

.method private changePlaySpeed(I)Z
    .locals 3
    .param p1, "changeSpeedVal"    # I

    .prologue
    const/4 v0, 0x1

    .line 444
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    if-ge v1, v2, :cond_0

    if-eq p1, v0, :cond_1

    :cond_0
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    if-lez v1, :cond_2

    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 446
    :cond_1
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 451
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private downPlaySpeed()V
    .locals 4

    .prologue
    .line 437
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "downPlaySpeed()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->changePlaySpeed(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    if-lez v0, :cond_0

    .line 439
    const/4 v0, 0x2

    const-wide/16 v2, 0x1f4

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->sendMessage(IJ)V

    .line 441
    :cond_0
    return-void
.end method

.method public static getCurrentSpeed()I
    .locals 1

    .prologue
    .line 74
    sget v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    return v0
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 130
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    .line 131
    return-void
.end method

.method public static isDefaultSpeed()Z
    .locals 2

    .prologue
    .line 82
    sget v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isQuarterSpeedMode()Z
    .locals 1

    .prologue
    .line 93
    sget-boolean v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mIsQuarterSpeedMode:Z

    return v0
.end method

.method private removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 428
    return-void
.end method

.method public static resetPlaySpeed()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "resetPlaySpeed"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    .line 71
    return-void
.end method

.method private sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 421
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 423
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 424
    return-void
.end method

.method private setPlaySpeed(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 97
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    .line 98
    .local v0, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 99
    if-nez p1, :cond_0

    .line 100
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setQuarterPlaySpeed()V

    .line 107
    :goto_0
    return-void

    .line 102
    :cond_0
    add-int/lit8 v1, p1, -0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    goto :goto_0
.end method

.method public static setQuarterSpeedMode(Z)V
    .locals 0
    .param p0, "quarterSpeed"    # Z

    .prologue
    .line 89
    sput-boolean p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mIsQuarterSpeedMode:Z

    .line 90
    return-void
.end method

.method private upPlaySpeed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 431
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "upPlaySpeed()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->changePlaySpeed(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    if-ge v0, v1, :cond_0

    .line 433
    const-wide/16 v0, 0x1f4

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->sendMessage(IJ)V

    .line 434
    :cond_0
    return-void
.end method

.method public static updatePlaySpeed(I)V
    .locals 0
    .param p0, "speed"    # I

    .prologue
    .line 78
    sput p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    .line 79
    return-void
.end method

.method private updatePlaySpeedText(I)V
    .locals 7
    .param p1, "progress"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x3dcccccd    # 0.1f

    .line 110
    const/4 v0, 0x0

    .line 111
    .local v0, "playspeed":F
    const/4 v1, 0x0

    .line 112
    .local v1, "text":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    .line 113
    if-nez p1, :cond_0

    .line 114
    const-string v1, "0.25"

    .line 124
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " X  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00f8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    return-void

    .line 116
    :cond_0
    add-int/lit8 v2, p1, 0x3

    int-to-float v2, v2

    mul-float v0, v2, v4

    .line 117
    const-string v2, "%.1f"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 120
    :cond_1
    add-int/lit8 v2, p1, 0x5

    int-to-float v2, v2

    mul-float v0, v2, v4

    .line 121
    const-string v2, "%.1f"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 59
    :cond_0
    return-void
.end method

.method public isPopupShowing()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 65
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromTouch"    # Z

    .prologue
    .line 399
    const-string v0, "VideoPlaySpeedPopup"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChanged : progress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iput p2, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    .line 402
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->updatePlaySpeedText(I)V

    .line 403
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setPlaySpeed(I)V

    .line 404
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 395
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 407
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "onStopTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    return-void
.end method

.method public setOnUpdatedListener(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

    .prologue
    .line 459
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

    .line 460
    return-void
.end method

.method public showPopup()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 134
    const-string v5, "VideoPlaySpeedPopup"

    const-string v6, "showPopup"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->initialize()V

    .line 136
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 137
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f03002b

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 138
    .local v3, "view":Landroid/view/View;
    sget v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    iput v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I

    .line 140
    sget v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setPlaySpeed(I)V

    .line 142
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 143
    .local v2, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    const v5, 0x7f0a00f8

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 145
    const v5, 0x7f0a0055

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v2, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 163
    const v5, 0x7f0a0026

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v2, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 175
    const v5, 0x7f0d0188

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/SeekBar;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 176
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 177
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMaxPlaySpeed:I

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setMax(I)V

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5, v8}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 179
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    sget v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 181
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 182
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v5, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    iget v5, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 184
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5, v8}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 187
    :cond_0
    const v5, 0x7f0d0186

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedText:Landroid/widget/TextView;

    .line 188
    sget v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->updatePlaySpeedText(I)V

    .line 190
    const v5, 0x7f0d0189

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    sput-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    .line 191
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_1

    .line 192
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a016d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 193
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 194
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 238
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 261
    :cond_1
    const v5, 0x7f0d0187

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    sput-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    .line 262
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_2

    .line 263
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0161

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 265
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$6;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$6;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 310
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$8;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$8;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 333
    :cond_2
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 334
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    .line 336
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v5, :cond_3

    .line 337
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog;->requestWindowFeature(I)Z

    .line 338
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 339
    .local v4, "wm":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x50

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 341
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 345
    .end local v4    # "wm":Landroid/view/WindowManager$LayoutParams;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 346
    return-void
.end method
