.class Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VideoTagBuddySetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContextualListAdapter"
.end annotation


# instance fields
.field entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

.field public individual_item_view:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mContextualList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440
    .local p3, "contextuallist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;>;"
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 436
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    .line 438
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->individual_item_view:Landroid/util/SparseArray;

    .line 441
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    .line 442
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContextualList:Ljava/util/List;

    .line 443
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContextualList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContextualList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 457
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 462
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContextualList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    .line 464
    if-nez p2, :cond_0

    .line 465
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 466
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 469
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v5, 0x7f0d0067

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 471
    .local v4, "tv":Landroid/widget/TextView;
    const-string v5, "Location"

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 472
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f0a009a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    :cond_1
    :goto_0
    const v5, 0x7f0d0068

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 478
    .local v1, "chk_box":Landroid/widget/CheckBox;
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 479
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 480
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 482
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagBuddyShowStatus()Z

    move-result v3

    .line 490
    .local v3, "showTagState":Z
    if-nez v3, :cond_4

    .line 491
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v5

    const v6, 0x3e99999a    # 0.3f

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAlpha(F)V

    .line 492
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 499
    :goto_1
    const/4 v0, 0x0

    .line 501
    .local v0, "check_box":Z
    const-string v5, "Location"

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 502
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getLocationTagShowStatus()Z

    move-result v0

    .line 507
    :cond_2
    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 508
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->individual_item_view:Landroid/util/SparseArray;

    invoke-virtual {v5, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 509
    return-object p2

    .line 473
    .end local v0    # "check_box":Z
    .end local v1    # "chk_box":Landroid/widget/CheckBox;
    .end local v3    # "showTagState":Z
    :cond_3
    const-string v5, "Date"

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 474
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f0a012b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 494
    .restart local v1    # "chk_box":Landroid/widget/CheckBox;
    .restart local v3    # "showTagState":Z
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ListView;->setAlpha(F)V

    .line 495
    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 496
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_1

    .line 503
    .restart local v0    # "check_box":Z
    :cond_5
    const-string v5, "Date"

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->entry:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 504
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getDateTagShowStatus()Z

    move-result v0

    goto :goto_2
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->notifyDataSetChanged()V

    .line 515
    return-void
.end method
