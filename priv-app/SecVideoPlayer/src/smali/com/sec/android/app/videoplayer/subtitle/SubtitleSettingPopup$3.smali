.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;
.super Ljava/lang/Object;
.source "SubtitleSettingPopup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 605
    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 607
    if-eqz p2, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSMPTETTCustomMode(Z)V

    .line 613
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    .line 619
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refresh()V

    .line 620
    return-void

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSMPTETTCustomMode(Z)V

    goto :goto_0

    .line 616
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    goto :goto_1
.end method
