.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 835
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v7, 0xbb8

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 837
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 838
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 873
    :cond_0
    :goto_0
    return v6

    .line 840
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const v3, 0x36ee80

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 844
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 848
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 850
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 851
    .local v0, "pressTime":J
    const-wide/16 v2, 0x1f4

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 852
    invoke-virtual {p1, v6}, Landroid/view/View;->playSoundEffect(I)V

    .line 853
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupMenu()V

    goto :goto_0

    .line 857
    .end local v0    # "pressTime":J
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 861
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 862
    :cond_2
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 864
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 838
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
