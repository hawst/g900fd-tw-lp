.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ChangePlayerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$1;,
        Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;",
        ">;"
    }
.end annotation


# static fields
.field private static DEVICE_NAME:Ljava/lang/String;


# instance fields
.field private deviceName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDmrDefaultIcon:Landroid/graphics/drawable/Drawable;

.field private mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

.field private mIconSize:I

.field private vi:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "device_name"

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->DEVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;>;"
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mContext:Landroid/content/Context;

    .line 37
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->vi:Landroid/view/LayoutInflater;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->DEVICE_NAME:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->deviceName:Ljava/lang/String;

    .line 39
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020085

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrDefaultIcon:Landroid/graphics/drawable/Drawable;

    .line 40
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mIconSize:I

    .line 43
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;->getOrCreateRetainableCache()Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;

    move-result-object v0

    .line 44
    .local v0, "c":Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;
    iget-object v3, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;->mRetainedCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    .line 46
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    if-nez v3, :cond_0

    .line 47
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mContext:Landroid/content/Context;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    invoke-virtual {v3}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    .line 48
    .local v1, "memClass":I
    const/high16 v3, 0x100000

    mul-int/2addr v3, v1

    div-int/lit8 v2, v3, 0x8

    .line 51
    .local v2, "size":I
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    invoke-direct {v3, v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    .line 52
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    iput-object v3, v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;->mRetainedCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    .line 54
    .end local v1    # "memClass":I
    .end local v2    # "size":I
    :cond_0
    return-void
.end method


# virtual methods
.method public clearDeviceIcon()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->clear()V

    .line 131
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0a0031

    .line 58
    move-object v11, p2

    .line 59
    .local v11, "v":Landroid/view/View;
    const/4 v10, 0x0

    .line 60
    .local v10, "holder":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;
    const/4 v8, 0x0

    .line 61
    .local v8, "deviceIcon":Landroid/graphics/drawable/Drawable;
    if-nez v11, :cond_3

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->vi:Landroid/view/LayoutInflater;

    const v1, 0x7f03001e

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 63
    new-instance v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;

    .end local v10    # "holder":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;
    invoke-direct {v10, v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$1;)V

    .line 64
    .restart local v10    # "holder":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;
    const v0, 0x7f0d0124

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->thubmail:Landroid/widget/ImageView;

    .line 65
    const v0, 0x7f0d0125

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrName:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0d0126

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrDescription:Landroid/widget/TextView;

    .line 67
    invoke-virtual {v11, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 72
    :goto_0
    const v0, 0x7f0d0127

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckedTextView;

    .line 73
    .local v7, "checkIcon":Landroid/widget/CheckedTextView;
    if-nez p1, :cond_4

    .line 74
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 79
    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    .line 81
    .local v9, "deviceItem":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;
    if-eqz v9, :cond_2

    .line 82
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrName:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrName:Landroid/widget/TextView;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :cond_0
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrDescription:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 98
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 103
    :cond_1
    :goto_2
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->thubmail:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 104
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceType()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_5

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrImageCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceIconUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->thubmail:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrDefaultIcon:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mIconSize:I

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->loadBitmap(Landroid/content/Context;Landroid/net/Uri;Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;ILandroid/widget/BaseAdapter;)V

    .line 118
    :cond_2
    :goto_3
    return-object v11

    .line 69
    .end local v7    # "checkIcon":Landroid/widget/CheckedTextView;
    .end local v9    # "deviceItem":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;
    :cond_3
    invoke-virtual {v11}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "holder":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;
    check-cast v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;

    .restart local v10    # "holder":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;
    goto :goto_0

    .line 76
    .restart local v7    # "checkIcon":Landroid/widget/CheckedTextView;
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 89
    .restart local v9    # "deviceItem":Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;
    :pswitch_0
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrDescription:Landroid/widget/TextView;

    const v1, 0x7f0a002f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 92
    :pswitch_1
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 95
    :pswitch_2
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->dmrDescription:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 107
    :cond_5
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 108
    if-eqz v8, :cond_6

    .line 109
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->thubmail:Landroid/widget/ImageView;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 111
    :cond_6
    iget-object v0, v10, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter$ViewHolder;->thubmail:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->mDmrDefaultIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
