.class public final enum Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;
.super Ljava/lang/Enum;
.source "VideoTitleController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LayoutMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

.field public static final enum ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

.field public static final enum NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

.field public static final enum NORMAL:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 205
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    const-string v1, "ALL_SHARE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NORMAL:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    .line 204
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NORMAL:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->$VALUES:[Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 204
    const-class v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->$VALUES:[Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    return-object v0
.end method
