.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;
.super Landroid/os/Handler;
.source "VideoTitleController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 2160
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x1

    .line 2162
    iget v7, p1, Landroid/os/Message;->what:I

    sparse-switch v7, :sswitch_data_0

    .line 2297
    :cond_0
    :goto_0
    return-void

    .line 2164
    :sswitch_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    goto :goto_0

    .line 2168
    :sswitch_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2169
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->captureVideo()V

    goto :goto_0

    .line 2175
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->callWatchOnClient()V

    .line 2176
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 2180
    :sswitch_3
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v6

    .line 2181
    .local v6, "mPrefMgr":Lcom/sec/android/app/videoplayer/db/SharedPreference;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v5

    .line 2183
    .local v5, "mDrmUtil":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    const-string v7, "showwifipopup_changeplayer"

    invoke-virtual {v6, v7, v9}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2184
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 2185
    .local v3, "inflate":Landroid/view/LayoutInflater;
    const v7, 0x7f030004

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2187
    .local v1, "PopupView":Landroid/view/View;
    const v7, 0x7f0d005a

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2188
    .local v0, "Checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2189
    new-instance v7, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$1;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2222
    const v7, 0x7f0d005b

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2223
    .local v4, "mCheckText":Landroid/widget/TextView;
    new-instance v7, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$2;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2256
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2257
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a003b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2258
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2260
    const v7, 0x7f0a00dd

    new-instance v8, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;

    invoke-direct {v8, p0, v0, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;Landroid/widget/CheckBox;Lcom/sec/android/app/videoplayer/db/SharedPreference;)V

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2275
    const v7, 0x7f0a0026

    new-instance v8, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;)V

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2281
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mConnectionCheckPopup:Landroid/app/AlertDialog;
    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1302(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 2282
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mConnectionCheckPopup:Landroid/app/AlertDialog;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/app/AlertDialog;

    move-result-object v7

    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2283
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mConnectionCheckPopup:Landroid/app/AlertDialog;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/app/AlertDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 2286
    .end local v0    # "Checkbox":Landroid/widget/CheckBox;
    .end local v1    # "PopupView":Landroid/view/View;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "inflate":Landroid/view/LayoutInflater;
    .end local v4    # "mCheckText":Landroid/widget/TextView;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 2288
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v8

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1202(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 2289
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->setContext(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 2290
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->createDevicePopup()V

    goto/16 :goto_0

    .line 2162
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0xa -> :sswitch_0
        0x20 -> :sswitch_2
    .end sparse-switch
.end method
