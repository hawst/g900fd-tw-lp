.class public Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
.super Landroid/app/Dialog;
.source "SideChapterViewDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;
    }
.end annotation


# static fields
.field private static final CHAPTER_FADE_OUT:I = 0xa

.field protected static final CHAPTER_TIME_OUT:I = 0xbb8

.field public static final TAG:Ljava/lang/String; = "videowall-SideChapterViewDialog"

.field private static llayoutIdList:[I

.field private static nochapIdList:[I

.field private static progressIdList:[I

.field private static ttsStringList:[I

.field private static viewIdList:[I


# instance fields
.field private atExtSdCard:Z

.field private displayHeight:I

.field private displayWidth:I

.field private duration:I

.field private fps:I

.field private height:I

.field private keyframe_time:[I

.field private mBeforePlayed:Z

.field private mChapterResumePos:J

.field private mContext:Landroid/content/Context;

.field private final transient mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private final mHandler:Landroid/os/Handler;

.field private mHandler2:Landroid/os/Handler;

.field private mHandler3:Landroid/os/Handler;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mLabelview:[Landroid/widget/ImageView;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mStop:I

.field private mVideoid:J

.field private mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

.field private mchapterbit:[Landroid/graphics/Bitmap;

.field private mchtimeview:[Landroid/widget/TextView;

.field private mfileManager:Lcom/sec/android/videowall/FileMgr;

.field private mll:[Landroid/widget/LinearLayout;

.field private mnochapview:[Landroid/widget/ImageView;

.field private mprogbar:[Landroid/widget/ProgressBar;

.field private num:I

.field private position:I

.field private realTranscode:Ljava/lang/Thread;

.field private sFilePath:Ljava/lang/String;

.field private sName:Ljava/lang/String;

.field private sPath:Ljava/lang/String;

.field private trd:I

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 94
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I

    .line 99
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->progressIdList:[I

    .line 104
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->nochapIdList:[I

    .line 109
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->llayoutIdList:[I

    .line 114
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->ttsStringList:[I

    return-void

    .line 94
    :array_0
    .array-data 4
        0x7f0d00a0
        0x7f0d00a6
        0x7f0d00ac
        0x7f0d00b2
        0x7f0d00b8
        0x7f0d00be
        0x7f0d00c4
        0x7f0d00ca
        0x7f0d00d0
        0x7f0d00d6
        0x7f0d00dc
        0x7f0d00e2
    .end array-data

    .line 99
    :array_1
    .array-data 4
        0x7f0d00a3
        0x7f0d00a9
        0x7f0d00af
        0x7f0d00b5
        0x7f0d00bb
        0x7f0d00c1
        0x7f0d00c7
        0x7f0d00cd
        0x7f0d00d3
        0x7f0d00d9
        0x7f0d00df
        0x7f0d00e5
    .end array-data

    .line 104
    :array_2
    .array-data 4
        0x7f0d00a2
        0x7f0d00a7
        0x7f0d00ad
        0x7f0d00b3
        0x7f0d00b9
        0x7f0d00bf
        0x7f0d00c5
        0x7f0d00cb
        0x7f0d00d1
        0x7f0d00d7
        0x7f0d00dd
        0x7f0d00e3
    .end array-data

    .line 109
    :array_3
    .array-data 4
        0x7f0d009e
        0x7f0d00a4
        0x7f0d00aa
        0x7f0d00b0
        0x7f0d00b6
        0x7f0d00bc
        0x7f0d00c2
        0x7f0d00c8
        0x7f0d00ce
        0x7f0d00d4
        0x7f0d00da
        0x7f0d00e0
    .end array-data

    .line 114
    :array_4
    .array-data 4
        0x7f0a0150
        0x7f0a0154
        0x7f0a0155
        0x7f0a0156
        0x7f0a0157
        0x7f0a0158
        0x7f0a0159
        0x7f0a015a
        0x7f0a015b
        0x7f0a0151
        0x7f0a0152
        0x7f0a0153
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 120
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    .line 47
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 49
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mBeforePlayed:Z

    .line 51
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mChapterResumePos:J

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    .line 57
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    .line 59
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mLabelview:[Landroid/widget/ImageView;

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchtimeview:[Landroid/widget/TextView;

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I

    .line 64
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;

    .line 66
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    .line 72
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->realTranscode:Ljava/lang/Thread;

    .line 82
    iput v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I

    .line 84
    iput v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    .line 88
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->atExtSdCard:Z

    .line 462
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$5;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler2:Landroid/os/Handler;

    .line 469
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$6;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler3:Landroid/os/Handler;

    .line 550
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 640
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$8;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 652
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$9;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    .line 121
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    .line 122
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->initdialog()V

    .line 124
    return-void
.end method

.method private CreateLiveThumbnail(Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 15
    .param p1, "Path"    # Ljava/lang/String;
    .param p2, "Name"    # Ljava/lang/String;
    .param p3, "ID"    # J
    .param p5, "thumbTime"    # I
    .param p6, "time_interval"    # I

    .prologue
    .line 477
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 478
    .local v8, "start":J
    const-string v10, "videowall-SideChapterViewDialog"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p3

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : chapter transcode Start!!"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    iget v10, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    new-array v7, v10, [Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;

    .line 481
    .local v7, "realMultipleTranscode":[Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v10, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    iget v11, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    div-int/2addr v10, v11

    if-ge v3, v10, :cond_3

    iget v10, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    if-nez v10, :cond_3

    .line 483
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    iget v10, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    if-ge v6, v10, :cond_0

    .line 484
    new-instance v10, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    invoke-direct {v10, p0, v11}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;Landroid/content/Context;)V

    aput-object v10, v7, v6

    .line 485
    aget-object v10, v7, v6

    iget v11, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    mul-int/2addr v11, v3

    add-int/2addr v11, v6

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v10, v11, v0, v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->setArg(III)V

    .line 486
    aget-object v10, v7, v6

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->setPriority(I)V

    .line 487
    aget-object v10, v7, v6

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->start()V

    .line 483
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 490
    :cond_0
    const/4 v6, 0x0

    :goto_2
    iget v10, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    if-ge v6, v10, :cond_2

    .line 492
    aget-object v10, v7, v6

    if-eqz v10, :cond_1

    .line 494
    :try_start_0
    aget-object v10, v7, v6

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 498
    :goto_3
    const-string v10, "videowall-SideChapterViewDialog"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "End Transcoding !! :: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 495
    :catch_0
    move-exception v2

    .line 496
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v10, "videowall-SideChapterViewDialog"

    const-string v11, "interrupted exception !!"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 481
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 502
    .end local v6    # "j":I
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 503
    .local v4, "end":J
    const-string v10, "videowall-SideChapterViewDialog"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p3

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : chapter transcode End!! :: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sub-long v12, v4, v8

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ms"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    return-void
.end method

.method private CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIII)V
    .locals 15
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "thischapter"    # I
    .param p6, "durthumbtime"    # I
    .param p7, "time_interval"    # I

    .prologue
    .line 530
    const-string v2, "videowall-SideChapterViewDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CreateLiveThumbnailofThisChapter - chapter [ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v2

    if-nez v2, :cond_0

    .line 534
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v3

    .line 535
    .local v3, "tumbFileName":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I

    mul-int v2, v2, p5

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    div-int v4, v2, v4

    iget v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I

    iget v8, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I

    iget v9, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I

    const/4 v11, 0x0

    move-object/from16 v2, p1

    move/from16 v5, p5

    move/from16 v10, p6

    move/from16 v12, p7

    invoke-static/range {v2 .. v12}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I

    move-result v13

    .line 536
    .local v13, "res":I
    const/4 v2, 0x1

    if-ne v13, v2, :cond_1

    .line 537
    const-string v2, "videowall-SideChapterViewDialog"

    const-string v4, "transcode success!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    move/from16 v0, p5

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setThumbnail(ILjava/lang/String;)I

    move-result v13

    .line 539
    const-string v2, "videowall-SideChapterViewDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "set Chapter : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    const/4 v2, 0x1

    if-ne v13, v2, :cond_0

    .line 541
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v2, v2, p5

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I

    iget v5, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I

    iget v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I

    move/from16 v0, p5

    invoke-virtual {v2, v0, v4, v5, v6}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setBitmapView(IIII)V

    .line 548
    .end local v3    # "tumbFileName":Ljava/lang/String;
    .end local v13    # "res":I
    :cond_0
    :goto_0
    return-void

    .line 544
    .restart local v3    # "tumbFileName":Ljava/lang/String;
    .restart local v13    # "res":I
    :cond_1
    const-string v2, "videowall-SideChapterViewDialog"

    const-string v4, "transcode fail!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/videowall/FileMgr;->deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V

    goto :goto_0
.end method

.method private ResetHideTimer()V
    .locals 4

    .prologue
    const/16 v1, 0xa

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 650
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->ResetHideTimer()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Lcom/sec/android/videowall/FileMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler2:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    return-object v0
.end method

.method static synthetic access$200()[I
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler3:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J
    .param p5, "x4"    # I
    .param p6, "x5"    # I

    .prologue
    .line 42
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->CreateLiveThumbnail(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;Ljava/lang/String;Ljava/lang/String;JIII)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J
    .param p5, "x4"    # I
    .param p6, "x5"    # I
    .param p7, "x6"    # I

    .prologue
    .line 42
    invoke-direct/range {p0 .. p7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIII)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->callMoviePlayerByTime(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    return-object v0
.end method

.method private callMoviePlayerByTime(I)V
    .locals 2
    .param p1, "sec"    # I

    .prologue
    .line 305
    mul-int/lit16 v0, p1, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mChapterResumePos:J

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    .line 308
    return-void
.end method

.method private initdialog()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x2

    .line 127
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->requestWindowFeature(I)Z

    .line 129
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 130
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 132
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 133
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 136
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 137
    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 138
    const/16 v2, 0x20

    const/high16 v3, 0x40000

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 139
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setCanceledOnTouchOutside(Z)V

    .line 140
    return-void
.end method

.method private initvideowallSetting()V
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    .line 196
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailDisplayWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->displayWidth:I

    .line 197
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailDisplayHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->displayHeight:I

    .line 199
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I

    .line 200
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I

    .line 202
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getThumbnailFps()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I

    .line 203
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterTranscodeThread()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    .line 206
    :goto_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 207
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->trd:I

    goto :goto_0

    .line 209
    :cond_0
    return-void
.end method

.method private onStopResume()V
    .locals 4

    .prologue
    .line 311
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mChapterResumePos:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mChapterResumePos:J

    long-to-int v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 320
    :cond_0
    :goto_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mChapterResumePos:J

    .line 321
    return-void

    .line 315
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mBeforePlayed:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isActivityPauseState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 316
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mBeforePlayed:Z

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    goto :goto_0
.end method

.method private setView(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    .line 212
    const-string v3, "videowall-SideChapterViewDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setView : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 215
    .local v2, "resource":Landroid/content/res/Resources;
    const v3, 0x7f0201eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 217
    .local v0, "bd":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;

    sget-object v3, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->llayoutIdList:[I

    aget v3, v3, p1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, p1

    .line 219
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    sget-object v3, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I

    aget v3, v3, p1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aput-object v3, v4, p1

    .line 220
    const/4 v1, 0x0

    .line 222
    .local v1, "params":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .end local v1    # "params":Landroid/widget/FrameLayout$LayoutParams;
    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->displayWidth:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->displayHeight:I

    add-int/lit8 v4, v4, -0x1

    const/16 v5, 0x11

    invoke-direct {v1, v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 224
    .restart local v1    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v3, v3, p1

    invoke-virtual {v3, v1}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v3, v3, p1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->ttsStringList:[I

    aget v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v3, v3, p1

    invoke-virtual {v3, v7}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVisibility(I)V

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v3, v3, p1

    new-instance v4, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v3, v3, p1

    new-instance v4, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    sget-object v3, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->progressIdList:[I

    aget v3, v3, p1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    aput-object v3, v4, p1

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    aget-object v3, v3, p1

    invoke-virtual {v3, v1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    aget-object v3, v3, p1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    sget-object v3, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->nochapIdList:[I

    aget v3, v3, p1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, p1

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    aget-object v3, v3, p1

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    aget-object v3, v3, p1

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    aget-object v3, v3, p1

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 302
    return-void
.end method


# virtual methods
.method public chapterviewPause()V
    .locals 4

    .prologue
    .line 437
    const-string v2, "videowall-SideChapterViewDialog"

    const-string v3, "chapterviewPause()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->realTranscode:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 439
    const-string v2, "videowall-SideChapterViewDialog"

    const-string v3, "Thread isAlive"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    .line 441
    iget v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_stopTranscoding(I)I

    .line 444
    :try_start_0
    const-string v2, "videowall-SideChapterViewDialog"

    const-string v3, "realTranscode.join();"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :cond_0
    :goto_0
    const-string v2, "videowall-SideChapterViewDialog"

    const-string v3, "stop thread"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_close()I

    .line 453
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    if-ge v1, v2, :cond_2

    .line 454
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v1

    if-eqz v2, :cond_1

    .line 455
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pauseView()V

    .line 456
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 457
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 453
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 446
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 447
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 460
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "i":I
    :cond_2
    return-void
.end method

.method public chapterviewResume()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 333
    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    .line 334
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_create()V

    .line 335
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_open(Z)I

    .line 337
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getDurationTime(Ljava/lang/String;Z)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I

    .line 338
    const-string v1, "videowall-SideChapterViewDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duration time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I

    if-gez v1, :cond_0

    .line 341
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a0093

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    .line 345
    :cond_0
    const v1, 0x7f030012

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setContentView(I)V

    .line 347
    const v1, 0x7f0d009d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    new-instance v2, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$3;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    if-nez v1, :cond_1

    .line 359
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    .line 360
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    if-nez v1, :cond_2

    .line 361
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mprogbar:[Landroid/widget/ProgressBar;

    .line 362
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    if-nez v1, :cond_3

    .line 363
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mnochapview:[Landroid/widget/ImageView;

    .line 364
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;

    if-nez v1, :cond_4

    .line 365
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;

    .line 366
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mLabelview:[Landroid/widget/ImageView;

    if-nez v1, :cond_5

    .line 367
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mLabelview:[Landroid/widget/ImageView;

    .line 368
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchtimeview:[Landroid/widget/TextView;

    if-nez v1, :cond_6

    .line 369
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchtimeview:[Landroid/widget/TextView;

    .line 370
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_7

    .line 371
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;

    .line 372
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I

    if-nez v1, :cond_8

    .line 373
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I

    .line 375
    :cond_8
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I

    if-ge v0, v1, :cond_9

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I

    if-nez v1, :cond_9

    .line 376
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setView(I)V

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 379
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 381
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;-><init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->realTranscode:Ljava/lang/Thread;

    .line 433
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 434
    return-void
.end method

.method public initDatas(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 149
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    .line 150
    new-instance v0, Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/videowall/FileMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    .line 152
    const-string v0, "videowall-SideChapterViewDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "videowall-SideChapterViewDialog"

    const-string v1, "run videoplayer : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/videowall/FileMgr;->getPathFileNameByVideoid(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/videowall/FileMgr;->getFileNameByVideoid(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sFilePath:Ljava/lang/String;

    .line 161
    const-string v0, "videowall-SideChapterViewDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sName : [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/videowall/FileMgr;->checkExtSdCardFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->atExtSdCard:Z

    .line 167
    const-string v0, "videowall-SideChapterViewDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "atExtSdCard : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->atExtSdCard:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->initvideowallSetting()V

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->chapterviewResume()V

    .line 173
    return-void
.end method

.method public initDatasForHelpMotionPeek(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 177
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    .line 178
    new-instance v0, Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/videowall/FileMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    .line 180
    const-string v0, "videowall-SideChapterViewDialog"

    const-string v1, "initDatasForHelpPeek"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    .line 184
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    .line 185
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sFilePath:Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 190
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->initvideowallSetting()V

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->chapterviewResume()V

    .line 192
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 324
    const-string v0, "videowall-SideChapterViewDialog"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->onStopResume()V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 328
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    .line 329
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 330
    return-void
.end method
