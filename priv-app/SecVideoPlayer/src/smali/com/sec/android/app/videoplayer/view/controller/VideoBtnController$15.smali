.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1321
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 1334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setInvisibleAllViews()V

    .line 1335
    const-string v0, "VideoBtnController"

    const-string v1, "hide animation end"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1329
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1324
    const-string v0, "VideoBtnController"

    const-string v1, "hide animation start"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutState:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1002(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z

    .line 1326
    return-void
.end method
