.class Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 380
    :goto_0
    return-void

    .line 370
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
