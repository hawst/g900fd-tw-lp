.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 885
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/drm/DrmManagerClient;Landroid/drm/DrmErrorEvent;)V
    .locals 5
    .param p1, "client"    # Landroid/drm/DrmManagerClient;
    .param p2, "event"    # Landroid/drm/DrmErrorEvent;

    .prologue
    const/16 v4, 0x6d

    .line 887
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDrmErrorListeneer event.getType(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 906
    const-string v1, "MoviePlaybackService"

    const-string v2, "mDrmErrorListeneer Unknown Error"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 910
    :goto_0
    return-void

    .line 896
    :pswitch_0
    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, "url":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 898
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDrmErrorListener. event.getMessage() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(ILjava/lang/String;)V
    invoke-static {v1, v4, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;ILjava/lang/String;)V

    goto :goto_0

    .line 901
    :cond_0
    const-string v1, "MoviePlaybackService"

    const-string v2, "mDrmErrorListener. event.getMessage() : is null "

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 888
    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
