.class Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;
.super Ljava/lang/Object;
.source "PreviewManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/preview/PreviewManager;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/preview/PreviewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->access$000(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->access$000(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->getResumePos()I

    move-result v0

    .line 40
    .local v0, "pos":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x384

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->access$002(Lcom/sec/android/app/videoplayer/preview/PreviewManager;Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .line 42
    return-void

    .line 39
    .end local v0    # "pos":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
