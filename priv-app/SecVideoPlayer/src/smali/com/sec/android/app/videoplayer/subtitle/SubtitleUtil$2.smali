.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;
.super Landroid/database/ContentObserver;
.source "SubtitleUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1277
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 1280
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1281
    .local v1, "uriPath":Ljava/lang/String;
    const/16 v3, 0x2f

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1283
    .local v0, "name":Ljava/lang/String;
    const-string v3, "accessibility_captioning_enabled"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1284
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mCaptioningObserver E. captioning enabled value is changed!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->isCaptionEnable()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)I

    move-result v3

    const/16 v4, 0x69

    if-ne v3, v4, :cond_0

    .line 1289
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1290
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setScreenMode(I)V

    .line 1301
    :cond_0
    :goto_0
    return-void

    .line 1293
    :cond_1
    const-string v3, "accessibility_sec_captioning_enabled"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1294
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mCaptioningObserver E. samsung captioning enabled value is changed!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1296
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1297
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->initValues()V

    .line 1298
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSubtitleActivation()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method
