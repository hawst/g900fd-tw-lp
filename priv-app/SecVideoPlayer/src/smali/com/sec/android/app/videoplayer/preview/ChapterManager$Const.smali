.class interface abstract Lcom/sec/android/app/videoplayer/preview/ChapterManager$Const;
.super Ljava/lang/Object;
.source "ChapterManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/preview/ChapterManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "Const"
.end annotation


# static fields
.field public static final CHAPTER:I = 0x0

.field public static final DURATION_MIN:I = 0x3c

.field public static final INTERVAL_CHAPTER:I = 0x5

.field public static final NOCHAPTER:I = 0x1

.field public static final SHOW_LIVETHUMBNAIL:I = 0x3

.field public static final SHOW_THUMBNAIL:I = 0x4

.field public static final STOP:I = 0x1

.field public static final SUCCESS:I = 0x1

.field public static final UPDATE_CHAPTERTIME:I = 0x1

.field public static final UPDATE_CHAPTERVIEW:I
