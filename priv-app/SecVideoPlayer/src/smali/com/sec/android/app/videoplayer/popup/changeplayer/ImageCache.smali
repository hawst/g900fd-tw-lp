.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String; = null

.field private static final userAgent:Ljava/lang/String; = "videoplayer-image-updater"


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mBitmapCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDmrIconSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$1;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mBitmapCache:Landroid/util/LruCache;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mCurrentTasks:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mCurrentTasks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mDmrIconSize:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->getBitmapFromUrl(Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)Landroid/widget/BaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mAdapter:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method private addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->getBitmapFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_0
    return-void
.end method

.method private getBitmapFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private getBitmapFromUrl(Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 17
    .param p1, "src"    # Landroid/net/Uri;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 121
    sget-object v14, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getBitmapFromUrl. "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const/4 v2, 0x0

    .line 123
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 124
    .local v10, "inputStream":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 125
    .local v5, "entity":Lorg/apache/http/HttpEntity;
    const/4 v3, 0x0

    .line 126
    .local v3, "client":Landroid/net/http/AndroidHttpClient;
    const/4 v6, 0x0

    .line 129
    .local v6, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    if-eqz p1, :cond_4

    .line 130
    :try_start_0
    const-string v14, "videoplayer-image-updater"

    invoke-static {v14}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    .line 131
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v7, v14}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v7, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v8

    .line 134
    .local v8, "host":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPort()I

    move-result v11

    .line 136
    .local v11, "port":I
    new-instance v12, Lorg/apache/http/HttpHost;

    invoke-direct {v12, v8, v11}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 137
    .local v12, "proxy":Lorg/apache/http/HttpHost;
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v14

    const-string v15, "http.route.default-proxy"

    invoke-interface {v14, v15, v12}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 139
    invoke-virtual {v3, v7}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 141
    .local v13, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    const/16 v15, 0xc8

    if-eq v14, v15, :cond_3

    .line 142
    sget-object v14, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " while retrieving bitmap from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 143
    const/4 v14, 0x0

    .line 162
    if-eqz v5, :cond_0

    .line 163
    :try_start_2
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 166
    :cond_0
    if-eqz v3, :cond_1

    .line 167
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 170
    :cond_1
    if-eqz v10, :cond_2

    .line 171
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_0
    move-object v6, v7

    .line 178
    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "host":Ljava/lang/String;
    .end local v11    # "port":I
    .end local v12    # "proxy":Lorg/apache/http/HttpHost;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :goto_1
    return-object v14

    .line 173
    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v8    # "host":Ljava/lang/String;
    .restart local v11    # "port":I
    .restart local v12    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v4

    .line 174
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 146
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_3
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 148
    if-eqz v5, :cond_e

    .line 149
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v10

    .line 151
    invoke-static {v10}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 152
    .local v9, "icon":Landroid/graphics/Bitmap;
    const/4 v14, 0x1

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v9, v0, v1, v14}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    move-object v6, v7

    .line 162
    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "host":Ljava/lang/String;
    .end local v9    # "icon":Landroid/graphics/Bitmap;
    .end local v11    # "port":I
    .end local v12    # "proxy":Lorg/apache/http/HttpHost;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :cond_4
    :goto_2
    if-eqz v5, :cond_5

    .line 163
    :try_start_4
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 166
    :cond_5
    if-eqz v3, :cond_6

    .line 167
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 170
    :cond_6
    if-eqz v10, :cond_7

    .line 171
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_7
    :goto_3
    move-object v14, v2

    .line 178
    goto :goto_1

    .line 173
    :catch_1
    move-exception v4

    .line 174
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 155
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v4

    .line 156
    .restart local v4    # "e":Ljava/lang/Exception;
    :goto_4
    if-eqz v6, :cond_8

    .line 157
    :try_start_5
    invoke-virtual {v6}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 159
    :cond_8
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 162
    if-eqz v5, :cond_9

    .line 163
    :try_start_6
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 166
    :cond_9
    if-eqz v3, :cond_a

    .line 167
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 170
    :cond_a
    if-eqz v10, :cond_7

    .line 171
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_3

    .line 173
    :catch_3
    move-exception v4

    .line 174
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 161
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    .line 162
    :goto_5
    if-eqz v5, :cond_b

    .line 163
    :try_start_7
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 166
    :cond_b
    if-eqz v3, :cond_c

    .line 167
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 170
    :cond_c
    if-eqz v10, :cond_d

    .line 171
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 175
    :cond_d
    :goto_6
    throw v14

    .line 173
    :catch_4
    move-exception v4

    .line 174
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 161
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catchall_1
    move-exception v14

    move-object v6, v7

    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_5

    .line 155
    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catch_5
    move-exception v4

    move-object v6, v7

    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_4

    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v8    # "host":Ljava/lang/String;
    .restart local v11    # "port":I
    .restart local v12    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    :cond_e
    move-object v6, v7

    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_2
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 47
    return-void
.end method

.method public loadBitmap(Landroid/content/Context;Landroid/net/Uri;Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;ILandroid/widget/BaseAdapter;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "src"    # Landroid/net/Uri;
    .param p3, "imageView"    # Landroid/widget/ImageView;
    .param p4, "defaultImage"    # Landroid/graphics/drawable/Drawable;
    .param p5, "size"    # I
    .param p6, "adapter"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 64
    if-nez p2, :cond_1

    .line 65
    invoke-virtual {p3, p4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "imageKey":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->getBitmapFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 73
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 74
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 76
    :cond_2
    iput-object p6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mAdapter:Landroid/widget/BaseAdapter;

    .line 77
    invoke-virtual {p3, p4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mCurrentTasks:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 79
    iput p5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mDmrIconSize:I

    .line 80
    new-instance v2, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;Ljava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
