.class Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;
.super Ljava/lang/Object;
.source "VideoDrmPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->createDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 158
    sparse-switch p2, :sswitch_data_0

    .line 172
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    .line 161
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "createDialog - KEYCODE_POWER_ACTION_UP"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 168
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 158
    nop

    :sswitch_data_0
    .sparse-switch
        0x1a -> :sswitch_0
        0x54 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
