.class public Lcom/sec/android/app/videoplayer/Video360/Shader;
.super Ljava/lang/Object;
.source "Shader.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Shader"

.field private static final fragmentShaderExternalCode:Ljava/lang/String; = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;uniform samplerExternalOES uTexture;varying vec2 vTexCoord;void main() {  gl_FragColor = texture2D(uTexture, vTexCoord);}"

.field private static final threesixtyPlayerVertexShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform mat4 uMVPMatrix;uniform mat4 uTextureMat;attribute vec3 aPosition;attribute vec2 aTexCoord;\nvarying vec2 vTexCoord;void main() {  gl_Position = uMVPMatrix * vec4(aPosition, 1.0);  vec4 texCoords = uTextureMat * vec4(aTexCoord.s, 1.0 - aTexCoord.t, 0.0, 1.0);  vTexCoord = texCoords.st;}"


# instance fields
.field public m360PlayerClearProgram:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Shader;->m360PlayerClearProgram:I

    return-void
.end method

.method public static checkGlError(Ljava/lang/String;)V
    .locals 4
    .param p0, "op"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .local v0, "error":I
    if-eqz v0, :cond_0

    .line 133
    const-string v1, "Shader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_0
    return-void
.end method

.method public static createProgram(II)I
    .locals 5
    .param p0, "vertShadrHndl"    # I
    .param p1, "fragShadrHndl"    # I

    .prologue
    const/4 v3, 0x0

    .line 105
    const/4 v1, 0x0

    .line 106
    .local v1, "progHndl":I
    if-lez p0, :cond_0

    if-lez p1, :cond_0

    .line 108
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v1

    .line 109
    if-eqz v1, :cond_0

    .line 111
    invoke-static {v1, p0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 113
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 115
    invoke-static {v1}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 117
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 118
    .local v0, "linkStatus":[I
    const v2, 0x8b82

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 120
    aget v2, v0, v3

    if-nez v2, :cond_0

    .line 121
    const-string v2, "Shader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error compiling program: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 123
    const/4 v1, 0x0

    .line 127
    .end local v0    # "linkStatus":[I
    :cond_0
    return v1
.end method

.method public static createProgram(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p0, "vertShadrSrc"    # Ljava/lang/String;
    .param p1, "fragShadrSrc"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 69
    const/4 v2, 0x0

    .line 71
    .local v2, "progHndl":I
    const v4, 0x8b31

    invoke-static {v4, p0}, Lcom/sec/android/app/videoplayer/Video360/Shader;->loadShader(ILjava/lang/String;)I

    move-result v3

    .line 73
    .local v3, "vertShadrHndl":I
    const v4, 0x8b30

    invoke-static {v4, p1}, Lcom/sec/android/app/videoplayer/Video360/Shader;->loadShader(ILjava/lang/String;)I

    move-result v0

    .line 74
    .local v0, "fragShadrHndl":I
    if-lez v3, :cond_0

    if-lez v0, :cond_0

    .line 76
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v2

    .line 77
    if-eqz v2, :cond_0

    .line 79
    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 81
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 83
    invoke-static {v2}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 85
    const/4 v4, 0x1

    new-array v1, v4, [I

    .line 86
    .local v1, "linkStatus":[I
    const v4, 0x8b82

    invoke-static {v2, v4, v1, v5}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 88
    aget v4, v1, v5

    if-nez v4, :cond_0

    .line 89
    const-string v4, "Shader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error compiling program: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const/4 v2, 0x0

    .line 95
    .end local v1    # "linkStatus":[I
    :cond_0
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    if-nez v2, :cond_2

    .line 96
    :cond_1
    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 97
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 98
    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 100
    :cond_2
    return v2
.end method

.method public static loadShader(ILjava/lang/String;)I
    .locals 5
    .param p0, "type"    # I
    .param p1, "shaderCode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 47
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 48
    .local v1, "shader":I
    if-eqz v1, :cond_0

    .line 50
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 51
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 54
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 55
    .local v0, "compileStatus":[I
    const v2, 0x8b81

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 58
    aget v2, v0, v3

    if-nez v2, :cond_0

    .line 59
    const-string v2, "Shader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error compiling shader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 61
    const/4 v1, 0x0

    .line 64
    .end local v0    # "compileStatus":[I
    :cond_0
    return v1
.end method


# virtual methods
.method public initPrograms(I)V
    .locals 4
    .param p1, "textureType"    # I

    .prologue
    .line 36
    const v2, 0x8b30

    const-string v3, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;uniform samplerExternalOES uTexture;varying vec2 vTexCoord;void main() {  gl_FragColor = texture2D(uTexture, vTexCoord);}"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Shader;->loadShader(ILjava/lang/String;)I

    move-result v0

    .line 39
    .local v0, "fragmentShaderExternal":I
    const v2, 0x8b31

    const-string v3, "precision mediump float;\nuniform mat4 uMVPMatrix;uniform mat4 uTextureMat;attribute vec3 aPosition;attribute vec2 aTexCoord;\nvarying vec2 vTexCoord;void main() {  gl_Position = uMVPMatrix * vec4(aPosition, 1.0);  vec4 texCoords = uTextureMat * vec4(aTexCoord.s, 1.0 - aTexCoord.t, 0.0, 1.0);  vTexCoord = texCoords.st;}"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Shader;->loadShader(ILjava/lang/String;)I

    move-result v1

    .line 41
    .local v1, "t360playervertexShader":I
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/Video360/Shader;->createProgram(II)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/Video360/Shader;->m360PlayerClearProgram:I

    .line 42
    return-void
.end method
