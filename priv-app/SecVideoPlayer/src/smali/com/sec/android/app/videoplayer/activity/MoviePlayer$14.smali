.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 2145
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 14
    .param p1, "event"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    const-wide/16 v12, 0x3

    const-wide/16 v8, -0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2148
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 2233
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 2153
    :sswitch_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->isMotionPeekEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2156
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2157
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2158
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    goto :goto_0

    .line 2162
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "window"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 2163
    .local v5, "wManager":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 2164
    .local v4, "mDisplay":Landroid/view/Display;
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getPanningDxImage()I

    move-result v6

    mul-int/lit8 v0, v6, 0x5

    .line 2165
    .local v0, "dX":I
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getPanningDyImage()I

    move-result v6

    mul-int/lit8 v1, v6, 0x5

    .line 2167
    .local v1, "dY":I
    const/4 v2, 0x0

    .line 2168
    .local v2, "deltaX":I
    const/4 v3, 0x0

    .line 2169
    .local v3, "deltaY":I
    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 2188
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # += operator for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2412(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 2189
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # += operator for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I
    invoke-static {v6, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2512(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 2191
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v6, v12

    if-gez v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v6, v12

    if-gez v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    .line 2193
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 2194
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I
    invoke-static {v6, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    goto/16 :goto_0

    .line 2171
    :pswitch_0
    move v2, v0

    .line 2172
    neg-int v3, v1

    .line 2173
    goto :goto_1

    .line 2175
    :pswitch_1
    neg-int v2, v0

    .line 2176
    move v3, v1

    .line 2177
    goto :goto_1

    .line 2179
    :pswitch_2
    neg-int v2, v1

    .line 2180
    neg-int v3, v0

    .line 2181
    goto :goto_1

    .line 2183
    :pswitch_3
    move v2, v1

    .line 2184
    move v3, v0

    goto :goto_1

    .line 2198
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v6

    if-ne v6, v11, :cond_4

    .line 2199
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 2200
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I
    invoke-static {v6, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    goto/16 :goto_0

    .line 2204
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-boolean v6, v6, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    if-ne v6, v11, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v6

    if-ne v6, v11, :cond_8

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getPeekHelpInstance()Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    move-result-object v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getPeekHelpInstance()Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->isScreenTouched()Z

    move-result v6

    if-ne v6, v11, :cond_8

    .line 2208
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v6

    int-to-long v6, v6

    const-wide/16 v8, 0x46

    cmp-long v6, v6, v8

    if-gtz v6, :cond_7

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v6

    int-to-long v6, v6

    const-wide/16 v8, -0x46

    cmp-long v6, v6, v8

    if-gez v6, :cond_8

    .line 2211
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isBlockMotionPeek()Z
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2212
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "SIDE_MIRROR :: block"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    const v7, 0x7f0a0066

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 2226
    :cond_8
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I
    invoke-static {v6, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 2227
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I
    invoke-static {v6, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    goto/16 :goto_0

    .line 2215
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->PrecessingSideMirroring()V
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 2216
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_a

    .line 2217
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 2218
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->callSlideChapterPreview()V

    .line 2220
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v6

    if-ne v6, v11, :cond_8

    .line 2221
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideMotionPeekHelpText()V

    goto :goto_2

    .line 2148
    nop

    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_1
        0x48 -> :sswitch_0
    .end sparse-switch

    .line 2169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
