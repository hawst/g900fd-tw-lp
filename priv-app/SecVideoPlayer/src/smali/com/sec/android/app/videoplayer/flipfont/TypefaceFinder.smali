.class public Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;
.super Ljava/lang/Object;
.source "TypefaceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefacesClass;,
        Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefaceSortByName;
    }
.end annotation


# static fields
.field public static final DEFAULT_FONT_VALUE:Ljava/lang/String; = "default"

.field private static final FONT_ASSET_DIR:Ljava/lang/String; = "xml"

.field private static final FONT_DIRECTORY:Ljava/lang/String; = "fonts/"

.field private static final FONT_EXTENSION:Ljava/lang/String; = ".ttf"

.field private static final TAG:Ljava/lang/String; = "TypefaceFinder"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mTypefaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/videoplayer/flipfont/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    .line 67
    return-void
.end method


# virtual methods
.method public clearTypfaceArray()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 121
    :cond_0
    return-void
.end method

.method public findTypefaces(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 7
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 74
    const/4 v3, 0x0

    .line 76
    .local v3, "xmlfiles":[Ljava/lang/String;
    :try_start_0
    const-string v4, "xml"

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 81
    const/4 v1, 0x0

    .line 82
    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 84
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "xml/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 85
    .local v2, "in":Ljava/io/InputStream;
    aget-object v4, v3, v1

    invoke-virtual {p0, v4, v2, p2}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->parseTypefaceXml(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 91
    .end local v2    # "in":Ljava/io/InputStream;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 78
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 93
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_2
    return v4

    .line 87
    .restart local v1    # "i":I
    :catch_1
    move-exception v0

    .line 89
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v4, "TypefaceFinder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Not possible to open, continue to next file, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 93
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_0
    const/4 v4, 0x1

    goto :goto_2
.end method

.method public getSansEntries(Landroid/content/pm/PackageManager;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;Landroid/graphics/Typeface;Landroid/graphics/Typeface;)V
    .locals 13
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p5, "mCourierFont"    # Landroid/graphics/Typeface;
    .param p6, "mTimesNewRomanFont"    # Landroid/graphics/Typeface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/Typeface;",
            "Landroid/graphics/Typeface;",
            ")V"
        }
    .end annotation

    .prologue
    .line 137
    .local p2, "entries":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .local p3, "entryValues":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .local p4, "fontPackageName":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00bd

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {p2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 138
    const-string v10, "default"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v10, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 141
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-nez v10, :cond_2

    .line 142
    if-eqz p5, :cond_0

    .line 143
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00bf

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {p2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00bf

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 145
    const-string v10, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_0
    if-eqz p6, :cond_1

    .line 149
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00c1

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {p2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00c1

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v10, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00be

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {p2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00be

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v10, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00c0

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {p2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00c0

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 160
    const-string v10, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    new-instance v11, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefaceSortByName;

    invoke-direct {v11, p0}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefaceSortByName;-><init>(Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;)V

    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 165
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v5, v10, :cond_8

    .line 166
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->getSansName()Ljava/lang/String;

    move-result-object v8

    .line 167
    .local v8, "s":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 169
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->getTypefaceFilename()Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "fontName":Ljava/lang/String;
    if-nez v4, :cond_4

    .line 171
    const-string v10, "TypefaceFinder"

    const-string v11, "getSansEntries :: fontName is null"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    .end local v4    # "fontName":Ljava/lang/String;
    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 175
    .restart local v4    # "fontName":Ljava/lang/String;
    :cond_4
    const/16 v10, 0x2f

    invoke-virtual {v4, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    .line 176
    .local v9, "start_pos":I
    const/16 v10, 0x2e

    invoke-virtual {v4, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 177
    .local v3, "end_pos":I
    if-gez v3, :cond_5

    .line 178
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 180
    :cond_5
    add-int/lit8 v10, v9, 0x1

    invoke-virtual {v4, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 181
    .local v6, "loadTypeface":Ljava/lang/String;
    const-string v10, " "

    const-string v11, ""

    invoke-virtual {v6, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 183
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->getFontPackageName()Ljava/lang/String;

    move-result-object v7

    .line 184
    .local v7, "packageName":Ljava/lang/String;
    if-nez v7, :cond_6

    .line 185
    const-string v10, "TypefaceFinder"

    const-string v11, "getSansEntries :: packageName is null"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 190
    :cond_6
    const/16 v10, 0x80

    :try_start_0
    invoke-virtual {p1, v7, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 191
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v10, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v10, v1, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 194
    const-string v10, "com.monotype.android.font.droidserifitalic"

    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 198
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->KOREA:Z

    if-eqz v10, :cond_7

    .line 199
    const-string v10, "com.monotype.android.font.cooljazz"

    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 204
    :cond_7
    invoke-virtual {p2, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 205
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 206
    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 207
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 208
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "TypefaceFinder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getSansEntries - Typeface.createFromAsset caused an exception for - fonts/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".ttf"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 215
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "end_pos":I
    .end local v4    # "fontName":Ljava/lang/String;
    .end local v6    # "loadTypeface":Ljava/lang/String;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "s":Ljava/lang/String;
    .end local v9    # "start_pos":I
    :cond_8
    return-void
.end method

.method public parseTypefaceXml(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 9
    .param p1, "xmlFilename"    # Ljava/lang/String;
    .param p2, "inStream"    # Ljava/io/InputStream;
    .param p3, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 101
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v4

    .line 102
    .local v4, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 103
    .local v3, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v5

    .line 104
    .local v5, "xr":Lorg/xml/sax/XMLReader;
    new-instance v1, Lcom/sec/android/app/videoplayer/flipfont/TypefaceParser;

    invoke-direct {v1}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceParser;-><init>()V

    .line 105
    .local v1, "fontParser":Lcom/sec/android/app/videoplayer/flipfont/TypefaceParser;
    invoke-interface {v5, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 106
    new-instance v6, Lorg/xml/sax/InputSource;

    invoke-direct {v6, p2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v5, v6}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 107
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceParser;->getParsedData()Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    move-result-object v2

    .line 108
    .local v2, "newTypeface":Lcom/sec/android/app/videoplayer/flipfont/Typeface;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->setTypefaceFilename(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v2, p3}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->setFontPackageName(Ljava/lang/String;)V

    .line 110
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v1    # "fontParser":Lcom/sec/android/app/videoplayer/flipfont/TypefaceParser;
    .end local v2    # "newTypeface":Lcom/sec/android/app/videoplayer/flipfont/Typeface;
    .end local v3    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v4    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .end local v5    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "TypefaceFinder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File parsing is not possible, omit this typeface, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
