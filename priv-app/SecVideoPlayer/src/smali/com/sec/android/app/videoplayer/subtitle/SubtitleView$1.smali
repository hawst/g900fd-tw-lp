.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SubtitleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayoutDefaultPosition()V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Z)Z

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setBackgroundColor(I)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->performHapticFeedback(I)Z

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    .line 199
    const/4 v0, 0x1

    return v0
.end method
