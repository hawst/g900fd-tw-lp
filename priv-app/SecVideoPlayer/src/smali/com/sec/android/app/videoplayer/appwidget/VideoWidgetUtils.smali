.class public Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;
.super Ljava/lang/Object;
.source "VideoWidgetUtils.java"


# static fields
.field public static final FOUR_BLOCKS:I = 0x4

.field public static final ONE_BLOCK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoWidgetUtils"

.field public static final THREE_BLOCKS:I = 0x3

.field public static final TWO_BLOCKS:I = 0x2


# instance fields
.field private mWidgetOneBlockHeight:I

.field private mWidgetRemoteViews:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetOneBlockHeight:I

    .line 26
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetRemoteViews:Landroid/widget/RemoteViews;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetOneBlockHeight:I

    .line 30
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetRemoteViews:Landroid/widget/RemoteViews;

    .line 31
    return-void
.end method

.method private getWidgetHeightValue(Landroid/content/Context;I)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 92
    .local v0, "density":F
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v1

    .line 94
    .local v1, "widgetOption":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 95
    const-string v2, "appWidgetMaxHeight"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 98
    :goto_0
    return v2

    .line 97
    :cond_0
    const-string v2, "VideoWidgetUtils"

    const-string v3, "getWidgetHeightValue E: getAppWidgetOptions(id) returns null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getWidgetSpanY(I)I
    .locals 2
    .param p1, "wigetHeight"    # I

    .prologue
    .line 84
    const/4 v0, 0x4

    .local v0, "i":I
    :goto_0
    if-lez v0, :cond_1

    .line 85
    iget v1, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetOneBlockHeight:I

    mul-int/2addr v1, v0

    if-lt p1, v1, :cond_0

    .line 87
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 84
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public updateAllWidgetView(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 34
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetRemoteViews:Landroid/widget/RemoteViews;

    .line 35
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;

    invoke-direct {v3, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 36
    .local v1, "ids":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 37
    aget v2, v1, v0

    aget v3, v1, v0

    invoke-direct {p0, p1, v3}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->getWidgetHeightValue(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->getWidgetSpanY(I)I

    move-result v3

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->updateWidgetView(Landroid/content/Context;Landroid/widget/RemoteViews;II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 39
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    aget v3, v1, v0

    invoke-virtual {v2, v3, p2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_1
    return-void
.end method

.method public updateWidgetView(Landroid/content/Context;II)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetId"    # I
    .param p3, "spanY"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->mWidgetRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->updateWidgetView(Landroid/content/Context;Landroid/widget/RemoteViews;II)Z

    move-result v0

    return v0
.end method

.method public updateWidgetView(Landroid/content/Context;Landroid/widget/RemoteViews;II)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "views"    # Landroid/widget/RemoteViews;
    .param p3, "widgetId"    # I
    .param p4, "spanY"    # I

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v4

    const-string v5, "lastPlayedItem"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "lastPath":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 52
    .local v2, "uri":Landroid/net/Uri;
    if-eqz v2, :cond_0

    if-eqz p3, :cond_0

    if-gtz p4, :cond_2

    .line 53
    :cond_0
    const-string v4, "VideoWidgetUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateWidgetView E: no item to play or no wiget ID :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_1
    :goto_0
    return v3

    .line 57
    :cond_2
    const-string v4, "VideoWidgetUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateWidgetView E:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    if-nez p2, :cond_3

    .line 60
    new-instance p2, Landroid/widget/RemoteViews;

    .end local p2    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030043

    invoke-direct {p2, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 63
    .restart local p2    # "views":Landroid/widget/RemoteViews;
    :cond_3
    const/4 v1, -0x1

    .line 64
    .local v1, "resID":I
    packed-switch p4, :pswitch_data_0

    .line 75
    :goto_1
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    .line 76
    const v3, 0x7f0d01f7

    invoke-virtual {p2, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 77
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3, p3, p2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 78
    const/4 v3, 0x1

    goto :goto_0

    .line 66
    :pswitch_0
    const v1, 0x7f020045

    .line 67
    goto :goto_1

    .line 71
    :pswitch_1
    const v1, 0x7f020046

    goto :goto_1

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
