.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 1878
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 1881
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/16 v1, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    .line 1882
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1883
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1884
    const-string v0, "VideoTitleController"

    const-string v1, "mAllshareVolumeMuteClickListener call"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1885
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setMute()V

    .line 1887
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    .line 1889
    :cond_1
    return-void
.end method
