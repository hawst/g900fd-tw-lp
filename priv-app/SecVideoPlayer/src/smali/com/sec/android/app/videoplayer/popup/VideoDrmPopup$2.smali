.class Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;
.super Ljava/lang/Object;
.source "VideoDrmPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->createDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 134
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)I

    move-result v1

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "autoPlay"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    .line 142
    .local v0, "mAutoPlay":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createDialog - mAutoPlay = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isVideoList()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "createDialog  SCHEME_VIDEO_LIST - next: false -> exit"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 152
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0
.end method
