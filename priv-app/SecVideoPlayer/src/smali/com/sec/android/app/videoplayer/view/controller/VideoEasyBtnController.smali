.class public Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;
.super Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.source "VideoEasyBtnController.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoEasyBtnController"


# instance fields
.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mContext:Landroid/content/Context;

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private mPauseKeyListener:Landroid/view/View$OnKeyListener;

.field private mPauseTouchListener:Landroid/view/View$OnTouchListener;

.field private mPlayPauseButton:Landroid/widget/ImageButton;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private rtl:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    .line 105
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    .line 176
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    .line 36
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    .line 38
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 39
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 40
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method private addPlayPauseView()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-nez v2, :cond_0

    .line 75
    const-string v2, "VideoEasyBtnController"

    const-string v3, "addPlayPauseView : mMainVideoView is NOT init"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 78
    :cond_0
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    .line 79
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080186

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080185

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 82
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 94
    .local v0, "hoverPopupWindow":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_1

    .line 95
    const/16 v2, 0x5051

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 96
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->updatePausePlayBtn()V

    .line 101
    .end local v0    # "hoverPopupWindow":Landroid/widget/HoverPopupWindow;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0xbb8

    .line 223
    const/4 v0, 0x0

    .line 224
    .local v0, "retVal":Z
    packed-switch p1, :pswitch_data_0

    .line 248
    :goto_0
    return v0

    .line 229
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 241
    :goto_1
    const/4 v0, 0x1

    .line 242
    goto :goto_0

    .line 231
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 235
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 224
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 229
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public changeControllerLayout(I)V
    .locals 13
    .param p1, "windowWidth"    # I

    .prologue
    const v12, 0x7f080195

    const v11, 0x7f080193

    const v10, 0x7f080043

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 134
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d012f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 135
    .local v0, "btn_ctrl_layout":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0140

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 136
    .local v3, "ctrl_progressbar":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0131

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 137
    .local v2, "ctrl_playtime_and_buttons_layout":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0133

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 138
    .local v1, "ctrl_cur_playtime_text":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0134

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 140
    .local v4, "ctrl_total_playtime_text":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 141
    iput v8, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 142
    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 147
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080197

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080197

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080192

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080196

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080194

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 159
    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090015

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-float v5, v6

    .line 163
    .local v5, "textSize":F
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v6, v9, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 164
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mCurrentTime:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 166
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v6, v9, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 167
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mEndTime:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 169
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0131

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0140

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d012f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0133

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v7, 0x7f0d0134

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    return-void

    .line 144
    .end local v5    # "textSize":F
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_0
.end method

.method public forceHide()V
    .locals 2

    .prologue
    .line 263
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->forceHide()V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 265
    return-void
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 258
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->hide()V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 260
    return-void
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->makeControllerView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->addPlayPauseView()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->setInvisibleAllViews()V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    return-object v0
.end method

.method public setVisibleAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0140

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0141

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d011e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0133

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0134

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0128

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mDetailSeekViewStub:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->rtl:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 70
    :cond_2
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 252
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->show()V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 255
    :cond_0
    return-void
.end method

.method public updatePausePlayBtn()V
    .locals 5

    .prologue
    const v4, 0x7f020055

    const v3, 0x7f0a016c

    const v2, 0x7f0a016b

    const v1, 0x7f020056

    .line 268
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePausePlayBtn()V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 273
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 286
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 291
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const v1, 0x7f02001f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 295
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a014d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
