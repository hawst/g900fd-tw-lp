.class public Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;
.super Landroid/app/Service;
.source "VideoWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateService"
.end annotation


# static fields
.field private static final HANDLE_UPDATE_CLOUD_CONTENT:I = 0x7d0

.field private static final HANDLE_UPDATE_CONTENT:I = 0x3e8

.field private static mDBObserver:Landroid/database/ContentObserver;

.field private static mIsNewlyLaunched:Z

.field private static mIsSetTheme:Z

.field private static mThemeMgr:Lcom/samsung/android/theme/SThemeManager;


# instance fields
.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mContentHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field private mVWUtils:Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    .line 102
    sput-boolean v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    .line 104
    sput-boolean v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 84
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 88
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 90
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 92
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVWUtils:Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;

    .line 517
    new-instance v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$2;-><init>(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private backgroundUpdate()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const v8, 0x7f0d01ed

    .line 169
    const/4 v5, 0x0

    .line 170
    .local v5, "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 172
    .local v0, "configuration":Landroid/content/res/Configuration;
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHAGALL_VZW_WIDGET_CONCEPT:Z

    if-eqz v6, :cond_1

    .line 173
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030042

    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 174
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    iget v6, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v9, :cond_0

    .line 175
    const v6, 0x7f02026c

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 200
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 201
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "com.samsung.everglades.video"

    const-string v7, "com.samsung.everglades.video.VideoMain"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    const/high16 v7, 0x8000000

    invoke-static {v6, v10, v1, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 205
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0d01f0

    invoke-virtual {v5, v6, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 208
    new-instance v4, Landroid/content/ComponentName;

    const-class v6, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;

    invoke-direct {v4, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 209
    .local v4, "thisWidget":Landroid/content/ComponentName;
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 210
    .local v2, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v2, v4, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 211
    return-void

    .line 177
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v3    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v4    # "thisWidget":Landroid/content/ComponentName;
    :cond_0
    const v6, 0x7f02026d

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 179
    :cond_1
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MAIN_WIDGET_FEATURE:Z

    if-eqz v6, :cond_3

    .line 180
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030041

    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 182
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    iget v6, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v9, :cond_2

    .line 183
    const v6, 0x7f02026b

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 185
    :cond_2
    const v6, 0x7f02026e

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 189
    :cond_3
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030040

    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 190
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    const v6, 0x7f0d01f2

    invoke-virtual {v5, v6, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 192
    iget v6, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v9, :cond_4

    .line 193
    const v6, 0x7f0200a4

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 195
    :cond_4
    const v6, 0x7f0200a5

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0
.end method

.method private static getBitmapfromSThemeManager(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "bitmapId"    # Ljava/lang/String;

    .prologue
    .line 489
    sget-boolean v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    if-nez v2, :cond_0

    .line 490
    const/4 v0, 0x0

    .line 500
    :goto_0
    return-object v0

    .line 492
    :cond_0
    const/4 v0, 0x0

    .line 495
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v2, p0}, Lcom/samsung/android/theme/SThemeManager;->getItemBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 496
    :catch_0
    move-exception v1

    .line 497
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v2, "VideoWidget"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getClockBitmap( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) failed! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getColorfromSThemeManager(Ljava/lang/String;)I
    .locals 5
    .param p0, "colorId"    # Ljava/lang/String;

    .prologue
    .line 504
    sget-boolean v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    if-nez v2, :cond_0

    .line 505
    const/4 v0, 0x0

    .line 514
    :goto_0
    return v0

    .line 507
    :cond_0
    const/4 v0, 0x0

    .line 510
    .local v0, "color":I
    :try_start_0
    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v2, p0}, Lcom/samsung/android/theme/SThemeManager;->getItemColor(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 511
    :catch_0
    move-exception v1

    .line 512
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v2, "VideoWidget"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getClockBitmap( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) failed! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getThumbnail(Landroid/content/Context;JLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remainTime"    # J
    .param p4, "path"    # Ljava/lang/String;

    .prologue
    .line 400
    const-string v4, "VideoWidget"

    const-string v5, "getWidgetThumbnail"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    const/4 v3, 0x0

    .line 402
    .local v3, "thumb":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 405
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v2, p4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 407
    const/16 v4, 0x3c0

    const/16 v5, 0x21c

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 408
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p2

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 415
    :try_start_1
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 421
    :goto_0
    return-object v3

    .line 416
    :catch_0
    move-exception v1

    .line 417
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v4, "VideoWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getThumbnail() RuntimeException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 410
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 411
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 412
    const-string v4, "VideoWidget"

    const-string v5, "getThumbnail() - setDataSource failed. DRM or anything else?"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 415
    :try_start_3
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 416
    :catch_2
    move-exception v1

    .line 417
    .restart local v1    # "ex":Ljava/lang/RuntimeException;
    const-string v4, "VideoWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getThumbnail() RuntimeException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 414
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    .line 415
    :try_start_4
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 418
    :goto_1
    throw v4

    .line 416
    :catch_3
    move-exception v1

    .line 417
    .restart local v1    # "ex":Ljava/lang/RuntimeException;
    const-string v5, "VideoWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getThumbnail() RuntimeException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private isRTL()I
    .locals 3

    .prologue
    .line 214
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 215
    .local v0, "defLocale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "lang":Ljava/lang/String;
    const-string v2, "iw"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "fa"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ur"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 219
    :cond_0
    const/4 v2, 0x1

    .line 221
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isSetTheme(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 473
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "current_sec_theme_package"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 475
    .local v0, "themePackageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 476
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    .line 480
    :goto_0
    if-nez v0, :cond_2

    .line 481
    const-string v1, "VideoWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSetTheme   themePackageName == null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :goto_1
    return-void

    .line 478
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    goto :goto_0

    .line 482
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 483
    const-string v1, "VideoWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSetTheme   themePackageName.isEmpty()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsSetTheme:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 485
    :cond_3
    const-string v1, "VideoWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSetTheme  themePackageName "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 425
    const-string v1, "VideoWidget"

    const-string v2, "registerContentObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    if-nez v1, :cond_1

    .line 428
    new-instance v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;-><init>(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;Landroid/os/Handler;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    .line 446
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 447
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 448
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 450
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v1, :cond_0

    .line 451
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 454
    :cond_0
    const-string v1, "VideoWidget"

    const-string v2, "registerContentObserver - mDBObserver is now registered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :goto_0
    return-void

    .line 456
    :cond_1
    const-string v1, "VideoWidget"

    const-string v2, "registerContentObserver - mDBObserver already exists"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 391
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 392
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 393
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 394
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 396
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private unregisterContentObserver()V
    .locals 3

    .prologue
    .line 461
    const-string v1, "VideoWidget"

    const-string v2, "unregisterContentObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 464
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 465
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 466
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mDBObserver:Landroid/database/ContentObserver;

    .line 467
    const-string v1, "VideoWidget"

    const-string v2, "unregisterContentObserver - mDBObserver is unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_0
    const-string v1, "VideoWidget"

    const-string v2, "unregisterContentObserver : registerContentObserver is NULL"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    return-void
.end method


# virtual methods
.method public bulidUpdate()V
    .locals 25

    .prologue
    .line 226
    const-string v21, "VideoWidget"

    const-string v22, "bulidUpdate"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateVersion()V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v21

    const-string v22, "lastPlayedItem"

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 231
    .local v10, "lastPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 232
    .local v19, "uri":Landroid/net/Uri;
    const/16 v20, 0x0

    .line 233
    .local v20, "views":Landroid/widget/RemoteViews;
    const/4 v7, 0x0

    .line 235
    .local v7, "intent":Landroid/content/Intent;
    const/16 v21, 0x0

    sput-boolean v21, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    .line 237
    if-nez v19, :cond_2

    .line 238
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v21, :cond_2

    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v21, :cond_0

    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    if-eqz v21, :cond_1

    :cond_0
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-nez v21, :cond_2

    .line 239
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v21

    const-string v22, "newlyLaunched"

    const/16 v23, 0x1

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v21

    sput-boolean v21, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    .line 240
    sget-boolean v21, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    if-eqz v21, :cond_2

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v21

    sget-object v22, Lcom/sec/android/app/videoplayer/common/feature/Path;->DEFAULT_WIDGET_VIDEO:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 246
    :cond_2
    const-string v21, "VideoWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "widget is newly launched : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-boolean v23, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    if-nez v19, :cond_b

    .line 249
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 251
    .local v6, "configuration":Landroid/content/res/Configuration;
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHAGALL_VZW_WIDGET_CONCEPT:Z

    if-eqz v21, :cond_6

    .line 252
    new-instance v20, Landroid/widget/RemoteViews;

    .end local v20    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030042

    invoke-direct/range {v20 .. v22}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 253
    .restart local v20    # "views":Landroid/widget/RemoteViews;
    const v21, 0x7f0d01f1

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 255
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_5

    .line 256
    const v21, 0x7f0d01ed

    const v22, 0x7f02026c

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 285
    :goto_0
    const-string v21, "VideoWidget"

    const-string v22, "bulidUpdate() : No video widget setup done...!!!"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    new-instance v7, Landroid/content/Intent;

    .end local v7    # "intent":Landroid/content/Intent;
    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 287
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v21, "com.samsung.everglades.video"

    const-string v22, "com.samsung.everglades.video.VideoMain"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    const-string v21, "android.intent.action.MAIN"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v21, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    .end local v6    # "configuration":Landroid/content/res/Configuration;
    :goto_1
    if-eqz v7, :cond_3

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/high16 v23, 0x8000000

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v7, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    .line 378
    .local v12, "pendingIntent":Landroid/app/PendingIntent;
    const v21, 0x7f0d01f0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v12}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 381
    .end local v12    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_3
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v21, :cond_4

    if-nez v19, :cond_16

    .line 382
    :cond_4
    new-instance v16, Landroid/content/ComponentName;

    const-class v21, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 383
    .local v16, "thisWidget":Landroid/content/ComponentName;
    invoke-static/range {p0 .. p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v11

    .line 384
    .local v11, "manager":Landroid/appwidget/AppWidgetManager;
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 388
    .end local v11    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v16    # "thisWidget":Landroid/content/ComponentName;
    :goto_2
    return-void

    .line 258
    .restart local v6    # "configuration":Landroid/content/res/Configuration;
    :cond_5
    const v21, 0x7f0d01ed

    const v22, 0x7f02026d

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 260
    :cond_6
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MAIN_WIDGET_FEATURE:Z

    if-eqz v21, :cond_8

    .line 261
    new-instance v20, Landroid/widget/RemoteViews;

    .end local v20    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030041

    invoke-direct/range {v20 .. v22}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 263
    .restart local v20    # "views":Landroid/widget/RemoteViews;
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 264
    const v21, 0x7f0d01ed

    const v22, 0x7f02026b

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0

    .line 266
    :cond_7
    const v21, 0x7f0d01ed

    const v22, 0x7f02026e

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0

    .line 269
    :cond_8
    new-instance v20, Landroid/widget/RemoteViews;

    .end local v20    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030040

    invoke-direct/range {v20 .. v22}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 271
    .restart local v20    # "views":Landroid/widget/RemoteViews;
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v21, :cond_a

    .line 272
    const v21, 0x7f0d01f2

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 274
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    .line 275
    const v21, 0x7f0d01ed

    const v22, 0x7f0200a4

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0

    .line 277
    :cond_9
    const v21, 0x7f0d01ed

    const v22, 0x7f0200a5

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0

    .line 280
    :cond_a
    const v21, 0x7f0d01f1

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0

    .line 292
    .end local v6    # "configuration":Landroid/content/res/Configuration;
    :cond_b
    const/16 v17, 0x0

    .line 293
    .local v17, "thumb":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 294
    .local v18, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v14

    .line 295
    .local v14, "remainTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDurationTime(Landroid/net/Uri;)J

    move-result-wide v8

    .line 297
    .local v8, "durationTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 299
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v21, :cond_c

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 303
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v21, v0

    if-eqz v21, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->requestThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 318
    :goto_3
    new-instance v20, Landroid/widget/RemoteViews;

    .end local v20    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030043

    invoke-direct/range {v20 .. v22}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 319
    .restart local v20    # "views":Landroid/widget/RemoteViews;
    const v21, 0x7f0d01f9

    const-string v22, "#ffffff"

    invoke-static/range {v22 .. v22}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 321
    if-nez v17, :cond_d

    .line 322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f020280

    invoke-static/range {v21 .. v22}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 325
    :cond_d
    if-eqz v17, :cond_e

    .line 326
    const v21, 0x7f0d01f4

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 329
    :cond_e
    const/4 v13, 0x0

    .line 331
    .local v13, "remainPercent":I
    const-wide/16 v22, 0x0

    cmp-long v21, v8, v22

    if-lez v21, :cond_f

    const-wide/16 v22, 0x0

    cmp-long v21, v14, v22

    if-lez v21, :cond_f

    .line 332
    const-wide/16 v22, 0x64

    mul-long v22, v22, v14

    div-long v22, v22, v8

    move-wide/from16 v0, v22

    long-to-int v13, v0

    .line 333
    if-gtz v13, :cond_f

    .line 334
    const/4 v13, 0x0

    .line 338
    :cond_f
    const v21, 0x7f0d01f8

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 341
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v21, :cond_14

    sget-boolean v21, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    if-eqz v21, :cond_14

    .line 342
    const v21, 0x7f0d01f9

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->stringForTime(J)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 343
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->isRTL()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_13

    .line 344
    const v21, 0x7f0d01fa

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->stringForTime(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " / "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 349
    :goto_4
    const v21, 0x7f0d0088

    const/16 v22, 0x64

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 370
    :goto_5
    new-instance v7, Landroid/content/Intent;

    .end local v7    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const-class v22, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 371
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v21, "isFromWidget"

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372
    const-string v21, "filePath"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const-string v21, "uri"

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 306
    .end local v13    # "remainPercent":I
    :cond_10
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v21, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v21, v0

    if-eqz v21, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->requestThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v17

    goto/16 :goto_3

    .line 310
    :cond_11
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v21, :cond_12

    sget-boolean v21, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    if-eqz v21, :cond_12

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const-wide/32 v22, 0xf230

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, v22

    move-object/from16 v4, v24

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->getThumbnail(Landroid/content/Context;JLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v17

    goto/16 :goto_3

    .line 313
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->getThumbnail(Landroid/content/Context;JLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v17

    goto/16 :goto_3

    .line 347
    .restart local v13    # "remainPercent":I
    :cond_13
    const v21, 0x7f0d01fa

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " / "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->stringForTime(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 351
    :cond_14
    const v21, 0x7f0d01f9

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->stringForTime(J)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 352
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->isRTL()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_15

    .line 353
    const v21, 0x7f0d01fa

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->stringForTime(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " / "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 359
    :goto_6
    const v21, 0x7f0d0088

    const/16 v22, 0x64

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v13, v3}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    goto/16 :goto_5

    .line 356
    :cond_15
    const v21, 0x7f0d01fa

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " / "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->stringForTime(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_6

    .line 386
    .end local v8    # "durationTime":J
    .end local v13    # "remainPercent":I
    .end local v14    # "remainTime":J
    .end local v17    # "thumb":Landroid/graphics/Bitmap;
    .end local v18    # "title":Ljava/lang/String;
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;->getVWUtils(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetUtils;->updateAllWidgetView(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    goto/16 :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 158
    const-string v0, "VideoWidget"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 117
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MAIN_WIDGET_FEATURE:Z

    if-eqz v2, :cond_1

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "lastPlayedItem"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "lastPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 121
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_1

    .line 122
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v2, :cond_3

    .line 123
    sget-boolean v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mIsNewlyLaunched:Z

    if-eqz v2, :cond_2

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/common/feature/Path;->DEFAULT_WIDGET_VIDEO:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 134
    .end local v0    # "lastPath":Ljava/lang/String;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-void

    .line 126
    .restart local v0    # "lastPath":Ljava/lang/String;
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->backgroundUpdate()V

    goto :goto_0

    .line 129
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->backgroundUpdate()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "VideoWidget"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 110
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContext:Landroid/content/Context;

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->registerContentObserver()V

    .line 112
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->unregisterContentObserver()V

    .line 165
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 166
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 138
    const-string v0, "VideoWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->bulidUpdate()V

    .line 153
    const/4 v0, 0x1

    return v0
.end method
