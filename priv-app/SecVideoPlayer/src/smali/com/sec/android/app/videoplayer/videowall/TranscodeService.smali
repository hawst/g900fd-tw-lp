.class public Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
.super Landroid/app/Service;
.source "TranscodeService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
    }
.end annotation


# static fields
.field private static final ACTION_CHECK_SIOP_LEVEL_CHANGED:Ljava/lang/String; = "android.intent.action.SIOP_LEVEL_CHANGED"

.field public static final TAG:Ljava/lang/String;

.field public static runvideoplayer:Z


# instance fields
.field private ChapterCount:I

.field private ChapterThumbHeight:I

.field private ChapterThumbWidth:I

.field private Fps:I

.field private ListLiveThumbHeight:I

.field private ListLiveThumbWidth:I

.field private ThreadCount:I

.field private bStop:Z

.field private final mBatteryReceiver:Landroid/content/BroadcastReceiver;

.field private mDuration:I

.field private mFileMgr:Lcom/sec/android/videowall/FileMgr;

.field private mFileName:Ljava/lang/String;

.field private mFilePath:Ljava/lang/String;

.field private mPathName:Ljava/lang/String;

.field private mPowerMgr:Landroid/os/PowerManager;

.field private final mSIOPLevelReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenReceiver:Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

.field private mTelephoneMgr:Landroid/telephony/TelephonyManager;

.field private mTranscodeThread:Ljava/lang/Thread;

.field private mVideoId:J

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mScreenReceiver:Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPowerMgr:Landroid/os/PowerManager;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTelephoneMgr:Landroid/telephony/TelephonyManager;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    .line 344
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$2;-><init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    .line 361
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$3;-><init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mSIOPLevelReceiver:Landroid/content/BroadcastReceiver;

    .line 426
    return-void
.end method

.method private CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V
    .locals 17
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "videoId"    # J
    .param p5, "thischapter"    # I
    .param p6, "thumbtime"    # I
    .param p7, "filepath"    # Ljava/lang/String;

    .prologue
    .line 457
    if-gez p5, :cond_2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ListLiveThumbWidth:I

    .line 458
    .local v15, "width":I
    :goto_0
    if-gez p5, :cond_3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ListLiveThumbHeight:I

    .line 459
    .local v13, "height":I
    :goto_1
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createLivethumbnail - videoId [ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ] : chapter [ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "width : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "height : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const/4 v14, 0x0

    .line 462
    .local v14, "result":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v2}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 463
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v3

    .line 464
    .local v3, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    if-nez v2, :cond_0

    .line 465
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I

    mul-int v2, v2, p5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterCount:I

    div-int v4, v2, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterCount:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->Fps:I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterCount:I

    div-int v12, v2, v5

    move-object/from16 v2, p1

    move/from16 v5, p5

    move v7, v15

    move v8, v13

    move/from16 v10, p6

    invoke-static/range {v2 .. v12}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I

    move-result v14

    .line 468
    :cond_0
    const/4 v2, 0x1

    if-ne v14, v2, :cond_4

    .line 469
    const/4 v2, -0x1

    move/from16 v0, p5

    if-le v0, v2, :cond_1

    .line 470
    move-object/from16 v0, p7

    move/from16 v1, p5

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setKeyframeTime(Ljava/lang/String;I)I

    .line 471
    :cond_1
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v4, "transcode success!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    .end local v3    # "fileName":Ljava/lang/String;
    :goto_2
    return-void

    .line 457
    .end local v13    # "height":I
    .end local v14    # "result":I
    .end local v15    # "width":I
    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterThumbWidth:I

    goto/16 :goto_0

    .line 458
    .restart local v15    # "width":I
    :cond_3
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterThumbHeight:I

    goto/16 :goto_1

    .line 473
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v13    # "height":I
    .restart local v14    # "result":I
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v4, "transcode fail!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/videowall/FileMgr;->deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V

    goto :goto_2

    .line 477
    .end local v3    # "fileName":Ljava/lang/String;
    :cond_5
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v4, "Chapter View thumbnail is existed!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->initWakeLock()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J
    .param p5, "x4"    # I
    .param p6, "x5"    # I
    .param p7, "x6"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct/range {p0 .. p7}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
    .param p1, "x1"    # J

    .prologue
    .line 29
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J

    return-wide p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFilePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseWakeLock()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseTranscodeThread()V

    return-void
.end method

.method private getVideowallSetting()V
    .locals 2

    .prologue
    .line 331
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getServiceTranscodeThread()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ThreadCount:I

    .line 332
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getThumbnailFps()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->Fps:I

    .line 333
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterCount:I

    .line 335
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterThumbWidth:I

    .line 336
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterThumbHeight:I

    .line 338
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getListViewThumbnailWidthEverglades()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ListLiveThumbWidth:I

    .line 339
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getListViewThumbnailHeightEverglades()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ListLiveThumbHeight:I

    .line 341
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->printInfo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method private initWakeLock()V
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPowerMgr:Landroid/os/PowerManager;

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 77
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "WakeLock.acquire()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    return-void
.end method

.method private isMusicRunning()Z
    .locals 5

    .prologue
    .line 255
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 256
    .local v0, "audioMgr":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    .line 257
    .local v1, "isMusicActive":Z
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Music is = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return v1
.end method

.method private printInfo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 421
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TranscodeService [ChapterCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ThreadCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ThreadCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ChapterPreviewThumbnailWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterThumbWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ChapterPreviewThumbnailHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterThumbHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Fps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->Fps:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ListLiveThumbnailWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ListLiveThumbWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ListLiveThumbnailHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ListLiveThumbHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private registerBatteryBroadcastReciever()V
    .locals 2

    .prologue
    .line 356
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 357
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 359
    return-void
.end method

.method private registerSIOPLevelBroadcastReveiver()V
    .locals 2

    .prologue
    .line 377
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 378
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mSIOPLevelReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 380
    return-void
.end method

.method private registerTranscodeReciever()V
    .locals 2

    .prologue
    .line 383
    new-instance v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    invoke-direct {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mScreenReceiver:Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    .line 385
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 386
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 387
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mScreenReceiver:Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 390
    return-void
.end method

.method private releaseTranscodeThread()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 393
    sget-object v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v2, "releaseTranscodeThread."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 395
    sget-object v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v2, "Thread isAlive"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    .line 397
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v1, v4}, Lcom/sec/android/videowall/FileMgr;->setInterruptflag(Z)V

    .line 398
    const-wide/16 v2, 0x12c

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 399
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_stopTranscoding(I)I

    .line 402
    :try_start_0
    sget-object v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v2, "transThread.join();"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseWakeLock()V

    .line 409
    sget-object v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop thread service_vid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    return-void

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 415
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "WakeLock.release()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 418
    :cond_0
    return-void
.end method


# virtual methods
.method protected CreateLiveThumbnailforChapterView(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)I
    .locals 21
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "videoId"    # J
    .param p5, "thumbTime"    # I
    .param p6, "filePath"    # Ljava/lang/String;

    .prologue
    .line 263
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-> CreateLiveThumbnailforChapterView() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    .line 297
    :goto_0
    return v4

    .line 267
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 269
    .local v18, "start":J
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .local v16, "multipleChapterThread":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ChapterCount:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ThreadCount:I

    div-int/2addr v4, v5

    if-ge v11, v4, :cond_3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    if-nez v4, :cond_3

    .line 272
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ThreadCount:I

    if-ge v15, v4, :cond_1

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    if-nez v4, :cond_1

    .line 273
    new-instance v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;-><init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Landroid/content/Context;)V

    .line 274
    .local v3, "chapterThread":Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->ThreadCount:I

    mul-int/2addr v4, v11

    add-int v8, v4, v15

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v9, p5

    move-object/from16 v10, p6

    invoke-virtual/range {v3 .. v10}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->setArgs(Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V

    .line 275
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->setPriority(I)V

    .line 276
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->start()V

    .line 277
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : Chapter TranscodeThread Start!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 281
    .end local v3    # "chapterThread":Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
    :cond_1
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;

    .line 283
    .local v17, "tr":Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
    :try_start_0
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 284
    :catch_0
    move-exception v2

    .line 285
    .local v2, "e":Ljava/lang/InterruptedException;
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v5, "interrupted exception !!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 288
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v17    # "tr":Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
    :cond_2
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "End Transcoding for chapter!! :: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    .line 271
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 292
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "j":I
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 293
    .local v12, "end":J
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : Chapter TranscodeThread End!! :: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v12, v18

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 297
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method protected CreateLiveThumbnailforListView(Ljava/lang/String;Ljava/lang/String;JI)I
    .locals 17
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "videoId"    # J
    .param p5, "thumbTime"    # I

    .prologue
    .line 301
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-> CreateLiveThumbnailforListView() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    .line 327
    :goto_0
    return v4

    .line 305
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 307
    .local v14, "start":J
    new-instance v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;-><init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Landroid/content/Context;)V

    .line 308
    .local v3, "transThread":Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
    const/4 v8, -0x1

    const/4 v10, 0x0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v9, p5

    invoke-virtual/range {v3 .. v10}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->setArgs(Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V

    .line 309
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->setPriority(I)V

    .line 310
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->start()V

    .line 311
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : ListView TranscodeThread Start!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    if-eqz v3, :cond_1

    .line 315
    :try_start_0
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_1
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v5, "End Transcoding for List !!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 323
    .local v12, "end":J
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : ListView TranscodeThread!! :: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v12, v14

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 327
    const/4 v4, 0x1

    goto :goto_0

    .line 316
    .end local v12    # "end":J
    :catch_0
    move-exception v2

    .line 317
    .local v2, "e":Ljava/lang/InterruptedException;
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v5, "interrupted exception !!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 251
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->serviceStop:Z

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->registerBatteryBroadcastReciever()V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->registerTranscodeReciever()V

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->registerSIOPLevelBroadcastReveiver()V

    .line 68
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPowerMgr:Landroid/os/PowerManager;

    .line 69
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTelephoneMgr:Landroid/telephony/TelephonyManager;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 83
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v3, "onDestroy()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->serviceStop:Z

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseTranscodeThread()V

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 89
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mScreenReceiver:Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    if-eqz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mScreenReceiver:Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 92
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mSIOPLevelReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_2

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mSIOPLevelReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 95
    :cond_2
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 96
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "pkgName":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 98
    sget-object v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPackageName(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->killBackgroundProcesses(Ljava/lang/String;)V

    .line 101
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseWakeLock()V

    .line 102
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_create()V

    .line 107
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_create()V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getVideowallSetting()V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Lcom/sec/android/videowall/FileMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/videowall/FileMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 115
    :cond_0
    sget-object v4, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "power? : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v5, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " / screen off : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v5, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " / ssrm.live_thumbnail : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v0, "1"

    const-string v6, "dev.ssrm.live_thumbnail"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "dev.ssrm.live_thumbnail"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v0, v2}, Lcom/sec/android/videowall/FileMgr;->setInterruptflag(Z)V

    .line 123
    sget-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTelephoneMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->isMusicRunning()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "1"

    const-string v2, "dev.ssrm.live_thumbnail"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 130
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v2, "runvideoplayer is false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v3

    .line 246
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 115
    goto :goto_0

    .line 136
    :cond_3
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;-><init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mTranscodeThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 242
    :goto_2
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const/16 v2, 0x64

    if-lt v0, v2, :cond_1

    move v1, v3

    .line 243
    goto :goto_1

    .line 239
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseTranscodeThread()V

    goto :goto_2
.end method
