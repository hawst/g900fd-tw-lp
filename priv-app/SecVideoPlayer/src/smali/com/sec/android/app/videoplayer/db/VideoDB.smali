.class public Lcom/sec/android/app/videoplayer/db/VideoDB;
.super Ljava/lang/Object;
.source "VideoDB.java"


# static fields
.field public static final ALLSHARE_DB_URI:Landroid/net/Uri;

.field private static final DEFAULT_WHERE_CLAUSE_FOLDER_VIEW:Ljava/lang/String; = "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

.field public static final EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

.field public static final EXTERNAL_MEDIA_DB_URI_2Str:Ljava/lang/String;

.field public static final INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

.field private static final RECENTLY_PLAYED_COLUMN:Ljava/lang/String; = "isPlayed"

.field public static final SEARCH_ESCAPE_STRING:Ljava/lang/String; = " escape \'!\'"

.field private static final TAG:Ljava/lang/String; = "VideoDB"

.field private static final TITLE_ASCENDING_ORDER:Ljava/lang/String; = " COLLATE LOCALIZED ASC"

.field public static final VIDEOHUB_DB_URI:Landroid/net/Uri;

.field public static final VIDEO_DB_COLUMN_RESUME_POS:Ljava/lang/String; = "resumePos"

.field private static mInstance:Lcom/sec/android/app/videoplayer/db/VideoDB;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 46
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 48
    sget-object v0, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    .line 50
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->VIDEOHUB_DB_URI:Landroid/net/Uri;

    .line 52
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI_2Str:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mInstance:Lcom/sec/android/app/videoplayer/db/VideoDB;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    .line 63
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const-class v1, Lcom/sec/android/app/videoplayer/db/VideoDB;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mInstance:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/db/VideoDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mInstance:Lcom/sec/android/app/videoplayer/db/VideoDB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mInstance:Lcom/sec/android/app/videoplayer/db/VideoDB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private excludeDRMFiles(Landroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 22
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "mListType"    # I

    .prologue
    .line 263
    if-nez p1, :cond_0

    .line 264
    const/4 v4, 0x0

    .line 315
    :goto_0
    return-object v4

    .line 266
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-nez v19, :cond_2

    .line 267
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 268
    const/4 v4, 0x0

    goto :goto_0

    .line 271
    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    move/from16 v0, v19

    new-array v12, v0, [I

    .line 272
    .local v12, "noDRMIndex":[I
    const/4 v2, 0x0

    .line 273
    .local v2, "Index":I
    const/4 v10, 0x0

    .line 274
    .local v10, "noDRMCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v6

    .line 277
    .local v6, "drmUtil":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    :cond_3
    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 279
    .local v5, "data":Ljava/lang/String;
    invoke-virtual {v6, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 280
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "noDRMCount":I
    .local v11, "noDRMCount":I
    aput v2, v12, v10

    move v10, v11

    .line 282
    .end local v11    # "noDRMCount":I
    .restart local v10    # "noDRMCount":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 283
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-nez v19, :cond_3

    .line 285
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    move/from16 v0, v19

    if-ne v10, v0, :cond_5

    .line 286
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-object/from16 v4, p1

    .line 287
    goto :goto_0

    .line 288
    :cond_5
    if-nez v10, :cond_6

    .line 289
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 290
    const/16 p1, 0x0

    .line 291
    const/4 v4, 0x0

    goto :goto_0

    .line 294
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v3

    .line 295
    .local v3, "cols":[Ljava/lang/String;
    new-instance v4, Landroid/database/MatrixCursor;

    invoke-direct {v4, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 297
    .local v4, "cursorToReturn":Landroid/database/MatrixCursor;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v10, :cond_7

    .line 298
    aget v19, v12, v8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 299
    const-string v19, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 300
    .local v18, "videoId":Ljava/lang/String;
    const-string v19, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 301
    .local v17, "title":Ljava/lang/String;
    const-string v19, "duration"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 302
    .local v7, "duration":Ljava/lang/String;
    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 303
    const-string v19, "isPlayed"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 304
    .local v13, "played":I
    const-string v19, "resumePos"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 305
    .local v14, "resumePos":J
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    .line 306
    .local v16, "resumePosition":Ljava/lang/String;
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 307
    .local v9, "isPlayed":Ljava/lang/String;
    const-string v19, "VideoDB"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "isPlayed = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", resumePosition : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const/16 v19, 0x6

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v18, v19, v20

    const/16 v20, 0x1

    aput-object v17, v19, v20

    const/16 v20, 0x2

    aput-object v7, v19, v20

    const/16 v20, 0x3

    aput-object v5, v19, v20

    const/16 v20, 0x4

    aput-object v16, v19, v20

    const/16 v20, 0x5

    aput-object v9, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 297
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 313
    .end local v7    # "duration":Ljava/lang/String;
    .end local v9    # "isPlayed":Ljava/lang/String;
    .end local v13    # "played":I
    .end local v14    # "resumePos":J
    .end local v16    # "resumePosition":Ljava/lang/String;
    .end local v17    # "title":Ljava/lang/String;
    .end local v18    # "videoId":Ljava/lang/String;
    :cond_7
    const-string v19, "VideoDB"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "excludeDRMFiles. cursorToReturn count = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->moveToFirst()Z

    goto/16 :goto_0
.end method

.method private fetchInt(Landroid/net/Uri;Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v8, -0x1

    .line 829
    if-nez p1, :cond_1

    move v0, v8

    .line 849
    :cond_0
    :goto_0
    return v0

    .line 832
    :cond_1
    const/4 v6, 0x0

    .line 833
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 837
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 838
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 839
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 840
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 845
    if-eqz v6, :cond_0

    .line 846
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 845
    :cond_2
    if-eqz v6, :cond_3

    .line 846
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move v0, v8

    .line 849
    goto :goto_0

    .line 842
    :catch_0
    move-exception v7

    .line 843
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnLong() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " column:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 845
    if-eqz v6, :cond_3

    .line 846
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 845
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 846
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private fetchLong(Landroid/net/Uri;Ljava/lang/String;)J
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    .line 853
    if-nez p1, :cond_1

    move-wide v0, v8

    .line 873
    :cond_0
    :goto_0
    return-wide v0

    .line 856
    :cond_1
    const/4 v6, 0x0

    .line 857
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 861
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 862
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 863
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 864
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 869
    if-eqz v6, :cond_0

    .line 870
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 869
    :cond_2
    if-eqz v6, :cond_3

    .line 870
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-wide v0, v8

    .line 873
    goto :goto_0

    .line 866
    :catch_0
    move-exception v7

    .line 867
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnLong() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " column:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 869
    if-eqz v6, :cond_3

    .line 870
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 869
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 870
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;
    .locals 2

    .prologue
    .line 74
    const-class v0, Lcom/sec/android/app/videoplayer/db/VideoDB;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->mInstance:Lcom/sec/android/app/videoplayer/db/VideoDB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 246
    if-nez p0, :cond_0

    .line 247
    const-string p0, "_"

    .line 253
    :goto_0
    return-object p0

    .line 249
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 250
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 251
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 252
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 253
    goto :goto_0
.end method


# virtual methods
.method public deleteVideo(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 906
    if-nez p1, :cond_1

    .line 912
    :cond_0
    :goto_0
    return v0

    .line 909
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v1, p1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 910
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 877
    if-nez p1, :cond_1

    move-object v0, v8

    .line 897
    :cond_0
    :goto_0
    return-object v0

    .line 880
    :cond_1
    const/4 v6, 0x0

    .line 881
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 885
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 886
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 887
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 888
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 893
    if-eqz v6, :cond_0

    .line 894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 893
    :cond_2
    if-eqz v6, :cond_3

    .line 894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v8

    .line 897
    goto :goto_0

    .line 890
    :catch_0
    move-exception v7

    .line 891
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnString() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " column:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 893
    if-eqz v6, :cond_3

    .line 894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 893
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public get360Video(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    .line 364
    const-string v1, "is_360_video"

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchInt(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlbum(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 376
    const-string v1, "album"

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "VideoDB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAlbum. Album : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    return-object v0
.end method

.method public getArtist(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 369
    const-string v1, "artist"

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 370
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "VideoDB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getArtist. Artist : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    return-object v0
.end method

.method public getBucketIdbyPath(Ljava/lang/String;)I
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 388
    const/4 v8, -0x1

    .line 389
    .local v8, "id":I
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "bucket_id"

    aput-object v0, v2, v1

    .line 392
    .local v2, "cols":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 394
    .local v10, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data = ?"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    new-array v4, v3, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 398
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 401
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 403
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 404
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 406
    .local v9, "sBucketId":Ljava/lang/String;
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 414
    .end local v9    # "sBucketId":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 415
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 417
    :cond_1
    :goto_0
    return v8

    .line 409
    :catch_0
    move-exception v7

    .line 410
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectionArg - exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    if-eqz v6, :cond_1

    .line 415
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 411
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v7

    .line 412
    .local v7, "e":Ljava/lang/RuntimeException;
    :try_start_2
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBucketIdbyPath - RuntimeException :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 414
    if-eqz v6, :cond_1

    .line 415
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 414
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 415
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getDateTaken(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 324
    const-string v0, "date_modified"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 348
    const-string v0, "_display_name"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDurationTime(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 328
    const-string v0, "duration"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileId(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 332
    const-string v0, "_id"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileIdByPath(Ljava/lang/String;)J
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 461
    if-eqz p1, :cond_0

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-wide v0, v2

    .line 477
    :cond_1
    :goto_0
    return-wide v0

    .line 464
    :cond_2
    sget-object v4, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 466
    .local v0, "id":J
    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    .line 467
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 469
    :cond_3
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v4, :cond_4

    .line 470
    cmp-long v4, v0, v2

    if-nez v4, :cond_4

    .line 471
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 474
    :cond_4
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 475
    sget-object v2, Lcom/sec/android/app/videoplayer/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 734
    const-wide/16 v8, -0x1

    .line 735
    .local v8, "id":J
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 738
    .local v2, "cols":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 739
    .local v10, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data LIKE \'%\' || ? || \'%\'"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 740
    const/4 v6, 0x0

    .line 742
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v4, v1

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 745
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 746
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 747
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 752
    :cond_0
    if-eqz v6, :cond_1

    .line 753
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 756
    :cond_1
    :goto_0
    return-wide v8

    .line 749
    :catch_0
    move-exception v7

    .line 750
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFileIdByPath() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 752
    if-eqz v6, :cond_1

    .line 753
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 752
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 753
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFileIdByPathForAllShare(Ljava/lang/String;)J
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 766
    const-wide/16 v8, -0x1

    .line 768
    .local v8, "id":J
    if-eqz p1, :cond_0

    const-string v0, "sshttp://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    const-string v0, "sshttp://"

    const-string v1, "http://"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 771
    :cond_0
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    .line 774
    .local v2, "cols":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 775
    .local v10, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data LIKE \'%\' || ? || \'%\'"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    const/4 v6, 0x0

    .line 779
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 781
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 782
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 783
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 788
    :cond_1
    if-eqz v6, :cond_2

    .line 789
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 792
    :cond_2
    :goto_0
    return-wide v8

    .line 785
    :catch_0
    move-exception v7

    .line 786
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFileIdByPathForAllShare() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 788
    if-eqz v6, :cond_2

    .line 789
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 788
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 789
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getFileNameByMMS(Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 703
    const-string v8, " "

    .line 704
    .local v8, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 707
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 708
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 709
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 710
    const-string v0, "cl"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 711
    new-instance v9, Ljava/lang/String;

    const-string v0, "ISO-8859-1"

    invoke-virtual {v8, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v9, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v8    # "name":Ljava/lang/String;
    .local v9, "name":Ljava/lang/String;
    move-object v8, v9

    .line 719
    .end local v9    # "name":Ljava/lang/String;
    .restart local v8    # "name":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 720
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 724
    :cond_1
    :goto_0
    return-object v8

    .line 716
    :catch_0
    move-exception v7

    .line 717
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFilePathbyId() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 719
    if-eqz v6, :cond_1

    .line 720
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 719
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 720
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 340
    const-string v0, "_data"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilePathById(J)Ljava/lang/String;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 481
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_1

    .line 482
    const/4 v0, 0x0

    .line 492
    :cond_0
    :goto_0
    return-object v0

    .line 484
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 486
    .local v0, "path":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 487
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 489
    :cond_2
    if-nez v0, :cond_0

    .line 490
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "id"    # Ljava/lang/Long;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 802
    const/4 v8, 0x0

    .line 803
    .local v8, "path":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 806
    .local v2, "cols":[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 807
    .local v9, "where":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id LIKE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808
    const/4 v6, 0x0

    .line 811
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 813
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 814
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 820
    :cond_0
    if-eqz v6, :cond_1

    .line 821
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 824
    :cond_1
    :goto_0
    return-object v8

    .line 817
    :catch_0
    move-exception v7

    .line 818
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFilePathById() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 820
    if-eqz v6, :cond_1

    .line 821
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 820
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 821
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFileSize(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 360
    const-string v0, "_size"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFinDoRegexCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "strFilter"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 982
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    .local v1, "strFilterList":[Ljava/lang/String;
    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 985
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFinDoRegexCursor([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFinDoRegexCursor([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "strFilter"    # [Ljava/lang/String;
    .param p2, "sTimeStr"    # Ljava/lang/String;
    .param p3, "eTimeStr"    # Ljava/lang/String;
    .param p4, "location"    # Ljava/lang/String;
    .param p5, "weather"    # Ljava/lang/String;

    .prologue
    .line 990
    const/4 v3, 0x0

    .line 991
    .local v3, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 992
    .local v7, "orderByClause":Ljava/lang/String;
    const/4 v4, 0x0

    .line 993
    .local v4, "cols":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 995
    .local v8, "cursorVideo":Landroid/database/Cursor;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 997
    .local v16, "stringBuilder":Ljava/lang/StringBuilder;
    sget-object v3, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 998
    const-string v7, "title COLLATE LOCALIZED ASC"

    .line 999
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getFindoRegexVideoColumns()[Ljava/lang/String;

    move-result-object v4

    .line 1001
    const-string v2, "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    if-eqz p1, :cond_3

    .line 1004
    const-string v2, " AND ("

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v13, v2, :cond_2

    .line 1006
    aget-object v2, p1, v13

    if-eqz v2, :cond_0

    aget-object v2, p1, v13

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1007
    rem-int/lit8 v2, v13, 0x2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    .line 1008
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v5, p1, v13

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    :cond_0
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 1010
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "title like \'%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v5, p1, v13

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1015
    :cond_2
    const-string v2, ")"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1018
    .end local v13    # "i":I
    :cond_3
    if-eqz p2, :cond_4

    if-eqz p3, :cond_4

    .line 1019
    invoke-static/range {p2 .. p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 1020
    .local v14, "sTime":J
    invoke-static/range {p3 .. p3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1021
    .local v10, "eTime":J
    const-string v2, " AND "

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1022
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "( datetaken >= \'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\' AND datetaken <= \'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\' )"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1026
    .end local v10    # "eTime":J
    .end local v14    # "sTime":J
    :cond_4
    if-eqz p4, :cond_5

    .line 1027
    if-nez p1, :cond_7

    .line 1028
    const-string v2, " AND "

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1032
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ( addr like \'%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%\')"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1035
    :cond_5
    if-eqz p5, :cond_6

    .line 1036
    if-nez p1, :cond_8

    .line 1037
    const-string v2, " AND "

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1041
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ( weather_ID like \'%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%\')"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044
    :cond_6
    const-string v2, "VideoDB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFindoRegexCursor query string : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v8

    .line 1059
    if-nez v8, :cond_9

    .line 1060
    const/4 v2, 0x0

    .line 1068
    :goto_4
    return-object v2

    .line 1030
    :cond_7
    const-string v2, " OR "

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1039
    :cond_8
    const-string v2, " OR "

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1048
    :catch_0
    move-exception v9

    .line 1049
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "VideoDB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFindoRegexCursor - SQLiteException :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const/4 v2, 0x0

    goto :goto_4

    .line 1051
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v12

    .line 1052
    .local v12, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "VideoDB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFindoRegexCursor - UnsupportedOperationException :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    const/4 v2, 0x0

    goto :goto_4

    .line 1054
    .end local v12    # "ex":Ljava/lang/UnsupportedOperationException;
    :catch_2
    move-exception v9

    .line 1055
    .local v9, "e":Ljava/lang/RuntimeException;
    const-string v2, "VideoDB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFindoRegexCursor - RuntimeException :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    const/4 v2, 0x0

    goto :goto_4

    .line 1061
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :cond_9
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_a

    .line 1062
    const-string v2, "VideoDB"

    const-string v5, "getFindoRegexCursor - cursorVideo.getCount() <= 0"

    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v8

    .line 1063
    goto :goto_4

    .line 1066
    :cond_a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v2, v8

    .line 1068
    goto :goto_4
.end method

.method public getFinDoTagCursor(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14
    .param p1, "startIdStr"    # Ljava/lang/String;
    .param p2, "endIdStr"    # Ljava/lang/String;

    .prologue
    .line 926
    const/4 v1, 0x0

    .line 927
    .local v1, "uri":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 928
    .local v5, "orderByClause":Ljava/lang/String;
    const/4 v2, 0x0

    .line 929
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 931
    .local v6, "cursorVideo":Landroid/database/Cursor;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 933
    .local v11, "stringBuilder":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 934
    const-string v5, "title COLLATE LOCALIZED ASC"

    .line 935
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getFindoTagVideoColumns()[Ljava/lang/String;

    move-result-object v2

    .line 937
    const-string v0, "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    if-eqz p1, :cond_0

    .line 941
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v12, v0

    .line 943
    .local v12, "startId":J
    if-nez p2, :cond_1

    .line 944
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND _id == \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 954
    .end local v12    # "startId":J
    :cond_0
    :goto_0
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFindoTagCursor query string : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    .line 969
    if-nez v6, :cond_2

    .line 970
    const/4 v0, 0x0

    .line 978
    :goto_1
    return-object v0

    .line 947
    .restart local v12    # "startId":J
    :cond_1
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    .line 949
    .local v8, "endId":J
    const-string v0, " AND "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 950
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "( _id >= \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\' AND _id <= \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\' )"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 958
    .end local v8    # "endId":J
    .end local v12    # "startId":J
    :catch_0
    move-exception v7

    .line 959
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFindoTagCursor - SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const/4 v0, 0x0

    goto :goto_1

    .line 961
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v10

    .line 962
    .local v10, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFindoTagCursor - UnsupportedOperationException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    const/4 v0, 0x0

    goto :goto_1

    .line 964
    .end local v10    # "ex":Ljava/lang/UnsupportedOperationException;
    :catch_2
    move-exception v7

    .line 965
    .local v7, "e":Ljava/lang/RuntimeException;
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFindoTagCursor - RuntimeException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 971
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_3

    .line 972
    const-string v0, "VideoDB"

    const-string v3, "getFindoTagCursor - cursorVideo.getCount() <= 0"

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 973
    goto/16 :goto_1

    .line 976
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v0, v6

    .line 978
    goto/16 :goto_1
.end method

.method public getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 352
    const-string v0, "mime_type"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteItemSeed(J)Ljava/lang/String;
    .locals 11
    .param p1, "videoID"    # J

    .prologue
    const/4 v10, 0x0

    .line 663
    const-wide/16 v4, 0x1

    cmp-long v0, p1, v4

    if-gez v0, :cond_0

    .line 664
    const-string v8, ""

    .line 694
    :goto_0
    return-object v8

    .line 667
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 668
    .local v1, "uri":Landroid/net/Uri;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 669
    .local v9, "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRemoteItemSeed. uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "seed"

    aput-object v0, v2, v10

    .line 676
    .local v2, "cols":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 677
    .local v8, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 680
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 681
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 682
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 683
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 688
    :cond_1
    if-eqz v6, :cond_2

    .line 689
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 693
    :cond_2
    :goto_1
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRemoteItemSeed. seed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 685
    :catch_0
    move-exception v7

    .line 686
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRemoteItemSeed() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 688
    if-eqz v6, :cond_2

    .line 689
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 688
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 689
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getResolution(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 356
    const-string v0, "resolution"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResumePosition(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 336
    const-string v0, "resumePos"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSeekModeForAllShare(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 921
    const-string v0, "seekmode"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 320
    const-string v0, "_size"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSortOrderString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 83
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "sortorder"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    .line 84
    .local v1, "sortOrder":I
    const-string v2, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSortOrderString() order:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    packed-switch v1, :pswitch_data_0

    .line 107
    const-string v2, "title COLLATE LOCALIZED ASC"

    :goto_0
    return-object v2

    .line 88
    :pswitch_0
    const-string v2, "title COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 91
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "(date_added * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v2, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    const-string v2, "(datetaken * 1) DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 98
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v2, "(_size * 1) DESC"

    goto :goto_0

    .line 101
    :pswitch_3
    const-string v2, "mime_type ASC"

    goto :goto_0

    .line 104
    :pswitch_4
    const-string v2, "isPlayed DESC"

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getTitleName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 344
    const-string v0, "title"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUriById(J)Landroid/net/Uri;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 496
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gez v2, :cond_1

    .line 497
    const/4 v1, 0x0

    .line 511
    :cond_0
    :goto_0
    return-object v1

    .line 499
    :cond_1
    const/4 v1, 0x0

    .line 500
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 502
    .local v0, "path":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 503
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 505
    :cond_2
    if-nez v0, :cond_3

    .line 506
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePathById(Ljava/lang/Long;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 508
    :cond_3
    if-eqz v0, :cond_0

    .line 509
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public getUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const-wide/16 v8, -0x1

    .line 426
    if-eqz p1, :cond_0

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v2, v3

    .line 457
    :cond_1
    :goto_0
    return-object v2

    .line 429
    :cond_2
    const/4 v2, 0x0

    .line 430
    .local v2, "uri":Landroid/net/Uri;
    const-wide/16 v0, -0x1

    .line 432
    .local v0, "id":J
    cmp-long v4, v0, v8

    if-nez v4, :cond_3

    .line 433
    sget-object v4, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 434
    sget-object v4, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 437
    :cond_3
    cmp-long v4, v0, v8

    if-nez v4, :cond_4

    .line 438
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 439
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 442
    :cond_4
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v4, :cond_5

    .line 443
    cmp-long v4, v0, v8

    if-nez v4, :cond_5

    .line 444
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 445
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 449
    :cond_5
    cmp-long v4, v0, v8

    if-nez v4, :cond_6

    .line 450
    sget-object v4, Lcom/sec/android/app/videoplayer/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 451
    sget-object v4, Lcom/sec/android/app/videoplayer/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 454
    :cond_6
    cmp-long v4, v0, v8

    if-nez v4, :cond_1

    move-object v2, v3

    .line 455
    goto :goto_0
.end method

.method public getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;
    .locals 6
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z

    .prologue
    .line 178
    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZI)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursor(IZLjava/lang/String;ZI)Landroid/database/Cursor;
    .locals 10
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z
    .param p5, "listType"    # I

    .prologue
    .line 191
    const/4 v1, 0x0

    .line 192
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    .line 193
    .local v5, "orderByClause":Ljava/lang/String;
    const/4 v2, 0x0

    .line 194
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 195
    .local v6, "cursorVideo":Landroid/database/Cursor;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    .local v9, "stringBuilder":Ljava/lang/StringBuilder;
    const/16 v0, 0xb

    if-eq p5, v0, :cond_0

    const/16 v0, 0xc

    if-ne p5, v0, :cond_3

    .line 199
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    .line 200
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getAllShareVideoColumns()[Ljava/lang/String;

    move-result-object v2

    .line 219
    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    .line 231
    if-eqz p4, :cond_2

    .line 232
    const/4 v0, 0x1

    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->excludeDRMFiles(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v6

    .line 234
    :cond_2
    if-nez v6, :cond_6

    .line 235
    const/4 v0, 0x0

    .line 242
    :goto_1
    return-object v0

    .line 201
    :cond_3
    const/16 v0, 0xe

    if-ne p5, v0, :cond_4

    .line 202
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->VIDEOHUB_DB_URI:Landroid/net/Uri;

    .line 203
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getVideoHubColumns()[Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 205
    :cond_4
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 206
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v2

    .line 207
    const-string v0, "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    if-nez p2, :cond_5

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND bucket_id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_5
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND title like \'%"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "%\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " escape \'!\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 220
    :catch_0
    move-exception v7

    .line 221
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoCursor - SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x0

    goto :goto_1

    .line 223
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 224
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoCursor - UnsupportedOperationException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 226
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :catch_2
    move-exception v7

    .line 227
    .local v7, "e":Ljava/lang/RuntimeException;
    const-string v0, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoCursor - RuntimeException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 236
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_7

    .line 237
    const-string v0, "VideoDB"

    const-string v3, "cursorVideo.getCount() <= 0"

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 238
    goto/16 :goto_1

    .line 241
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v0, v6

    .line 242
    goto/16 :goto_1
.end method

.method public getVideoCursorExternalType(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "filePath":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursorExternalType(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getVideoCursorExternalType(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 125
    if-nez p1, :cond_0

    .line 126
    const-string v0, "VideoDB"

    const-string v4, "getVideoCursorExternalType(). path is null"

    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v11

    .line 166
    :goto_0
    return-object v7

    .line 130
    :cond_0
    const/4 v6, 0x0

    .line 131
    .local v6, "bucketId":I
    const-string v10, ""

    .line 132
    .local v10, "parentPath":Ljava/lang/String;
    const/4 v7, 0x0

    .line 135
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v9

    .line 137
    .local v9, "parentFile":Ljava/io/File;
    if-nez v9, :cond_1

    .line 138
    const-string v0, "VideoDB"

    const-string v4, "getVideoCursorExternalType(). parentFile is null"

    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v11

    .line 139
    goto :goto_0

    .line 141
    :cond_1
    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    .line 142
    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v6

    .line 145
    const-string v0, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getVideoCursorExternalType(). bucketId : "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 149
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    .line 152
    .local v2, "cols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id = \'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "selection":Ljava/lang/String;
    const-string v5, "_display_name COLLATE LOCALIZED ASC"

    .line 156
    .local v5, "sortOrder":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_0

    .line 157
    :catch_0
    move-exception v8

    .line 158
    .local v8, "ex":Landroid/database/sqlite/SQLiteException;
    if-eqz v7, :cond_2

    .line 159
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 160
    const/4 v7, 0x0

    .line 162
    :cond_2
    const-string v0, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getVideoCursorExternalType. SQLiteException :"

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v11

    .line 163
    goto/16 :goto_0
.end method

.method public saveRecentlyPlayedTime(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 545
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v0, v4

    .line 547
    .local v0, "currentTime":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 548
    .local v2, "val":Landroid/content/ContentValues;
    const-string v3, "isPlayed"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 551
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 555
    :goto_0
    return-void

    .line 552
    :catch_0
    move-exception v1

    .line 553
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveRecentlyPlayedTime - exception :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setContentDuration(Landroid/net/Uri;J)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "pos"    # J

    .prologue
    .line 564
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setContentDuration() - pos : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const/4 v0, 0x0

    .line 567
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 568
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "duration"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 571
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 576
    :goto_0
    return v0

    .line 572
    :catch_0
    move-exception v1

    .line 573
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setResumePosition - Exception occured:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setResumePosition(Landroid/net/Uri;J)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "pos"    # J

    .prologue
    .line 523
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setResumePosition() - pos : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const/4 v0, 0x0

    .line 526
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 527
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "resumePos"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 530
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 535
    :goto_0
    return v0

    .line 531
    :catch_0
    move-exception v1

    .line 532
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setResumePosition - Exception occured:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateLastPlayedItem(Landroid/net/Uri;)V
    .locals 12
    .param p1, "curFileUri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 586
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateLastPlayedItem E. curFileUri = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 589
    :cond_0
    const-string v0, "VideoDB"

    const-string v1, "updateLastPlayedItem. context or uri is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    :cond_1
    :goto_0
    return-void

    .line 593
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".pyv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 597
    :cond_3
    const-string v0, "VideoDB"

    const-string v1, "updateLastPlayedItem. this file is not local file"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 601
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 604
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_5

    .line 605
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 607
    .local v9, "newValue":Landroid/content/ContentValues;
    const-string v0, "duration"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDurationTime(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 608
    const-string v0, "_size"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileSize(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 609
    const-string v0, "mime_type"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const-string v0, "date_modified"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDateTaken(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 612
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 613
    const-string v0, "title"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const-string v0, "resolution"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResolution(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v0, "_data"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_7

    .line 624
    const-string v0, "_id"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 627
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    .line 629
    .local v8, "newUri":Landroid/net/Uri;
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateLastPlayedItme. insert result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    .end local v8    # "newUri":Landroid/net/Uri;
    .end local v9    # "newValue":Landroid/content/ContentValues;
    :cond_5
    :goto_2
    if-eqz v6, :cond_1

    .line 649
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 650
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 651
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 617
    .restart local v9    # "newValue":Landroid/content/ContentValues;
    :cond_6
    const-string v0, "title"

    const-string v1, "Name"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v0, "resolution"

    const-string v1, "1280X800"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    const-string v0, "_data"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 630
    :catch_0
    move-exception v7

    .line 631
    .local v7, "e":Landroid/database/SQLException;
    const-string v0, "VideoDB"

    const-string v1, "updateLastPlayedItme. insert result : SQLException"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 635
    .end local v7    # "e":Landroid/database/SQLException;
    :cond_7
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 636
    .local v11, "where":Ljava/lang/StringBuilder;
    const-string v0, "_id=1"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->getVideoProviderContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 641
    .local v10, "ret":I
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateLastPlayedItem. update result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 642
    .end local v10    # "ret":I
    :catch_1
    move-exception v7

    .line 643
    .local v7, "e":Landroid/database/sqlite/SQLiteConstraintException;
    const-string v0, "VideoDB"

    const-string v1, "updateLastPlayedItem. update result : SQLiteConstraintException"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
