.class Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;
.super Landroid/os/Handler;
.source "HelpClip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/help/HelpClip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 239
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$300(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Landroid/os/Handler;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 241
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 264
    :goto_0
    return-void

    .line 243
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$100(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/view/help/HelpData;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$000(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    if-nez v1, :cond_1

    .line 244
    :cond_0
    const-string v1, "HelpClip"

    const-string v2, "handleMessage E : mData or mServiceUtil is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # invokes: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->getBtnPosition()I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$400(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)I

    move-result v0

    .line 249
    .local v0, "pos":I
    const-string v1, "HelpClip"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage E :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mPrevPos:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$500(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->updateContents(I)V

    .line 253
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # invokes: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runHandler()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$600(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)V

    goto :goto_0

    .line 257
    .end local v0    # "pos":I
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->changeButtonLayout(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$700(Lcom/sec/android/app/videoplayer/view/help/HelpClip;Z)V

    goto :goto_0

    .line 241
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method
