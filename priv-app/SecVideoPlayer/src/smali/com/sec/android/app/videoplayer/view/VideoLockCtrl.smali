.class public Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
.super Ljava/lang/Object;
.source "VideoLockCtrl.java"


# static fields
.field private static final HIDE_LOCK_ICON:I

.field private static mbLockMode:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mLockBtn:Landroid/widget/ImageButton;

.field private mLockBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mLockCtrlView:Landroid/widget/RelativeLayout;

.field private mParentView:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mbLockMode:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    .line 48
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 84
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$2;-><init>(Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getLockState()Z
    .locals 1

    .prologue
    .line 101
    sget-boolean v0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mbLockMode:Z

    return v0
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0176

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 46
    return-void
.end method

.method public static setLockState(Z)V
    .locals 0
    .param p0, "mode"    # Z

    .prologue
    .line 97
    sput-boolean p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mbLockMode:Z

    .line 98
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 33
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    .line 35
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 36
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030027

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->initViews()V

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 39
    return-void
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 119
    :cond_1
    return-void
.end method

.method public hideLockIcon()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 82
    return-void
.end method

.method public isLockBtnVisible()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    .line 108
    return-void
.end method

.method public showLockIcon()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 77
    return-void
.end method
