.class Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;
.super Landroid/database/ContentObserver;
.source "VideoBrightnessPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Z)V

    .line 218
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1000(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Z)V

    .line 226
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1000(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0a0025

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
