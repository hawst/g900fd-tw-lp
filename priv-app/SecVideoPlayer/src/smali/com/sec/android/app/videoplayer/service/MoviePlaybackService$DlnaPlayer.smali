.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/service/IPlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DlnaPlayer"
.end annotation


# instance fields
.field mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 1

    .prologue
    .line 2140
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2141
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 2142
    return-void
.end method


# virtual methods
.method public changePlayer(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 2230
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DlnaPlayer :: changePlayer() - mode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2231
    if-nez p1, :cond_0

    .line 2232
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setPlayer(Lcom/sec/android/app/videoplayer/service/IPlayer;)V

    .line 2234
    :cond_0
    return-void
.end method

.method public duration()J
    .locals 2

    .prologue
    .line 2203
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getDuration()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 2169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isInitialized()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 2198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 2190
    const-string v0, "MoviePlaybackService"

    const-string v1, "DlnaPlayer :: pause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 2192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->pause()V

    .line 2194
    :cond_0
    return-void
.end method

.method public position()J
    .locals 2

    .prologue
    .line 2208
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public realSeek(II)J
    .locals 4
    .param p1, "whereto"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 2222
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 2223
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->seek(J)V

    .line 2225
    :cond_0
    int-to-long v0, p1

    return-wide v0
.end method

.method public seek(J)J
    .locals 3
    .param p1, "whereto"    # J

    .prologue
    .line 2213
    const-string v0, "MoviePlaybackService"

    const-string v1, "DlnaPlayer :: seek()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2214
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 2215
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->seek(J)V

    .line 2217
    :cond_0
    return-wide p1
.end method

.method public setDataSourcePrepare(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2146
    if-nez p1, :cond_1

    .line 2164
    :cond_0
    :goto_0
    return-void

    .line 2149
    :cond_1
    const-string v0, "MoviePlaybackService"

    const-string v1, "DlnaPlayer :: setDataSourcePrepare()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2151
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 2152
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->notifyPlayerState()V

    .line 2154
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isContentChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setContentChanged(Z)V

    .line 2156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->stop()V

    .line 2160
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2161
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->play()V

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 2174
    const-string v0, "MoviePlaybackService"

    const-string v1, "DlnaPlayer :: start()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2175
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 2176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->play()V

    .line 2178
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 2182
    const-string v0, "MoviePlaybackService"

    const-string v1, "DlnaPlayer :: stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2183
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 2184
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->stop()V

    .line 2186
    :cond_0
    return-void
.end method
