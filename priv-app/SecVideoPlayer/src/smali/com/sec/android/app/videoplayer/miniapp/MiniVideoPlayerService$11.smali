.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1682
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1685
    const-string v0, "MiniVideoPlayerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mInfoListener. info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1686
    sparse-switch p2, :sswitch_data_0

    .line 1738
    :goto_0
    :sswitch_0
    return v3

    .line 1695
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->createAudioOnlyClipWindow()V

    goto :goto_0

    .line 1703
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a017c

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    .line 1704
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->createAudioOnlyClipWindow()V

    goto :goto_0

    .line 1708
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a017a

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    goto :goto_0

    .line 1712
    :sswitch_4
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "mInfoListener. MEDIA_INFO_BUFFERING_START Show Loading Dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1713
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    goto :goto_0

    .line 1717
    :sswitch_5
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "mInfoListener. MEDIA_INFO_BUFFERING_END hide Loading Dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1718
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    goto :goto_0

    .line 1722
    :sswitch_6
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "mInfoListener. MEDIA_INFO_VIDEO_RENDERING_START hide Loading Dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1723
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1724
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setVideoRenderingStarted()V

    .line 1725
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1726
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSubtitleSyncTime()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 1730
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 1731
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    goto :goto_0

    .line 1686
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_6
        0x2bc -> :sswitch_0
        0x2bd -> :sswitch_4
        0x2be -> :sswitch_5
        0x320 -> :sswitch_0
        0x321 -> :sswitch_0
        0x3b6 -> :sswitch_3
        0x3b7 -> :sswitch_2
        0x3cc -> :sswitch_0
        0x3cd -> :sswitch_1
    .end sparse-switch
.end method
