.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 3645
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x1

    .line 3647
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3649
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsCoverOpen:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3724
    :cond_1
    :goto_0
    return-void

    .line 3653
    :cond_2
    const-string v2, "command"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3654
    .local v1, "cmd":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mBtMediaBtnReceiver - cmd : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3660
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setBTMediaBtnStatus(Ljava/lang/String;)V

    .line 3662
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3663
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 3664
    :cond_3
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_PLAY_CMD:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3665
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->resumeOrStartPlaying()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 3666
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3667
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    goto :goto_0

    .line 3668
    :cond_5
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_UP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_UP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3670
    :cond_6
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "KEYCODE_MEDIA_REWIND/KEYCODE_MEDIA_FAST_FORWARD up"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3671
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 3673
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_7

    .line 3674
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 3677
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v2

    if-lez v2, :cond_1

    .line 3678
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendSeekBroadcast(Ljava/lang/String;)V

    .line 3679
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3302(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    goto/16 :goto_0

    .line 3681
    :cond_8
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3682
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[c] VIDEO_MEDIA_BTN_PLAYPAUSE"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3683
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isDialogPopupShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3684
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v2, :cond_9

    .line 3685
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->resetHoldLongSeek()V

    .line 3687
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3689
    :cond_a
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 3690
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MEDIA_BTN_NEXT"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3691
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3692
    :cond_b
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3693
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MEDIA_BTN_PREV"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3694
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3695
    :cond_c
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_DOWN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 3696
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "KEYCODE_MEDIA_FAST_FORWARD"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3697
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # operator++ for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3308(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    .line 3698
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v2

    if-ne v2, v5, :cond_d

    .line 3699
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendSeekBroadcast(Ljava/lang/String;)V

    .line 3701
    :cond_d
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 3702
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3704
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3705
    :cond_f
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_DOWN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 3706
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "KEYCODE_MEDIA_REWIND"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3707
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # operator++ for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3308(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    .line 3708
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v2

    if-ne v2, v5, :cond_10

    .line 3709
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendSeekBroadcast(Ljava/lang/String;)V

    .line 3711
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 3712
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3714
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 3715
    :cond_12
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_UPDATE_VOLUME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3716
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    .line 3718
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3400()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3720
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a0011

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 3721
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0
.end method
