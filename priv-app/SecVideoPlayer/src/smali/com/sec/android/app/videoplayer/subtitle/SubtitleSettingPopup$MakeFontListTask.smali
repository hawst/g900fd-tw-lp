.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;
.super Landroid/os/AsyncTask;
.source "SubtitleSettingPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MakeFontListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 1260
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;

    .prologue
    .line 1260
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->makeFontList()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1263
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1265
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1260
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "i"    # Ljava/lang/Boolean;

    .prologue
    .line 1270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Lcom/sec/android/app/videoplayer/view/VideoStateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Lcom/sec/android/app/videoplayer/view/VideoStateView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1902(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Lcom/sec/android/app/videoplayer/view/VideoStateView;)Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 1275
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->callSubtitleFontPopup()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$2000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    .line 1276
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1260
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
