.class public Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;
.super Ljava/lang/Object;
.source "VideoInvalidSubtitlePopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private final transient mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mOnDismissListener:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "VideoSubtitlePopup"

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mContext:Landroid/content/Context;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mOnDismissListener:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;

    .line 124
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 28
    sget-object v1, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->TAG:Ljava/lang/String;

    const-string v2, "VideoInvalidSubtitlePopup E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mContext:Landroid/content/Context;

    .line 31
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a00a5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 34
    const v1, 0x7f0a002e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 36
    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 45
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 54
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 85
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 87
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mOnDismissListener:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public deletePopup()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    .line 111
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 104
    :cond_0
    return-void
.end method

.method public getDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 117
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    const/4 v0, 0x1

    .line 121
    :cond_0
    return v0
.end method

.method public setOnDismissListener(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mOnDismissListener:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;

    .line 137
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 98
    :cond_0
    return-void
.end method
