.class Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;
.super Ljava/lang/Object;
.source "PlayerListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x0

    .line 167
    .local v2, "path":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 168
    .local v4, "resumePos":J
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v8, v8, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    invoke-virtual {v8, p3}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 169
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 170
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 171
    const-string v8, "resumePos"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 173
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$000(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    .line 174
    .local v1, "db":Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 175
    .local v7, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    .line 177
    .local v3, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    if-eqz v7, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://cloud/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://com.skp.tcloud/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 179
    :cond_1
    const/16 v8, 0x23

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->setKeyType(I)V

    .line 182
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$100(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 183
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$100(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 185
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$000(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 186
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isExternal()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 187
    const-wide/16 v4, 0x0

    .line 189
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$100(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 190
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v6

    .line 191
    .local v6, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitle()V

    .line 198
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$100(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    .line 200
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v8, v8, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->notifyDataSetInvalidated()V

    .line 201
    return-void
.end method
