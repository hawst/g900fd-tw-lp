.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;
.super Landroid/content/BroadcastReceiver;
.source "ChangePlayerListenerNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 110
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "action":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v2, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->STARTED:Z

    .line 115
    const-string v2, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/WifiDisplayStatus;

    .line 116
    .local v1, "status":Landroid/hardware/display/WifiDisplayStatus;
    if-eqz v1, :cond_0

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wifi display status changed! scanState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", activeDisplayState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->setWifiDisplayStatus(Landroid/hardware/display/WifiDisplayStatus;)V

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 122
    .end local v1    # "status":Landroid/hardware/display/WifiDisplayStatus;
    :cond_1
    return-void
.end method
