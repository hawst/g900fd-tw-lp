.class Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;
.super Landroid/os/AsyncTask;
.source "VideoSKTCloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncWithTcloud"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncWithTcloud"


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 796
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;->this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 794
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;->mContext:Landroid/content/Context;

    .line 797
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;->mContext:Landroid/content/Context;

    .line 798
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 792
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 802
    :try_start_0
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 803
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->sync(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 810
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 805
    :catch_0
    move-exception v0

    .line 806
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "SyncWithTcloud"

    const-string v2, "syncWithDropbox() - NullPointerException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 807
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 808
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SyncWithTcloud"

    const-string v2, "syncWithDropbox() - Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
