.class public Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "AutoShutdownThreadPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mOnAllTaskDoneListener:Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;

.field private final runningTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V
    .locals 9
    .param p1, "corePoolSize"    # I
    .param p2, "maximumPoolSize"    # I
    .param p3, "keepAliveTime"    # J
    .param p5, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p6, "workQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    new-instance v8, Lcom/sec/android/app/videoplayer/subtitle/threadpool/PriorityConfigurableThreadFactory;

    const/16 v0, 0xa

    invoke-direct {v8, v0}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/PriorityConfigurableThreadFactory;-><init>(I)V

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 15
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->runningTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 19
    return-void
.end method


# virtual methods
.method protected afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->runningTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 35
    .local v0, "count":I
    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->isTerminated()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->isTerminating()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 37
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->TAG:Ljava/lang/String;

    const-string v2, "Thread Pool is terminated or terminating before calling shutdown by myself. so don\'t call listener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->shutdown()V

    .line 45
    :cond_2
    return-void

    .line 39
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->mOnAllTaskDoneListener:Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;

    if-eqz v1, :cond_1

    .line 40
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->mOnAllTaskDoneListener:Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;->done()V

    goto :goto_0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "command"    # Ljava/lang/Runnable;

    .prologue
    .line 23
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->runningTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 25
    :try_start_0
    invoke-super {p0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    return-void

    .line 26
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shutdown?? e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/RejectedExecutionException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAllTaskDoneListener(Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->mOnAllTaskDoneListener:Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;

    .line 62
    return-void
.end method

.method public terminated()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->runningTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->runningTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 52
    :cond_0
    return-void
.end method
