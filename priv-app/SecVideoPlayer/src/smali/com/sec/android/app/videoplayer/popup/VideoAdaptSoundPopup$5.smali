.class Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$5;
.super Ljava/lang/Object;
.source "VideoAdaptSoundPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 193
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoAdaptSoundPopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$402(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 198
    :cond_2
    return-void
.end method
