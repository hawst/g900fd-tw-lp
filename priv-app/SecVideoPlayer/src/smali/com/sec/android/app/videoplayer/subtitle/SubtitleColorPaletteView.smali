.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
.super Landroid/view/View;
.source "SubtitleColorPaletteView.java"


# instance fields
.field mContext:Landroid/content/Context;

.field mPointer:Landroid/graphics/Bitmap;

.field mPointerX:I

.field mPointerY:I

.field mVisible:Z

.field paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mVisible:Z

    .line 22
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerX:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerY:I

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mContext:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->init()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mVisible:Z

    .line 22
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerX:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerY:I

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mContext:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->init()V

    .line 37
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02017c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerX:I

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerY:I

    .line 49
    return-void
.end method


# virtual methods
.method public getMarginX()I
    .locals 2

    .prologue
    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v0, v1, 0x2

    .line 83
    :cond_0
    return v0
.end method

.method public getMarginY()I
    .locals 2

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int/lit8 v0, v1, 0x2

    .line 91
    :cond_0
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 56
    .local v1, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v0, 0x0

    .line 57
    .local v0, "margin":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v2

    if-nez v2, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080273

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 62
    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 63
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mVisible:Z

    if-eqz v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerX:I

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerY:I

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointer:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 71
    :cond_0
    return-void

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080272

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public setPointerPos(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerX:I

    .line 75
    iput p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mPointerY:I

    .line 76
    return-void
.end method

.method public setPointerVisible(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->mVisible:Z

    .line 95
    return-void
.end method
