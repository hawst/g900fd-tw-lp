.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;
.super Landroid/content/BroadcastReceiver;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1009
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1011
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1012
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1037
    :cond_0
    :goto_0
    return-void

    .line 1015
    :cond_1
    const-string v2, "command"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1016
    .local v1, "cmd":Ljava/lang/String;
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mBtMediaBtnReceiver - cmd : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1019
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "VIDEO_MEDIA_PAUSE_CMD"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->doPauseResume()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0

    .line 1021
    :cond_2
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_PLAY_CMD:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1022
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "VIDEO_MEDIA_PLAY_CMD"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->doPauseResume()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0

    .line 1024
    :cond_3
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1025
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "VIDEO_MEDIA_BTN_STOP"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0

    .line 1027
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1028
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "VIDEO_MEDIA_BTN_PLAYPAUSE"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->doPauseResume()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0

    .line 1030
    :cond_5
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1031
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "VIDEO_MEDIA_BTN_NEXT"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->playNext()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 1033
    :cond_6
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1034
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "VIDEO_MEDIA_BTN_PREV"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->playPrevious()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0
.end method
