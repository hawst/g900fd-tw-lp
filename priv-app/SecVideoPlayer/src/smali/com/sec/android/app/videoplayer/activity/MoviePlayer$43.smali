.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;
.super Landroid/database/ContentObserver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 5989
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 5991
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerContentObserver onChange()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5993
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5994
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5995
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onChange() : AutoRotation is on - disable mOrientationEventListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5996
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 6003
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6004
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerContentObserver onChange() DONOT UPDATE FOR Came from VZW Guide Tour!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6016
    :goto_1
    return-void

    .line 5998
    :cond_1
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onChange() : AutoRotation is off - enable mOrientationEventListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5999
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    goto :goto_0

    .line 6006
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_4

    .line 6007
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6008
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen()V

    goto :goto_1

    .line 6010
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateSecondScreen()V

    goto :goto_1

    .line 6013
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen()V

    goto :goto_1
.end method
