.class Lcom/sec/android/app/videoplayer/slink/SLink$4;
.super Ljava/lang/Object;
.source "SLink.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/slink/SLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/slink/SLink;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/slink/SLink;)V
    .locals 0

    .prologue
    .line 1018
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink$4;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1021
    const-string v0, "SLink"

    const-string v1, "onServiceConnected E"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$4;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$902(Lcom/sec/android/app/videoplayer/slink/SLink;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$4;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->requestAccessToken()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$700(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    .line 1024
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 1028
    const-string v0, "SLink"

    const-string v1, "onServiceDisconnected E"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1029
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$4;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->destroySAService()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$600(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    .line 1030
    return-void
.end method
