.class final enum Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
.super Ljava/lang/Enum;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VideoGestureMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

.field public static final enum BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

.field public static final enum MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

.field public static final enum VOLUME_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 179
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    const-string v1, "MODE_UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    const-string v1, "VOLUME_MODE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    const-string v1, "BRIGHTNESS_MODE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    .line 178
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->$VALUES:[Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 178
    const-class v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->$VALUES:[Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    return-object v0
.end method
