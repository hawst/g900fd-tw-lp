.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 843
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 845
    const/4 v1, 0x0

    .line 846
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 847
    sparse-switch p2, :sswitch_data_0

    .line 866
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 867
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 885
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 850
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 852
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 856
    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 857
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->showPopupMenu()V

    goto :goto_0

    .line 870
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 872
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 873
    goto :goto_0

    .line 876
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 877
    goto :goto_0

    .line 847
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 850
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 870
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
