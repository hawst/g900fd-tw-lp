.class public Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
.super Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
.source "AsfUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$9;
    }
.end annotation


# static fields
.field private static final DIRECT_DMC_DELAY_TIME:I = 0x64

.field private static final EVENT_LISTENER_DELAY_TIME:I = 0x1f4

.field private static final PREPARE_DIRECT_DMC:I = 0x1f6

.field private static final START_DIRECT_DMC:I = 0x1f7

.field private static final START_NEWFILE:I = 0x1f4

.field private static sAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;


# instance fields
.field private final POWER_MGR_TAG:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field bWaitStateListener:Z

.field private handlingSeek:Z

.field private mAVPlayerPlaybackResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

.field private mAllShareIntentSubtitlePath:Ljava/lang/String;

.field private mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

.field private mAvPlayerCurrentPosition:J

.field private mAvPlayerDuration:J

.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mContentChanged:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;

.field private mCurrentProviderName:Ljava/lang/String;

.field private mDMCStateFromAllShareIntent:Z

.field private mDirectDmcMode:Z

.field private mDmrUdn:Ljava/lang/String;

.field private mDmsNIC:Ljava/lang/String;

.field private mError:I

.field private mExitAfterCurrentMovie:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsAllShareIntent:Z

.field private mItemFromAllShareIntent:Lcom/samsung/android/allshare/Item;

.field private mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

.field private mMsg:[I

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mPlayerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayerServiceConnectListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

.field private mPlayerState:I

.field private mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

.field private mSecAvPlayerEventListener:Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;

.field private mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

.field private mSeekableOnPausedThisDevice:Z

.field private mSelectedPlayer:Lcom/samsung/android/allshare/Device;

.field private final mServerRemovedListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mUseASF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->sAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;-><init>()V

    .line 58
    const-class v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    .line 60
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    .line 66
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 68
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 70
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;

    .line 72
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmsNIC:Ljava/lang/String;

    .line 74
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 76
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    .line 80
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z

    .line 82
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 88
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    .line 92
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 94
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    .line 96
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmrUdn:Ljava/lang/String;

    .line 98
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDirectDmcMode:Z

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAllShareIntentSubtitlePath:Ljava/lang/String;

    .line 111
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mIsAllShareIntent:Z

    .line 113
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDMCStateFromAllShareIntent:Z

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    .line 119
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.sec.android.app.videoplayer."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->POWER_MGR_TAG:Ljava/lang/String;

    .line 123
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    .line 125
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z

    .line 533
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$1;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceConnectListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 587
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 606
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mServerRemovedListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 877
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayerEventListener:Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;

    .line 988
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$5;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAVPlayerPlaybackResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    .line 1056
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->bWaitStateListener:Z

    .line 1058
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    .line 1192
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMsg:[I

    .line 138
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/Provider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    return-wide v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # J

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    return-wide p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getAutoPlayNext()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContentChanged:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;JZ)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playViaAllShare(JZ)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmrUdn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setSelectedDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;JLandroid/net/Uri;)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # J
    .param p3, "x2"    # Landroid/net/Uri;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playAirContent(JLandroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setPowerWakeLock()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mServerRemovedListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->connectAllShareFwk()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/Device;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Lcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .param p1, "x1"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->processErrorEvent(Lcom/samsung/android/allshare/ERROR;)V

    return-void
.end method

.method private availableToControl()Z
    .locals 2

    .prologue
    .line 1202
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private connectAllShareFwk()V
    .locals 4

    .prologue
    .line 519
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v2, "connectAllShareFwk"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    if-nez v1, :cond_0

    .line 524
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceConnectListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    const-string v3, "com.samsung.android.allshare.media"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 525
    :catch_0
    move-exception v0

    .line 526
    .local v0, "e":Ljava/lang/SecurityException;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectAllShareFwk - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 527
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 528
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectAllShareFwk - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createSecAvPlayer()V
    .locals 3

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "createSecAvPlayer"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    check-cast v0, Lcom/samsung/android/allshare/media/AVPlayer;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    .line 675
    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/extension/SECAVPlayer;-><init>(Lcom/samsung/android/allshare/media/AVPlayer;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayerEventListener:Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->setSmartAVPlayerEventListener(Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;)V

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAVPlayerPlaybackResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->setResponseListener(Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;)V

    .line 678
    return-void
.end method

.method private disconnectAllShareFwk()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "disconnectAllShareFwk"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mItemFromAllShareIntent:Lcom/samsung/android/allshare/Item;

    .line 572
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAllShareIntentSubtitlePath:Ljava/lang/String;

    .line 573
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDMCStateFromAllShareIntent:Z

    .line 574
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 575
    const/16 v0, 0x2ea

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 577
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 583
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 585
    return-void
.end method

.method private downloadAllshareFile(Landroid/net/Uri;)V
    .locals 10
    .param p1, "itemUri"    # Landroid/net/Uri;

    .prologue
    const v9, 0x7f0a006c

    .line 1213
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v7, "downloadAllshareFile E"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    const/4 v3, 0x0

    .line 1215
    .local v3, "downloadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v2, 0x0

    .line 1217
    .local v2, "deviceName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-nez v6, :cond_0

    .line 1218
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v7, "downloadAllshareFile. downLoadRemoteFiles is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1264
    :goto_0
    return-void

    .line 1223
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    .line 1225
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    if-nez v6, :cond_1

    .line 1226
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v7, "downloadAllshareFile. mSecDownloadManager == null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_0

    .line 1231
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "downloadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1233
    .restart local v3    # "downloadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    .line 1235
    .local v1, "db":Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1236
    .local v4, "itemId":Ljava/lang/Long;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downLoadRemoteFiles. ItemSeed = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getRemoteItemSeed(J)Ljava/lang/String;

    move-result-object v0

    .line 1239
    .local v0, "ItemSeed":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1241
    .local v5, "tmpItem":Lcom/samsung/android/allshare/Item;
    if-eqz v0, :cond_2

    .line 1242
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/ItemExtractor;->create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v5

    .line 1244
    if-eqz v5, :cond_2

    .line 1245
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downLoadRemoteFiles. tmpItem.getTitle = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1250
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    if-eqz v6, :cond_4

    .line 1251
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1256
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    if-eqz v6, :cond_5

    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    .line 1257
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downLoadRemoteFiles. DeviceName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    invoke-virtual {v6, v2, v3}, Lcom/samsung/android/allshare/extension/SECDownloader;->Download(Ljava/lang/String;Ljava/util/ArrayList;)Z

    .line 1259
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    const v7, 0x7f0a0132

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto/16 :goto_0

    .line 1252
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;

    if-eqz v6, :cond_3

    .line 1253
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;

    goto :goto_1

    .line 1261
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v7, "downloadAllshareFile. download fail!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto/16 :goto_0
.end method

.method private duration()J
    .locals 4

    .prologue
    .line 1324
    const-wide/16 v0, -0x1

    .line 1326
    .local v0, "duration":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1327
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDuration()J

    move-result-wide v0

    .line 1332
    :goto_0
    return-wide v0

    .line 1329
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDurationTime(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-long v0, v2

    goto :goto_0
.end method

.method private getAutoPlayNext()Z
    .locals 4

    .prologue
    .line 1206
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    .line 1207
    .local v1, "prefMgr":Lcom/sec/android/app/videoplayer/db/SharedPreference;
    const-string v2, "autoPlay"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    .line 1209
    .local v0, "autoPlayNext":Z
    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->sAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    return-object v0
.end method

.method private isCloudContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1280
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v1, :cond_0

    .line 1281
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 1284
    :cond_0
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$7;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    .line 1293
    .local v0, "listener":Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1294
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->setOnUrlUpdatedListener(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;)V

    .line 1295
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getUrlByUri(Landroid/net/Uri;)V

    .line 1296
    const/4 v1, 0x1

    .line 1299
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSKTCloudContent(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1303
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_0

    .line 1304
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    .line 1305
    .local v0, "SKTcloudUtil":Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$8;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    .line 1314
    .local v1, "listener":Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1315
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->setOnUrlUpdatedSKTcloudListener(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;)V

    .line 1316
    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getUrlByUri(Landroid/net/Uri;)V

    .line 1317
    const/4 v2, 0x1

    .line 1320
    .end local v0    # "SKTcloudUtil":Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
    .end local v1    # "listener":Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isStreaming(Ljava/lang/String;)Z
    .locals 1
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    .line 641
    const-string v0, "http"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "rtsp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sshttp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 642
    :cond_0
    const/4 v0, 0x1

    .line 644
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyChanged(II)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 1195
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMsg:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMsg:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 1197
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setChanged()V

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMsg:[I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyObservers(Ljava/lang/Object;)V

    .line 1199
    return-void
.end method

.method private playAirContent(JLandroid/net/Uri;)V
    .locals 15
    .param p1, "playPos"    # J
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const-wide/16 v10, 0x0

    .line 823
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v12, "playAirContent. E"

    invoke-static {v9, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    invoke-virtual/range {p3 .. p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 825
    .local v6, "path":Ljava/lang/String;
    const-string v7, ""

    .line 826
    .local v7, "pathEx":Ljava/lang/String;
    const-string v9, "sshttp://"

    invoke-virtual {v6, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 827
    const-string v9, "ss"

    const-string v12, ""

    invoke-virtual {v6, v9, v12}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 828
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "playAirContent. path : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p3

    .line 832
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v8

    .line 833
    .local v8, "title":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 834
    .local v4, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 836
    .local v5, "mimeType":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "playAirContent. mimeType : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    if-eqz v5, :cond_1

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 839
    :cond_1
    const-string v5, "video/*"

    .line 842
    :cond_2
    new-instance v2, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-object/from16 v0, p3

    invoke-direct {v2, v0, v5}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 843
    .local v2, "builder":Lcom/samsung/android/allshare/Item$WebContentBuilder;
    invoke-virtual {v2, v8}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    .line 845
    cmp-long v9, p1, v10

    if-lez v9, :cond_3

    const-wide/16 v10, 0x3e8

    div-long v10, p1, v10

    :cond_3
    iput-wide v10, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    .line 847
    new-instance v9, Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    invoke-direct {v9}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;-><init>()V

    iget-wide v10, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    invoke-virtual {v9, v10, v11}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->build()Lcom/samsung/android/allshare/media/ContentInfo;

    move-result-object v3

    .line 848
    .local v3, "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v9, :cond_4

    .line 849
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v10

    invoke-virtual {v9, v10, v3}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->play(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    .line 850
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "playViaAllShare. SECAVPlayer.play(). StartingPosition : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_4
    return-void
.end method

.method private playDirectDmc()V
    .locals 4

    .prologue
    const/16 v3, 0x1f6

    .line 1273
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v2, "playDirectDmc"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1275
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1276
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1277
    return-void
.end method

.method private playViaAllShare(JZ)V
    .locals 23
    .param p1, "playPos"    # J
    .param p3, "reset"    # Z

    .prologue
    .line 696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare() playPos : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " reset : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 699
    sget-object v18, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->processErrorEvent(Lcom/samsung/android/allshare/ERROR;)V

    .line 700
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 820
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContentChanged:Z

    .line 706
    const-wide/16 v18, 0x0

    cmp-long v18, p1, v18

    if-gtz v18, :cond_2

    .line 707
    const-wide/16 p1, 0x0

    .line 710
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 711
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z

    .line 713
    if-eqz p3, :cond_3

    .line 714
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->resetSecAvPlayer()V

    .line 715
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->createSecAvPlayer()V

    .line 719
    :cond_3
    const/16 v14, 0x2de

    .line 721
    .local v14, "value":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isSupportDynamicBuffering()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 722
    const/16 v14, 0x2dd

    .line 725
    :cond_4
    const/16 v18, 0x2e8

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1, v14}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 727
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v10

    .line 728
    .local v10, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 729
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/slink/SLink;->setDLNAPath(Landroid/content/Context;)V

    .line 733
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v15

    .line 734
    .local v15, "videoFileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v7

    .line 735
    .local v7, "fileInfo":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v16

    .line 737
    .local v16, "videoId":J
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 738
    .local v13, "uri":Landroid/net/Uri;
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 740
    const-wide/16 v18, 0x0

    cmp-long v18, p1, v18

    if-lez v18, :cond_7

    const-wide/16 v18, 0x3e8

    div-long v18, p1, v18

    :goto_1
    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    .line 741
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->duration()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    .line 743
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v11

    .line 745
    .local v11, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDMCStateFromAllShareIntent:Z

    move/from16 v18, v0

    if-eqz v18, :cond_8

    .line 746
    new-instance v18, Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->build()Lcom/samsung/android/allshare/media/ContentInfo;

    move-result-object v6

    .line 747
    .local v6, "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getItemFromAllShareIntent()Lcom/samsung/android/allshare/Item;

    move-result-object v8

    .line 748
    .local v8, "item":Lcom/samsung/android/allshare/Item;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    if-eqz v8, :cond_6

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "playViaAllShare() - DMCState is true."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare() - item.getURI() : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v6}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->play(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    .line 752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare. SECAVPlayer.play(). StartingPosition : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    .end local v6    # "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    .end local v8    # "item":Lcom/samsung/android/allshare/Item;
    :cond_6
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setPowerWakeLock()V

    goto/16 :goto_0

    .line 740
    .end local v11    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    :cond_7
    const-wide/16 v18, 0x0

    goto/16 :goto_1

    .line 754
    .restart local v11    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    :cond_8
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v18

    if-eqz v18, :cond_a

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "playViaAllShare() - content from Nearby device"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getRemoteItemSeed(J)Ljava/lang/String;

    move-result-object v4

    .line 757
    .local v4, "ItemSeed":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/extension/ItemExtractor;->create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v8

    .line 759
    .restart local v8    # "item":Lcom/samsung/android/allshare/Item;
    if-nez v8, :cond_9

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "playViaAllShare() - ItemExtractor create failed"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 765
    :cond_9
    new-instance v18, Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->build()Lcom/samsung/android/allshare/media/ContentInfo;

    move-result-object v6

    .line 766
    .restart local v6    # "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v6}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->play(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    .line 768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare. SECAVPlayer.play(). StartingPosition : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 770
    .end local v4    # "ItemSeed":Ljava/lang/String;
    .end local v6    # "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    .end local v8    # "item":Lcom/samsung/android/allshare/Item;
    :cond_a
    invoke-virtual {v13}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isStreaming(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "playViaAllShare() - content from streaming"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v13}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playAirContent(JLandroid/net/Uri;)V

    goto/16 :goto_2

    .line 774
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v18

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "playViaAllShare() - content from local device"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare. videoId : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", uri : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " fileName : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    const-wide/16 v18, 0x0

    cmp-long v18, v16, v18

    if-gez v18, :cond_d

    .line 782
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v18

    if-nez v18, :cond_c

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileHidden()Z

    move-result v18

    if-eqz v18, :cond_10

    .line 783
    :cond_c
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 784
    .local v9, "sb":Ljava/lang/StringBuilder;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "file://"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare. this file is in USB. uri "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    :cond_d
    new-instance v5, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "video/*"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v5, v0, v1}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    .local v5, "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "file://"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 802
    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    .line 806
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v12

    .line 807
    .local v12, "subtitlePath":Ljava/lang/String;
    if-eqz v12, :cond_f

    .line 808
    invoke-virtual {v5, v12}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->setSubtitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    .line 811
    :cond_f
    new-instance v18, Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->build()Lcom/samsung/android/allshare/media/ContentInfo;

    move-result-object v6

    .line 813
    .restart local v6    # "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    move-object/from16 v18, v0

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->play(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare. SECAVPlayer.play(). StartingPosition : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 790
    .end local v5    # "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    .end local v6    # "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    .end local v12    # "subtitlePath":Ljava/lang/String;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "playViaAllShare. invalid Id: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setPlayerState(I)V

    .line 792
    if-eqz v11, :cond_11

    .line 793
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->changePlayer(I)V

    .line 795
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playViaDevice()V

    goto/16 :goto_0
.end method

.method private playViaDevice()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 649
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v2, "playViaDevice"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 653
    const/16 v1, 0x2ea

    invoke-direct {p0, v1, v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 656
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    .line 657
    .local v0, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 658
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playViaDevice. VideoServiceUtil.mPositionWhenPaused : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 662
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->releaseSecAvPlayer()V

    .line 664
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLink()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 665
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->setNormalPath(Landroid/content/Context;)V

    .line 668
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    .line 669
    return-void
.end method

.method private processErrorEvent(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-nez v0, :cond_0

    .line 1190
    :goto_0
    return-void

    .line 1153
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processErrorEvent error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_1

    .line 1156
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    .line 1188
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1189
    const/16 v0, 0x2eb

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    goto :goto_0

    .line 1157
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_2

    .line 1158
    const/16 v0, 0x2bd

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1159
    :cond_2
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_3

    .line 1160
    const/16 v0, 0x2be

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1161
    :cond_3
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_4

    .line 1162
    const/16 v0, 0x2bf

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1163
    :cond_4
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_5

    .line 1164
    const/16 v0, 0x2c0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1165
    :cond_5
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_6

    .line 1166
    const/16 v0, 0x2c1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1167
    :cond_6
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_7

    .line 1168
    const/16 v0, 0x2c2

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1169
    :cond_7
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_8

    .line 1170
    const/16 v0, 0x2c3

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1171
    :cond_8
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NO_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_9

    .line 1172
    const/16 v0, 0x2c4

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1173
    :cond_9
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->OUT_OF_MEMORY:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_a

    .line 1174
    const/16 v0, 0x2c5

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1175
    :cond_a
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_b

    .line 1176
    const/16 v0, 0x2c6

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1177
    :cond_b
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_c

    .line 1178
    const/16 v0, 0x2c7

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1179
    :cond_c
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->CONTENT_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_d

    .line 1180
    const/16 v0, 0x2c8

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto :goto_1

    .line 1181
    :cond_d
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

    if-ne p1, v0, :cond_e

    .line 1182
    const/16 v0, 0x2c9

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto/16 :goto_1

    .line 1184
    :cond_e
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mError:I

    goto/16 :goto_0
.end method

.method private declared-synchronized releasePowerWakeLock()V
    .locals 1

    .prologue
    .line 634
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638
    :cond_0
    monitor-exit p0

    return-void

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private releaseSecAvPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "releaseSecAvPlayer"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 857
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->releasePowerWakeLock()V

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v0, :cond_0

    .line 860
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->stop()V

    .line 861
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->setSmartAVPlayerEventListener(Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;)V

    .line 862
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    if-eqz v0, :cond_1

    .line 866
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    .line 869
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 870
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    .line 871
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    .line 873
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 874
    const/16 v0, 0x2e5

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 875
    return-void
.end method

.method private resetSecAvPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "resetSecAvPlayer"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 684
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->stop()V

    .line 686
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->setSmartAVPlayerEventListener(Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;)V

    .line 687
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    if-eqz v0, :cond_1

    .line 691
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayer:Lcom/samsung/android/allshare/media/AVPlayer;

    .line 693
    :cond_1
    return-void
.end method

.method private setCurrentProviderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mFlatProviderName"    # Ljava/lang/String;

    .prologue
    .line 1269
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;

    .line 1270
    return-void
.end method

.method private declared-synchronized setPowerWakeLock()V
    .locals 3

    .prologue
    .line 624
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 625
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->POWER_MGR_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 627
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 631
    :cond_0
    monitor-exit p0

    return-void

    .line 624
    .end local v0    # "pm":Landroid/os/PowerManager;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private setSelectedDevice(I)V
    .locals 4
    .param p1, "which"    # I

    .prologue
    .line 1116
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Device;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    .line 1117
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->isSeekableOnPaused()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    .line 1118
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectedDeviceID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SelectedDeviceName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mSeekableOnPausedThisDevice : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1124
    :goto_0
    return-void

    .line 1120
    :catch_0
    move-exception v0

    .line 1121
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSelectedDevice : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    goto :goto_0
.end method

.method private setSelectedDevice(Ljava/lang/String;)Z
    .locals 6
    .param p1, "devID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1128
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v3, p1, v4}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device$DeviceType;)Lcom/samsung/android/allshare/Device;

    move-result-object v0

    .line 1129
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    if-eqz v0, :cond_0

    .line 1130
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    .line 1131
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->isSeekableOnPaused()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    .line 1132
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SelectedDeviceID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SelectedDeviceName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mSeekableOnPausedThisDevice : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    const/4 v2, 0x1

    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :goto_0
    return v2

    .line 1136
    .restart local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v4, "mSelectedDevice is NULL!!!!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1139
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :catch_0
    move-exception v1

    .line 1140
    .local v1, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSelectedDevice : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    goto :goto_0
.end method


# virtual methods
.method public changePlayer(IJZ)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "playPos"    # J
    .param p4, "reset"    # Z

    .prologue
    const/4 v1, 0x1

    .line 172
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    .line 174
    .local v0, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    packed-switch p1, :pswitch_data_0

    .line 184
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->changePlayer(I)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playViaDevice()V

    .line 188
    :goto_0
    return-void

    .line 176
    :pswitch_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->changePlayer(I)V

    .line 177
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playViaAllShare(JZ)V

    goto :goto_0

    .line 180
    :pswitch_1
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->changePlayer(I)V

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playDirectDmc()V

    goto :goto_0

    .line 174
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public checkDmcDisabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 508
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mIsAllShareIntent:Z

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDMCStateFromAllShareIntent:Z

    if-nez v1, :cond_0

    .line 511
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public connect()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->connectAllShareFwk()V

    .line 153
    return-void
.end method

.method public controlable()Z
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->availableToControl()Z

    move-result v0

    return v0
.end method

.method public disconnect()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->disconnectAllShareFwk()V

    .line 158
    return-void
.end method

.method public download(Landroid/net/Uri;)V
    .locals 0
    .param p1, "itemUri"    # Landroid/net/Uri;

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->downloadAllshareFile(Landroid/net/Uri;)V

    .line 208
    return-void
.end method

.method public getAllShareIntentSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAllShareIntentSubtitlePath:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 4

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurrentPosition. mAVPlayerCurrentPosition : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public getCurrentProvider()Lcom/samsung/android/allshare/media/Provider;
    .locals 1

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;

    return-object v0
.end method

.method public getDuration()I
    .locals 4

    .prologue
    .line 356
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPauseEnable(Z)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->getLastReceivedMediaInfo()Lcom/samsung/android/allshare/media/MediaInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->getLastReceivedMediaInfo()Lcom/samsung/android/allshare/media/MediaInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/MediaInfo;->getDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    .line 365
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDuration. duration : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    long-to-int v0, v0

    return v0

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPauseEnable(Z)V

    goto :goto_0
.end method

.method public getItemFromAllShareIntent()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mItemFromAllShareIntent:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method public getPlayerList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    return-object v0
.end method

.method public getPlayerState()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    return v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPlayer()Lcom/samsung/android/allshare/Device;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;

    return-object v0
.end method

.method public getStateAllShareIntent()Z
    .locals 1

    .prologue
    .line 503
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mIsAllShareIntent:Z

    return v0
.end method

.method public isASFConnected()Z
    .locals 1

    .prologue
    .line 1360
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    return v0
.end method

.method public isContentChanged()Z
    .locals 1

    .prologue
    .line 980
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContentChanged:Z

    return v0
.end method

.method public isDirectDmcMode()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDirectDmcMode:Z

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 4

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "isPlaying E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPlaying. mPlayerState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mState:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 307
    const/4 v0, 0x1

    .line 309
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlayingPauseBtnCheck()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1339
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-nez v1, :cond_1

    .line 1347
    :cond_0
    :goto_0
    return v0

    .line 1342
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPlayingPauseBtnCheck : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->getPlayerState()Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->getPlayerState()Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    if-ne v1, v2, :cond_0

    .line 1345
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSupportDynamicBuffering()Z
    .locals 4

    .prologue
    .line 440
    const/4 v0, 0x0

    .line 442
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v1, :cond_0

    .line 443
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->isSupportDynamicBuffering()Z

    move-result v0

    .line 446
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSupportDynamicBuffering. ret = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    return v0
.end method

.method public notify(II)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 222
    return-void
.end method

.method public notifyPlayerState()V
    .locals 2

    .prologue
    .line 226
    const/16 v0, 0x2e5

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 227
    return-void
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "pause E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause. mPlayerState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mState:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 337
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->pause()V

    .line 341
    :cond_0
    return-void
.end method

.method public play()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "play E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "play. mPlayerState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mState:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 324
    iput v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->resume()V

    .line 326
    :cond_2
    const/16 v0, 0x2e5

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    goto :goto_0

    .line 328
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x1f4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public refreshPlayerList()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "refreshPlayerList"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z

    if-nez v0, :cond_1

    .line 280
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->connectAllShareFwk()V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->refresh()V

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->releaseSecAvPlayer()V

    .line 163
    return-void
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->resetSecAvPlayer()V

    .line 168
    return-void
.end method

.method public seek(J)V
    .locals 7
    .param p1, "position"    # J

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x3e8

    const-wide/16 v2, 0x0

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    cmp-long v0, p1, v2

    if-lez v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSeekableOnPausedThisDevice:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "seekTo. seek position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-long v2, p1, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    div-long v0, p1, v4

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    div-long v2, p1, v4

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->seek(J)V

    .line 391
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z

    .line 393
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 394
    const/16 v0, 0x2e5

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    goto :goto_0

    .line 395
    :cond_3
    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "seekTo 0."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->seek(J)V

    .line 398
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J

    .line 399
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z

    goto :goto_0
.end method

.method public setAllShareIntentSubtitle(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 484
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAllShareIntentSubtitlePath:Ljava/lang/String;

    .line 485
    return-void
.end method

.method public setAutoPlayingSetting(Z)V
    .locals 0
    .param p1, "exit"    # Z

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z

    .line 237
    return-void
.end method

.method public setContentChanged(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 985
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContentChanged:Z

    .line 986
    return-void
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContext:Landroid/content/Context;

    .line 147
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->sAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    return-object v0
.end method

.method public bridge synthetic setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v0

    return-object v0
.end method

.method public setCurrentProvider(Lcom/samsung/android/allshare/media/Provider;)V
    .locals 0
    .param p1, "mFlatProvider"    # Lcom/samsung/android/allshare/media/Provider;

    .prologue
    .line 1352
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 1353
    return-void
.end method

.method public setDirectDmcMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 467
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDirectDmcMode:Z

    .line 468
    return-void
.end method

.method public setDmrUdn(Ljava/lang/String;)V
    .locals 0
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 462
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmrUdn:Ljava/lang/String;

    .line 463
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 371
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J

    .line 372
    return-void
.end method

.method public setInfoFromAllShareIntent(Lcom/samsung/android/allshare/Item;Z)V
    .locals 0
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "enabled"    # Z

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mItemFromAllShareIntent:Lcom/samsung/android/allshare/Item;

    .line 479
    iput-boolean p2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDMCStateFromAllShareIntent:Z

    .line 480
    return-void
.end method

.method public setMute()V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-nez v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "AVPlayer is not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :goto_0
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->changeMute()V

    goto :goto_0
.end method

.method public setNIC(Ljava/lang/String;)V
    .locals 0
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmsNIC:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public setPlayer(I)V
    .locals 0
    .param p1, "which"    # I

    .prologue
    .line 294
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setSelectedDevice(I)V

    .line 295
    return-void
.end method

.method public setPlayerState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 434
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 435
    return-void
.end method

.method public setPowerWakeLock(Z)V
    .locals 0
    .param p1, "bEnable"    # Z

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setPowerWakeLock()V

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->releasePowerWakeLock()V

    goto :goto_0
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setCurrentProviderName(Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method public setStateAllShareIntent(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 498
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mIsAllShareIntent:Z

    .line 499
    return-void
.end method

.method public setVolume(Z)V
    .locals 2
    .param p1, "up"    # Z

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "AVPlayer is not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :goto_0
    return-void

    .line 420
    :cond_0
    if-eqz p1, :cond_1

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->volumeUp()V

    goto :goto_0

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->volumeDown()V

    goto :goto_0
.end method

.method public skipDynamicBuffering()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "skipDynamicBuffering"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->skipDynamicBuffering()V

    .line 457
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v1, "stop E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop. mPlayerState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mState:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 348
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSecAvPlayer:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->stop()V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 352
    return-void
.end method

.method public updatePlayerList()V
    .locals 7

    .prologue
    .line 246
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    const-string v5, "updatePlayerList"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 250
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v4, :cond_3

    .line 251
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 252
    .local v1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    const-string v4, "AVPLAYER_VIDEO"

    invoke-static {v1, v4}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 253
    .local v2, "dmrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmsNIC:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 254
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 255
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmsNIC:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 256
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 260
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 263
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "device total count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 266
    .restart local v0    # "device":Lcom/samsung/android/allshare/Device;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "device id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "device name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 272
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v1    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .end local v2    # "dmrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    const/16 v4, 0x2e7

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V

    .line 273
    return-void
.end method
