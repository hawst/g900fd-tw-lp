.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;
.super Landroid/os/Handler;
.source "VideoSubviewBtnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 2927
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2932
    const/4 v1, 0x0

    .line 2934
    .local v1, "progress":I
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_1

    .line 2936
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    .line 2937
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 2938
    .local v2, "width":I
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 2940
    .local v0, "height":I
    const-string v3, "VideoSubViewBtnController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mainthread bitmap width : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " height : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2942
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/ImageView;

    move-result-object v3

    if-nez v3, :cond_2

    .line 2943
    :cond_0
    const-string v3, "VideoSubViewBtnController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mProgressPreviewShowTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2944
    const-string v3, "VideoSubViewBtnController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mProgressPreviewImage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2983
    .end local v0    # "height":I
    .end local v2    # "width":I
    :cond_1
    :goto_0
    return-void

    .line 2948
    .restart local v0    # "height":I
    .restart local v2    # "width":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget v7, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v6, v7, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2949
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget v9, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v8, v9, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2951
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/ImageView;

    move-result-object v6

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2953
    const-string v3, "VideoSubViewBtnController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mainthread position : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2955
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextWidth:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 2956
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    .line 2959
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget v7, p1, Landroid/os/Message;->arg2:I

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v4

    :goto_1
    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updateProgressbarPreviewView(IZ)I
    invoke-static {v6, v7, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)I

    move-result v1

    .line 2961
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v5

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v3, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/HoverPopupWindow;)Landroid/widget/HoverPopupWindow;

    .line 2963
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2964
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/16 v5, 0x19

    invoke-virtual {v3, v1, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2967
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$5000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v3

    if-ne v3, v4, :cond_6

    .line 2968
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f02006f

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2975
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2976
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v4

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRightPadding:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v6, v2

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTopPadding:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBottomPadding:I
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v7, v0

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextHeight:I
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mArrowHeight:I
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4, v5}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_5
    move v3, v5

    .line 2959
    goto/16 :goto_1

    .line 2969
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$5000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    .line 2970
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020070

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 2972
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f02006e

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2
.end method
