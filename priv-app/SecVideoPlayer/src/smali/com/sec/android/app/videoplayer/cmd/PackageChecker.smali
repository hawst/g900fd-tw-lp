.class public Lcom/sec/android/app/videoplayer/cmd/PackageChecker;
.super Ljava/lang/Object;
.source "PackageChecker.java"


# static fields
.field public static NXP_EDITOR:I

.field public static SHARE_VIDEO:I

.field public static S_APPS:I

.field public static S_STUDIO:I

.field private static final TAG:Ljava/lang/String;

.field public static VIDEO_EDITOR:I

.field public static VIDEO_EDITOR_ON_S_APPS:I

.field public static sPackageInfoList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->TAG:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->NXP_EDITOR:I

    .line 22
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    .line 24
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_STUDIO:I

    .line 26
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    .line 28
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR_ON_S_APPS:I

    .line 30
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->SHARE_VIDEO:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkAvailable(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 51
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    if-nez v3, :cond_0

    .line 98
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    .line 55
    .local v1, "vUtils":Lcom/sec/android/app/videoplayer/common/VUtils;
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 56
    .local v0, "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    if-nez v3, :cond_3

    .line 57
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR_FULL:Z

    if-eqz v3, :cond_1

    const-string v3, "com.sec.android.app.vefull"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR:Z

    if-eqz v3, :cond_b

    const-string v3, "com.sec.android.app.ve"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_2
    move v3, v5

    :goto_1
    iput-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 59
    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 62
    :cond_3
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 63
    .restart local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    if-nez v3, :cond_5

    .line 64
    const-string v3, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-nez v3, :cond_c

    :cond_4
    move v3, v5

    :goto_2
    iput-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 66
    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 69
    :cond_5
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR_ON_S_APPS:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 70
    .restart local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    if-nez v3, :cond_6

    .line 71
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR:Z

    if-eqz v3, :cond_d

    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iget-boolean v3, v3, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    if-eqz v3, :cond_d

    move v3, v5

    :goto_3
    iput-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 72
    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 75
    :cond_6
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->NXP_EDITOR:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 76
    .restart local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    if-nez v3, :cond_7

    .line 77
    const-string v3, "com.lifevibes.trimapp"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 78
    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 81
    :cond_7
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_STUDIO:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 82
    .restart local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    if-nez v3, :cond_8

    .line 83
    const-string v3, "com.sec.android.mimage.sstudio"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 84
    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 87
    :cond_8
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v6, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->SHARE_VIDEO:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 88
    .restart local v0    # "packageInfo":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v3, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    if-nez v3, :cond_a

    .line 89
    iput-boolean v4, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 90
    const-string v3, "com.sec.android.app.mv.player"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 91
    const-string v3, "com.samsung.groupcast"

    invoke-virtual {v1, v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getPackageVersion(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "version":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "3."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    .line 94
    .end local v2    # "version":Ljava/lang/String;
    :cond_9
    iput-boolean v5, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 97
    :cond_a
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkAvailable. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->printInfo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_b
    move v3, v4

    .line 57
    goto/16 :goto_1

    :cond_c
    move v3, v4

    .line 64
    goto/16 :goto_2

    :cond_d
    move v3, v4

    .line 71
    goto/16 :goto_3
.end method

.method public static checkChanged(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "changed":Z
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR_FULL:Z

    if-eqz v1, :cond_0

    const-string v1, "com.sec.android.app.vefull"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR:Z

    if-eqz v1, :cond_4

    const-string v1, "com.sec.android.app.ve"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 110
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 111
    const/4 v0, 0x1

    .line 128
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 129
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkChanged. packageName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->checkAvailable(Landroid/content/Context;)V

    .line 132
    :cond_3
    return-void

    .line 112
    :cond_4
    const-string v1, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 113
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 114
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR_ON_S_APPS:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 115
    const/4 v0, 0x1

    goto :goto_0

    .line 116
    :cond_5
    const-string v1, "com.sec.android.app.mv.player"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "com.samsung.groupcast"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 118
    :cond_6
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->SHARE_VIDEO:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 119
    const/4 v0, 0x1

    goto :goto_0

    .line 120
    :cond_7
    const-string v1, "com.sec.android.mimage.sstudio"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 121
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_STUDIO:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 122
    const/4 v0, 0x1

    goto :goto_0

    .line 123
    :cond_8
    const-string v1, "com.lifevibes.trimapp"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    sget-object v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->NXP_EDITOR:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->checked:Z

    .line 125
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public static init()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;->create()Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    .line 37
    return-void
.end method

.method public static isAvailable(I)Z
    .locals 1
    .param p0, "key"    # I

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-gt v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 42
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    goto :goto_0
.end method

.method private static printInfo()Ljava/lang/String;
    .locals 6

    .prologue
    .line 135
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    if-nez v5, :cond_0

    const/4 v5, 0x0

    .line 150
    .local v0, "buffer":Ljava/lang/StringBuilder;
    .local v1, "i":I
    .local v3, "size":I
    :goto_0
    return-object v5

    .line 137
    .end local v0    # "buffer":Ljava/lang/StringBuilder;
    .end local v1    # "i":I
    .end local v3    # "size":I
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .restart local v0    # "buffer":Ljava/lang/StringBuilder;
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 141
    .restart local v3    # "size":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 142
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 143
    .local v2, "key":I
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 144
    const/16 v5, 0x3d

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 145
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->sPackageInfoList:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    .line 146
    .local v4, "value":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    iget-boolean v5, v4, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;->available:Z

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 147
    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    .end local v2    # "key":I
    .end local v4    # "value":Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method
