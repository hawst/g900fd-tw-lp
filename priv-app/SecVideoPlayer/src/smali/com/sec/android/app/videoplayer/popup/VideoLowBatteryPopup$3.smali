.class Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;
.super Ljava/lang/Object;
.source "VideoLowBatteryPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x1

    .line 51
    sparse-switch p2, :sswitch_data_0

    .line 75
    :cond_0
    :goto_0
    return v6

    .line 54
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 57
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 59
    .local v0, "pressTime":J
    const-wide/16 v2, 0x1f4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.set.lock"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 67
    .end local v0    # "pressTime":J
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 68
    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "VideoLowBatteryPopup - KEYCODE_SEARCH/KEYCODE_BACK"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.exit"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x1a -> :sswitch_0
        0x54 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
