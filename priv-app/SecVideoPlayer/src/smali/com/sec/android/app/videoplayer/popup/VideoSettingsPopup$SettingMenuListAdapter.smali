.class Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;
.super Landroid/widget/SimpleAdapter;
.source "VideoSettingsPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingMenuListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Landroid/content/Context;Ljava/util/ArrayList;I[Ljava/lang/String;[I)V
    .locals 6
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "resource"    # I
    .param p5, "key"    # [Ljava/lang/String;
    .param p6, "values"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 264
    .local p3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 265
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 266
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 269
    if-nez p2, :cond_0

    .line 270
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 271
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f030010

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 274
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->getCount()I

    move-result v6

    if-nez v6, :cond_2

    .line 275
    const-string v6, "VideoSettingsPopup"

    const-string v7, "SettingMenuListAdapter : getView() - getCount() is 0"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_1
    :goto_0
    return-object p2

    .line 280
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 282
    .local v2, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v2, :cond_3

    .line 283
    const-string v6, "VideoSettingsPopup"

    const-string v7, "SettingMenuListAdapter : getView() - item is null"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v6

    if-nez v6, :cond_4

    .line 288
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v7

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$102(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Lcom/sec/android/app/videoplayer/db/SharedPreference;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 291
    :cond_4
    const v6, 0x7f0d0006

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 292
    .local v3, "menuName":Landroid/widget/TextView;
    const-string v6, "MENU_TITLE"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 295
    const v6, 0x7f0d0096

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 296
    const v6, 0x7f0d0097

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 297
    const v6, 0x7f0d009a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 298
    const v6, 0x7f0d0098

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 299
    const v6, 0x7f0d0099

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 301
    const v6, 0x7f0d0099

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Switch;

    .line 302
    .local v4, "menuSwitch":Landroid/widget/Switch;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/Switch;->setFocusable(Z)V

    .line 303
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/Switch;->setFocusableInTouchMode(Z)V

    .line 305
    const v6, 0x7f0d009a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 306
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 308
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 309
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 310
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 312
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 313
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 314
    .local v1, "id":I
    packed-switch v1, :pswitch_data_0

    .line 399
    :pswitch_0
    const v6, 0x7f0d0096

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 400
    const v6, 0x7f0d0097

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 401
    const v6, 0x7f0d0097

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const-string v7, "MENU_VALUE"

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 316
    :pswitch_1
    const v6, 0x7f0d009a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 317
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 328
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlMode()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    :goto_1
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_1

    .line 332
    :pswitch_2
    const v6, 0x7f0d009a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 333
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 340
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getPremiumMode()I

    move-result v6

    if-nez v6, :cond_6

    const/4 v6, 0x1

    :goto_2
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    :cond_6
    const/4 v6, 0x0

    goto :goto_2

    .line 344
    :pswitch_3
    const v6, 0x7f0d009a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 345
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 352
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getCaptureVisibility()I

    move-result v6

    sget v7, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->ON:I

    if-ne v6, v7, :cond_7

    const/4 v6, 0x1

    :goto_3
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    :cond_7
    const/4 v6, 0x0

    goto :goto_3

    .line 356
    :pswitch_4
    const v6, 0x7f0d009a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 357
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 364
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getPlaySpeedVisibility()I

    move-result v6

    sget v7, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->ON:I

    if-ne v6, v7, :cond_8

    const/4 v6, 0x1

    :goto_4
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    :cond_8
    const/4 v6, 0x0

    goto :goto_4

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
