.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 1554
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 12

    .prologue
    const/16 v11, 0xcf

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v10, -0x1

    .line 1558
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->isFinishing()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1627
    :cond_0
    :goto_0
    return-void

    .line 1562
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    .line 1564
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1565
    .local v2, "newWidth":I
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 1566
    .local v0, "newHeight":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_c

    move v1, v6

    .line 1568
    .local v1, "newOrientation":I
    :goto_1
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onGlobalLayout E : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppOrientation:I
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v5, :cond_8

    .line 1571
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-ne v5, v2, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-ne v5, v0, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppOrientation:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-eq v5, v1, :cond_d

    :cond_2
    move v3, v7

    .line 1572
    .local v3, "updateView":Z
    :goto_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1573
    if-eqz v3, :cond_8

    .line 1574
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 1575
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v5

    const-wide/16 v8, 0x1f4

    invoke-virtual {v5, v11, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1577
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v5, :cond_3

    .line 1578
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1579
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    .line 1583
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubEdit:Landroid/view/SubMenu;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/SubMenu;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubEdit:Landroid/view/SubMenu;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1584
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubEdit:Landroid/view/SubMenu;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/SubMenu;->close()V

    .line 1587
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 1588
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v5

    invoke-virtual {v5, v2, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setMWFontSize(II)V

    .line 1595
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/Window;->setFormat(I)V

    .line 1597
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v5

    if-ne v5, v7, :cond_6

    .line 1598
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setScreenMode(I)V

    .line 1601
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAIAMWNotSupportedOnHEVC1080P()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1602
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v5

    if-nez v5, :cond_8

    .line 1603
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createPlayContinueDialog()V
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 1617
    .end local v3    # "updateView":Z
    :cond_8
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-ne v5, v2, :cond_9

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-eq v5, v0, :cond_b

    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-ne v5, v10, :cond_a

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v5

    if-eq v5, v10, :cond_b

    .line 1620
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->prepareChangeView(I)V

    .line 1621
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeDialogView()V
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 1624
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I
    invoke-static {v5, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1302(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 1625
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I
    invoke-static {v5, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 1626
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppOrientation:I
    invoke-static {v5, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    goto/16 :goto_0

    .end local v1    # "newOrientation":I
    :cond_c
    move v1, v7

    .line 1566
    goto/16 :goto_1

    .restart local v1    # "newOrientation":I
    :cond_d
    move v3, v6

    .line 1571
    goto/16 :goto_2

    .line 1608
    .restart local v3    # "updateView":Z
    :cond_e
    if-eqz v3, :cond_8

    .line 1612
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/4 v6, -0x2

    invoke-virtual {v5, v6}, Landroid/view/Window;->setFormat(I)V

    goto :goto_3
.end method
