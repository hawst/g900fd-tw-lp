.class Lcom/sec/android/app/videoplayer/preview/PreviewLayout$4;
.super Ljava/lang/Object;
.source "PreviewLayout.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->addView()Lcom/sec/android/app/videoplayer/preview/Preview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$4;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v2, 0x7f0d008f

    .line 109
    sparse-switch p2, :sswitch_data_0

    .line 120
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 112
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201ef

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 114
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 109
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method
