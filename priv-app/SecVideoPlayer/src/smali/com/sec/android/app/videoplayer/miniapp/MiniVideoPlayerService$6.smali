.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1115
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1, "arg0"    # Landroid/media/MediaPlayer;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleSyncTime(I)V

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleSettings()V

    .line 1119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isFromSideSync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendSideSyncLastPlayedItemBroadcast(J)V
    invoke-static {v0, v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;J)V

    .line 1125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1126
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 1129
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->canPlayNext()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1130
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "onCompletion() - go next."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1131
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isCompletion:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2802(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->playNext()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 1133
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isCompletion:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2802(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 1143
    :goto_0
    return-void

    .line 1135
    :cond_2
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "onCompletion() - finish."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1138
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->abandonAudioFocus()V

    .line 1141
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0
.end method
