.class public Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;
.super Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;
.source "SpotZoomDetector.java"


# static fields
.field private static final MIN_ALLOWED_RECT_HEIGHT:I = 0x3c

.field private static final MIN_ALLOWED_RECT_WIDTH:I = 0x3c

.field private static final NUM_OF_POINTERS_ALLOWED_FOR_SPOT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpotZoomDetector"


# instance fields
.field private mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

.field private mFrameAspectRatio:F

.field private mMaxNumOfPointer:I

.field private mMaxX:F

.field private mMaxY:F

.field private mMinX:F

.field private mMinY:F

.field private mPlayerHeight:I

.field private mPlayerStartX:I

.field private mPlayerStartY:I

.field private mPlayerWidth:I

.field private mXFactor:F

.field private mYFactor:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;)V
    .locals 4
    .param p1, "app"    # Landroid/content/Context;
    .param p2, "LinearZoomDet"    # Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;-><init>(Landroid/content/Context;)V

    .line 39
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxNumOfPointer:I

    .line 40
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    .line 41
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    .line 42
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    .line 43
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mFrameAspectRatio:F

    .line 46
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mXFactor:F

    .line 47
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mYFactor:F

    .line 49
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerWidth:I

    .line 50
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerHeight:I

    .line 52
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerStartX:I

    .line 53
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerStartY:I

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .line 61
    const-string v0, "SpotZoomDetector"

    const-string v1, "SpotZoomDetector"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .line 63
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 30
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 68
    const-string v24, "EVENTMAP Spot Zoom"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Received event"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/16 v18, 0x0

    .line 70
    .local v18, "isEventConsumed":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 71
    .local v3, "action":I
    and-int/lit16 v4, v3, 0xff

    .line 73
    .local v4, "actionCode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isPanEnabled()Z

    move-result v24

    if-eqz v24, :cond_0

    move/from16 v19, v18

    .line 224
    .end local v18    # "isEventConsumed":Z
    .local v19, "isEventConsumed":I
    :goto_0
    return v19

    .line 77
    .end local v19    # "isEventConsumed":I
    .restart local v18    # "isEventConsumed":Z
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1

    .line 78
    packed-switch v4, :pswitch_data_0

    .line 217
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isZoomEnabled()Z

    move-result v24

    if-eqz v24, :cond_2

    .line 218
    const/16 v18, 0x1

    .line 221
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxNumOfPointer:I

    move/from16 v24, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_3

    .line 222
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxNumOfPointer:I

    :cond_3
    move/from16 v19, v18

    .line 224
    .restart local v19    # "isEventConsumed":I
    goto :goto_0

    .line 81
    .end local v19    # "isEventConsumed":I
    :pswitch_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxNumOfPointer:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->isGestureTracked()Z

    move-result v24

    if-eqz v24, :cond_1

    .line 84
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v25, v0

    sub-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v17, v0

    .line 85
    .local v17, "height":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v25, v0

    sub-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v23, v0

    .line 87
    .local v23, "width":I
    const/16 v24, 0x3c

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_7

    const/16 v24, 0x3c

    move/from16 v0, v17

    move/from16 v1, v24

    if-lt v0, v1, :cond_7

    .line 89
    const-string v24, "SpotZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "SpotZoom(b) l:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " t:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " r:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " b: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v25, v0

    sub-float v21, v24, v25

    .line 93
    .local v21, "spotWidth":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v25, v0

    sub-float v20, v24, v25

    .line 95
    .local v20, "spotHeight":F
    const/4 v6, 0x0

    .line 96
    .local v6, "adjWidth":F
    const/4 v5, 0x0

    .line 100
    .local v5, "adjHeight":F
    cmpg-float v24, v21, v20

    if-gez v24, :cond_5

    .line 101
    move/from16 v6, v21

    .line 102
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mFrameAspectRatio:F

    move/from16 v24, v0

    div-float v5, v21, v24

    .line 108
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    div-float v25, v21, v25

    add-float v7, v24, v25

    .line 109
    .local v7, "centerX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    div-float v25, v20, v25

    add-float v8, v24, v25

    .line 110
    .local v8, "centerY":F
    sub-float v14, v21, v6

    .line 111
    .local v14, "diffWidth":F
    sub-float v10, v20, v5

    .line 113
    .local v10, "diffHeight":F
    sget-object v22, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 115
    .local v22, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    mul-float v24, v7, v14

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    div-float v11, v24, v25

    .line 116
    .local v11, "diffLeft":F
    sub-float v12, v14, v11

    .line 117
    .local v12, "diffRight":F
    mul-float v24, v8, v10

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    div-float v13, v24, v25

    .line 118
    .local v13, "diffTop":F
    sub-float v9, v10, v13

    .line 124
    .local v9, "diffBottom":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v24, v0

    add-float v24, v24, v11

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    .line 125
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v24, v0

    sub-float v24, v24, v12

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    .line 126
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v24, v0

    add-float v24, v24, v13

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    .line 127
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v24, v0

    sub-float v24, v24, v9

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    .line 130
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerStartX:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v25

    add-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v25, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(FF)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    .line 131
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerStartY:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v25

    add-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v25, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(FF)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    .line 134
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    sub-int v24, v24, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    sub-int v25, v25, v26

    mul-int v2, v24, v25

    .line 135
    .local v2, "Area":I
    const-string v24, "SpotZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "## Area "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const-string v24, "SpotZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "SpotZoom l:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " t:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " r:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " b: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    int-to-float v0, v2

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mXFactor:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mYFactor:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->withinScaleBounds(F)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 140
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->getZoomPercent(I)F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getZoomPercentWithMargin()F

    move-result v25

    cmpl-float v24, v24, v25

    if-lez v24, :cond_6

    .line 141
    const-string v24, "SpotZoomDetector"

    const-string v25, "Apply Crop"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    new-instance v25, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v29, v0

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v29, v0

    invoke-direct/range {v25 .. v29}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface/range {v24 .. v25}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->applyCrop(Landroid/graphics/Rect;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->adjustZoomState(I)V

    .line 161
    .end local v2    # "Area":I
    .end local v5    # "adjHeight":F
    .end local v6    # "adjWidth":F
    .end local v7    # "centerX":F
    .end local v8    # "centerY":F
    .end local v9    # "diffBottom":F
    .end local v10    # "diffHeight":F
    .end local v11    # "diffLeft":F
    .end local v12    # "diffRight":F
    .end local v13    # "diffTop":F
    .end local v14    # "diffWidth":F
    .end local v20    # "spotHeight":F
    .end local v21    # "spotWidth":F
    .end local v22    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_4
    :goto_3
    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->setGestureBeingTracked(Z)V

    .line 162
    const/16 v18, 0x1

    .line 163
    goto/16 :goto_1

    .line 104
    .restart local v5    # "adjHeight":F
    .restart local v6    # "adjWidth":F
    .restart local v20    # "spotHeight":F
    .restart local v21    # "spotWidth":F
    :cond_5
    move/from16 v5, v20

    .line 105
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mFrameAspectRatio:F

    move/from16 v24, v0

    mul-float v6, v20, v24

    goto/16 :goto_2

    .line 150
    .restart local v2    # "Area":I
    .restart local v7    # "centerX":F
    .restart local v8    # "centerY":F
    .restart local v9    # "diffBottom":F
    .restart local v10    # "diffHeight":F
    .restart local v11    # "diffLeft":F
    .restart local v12    # "diffRight":F
    .restart local v13    # "diffTop":F
    .restart local v14    # "diffWidth":F
    .restart local v22    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->reset()V

    .line 151
    const/16 v18, 0x1

    move/from16 v19, v18

    .line 152
    .restart local v19    # "isEventConsumed":I
    goto/16 :goto_0

    .line 158
    .end local v2    # "Area":I
    .end local v5    # "adjHeight":F
    .end local v6    # "adjWidth":F
    .end local v7    # "centerX":F
    .end local v8    # "centerY":F
    .end local v9    # "diffBottom":F
    .end local v10    # "diffHeight":F
    .end local v11    # "diffLeft":F
    .end local v12    # "diffRight":F
    .end local v13    # "diffTop":F
    .end local v14    # "diffWidth":F
    .end local v19    # "isEventConsumed":I
    .end local v20    # "spotHeight":F
    .end local v21    # "spotWidth":F
    .end local v22    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_7
    const-string v24, "SpotZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "IGNORING GESTURE_RECT h:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " w:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 166
    .end local v17    # "height":I
    .end local v23    # "width":I
    :cond_8
    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->setGestureBeingTracked(Z)V

    .line 167
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxNumOfPointer:I

    goto/16 :goto_1

    .line 176
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->updateVideoFrameProperties()V

    .line 178
    const-string v24, "SpotZoomDetector"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->printVideoFrameProperties(Ljava/lang/String;)V

    .line 180
    sget-object v22, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 182
    .restart local v22    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v15, v24, v25

    .line 183
    .local v15, "eventX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v24

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v16, v24, v25

    .line 185
    .local v16, "eventY":F
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    .line 186
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    .line 187
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    .line 188
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    .line 190
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->setGestureBeingTracked(Z)V

    .line 191
    const/16 v18, 0x1

    .line 193
    goto/16 :goto_1

    .line 199
    .end local v15    # "eventX":F
    .end local v16    # "eventY":F
    .end local v22    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->isGestureTracked()Z

    move-result v24

    if-eqz v24, :cond_1

    .line 201
    sget-object v22, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 203
    .restart local v22    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v15, v24, v25

    .line 204
    .restart local v15    # "eventX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v24

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v16, v24, v25

    .line 206
    .restart local v16    # "eventY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-static {v0, v15}, Ljava/lang/Math;->min(FF)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    .line 207
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-static {v0, v15}, Ljava/lang/Math;->max(FF)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    .line 209
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    .line 210
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    .line 211
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 78
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 255
    const-string v0, "SpotZoomDetector"

    const-string v1, " reset "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 257
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxNumOfPointer:I

    .line 259
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinX:F

    .line 260
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMinY:F

    .line 261
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxX:F

    .line 262
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mMaxY:F

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->resetCropRect()V

    .line 265
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->setGestureBeingTracked(Z)V

    .line 267
    return-void
.end method

.method protected updateVideoFrameProperties()V
    .locals 3

    .prologue
    .line 230
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->updateVideoFrameProperties()V

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->updateVideoFrameData()V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->frameWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->frameHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mFrameAspectRatio:F

    .line 241
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 243
    .local v0, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerWidth:I

    .line 244
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerHeight:I

    .line 246
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerStartX:I

    .line 247
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerStartY:I

    .line 249
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerWidth:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->frameWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mXFactor:F

    .line 250
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mPlayerHeight:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->frameHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/SpotZoomDetector;->mYFactor:F

    .line 251
    return-void
.end method
