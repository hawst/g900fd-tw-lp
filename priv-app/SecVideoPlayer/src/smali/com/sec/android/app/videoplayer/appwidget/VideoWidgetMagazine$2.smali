.class final Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine$2;
.super Landroid/os/Handler;
.source "VideoWidgetMagazine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Looper;

    .prologue
    .line 348
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 350
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3e8

    if-eq v2, v3, :cond_0

    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x7d0

    if-ne v2, v3, :cond_1

    .line 351
    :cond_0
    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 352
    .local v1, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    new-instance v0, Landroid/content/ComponentName;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->access$100()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 353
    .local v0, "thisWidget":Landroid/content/ComponentName;
    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->buildUpdate(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 355
    .end local v0    # "thisWidget":Landroid/content/ComponentName;
    .end local v1    # "widgetMgr":Landroid/appwidget/AppWidgetManager;
    :cond_1
    return-void
.end method
