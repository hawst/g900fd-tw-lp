.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;
.super Ljava/lang/Object;
.source "SubtitleSettingPopup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->updateActionbarView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 293
    if-eqz p2, :cond_1

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSamsungSubtitleActivation(Z)V

    .line 300
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setCustomLabelEnable(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)V

    .line 312
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v0, :cond_4

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSMPTETTCustomMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    .line 325
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitlePreview()V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refresh()V

    .line 327
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setActivation(Z)V

    goto :goto_0

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSamsungSubtitleActivation(Z)V

    .line 308
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setCustomLabelEnable(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)V

    goto :goto_1

    .line 305
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setActivation(Z)V

    goto :goto_3

    .line 316
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    goto :goto_2

    .line 319
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSubtitleActivation()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    goto :goto_2

    .line 322
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    goto :goto_2
.end method
