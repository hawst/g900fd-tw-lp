.class public final Lcom/sec/android/app/videoplayer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Buffering:I = 0x7f0a0000

.field public static final Error_Subtitles:I = 0x7f0a0001

.field public static final IDS_COM_POP_EXPIREAFTERFIRSTUSE:I = 0x7f0a0002

.field public static final IDS_DRM_BODY_IMPOSSIBLE:I = 0x7f0a0003

.field public static final IDS_DRM_BODY_RIGHTSTATUS_FORWARDING:I = 0x7f0a0004

.field public static final IDS_VPL_GO_TO_WEBVTT_SETTINGS:I = 0x7f0a0005

.field public static final Nolanguage:I = 0x7f0a0006

.field public static final ShareList_done:I = 0x7f0a0007

.field public static final adapt_sound:I = 0x7f0a0008

.field public static final adapt_sound_check_again:I = 0x7f0a0009

.field public static final add:I = 0x7f0a000a

.field public static final add_bookmark:I = 0x7f0a01cc

.field public static final add_weather:I = 0x7f0a000b

.field public static final alignment:I = 0x7f0a000c

.field public static final all_videos_cloud:I = 0x7f0a01cd

.field public static final allshare_error_fail:I = 0x7f0a000d

.field public static final allsharecast:I = 0x7f0a000e

.field public static final allsharecast_exit_msg:I = 0x7f0a000f

.field public static final allsharecast_explain:I = 0x7f0a0010

.field public static final app_in_app_exit_dialog:I = 0x7f0a0011

.field public static final app_name:I = 0x7f0a0012

.field public static final app_name_videolist:I = 0x7f0a0013

.field public static final ask_allshare_close:I = 0x7f0a0014

.field public static final at_location:I = 0x7f0a0015

.field public static final att_mobile_share:I = 0x7f0a0016

.field public static final audio_only:I = 0x7f0a0017

.field public static final audio_play_only:I = 0x7f0a0018

.field public static final audio_track:I = 0x7f0a0019

.field public static final auto:I = 0x7f0a001a

.field public static final auto_brightness_check:I = 0x7f0a001b

.field public static final autoon:I = 0x7f0a001c

.field public static final autoplayeoff:I = 0x7f0a001d

.field public static final autoplayeon:I = 0x7f0a001e

.field public static final autoplaysettingtitle:I = 0x7f0a001f

.field public static final back_key_infromation:I = 0x7f0a0020

.field public static final black_color:I = 0x7f0a0021

.field public static final blue_color:I = 0x7f0a0022

.field public static final bookmark_added:I = 0x7f0a01ce

.field public static final bookmark_already_exists:I = 0x7f0a01cf

.field public static final bright:I = 0x7f0a0023

.field public static final brightness_text_check:I = 0x7f0a0024

.field public static final brightness_text_uncheck:I = 0x7f0a0025

.field public static final cancel:I = 0x7f0a0026

.field public static final cannot_play_video:I = 0x7f0a0027

.field public static final cannot_shared:I = 0x7f0a0028

.field public static final caption_window:I = 0x7f0a0029

.field public static final capture_image:I = 0x7f0a002a

.field public static final captured:I = 0x7f0a002b

.field public static final category:I = 0x7f0a002c

.field public static final center:I = 0x7f0a002d

.field public static final change_subtitle_file:I = 0x7f0a002e

.field public static final changeplayer_descrpition_mirroron:I = 0x7f0a002f

.field public static final changeplayer_descrpition_playing:I = 0x7f0a0030

.field public static final changeplayer_descrpition_playvia:I = 0x7f0a0031

.field public static final chapterview_name:I = 0x7f0a0032

.field public static final clear_night:I = 0x7f0a0033

.field public static final close:I = 0x7f0a0034

.field public static final closedcaption:I = 0x7f0a0035

.field public static final cloudy_day:I = 0x7f0a0036

.field public static final cold:I = 0x7f0a01d0

.field public static final color:I = 0x7f0a0037

.field public static final color_cap:I = 0x7f0a0038

.field public static final colortone:I = 0x7f0a01d1

.field public static final connection_check_hdmi:I = 0x7f0a0039

.field public static final connection_check_title_hdmi:I = 0x7f0a003a

.field public static final connection_check_title_wifi:I = 0x7f0a003b

.field public static final connection_check_wifi:I = 0x7f0a003c

.field public static final constraint_details:I = 0x7f0a003d

.field public static final constraint_tag:I = 0x7f0a003e

.field public static final contextual_location:I = 0x7f0a003f

.field public static final contextual_title:I = 0x7f0a0040

.field public static final count:I = 0x7f0a0041

.field public static final custom_color:I = 0x7f0a0042

.field public static final cyan_color:I = 0x7f0a0043

.field public static final delete_complete:I = 0x7f0a0044

.field public static final delete_fail:I = 0x7f0a0045

.field public static final delete_list_title:I = 0x7f0a01d2

.field public static final delete_popup_all:I = 0x7f0a0046

.field public static final delete_popup_nitems:I = 0x7f0a0047

.field public static final delete_popup_text:I = 0x7f0a0048

.field public static final delete_popup_title:I = 0x7f0a0049

.field public static final depressed_edge:I = 0x7f0a004a

.field public static final details_audio_channel:I = 0x7f0a004b

.field public static final details_date:I = 0x7f0a004c

.field public static final details_duration:I = 0x7f0a004d

.field public static final details_format:I = 0x7f0a004e

.field public static final details_forwarding:I = 0x7f0a004f

.field public static final details_name:I = 0x7f0a0050

.field public static final details_resolution:I = 0x7f0a0051

.field public static final details_size:I = 0x7f0a0052

.field public static final disconnected_dms:I = 0x7f0a0053

.field public static final do_not_ask_again:I = 0x7f0a0054

.field public static final done:I = 0x7f0a0055

.field public static final dot:I = 0x7f0a0056

.field public static final download_done:I = 0x7f0a01d3

.field public static final download_video_editor:I = 0x7f0a0057

.field public static final downloaded:I = 0x7f0a0058

.field public static final drm_MTP_connect_block:I = 0x7f0a0059

.field public static final drm_count_expired:I = 0x7f0a005a

.field public static final drm_date_expired:I = 0x7f0a005b

.field public static final drm_exit_popup:I = 0x7f0a005c

.field public static final drm_hdmi_block:I = 0x7f0a005d

.field public static final drm_replay_popup:I = 0x7f0a005e

.field public static final drm_tag:I = 0x7f0a005f

.field public static final drm_unable_to_play:I = 0x7f0a0060

.field public static final drm_warning_popup1:I = 0x7f0a0061

.field public static final drm_warning_popup2:I = 0x7f0a0062

.field public static final drm_warning_popup3:I = 0x7f0a0063

.field public static final drop_shadowed_edge:I = 0x7f0a0064

.field public static final dropbox:I = 0x7f0a0065

.field public static final duration_short:I = 0x7f0a0066

.field public static final during_call:I = 0x7f0a0067

.field public static final edit_weather:I = 0x7f0a0068

.field public static final empty_video:I = 0x7f0a01d4

.field public static final error:I = 0x7f0a0069

.field public static final error_device_not_found:I = 0x7f0a006a

.field public static final error_drm_contents_is_already_playing:I = 0x7f0a006b

.field public static final error_failed_to_download:I = 0x7f0a006c

.field public static final error_file_already_exists:I = 0x7f0a006d

.field public static final error_file_error:I = 0x7f0a006e

.field public static final error_network_error_occurred:I = 0x7f0a006f

.field public static final error_not_enough_memory:I = 0x7f0a0070

.field public static final error_transcoding_lack_of_resource:I = 0x7f0a0071

.field public static final error_unknown:I = 0x7f0a0072

.field public static final error_unsupported_video_codec:I = 0x7f0a0073

.field public static final fast_forward_not_supported:I = 0x7f0a0074

.field public static final faster:I = 0x7f0a0075

.field public static final file_not_found:I = 0x7f0a0076

.field public static final first_sync_notice:I = 0x7f0a0077

.field public static final font:I = 0x7f0a0078

.field public static final font_edge:I = 0x7f0a0079

.field public static final font_size:I = 0x7f0a007a

.field public static final full_screen:I = 0x7f0a007b

.field public static final green_color:I = 0x7f0a007c

.field public static final half_speed_scrubbing:I = 0x7f0a007d

.field public static final help_invalid_action:I = 0x7f0a007e

.field public static final help_jumb_btn_description:I = 0x7f0a007f

.field public static final help_motion_peek_msg:I = 0x7f0a0080

.field public static final help_progress_bar_preview:I = 0x7f0a0081

.field public static final help_progress_bar_preview_only_pen:I = 0x7f0a0082

.field public static final help_progress_bar_preview_pen:I = 0x7f0a0083

.field public static final help_smart_pause_msg:I = 0x7f0a0084

.field public static final hi_speed_scrubbing:I = 0x7f0a0085

.field public static final hover_fast_forward:I = 0x7f0a0086

.field public static final hover_list:I = 0x7f0a0087

.field public static final hover_mute:I = 0x7f0a0088

.field public static final hover_rewind:I = 0x7f0a0089

.field public static final hover_rotate:I = 0x7f0a008a

.field public static final hover_show_screen_on_another_device:I = 0x7f0a008b

.field public static final hover_speed:I = 0x7f0a008c

.field public static final hover_volume:I = 0x7f0a008d

.field public static final hover_volume_down:I = 0x7f0a008e

.field public static final hover_volume_up:I = 0x7f0a008f

.field public static final impossible:I = 0x7f0a0090

.field public static final in_location:I = 0x7f0a0091

.field public static final invalid_subtitle_file:I = 0x7f0a0092

.field public static final invalid_video:I = 0x7f0a0093

.field public static final language:I = 0x7f0a0094

.field public static final large:I = 0x7f0a0095

.field public static final left:I = 0x7f0a0096

.field public static final license_present:I = 0x7f0a0097

.field public static final list:I = 0x7f0a0098

.field public static final loading:I = 0x7f0a0099

.field public static final location:I = 0x7f0a009a

.field public static final lock_btn:I = 0x7f0a009b

.field public static final lowbattery_info:I = 0x7f0a009c

.field public static final lowbattery_title:I = 0x7f0a009d

.field public static final lowbattery_toast:I = 0x7f0a009e

.field public static final magenta_color:I = 0x7f0a009f

.field public static final mediascanner_finished:I = 0x7f0a01d5

.field public static final mediascanner_running:I = 0x7f0a01d6

.field public static final mediascanner_started:I = 0x7f0a00a0

.field public static final medium:I = 0x7f0a00a1

.field public static final menu_Delete:I = 0x7f0a00a2

.field public static final menu_ShareVia:I = 0x7f0a00a3

.field public static final menu_Shop:I = 0x7f0a00a4

.field public static final menu_Subtitle:I = 0x7f0a00a5

.field public static final menu_Subtitle_from_setting:I = 0x7f0a00a6

.field public static final menu_VideoMaker:I = 0x7f0a00a7

.field public static final menu_ViewBookmark:I = 0x7f0a01d7

.field public static final menu_audio:I = 0x7f0a00a8

.field public static final menu_bigpondtv:I = 0x7f0a00a9

.field public static final menu_closedcaption:I = 0x7f0a00aa

.field public static final menu_color_and_opacity:I = 0x7f0a00ab

.field public static final menu_color_and_opacity_cap:I = 0x7f0a00ac

.field public static final menu_details:I = 0x7f0a00ad

.field public static final menu_download:I = 0x7f0a00ae

.field public static final menu_downloadmusicvideo:I = 0x7f0a00af

.field public static final menu_edit:I = 0x7f0a00b0

.field public static final menu_miniController:I = 0x7f0a00b1

.field public static final menu_overview:I = 0x7f0a00b2

.field public static final menu_overview_cap:I = 0x7f0a00b3

.field public static final menu_play_via_group_play:I = 0x7f0a00b4

.field public static final menu_sendto_another_device:I = 0x7f0a00b5

.field public static final menu_sstudio:I = 0x7f0a00b6

.field public static final menu_text:I = 0x7f0a00b7

.field public static final menu_text_cap:I = 0x7f0a00b8

.field public static final menu_trim:I = 0x7f0a00b9

.field public static final menu_upload:I = 0x7f0a01d8

.field public static final menu_video_trimmer:I = 0x7f0a00ba

.field public static final micro_control_message:I = 0x7f0a01d9

.field public static final mini_controller_will_be_applied_to_landscape_mode:I = 0x7f0a00bb

.field public static final mono:I = 0x7f0a00bc

.field public static final monotype_default_font:I = 0x7f0a00bd

.field public static final monotype_dialog_font_AndaleMono:I = 0x7f0a00be

.field public static final monotype_dialog_font_Courier:I = 0x7f0a00bf

.field public static final monotype_dialog_font_SamsungSansSmallCap:I = 0x7f0a00c0

.field public static final monotype_dialog_font_TimesNewRoman:I = 0x7f0a00c1

.field public static final monotype_dialog_font_applemint:I = 0x7f0a00c2

.field public static final monotype_dialog_font_choco:I = 0x7f0a00c3

.field public static final monotype_dialog_font_cool:I = 0x7f0a00c4

.field public static final monotype_dialog_font_girl:I = 0x7f0a00c5

.field public static final monotype_dialog_font_kaiti:I = 0x7f0a00c6

.field public static final monotype_dialog_font_maruberi:I = 0x7f0a00c7

.field public static final monotype_dialog_font_miao:I = 0x7f0a00c8

.field public static final monotype_dialog_font_mincho:I = 0x7f0a00c9

.field public static final monotype_dialog_font_pop:I = 0x7f0a00ca

.field public static final monotype_dialog_font_rose:I = 0x7f0a00cb

.field public static final monotype_dialog_font_tinkerbell:I = 0x7f0a00cc

.field public static final mosaic_search:I = 0x7f0a01da

.field public static final music:I = 0x7f0a00cd

.field public static final my_device:I = 0x7f0a00ce

.field public static final my_tablet:I = 0x7f0a00cf

.field public static final no_bookmark_found:I = 0x7f0a01db

.field public static final no_connectivity:I = 0x7f0a00d0

.field public static final no_edge:I = 0x7f0a00d1

.field public static final no_file_selected:I = 0x7f0a01dc

.field public static final no_file_to_deleted:I = 0x7f0a01dd

.field public static final no_license:I = 0x7f0a00d2

.field public static final no_local_contents:I = 0x7f0a00d3

.field public static final no_online_contents:I = 0x7f0a00d4

.field public static final no_service:I = 0x7f0a00d5

.field public static final no_tags:I = 0x7f0a00d6

.field public static final no_videos:I = 0x7f0a01de

.field public static final none:I = 0x7f0a00d7

.field public static final normal:I = 0x7f0a00d8

.field public static final not_enough_memory:I = 0x7f0a00d9

.field public static final not_support:I = 0x7f0a00da

.field public static final noti_date:I = 0x7f0a00db

.field public static final noti_weather:I = 0x7f0a00dc

.field public static final ok:I = 0x7f0a00dd

.field public static final on_location:I = 0x7f0a00de

.field public static final only_play_in_fullscreen:I = 0x7f0a00df

.field public static final only_play_in_talkbackoff:I = 0x7f0a00e0

.field public static final opacity:I = 0x7f0a00e1

.field public static final opacity_cap:I = 0x7f0a00e2

.field public static final out_of_bookmark:I = 0x7f0a01df

.field public static final outdoor:I = 0x7f0a01e0

.field public static final over_capacity:I = 0x7f0a00e3

.field public static final path:I = 0x7f0a00e4

.field public static final pemrission_tag:I = 0x7f0a00e5

.field public static final percent:I = 0x7f0a00e6

.field public static final playback:I = 0x7f0a00e7

.field public static final playback_from:I = 0x7f0a00e8

.field public static final playback_until:I = 0x7f0a00e9

.field public static final player_error_aia_error_msg:I = 0x7f0a00ea

.field public static final player_error_text_DecryptFailed:I = 0x7f0a00eb

.field public static final player_error_text_LicenseExpired:I = 0x7f0a00ec

.field public static final player_error_text_LicenseNotFound:I = 0x7f0a00ed

.field public static final player_error_text_OplBlocked:I = 0x7f0a00ee

.field public static final player_error_text_TamperDetected:I = 0x7f0a00ef

.field public static final player_error_text_acqFailed:I = 0x7f0a00f0

.field public static final player_error_text_currupt:I = 0x7f0a00f1

.field public static final player_error_text_invalid_progressive_playback:I = 0x7f0a00f2

.field public static final player_error_text_network:I = 0x7f0a00f3

.field public static final player_error_text_notsupport:I = 0x7f0a00f4

.field public static final player_error_text_unable_play_usinig_sidesync:I = 0x7f0a00f5

.field public static final player_error_text_unable_play_while_using_sidesync:I = 0x7f0a00f6

.field public static final player_error_text_unknown:I = 0x7f0a00f7

.field public static final playing_video_hdmi:I = 0x7f0a01e1

.field public static final playspeed:I = 0x7f0a00f8

.field public static final plus_two:I = 0x7f0a00f9

.field public static final possible:I = 0x7f0a00fa

.field public static final proceed_with_sharing_after:I = 0x7f0a00fb

.field public static final processing_popup:I = 0x7f0a00fc

.field public static final quarter_speed_scrubbing:I = 0x7f0a00fd

.field public static final rainy_day:I = 0x7f0a00fe

.field public static final raised_edge:I = 0x7f0a00ff

.field public static final red_color:I = 0x7f0a0100

.field public static final refresh:I = 0x7f0a0101

.field public static final reset:I = 0x7f0a0102

.field public static final rewind_fast_forwarding_not_supported:I = 0x7f0a0103

.field public static final rewind_not_supported:I = 0x7f0a0104

.field public static final right:I = 0x7f0a0105

.field public static final rights_tag:I = 0x7f0a0106

.field public static final row_item_count:I = 0x7f0a01e2

.field public static final samsung_apps_description:I = 0x7f0a0107

.field public static final save:I = 0x7f0a0108

.field public static final sbeam:I = 0x7f0a0109

.field public static final sbeam_explain:I = 0x7f0a010a

.field public static final sbeam_explain_wo_snote:I = 0x7f0a010b

.field public static final sbeam_title:I = 0x7f0a010c

.field public static final screen_locked:I = 0x7f0a010d

.field public static final screen_mirroring_fail_to_connect:I = 0x7f0a010e

.field public static final screen_mirroring_guide_scan_constraint_limited_contents:I = 0x7f0a010f

.field public static final screen_mirroring_guide_scan_constraint_power_saving_mode_on:I = 0x7f0a0110

.field public static final screen_mirroring_guide_scan_constraint_running_all_together:I = 0x7f0a0111

.field public static final screen_mirroring_guide_scan_constraint_running_group_play:I = 0x7f0a0112

.field public static final screen_mirroring_guide_scan_constraint_running_sidesync:I = 0x7f0a0113

.field public static final screen_mirroring_guide_scan_constraint_unable_to_scan_devices:I = 0x7f0a0114

.field public static final screen_mirroring_guide_scan_constraint_wifi_direct:I = 0x7f0a0115

.field public static final screen_mirroring_guide_scan_constraint_wifi_hotspot:I = 0x7f0a0116

.field public static final screen_mirroring_guide_scan_constraint_wlan_direct_for_chn:I = 0x7f0a0117

.field public static final screen_mirroring_guide_scan_constraint_wlan_hotspot_for_chn:I = 0x7f0a0118

.field public static final screen_shot_save_toast:I = 0x7f0a0119

.field public static final screen_shot_to_save:I = 0x7f0a011a

.field public static final screen_to_play_or_pause_media:I = 0x7f0a011b

.field public static final scrubbing_at_psx_speed:I = 0x7f0a011c

.field public static final sd_unmounted:I = 0x7f0a011d

.field public static final search_empty:I = 0x7f0a01e3

.field public static final search_hint:I = 0x7f0a011e

.field public static final search_label:I = 0x7f0a011f

.field public static final sec:I = 0x7f0a0120

.field public static final sec_faster:I = 0x7f0a0121

.field public static final sec_slower:I = 0x7f0a0122

.field public static final seekspeed:I = 0x7f0a0123

.field public static final select_all:I = 0x7f0a01e4

.field public static final select_at_least_one:I = 0x7f0a0124

.field public static final select_subtitle:I = 0x7f0a0125

.field public static final settings:I = 0x7f0a0126

.field public static final setvideoautooff:I = 0x7f0a01e5

.field public static final slower:I = 0x7f0a0127

.field public static final small:I = 0x7f0a0128

.field public static final smart_pause_enabled:I = 0x7f0a0129

.field public static final snowy_day:I = 0x7f0a012a

.field public static final sortyby:I = 0x7f0a01e6

.field public static final sortybydate:I = 0x7f0a012b

.field public static final sortybyname:I = 0x7f0a01e7

.field public static final sortybysize:I = 0x7f0a01e8

.field public static final sortybytype:I = 0x7f0a01e9

.field public static final sound_alive:I = 0x7f0a012c

.field public static final sound_effect_works_in_earphone_only:I = 0x7f0a012d

.field public static final split_subtitle:I = 0x7f0a012e

.field public static final ss_move_panel_left_opt:I = 0x7f0a012f

.field public static final ss_move_panel_right_opt:I = 0x7f0a0130

.field public static final ss_move_panel_to_centre_opt:I = 0x7f0a0131

.field public static final start_download:I = 0x7f0a0132

.field public static final stereo:I = 0x7f0a0133

.field public static final stms_appgroup:I = 0x7f0a0134

.field public static final stms_version:I = 0x7f0a0135

.field public static final stop_playing_appinapp:I = 0x7f0a0136

.field public static final storage_location:I = 0x7f0a0137

.field public static final subtitle_Empty:I = 0x7f0a0138

.field public static final subtitle_activity_lable:I = 0x7f0a0139

.field public static final subtitle_backgroundcolor:I = 0x7f0a013a

.field public static final subtitle_custom:I = 0x7f0a013b

.field public static final subtitle_fontcolor:I = 0x7f0a013c

.field public static final subtitle_on_off:I = 0x7f0a013d

.field public static final subtitle_video_off:I = 0x7f0a013e

.field public static final sunny_day:I = 0x7f0a013f

.field public static final surround:I = 0x7f0a0140

.field public static final sync:I = 0x7f0a0141

.field public static final sync_with_dropbox_cloud:I = 0x7f0a01ea

.field public static final tab_Nearby_devices:I = 0x7f0a01eb

.field public static final tab_allshare:I = 0x7f0a01ec

.field public static final tab_folders:I = 0x7f0a01ed

.field public static final tab_lists:I = 0x7f0a01ee

.field public static final tab_thumbnail:I = 0x7f0a01ef

.field public static final tag_buddy:I = 0x7f0a0142

.field public static final tcloud:I = 0x7f0a0143

.field public static final times:I = 0x7f0a0144

.field public static final title_unknown:I = 0x7f0a0145

.field public static final tts_51ch_off_button:I = 0x7f0a0146

.field public static final tts_51ch_on_button:I = 0x7f0a0147

.field public static final tts_app_in_app:I = 0x7f0a0148

.field public static final tts_app_in_app_close:I = 0x7f0a0149

.field public static final tts_battery:I = 0x7f0a014a

.field public static final tts_battery_charging:I = 0x7f0a014b

.field public static final tts_brightness_seek_control:I = 0x7f0a014c

.field public static final tts_button:I = 0x7f0a014d

.field public static final tts_capture_button:I = 0x7f0a014e

.field public static final tts_change_player:I = 0x7f0a014f

.field public static final tts_chapter_1:I = 0x7f0a0150

.field public static final tts_chapter_10:I = 0x7f0a0151

.field public static final tts_chapter_11:I = 0x7f0a0152

.field public static final tts_chapter_12:I = 0x7f0a0153

.field public static final tts_chapter_2:I = 0x7f0a0154

.field public static final tts_chapter_3:I = 0x7f0a0155

.field public static final tts_chapter_4:I = 0x7f0a0156

.field public static final tts_chapter_5:I = 0x7f0a0157

.field public static final tts_chapter_6:I = 0x7f0a0158

.field public static final tts_chapter_7:I = 0x7f0a0159

.field public static final tts_chapter_8:I = 0x7f0a015a

.field public static final tts_chapter_9:I = 0x7f0a015b

.field public static final tts_ff_button:I = 0x7f0a015c

.field public static final tts_fit_to_height:I = 0x7f0a015d

.field public static final tts_header:I = 0x7f0a015e

.field public static final tts_hours:I = 0x7f0a015f

.field public static final tts_keep_aspect_ratio_button:I = 0x7f0a0160

.field public static final tts_minus_button:I = 0x7f0a0161

.field public static final tts_minutes:I = 0x7f0a0162

.field public static final tts_more_options:I = 0x7f0a0163

.field public static final tts_navigate_up:I = 0x7f0a0164

.field public static final tts_next_frame:I = 0x7f0a0165

.field public static final tts_not_ticked:I = 0x7f0a0166

.field public static final tts_opacity_minus_button:I = 0x7f0a0167

.field public static final tts_opacity_plus_button:I = 0x7f0a0168

.field public static final tts_original_size:I = 0x7f0a0169

.field public static final tts_overlook_aspect_ratio_button:I = 0x7f0a016a

.field public static final tts_pause_button:I = 0x7f0a016b

.field public static final tts_play_button:I = 0x7f0a016c

.field public static final tts_plus_button:I = 0x7f0a016d

.field public static final tts_premium_movie:I = 0x7f0a016e

.field public static final tts_previous_frame:I = 0x7f0a016f

.field public static final tts_rew_button:I = 0x7f0a0170

.field public static final tts_seconds:I = 0x7f0a0171

.field public static final tts_stop_button:I = 0x7f0a0172

.field public static final tts_ticked:I = 0x7f0a0173

.field public static final tts_videoprogress_percent:I = 0x7f0a0174

.field public static final tts_videoprogress_seek_control:I = 0x7f0a0175

.field public static final tts_videoprogress_swipe:I = 0x7f0a0176

.field public static final tts_volume_button:I = 0x7f0a0177

.field public static final unable_to_access_videolist_resctricted_profile:I = 0x7f0a0178

.field public static final unauthorized_video_output_so_Analog:I = 0x7f0a01f0

.field public static final unauthorized_video_output_so_HDMI:I = 0x7f0a01f1

.field public static final uniform_edge:I = 0x7f0a0179

.field public static final unsupported_audio:I = 0x7f0a017a

.field public static final unsupported_file_type:I = 0x7f0a017b

.field public static final unsupported_video:I = 0x7f0a017c

.field public static final upload_done:I = 0x7f0a01f2

.field public static final via_Bluetooth:I = 0x7f0a017d

.field public static final via_Device:I = 0x7f0a017e

.field public static final video_51_chanel_sound_effect_not_works_in_line_out:I = 0x7f0a017f

.field public static final video_51_chanel_sound_effect_works_in_earphone_mode_only:I = 0x7f0a0180

.field public static final video_allshare_available_devices:I = 0x7f0a0181

.field public static final video_allshare_available_devices_cap:I = 0x7f0a0182

.field public static final video_allshare_busy:I = 0x7f0a0183

.field public static final video_allshare_change_player:I = 0x7f0a0184

.field public static final video_allshare_derr_need_to_reset:I = 0x7f0a0185

.field public static final video_allshare_derr_no_response:I = 0x7f0a0186

.field public static final video_allshare_devices_found:I = 0x7f0a0187

.field public static final video_allshare_download_fail:I = 0x7f0a0188

.field public static final video_allshare_huge_data_warning:I = 0x7f0a0189

.field public static final video_allshare_local_dms_full:I = 0x7f0a018a

.field public static final video_allshare_no_contents_location:I = 0x7f0a018b

.field public static final video_allshare_no_devices:I = 0x7f0a018c

.field public static final video_allshare_no_devices_found:I = 0x7f0a018d

.field public static final video_allshare_no_player:I = 0x7f0a018e

.field public static final video_allshare_play_error:I = 0x7f0a018f

.field public static final video_allshare_scan_for_nearby_devices:I = 0x7f0a0190

.field public static final video_allshare_scan_started:I = 0x7f0a0191

.field public static final video_allshare_unable_to_use_ff_rew:I = 0x7f0a0192

.field public static final video_automatic_registration:I = 0x7f0a0193

.field public static final video_detail_drm_constraint_type_accumulated_time:I = 0x7f0a0194

.field public static final video_detail_drm_constraint_type_count:I = 0x7f0a0195

.field public static final video_detail_drm_constraint_type_date:I = 0x7f0a0196

.field public static final video_detail_drm_constraint_type_interval:I = 0x7f0a0197

.field public static final video_detail_drm_constraint_type_time:I = 0x7f0a0198

.field public static final video_detail_drm_constraint_type_timed_count:I = 0x7f0a0199

.field public static final video_detail_drm_constraint_type_unlimited:I = 0x7f0a019a

.field public static final video_detail_drm_permission_type_display:I = 0x7f0a019b

.field public static final video_detail_drm_permission_type_excute:I = 0x7f0a019c

.field public static final video_detail_drm_permission_type_play:I = 0x7f0a019d

.field public static final video_detail_drm_permission_type_print:I = 0x7f0a019e

.field public static final video_drm_acquiring_license:I = 0x7f0a019f

.field public static final video_drm_detail_available_uses:I = 0x7f0a01a0

.field public static final video_drm_detail_drm_validity:I = 0x7f0a01a1

.field public static final video_drm_divx_back_key:I = 0x7f0a01a2

.field public static final video_drm_divx_continue:I = 0x7f0a01a3

.field public static final video_drm_divx_not_authorized:I = 0x7f0a01a4

.field public static final video_drm_divx_register_at:I = 0x7f0a01a5

.field public static final video_drm_divx_register_code:I = 0x7f0a01a6

.field public static final video_drm_divx_rental_expired1:I = 0x7f0a01a7

.field public static final video_drm_divx_rental_expired2:I = 0x7f0a01a8

.field public static final video_drm_first_render:I = 0x7f0a01a9

.field public static final video_drm_item_expired:I = 0x7f0a01aa

.field public static final video_drm_license_invalid:I = 0x7f0a01ab

.field public static final video_drm_locked:I = 0x7f0a01ac

.field public static final video_drm_play_now_q:I = 0x7f0a01ad

.field public static final video_drm_this_item_no_longer_use:I = 0x7f0a01ae

.field public static final video_drm_unable_to_play:I = 0x7f0a01af

.field public static final video_drm_unlock_it_q:I = 0x7f0a01b0

.field public static final video_drm_use_1_time:I = 0x7f0a01b1

.field public static final video_drm_use_n_times:I = 0x7f0a01b2

.field public static final video_editor_description:I = 0x7f0a01b3

.field public static final video_list_title:I = 0x7f0a01b4

.field public static final video_magazine_novideo_comment:I = 0x7f0a01b5

.field public static final video_not_bookmark:I = 0x7f0a01f3

.field public static final video_play_only:I = 0x7f0a01b6

.field public static final video_selected:I = 0x7f0a01f4

.field public static final video_sound_alive_sound_effect_works_in_earphone_mode_only:I = 0x7f0a01b7

.field public static final video_subtitle_lang_ch:I = 0x7f0a01b8

.field public static final video_subtitle_lang_de:I = 0x7f0a01b9

.field public static final video_subtitle_lang_eng:I = 0x7f0a01ba

.field public static final video_subtitle_lang_es:I = 0x7f0a01bb

.field public static final video_subtitle_lang_fr:I = 0x7f0a01bc

.field public static final video_subtitle_lang_it:I = 0x7f0a01bd

.field public static final video_subtitle_lang_jp:I = 0x7f0a01be

.field public static final video_subtitle_lang_kr:I = 0x7f0a01bf

.field public static final video_taptoaccessvideos:I = 0x7f0a01c0

.field public static final video_taptoadd:I = 0x7f0a01c1

.field public static final video_taptoaddvideo:I = 0x7f0a01c2

.field public static final video_taptoaddvideos:I = 0x7f0a01c3

.field public static final video_text:I = 0x7f0a01c4

.field public static final videoautooff:I = 0x7f0a01f5

.field public static final videoautooff_15m:I = 0x7f0a01f6

.field public static final videoautooff_1h:I = 0x7f0a01f7

.field public static final videoautooff_1h30m:I = 0x7f0a01f8

.field public static final videoautooff_2h:I = 0x7f0a01f9

.field public static final videoautooff_30m:I = 0x7f0a01fa

.field public static final videoautooff_off:I = 0x7f0a01fb

.field public static final videocapturetitle:I = 0x7f0a01c5

.field public static final videos_selected:I = 0x7f0a01fc

.field public static final view_by_cloud:I = 0x7f0a01fd

.field public static final voice:I = 0x7f0a01c6

.field public static final warm:I = 0x7f0a01fe

.field public static final weather:I = 0x7f0a01c7

.field public static final white_color:I = 0x7f0a01c8

.field public static final widget_name:I = 0x7f0a01c9

.field public static final yellow_color:I = 0x7f0a01ca

.field public static final zoom_not_support:I = 0x7f0a01cb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
