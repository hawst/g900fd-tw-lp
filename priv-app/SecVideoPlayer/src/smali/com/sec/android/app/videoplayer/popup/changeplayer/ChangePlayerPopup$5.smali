.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;
.super Ljava/lang/Object;
.source "ChangePlayerPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->showDevicePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->bConnectingScreenMirroring:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$602(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;Z)Z

    .line 393
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSelectedbyChangePlayer(Z)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerListener:Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->registerReceiver()V

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5$1;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 422
    return-void
.end method
