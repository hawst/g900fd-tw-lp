.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;
.super Landroid/database/ContentObserver;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 3716
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 9
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 3719
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 3720
    .local v3, "uriPath":Ljava/lang/String;
    const/16 v6, 0x2f

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 3721
    .local v2, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 3722
    .local v0, "active":Z
    const-string v6, "MiniVideoPlayerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mCaptioningObserver E. name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3724
    const-string v6, "accessibility_captioning_enabled"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3725
    const-string v6, "MiniVideoPlayerService"

    const-string v7, "mCaptioningObserver E. captioning enabled value is changed!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3737
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleActive()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3738
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->startTimedText()V

    .line 3743
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFileTyp()I

    move-result v6

    const/16 v7, 0x69

    if-ne v6, v7, :cond_1

    .line 3744
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleActive()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3745
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWebvttRendererVisibility(Z)V

    .line 3750
    :cond_1
    :goto_2
    return-void

    .line 3726
    :cond_2
    const-string v6, "accessibility_sec_captioning_enabled"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3727
    const-string v6, "MiniVideoPlayerService"

    const-string v7, "mCaptioningObserver E. samsung captioning enabled value is changed!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3729
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "accessibility_sec_captioning_enabled"

    invoke-static {v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-ne v6, v4, :cond_3

    move v0, v4

    .line 3733
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 3734
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    goto :goto_0

    :cond_3
    move v0, v5

    .line 3729
    goto :goto_3

    .line 3730
    :catch_0
    move-exception v1

    .line 3731
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 3740
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->updateSubtitle(Ljava/lang/String;)V

    goto :goto_1

    .line 3747
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$19;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWebvttRendererVisibility(Z)V

    goto :goto_2
.end method
