.class public Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
.super Ljava/lang/Object;
.source "VideoFileInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;,
        Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;
    }
.end annotation


# static fields
.field public static final FILE_MEDIA_TYPE_AUDIO_ONLY:I = 0xb

.field public static final FILE_MEDIA_TYPE_NORMAL:I = 0xa

.field public static final FILE_MEDIA_TYPE_VIDEO_ONLY:I = 0xc

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;


# instance fields
.field private mAIAMWNotSupportedOnHEVC1080P:Z

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mCloudFile:Z

.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDRMFile:Z

.field private mDmsContentTitle:Ljava/lang/String;

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mFileDBId:J

.field private mFileDRMType:I

.field private mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

.field private mFileMeidaType:I

.field private mFilePath:Ljava/lang/String;

.field private mFileTitle:Ljava/lang/String;

.field private mFileUri:Landroid/net/Uri;

.field private mHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

.field private mIsNotAllowedScreenMirroring:Z

.field private mMMSContent:Z

.field private mMMSTitleName:Ljava/lang/String;

.field private mMediaHubFileInComplete:Z

.field private mMediaHubFileIsPyv:Z

.field private mMediaHubFileLen:J

.field private mMediaHubFilePath:Ljava/lang/String;

.field private mOnFinishListener:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;

.field private mOneFrameSeekEnable:Z

.field private mPauseEnable:Z

.field private mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field private mShareViaEnable:Z

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "VideoFileInfo"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    .line 43
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileTitle:Ljava/lang/String;

    .line 47
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    .line 51
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDmsContentTitle:Ljava/lang/String;

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    .line 55
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    .line 59
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mPauseEnable:Z

    .line 61
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOneFrameSeekEnable:Z

    .line 63
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mIsNotAllowedScreenMirroring:Z

    .line 65
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAIAMWNotSupportedOnHEVC1080P:Z

    .line 67
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDRMFile:Z

    .line 69
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileIsPyv:Z

    .line 71
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z

    .line 73
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudFile:Z

    .line 75
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSContent:Z

    .line 77
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mShareViaEnable:Z

    .line 81
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mContext:Landroid/content/Context;

    .line 83
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 85
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 89
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 99
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mContext:Landroid/content/Context;

    .line 103
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 104
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 106
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    .line 107
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 108
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    .line 110
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->initInfo()V

    .line 115
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    .line 122
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    return-object v0
.end method

.method private isMediaHubFileIncomplete(Ljava/lang/String;)Z
    .locals 17
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    const/16 v13, 0x8

    new-array v12, v13, [I

    .line 268
    .local v12, "temp_byte":[I
    const-wide/16 v4, 0x0

    .line 270
    .local v4, "actual_length":J
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 271
    .local v2, "PYVFile":Ljava/io/File;
    const/4 v10, 0x0

    .line 272
    .local v10, "is":Ljava/io/FileInputStream;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    invoke-virtual {v13}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getFileSize()J

    move-result-wide v4

    .line 274
    sget-object v13, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "isMediaHubFileIncomplete E. actual_length : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-wide/16 v14, 0x0

    cmp-long v13, v4, v14

    if-gtz v13, :cond_2

    .line 278
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    .end local v10    # "is":Ljava/io/FileInputStream;
    .local v11, "is":Ljava/io/FileInputStream;
    if-eqz v11, :cond_a

    .line 289
    const-wide/16 v14, 0x46

    invoke-virtual {v11, v14, v15}, Ljava/io/FileInputStream;->skip(J)J

    .line 291
    const/4 v7, 0x7

    .local v7, "index":I
    :goto_0
    if-ltz v7, :cond_0

    .line 293
    :try_start_1
    invoke-virtual {v11}, Ljava/io/FileInputStream;->read()I

    move-result v13

    aput v13, v12, v7
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 291
    :goto_1
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 294
    :catch_0
    move-exception v3

    .line 295
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 296
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    goto :goto_1

    .line 300
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v7, 0x0

    :goto_2
    const/4 v13, 0x7

    if-ge v7, v13, :cond_1

    .line 301
    aget v13, v12, v7

    int-to-long v14, v13

    add-long/2addr v4, v14

    .line 302
    const/16 v13, 0x8

    shl-long/2addr v4, v13

    .line 300
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 305
    :cond_1
    const/4 v13, 0x7

    aget v13, v12, v13

    int-to-long v14, v13

    add-long/2addr v4, v14

    .line 306
    sget-object v13, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "IsMediaHubFileIncomplete: Value at 0x46th byte:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    move-object v10, v11

    .line 312
    .end local v7    # "index":I
    .end local v11    # "is":Ljava/io/FileInputStream;
    .restart local v10    # "is":Ljava/io/FileInputStream;
    :cond_2
    :goto_3
    sget-object v13, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "isMediaHubFileIncomplete E. actual_length : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v14

    sub-long v8, v4, v14

    .line 314
    .local v8, "i":J
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileLen:J

    .line 316
    const-wide/16 v14, 0x0

    cmp-long v13, v8, v14

    if-gtz v13, :cond_9

    .line 317
    sget-object v13, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v14, "IsMediaHubFileIncomplete: File is complete"

    invoke-static {v13, v14}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v13, 0x0

    .line 321
    :goto_4
    return v13

    .line 279
    .end local v8    # "i":J
    :catch_1
    move-exception v3

    .line 280
    .local v3, "e":Ljava/io/FileNotFoundException;
    if-eqz v10, :cond_3

    .line 282
    :try_start_2
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 288
    :cond_3
    :goto_5
    if-eqz v10, :cond_2

    .line 289
    const-wide/16 v14, 0x46

    invoke-virtual {v10, v14, v15}, Ljava/io/FileInputStream;->skip(J)J

    .line 291
    const/4 v7, 0x7

    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .restart local v7    # "index":I
    :goto_6
    if-ltz v7, :cond_4

    .line 293
    :try_start_3
    invoke-virtual {v10}, Ljava/io/FileInputStream;->read()I

    move-result v13

    aput v13, v12, v7
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 291
    :goto_7
    add-int/lit8 v7, v7, -0x1

    goto :goto_6

    .line 283
    .end local v7    # "index":I
    .restart local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v6

    .line 284
    .local v6, "e1":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_5

    .line 288
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v6    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    if-eqz v10, :cond_8

    .line 289
    const-wide/16 v14, 0x46

    invoke-virtual {v10, v14, v15}, Ljava/io/FileInputStream;->skip(J)J

    .line 291
    const/4 v7, 0x7

    .restart local v7    # "index":I
    :goto_8
    if-ltz v7, :cond_6

    .line 293
    :try_start_5
    invoke-virtual {v10}, Ljava/io/FileInputStream;->read()I

    move-result v14

    aput v14, v12, v7
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 291
    :goto_9
    add-int/lit8 v7, v7, -0x1

    goto :goto_8

    .line 294
    :catch_3
    move-exception v3

    .line 295
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 296
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    goto :goto_7

    .line 300
    .end local v3    # "e":Ljava/io/IOException;
    :cond_4
    const/4 v7, 0x0

    :goto_a
    const/4 v13, 0x7

    if-ge v7, v13, :cond_5

    .line 301
    aget v13, v12, v7

    int-to-long v14, v13

    add-long/2addr v4, v14

    .line 302
    const/16 v13, 0x8

    shl-long/2addr v4, v13

    .line 300
    add-int/lit8 v7, v7, 0x1

    goto :goto_a

    .line 305
    :cond_5
    const/4 v13, 0x7

    aget v13, v12, v13

    int-to-long v14, v13

    add-long/2addr v4, v14

    .line 306
    sget-object v13, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "IsMediaHubFileIncomplete: Value at 0x46th byte:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_3

    .line 294
    :catch_4
    move-exception v3

    .line 295
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 296
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    goto :goto_9

    .line 300
    .end local v3    # "e":Ljava/io/IOException;
    :cond_6
    const/4 v7, 0x0

    :goto_b
    const/4 v14, 0x7

    if-ge v7, v14, :cond_7

    .line 301
    aget v14, v12, v7

    int-to-long v14, v14

    add-long/2addr v4, v14

    .line 302
    const/16 v14, 0x8

    shl-long/2addr v4, v14

    .line 300
    add-int/lit8 v7, v7, 0x1

    goto :goto_b

    .line 305
    :cond_7
    const/4 v14, 0x7

    aget v14, v12, v14

    int-to-long v14, v14

    add-long/2addr v4, v14

    .line 306
    sget-object v14, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "IsMediaHubFileIncomplete: Value at 0x46th byte:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .end local v7    # "index":I
    :cond_8
    throw v13

    .line 320
    .restart local v8    # "i":J
    :cond_9
    sget-object v13, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v14, "IsMediaHubFileIncomplete: File is incomplete"

    invoke-static {v13, v14}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const/4 v13, 0x1

    goto/16 :goto_4

    .end local v8    # "i":J
    .end local v10    # "is":Ljava/io/FileInputStream;
    .restart local v11    # "is":Ljava/io/FileInputStream;
    :cond_a
    move-object v10, v11

    .end local v11    # "is":Ljava/io/FileInputStream;
    .restart local v10    # "is":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method private parseFileName(Lcom/sec/android/app/videoplayer/type/SchemeType;)Ljava/lang/String;
    .locals 12
    .param p1, "schemeType"    # Lcom/sec/android/app/videoplayer/type/SchemeType;

    .prologue
    const/16 v10, 0x2e

    .line 326
    const-string v0, " "

    .line 328
    .local v0, "displayName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 329
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoName()Ljava/lang/String;

    move-result-object v0

    .line 331
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    sget-object v8, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 332
    invoke-virtual {v0, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 334
    .local v3, "index":I
    if-lez v3, :cond_0

    .line 335
    const/4 v8, 0x0

    invoke-virtual {v0, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 409
    .end local v3    # "index":I
    :cond_0
    sget-object v8, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parseFileName. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v8, v0

    .line 410
    :goto_0
    return-object v8

    .line 337
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    if-eqz v8, :cond_0

    .line 339
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isBrowser(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 340
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 341
    .local v2, "filePath":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v9, "parseFileName(): streaming type."

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const/16 v8, 0x2f

    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 344
    .local v4, "lastIndex1":I
    invoke-virtual {v2, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 346
    .local v5, "lastIndex2":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v4, v8, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v5, v8, :cond_3

    .line 348
    add-int/lit8 v8, v4, 0x1

    :try_start_0
    invoke-virtual {v2, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 401
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v4    # "lastIndex1":I
    .end local v5    # "lastIndex2":I
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 402
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://mms"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://gmail-ls"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 404
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 349
    .restart local v2    # "filePath":Ljava/lang/String;
    .restart local v4    # "lastIndex1":I
    .restart local v5    # "lastIndex2":I
    :catch_0
    move-exception v1

    .line 350
    .local v1, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string v0, " "

    .line 351
    goto :goto_1

    .line 353
    .end local v1    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_3
    const-string v0, " "

    goto :goto_1

    .line 355
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v4    # "lastIndex1":I
    .end local v5    # "lastIndex2":I
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 356
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "file://"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 357
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 359
    .local v7, "temp":Ljava/io/File;
    if-eqz v7, :cond_2

    .line 360
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 361
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 363
    .end local v7    # "temp":Ljava/io/File;
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://mms/part"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://security_mms/part"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 366
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_7

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    goto/16 :goto_1

    .line 369
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileNameByMMS(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 370
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content://"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 371
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 374
    :cond_9
    const/4 v6, 0x0

    .line 376
    .local v6, "newUri":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v8

    if-nez v8, :cond_a

    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 377
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    .line 384
    :goto_2
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v8

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    if-eqz v8, :cond_d

    .line 385
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 390
    :goto_3
    if-nez v0, :cond_2

    .line 391
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v8, :cond_e

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 392
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v9, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 378
    :cond_b
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 379
    sget-object v8, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    goto :goto_2

    .line 381
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    goto :goto_2

    .line 387
    :cond_d
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 393
    :cond_e
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 394
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v9, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 396
    :cond_f
    const-string v0, " "

    goto/16 :goto_1
.end method

.method private setMediaHubFilePath(Landroid/net/Uri;)V
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 219
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v7, "setMediaHubFilePath"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 222
    .local v2, "id":J
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 224
    .local v4, "path":Ljava/lang/String;
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-eqz v6, :cond_1

    .line 225
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    .line 230
    :goto_0
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setMediaHubFilePath. path : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , id : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mMediaHubFilePath : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 233
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v7, "setMediaHubFilePath. mMediaHubFilePath get null. update uri"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v5, "file://"

    .line 236
    .local v5, "preFix":Ljava/lang/String;
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 237
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 238
    .local v1, "len":I
    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    .line 244
    .end local v1    # "len":I
    .end local v5    # "preFix":Ljava/lang/String;
    :cond_0
    :goto_1
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setMediaHubFilePath. mMediaHubFilePath : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWMDRM(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 247
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileIsPyv:Z

    .line 250
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isMediaHubFileIncomplete(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_2
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setMediaHubFilePath. This file is Store Hub file ? "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileIsPyv:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    return-void

    .line 227
    :cond_1
    iput-object v10, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    goto/16 :goto_0

    .line 240
    .restart local v5    # "preFix":Ljava/lang/String;
    :cond_2
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    goto :goto_1

    .line 251
    .end local v5    # "preFix":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z

    .line 253
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 256
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileIsPyv:Z

    .line 257
    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z

    .line 258
    iput-object v10, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public IncompletedMediaHub()Z
    .locals 1

    .prologue
    .line 560
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z

    return v0
.end method

.method public IsNotAllowedScreenMirroring()Z
    .locals 1

    .prologue
    .line 524
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mIsNotAllowedScreenMirroring:Z

    return v0
.end method

.method public SetNotAllowedScreenMirroring(Z)V
    .locals 0
    .param p1, "isNotAllowed"    # Z

    .prologue
    .line 436
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mIsNotAllowedScreenMirroring:Z

    .line 437
    return-void
.end method

.method public checkIsShare()Z
    .locals 3

    .prologue
    .line 495
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkIsShare. mShareViaEnable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mShareViaEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mShareViaEnable:Z

    return v0
.end method

.method public checkIsWMDRM()Z
    .locals 2

    .prologue
    .line 490
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WMDRM_NOT_SUPPORT:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkMediaHubIsPyv()Z
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileIsPyv:Z

    return v0
.end method

.method public getCurPlayingPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 466
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurPlayingPath. mFilePath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getDRMType()I
    .locals 3

    .prologue
    .line 485
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDRMType. mFileDRMType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    return v0
.end method

.method public getDmsContentTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDmsContentTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getFileMediaType()I
    .locals 3

    .prologue
    .line 510
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFileMediaType. mFileMeidaType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    return v0
.end method

.method public getFileTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 471
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromDms()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDmsContentTitle:Ljava/lang/String;

    .line 476
    :goto_0
    return-object v0

    .line 475
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFileTitle. mFileTitle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMMSTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaHubFileTotalLength()J
    .locals 2

    .prologue
    .line 552
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileLen:J

    return-wide v0
.end method

.method public getMediaHubPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getOneFrameSeekEnable()Z
    .locals 3

    .prologue
    .line 505
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOneFrameSeekEnable. mOneFrameSeekEnable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOneFrameSeekEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOneFrameSeekEnable:Z

    return v0
.end method

.method public getPauseEnable()Z
    .locals 1

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mPauseEnable:Z

    return v0
.end method

.method public getVideoDbId()J
    .locals 2

    .prologue
    .line 457
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    return-wide v0
.end method

.method public getVideoUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 461
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoUri. mFileUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    return-object v0
.end method

.method public initInfo()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 126
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    .line 127
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    .line 128
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    .line 129
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    .line 130
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mPauseEnable:Z

    .line 131
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOneFrameSeekEnable:Z

    .line 132
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileTitle:Ljava/lang/String;

    .line 133
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mIsNotAllowedScreenMirroring:Z

    .line 134
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDRMFile:Z

    .line 135
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileIsPyv:Z

    .line 136
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z

    .line 137
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudFile:Z

    .line 138
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSContent:Z

    .line 139
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    .line 140
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mShareViaEnable:Z

    .line 141
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDmsContentTitle:Ljava/lang/String;

    .line 142
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public isAIAMWNotSupportedOnHEVC1080P()Z
    .locals 3

    .prologue
    .line 519
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAIAMWNotSupportedOnHEVC1080P. mAIAMWNotSupportedOnHEVC1080P : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAIAMWNotSupportedOnHEVC1080P:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAIAMWNotSupportedOnHEVC1080P:Z

    return v0
.end method

.method public isAudioOnlyClip()Z
    .locals 2

    .prologue
    .line 540
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCloudFile()Z
    .locals 3

    .prologue
    .line 500
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCloudFile. mCloudFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudFile:Z

    return v0
.end method

.method public isCurPlayingFileExist()Z
    .locals 2

    .prologue
    .line 621
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 622
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 623
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    const/4 v1, 0x1

    .line 626
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDRMFile()Z
    .locals 3

    .prologue
    .line 480
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isDRMFile. mDRMFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDRMFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDRMFile:Z

    return v0
.end method

.method public isFtpSeverFile()Z
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 574
    :cond_0
    const/4 v0, 0x0

    .line 576
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(?i).*/ftp.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isHEVCextension()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 564
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 567
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    const-string v2, ".mp4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFilePath:Ljava/lang/String;

    const-string v2, ".sm4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isLocalContents()Z
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->LOCAL_EXTERNAL:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->LOCAL_INTERNAL:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->LOCAL_USB:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->PRIVATE_STORAGE:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMMSContent()Z
    .locals 1

    .prologue
    .line 528
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSContent:Z

    return v0
.end method

.method public isVideoOnlyClip()Z
    .locals 2

    .prologue
    .line 544
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAIAMWNotSupportedOnHEVC1080P(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 431
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAIANotSupportedOnHEVC1080P set : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAIAMWNotSupportedOnHEVC1080P:Z

    .line 433
    return-void
.end method

.method public setDmsContentTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "dmsContentTitle"    # Ljava/lang/String;

    .prologue
    .line 452
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDmsContentTitle:Ljava/lang/String;

    .line 453
    return-void
.end method

.method public setExitListener(Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;

    .prologue
    .line 584
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOnFinishListener:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;

    .line 585
    return-void
.end method

.method public setFileMediaType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 421
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFileMediaType. type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileMeidaType:I

    .line 423
    return-void
.end method

.method public setFileType(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 592
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 593
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "str":Ljava/lang/String;
    const-string v1, "/storage/extSdCard"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 596
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->LOCAL_EXTERNAL:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    .line 611
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 597
    .restart local v0    # "str":Ljava/lang/String;
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 598
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->LOCAL_INTERNAL:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    goto :goto_0

    .line 599
    :cond_2
    const-string v1, "/storage/UsbDrive"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 600
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->LOCAL_USB:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    goto :goto_0

    .line 601
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 602
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->PRIVATE_STORAGE:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    goto :goto_0

    .line 603
    :cond_4
    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 604
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->HTTP:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    goto :goto_0

    .line 605
    :cond_5
    const-string v1, "rtsp://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->RTSP:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    goto :goto_0

    .line 609
    .end local v0    # "str":Ljava/lang/String;
    :cond_6
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileLocationType:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$FileLocationType;

    goto :goto_0
.end method

.method public setIncompletedMediaHub(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 448
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMediaHubFileInComplete:Z

    .line 449
    return-void
.end method

.method public setMmsContents(Z)V
    .locals 0
    .param p1, "isMms"    # Z

    .prologue
    .line 440
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSContent:Z

    .line 441
    return-void
.end method

.method public setMmsTitleName(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleName"    # Ljava/lang/String;

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mMMSTitleName:Ljava/lang/String;

    .line 445
    return-void
.end method

.method public setOneFrameSeekEnable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 416
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOneFrameSeekEnable. enable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOneFrameSeekEnable:Z

    .line 418
    return-void
.end method

.method public setPauseEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 427
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mPauseEnable:Z

    .line 428
    return-void
.end method

.method public setPlayingFileInfo(Landroid/net/Uri;)Z
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x1

    .line 146
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setPlayingFileInfo E. uri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->initInfo()V

    .line 148
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    .line 150
    if-eqz p1, :cond_a

    .line 151
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    .line 153
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v4

    .line 155
    .local v4, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 156
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceID()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    .line 161
    :goto_0
    iget-wide v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 162
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    .line 165
    :cond_0
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 166
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v7, "setPlayingFileInfo. This is Store Hub File."

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setMediaHubFilePath(Landroid/net/Uri;)V

    .line 170
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getStateAllShareIntent()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 173
    :cond_2
    if-eqz v1, :cond_3

    const-string v6, "http://"

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 174
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .local v3, "sb":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ss"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 180
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 181
    :cond_4
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v7, "setPlayingFileInfo. path is null"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v2, "file://"

    .line 184
    .local v2, "preFix":Ljava/lang/String;
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 185
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 186
    .local v0, "len":I
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 192
    .end local v0    # "len":I
    .end local v2    # "preFix":Ljava/lang/String;
    :cond_5
    :goto_1
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    .line 193
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setFileType(Ljava/lang/String;)V

    .line 195
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->parseFileName(Lcom/sec/android/app/videoplayer/type/SchemeType;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileTitle:Ljava/lang/String;

    .line 196
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v6, v1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    .line 198
    iget v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDRMType:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_6

    .line 199
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDRMFile:Z

    .line 200
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v6, v1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsShare(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mShareViaEnable:Z

    .line 203
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 204
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mCloudFile:Z

    .line 207
    :cond_7
    sget-object v6, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setPlayingFileInfo. mFileUri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mFilePath : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mFileTitle : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileTitle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mFileDBId : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mDRMFile : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mDRMFile:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .end local v1    # "path":Ljava/lang/String;
    .end local v4    # "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    :goto_2
    return v5

    .line 158
    .restart local v4    # "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileDBId:J

    goto/16 :goto_0

    .line 188
    .restart local v1    # "path":Ljava/lang/String;
    .restart local v2    # "preFix":Ljava/lang/String;
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 212
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "preFix":Ljava/lang/String;
    .end local v4    # "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    :cond_a
    sget-object v5, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->TAG:Ljava/lang/String;

    const-string v6, "setPlayingFileInfo() - uri is null. cannot play! -> finish!"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->mOnFinishListener:Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;

    invoke-interface {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;->onFinish()V

    .line 214
    const/4 v5, 0x0

    goto :goto_2
.end method
