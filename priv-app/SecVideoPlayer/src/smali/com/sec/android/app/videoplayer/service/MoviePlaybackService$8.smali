.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 1323
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 6
    .param p1, "focusChange"    # I

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1325
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAudioFocusListener - focusChange : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    packed-switch p1, :pswitch_data_0

    .line 1430
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1330
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z

    .line 1334
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1335
    const-string v0, "MoviePlaybackService"

    const-string v1, "mAudioFocusListener - AUDIOFOCUS_LOSS : Music is Playing Do Not Auto Resume Video Playback..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 1340
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_2

    .line 1341
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v4, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 1342
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    .line 1344
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    goto :goto_0

    .line 1346
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->pause()V

    goto :goto_0

    .line 1349
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    goto :goto_0

    .line 1359
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->checkIsRinging(Landroid/content/Context;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setVolume(F)V

    .line 1361
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA_CSFB_STREAMING:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1364
    const-string v0, "MoviePlaybackService"

    const-string v1, "mAudioFocusListener. stop by calling"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1902(Z)Z

    .line 1366
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1367
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v4, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 1368
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    .line 1369
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x66

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 1371
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z

    .line 1372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->gotoIdleState()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    goto/16 :goto_0

    .line 1376
    :cond_5
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-eqz v0, :cond_6

    .line 1377
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 1384
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1385
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1386
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1387
    const-string v0, "MoviePlaybackService"

    const-string v1, "mAudioFocusListener. pause by alert sound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z

    .line 1389
    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1902(Z)Z

    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->pauseOrStopByAlertSound()V

    goto/16 :goto_0

    .line 1380
    :cond_7
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    .line 1392
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2102(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 1393
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unPausable-mute: savedVolume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1394
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setVolume(I)V

    goto/16 :goto_0

    .line 1401
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1402
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z

    .line 1403
    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1902(Z)Z

    .line 1405
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-eqz v0, :cond_9

    .line 1406
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 1409
    :cond_9
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPauseSetEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1410
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    if-ne v0, v4, :cond_b

    .line 1411
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->updatePlaySpeed(I)V

    .line 1412
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1413
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    .line 1419
    :cond_a
    :goto_1
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSavedVolumeByTransientLossOfFocus "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    if-eq v0, v5, :cond_0

    .line 1421
    const-string v0, "MoviePlaybackService"

    const-string v1, "unPausable-muted. set savedVolume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setVolume(I)V

    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I
    invoke-static {v0, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2102(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    goto/16 :goto_0

    .line 1415
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startPlay()Z

    goto :goto_1

    .line 1327
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
