.class public Lcom/sec/android/app/videoplayer/type/VideoListType;
.super Ljava/lang/Object;
.source "VideoListType.java"


# static fields
.field public static final SORTBY_DATE:I = 0x1

.field public static final SORTBY_NAME:I = 0x2

.field public static final SORTBY_RECENTLY_VIEWED:I = 0x0

.field public static final SORTBY_SIZE:I = 0x3

.field public static final SORTBY_TYPE:I = 0x4

.field public static final VIDEOLIST_ALLSHARE_DEVICE_LIST:I = 0xa

.field public static final VIDEOLIST_ALLSHARE_DOWNLOAD:I = 0xc

.field public static final VIDEOLIST_ALLSHARE_FILE_LIST:I = 0xb

.field public static final VIDEOLIST_DOWNLOAD:I = 0xe

.field public static final VIDEOLIST_ETC:I = -0x1

.field public static final VIDEOLIST_FOLDER:I = 0x2

.field public static final VIDEOLIST_FOLDER_LIST:I = 0x9

.field public static final VIDEOLIST_GRID:I = 0x0

.field public static final VIDEOLIST_LIST:I = 0x1

.field public static final VIDEOLIST_REMOVE:I = 0x5

.field public static final VIDEOLIST_REMOVE_FOLDERLIST:I = 0x7

.field public static final VIDEOLIST_SEARCH_RESULT:I = 0x6

.field public static final VIDEOLIST_SELECT:I = 0x3

.field public static final VIDEOLIST_SELECT_CLOUD:I = 0x4

.field public static final VIDEOLIST_SELECT_FOLDERLIST:I = 0x8

.field public static final VIDEOLIST_SELECT_REVERT_CLOUD:I = 0xd

.field public static final VIDEO_DB_COLUMN_ADDRESS:Ljava/lang/String; = "addr"

.field public static final VIDEO_DB_COLUMN_ISPLAYED:Ljava/lang/String; = "isPlayed"

.field public static final VIDEO_DB_COLUMN_RESUME_POS:Ljava/lang/String; = "resumePos"

.field public static final VIDEO_DB_COLUMN_WEATHER_TAG:Ljava/lang/String; = "weather_ID"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAllShareVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 79
    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "thumbnail"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "seed"

    aput-object v2, v0, v1

    .line 87
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getCloudFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "distinct bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1 as _id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "1 as cloud_server_id"

    aput-object v2, v0, v1

    .line 66
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getDefaultVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "isPlayed"

    aput-object v2, v0, v1

    .line 48
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getDrmCustomDataColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 109
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user_guid"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "imei"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "app_Id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mv_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "svr_id"

    aput-object v2, v0, v1

    .line 116
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static final getFindoRegexVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 120
    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    .line 128
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static final getFindoTagVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 132
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "addr"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "weather_ID"

    aput-object v2, v0, v1

    .line 137
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "distinct bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1 as _id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    .line 57
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getSKTCloudFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "distinct bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1 as _id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "1 as tcloud_server_id"

    aput-object v2, v0, v1

    .line 75
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getVideoHubColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    const/16 v1, 0xc

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "order_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "video_attr_type_code_2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "wfd_yn"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "product_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "fhd_file_size"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "hd_file_size"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sd_file_size"

    aput-object v2, v0, v1

    .line 105
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method
