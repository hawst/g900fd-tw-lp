.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 961
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 963
    const/4 v1, 0x0

    .line 964
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 965
    sparse-switch p2, :sswitch_data_0

    .line 989
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 990
    .local v0, "isReturn":Z
    if-eqz v0, :cond_3

    .line 1008
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 968
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 970
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 974
    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 975
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 977
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    if-nez v2, :cond_2

    .line 978
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    new-instance v3, Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;-><init>(Landroid/content/Context;)V

    iput-object v3, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .line 980
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->showPopup()V

    goto :goto_0

    .line 993
    .restart local v0    # "isReturn":Z
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 995
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 996
    goto :goto_0

    .line 999
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1000
    goto :goto_0

    .line 965
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 968
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 993
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
