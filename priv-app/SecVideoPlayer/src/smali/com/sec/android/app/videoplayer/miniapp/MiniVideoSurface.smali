.class public Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.super Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;
.source "MiniVideoSurface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;,
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;,
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;
    }
.end annotation


# static fields
.field private static final MAX_MULTI_SUBTITLE:I = 0x32

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PAUSED:I = 0x4

.field private static final STATE_PLAYBACK_COMPLETED:I = 0x5

.field private static final STATE_PLAYING:I = 0x3

.field private static final STATE_PREPARED:I = 0x2

.field private static final STATE_PREPARING:I = 0x1

.field private static mHasAudioFocus:Z


# instance fields
.field private final SEND_TO_BT_METADATA:I

.field private final SEND_TO_BT_PLAYSTATUS:I

.field private TAG:Ljava/lang/String;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCurrentState:I

.field private mDuration:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mIsAudioOnlyClip:Z

.field private mIsCoverClosed:Z

.field private mIsPauseEnable:Z

.field private mIsVideoRenderingStarted:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mOnSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

.field private mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

.field private final mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

.field mPausedByTransientLossOfFocus:Z

.field private mPercentchecker:Z

.field mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSA:Landroid/media/audiofx/SoundAlive;

.field mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mSeekWhenPrepared:I

.field mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mSoundAliveMode:I

.field private mSubtitleController:Landroid/media/SubtitleController;

.field private mSubtitleTempText:[Ljava/lang/String;

.field private mSubtitleText:Ljava/lang/String;

.field private mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mTargetState:I

.field private mUri:Landroid/net/Uri;

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;-><init>(Landroid/content/Context;)V

    .line 78
    const-string v0, "MiniVideoSurface"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 92
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 93
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    .line 113
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;

    .line 123
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 126
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 127
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSoundAliveMode:I

    .line 134
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsVideoRenderingStarted:Z

    .line 135
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsCoverClosed:Z

    .line 428
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 447
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 492
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 507
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 525
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 569
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    .line 638
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 863
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    .line 864
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1249
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$10;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 1320
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 1525
    const/16 v0, 0x12d

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->SEND_TO_BT_PLAYSTATUS:I

    .line 1527
    const/16 v0, 0x12e

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->SEND_TO_BT_METADATA:I

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 139
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .line 141
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->initVideoView()V

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    const-string v0, "MiniVideoSurface"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 92
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 93
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    .line 113
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;

    .line 123
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 126
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 127
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSoundAliveMode:I

    .line 134
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsVideoRenderingStarted:Z

    .line 135
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsCoverClosed:Z

    .line 428
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 447
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 492
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 507
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 525
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 569
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    .line 638
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 863
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    .line 864
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1249
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$10;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 1320
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 1525
    const/16 v0, 0x12d

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->SEND_TO_BT_PLAYSTATUS:I

    .line 1527
    const/16 v0, 0x12e

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->SEND_TO_BT_METADATA:I

    .line 146
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->initVideoView()V

    .line 148
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnInfoListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->release(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$2002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 73
    sput-boolean p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    return p0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Lcom/sec/android/app/videoplayer/type/SchemeType;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/type/SchemeType;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isBufferingStreamingType()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isDownloadStreamingType()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    return p1
.end method

.method static synthetic access$602(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    return v0
.end method

.method private addOutbandSubTitleinVideoClip(Ljava/lang/String;)Z
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 821
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v4, "addOutbandSubTitleinVideoClip"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_2

    .line 824
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 825
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v1

    .line 826
    .local v1, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->clearOutbandTextTrack()V

    .line 829
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->removeOutbandTimedTextSource()V

    .line 831
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->removeOutbandSubtitleSource()V

    .line 833
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFileTyp()I

    move-result v3

    const/16 v4, 0x69

    if-ne v3, v4, :cond_1

    .line 834
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v4, "addOutbandSubTitle. WebVTT type so we call another API"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v5, "text/vtt"

    const-string v6, "und"

    invoke-static {v5, v6}, Landroid/media/MediaFormat;->createSubtitleFormat(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaFormat;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/media/MediaPlayer;->addSubtitleSource(Ljava/io/InputStream;Landroid/media/MediaFormat;)V

    .line 860
    .end local v1    # "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :cond_0
    :goto_0
    return v2

    .line 842
    .restart local v1    # "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-string v4, "application/x-subrip"

    invoke-virtual {v3, p1, v4}, Landroid/media/MediaPlayer;->addTimedTextSourceSEC(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getOutbandTimedTextTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->addOutbandTextTrackInfo([Landroid/media/MediaPlayer$TrackInfo;)V

    .line 846
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitle()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v3

    if-nez v3, :cond_0

    .line 860
    .end local v1    # "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :cond_2
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 848
    .restart local v1    # "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :catch_0
    move-exception v0

    .line 849
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 850
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 851
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 852
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 853
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 854
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_3
    move-exception v0

    .line 855
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private deselectSubtitleTrack()V
    .locals 6

    .prologue
    .line 1471
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v4, "deselectSubtitleTrack()"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_2

    .line 1475
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    .line 1477
    .local v2, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 1478
    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 1481
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v1}, Landroid/media/MediaPlayer;->deselectTrack(I)V

    .line 1482
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deselectSubtitleTrack : mMediaPlayer.selectTrack : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1477
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1485
    .end local v1    # "index":I
    .end local v2    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 1486
    .local v0, "e":Ljava/lang/RuntimeException;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v4, "deselectSubtitleTrack : RuntimeException"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1489
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_2
    return-void
.end method

.method private getSoundAliveMode(I)I
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 1052
    return p1
.end method

.method private initScreenScaling()V
    .locals 1

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v0, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->initScreenScaling()V

    .line 1263
    :cond_0
    return-void
.end method

.method private initVideoView()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 210
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    .line 211
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    .line 212
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 214
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setFocusable(Z)V

    .line 215
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setFocusableInTouchMode(Z)V

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestFocus()Z

    .line 217
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 218
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 221
    .local v0, "powermanager":Landroid/os/PowerManager;
    const v1, 0x2000000a

    const-string v2, "MiniMoviePlayer-Sleep"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 225
    new-instance v1, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$1;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    .line 249
    return-void
.end method

.method private isBufferingStreamingType()Z
    .locals 1

    .prologue
    .line 1365
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDownloadStreamingType()Z
    .locals 1

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScreenScaling()Z
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-nez v0, :cond_0

    .line 1267
    const/4 v0, 0x0

    .line 1269
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isScreenScaling()Z

    move-result v0

    goto :goto_0
.end method

.method private release(Z)V
    .locals 3
    .param p1, "cleartargetstate"    # Z

    .prologue
    const/4 v2, 0x0

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v1, "mini release() start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->unregisterMotionListener()V

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 680
    if-eqz p1, :cond_1

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAIAContext(Z)V

    .line 683
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 684
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 685
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 686
    if-eqz p1, :cond_2

    .line 687
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 689
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    .line 691
    :cond_3
    return-void
.end method

.method private requestAudioFocus()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1152
    sget-boolean v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    if-nez v2, :cond_1

    .line 1153
    sput-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    .line 1154
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->gainAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 1155
    .local v0, "ret":I
    if-ne v0, v1, :cond_0

    .line 1162
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 1158
    .restart local v0    # "ret":I
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    .line 1162
    .end local v0    # "ret":I
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    goto :goto_0
.end method

.method private selectSubtitleTrack(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 1450
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 1452
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    .line 1454
    .local v1, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    array-length v2, v1

    if-ge p1, v2, :cond_2

    if-ltz p1, :cond_2

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1459
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectSubtitleTrack : mMediaPlayer.selectTrack : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p1}, Landroid/media/MediaPlayer;->selectTrack(I)V

    .line 1468
    .end local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :cond_1
    :goto_0
    return-void

    .line 1462
    .restart local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectSubtitleTrack : Subtitle NOT Selected! : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1464
    .end local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 1465
    .local v0, "e":Ljava/lang/RuntimeException;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v3, "selectSubtitleTrack : RuntimeException"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "mode"    # Z

    .prologue
    .line 301
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    if-nez v1, :cond_0

    .line 302
    new-instance v1, Landroid/media/audiofx/SoundAlive;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    if-eqz v1, :cond_1

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v1, p2}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 307
    if-eqz p2, :cond_2

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setEnableSoundAlive New usePreset "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSoundAliveMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSoundAliveMode:I

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Landroid/media/audiofx/SoundAlive;->usePreset(S)V

    .line 324
    :cond_1
    :goto_0
    return-void

    .line 311
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v1}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 312
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 315
    :catch_0
    move-exception v0

    .line 316
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 317
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 318
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 319
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 321
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_3
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method private setTracksAndGetLangIndex(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;)I
    .locals 2
    .param p1, "track"    # Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    if-eqz v0, :cond_0

    .line 1493
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setTimedTextTrack()V

    .line 1494
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v0

    .line 1498
    :goto_0
    return v0

    .line 1497
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v1, "setSubtitleLanguage : mSubtitleUtil is NULL!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1498
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setXY()V
    .locals 3

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v0, :cond_0

    .line 1274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setXY(II)V

    .line 1275
    :cond_0
    return-void
.end method


# virtual methods
.method public IsPauseEnable()Z
    .locals 1

    .prologue
    .line 1184
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    return v0
.end method

.method public abandonAudioFocus()V
    .locals 2

    .prologue
    .line 1166
    sget-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 1167
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z

    .line 1168
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1170
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1209
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-nez v1, :cond_0

    .line 1210
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 1237
    :goto_0
    return v1

    .line 1213
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setXY()V

    .line 1215
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getScaleDetector()Landroid/view/ScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1217
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1218
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 1231
    :goto_1
    :pswitch_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getScaleDetector()Landroid/view/ScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1232
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setBackgroundVisible(Ljava/lang/Boolean;)V

    .line 1235
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isScreenScaling()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    .line 1220
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->initScreenScaling()V

    goto :goto_1

    .line 1225
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->initScreenScaling()V

    goto :goto_1

    .line 1237
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 1218
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 981
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 984
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 969
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 970
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    if-lez v0, :cond_0

    .line 971
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    .line 977
    :goto_0
    return v0

    .line 973
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    .line 974
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    goto :goto_0

    .line 976
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    .line 977
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    goto :goto_0
.end method

.method public getFPS()I
    .locals 4

    .prologue
    .line 1278
    const/4 v0, -0x1

    .line 1279
    .local v0, "fps":I
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->HIGH_SPEED_PLAY:Z

    if-eqz v1, :cond_0

    .line 1280
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1281
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5e1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->getIntParameter(I)I

    move-result v0

    .line 1284
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFPS() - fps:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1285
    return v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    return v0
.end method

.method public getTimedTextLanguage()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 799
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v4, :cond_0

    move-object v1, v3

    .line 817
    :goto_0
    return-object v1

    .line 802
    :cond_0
    const/4 v1, 0x0

    .line 803
    .local v1, "str":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    .line 805
    .local v2, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    if-nez v2, :cond_1

    move-object v1, v3

    .line 806
    goto :goto_0

    .line 808
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_4

    .line 809
    aget-object v3, v2, v0

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    aget-object v3, v2, v0

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3

    .line 810
    :cond_2
    aget-object v3, v2, v0

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 811
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTimedTextLanguage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 808
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 816
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v4, "getTimedTextLanguage : NULL"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 1201
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 1197
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    return v0
.end method

.method public isAudioOnlyClip()Z
    .locals 1

    .prologue
    .line 1317
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    return v0
.end method

.method public isInPlaybackState()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1145
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1121
    const/4 v1, 0x0

    .line 1123
    .local v1, "isplaying":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v3, :cond_1

    .line 1137
    :cond_0
    :goto_0
    return v2

    .line 1127
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1132
    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1133
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isPlaying() - mCurrentState is mass up : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1134
    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 1137
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 1128
    :catch_0
    move-exception v0

    .line 1129
    .local v0, "e":Ljava/lang/IllegalStateException;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isPreparing()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1141
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1289
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->HIGH_SPEED_PLAY:Z

    if-eqz v1, :cond_0

    .line 1290
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1291
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5e2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->getIntParameter(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1292
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "isSECVideo() - true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1297
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoRenderingStarted()Z
    .locals 1

    .prologue
    .line 1502
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsVideoRenderingStarted:Z

    return v0
.end method

.method public isWideClip()Z
    .locals 2

    .prologue
    .line 1205
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v7, 0x78

    .line 159
    const/4 v3, 0x0

    .line 160
    .local v3, "videoWidth":I
    const/4 v2, 0x0

    .line 161
    .local v2, "videoHeight":I
    const/4 v1, 0x1

    .line 162
    .local v1, "surfaceWidth":I
    const/4 v0, 0x1

    .line 163
    .local v0, "surfaceHeight":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_0

    .line 164
    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    .line 165
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    .line 166
    invoke-virtual {p0, v3, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resolveAdjustedSize(II)I

    move-result v1

    .line 167
    invoke-virtual {p0, v2, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resolveAdjustedSize(II)I

    move-result v0

    .line 169
    if-lez v3, :cond_3

    if-lez v2, :cond_3

    .line 170
    sget-boolean v4, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mLarge:Z

    if-eqz v4, :cond_2

    .line 171
    mul-int v4, v3, v0

    mul-int v5, v2, v1

    if-lt v4, v5, :cond_1

    .line 172
    mul-int v4, v1, v2

    div-int v0, v4, v3

    .line 185
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setting video size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setting size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-super {p0, v1, v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setMeasuredDimension(II)V

    .line 188
    return-void

    .line 174
    :cond_1
    mul-int v4, v0, v3

    div-int v1, v4, v2

    goto :goto_0

    .line 176
    :cond_2
    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I

    invoke-static {v4, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDefaultSize(II)I

    move-result v1

    .line 177
    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I

    invoke-static {v4, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDefaultSize(II)I

    move-result v0

    goto :goto_0

    .line 179
    :cond_3
    if-nez v3, :cond_0

    if-nez v2, :cond_0

    .line 180
    const/4 v1, 0x1

    .line 181
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onScreenStateChanged(I)V
    .locals 2
    .param p1, "screenState"    # I

    .prologue
    .line 1242
    if-nez p1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1243
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    .line 1246
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->onScreenStateChanged(I)V

    .line 1247
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 695
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v1, "onTouchEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    const/4 v0, 0x0

    return v0
.end method

.method public openVideo()V
    .locals 15

    .prologue
    const-wide/16 v10, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, -0x1

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "mini openVideo() start"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "openVideo() uri null or surfaceholder null"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    :goto_0
    return-void

    .line 343
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resetVideoRenderingStarted()V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 350
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-nez v0, :cond_3

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getSchemeType()Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 355
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v9

    .line 361
    .local v9, "path":Ljava/lang/String;
    :goto_1
    invoke-direct {p0, v14}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->release(Z)V

    .line 364
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 368
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 374
    new-instance v0, Landroid/media/SubtitleController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getMediaTimeProvider()Landroid/media/MediaTimeProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {v0, v2, v3, v4}, Landroid/media/SubtitleController;-><init>(Landroid/content/Context;Landroid/media/MediaTimeProvider;Landroid/media/SubtitleController$Listener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleController:Landroid/media/SubtitleController;

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleController:Landroid/media/SubtitleController;

    new-instance v2, Landroid/media/WebVttRenderer;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/media/WebVttRenderer;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/media/SubtitleController;->registerRenderer(Landroid/media/SubtitleController$Renderer;)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleController:Landroid/media/SubtitleController;

    invoke-virtual {v0, v2, p0}, Landroid/media/MediaPlayer;->setSubtitleAnchor(Landroid/media/SubtitleController;Landroid/media/SubtitleController$Anchor;)V

    .line 378
    const-string v0, "content://"

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 401
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAIAContext(Z)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 406
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 407
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 408
    :catch_0
    move-exception v6

    .line 409
    .local v6, "ex":Ljava/io/IOException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 410
    iput v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 411
    iput v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v0, v2, v13, v14}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0

    .line 358
    .end local v6    # "ex":Ljava/io/IOException;
    .end local v9    # "path":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "path":Ljava/lang/String;
    goto/16 :goto_1

    .line 380
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isHEVCPDType()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getFileSize()J

    move-result-wide v2

    cmp-long v0, v2, v10

    if-lez v0, :cond_9

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "setDataSource. play store hub HEVC MP4 PD!!!!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 384
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 385
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 388
    .local v8, "is":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    .line 389
    .local v1, "fd":Ljava/io/FileDescriptor;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-wide/16 v2, 0x0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getFileSize()J

    move-result-wide v4

    const-wide/high16 v10, -0x100000000000000L

    or-long/2addr v4, v10

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391
    if-eqz v8, :cond_4

    .line 392
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2

    .line 414
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "is":Ljava/io/FileInputStream;
    :catch_1
    move-exception v6

    .line 415
    .local v6, "ex":Ljava/lang/IllegalStateException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalStateException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 416
    iput v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 417
    iput v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v0, v2, v13, v14}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0

    .line 391
    .end local v6    # "ex":Ljava/lang/IllegalStateException;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "is":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_7

    .line 392
    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    :cond_7
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    .line 419
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "is":Ljava/io/FileInputStream;
    :catch_2
    move-exception v6

    .line 420
    .local v6, "ex":Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalArgumentException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 421
    iput v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 422
    iput v12, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v0, v2, v13, v14}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0

    .line 396
    .end local v6    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v7    # "file":Ljava/io/File;
    :cond_8
    :try_start_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v9}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 399
    .end local v7    # "file":Ljava/io/File;
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v9}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_2
.end method

.method public pause()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 918
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->unregisterMotionListener()V

    .line 921
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 922
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    if-eqz v0, :cond_4

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 924
    const/16 v0, 0x12d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->sendBTbroadcast(I)V

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    if-eqz v0, :cond_1

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;->onPause()V

    .line 933
    :cond_1
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 947
    :cond_2
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    if-nez v0, :cond_3

    .line 948
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->abandonAudioFocus()V

    .line 951
    :cond_3
    iput v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 952
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    .line 953
    :goto_1
    return-void

    .line 929
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    goto :goto_1

    .line 934
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 935
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() - isPreparing() :: isPlaying = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mIsPauseEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    if-eqz v0, :cond_6

    .line 939
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    if-eqz v0, :cond_2

    .line 940
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;->onPause()V

    goto :goto_0

    .line 942
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    goto :goto_1
.end method

.method public resetVideoRenderingStarted()V
    .locals 2

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v1, "resetVideoRenderingStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1507
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsVideoRenderingStarted:Z

    .line 1508
    return-void
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 191
    move v0, p1

    .line 192
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 193
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 195
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 206
    :goto_0
    return v0

    .line 197
    :sswitch_0
    move v0, p1

    .line 198
    goto :goto_0

    .line 200
    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 201
    goto :goto_0

    .line 203
    :sswitch_2
    move v0, v2

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 960
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 961
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->start()V

    .line 965
    :cond_0
    :goto_0
    return-void

    .line 962
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 963
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->openVideo()V

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 1077
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 1079
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    .line 1083
    :goto_0
    return-void

    .line 1081
    :cond_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    goto :goto_0
.end method

.method public seekTo(II)V
    .locals 4
    .param p1, "msec"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 1087
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1118
    :cond_0
    :goto_0
    return-void

    .line 1089
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v1

    if-gtz v1, :cond_2

    .line 1090
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "seekTo() - visual Seek - live streaming. don\'t seek."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1094
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-nez v1, :cond_3

    .line 1095
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getSchemeType()Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 1098
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1099
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "seekTo() - visual Seek - streaming/download tab. call seek()."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(I)V

    goto :goto_0

    .line 1104
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1105
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getSeekModeForAllShare(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1107
    .local v0, "ASFseekMode":Ljava/lang/String;
    if-eqz v0, :cond_6

    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1108
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "seekTo. SeekMode is NONE return"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1113
    .end local v0    # "ASFseekMode":Ljava/lang/String;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mDuration:I

    if-lez v1, :cond_0

    .line 1114
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: seek() pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seekMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1, p2}, Landroid/media/MediaPlayer;->seekTo(II)V

    .line 1116
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    goto/16 :goto_0
.end method

.method public selectSubtitle()Z
    .locals 12

    .prologue
    .line 1370
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle E"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1371
    const/4 v5, 0x0

    .line 1373
    .local v5, "ret":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v9

    .line 1374
    .local v9, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    const/4 v8, 0x0

    .line 1376
    .local v8, "track":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    if-nez v9, :cond_0

    move v6, v5

    .line 1446
    .end local v5    # "ret":Z
    .local v6, "ret":I
    :goto_0
    return v6

    .line 1380
    .end local v6    # "ret":I
    .restart local v5    # "ret":Z
    :cond_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFile()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 1381
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getOutbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v8

    .line 1386
    :goto_1
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v10

    if-lez v10, :cond_4

    .line 1387
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->deselectSubtitleTrack()V

    .line 1389
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getIsMultiSubtitle()Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v10

    if-eqz v10, :cond_5

    .line 1391
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle. subtitle language is multiple"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v10, v10, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedTrackIndex:[I

    if-nez v10, :cond_1

    .line 1394
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    const/16 v11, 0x32

    new-array v11, v11, [I

    iput-object v11, v10, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedTrackIndex:[I

    .line 1397
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v10

    array-length v10, v10

    if-ge v0, v10, :cond_3

    .line 1398
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v10

    aget v7, v10, v0

    .line 1399
    .local v7, "selectedIndex":I
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v1, v10, v7

    .line 1400
    .local v1, "idx":I
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitleTrack(I)V

    .line 1401
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v10, v10, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedTrackIndex:[I

    aput v1, v10, v0

    .line 1397
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1383
    .end local v0    # "i":I
    .end local v1    # "idx":I
    .end local v7    # "selectedIndex":I
    :cond_2
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v8

    goto :goto_1

    .line 1404
    .restart local v0    # "i":I
    :cond_3
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setTracksAndGetLangIndex(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;)I

    .line 1405
    const/4 v5, 0x1

    .end local v0    # "i":I
    :cond_4
    :goto_3
    move v6, v5

    .line 1446
    .restart local v6    # "ret":I
    goto :goto_0

    .line 1408
    .end local v6    # "ret":I
    :cond_5
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle. subtitle language is single"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setTracksAndGetLangIndex(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;)I

    move-result v0

    .line 1411
    .restart local v0    # "i":I
    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->getTrackType()I

    move-result v10

    const/4 v11, 0x4

    if-ne v10, v11, :cond_b

    .line 1412
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle. subtitle language is single. WebVTT"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    move-result-object v4

    .line 1415
    .local v4, "mCaptionMgr":Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->isCaptionEnable()Z

    move-result v10

    if-nez v10, :cond_7

    .line 1416
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle : CaptioningEnabled = false"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    .end local v4    # "mCaptionMgr":Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    :cond_6
    :goto_4
    const/4 v5, 0x1

    goto :goto_3

    .line 1417
    .restart local v4    # "mCaptionMgr":Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    :cond_7
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFile()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    .line 1418
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle : SUBTITLE_TYPE_OUTBAND"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v10, v10, v0

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitleTrack(I)V

    goto :goto_4

    .line 1421
    :cond_8
    const/4 v2, 0x0

    .line 1423
    .local v2, "inbandLanguage":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->getLocale()Ljava/util/Locale;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 1424
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->getLocale()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    .line 1427
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_5
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v3, v10, :cond_6

    .line 1428
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1429
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v10, v10, v3

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitleTrack(I)V

    goto :goto_4

    .line 1427
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1434
    .end local v3    # "j":I
    :cond_a
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v10, v10, v0

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitleTrack(I)V

    goto :goto_4

    .line 1438
    .end local v2    # "inbandLanguage":Ljava/lang/String;
    .end local v4    # "mCaptionMgr":Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    :cond_b
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v11, "selectSubtitle. subtitle language is single. SMI, SRT, SUB"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    iget-object v10, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v10, v10, v0

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitleTrack(I)V

    goto :goto_4
.end method

.method public sendBTbroadcast(I)V
    .locals 14
    .param p1, "mode"    # I

    .prologue
    const/4 v13, 0x1

    .line 1530
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    .line 1532
    .local v2, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    .line 1534
    const/16 v10, 0x12d

    if-ne p1, v10, :cond_1

    .line 1535
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v12

    invoke-virtual {v2, v10, v11, v12}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(JZ)V

    .line 1537
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. ACTION_SEND_TO_BT_PLAYSTATUS = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    new-instance v5, Landroid/content/Intent;

    const-string v10, "com.sec.android.videoplayer.playerstatus"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1539
    .local v5, "i":Landroid/content/Intent;
    const-string v10, "playing"

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v11

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1540
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-virtual {v10, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1586
    .end local v5    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1541
    :cond_1
    const/16 v10, 0x12e

    if-ne p1, v10, :cond_0

    .line 1542
    const/4 v3, 0x0

    .line 1543
    .local v3, "curIdx":I
    const/4 v9, 0x0

    .line 1544
    .local v9, "totalVideoCnt":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v10

    int-to-long v6, v10

    .line 1546
    .local v6, "duration":J
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    .line 1548
    .local v4, "curUri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v10

    invoke-virtual {v10, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 1549
    .local v8, "title":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v10

    invoke-virtual {v10, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getArtist(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1550
    .local v1, "artist":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v10

    invoke-virtual {v10, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getAlbum(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1552
    .local v0, "album":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1553
    const/4 v3, 0x0

    .line 1555
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-gtz v10, :cond_3

    .line 1556
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0145

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1559
    :cond_3
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v10, :cond_4

    .line 1560
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_TITLE = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1561
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_ALBUMARTIST = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1562
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_ALBUM = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1563
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_CD_TRACK_NUMBER = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_DISC_NUMBER = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1565
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_DURATION = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1568
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->initMetaData()V

    .line 1569
    const-string v10, "android.media.metadata.TITLE"

    invoke-virtual {v2, v10, v8}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    const-string v10, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {v2, v10, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    const-string v10, "android.media.metadata.ALBUM"

    invoke-virtual {v2, v10, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    const-string v10, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {v2, v10, v13}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataInt(Ljava/lang/String;I)V

    .line 1573
    const-string v10, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v2, v10, v9}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataInt(Ljava/lang/String;I)V

    .line 1574
    const-string v10, "android.media.metadata.DURATION"

    invoke-virtual {v2, v10, v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataLong(Ljava/lang/String;J)V

    .line 1575
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->updateMetadata()V

    .line 1577
    new-instance v5, Landroid/content/Intent;

    const-string v10, "com.sec.android.videoplayer.metachanged"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1578
    .restart local v5    # "i":Landroid/content/Intent;
    const-string v10, "title"

    invoke-virtual {v5, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1579
    const-string v10, "artist"

    invoke-virtual {v5, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1580
    const-string v10, "album"

    invoke-virtual {v5, v10, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1581
    const-string v10, "mediaCount"

    invoke-virtual {v5, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1582
    const-string v10, "listpos"

    invoke-virtual {v5, v10, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1583
    const-string v10, "duration"

    invoke-virtual {v5, v10, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1584
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-virtual {v10, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public setAudioTrack(I)V
    .locals 4
    .param p1, "mTrackNumber"    # I

    .prologue
    .line 991
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 992
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTrackNumber() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->selectTrack(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 998
    :cond_0
    :goto_0
    return-void

    .line 995
    :catch_0
    move-exception v0

    .line 996
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public setIsCoverClosed(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 1589
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SCover : State "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_0

    const-string v0, "Closed"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1590
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsCoverClosed:Z

    .line 1591
    return-void

    .line 1589
    :cond_0
    const-string v0, "Open"

    goto :goto_0
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 619
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 620
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 623
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 624
    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnInfoListener;

    .prologue
    .line 627
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 628
    return-void
.end method

.method public setOnPauseListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    .prologue
    .line 1305
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPauseListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnPauseListener;

    .line 1306
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 615
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 616
    return-void
.end method

.method public setOnSizeChangeListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    .prologue
    .line 631
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    .line 632
    return-void
.end method

.method public setOnStartListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    .prologue
    .line 1313
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    .line 1314
    return-void
.end method

.method public setPlaySpeed(F)V
    .locals 6
    .param p1, "speed"    # F

    .prologue
    .line 1057
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPlaySpeed (speed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is called"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    .line 1060
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    .line 1061
    .local v1, "playSpeed":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 1062
    .local v2, "playSpeed_Reply":Landroid/os/Parcel;
    const/16 v3, 0x400

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1063
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1065
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v1, v2}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1074
    .end local v1    # "playSpeed":Landroid/os/Parcel;
    .end local v2    # "playSpeed_Reply":Landroid/os/Parcel;
    :cond_0
    :goto_0
    return-void

    .line 1066
    :catch_0
    move-exception v0

    .line 1067
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1068
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1069
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 1070
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_2
    move-exception v0

    .line 1071
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSeekPosition(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSeekPosition. pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I

    .line 329
    return-void
.end method

.method public setSoundAliveMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 1001
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSoundAliveMode (mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is called"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getSoundAliveMode(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSoundAliveMode:I

    .line 1014
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v1}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1015
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSA:Landroid/media/audiofx/SoundAlive;

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSoundAliveMode:I

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Landroid/media/audiofx/SoundAlive;->usePreset(S)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1026
    :cond_0
    :goto_0
    return-void

    .line 1017
    :catch_0
    move-exception v0

    .line 1018
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1019
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1020
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1021
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1022
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 1023
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_3
    move-exception v0

    .line 1024
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSubtitleSyncTime(I)V
    .locals 4
    .param p1, "synctime"    # I

    .prologue
    .line 1188
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitleSyncTime() - synctime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5dd

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setParameter(II)Z

    move-result v0

    .line 1192
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitleSyncTime setParameter : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    .end local v0    # "result":Z
    :cond_0
    return-void
.end method

.method public setSubtitleUtil(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;)V
    .locals 2
    .param p1, "subtitleUtil"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    .prologue
    .line 151
    if-eqz p1, :cond_0

    .line 152
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v1, "setSubtitleUtil. subtitleUtil is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVideoRenderingStarted()V
    .locals 2

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v1, "setVideoRenderingStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1512
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsVideoRenderingStarted:Z

    .line 1513
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mUri:Landroid/net/Uri;

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->openVideo()V

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestLayout()V

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->invalidate()V

    .line 256
    return-void
.end method

.method public setWakeMode(Z)V
    .locals 3
    .param p1, "mode"    # Z

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWakeMode E :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    if-eqz p1, :cond_1

    .line 1175
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1181
    :cond_0
    :goto_0
    return-void

    .line 1178
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0
.end method

.method public setWebvttRendererVisibility(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1516
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleController:Landroid/media/SubtitleController;

    if-nez v0, :cond_0

    .line 1523
    :goto_0
    return-void

    .line 1518
    :cond_0
    if-eqz p1, :cond_1

    .line 1519
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleController:Landroid/media/SubtitleController;

    invoke-virtual {v0}, Landroid/media/SubtitleController;->show()V

    goto :goto_0

    .line 1521
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleController:Landroid/media/SubtitleController;

    invoke-virtual {v0}, Landroid/media/SubtitleController;->reset()V

    goto :goto_0
.end method

.method public start()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 702
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 703
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->unregisterRemoteControlReceiver()V

    .line 704
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    .line 706
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 707
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleExitLayout(I)V

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 711
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsCoverClosed:Z

    if-eqz v1, :cond_2

    .line 712
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "start() : Cover Closed!"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 716
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v1, :cond_3

    .line 717
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->registerMotionListener()V

    .line 719
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 720
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "start() before"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->startTimedText()V

    .line 723
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestAudioFocus()Z

    move-result v1

    if-nez v1, :cond_4

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v2, "start() : requestAudioFocus() is false"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 727
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a0067

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 728
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v1, :cond_0

    .line 729
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    goto :goto_0

    .line 734
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    if-nez v1, :cond_5

    .line 735
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 736
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleAudioOnlyView(I)V

    .line 739
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 742
    const/16 v1, 0x12d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->sendBTbroadcast(I)V

    .line 743
    const/16 v1, 0x12e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->sendBTbroadcast(I)V

    .line 746
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V

    .line 749
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    .line 751
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    if-eqz v1, :cond_6

    .line 752
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnStartListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnStartListener;->onStart()V

    .line 754
    :cond_6
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 755
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    .line 758
    :cond_7
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    goto/16 :goto_0
.end method

.method public startTimedText()V
    .locals 6

    .prologue
    .line 762
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    const-string v4, "startTimedText E"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v3, :cond_0

    .line 765
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getIsMultiSubtitle()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 766
    const/16 v3, 0x32

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;

    .line 767
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v4, 0x5de

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 771
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v3, :cond_1

    .line 772
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v2

    .line 774
    .local v2, "trackinfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getInbandTracksInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->updateInbandTrackinfo([Landroid/media/MediaPlayer$TrackInfo;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 783
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleActive()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 784
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSubtitleFile()Ljava/lang/String;

    move-result-object v1

    .line 785
    .local v1, "subtitleFile":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startTimedText. subtitleFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    if-eqz v1, :cond_2

    .line 789
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startTimedText. outbandsubtitle! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->addOutbandSubTitleinVideoClip(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    .end local v1    # "subtitleFile":Ljava/lang/String;
    .end local v2    # "trackinfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :cond_1
    :goto_1
    return-void

    .line 775
    .restart local v2    # "trackinfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 777
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 778
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 779
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 780
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 792
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v1    # "subtitleFile":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startTimedText. inbandsubtitle! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitle()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public stopPlayback()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_4

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->unregisterMotionListener()V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 268
    const/16 v0, 0x12d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->sendBTbroadcast(I)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V

    .line 276
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resetVideoRenderingStarted()V

    .line 278
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    if-nez v0, :cond_2

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->abandonAudioFocus()V

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAIAContext(Z)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 285
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z

    .line 286
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v0, :cond_3

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleExitLayout(I)V

    .line 291
    :cond_3
    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I

    .line 292
    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I

    .line 293
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestLayout()V

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->invalidate()V

    .line 297
    :cond_4
    return-void
.end method

.method public suspend()V
    .locals 1

    .prologue
    .line 956
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->release(Z)V

    .line 957
    return-void
.end method
