.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/service/IPlayer;
.implements Lcom/sec/android/app/videoplayer/service/ITouchPlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchPlayer"
.end annotation


# instance fields
.field private final mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field mCloludListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

.field mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mDuration:J

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mHandler:Landroid/os/Handler;

.field private final mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mIsAdaptSoundOn:Z

.field private mIsInitialized:Z

.field private mIsOutbandSubtitle:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private final mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

.field private final mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field mSKTcloudListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

.field private mSubtitleFile:Ljava/lang/String;

.field private final mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mWarnAdaptSound:Z

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2253
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2245
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 2247
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 2249
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 2251
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 2255
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 2257
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 2259
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$1;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloludListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    .line 2281
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTcloudListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    .line 2351
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mWarnAdaptSound:Z

    .line 2353
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsAdaptSoundOn:Z

    .line 3271
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$3;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 3286
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$4;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 3397
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 3521
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$6;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 3603
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$7;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 3633
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$8;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 3683
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$9;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    .line 2253
    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 2240
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 2240
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 2240
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->initTrackInfoUtil()V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 2240
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 2240
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    return v0
.end method

.method private ensureAdaptSound()V
    .locals 5

    .prologue
    .line 2342
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->FLAG_SUPPORT_ADAPT_SOUND:Z

    if-eqz v0, :cond_0

    .line 2343
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    new-instance v1, Lcom/sec/android/app/videoplayer/common/AdaptSound;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/videoplayer/common/AdaptSound;-><init>(Landroid/content/Context;II)V

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2502(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Lcom/sec/android/app/videoplayer/common/AdaptSound;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    .line 2346
    const-string v0, "MoviePlaybackService"

    const-string v1, "ensureAdaptSound() :: Adapt sound created"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2349
    :cond_0
    return-void
.end method

.method private getSoundAliveMode(I)I
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 2497
    return p1
.end method

.method private initTrackInfoUtil()V
    .locals 7

    .prologue
    .line 2693
    const-string v4, "MoviePlaybackService"

    const-string v5, "initTrackInfoUtil()"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2694
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetTrackInfo()V

    .line 2696
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v3

    .line 2699
    .local v3, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getInbandTracksInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->updateInbandTrackinfo([Landroid/media/MediaPlayer$TrackInfo;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2708
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v2

    .line 2710
    .local v2, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    if-eqz v4, :cond_1

    .line 2711
    const-string v4, "MoviePlaybackService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TouchPlayer :: initTrackInfoUtil() : mIsOutbandSubtitle: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2712
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;)V

    .line 2727
    :cond_0
    :goto_1
    return-void

    .line 2700
    .end local v2    # "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    :catch_0
    move-exception v0

    .line 2701
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2702
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2703
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2704
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 2705
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 2714
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v2    # "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 2716
    .local v1, "inbandTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 2717
    const-string v4, "MoviePlaybackService"

    const-string v5, "TouchPlayer :: initTrackInfoUtil() : inband subtitle "

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2719
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->getTrackType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 2720
    const/16 v4, 0x69

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFileType(I)V

    .line 2723
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initInbandSubtitle()V

    .line 2724
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    goto :goto_1
.end method

.method private isEnableAdaptSoundPath()Z
    .locals 2

    .prologue
    .line 2383
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 2384
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2385
    :cond_0
    const/4 v1, 0x1

    .line 2388
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setAIAContext(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 3903
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 3904
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAIAContext(Z)V

    .line 3906
    :cond_0
    return-void
.end method

.method private setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "mode"    # Z

    .prologue
    .line 2501
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnableSoundAlive E. mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2503
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 2505
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    new-instance v1, Landroid/media/audiofx/SoundAlive;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2702(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Landroid/media/audiofx/SoundAlive;)Landroid/media/audiofx/SoundAlive;

    .line 2509
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 2517
    :cond_1
    :goto_0
    return-void

    .line 2511
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 2513
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 2514
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2702(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Landroid/media/audiofx/SoundAlive;)Landroid/media/audiofx/SoundAlive;

    goto :goto_0
.end method

.method private updateAdaptSound(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2392
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mWarnAdaptSound:Z

    .line 2393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2410
    :goto_0
    return-void

    .line 2397
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsAdaptSoundOn:Z

    if-eqz v0, :cond_2

    .line 2398
    if-eqz p1, :cond_1

    .line 2399
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->activate(Z)V

    .line 2408
    :goto_1
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateAdaptSound mWarnAdaptSound "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mWarnAdaptSound:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Connected ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2402
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mWarnAdaptSound:Z

    .line 2403
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->activate(Z)V

    goto :goto_1

    .line 2406
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->activate(Z)V

    goto :goto_1
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;)V
    .locals 6
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 2638
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    if-nez v2, :cond_0

    .line 2639
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle. mIsOutbandSubtitle is false. return"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2690
    :goto_0
    return-void

    .line 2643
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2644
    :cond_1
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle. filepath is invalid. return"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2648
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2649
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle E"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2651
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v1

    .line 2652
    .local v1, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->clearOutbandTextTrack()V

    .line 2655
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->removeOutbandTimedTextSource()V

    .line 2656
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->removeOutbandSubtitleSource()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2665
    :goto_1
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addOutbandSubTitle() : filepath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2668
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFileType()I

    move-result v2

    const/16 v3, 0x69

    if-ne v2, v3, :cond_3

    .line 2669
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle. WebVTT type so we call another API"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2671
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v4, "text/vtt"

    const-string v5, "und"

    invoke-static {v4, v5}, Landroid/media/MediaFormat;->createSubtitleFormat(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaFormat;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->addSubtitleSource(Ljava/io/InputStream;Landroid/media/MediaFormat;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_6

    goto :goto_0

    .line 2677
    :catch_0
    move-exception v0

    .line 2678
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2657
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2658
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 2659
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 2660
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 2661
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 2662
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 2674
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-string v3, "application/x-subrip"

    invoke-virtual {v2, p1, v3}, Landroid/media/MediaPlayer;->addTimedTextSourceSEC(Ljava/lang/String;Ljava/lang/String;)V

    .line 2675
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getOutbandTimedTextTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->addOutbandTextTrackInfo([Landroid/media/MediaPlayer$TrackInfo;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_6

    goto/16 :goto_0

    .line 2679
    :catch_4
    move-exception v0

    .line 2680
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 2681
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v0

    .line 2682
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 2683
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 2684
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_0

    .line 2687
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .end local v1    # "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :cond_4
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle : MediaPlayer is NOT initialized!!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 2544
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: addOutbandSubTitle()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2546
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 2547
    iput-boolean p2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 2548
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;)V

    .line 2549
    return-void
.end method

.method public changePlayer(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 3910
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: changePlayer() - mode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3911
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 3912
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDlnaPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$4200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setPlayer(Lcom/sec/android/app/videoplayer/service/IPlayer;)V

    .line 3914
    :cond_0
    return-void
.end method

.method public deselectSubtitleTrack()V
    .locals 6

    .prologue
    .line 2817
    const-string v3, "MoviePlaybackService"

    const-string v4, "deselectSubtitleTrack()"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2819
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_2

    .line 2821
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    .line 2823
    .local v2, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 2824
    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 2827
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v1}, Landroid/media/MediaPlayer;->deselectTrack(I)V

    .line 2828
    const-string v3, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deselectSubtitleTrack : mMediaPlayer.selectTrack : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2823
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2831
    .end local v1    # "index":I
    .end local v2    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 2832
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v3, "MoviePlaybackService"

    const-string v4, "deselectSubtitleTrack : RuntimeException"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2835
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_2
    return-void
.end method

.method public duration()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 3699
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 3700
    const-string v0, "MoviePlaybackService"

    const-string v1, "duration. mIsInitialized is false. return!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3701
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 3702
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 3724
    :goto_0
    return-wide v0

    .line 3705
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3706
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 3708
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 3709
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: duration E. mDuration <= 0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3710
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x3ea

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 3711
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPauseEnable(Z)V

    .line 3722
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x8e

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 3724
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    goto :goto_0

    .line 3714
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPauseEnable(Z)V

    .line 3715
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3716
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x3e9

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    goto :goto_1

    .line 3718
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x3e8

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    goto :goto_1
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 3896
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 3897
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3899
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFPS()I
    .locals 4

    .prologue
    .line 3864
    const/4 v0, -0x1

    .line 3866
    .local v0, "fps":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 3867
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5e1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->getIntParameter(I)I

    move-result v0

    .line 3870
    :cond_0
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFPS() - fps:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3871
    return v0
.end method

.method public getFullDumpSubtitle(Ljava/lang/String;Z)[B
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 2561
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TouchPlayer :: getFullDumpSubtitle().filepath"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2563
    const/4 v4, 0x0

    .line 2564
    .local v4, "type":Ljava/lang/String;
    const-string v4, "application/x-subrip"

    .line 2566
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2567
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2568
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2571
    .local v3, "is":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    .line 2572
    .local v1, "fd":Ljava/io/FileDescriptor;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2573
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, v1, v4}, Landroid/media/MediaPlayer;->getFullDumpSubtitle(Ljava/io/FileDescriptor;Ljava/lang/String;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2578
    if-eqz v3, :cond_0

    .line 2579
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2586
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "is":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-object v5

    .line 2575
    .restart local v1    # "fd":Ljava/io/FileDescriptor;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "is":Ljava/io/FileInputStream;
    :cond_1
    :try_start_3
    const-string v5, "MoviePlaybackService"

    const-string v6, "MediaPlayer is NOT Initialised"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2578
    if-eqz v3, :cond_2

    .line 2579
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 2586
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "is":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    const/4 v5, 0x0

    goto :goto_0

    .line 2578
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "is":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_3

    .line 2579
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_3
    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2583
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "is":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 2584
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public initSelectSubtitleTrack()V
    .locals 11

    .prologue
    .line 2731
    const-string v9, "MoviePlaybackService"

    const-string v10, "initSelectSubtitleTrack()"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2733
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v6

    .line 2734
    .local v6, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v8

    .line 2736
    .local v8, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v8, :cond_0

    if-nez v6, :cond_1

    .line 2791
    :cond_0
    :goto_0
    return-void

    .line 2739
    :cond_1
    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v7

    .line 2741
    .local v7, "track":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    if-eqz v7, :cond_9

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v9

    if-lez v9, :cond_9

    .line 2742
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->deselectSubtitleTrack()V

    .line 2744
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v9

    if-eqz v9, :cond_3

    .line 2746
    const-string v9, "MoviePlaybackService"

    const-string v10, "initSelectSubtitleTrack. subtitle language is multiple"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2748
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v9

    array-length v9, v9

    if-ge v0, v9, :cond_2

    .line 2749
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v9

    aget v5, v9, v0

    .line 2750
    .local v5, "selectedIndex":I
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v1, v9, v5

    .line 2751
    .local v1, "idx":I
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    .line 2748
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2754
    .end local v1    # "idx":I
    .end local v5    # "selectedIndex":I
    :cond_2
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setTracksAndGetLangIndex()I

    goto :goto_0

    .line 2756
    .end local v0    # "i":I
    :cond_3
    const-string v9, "MoviePlaybackService"

    const-string v10, "initSelectSubtitleTrack. subtitle language is single"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setTracksAndGetLangIndex()I

    move-result v0

    .line 2760
    .restart local v0    # "i":I
    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->getTrackType()I

    move-result v9

    const/4 v10, 0x4

    if-ne v9, v10, :cond_8

    .line 2761
    sget-object v9, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    move-result-object v4

    .line 2763
    .local v4, "mCaptionMgr":Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->isCaptionEnable()Z

    move-result v9

    if-nez v9, :cond_4

    .line 2764
    const-string v9, "MoviePlaybackService"

    const-string v10, "selectSubtitle : CaptioningEnabled = false"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2765
    :cond_4
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleType()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_5

    .line 2766
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v9, v9, v0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    goto :goto_0

    .line 2768
    :cond_5
    const/4 v2, 0x0

    .line 2770
    .local v2, "inbandLanguage":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->getLocale()Ljava/util/Locale;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 2771
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->getLocale()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    .line 2774
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_0

    .line 2775
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2776
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v9, v9, v3

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    goto/16 :goto_0

    .line 2774
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2781
    .end local v3    # "j":I
    :cond_7
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v9, v9, v0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    goto/16 :goto_0

    .line 2785
    .end local v2    # "inbandLanguage":Ljava/lang/String;
    .end local v4    # "mCaptionMgr":Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    :cond_8
    iget-object v9, v7, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v9, v9, v0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    goto/16 :goto_0

    .line 2789
    .end local v0    # "i":I
    :cond_9
    const-string v9, "MoviePlaybackService"

    const-string v10, "initSelectSubtitleTrack : There is NO TIMED TEXT in track!!!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 2553
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: initSubtitle(). filepath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isOutbandType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2555
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 2556
    iput-boolean p2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 2557
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 3088
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 3245
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3876
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->HIGH_SPEED_PLAY:Z

    if-eqz v2, :cond_1

    .line 3877
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 3878
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3879
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v3, 0x5e2

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->getIntParameter(I)I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 3880
    const-string v1, "MoviePlaybackService"

    const-string v2, "isSECVideo() - true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3891
    :goto_0
    return v0

    .line 3884
    :cond_0
    const-string v0, "MoviePlaybackService"

    const-string v2, "isSECVideo() StreamingType - false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 3885
    goto :goto_0

    .line 3890
    :cond_1
    const-string v0, "MoviePlaybackService"

    const-string v2, "isSECVideo() - false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 3891
    goto :goto_0
.end method

.method public oneFrameSeek(I)V
    .locals 5
    .param p1, "plusOrMinus"    # I

    .prologue
    const/4 v4, 0x3

    .line 3734
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 3736
    .local v0, "currentPosition":I
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    if-eq p1, v4, :cond_0

    .line 3737
    const-string v1, "MoviePlaybackService"

    const-string v2, "oneFrameSeek Value invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3743
    :goto_0
    return-void

    .line 3741
    :cond_0
    const-string v2, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "oneFrameSeek : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-ne p1, v4, :cond_1

    const-string v1, "Forward"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " plusOrMinus="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3742
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, p1}, Landroid/media/MediaPlayer;->seekTo(II)V

    goto :goto_0

    .line 3741
    :cond_1
    const-string v1, "Backward"

    goto :goto_1
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 3184
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: pause()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3186
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-eqz v1, :cond_3

    .line 3187
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 3188
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3189
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: pause() isWiredHeadsetOn()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3190
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v2, 0x0

    iput v2, v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    .line 3191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget v1, v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 3194
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSmartPauseOn(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->unregisterMotionListener()V

    .line 3200
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3201
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer called mediaplayer.pause()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3202
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    .line 3203
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 3205
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAudioOnlyClip()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3206
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->notifyPlayer(I)V

    .line 3208
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v2, 0x12d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBTbroadcast(I)V

    .line 3214
    .end local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 3215
    return-void

    .line 3210
    .restart local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_4
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer isPlaying() == false, not called mediaplayer.pause()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public position()J
    .locals 2

    .prologue
    .line 3729
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public realSeek(II)J
    .locals 6
    .param p1, "whereto"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 3773
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3774
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getSeekModeForAllShare(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 3776
    .local v0, "ASFseekMode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3777
    const-string v1, "MoviePlaybackService"

    const-string v2, "seekTo. SeekMode is NONE return"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3778
    int-to-long v2, p1

    .line 3787
    .end local v0    # "ASFseekMode":Ljava/lang/String;
    :goto_0
    return-wide v2

    .line 3782
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 3783
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: seek() pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seekMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3784
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1, p2}, Landroid/media/MediaPlayer;->seekTo(II)V

    .line 3787
    :cond_1
    int-to-long v2, p1

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 3917
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 3918
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 3919
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 3920
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 3922
    :cond_0
    return-void
.end method

.method public releaseAdaptSound()V
    .locals 2

    .prologue
    .line 2413
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v0, :cond_1

    .line 2420
    :cond_0
    :goto_0
    return-void

    .line 2414
    :cond_1
    const-string v0, "MoviePlaybackService"

    const-string v1, "releaseAdaptSound()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->release()V

    .line 2418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2502(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Lcom/sec/android/app/videoplayer/common/AdaptSound;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 3219
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: reset()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 3226
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 3227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 3228
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 3229
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 3230
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 3233
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetTrackInfo()V

    .line 3235
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_1

    .line 3236
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->setOnUrlUpdatedSKTcloudListener(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;)V

    .line 3237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v0, :cond_2

    .line 3238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->setOnUrlUpdatedListener(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;)V

    .line 3240
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3241
    return-void
.end method

.method public resetPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3255
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer::resetPlayer() - start."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3257
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 3260
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 3261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 3262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 3263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 3265
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetTrackInfo()V

    .line 3267
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 3268
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 3269
    return-void
.end method

.method public resetSubtitle()V
    .locals 3

    .prologue
    .line 2591
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: resetSubtitle()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2592
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 2593
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 2595
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-nez v1, :cond_1

    .line 2596
    :cond_0
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: resetSubtitle() - mMediaplayer is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2610
    :goto_0
    return-void

    .line 2600
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->deselectSubtitleTrack()V

    .line 2603
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->removeOutbandTimedTextSource()V

    .line 2604
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->removeOutbandSubtitleSource()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2609
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetTrackInfo()V

    goto :goto_0

    .line 2605
    :catch_0
    move-exception v0

    .line 2606
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: resetSubtitle() - removeOutbandTimedTextSource : RuntimeException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public seek(J)J
    .locals 5
    .param p1, "whereto"    # J

    .prologue
    .line 3747
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3748
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getSeekModeForAllShare(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 3750
    .local v0, "seekMode":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3751
    const-string v1, "MoviePlaybackService"

    const-string v2, "seekTo. SeekMode is NONE return"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3767
    .end local v0    # "seekMode":Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide p1

    .line 3756
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 3757
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v1

    const/16 v2, 0x3ea

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3759
    const-string v1, "MoviePlaybackService"

    const-string v2, "seekTo. mPlayType is LIVE_PLAY, return"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3763
    :cond_2
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: seek() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3764
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    long-to-int v2, p1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public selectSubtitleTrack(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 2795
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 2797
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    .line 2799
    .local v1, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    array-length v2, v1

    if-ge p1, v2, :cond_2

    if-ltz p1, :cond_2

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 2804
    :cond_0
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectSubtitleTrack : mMediaPlayer.selectTrack : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2805
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p1}, Landroid/media/MediaPlayer;->selectTrack(I)V

    .line 2813
    .end local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :cond_1
    :goto_0
    return-void

    .line 2807
    .restart local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :cond_2
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectSubtitleTrack : Subtitle NOT Selected! : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2809
    .end local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 2810
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "MoviePlaybackService"

    const-string v3, "selectSubtitleTrack : RuntimeException"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAdaptSound(ZZ)V
    .locals 4
    .param p1, "isOn"    # Z
    .param p2, "showToast"    # Z

    .prologue
    .line 2357
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v1, :cond_1

    .line 2380
    :cond_0
    :goto_0
    return-void

    .line 2359
    :cond_1
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdaptSound() :: isOn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    if-eqz p1, :cond_2

    .line 2362
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->ensureAdaptSound()V

    .line 2365
    :cond_2
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsAdaptSoundOn:Z

    .line 2367
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2369
    if-nez p1, :cond_0

    .line 2370
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    goto :goto_0

    .line 2375
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->isEnableAdaptSoundPath()Z

    move-result v0

    .line 2376
    .local v0, "connected":Z
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->updateAdaptSound(Z)V

    .line 2379
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public setAudioTrack(I)V
    .locals 4
    .param p1, "mTrackNumber"    # I

    .prologue
    .line 3800
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v1

    const/16 v2, 0x3ea

    if-eq v1, v2, :cond_0

    .line 3802
    :try_start_0
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: mTrackNumber() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3803
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->selectTrack(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3808
    :cond_0
    :goto_0
    return-void

    .line 3804
    :catch_0
    move-exception v0

    .line 3805
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 22
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2935
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    .line 2936
    .local v15, "path":Ljava/lang/String;
    const-string v2, "MoviePlaybackService"

    const-string v4, "TouchPlayer :: setDataSource()"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2938
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 2939
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 2940
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    .line 2941
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 2942
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2945
    :cond_0
    const/4 v14, 0x0

    .line 2947
    .local v14, "modified_path":Ljava/lang/String;
    if-eqz v15, :cond_1

    const-string v2, "sshttp://"

    invoke-virtual {v15, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v13

    .line 2950
    .local v13, "mimeType":Ljava/lang/String;
    if-eqz v13, :cond_1

    const-string v2, "application/x-dtcp1"

    invoke-virtual {v13, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2951
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "??"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 2953
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getSeekModeForAllShare(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    .line 2955
    .local v16, "seekMode":Ljava/lang/String;
    if-eqz v16, :cond_6

    .line 2956
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&&"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 2964
    .end local v13    # "mimeType":Ljava/lang/String;
    .end local v16    # "seekMode":Ljava/lang/String;
    :cond_1
    :goto_0
    const-wide/16 v4, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 2965
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v4, 0x3e8

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 2967
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2970
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2971
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 2972
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2973
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2974
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 2975
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 2976
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 2978
    new-instance v8, Landroid/media/SubtitleController;

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getMediaTimeProvider()Landroid/media/MediaTimeProvider;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {v8, v2, v4, v5}, Landroid/media/SubtitleController;-><init>(Landroid/content/Context;Landroid/media/MediaTimeProvider;Landroid/media/SubtitleController$Listener;)V

    .line 2979
    .local v8, "controller":Landroid/media/SubtitleController;
    new-instance v2, Landroid/media/WebVttRenderer;

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/media/WebVttRenderer;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v2}, Landroid/media/SubtitleController;->registerRenderer(Landroid/media/SubtitleController$Renderer;)V

    .line 2980
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v2, :cond_2

    .line 2981
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2, v8, v4}, Landroid/media/MediaPlayer;->setSubtitleAnchor(Landroid/media/SubtitleController;Landroid/media/SubtitleController$Anchor;)V

    .line 2984
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2902(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 2986
    if-eqz v15, :cond_9

    const-string v2, "content://"

    invoke-virtual {v15, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2987
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v2, :cond_3

    .line 2988
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 2991
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2993
    :cond_5
    const-string v2, "MoviePlaybackService"

    const-string v4, "setDataSource : this is cloud contents"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_4

    .line 3084
    .end local v8    # "controller":Landroid/media/SubtitleController;
    :goto_1
    return-void

    .line 2958
    .restart local v13    # "mimeType":Ljava/lang/String;
    .restart local v16    # "seekMode":Ljava/lang/String;
    :cond_6
    const-string v2, "MoviePlaybackService"

    const-string v4, "DTCP seekMode is NULL"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2996
    .end local v13    # "mimeType":Ljava/lang/String;
    .end local v16    # "seekMode":Ljava/lang/String;
    .restart local v8    # "controller":Landroid/media/SubtitleController;
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-object/from16 v0, p1

    invoke-virtual {v2, v4, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 3021
    :cond_8
    :goto_2
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_JA_CHIP:Z

    if-nez v2, :cond_e

    .line 3026
    :goto_3
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v2, :cond_10

    .line 3027
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v17

    .line 3029
    .local v17, "sh":Landroid/view/SurfaceHolder;
    if-eqz v17, :cond_f

    .line 3030
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 3048
    .end local v17    # "sh":Landroid/view/SurfaceHolder;
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 3050
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 3051
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 3055
    :goto_5
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 3056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 3057
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    .line 3059
    .end local v8    # "controller":Landroid/media/SubtitleController;
    :catch_0
    move-exception v9

    .line 3060
    .local v9, "ex":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3061
    const-string v2, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException occured  9 :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3062
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v4, 0x2714

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto/16 :goto_1

    .line 2997
    .end local v9    # "ex":Ljava/io/IOException;
    .restart local v8    # "controller":Landroid/media/SubtitleController;
    :cond_9
    if-eqz v14, :cond_a

    .line 2998
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_2

    .line 3064
    .end local v8    # "controller":Landroid/media/SubtitleController;
    :catch_1
    move-exception v9

    .line 3065
    .local v9, "ex":Ljava/lang/IllegalArgumentException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3066
    const-string v2, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalArgumentException occured  10 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3068
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    const-string v5, "videoplayer.exit"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_1

    .line 2999
    .end local v9    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v8    # "controller":Landroid/media/SubtitleController;
    :cond_a
    :try_start_3
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHEVCPDType()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 3000
    const-string v2, "MoviePlaybackService"

    const-string v4, "setDataSource. play store hub HEVC MP4 PD!!!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3002
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3003
    .local v10, "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3004
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_4

    .line 3007
    .local v11, "is":Ljava/io/FileInputStream;
    :try_start_4
    invoke-virtual {v11}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    .line 3008
    .local v3, "fd":Ljava/io/FileDescriptor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-wide/16 v4, 0x0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getFileSize()J

    move-result-wide v6

    const-wide/high16 v20, -0x100000000000000L

    or-long v6, v6, v20

    invoke-virtual/range {v2 .. v7}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3010
    if-eqz v11, :cond_8

    .line 3011
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_2

    .line 3071
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v8    # "controller":Landroid/media/SubtitleController;
    .end local v10    # "file":Ljava/io/File;
    .end local v11    # "is":Ljava/io/FileInputStream;
    :catch_2
    move-exception v9

    .line 3072
    .local v9, "ex":Ljava/lang/IllegalStateException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3073
    const-string v2, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalStateException occured  11 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 3010
    .end local v9    # "ex":Ljava/lang/IllegalStateException;
    .restart local v8    # "controller":Landroid/media/SubtitleController;
    .restart local v10    # "file":Ljava/io/File;
    .restart local v11    # "is":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_b

    .line 3011
    :try_start_6
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_b
    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_4

    .line 3075
    .end local v8    # "controller":Landroid/media/SubtitleController;
    .end local v10    # "file":Ljava/io/File;
    .end local v11    # "is":Ljava/io/FileInputStream;
    :catch_3
    move-exception v9

    .line 3076
    .local v9, "ex":Ljava/lang/SecurityException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3077
    const-string v2, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SecurityException occured  12 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 3015
    .end local v9    # "ex":Ljava/lang/SecurityException;
    .restart local v8    # "controller":Landroid/media/SubtitleController;
    .restart local v10    # "file":Ljava/io/File;
    :cond_c
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v15}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_2

    .line 3079
    .end local v8    # "controller":Landroid/media/SubtitleController;
    .end local v10    # "file":Ljava/io/File;
    :catch_4
    move-exception v9

    .line 3080
    .local v9, "ex":Landroid/database/sqlite/SQLiteException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3081
    const-string v2, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SQLiteException occured  13 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 3018
    .end local v9    # "ex":Landroid/database/sqlite/SQLiteException;
    .restart local v8    # "controller":Landroid/media/SubtitleController;
    :cond_d
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v15}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3022
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->releaseCpuBooster()V

    .line 3023
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->releaseBusBooster()V

    goto/16 :goto_3

    .line 3032
    .restart local v17    # "sh":Landroid/view/SurfaceHolder;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V

    goto/16 :goto_1

    .line 3036
    .end local v17    # "sh":Landroid/view/SurfaceHolder;
    :cond_10
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v18

    .line 3037
    .local v18, "surfaceTexture":Landroid/graphics/SurfaceTexture;
    if-eqz v18, :cond_11

    .line 3038
    new-instance v12, Landroid/view/Surface;

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 3039
    .local v12, "mediaSurface":Landroid/view/Surface;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v12}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 3040
    invoke-virtual {v12}, Landroid/view/Surface;->release()V

    goto/16 :goto_4

    .line 3042
    .end local v12    # "mediaSurface":Landroid/view/Surface;
    :cond_11
    const-string v2, "MoviePlaybackService"

    const-string v4, "setDataSource : surfaceTexture is null"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3043
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V

    goto/16 :goto_1

    .line 3053
    .end local v18    # "surfaceTexture":Landroid/graphics/SurfaceTexture;
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_5
.end method

.method public setDataSourceAsync(Landroid/net/Uri;)V
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    .line 2849
    const-string v5, "MoviePlaybackService"

    const-string v6, "TouchPlayer :: setDataSourceAsync()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2851
    const-string v5, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "android.resource"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2852
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v6, 0x3e8

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 2857
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v5, :cond_1

    .line 2858
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 2859
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->reset()V

    .line 2860
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->release()V

    .line 2861
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2865
    :cond_1
    const-wide/16 v6, 0x0

    :try_start_0
    iput-wide v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 2867
    new-instance v5, Landroid/media/MediaPlayer;

    invoke-direct {v5}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2868
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2869
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 2870
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2871
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2872
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 2873
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 2874
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 2875
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v5, v6, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2877
    new-instance v0, Landroid/media/SubtitleController;

    sget-object v5, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/media/MediaPlayer;->getMediaTimeProvider()Landroid/media/MediaTimeProvider;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {v0, v5, v6, v7}, Landroid/media/SubtitleController;-><init>(Landroid/content/Context;Landroid/media/MediaTimeProvider;Landroid/media/SubtitleController$Listener;)V

    .line 2878
    .local v0, "controller":Landroid/media/SubtitleController;
    new-instance v5, Landroid/media/WebVttRenderer;

    sget-object v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/media/WebVttRenderer;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/media/SubtitleController;->registerRenderer(Landroid/media/SubtitleController$Renderer;)V

    .line 2879
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v5, :cond_2

    .line 2880
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v5, v0, v6}, Landroid/media/MediaPlayer;->setSubtitleAnchor(Landroid/media/SubtitleController;Landroid/media/SubtitleController$Anchor;)V

    .line 2883
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2902(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 2885
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v5, :cond_5

    .line 2886
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    .line 2888
    .local v3, "sh":Landroid/view/SurfaceHolder;
    if-eqz v3, :cond_4

    .line 2889
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, v3}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 2907
    .end local v3    # "sh":Landroid/view/SurfaceHolder;
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2909
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2910
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 2914
    :goto_2
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 2915
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 2916
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2931
    .end local v0    # "controller":Landroid/media/SubtitleController;
    :goto_3
    return-void

    .line 2854
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v6, 0x3e9

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    goto/16 :goto_0

    .line 2891
    .restart local v0    # "controller":Landroid/media/SubtitleController;
    .restart local v3    # "sh":Landroid/view/SurfaceHolder;
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_3

    .line 2917
    .end local v0    # "controller":Landroid/media/SubtitleController;
    .end local v3    # "sh":Landroid/view/SurfaceHolder;
    :catch_0
    move-exception v1

    .line 2918
    .local v1, "ex":Ljava/io/IOException;
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 2919
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException occured  7 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2920
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v6, 0x2714

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto :goto_3

    .line 2895
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v0    # "controller":Landroid/media/SubtitleController;
    :cond_5
    :try_start_2
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v4

    .line 2896
    .local v4, "surfaceTexture":Landroid/graphics/SurfaceTexture;
    if-eqz v4, :cond_6

    .line 2897
    new-instance v2, Landroid/view/Surface;

    invoke-direct {v2, v4}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 2898
    .local v2, "mediaSurface":Landroid/view/Surface;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, v2}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 2899
    invoke-virtual {v2}, Landroid/view/Surface;->release()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 2922
    .end local v0    # "controller":Landroid/media/SubtitleController;
    .end local v2    # "mediaSurface":Landroid/view/Surface;
    .end local v4    # "surfaceTexture":Landroid/graphics/SurfaceTexture;
    :catch_1
    move-exception v1

    .line 2923
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 2924
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IllegalArgumentException occured  8 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2901
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "controller":Landroid/media/SubtitleController;
    .restart local v4    # "surfaceTexture":Landroid/graphics/SurfaceTexture;
    :cond_6
    :try_start_3
    const-string v5, "MoviePlaybackService"

    const-string v6, "setDataSource : surfaceTexture is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2902
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 2926
    .end local v0    # "controller":Landroid/media/SubtitleController;
    .end local v4    # "surfaceTexture":Landroid/graphics/SurfaceTexture;
    :catch_2
    move-exception v1

    .line 2927
    .local v1, "ex":Ljava/lang/IllegalStateException;
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 2928
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IllegalStateException occured : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2912
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    .restart local v0    # "controller":Landroid/media/SubtitleController;
    :cond_7
    :try_start_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2
.end method

.method public setDataSourcePrepare(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2303
    if-nez p1, :cond_0

    .line 2339
    :goto_0
    return-void

    .line 2306
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_1

    .line 2307
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 2310
    :cond_1
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: setDataSourcePrepare() : start."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2312
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x82

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 2314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 2316
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v0, :cond_2

    .line 2317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 2320
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "android.resource"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2321
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSourceAsync(Landroid/net/Uri;)V

    goto :goto_0

    .line 2322
    :cond_4
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2323
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2324
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getCloudCachedPath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSource(Landroid/net/Uri;)V

    goto :goto_0

    .line 2326
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTcloudListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->setOnUrlUpdatedSKTcloudListener(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;)V

    .line 2327
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getUrlByUri(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2329
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2330
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2331
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getCloudCachedPath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSource(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2333
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloludListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->setOnUrlUpdatedListener(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;)V

    .line 2334
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getUrlByUri(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2337
    :cond_8
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSource(Landroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 3250
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mHandler:Landroid/os/Handler;

    .line 3251
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 5

    .prologue
    .line 2614
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setInbandSubtitle()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2616
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_0

    .line 2617
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setInbandSubtitle() - mMediaplayer is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2634
    :goto_0
    return-void

    .line 2621
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 2622
    .local v1, "track":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    .line 2624
    .local v0, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    if-lez v2, :cond_1

    if-eqz v0, :cond_1

    .line 2625
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TouchPlayer :: setInbandSubtitle() track.size() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2626
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initInbandSubtitle()V

    .line 2628
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 2629
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 2630
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    goto :goto_0

    .line 2632
    :cond_1
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setInbandSubtitle() - track or subtitleutil is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPlaySpeed(F)V
    .locals 6
    .param p1, "speed"    # F

    .prologue
    .line 2521
    const-string v3, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TouchPlayer :: setPlaySpeed (speed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is called"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2522
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    .line 2524
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    .line 2525
    .local v1, "playSpeed":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2526
    .local v2, "playSpeed_Reply":Landroid/os/Parcel;
    const/16 v3, 0x400

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2527
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2529
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v1, v2}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2540
    .end local v1    # "playSpeed":Landroid/os/Parcel;
    .end local v2    # "playSpeed_Reply":Landroid/os/Parcel;
    :cond_0
    :goto_0
    return-void

    .line 2530
    :catch_0
    move-exception v0

    .line 2531
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2532
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2533
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 2534
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_2
    move-exception v0

    .line 2535
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2536
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 2537
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSoundAliveMode()V
    .locals 3

    .prologue
    .line 2424
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getSAEffectMode()I

    move-result v1

    .line 2426
    .local v1, "mode":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 2428
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isExtraSpeakerDockOn()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2429
    :cond_0
    const/16 v1, 0xa

    .line 2436
    :cond_1
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setSoundAliveMode(I)V

    .line 2437
    return-void

    .line 2430
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-nez v2, :cond_1

    .line 2431
    const/16 v2, 0xb

    if-le v1, v2, :cond_1

    .line 2432
    const/16 v1, 0xa

    goto :goto_0
.end method

.method public setSoundAliveMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 2441
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: setSoundAliveMode (mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is called"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2442
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->getSoundAliveMode(I)I

    move-result v2

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSoundAliveMode:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2602(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 2446
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V

    .line 2448
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2449
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSoundAliveMode:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Landroid/media/audiofx/SoundAlive;->usePreset(S)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2471
    :cond_0
    :goto_0
    return-void

    .line 2462
    :catch_0
    move-exception v0

    .line 2463
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2464
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2465
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2466
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 2467
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 2468
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_3
    move-exception v0

    .line 2469
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSubtitleSyncTime(I)V
    .locals 4
    .param p1, "synctime"    # I

    .prologue
    .line 2839
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: setSubtitleSyncTime() - synctime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2841
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 2842
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5dd

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setParameter(II)Z

    move-result v0

    .line 2843
    .local v0, "result":Z
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: setSubtitleSyncTime setParameter : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2845
    .end local v0    # "result":Z
    :cond_0
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 11
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 3815
    const-string v6, "android.media.MediaPlayer"

    .line 3816
    .local v6, "sClassName":Ljava/lang/String;
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setWindowCrop l:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " t:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " r:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " b:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3818
    if-ltz p1, :cond_1

    if-ltz p2, :cond_1

    if-lez p3, :cond_1

    if-lez p4, :cond_1

    .line 3821
    :try_start_0
    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 3825
    .local v2, "mediaplayerClass":Ljava/lang/Class;
    const-string v7, "invoke"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    invoke-virtual {v2, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 3827
    .local v1, "invoke_method":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 3829
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    .line 3832
    .local v5, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3833
    .local v4, "reply":Landroid/os/Parcel;
    const-string v7, "android.media.IMediaPlayer"

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3836
    const/16 v7, 0x3039

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 3837
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setWindowCrop(adj) l:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " t:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " r:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " b:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3839
    invoke-virtual {v5, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 3840
    invoke-virtual {v5, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3841
    invoke-virtual {v5, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3842
    invoke-virtual {v5, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 3845
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-virtual {v1, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 3846
    .local v3, "obj":Ljava/lang/Object;
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invoke Result : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3860
    .end local v1    # "invoke_method":Ljava/lang/reflect/Method;
    .end local v2    # "mediaplayerClass":Ljava/lang/Class;
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "reply":Landroid/os/Parcel;
    .end local v5    # "request":Landroid/os/Parcel;
    :cond_0
    :goto_0
    return-void

    .line 3850
    :catch_0
    move-exception v0

    .line 3852
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 3853
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 3855
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3858
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v7, "MoviePlaybackService"

    const-string v8, "SetWindowCrop ----- IGNORING INVALID PARAM --------"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVolume(F)V
    .locals 3
    .param p1, "vol"    # F

    .prologue
    .line 3792
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 3793
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: setVolume() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3794
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 3796
    :cond_0
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3093
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: start() : start!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3095
    const/4 v1, 0x0

    .line 3097
    .local v1, "movieVolume":F
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-eqz v2, :cond_1

    .line 3099
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 3100
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getImplicitVideoVolume()F

    move-result v1

    .line 3101
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3102
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 3106
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->startSubtitle()V

    .line 3108
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    .line 3110
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->registerMotionListener()V

    .line 3113
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    .line 3116
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setSoundAliveMode()V

    .line 3118
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z

    .line 3119
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 3120
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v3, 0x12d

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBTbroadcast(I)V

    .line 3121
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v3, 0x12e

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBTbroadcast(I)V

    .line 3122
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 3128
    .end local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_1
    return-void

    .line 3104
    .restart local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    goto :goto_0
.end method

.method public startSubtitle()V
    .locals 4

    .prologue
    .line 3131
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: startSubtitle E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3132
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    .line 3134
    .local v0, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3135
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: startSubtitle() : MultiSubtitle"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3136
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5de

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 3139
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    .line 3140
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setSubtitleSyncTime(I)V

    .line 3141
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 3145
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3147
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->unregisterMotionListener()V

    .line 3153
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 3155
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 3156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 3159
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V

    .line 3162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 3163
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 3164
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->notifyPlayer(I)V

    .line 3165
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 3166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v1, 0x12d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBTbroadcast(I)V

    .line 3167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 3170
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 3174
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->releaseAdaptSound()V

    .line 3177
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 3178
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 3179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2902(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 3180
    return-void
.end method
