.class public Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;
.super Ljava/lang/Object;
.source "VideoMakerCmd.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = ".activity.ProjectEditActivity"

.field public static final FULL_VERSION_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.vefull"

.field private static final MimeType:Ljava/lang/String; = "video/mp4"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.ve"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;->TAG:Ljava/lang/String;

    const-string v6, "execute"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    const-string v6, "com.sec.android.app.videoplayer"

    const-string v7, "VEDT"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    if-nez p1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "filePath":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 41
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;->TAG:Ljava/lang/String;

    const-string v6, "execute. filePath is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v4, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-lt v5, v6, :cond_0

    .line 50
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.EDIT"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "video/mp4"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v3, "com.sec.android.app.ve"

    .line 56
    .local v3, "packageName":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR_FULL:Z

    if-eqz v5, :cond_3

    .line 57
    const-string v3, "com.sec.android.app.vefull"

    .line 61
    :cond_3
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 64
    :try_start_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "excp":Landroid/content/ActivityNotFoundException;
    sget-object v5, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;->TAG:Ljava/lang/String;

    const-string v6, "ActivityNotFoundException"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
