.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
.super Ljava/lang/Object;
.source "SubtitleUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;
    }
.end annotation


# static fields
.field private static final CLEAR_SUBTITLE:I = 0x1

.field public static final MAX_MULTI_SUBTITLE:I = 0x32

.field private static final TAG:Ljava/lang/String;

.field private static final baseSubtitleDir:Ljava/lang/String;

.field private static final baseSubtitleFile:Ljava/lang/String; = "SubTitleFile"

.field private static mSubtitleLanguageIndex:I

.field private static sSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

.field public static sub:[B


# instance fields
.field private final mCaptioningObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mCountSubtitleOn:I

.field private mCustomMode:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerTemp:Landroid/os/Handler;

.field private mHasSubtitleFile:Z

.field private mIsMultiSubtitle:Z

.field public mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

.field private mPrevSubtitleViewVisibility:I

.field private mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

.field private mRemoteSubtitleFileExist:Z

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mSelectedSubtitleIndex:[I

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSideSyncSubtitleExist:Z

.field private mSideSyncSubtitleFilePath:Ljava/lang/String;

.field private mSubtitle:Ljava/lang/String;

.field private mSubtitleExt:[Ljava/lang/String;

.field private mSubtitleFilePath:Ljava/lang/String;

.field private mSubtitleFileType:I

.field private mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

.field private mSubtitlePath:Ljava/lang/String;

.field private mSubtitleTempText:[Ljava/lang/String;

.field private mSubtitleText:Ljava/lang/String;

.field private mSubtitleType:I

.field private mSubtitleUrl:Ljava/lang/String;

.field private mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

.field private mSyncTime:I

.field private mWatchNowSubtitleExist:Z

.field private mWatchNowSubtitleFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 81
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->SUBTITLE_PATH:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->baseSubtitleDir:Ljava/lang/String;

    .line 111
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->sub:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    .line 52
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    .line 54
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 56
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 60
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 62
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleType:I

    .line 64
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSyncTime:I

    .line 66
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 68
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    .line 75
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 77
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 79
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 85
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    .line 87
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 89
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 91
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    .line 93
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ".smi"

    aput-object v1, v0, v4

    const-string v1, ".srt"

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, ".sub"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ".xml"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ".vtt"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    .line 96
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleExist:Z

    .line 98
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    .line 101
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleExist:Z

    .line 103
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleFilePath:Ljava/lang/String;

    .line 109
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    .line 882
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandler:Landroid/os/Handler;

    .line 1200
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    .line 1201
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    .line 1275
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandlerTemp:Landroid/os/Handler;

    .line 1277
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandlerTemp:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCaptioningObserver:Landroid/database/ContentObserver;

    .line 113
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;)Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    return-object v0
.end method

.method private checkSubtitleFileType(Ljava/lang/String;)V
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x64

    .line 750
    if-nez p1, :cond_0

    .line 751
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 770
    :goto_0
    return-void

    .line 755
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 757
    .local v0, "fileName":Ljava/lang/String;
    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 758
    const/16 v1, 0x68

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_0

    .line 759
    :cond_1
    const-string v1, ".smi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 760
    const/16 v1, 0x65

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_0

    .line 761
    :cond_2
    const-string v1, ".srt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 762
    const/16 v1, 0x66

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_0

    .line 763
    :cond_3
    const-string v1, ".sub"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 764
    const/16 v1, 0x67

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_0

    .line 765
    :cond_4
    const-string v1, ".vtt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 766
    const/16 v1, 0x69

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_0

    .line 768
    :cond_5
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_0
.end method

.method private existFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "subTitleFileName"    # Ljava/lang/String;

    .prologue
    .line 836
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 837
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->sSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->sSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 120
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->sSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    return-object v0
.end method

.method private initSubtitleInternal()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 718
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "initSubtitleInternal"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isSubtitleFilePathExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 721
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "initSubtitleInternal() - mSubttitleFile NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const/4 v0, 0x0

    .line 731
    :goto_0
    return v0

    .line 725
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->initSubtitle(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private registerObserver(Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1264
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "registerObserver E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1265
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCaptioningObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1266
    return-void
.end method

.method private removeHandlerMsg(I)V
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 894
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 895
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 898
    :cond_0
    return-void
.end method


# virtual methods
.method public checkExistOutbandSubtitle(Ljava/lang/String;)Z
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 504
    if-nez p1, :cond_0

    .line 505
    sget-object v9, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v10, "checkExistOutbandSubtitle() - path is null"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    .line 568
    :goto_0
    return v0

    .line 509
    :cond_0
    sget-object v9, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkExistOutbandSubtitle(). filePath : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const/4 v0, 0x0

    .line 514
    .local v0, "bIsOutBandSubtitle":Z
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    .line 515
    .local v5, "subtitleFileName":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 516
    .local v2, "smiFileName":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 517
    .local v3, "srtFileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4}, Ljava/lang/String;-><init>()V

    .line 518
    .local v4, "subFileName":Ljava/lang/String;
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7}, Ljava/lang/String;-><init>()V

    .line 519
    .local v7, "xmlFileName":Ljava/lang/String;
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 521
    .local v6, "vttFileName":Ljava/lang/String;
    const/16 v9, 0x2e

    invoke-virtual {p1, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 523
    .local v1, "pos":I
    if-lez v1, :cond_1

    .line 524
    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 527
    :cond_1
    const-string v9, ".smi"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 528
    const-string v9, ".srt"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 529
    const-string v9, ".sub"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 530
    const-string v9, ".xml"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 531
    const-string v9, ".vtt"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 533
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 534
    const/4 v0, 0x1

    .line 535
    const/16 v9, 0x65

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 536
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 558
    :goto_1
    if-eqz v0, :cond_7

    .line 559
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 564
    :goto_2
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkExistOutbandSubtitle(). mSubtitleFilePath : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mSubtitleFileType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/videoplayer/subtitle/SubtitleConst;->SUBTITLE_FILE_TYPE:[Ljava/lang/String;

    iget v11, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    add-int/lit8 v11, v11, -0x64

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mHasSubtitleFile : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 537
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 538
    const/4 v0, 0x1

    .line 539
    const/16 v9, 0x66

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 540
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    goto :goto_1

    .line 541
    :cond_3
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 542
    const/4 v0, 0x1

    .line 543
    const/16 v9, 0x67

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 544
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    goto :goto_1

    .line 545
    :cond_4
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 546
    const/4 v0, 0x1

    .line 547
    const/16 v9, 0x68

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 548
    iput-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    goto :goto_1

    .line 549
    :cond_5
    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 550
    const/4 v0, 0x1

    .line 551
    const/16 v9, 0x69

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 552
    iput-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    goto :goto_1

    .line 554
    :cond_6
    const/4 v0, 0x0

    .line 555
    const/16 v9, 0x64

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    goto :goto_1

    .line 561
    :cond_7
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    goto :goto_2
.end method

.method public checkExistSideSyncSubtitle()Z
    .locals 1

    .prologue
    .line 1119
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleExist:Z

    return v0
.end method

.method public checkExistWatchNowSubtitle()Z
    .locals 1

    .prologue
    .line 1100
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleExist:Z

    return v0
.end method

.method public checkRemoteSubtitleFile(Z)V
    .locals 0
    .param p1, "check"    # Z

    .prologue
    .line 917
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 918
    return-void
.end method

.method public checkSubtitleValidty(Ljava/lang/String;)Z
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 925
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 926
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 927
    const/4 v1, 0x1

    .line 930
    :goto_1
    return v1

    .line 925
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 930
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public clearRemoteSubtitle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1005
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 1006
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 1007
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 1008
    return-void
.end method

.method public clearSubtitle()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 877
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "clearSubtitle E : "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->removeHandlerMsg(I)V

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 880
    return-void
.end method

.method public deleteRemoteSubTitleFile()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 986
    new-instance v3, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->baseSubtitleDir:Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 988
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 989
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 991
    .local v2, "childFileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 992
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 993
    .local v1, "childFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 992
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 997
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "childFile":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1000
    .end local v2    # "childFileList":[Ljava/io/File;
    :cond_1
    iput-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 1001
    iput-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 1002
    return-void
.end method

.method public deleteSubtitleFile()V
    .locals 2

    .prologue
    .line 801
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    if-eqz v1, :cond_1

    .line 802
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isSubtitleFilePathExist()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 803
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 804
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 805
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 809
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 810
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 812
    :cond_1
    return-void
.end method

.method public downloadRemoteSubTitleFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "subtitleUrl"    # Ljava/lang/String;

    .prologue
    .line 934
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 938
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->baseSubtitleDir:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 939
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 940
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 943
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 944
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 945
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->baseSubtitleDir:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SubTitleFile"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 943
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 949
    :cond_2
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    .line 952
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;->start()V

    .line 954
    return-void
.end method

.method public downloadRemoteSubTitleFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "subtitleUrl"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 957
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 961
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->baseSubtitleDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 962
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 963
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 966
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->baseSubtitleDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SubTitleFile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 968
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    .line 969
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;->start()V

    .line 971
    return-void
.end method

.method public getCaptionWinColor()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinColor()I

    move-result v0

    return v0
.end method

.method public getCaptionWinOpacity()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinOpacity()I

    move-result v0

    return v0
.end method

.method public getFontBGColor()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGColor()I

    move-result v0

    return v0
.end method

.method public getFontBGOpacity()I
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGOpacity()I

    move-result v0

    return v0
.end method

.method public getFontColor()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontColor()I

    move-result v0

    return v0
.end method

.method public getFontEdge()I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontEdge()I

    move-result v0

    return v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFontOpacity()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontOpacity()I

    move-result v0

    return v0
.end method

.method public getFontPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontSize()I

    move-result v0

    return v0
.end method

.method public getFullDumpSubtitle()[B
    .locals 1

    .prologue
    .line 735
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->sub:[B

    return-object v0
.end method

.method public getHasSubtitleFile()Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    return v0
.end method

.method public getIsMultiSubtitle()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    return v0
.end method

.method public getMultiSelectSubtitle()I
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    return v0
.end method

.method public getRemoteSubtitlePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 912
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRemoteSubtitlePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteSubtitleUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 907
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRemoteSubtitleUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSMPTETTCustomMode()Z
    .locals 2

    .prologue
    .line 269
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_0

    .line 270
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSelectedSubtitleIndex()[I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    return-object v0
.end method

.method public getSelectedTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public getSideSyncSubtitleFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getSplitSubtitleActive()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSplitSubtitleActivation()Z

    move-result v0

    return v0
.end method

.method public getSubtitleActive()Z
    .locals 2

    .prologue
    .line 261
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->isCaptionEnable()Z

    move-result v0

    .line 264
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSubtitleActivation()Z

    move-result v0

    goto :goto_0
.end method

.method public getSubtitleCount()I
    .locals 4

    .prologue
    .line 243
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v3, "getSubtitleCount()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v0

    .line 246
    .local v0, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 248
    .local v1, "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :goto_0
    if-nez v1, :cond_1

    .line 249
    const/4 v2, 0x0

    .line 252
    :goto_1
    return v2

    .line 246
    .end local v1    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 252
    .restart local v1    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    goto :goto_1
.end method

.method public getSubtitleFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 222
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getSubtitleFileType()I
    .locals 3

    .prologue
    .line 217
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleFileType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    return v0
.end method

.method public getSubtitleLanguage(I)Ljava/lang/String;
    .locals 8
    .param p1, "index"    # I

    .prologue
    const v7, 0x7f0a0145

    .line 455
    const-string v1, ""

    .line 456
    .local v1, "retValue":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v3

    .line 457
    .local v3, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v4

    .line 459
    .local v4, "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :goto_0
    if-nez v4, :cond_1

    move-object v5, v1

    .line 500
    :goto_1
    return-object v5

    .line 457
    .end local v4    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 461
    .restart local v4    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_1
    const/4 v5, -0x1

    if-ge v5, p1, :cond_2

    iget-object v5, v4, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, p1, :cond_2

    .line 462
    iget-object v5, v4, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "retValue":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 464
    .restart local v1    # "retValue":Ljava/lang/String;
    :cond_2
    const-string v5, "eng"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 465
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01ba

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 466
    :cond_3
    const-string v5, "kor"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 467
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01bf

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 468
    :cond_4
    const-string v5, "chi"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 469
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01b8

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 470
    :cond_5
    const-string v5, "jpn"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 471
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01be

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 472
    :cond_6
    const-string v5, "fre"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 473
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01bc

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 474
    :cond_7
    const-string v5, "ger"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 475
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01b9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 476
    :cond_8
    const-string v5, "spa"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 477
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01bb

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 478
    :cond_9
    const-string v5, "ita"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 479
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0a01bd

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 482
    :cond_a
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v2

    .line 484
    .local v2, "temp":[Ljava/util/Locale;
    if-eqz v2, :cond_c

    array-length v5, v2

    if-lez v5, :cond_c

    .line 485
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v5, v2

    if-ge v0, v5, :cond_c

    .line 486
    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 487
    sget-object v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getISO3Language() "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v0

    invoke-virtual {v7}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " retValue : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 490
    :cond_b
    sget-object v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v6, "Not Founud!!!"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 494
    .end local v0    # "i":I
    :cond_c
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_d

    .line 495
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    move-object v5, v1

    .line 500
    goto/16 :goto_1

    .line 497
    :cond_d
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method public getSubtitleLanguageIndex()I
    .locals 3

    .prologue
    .line 256
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleLanguageIndex(). mSubtitleLanguageIndex : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    sget v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    return v0
.end method

.method public getSubtitleType()I
    .locals 3

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleType:I

    return v0
.end method

.method public getSyncTime()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSyncTime:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTextAlignment()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTextAlignment()I

    move-result v0

    return v0
.end method

.method public getTitleFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 691
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "getTitleFileName()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const/4 v0, 0x0

    .line 695
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 696
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSideSync()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 697
    const/4 v1, 0x0

    .line 704
    :goto_0
    return-object v1

    .line 700
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    .line 703
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTitleFileName(). path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 704
    goto :goto_0
.end method

.method public getWatchNowSubtitleFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public initInbandSubtitle()V
    .locals 2

    .prologue
    .line 572
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "initInbandSubtitle"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 574
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleType:I

    .line 575
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 576
    return-void
.end method

.method public initSubtitle()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 599
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "initSubtitle"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    .line 603
    .local v0, "CurPlayingPath":Ljava/lang/String;
    const-string v6, "sshttp://"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromDms()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 605
    :cond_0
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "initSubtitle - remote device has subtitle. should download file"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    const-string v3, ""

    .line 607
    .local v3, "pathEx":Ljava/lang/String;
    const/4 v5, 0x0

    .line 608
    .local v5, "subtitleUrl":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/allshare/extension/SECVideoCaption;

    invoke-direct {v1}, Lcom/samsung/android/allshare/extension/SECVideoCaption;-><init>()V

    .line 610
    .local v1, "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    const-string v6, "sshttp://"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 611
    const-string v6, "ss"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 614
    :cond_1
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/extension/SECVideoCaption;->getSubTitleURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 616
    if-eqz v5, :cond_5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkSubtitleValidty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 617
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 618
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 619
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->downloadRemoteSubTitleFile(Ljava/lang/String;)V

    .line 620
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "initSubtitle. remote subtitle is exist : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    .end local v1    # "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    .end local v3    # "pathEx":Ljava/lang/String;
    .end local v5    # "subtitleUrl":Ljava/lang/String;
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleSettings()V

    .line 651
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getRemoteSubtitleUrl()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 652
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->remoteSubTitledownloadThreadCheck()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 653
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "DownloadCompelet!!!!"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getRemoteSubtitlePath()Ljava/lang/String;

    move-result-object v2

    .line 657
    .local v2, "mRemoteSubtitlePath":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mRemoteSubtitlePath  :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 660
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleInternal()Z

    .line 661
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    .line 681
    .end local v2    # "mRemoteSubtitlePath":Ljava/lang/String;
    :cond_4
    :goto_1
    return-void

    .line 622
    .restart local v1    # "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    .restart local v3    # "pathEx":Ljava/lang/String;
    .restart local v5    # "subtitleUrl":Ljava/lang/String;
    :cond_5
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 623
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 624
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "initSubtitle. remote subtitle is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 626
    .end local v1    # "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    .end local v3    # "pathEx":Ljava/lang/String;
    .end local v5    # "subtitleUrl":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getAllShareIntentSubtitle()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 627
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "initSubtitle - remote device has subtitle. should download file"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getAllShareIntentSubtitle()Ljava/lang/String;

    move-result-object v4

    .line 630
    .local v4, "subtitleURL":Ljava/lang/String;
    if-eqz v4, :cond_2

    const-string v6, "http://"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 631
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 632
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 633
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->downloadRemoteSubTitleFile(Ljava/lang/String;)V

    .line 634
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Subtitle is exist : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 636
    .end local v4    # "subtitleURL":Ljava/lang/String;
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 637
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSideSyncSubtitleFilePath()Ljava/lang/String;

    move-result-object v4

    .line 639
    .restart local v4    # "subtitleURL":Ljava/lang/String;
    if-eqz v4, :cond_2

    const-string v6, "http://"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 640
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 641
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 642
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->downloadRemoteSubTitleFile(Ljava/lang/String;)V

    .line 643
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Subtitle is exist : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 645
    .end local v4    # "subtitleURL":Ljava/lang/String;
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 646
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v6, p0, v7}, Lcom/sec/android/app/videoplayer/slink/SLink;->downloadSubtitleFile(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 662
    :cond_9
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getAllShareIntentSubtitle()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 663
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "initSubtitle: AllshareIntent subtitle"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getAllShareIntentSubtitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkExistOutbandSubtitle(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 665
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleInternal()Z

    .line 666
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    goto/16 :goto_1

    .line 669
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkExistWatchNowSubtitle()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 670
    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v7, "initSubtitle: WatchNow subtitle"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getWatchNowSubtitleFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 672
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleInternal()Z

    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    goto/16 :goto_1

    .line 674
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getTitleFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkExistOutbandSubtitle(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 675
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleInternal()Z

    .line 676
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    goto/16 :goto_1

    .line 678
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->stopSubtitle()V

    goto/16 :goto_1
.end method

.method public initSubtitleSetting()V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "initSubtitleSetting E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->initValues()V

    .line 147
    return-void
.end method

.method public isCheckNullSubtitleManager()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemoteSubtitleFile()Z
    .locals 1

    .prologue
    .line 921
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    return v0
.end method

.method public isSMPTETTFileType()Z
    .locals 2

    .prologue
    .line 791
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubtitleFilePathExist()Z
    .locals 1

    .prologue
    .line 787
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubtitleViewVisible()Z
    .locals 1

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWebVTTFileType()Z
    .locals 4

    .prologue
    .line 795
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v2, 0x69

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 796
    .local v0, "ret":Z
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isWebVTTFileType. getSubtitleActive() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    return v0

    .line 795
    .end local v0    # "ret":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTimedTextListener(Landroid/media/TimedText;)V
    .locals 11
    .param p1, "subtitleText"    # Landroid/media/TimedText;

    .prologue
    .line 1204
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v9, "onTimedTextListener E."

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v7

    .line 1208
    .local v7, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz p1, :cond_6

    .line 1210
    :try_start_0
    invoke-virtual {p1}, Landroid/media/TimedText;->getHead()Ljava/lang/String;

    move-result-object v1

    .line 1211
    .local v1, "headString":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/media/TimedText;->getText()Ljava/lang/String;

    move-result-object v6

    .line 1212
    .local v6, "textString":Ljava/lang/String;
    const/16 v8, 0x11

    invoke-virtual {p1, v8}, Landroid/media/TimedText;->getObject(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1214
    .local v5, "textIndex":I
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onTimedTextListener(). getHead: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onTimedTextListener(). textIndex : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " text = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    if-nez v6, :cond_0

    .line 1218
    const-string v6, ""

    .line 1221
    :cond_0
    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    if-eqz v8, :cond_5

    .line 1222
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    if-nez v8, :cond_1

    .line 1223
    const/16 v8, 0x32

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    .line 1226
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    const-string v9, ""

    aput-object v9, v8, v5

    .line 1227
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    aget-object v10, v10, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    .line 1228
    const-string v8, ""

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    .line 1230
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v8, v8

    if-ge v2, v8, :cond_4

    .line 1231
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    aget v4, v8, v2

    .line 1232
    .local v4, "selectedIndex":I
    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v3, v8, v4

    .line 1233
    .local v3, "idx":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    aget-object v8, v8, v3

    if-nez v8, :cond_3

    const-string v8, ""

    :goto_1
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    .line 1235
    iget v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    add-int/lit8 v8, v8, -0x1

    if-eq v2, v8, :cond_2

    .line 1236
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    .line 1230
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1233
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleTempText:[Ljava/lang/String;

    aget-object v8, v8, v3

    goto :goto_1

    .line 1240
    .end local v3    # "idx":I
    .end local v4    # "selectedIndex":I
    :cond_4
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onTimedTextListener() mSubtitleText = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleText:Ljava/lang/String;

    invoke-virtual {p0, v8, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    .end local v1    # "headString":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v5    # "textIndex":I
    .end local v6    # "textString":Ljava/lang/String;
    :goto_2
    return-void

    .line 1243
    .restart local v1    # "headString":Ljava/lang/String;
    .restart local v5    # "textIndex":I
    .restart local v6    # "textString":Ljava/lang/String;
    :cond_5
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onTimedTextListener() text = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1246
    .end local v1    # "headString":Ljava/lang/String;
    .end local v5    # "textIndex":I
    .end local v6    # "textString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1247
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v9, "IllegalArgumentException"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1250
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->clearSubtitle()V

    goto :goto_2
.end method

.method public remoteSubTitledownloadThreadCheck()Z
    .locals 4

    .prologue
    .line 975
    :try_start_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "remoteSubTitledownloadThreadCheck"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$RemoteSubTitledownloadThread;->join(J)V

    .line 977
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "RemoteSubTitledownloadThread join"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 982
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 978
    :catch_0
    move-exception v0

    .line 979
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 980
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetSubtitleActivation()V
    .locals 1

    .prologue
    .line 709
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->USA:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v0

    if-nez v0, :cond_1

    .line 710
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSamsungSubtitleActivation(Z)V

    .line 714
    :goto_0
    return-void

    .line 712
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSamsungSubtitleActivation(Z)V

    goto :goto_0
.end method

.method public resetSubtitleLang()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1070
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "resetSubtitleLang()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    .line 1072
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 1073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 1074
    sput v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 1075
    return-void
.end method

.method public resetSubtitleSettings()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 739
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 740
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 741
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSyncTime:I

    .line 742
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    .line 743
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleLang()V

    .line 744
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleType:I

    .line 745
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetSubtitle()V

    .line 747
    return-void
.end method

.method public resetSubtitleview()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleView(Landroid/content/Context;)V

    .line 164
    :cond_0
    return-void
.end method

.method public restartSubtitle()V
    .locals 2

    .prologue
    .line 684
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "restartSubtitle E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleInternal()Z

    .line 686
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    .line 687
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 688
    return-void
.end method

.method public saveSubtitleActivation(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v0, 0x1

    .line 290
    if-ne p1, v0, :cond_0

    .line 291
    .local v0, "active":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveSubtitleActivation() : enabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v2, 0x69

    if-ne v1, v2, :cond_1

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_captioning_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 298
    :goto_1
    return-void

    .line 290
    .end local v0    # "active":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 296
    .restart local v0    # "active":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSamsungSubtitleActivation(Z)V

    goto :goto_1
.end method

.method public saveSubtitleSetting()V
    .locals 2

    .prologue
    .line 150
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "saveSubtitleSetting E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->saveSubtitleSetting()V

    .line 152
    return-void
.end method

.method public selectOutbandSubtitle()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 579
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "selectOutbandSubtitle"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isSubtitleFilePathExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 582
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "selectOutbandSubtitle() - mSubttitleFile NULL"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const/4 v0, 0x0

    .line 595
    :goto_0
    return v0

    .line 586
    :cond_0
    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleType:I

    .line 587
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->addOutbandSubTitle(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setCaptionWinColor(I)V
    .locals 1
    .param p1, "setColor"    # I

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCaptionWinColor(I)V

    .line 365
    return-void
.end method

.method public setCaptionWinOpacity(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCaptionWinOpacity(I)V

    .line 381
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "setInfo E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    .line 126
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 127
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 130
    const-string v0, "accessibility_captioning_enabled"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->registerObserver(Ljava/lang/String;)V

    .line 131
    const-string v0, "accessibility_sec_captioning_enabled"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->registerObserver(Ljava/lang/String;)V

    .line 134
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCtx(Landroid/content/Context;)V

    .line 136
    return-void
.end method

.method public setFontBGColor(I)V
    .locals 1
    .param p1, "setColor"    # I

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontBGColor(I)V

    .line 369
    return-void
.end method

.method public setFontBGOpacity(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontBGOpacity(I)V

    .line 377
    return-void
.end method

.method public setFontColor(I)V
    .locals 1
    .param p1, "setColor"    # I

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontColor(I)V

    .line 361
    return-void
.end method

.method public setFontEdge(I)V
    .locals 1
    .param p1, "setEdge"    # I

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontEdge(I)V

    .line 353
    return-void
.end method

.method public setFontNameForPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 434
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontNameForPreference() :: fontName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFont(Ljava/lang/String;)V

    .line 436
    return-void
.end method

.method public setFontOpacity(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontOpacity(I)V

    .line 373
    return-void
.end method

.method public setFontPackageNameForPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 444
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontPackageNameForPreference() :: packageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontPackageName(Ljava/lang/String;)V

    .line 446
    return-void
.end method

.method public setFontSize()V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontSize(I)V

    .line 389
    return-void
.end method

.method public setFontSize(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontSize(I)V

    .line 385
    return-void
.end method

.method public setFontStringNameForPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "fontStringName"    # Ljava/lang/String;

    .prologue
    .line 439
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontStringNameForPreference() :: fontStringName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontStringName(Ljava/lang/String;)V

    .line 441
    return-void
.end method

.method public setIsMultiSubtitle(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 411
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 412
    return-void
.end method

.method public setMWFontSize(II)V
    .locals 1
    .param p1, "mwWidth"    # I
    .param p2, "mwHeight"    # I

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateMWFontSize(II)V

    .line 395
    :cond_0
    return-void
.end method

.method public setMultiSelectSubtitle(I)V
    .locals 0
    .param p1, "CountSubtitleOn"    # I

    .prologue
    .line 430
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    .line 431
    return-void
.end method

.method public setMultiSelectedSubtitleIndex([I)V
    .locals 4
    .param p1, "multiSubtitleIndex"    # [I

    .prologue
    .line 415
    if-nez p1, :cond_1

    .line 416
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "setMultiSelectSubtitle multiSubtitleIndex is null return"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_0
    return-void

    .line 420
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 422
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 423
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 424
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMultiSelectSubtitle mSelectedSubtitleIndex["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setOnSubtitleInfoListener(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    .prologue
    .line 1260
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    .line 1261
    return-void
.end method

.method public setRemoteSubtitleUrl(Ljava/lang/String;)V
    .locals 3
    .param p1, "SubtitleUrl"    # Ljava/lang/String;

    .prologue
    .line 902
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRemoteSubtitleUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 904
    return-void
.end method

.method public setSMPTETTCustomMode(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    .line 315
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    if-ne v1, p1, :cond_0

    .line 341
    :goto_0
    return-void

    .line 318
    :cond_0
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v2, 0x68

    if-eq v1, v2, :cond_1

    .line 319
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    .line 322
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    .line 325
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    if-eqz v1, :cond_3

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    .line 327
    const/4 v0, 0x0

    .line 329
    .local v0, "visibility":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v1, :cond_2

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateCaptionWinColorOpacity(I)V

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontSize()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setFontSize(I)V

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontEdge()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontEdge(I)V

    .line 340
    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    goto :goto_0

    .line 337
    .end local v0    # "visibility":I
    :cond_3
    const/4 v0, 0x4

    .restart local v0    # "visibility":I
    goto :goto_1
.end method

.method public setSamsungSubtitleActivation(Z)V
    .locals 4
    .param p1, "active"    # Z

    .prologue
    .line 301
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSamsungSubtitleActivation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v0, 0x4

    .line 304
    .local v0, "visibility":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setActivation(Z)V

    .line 306
    if-eqz p1, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    .line 308
    const/4 v0, 0x0

    .line 311
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    .line 312
    return-void
.end method

.method public setSelectSubtitleLang([Z)V
    .locals 8
    .param p1, "langArray"    # [Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1145
    if-nez p1, :cond_1

    .line 1184
    :cond_0
    return-void

    .line 1149
    :cond_1
    iput v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    .line 1150
    array-length v0, p1

    .line 1151
    .local v0, "Len":I
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSelectSubtitleLang(). Len : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_4

    .line 1154
    aget-boolean v3, p1, v1

    if-ne v3, v6, :cond_2

    .line 1155
    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    .line 1158
    :cond_2
    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    if-le v3, v6, :cond_3

    .line 1159
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 1153
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1161
    :cond_3
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mIsMultiSubtitle:Z

    goto :goto_1

    .line 1165
    :cond_4
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSelectSubtitleLang(). arraySize = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 1168
    const/4 v2, 0x0

    .line 1169
    .local v2, "k":I
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_6

    .line 1170
    aget-boolean v3, p1, v1

    if-ne v3, v6, :cond_5

    .line 1171
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSelectedSubtitleIndex(II)V

    .line 1172
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSubtitleUtil.setSavedSelectedSubtitleIndex["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    add-int/lit8 v2, v2, 0x1

    .line 1169
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1177
    :cond_6
    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCountSubtitleOn:I

    if-ne v3, v6, :cond_0

    .line 1178
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_0

    .line 1179
    aget-boolean v3, p1, v1

    if-ne v3, v6, :cond_7

    .line 1180
    sput v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 1178
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public setSelectedSubtitleIndex(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "value"    # I

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSelectedSubtitleIndex:[I

    aput p2, v0, p1

    .line 452
    :cond_0
    return-void
.end method

.method public setSideSyncSubtitleFilePath(Ljava/lang/String;)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleExist:Z

    .line 1106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleFilePath:Ljava/lang/String;

    .line 1108
    if-eqz p1, :cond_0

    .line 1109
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleFilePath:Ljava/lang/String;

    .line 1110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSideSyncSubtitleExist:Z

    .line 1112
    :cond_0
    return-void
.end method

.method public setSplitSubtitleActive(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setSplitSubtitleActivation(Z)V

    .line 345
    return-void
.end method

.method public setSubtitleFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 773
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 776
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 777
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 778
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkSubtitleFileType(Ljava/lang/String;)V

    .line 784
    :goto_0
    return-void

    .line 780
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 781
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 782
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkSubtitleFileType(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSubtitleFileType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 348
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    .line 349
    return-void
.end method

.method public setSubtitleLanguageIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 402
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleLanguageIndex E. index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    sput p1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 404
    return-void
.end method

.method public setSubtitleSyncTime()V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSyncTime:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setSubtitleSyncTime(I)V

    .line 408
    return-void
.end method

.method public setSubtitleView(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V
    .locals 0
    .param p1, "subtitleView"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 155
    if-eqz p1, :cond_0

    .line 156
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 158
    :cond_0
    return-void
.end method

.method public setSubtitleViewVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_2

    .line 1128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setVisibility(I)V

    .line 1130
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    if-eq v0, p1, :cond_0

    .line 1131
    if-nez p1, :cond_1

    .line 1132
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    .line 1137
    :cond_0
    :goto_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 1141
    :goto_1
    return-void

    .line 1134
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->SUBTITLE_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V

    goto :goto_0

    .line 1139
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "setSubtitleViewVisibility : mSubtitleView is NULL!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setSyncTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 398
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSyncTime:I

    .line 399
    return-void
.end method

.method public setTextAlignment(I)V
    .locals 1
    .param p1, "alignment"    # I

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setTextAlignment(I)V

    .line 357
    return-void
.end method

.method public setTracksAndGetLangIndex()I
    .locals 4

    .prologue
    .line 1187
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v3, "setTracksAndGetLangIndex() E"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1189
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v0

    .line 1190
    .local v0, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 1192
    .local v1, "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    sget v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 1193
    :cond_0
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 1194
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v3, "setTracksAndGetLangIndex(). selected index is out of range so reset mSubtitleLanguageIndex value!!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    :cond_1
    sget v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleLanguageIndex:I

    return v2

    .line 1190
    .end local v1    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWatchNowSubtitleFilePath(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1079
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleExist:Z

    .line 1080
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    .line 1082
    if-eqz p1, :cond_0

    .line 1083
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1084
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1085
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    .line 1086
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleExist:Z

    .line 1087
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWatchNowSubtitleFilePath() : mWatchNowSubtitleFilePath = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWatchNowSubtitleFilePath() : mWatchNowSubtitleExist = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mWatchNowSubtitleExist:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    return-void

    .line 1089
    .restart local v0    # "file":Ljava/io/File;
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v2, "setWatchNowSubtitleFilePath() : subtitle does not exist"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startSubtitle()V
    .locals 3

    .prologue
    .line 815
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startSubtitle E mHasSubtitleFile = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 817
    return-void
.end method

.method public stopSubtitle()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 820
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "stopSubtitle E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mHasSubtitleFile:Z

    .line 822
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 823
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSyncTime:I

    .line 824
    sput-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->sub:[B

    .line 825
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleLang()V

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_0

    .line 828
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    const-string v1, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->showSubtitleText(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 833
    return-void
.end method

.method public unRegisterObserver()V
    .locals 2

    .prologue
    .line 1269
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    const-string v1, "unRegisterObserver E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCaptioningObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCaptioningObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1273
    :cond_0
    return-void
.end method

.method public updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "headString"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    .line 841
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSubtitle E text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->removeHandlerMsg(I)V

    .line 844
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-nez v0, :cond_0

    .line 870
    :goto_0
    return-void

    .line 847
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPress()Z

    move-result v0

    if-nez v0, :cond_1

    .line 848
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    goto :goto_0

    .line 852
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSubtitleActivation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 853
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    .line 859
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleFileType:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_4

    .line 860
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mCustomMode:Z

    if-eqz v0, :cond_3

    .line 861
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getHTMLString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    .line 869
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->showSubtitleText(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 855
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleViewVisibility(I)V

    goto :goto_0

    .line 863
    :cond_3
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    goto :goto_1

    .line 866
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getHTMLString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    goto :goto_1
.end method
