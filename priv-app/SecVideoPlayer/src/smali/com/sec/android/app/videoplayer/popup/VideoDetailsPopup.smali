.class public Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;
.super Ljava/lang/Object;
.source "VideoDetailsPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoDetailsPopup"


# instance fields
.field private COLON:Ljava/lang/String;

.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mContext:Landroid/content/Context;

.field private mDetailsDialog:Landroid/app/AlertDialog;

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDrm:Z

.field private mIsWmDrm:Z

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mListType:I

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mShareEnable:Z

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mVideoUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    .line 52
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 54
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 56
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    .line 58
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsDrm:Z

    .line 60
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mShareEnable:Z

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    .line 64
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 66
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mListType:I

    .line 68
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsWmDrm:Z

    .line 70
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    .line 74
    const-string v1, " : "

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    .line 176
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 185
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    .line 79
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 82
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    .line 86
    .local v0, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    .line 87
    const-string v1, "VideoDetailsPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoDetailsPopup. URI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/db/AllShareProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const/16 v1, 0xb

    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mListType:I

    .line 98
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsDrm:Z

    .line 99
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkIsShare()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mShareEnable:Z

    .line 100
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkIsWMDRM()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsWmDrm:Z

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->initDetails()V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getFileSize(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 496
    const-wide/16 v0, 0x0

    .line 498
    .local v0, "fileSize":J
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 499
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 507
    :goto_0
    return-object v2

    .line 502
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 503
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->getFileSize()J

    move-result-wide v0

    .line 507
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 505
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getSize(Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_1
.end method

.method private initDetails()V
    .locals 7

    .prologue
    const/16 v6, 0x400

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v5, "VideoDetailsPopup"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeVideoInfo()V

    .line 109
    const/4 v0, 0x0

    .line 111
    .local v0, "adapter":Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
    const-string v4, "persist.sys.language"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "iw"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "persist.sys.language"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ur"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "persist.sys.language"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 114
    :cond_0
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;

    .end local v0    # "adapter":Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v4, v5}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 119
    .restart local v0    # "adapter":Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080063

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 120
    .local v1, "listPadding":I
    new-instance v2, Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 121
    .local v2, "listView":Landroid/widget/ListView;
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 123
    const/high16 v4, 0x2000000

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 124
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 126
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 127
    .local v3, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00ad

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 128
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 129
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00dd

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    .line 136
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->requestWindowFeature(I)Z

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 142
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    iput-object v5, v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 145
    return-void

    .line 116
    .end local v1    # "listPadding":I
    .end local v2    # "listView":Landroid/widget/ListView;
    .end local v3    # "popup":Landroid/app/AlertDialog$Builder;
    :cond_1
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;

    .end local v0    # "adapter":Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v4, v5}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;Landroid/content/Context;Ljava/util/ArrayList;)V

    .restart local v0    # "adapter":Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
    goto/16 :goto_0
.end method

.method private makeAudioChannel()V
    .locals 12

    .prologue
    .line 316
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 317
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v2, 0x0

    .line 320
    .local v2, "extracted":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 321
    const/16 v8, 0x24

    invoke-virtual {v4, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 327
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 330
    :goto_0
    if-eqz v2, :cond_0

    .line 331
    const/4 v0, 0x1

    .line 333
    .local v0, "ch":I
    const-string v8, ":"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 335
    .local v5, "splited":[Ljava/lang/String;
    array-length v8, v5

    const/4 v9, 0x1

    if-ge v8, v9, :cond_1

    .line 366
    .end local v0    # "ch":I
    .end local v5    # "splited":[Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 322
    :catch_0
    move-exception v1

    .line 323
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 324
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v1

    .line 325
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 327
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v8

    .line 338
    .restart local v0    # "ch":I
    .restart local v5    # "splited":[Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v8, v5

    if-ge v3, v8, :cond_3

    .line 339
    aget-object v8, v5, v3

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v0, :cond_2

    .line 340
    aget-object v8, v5, v3

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 338
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 344
    :cond_3
    packed-switch v0, :pswitch_data_0

    .line 358
    :pswitch_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 362
    .local v7, "toShow":Ljava/lang/String;
    :goto_3
    move-object v6, v7

    .line 364
    .local v6, "strAudioChannel":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v11, 0x7f0a004b

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 346
    .end local v6    # "strAudioChannel":Ljava/lang/String;
    .end local v7    # "toShow":Ljava/lang/String;
    :pswitch_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a00bc

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 347
    .restart local v7    # "toShow":Ljava/lang/String;
    goto :goto_3

    .line 349
    .end local v7    # "toShow":Ljava/lang/String;
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0133

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 350
    .restart local v7    # "toShow":Ljava/lang/String;
    goto :goto_3

    .line 352
    .end local v7    # "toShow":Ljava/lang/String;
    :pswitch_3
    const-string v7, "5.1ch"

    .line 353
    .restart local v7    # "toShow":Ljava/lang/String;
    goto :goto_3

    .line 355
    .end local v7    # "toShow":Ljava/lang/String;
    :pswitch_4
    const-string v7, "7.1ch"

    .line 356
    .restart local v7    # "toShow":Ljava/lang/String;
    goto :goto_3

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private makeDRM()V
    .locals 15

    .prologue
    .line 399
    const/4 v1, 0x0

    .line 400
    .local v1, "drmInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;>;"
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-nez v9, :cond_1

    .line 401
    const-string v9, "VideoDetailsPopup"

    const-string v10, "makeDRM - mDRmUtill is null"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    sget-object v10, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDetailInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 406
    const/4 v5, 0x0

    .line 407
    .local v5, "roCount":Ljava/lang/String;
    const/4 v0, 0x0

    .line 408
    .local v0, "availableUse":Ljava/lang/String;
    const/4 v7, 0x0

    .line 409
    .local v7, "typeStr":Ljava/lang/String;
    const/4 v8, 0x0

    .line 411
    .local v8, "validity":Ljava/lang/String;
    const-string v9, "VideoDetailsPopup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "drmInfo = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-boolean v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsDrm:Z

    if-eqz v9, :cond_0

    .line 414
    if-eqz v1, :cond_0

    .line 415
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 416
    .local v4, "permissionCount":I
    const/4 v6, 0x0

    .line 419
    .local v6, "strDRM":Ljava/lang/String;
    if-lez v4, :cond_7

    .line 420
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v10, 0x7f0a0097

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 422
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_8

    .line 423
    const-string v9, "(%d/%d)"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    add-int/lit8 v12, v2, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 424
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;

    .line 426
    .local v3, "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    iget v9, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_2

    .line 427
    const-string v9, "%s : %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v13, 0x7f0a01a0

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    iget v13, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 431
    :cond_2
    iget-object v9, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    const/4 v10, -0x1

    if-eq v9, v10, :cond_3

    .line 432
    iget-object v9, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    .line 433
    const-string v9, "%s-%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    iget-object v13, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v14, 0x0

    aget v13, v13, v14

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    iget-object v13, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v14, 0x1

    aget v13, v13, v14

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 441
    :cond_3
    :goto_2
    iget-object v9, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    if-eqz v9, :cond_4

    .line 442
    iget-boolean v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsWmDrm:Z

    if-eqz v9, :cond_6

    .line 443
    const-string v9, "VideoDetailsPopup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mIsWmDrm true...validity:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget-object v8, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 453
    :cond_4
    :goto_3
    const-string v9, "VideoDetailsPopup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "validity:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 438
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    iget-object v10, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 448
    :cond_6
    const-string v9, "%s : %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v13, 0x7f0a01a1

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 457
    .end local v2    # "i":I
    .end local v3    # "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v10, 0x7f0a00d2

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 460
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v12, 0x7f0a005f

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    if-eqz v5, :cond_9

    .line 463
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0106

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    :cond_9
    if-eqz v0, :cond_a

    .line 469
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00e5

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    :cond_a
    if-eqz v7, :cond_b

    .line 473
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v12, 0x7f0a003e

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    :cond_b
    if-eqz v8, :cond_0

    .line 477
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v12, 0x7f0a003d

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private makeDate()V
    .locals 11

    .prologue
    .line 369
    const-string v2, ""

    .line 371
    .local v2, "dateString":Ljava/lang/String;
    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mListType:I

    const/16 v8, 0xb

    if-ne v7, v8, :cond_0

    .line 372
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    const-string v9, "date_modified"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 393
    :goto_0
    move-object v6, v2

    .line 395
    .local v6, "strDate":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v10, 0x7f0a004c

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    return-void

    .line 374
    .end local v6    # "strDate":Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "date_format"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 376
    .local v3, "settingFormat":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 377
    const-string v3, "dd/MM/yyyy"

    .line 380
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDateTaken(Landroid/net/Uri;)J

    move-result-wide v4

    .line 383
    .local v4, "dateTaken":J
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 384
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDate()J

    move-result-wide v4

    .line 387
    :cond_2
    new-instance v0, Ljava/util/Date;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v4

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 389
    .local v0, "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 390
    .local v1, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private makeFileName()V
    .locals 5

    .prologue
    .line 253
    const/4 v0, 0x0

    .line 255
    .local v0, "strFileName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoName()Ljava/lang/String;

    move-result-object v0

    .line 261
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0050

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    return-void

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private makeFormat()V
    .locals 5

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 288
    .local v0, "strFormat":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getType()Ljava/lang/String;

    move-result-object v0

    .line 293
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a004e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    return-void

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private makeForwarding()V
    .locals 6

    .prologue
    .line 298
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromDms()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 301
    :cond_1
    const-string v2, "VideoDetailsPopup"

    const-string v3, "makeForwarding. current file is SLink or Nearby device or Cloud"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :goto_0
    return-void

    .line 305
    :cond_2
    const v1, 0x7f0a00fa

    .line 307
    .local v1, "stringId":I
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mShareEnable:Z

    if-nez v2, :cond_3

    .line 308
    const v1, 0x7f0a0090

    .line 311
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "strForwarding":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a004f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private makeLocation()V
    .locals 5

    .prologue
    .line 484
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 486
    .local v0, "strLocation":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a00e4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    return-void
.end method

.method private makeResolution()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResolution(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 272
    .local v2, "dimensionsString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 273
    new-instance v2, Ljava/lang/String;

    .end local v2    # "dimensionsString":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 276
    .restart local v2    # "dimensionsString":Ljava/lang/String;
    :cond_0
    if-eqz v2, :cond_1

    const-string v3, ""

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 277
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%d"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "x"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 278
    .local v1, "VideoWidth":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%d"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "x"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "VideoHeight":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0051

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    .end local v0    # "VideoHeight":Ljava/lang/String;
    .end local v1    # "VideoWidth":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private makeSize()V
    .locals 5

    .prologue
    .line 265
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->getFileSize(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "strSize":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0052

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    return-void
.end method

.method private makeStoredAt()V
    .locals 5

    .prologue
    .line 490
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 492
    .local v0, "strStoredAt":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0137

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    return-void
.end method


# virtual methods
.method public checkContentChanged(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mVideoUri:Landroid/net/Uri;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 174
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    .line 225
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeVideoInfo()V
    .locals 3

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeFileName()V

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeFormat()V

    .line 230
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeResolution()V

    .line 231
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeSize()V

    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeForwarding()V

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeAudioChannel()V

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeDate()V

    .line 237
    const-string v0, "VideoDetailsPopup"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeVideoInfo. is DRM?: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsDrm:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mIsDrm:Z

    if-eqz v0, :cond_0

    .line 240
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeDRM()V

    .line 243
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mListType:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-nez v0, :cond_2

    .line 244
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeLocation()V

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->makeStoredAt()V

    .line 250
    :cond_3
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 152
    const-string v0, "VideoDetailsPopup"

    const-string v1, "show()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    iput-object v1, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoDetailsPopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->mDetailsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 162
    :goto_0
    return-void

    .line 159
    :cond_0
    const-string v0, "VideoDetailsPopup"

    const-string v1, "mDetailsDialog is null!! recreate it!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->initDetails()V

    goto :goto_0
.end method
