.class Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 2167
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    .line 2169
    const-string v1, "VideoServiceUtil"

    const-string v2, "mBufferingChecker Run!"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2171
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2198
    :goto_0
    return-void

    .line 2173
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 2175
    .local v0, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2176
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getBufferPercentage()I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_1

    .line 2177
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2181
    :cond_1
    if-eqz v0, :cond_2

    .line 2182
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 2183
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2184
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showStateView()V

    .line 2188
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2190
    :cond_3
    if-eqz v0, :cond_4

    .line 2191
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 2192
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2193
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showStateView()V

    .line 2196
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
