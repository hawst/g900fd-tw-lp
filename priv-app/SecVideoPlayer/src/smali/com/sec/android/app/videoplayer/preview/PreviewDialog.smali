.class public Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
.super Landroid/app/Dialog;
.source "PreviewDialog.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/preview/ChapterManager$Const;
.implements Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;
    }
.end annotation


# static fields
.field private static final ADJUST_LANDSCAPE_INDEX:I = 0x3

.field private static final ADJUST_PORTRAIT_INDEX:I = 0x2


# instance fields
.field private PreviewMargin:I

.field private final TAG:Ljava/lang/String;

.field private mBitmap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mChapterFrameTime:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mChapterResumePos:I

.field private mContext:Landroid/content/Context;

.field private mCurrentIdx:I

.field private mHandler:Landroid/os/Handler;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mPreview:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/preview/Preview;",
            ">;"
        }
    .end annotation
.end field

.field mPreviewTask:Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;

.field private mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

.field private mScrollView:Landroid/widget/HorizontalScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 43
    const-class v0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    .line 378
    new-instance v0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;

    .line 423
    new-instance v0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$4;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08008c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->PreviewMargin:I

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->setWindow()V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->arrangeChapterView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->getChapterTimeView(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/WeakHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/WeakHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method private arrangeChapterView()V
    .locals 6

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->findChapterIdx()V

    .line 315
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    .line 317
    const/4 v0, 0x0

    .line 318
    .local v0, "moveIdx":I
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_1

    .line 319
    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    add-int/lit8 v0, v3, -0x3

    .line 324
    :goto_0
    if-lez v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 325
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/preview/Preview;->getPreviewLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    .line 326
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 327
    .local v1, "parent":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    .line 329
    .end local v1    # "parent":Landroid/widget/LinearLayout;
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void

    .line 321
    :cond_1
    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    add-int/lit8 v0, v3, -0x2

    goto :goto_0
.end method

.method private clear()V
    .locals 4

    .prologue
    .line 361
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 362
    .local v1, "bitmaps":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/graphics/Bitmap;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 364
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/graphics/Bitmap;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 365
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 366
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 371
    .end local v0    # "b":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->clear()V

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->clear()V

    .line 373
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 375
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->clear()V

    .line 376
    return-void
.end method

.method private findChapterIdx()V
    .locals 5

    .prologue
    .line 281
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findChapterIdx. resumePos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v0, -0x1

    .line 285
    .local v0, "curIdx":I
    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    if-lez v2, :cond_0

    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    if-lez v2, :cond_0

    .line 286
    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 289
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 290
    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 291
    add-int/lit8 v0, v0, -0x1

    .line 294
    :cond_1
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    if-lt v0, v2, :cond_4

    .line 295
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    add-int/lit8 v0, v2, -0x1

    .line 300
    :cond_2
    :goto_0
    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCurrentIdx: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    if-le v2, v3, :cond_3

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/preview/Preview;->getPreviewLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    .line 306
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f0201ef

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 308
    .end local v1    # "v":Landroid/view/View;
    :cond_3
    return-void

    .line 296
    :cond_4
    if-gez v0, :cond_2

    .line 297
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getChapterTimeView(I)Ljava/lang/String;
    .locals 10
    .param p1, "time"    # I

    .prologue
    const/4 v6, 0x0

    .line 343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 344
    .local v1, "formatBuilder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v2, v1, v7}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 346
    .local v2, "formatter":Ljava/util/Formatter;
    rem-int/lit8 v5, p1, 0x3c

    .line 347
    .local v5, "sec":I
    const/16 v7, 0x3c

    if-lt p1, v7, :cond_0

    div-int/lit8 v7, p1, 0x3c

    rem-int/lit8 v4, v7, 0x3c

    .line 348
    .local v4, "min":I
    :goto_0
    const/16 v7, 0xe10

    if-lt p1, v7, :cond_1

    div-int/lit16 v3, p1, 0xe10

    .line 350
    .local v3, "hrs":I
    :goto_1
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 351
    const-string v7, "%02d:%02d:%02d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    const/4 v6, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    const/4 v6, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v2, v7, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "converTimeToString":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    .line 354
    return-object v0

    .end local v0    # "converTimeToString":Ljava/lang/String;
    .end local v3    # "hrs":I
    .end local v4    # "min":I
    :cond_0
    move v4, v6

    .line 347
    goto :goto_0

    .restart local v4    # "min":I
    :cond_1
    move v3, v6

    .line 348
    goto :goto_1
.end method

.method private initView()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 231
    const v1, 0x7f03000f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->setContentView(I)V

    .line 233
    const v1, 0x7f0d0094

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/HorizontalScrollView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v2, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$2;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setFocusable(Z)V

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setFocusableInTouchMode(Z)V

    .line 255
    const v1, 0x7f0d0095

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    .line 256
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->setSelectedListener(Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;)V

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->DisplayWidth:I

    sget v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->DisplayHeight:I

    iget v4, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->PreviewMargin:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->init(III)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;

    if-nez v1, :cond_1

    .line 264
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    if-nez v1, :cond_2

    .line 267
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    .line 270
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    if-ge v0, v1, :cond_3

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->addView()Lcom/sec/android/app/videoplayer/preview/Preview;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->changeView()V

    .line 275
    return-void
.end method

.method private setWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 220
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->requestWindowFeature(I)Z

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    const/high16 v2, 0x40000

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 224
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->setCanceledOnTouchOutside(Z)V

    .line 225
    return-void
.end method


# virtual methods
.method public changeView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080095

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 168
    .local v1, "margin":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 169
    .local v2, "preview":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 170
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 171
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewlayout:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 174
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 175
    .restart local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->PreviewMargin:I

    invoke-virtual {v0, v3, v5, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 176
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    return-void
.end method

.method public findChapterView()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v1, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$1;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->post(Ljava/lang/Runnable;)Z

    .line 120
    return-void
.end method

.method public getChapterIdx()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mCurrentIdx:I

    return v0
.end method

.method public getResumePos()I
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getResumePos. mChapterResumePos: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    return v0
.end method

.method public initData(JI)Z
    .locals 5
    .param p1, "id"    # J
    .param p3, "resumePos"    # I

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    const-string v2, "initData"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->setData(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0093

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 107
    :goto_0
    return v0

    .line 104
    :cond_0
    iput p3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->resumeChapterView()Z

    move-result v0

    goto :goto_0
.end method

.method public onSelected(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick(). index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    .line 192
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->dismiss()V

    .line 193
    return-void

    .line 190
    :cond_0
    sget v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    mul-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterResumePos:I

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->pauseChapterView()V

    .line 208
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->clear()V

    .line 212
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 213
    return-void
.end method

.method public pauseChapterView()V
    .locals 4

    .prologue
    .line 149
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->bStart:Z

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewTask:Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;

    if-eqz v2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewTask:Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->cancel(Z)Z

    .line 154
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->cancel()V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 157
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/videoplayer/preview/Preview;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/preview/Preview;->getChapterView()Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v0

    .line 159
    .local v0, "cv":Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pauseView()V

    goto :goto_0

    .line 161
    .end local v0    # "cv":Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    :cond_1
    return-void
.end method

.method public resumeChapterView()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;

    const-string v3, "resumeChapterView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    sput-boolean v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->runvideoplayer:Z

    .line 130
    sput-boolean v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->bStart:Z

    .line 132
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->prepare()Z

    move-result v2

    if-nez v2, :cond_0

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0093

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 142
    :goto_0
    return v0

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->initView()V

    .line 139
    new-instance v2, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewTask:Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreviewTask:Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v1

    .line 142
    goto :goto_0
.end method
