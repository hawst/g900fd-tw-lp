.class public Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;
.super Ljava/lang/Object;
.source "ShareVideoCmd.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;


# static fields
.field public static final GROUP_PLAY_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.groupcast"

.field public static final INTENT_ACTION:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_VIDEO"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.mv.player"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    sget-object v2, Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;->TAG:Ljava/lang/String;

    const-string v3, "execute"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    if-nez p1, :cond_0

    .line 38
    :goto_0
    return-void

    .line 29
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.groupcast.action.SEND_VIDEO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 30
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "From"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 31
    const-string v2, "android.intent.extra.STREAM"

    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 34
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    .local v0, "excp":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;->TAG:Ljava/lang/String;

    const-string v3, "ActivityNotFoundException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
