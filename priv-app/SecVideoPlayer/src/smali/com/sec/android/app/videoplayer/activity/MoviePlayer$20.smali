.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mConfigurationClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Landroid/content/res/Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private mIntentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 1

    .prologue
    .line 3756
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 3757
    const-class v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;->mIntentClass:Ljava/lang/Class;

    .line 3758
    const-class v0, Landroid/content/res/Configuration;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;->mConfigurationClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 3761
    const/4 v2, 0x0

    .line 3762
    .local v2, "EXTRA_ARRANGE_MODE":Ljava/lang/String;
    const/4 v1, -0x1

    .line 3766
    .local v1, "ARRANGE_UNDEFINED":I
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;->mIntentClass:Ljava/lang/Class;

    const-string v7, "EXTRA_ARRANGE_MODE"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 3767
    .local v4, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v4, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 3769
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;->mConfigurationClass:Ljava/lang/Class;

    const-string v7, "ARRANGE_UNDEFINED"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 3770
    invoke-virtual {v4, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    .line 3775
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.sec.android.action.NOTIFY_SPLIT_WINDOWS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3776
    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 3777
    .local v5, "mode":I
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "com.sec.android.action.NOTIFY_SPLIT_WINDOWS. mode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3786
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3600()Z

    move-result v6

    if-nez v6, :cond_0

    .line 3787
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->prepareChangeView(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3794
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "mode":I
    :cond_0
    :goto_0
    return-void

    .line 3789
    :catch_0
    move-exception v3

    .line 3790
    .local v3, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v3}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 3791
    .end local v3    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v3

    .line 3792
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method
