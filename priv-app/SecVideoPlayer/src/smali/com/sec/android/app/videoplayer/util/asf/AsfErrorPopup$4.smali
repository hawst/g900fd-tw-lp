.class Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;
.super Ljava/lang/Object;
.source "AsfErrorPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->create()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v8, 0x2db

    const/4 v2, 0x1

    .line 193
    sparse-switch p2, :sswitch_data_0

    .line 236
    :cond_0
    :goto_0
    return v2

    .line 196
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 197
    if-eqz p1, :cond_1

    .line 198
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 200
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)I

    move-result v3

    if-ne v3, v8, :cond_2

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleStateChanged()V
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    goto :goto_0

    .line 203
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleAsfError()V
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    goto :goto_0

    .line 209
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 210
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 212
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 214
    .local v0, "pressTime":J
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-string v5, "videoplayer.set.lock"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)I

    move-result v3

    if-ne v3, v8, :cond_3

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleStateChanged()V
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    goto :goto_0

    .line 219
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleAsfError()V
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    goto :goto_0

    .line 232
    .end local v0    # "pressTime":J
    :sswitch_2
    const/4 v2, 0x0

    goto :goto_0

    .line 193
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x1a -> :sswitch_1
        0x42 -> :sswitch_2
        0x6f -> :sswitch_0
        0x7a -> :sswitch_1
    .end sparse-switch
.end method
