.class Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;
.super Landroid/os/AsyncTask;
.source "VideoCloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCloudUrlTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GetCloudUrlTask"


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 618
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->this$0:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 616
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->mContext:Landroid/content/Context;

    .line 619
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->mContext:Landroid/content/Context;

    .line 620
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 613
    check-cast p1, [Landroid/net/Uri;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->doInBackground([Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # [Landroid/net/Uri;

    .prologue
    .line 623
    if-nez p1, :cond_1

    .line 624
    const-string v2, "GetCloudUrlTask"

    const-string v3, "doInBackground() - uri is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    const/4 v1, 0x0

    .line 638
    :cond_0
    :goto_0
    return-object v1

    .line 628
    :cond_1
    const/4 v1, 0x0

    .line 632
    .local v1, "url":Ljava/lang/String;
    :try_start_0
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 633
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/android/cloudagent/CloudStore$API;->getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 634
    :catch_0
    move-exception v0

    .line 635
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 613
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 642
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->this$0:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->this$0:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;->onUpdated(Ljava/lang/String;)V

    .line 644
    :cond_0
    return-void
.end method
