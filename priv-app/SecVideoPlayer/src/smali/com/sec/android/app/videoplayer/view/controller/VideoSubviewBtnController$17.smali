.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 1903
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1905
    const/4 v4, 0x0

    .line 1906
    .local v4, "retVal":Z
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v5, :cond_0

    .line 1907
    const-wide/16 v2, 0x0

    .line 1908
    .local v2, "pressTime":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    .line 1910
    .local v0, "isDlna":Z
    sparse-switch p2, :sswitch_data_0

    .line 1977
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1978
    .local v1, "isReturn":Z
    if-eqz v1, :cond_a

    .line 1996
    .end local v0    # "isDlna":Z
    .end local v1    # "isReturn":Z
    .end local v2    # "pressTime":J
    :cond_0
    :goto_0
    return v4

    .line 1913
    .restart local v0    # "isDlna":Z
    .restart local v2    # "pressTime":J
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 1915
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 1916
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    .line 1917
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1920
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->blockSpeedSeek()Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1921
    const-string v5, "VideoSubViewBtnController"

    const-string v6, "mRewKeyListener. skip longseek"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x1e

    const/16 v8, 0x9

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v8, 0x1f4

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1924
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 1929
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 1930
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    const-wide/16 v6, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    .line 1932
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->blockSpeedSeek()Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1933
    const-wide/16 v6, 0x1f4

    cmp-long v5, v2, v6

    if-gez v5, :cond_4

    .line 1934
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x1e

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1936
    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1937
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1938
    if-eqz v0, :cond_5

    .line 1939
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1968
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v6, 0xbb8

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 1941
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 1942
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1943
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_1

    .line 1949
    :cond_6
    const-wide/16 v6, 0x1f4

    cmp-long v5, v2, v6

    if-gez v5, :cond_8

    .line 1950
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1951
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->resetHoldLongSeek()V

    goto :goto_1

    .line 1953
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 1954
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1955
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_1

    .line 1958
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const-string v6, "rewSeek"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPauseBy(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1959
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1960
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const-string v6, "rewSeek"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1962
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1702(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z

    .line 1963
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xf

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_1

    .line 1981
    .restart local v1    # "isReturn":Z
    :cond_a
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    goto/16 :goto_0

    .line 1983
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    .line 1984
    goto/16 :goto_0

    .line 1987
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v4

    .line 1988
    goto/16 :goto_0

    .line 1910
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 1913
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1981
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
