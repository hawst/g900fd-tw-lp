.class Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;
.super Ljava/lang/Object;
.source "AsfUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 2
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mPlayerFinderListener - onDeviceAdded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->updatePlayerList()V

    .line 603
    return-void
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 2
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mPlayerFinderListener - onDeviceRemoved"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->updatePlayerList()V

    .line 592
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$700(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/Device;

    move-result-object v0

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$800(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mSelectedPlayer:Lcom/samsung/android/allshare/Device;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$700(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/Device;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mPlayerFinderListener - Selected Device is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->processErrorEvent(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$900(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Lcom/samsung/android/allshare/ERROR;)V

    .line 598
    :cond_0
    return-void
.end method
