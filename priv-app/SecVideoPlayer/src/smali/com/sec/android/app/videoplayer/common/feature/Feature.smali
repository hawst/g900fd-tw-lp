.class public Lcom/sec/android/app/videoplayer/common/feature/Feature;
.super Ljava/lang/Object;
.source "Feature.java"


# static fields
.field public static final ALLSHARE_FRAMEWORK:Z

.field public static final AMOLED_DEVICE:Z

.field public static final APP_IN_APP:Z

.field public static final AUDIO_SHOCK_DISPLAY:Z = true

.field public static final BIGPONDTV_TELSTRA:Z

.field public static final BRIGHTENESS_SETTINGS_ENABLE:Z

.field public static final BRIGHTNESS_N_VOLUME_CONTROL:Z = true

.field public static final CHAGALL_VZW_WIDGET_CONCEPT:Z

.field public static final CHANGE_PLAYER_SUPPORT_WFD:Z

.field public static final CHINA:Z

.field public static final CHINA_CSFB_STREAMING:Z

.field public static final DISMISS_POPUP_TOUCH_OUTSIDE:Z

.field public static final DLNA_CERTIFICATION:Z = false

.field public static final DOWNLOADMUSICVIDEO_TELSTRA:Z

.field public static final ENABLE_CHANGABLE_HOME_THEME:Z = false

.field public static final ENABLE_MAGAZINE_HOME:Z

.field public static final FLAG_SUPPORT_ADAPT_SOUND:Z

.field public static final FLAG_SUPPORT_ANIMATION:Z = false

.field public static final HELP:Z = true

.field public static final HIGH_SPEED_PLAY:Z

.field public static final IS_DUAL_LCD:Z

.field public static final IS_FOLDER_TYPE:Z

.field public static final IS_TABLET:Z

.field public static final KDRM_FEATURE:Z = false

.field public static final KOREA:Z

.field public static final LANDSCAPE_ASSISTANTMENU:Z

.field public static final MAIN_WIDGET_FEATURE:Z

.field public static final MIRROR_CALL:Z

.field public static final MODEL_A5:Z

.field public static final MODEL_A7:Z

.field public static final MODEL_BERLUTI_USA:Z

.field public static final MODEL_CHAGALL:Z

.field public static final MODEL_E:Z

.field public static final MODEL_E5:Z

.field public static final MODEL_E7:Z

.field public static final MODEL_F:Z

.field public static final MODEL_KLIMT:Z

.field public static final MODEL_KONA:Z

.field public static final MODEL_K_USA:Z

.field public static final MODEL_P:Z

.field public static final MODEL_PATEK:Z

.field public static final MODEL_RUBENS:Z

.field public static final MODEL_SLTE:Z

.field public static final MODEL_T:Z

.field public static final MODEL_TAB_S:Z

.field public static final MODEL_VASTA:Z

.field public static final MULTIWINDOW:Z

.field public static final MULTIWINDOW_TRAY_BUTTON:Z = false

.field public static final MULTI_SPEAKER:Z

.field public static final NEW_PREVIEW:Z

.field public static final NULL_BACKGROUND_COLOR:Z = true

.field public static final PHONE_MINI_CTRL_ENABLE:Z

.field public static final PLAYING_ON_VZW_GUIDE_TOUR:Z

.field public static final PLAYING_VZW:Z

.field public static final QUALCOMM_CELOX_BASE_CHIP:Z

.field public static final QUALCOMM_H_BASE_CHIP:Z

.field public static final QUALCOMM_JF_CHIP:Z = false

.field public static final ROTATE_SECOND_SCREEN:Z

.field public static final SETTINGS_MOTION_CONCEPT_2014:Z

.field public static final SIDESYNC:Z = true

.field public static final SIDE_MIRRORING:Z

.field public static SKT_CLOUD:Z = false

.field public static final SLOW_CPU:Z

.field public static final SLSI_CHIP:Z

.field public static final SLSI_H_BASE_CHIP:Z

.field public static final SLSI_JA_CHIP:Z

.field public static final SLSI_Q1_U1_CHIP:Z

.field public static final SLSI_SMDK_CHIP:Z

.field public static final SMART_PAUSE:Z

.field public static final SMART_PAUSE_POPUP_PLAYER:Z

.field public static final SOUNDALIVE_FX_SEC:Z = true

.field public static final STE_CHIP:Z

.field public static final STE_GOLDEN_CHIP:Z

.field public static final STE_JANICE_CHIP:Z

.field public static final SUPPORT_2ND_SCREEN:Z

.field public static final SUPPORT_AIA_CONTROLLER:Z

.field public static final SUPPORT_AIR_DUAL_VIEW:Z

.field public static final SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

.field public static final SUPPORT_BAIDU_CLOUD:Z

.field public static final SUPPORT_DRM_AIA_FUNCTION:Z

.field public static final SUPPORT_FINGER_AIR_VIEW:Z

.field public static final SUPPORT_L_SCREEN_CONCEPT:Z

.field public static final SUPPORT_OMA_FL_ONLY:Z

.field public static final SUPPORT_TICK_PLAYBACK:Z

.field private static final TAG:Ljava/lang/String; = "Feature"

.field public static final TAG_BUDDY_INFO:Z

.field public static final UIBC_CONTROLLER:Z = true

.field public static final USA:Z

.field public static final UWA_CALL:Z

.field public static VIDEO_360:Z

.field public static final VIDEO_CAPTURE:Z

.field public static final VIDEO_EDITOR:Z

.field public static final VIDEO_EDITOR_FULL:Z

.field public static final VIDEO_SPLIT_SUBTITLE:Z

.field public static final VIDEO_WALL:Z

.field public static final VIDEO_ZOOM:Z

.field public static WATCH_ON:Z

.field public static final WEATHER_NEWS:Z

.field public static final WIDGET_LIVE_DEMO_UNIT:Z

.field public static final WIFI_DISPLAY:Z

.field public static final WMDRM_NOT_SUPPORT:Z

.field private static final mCscFeature:Lcom/sec/android/app/CscFeature;

.field public static mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 14
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mCscFeature:Lcom/sec/android/app/CscFeature;

    .line 15
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    .line 19
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    .line 21
    const-string v0, "smdk4210"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_Q1_U1_CHIP:Z

    .line 23
    const-string v0, "universal5410"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_JA_CHIP:Z

    .line 25
    const-string v0, "DB8520H"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_GOLDEN_CHIP:Z

    .line 26
    const-string v0, "montblanc"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_JANICE_CHIP:Z

    .line 29
    const-string v0, "MSM8660_SURF"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->QUALCOMM_CELOX_BASE_CHIP:Z

    .line 30
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_JANICE_CHIP:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_Q1_U1_CHIP:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_GOLDEN_CHIP:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->QUALCOMM_CELOX_BASE_CHIP:Z

    if-eqz v0, :cond_15

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLOW_CPU:Z

    .line 32
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "flte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_F:Z

    .line 33
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "chagall"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    .line 34
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klimt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    .line 35
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "picasso"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    if-eqz v0, :cond_16

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_P:Z

    .line 39
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "e5lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "e53g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_2
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E5:Z

    .line 40
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "e7lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "e73g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_3
    move v0, v2

    :goto_3
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E7:Z

    .line 41
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E5:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E7:Z

    if-eqz v0, :cond_19

    :cond_4
    move v0, v2

    :goto_4
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E:Z

    .line 43
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trhlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tre3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-01G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SCL24"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trhplte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tbhplte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tr3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_5
    move v0, v2

    :goto_5
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    .line 56
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "slte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_SLTE:Z

    .line 57
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "a5"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_A5:Z

    .line 58
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "a7"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_A7:Z

    .line 59
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "rubens"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_RUBENS:Z

    .line 60
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "pateklte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_PATEK:Z

    .line 61
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    if-eqz v0, :cond_1b

    :cond_6
    move v0, v2

    :goto_6
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    .line 62
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kltelra"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klteacg"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klteatt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kltevzw"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kltetmo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klteattactive"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klteusc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kltesprsports"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_7
    move v0, v2

    :goto_7
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_K_USA:Z

    .line 71
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "slteatt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_BERLUTI_USA:Z

    .line 74
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_SLTE:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_A5:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_A7:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_PATEK:Z

    if-eqz v0, :cond_1d

    :cond_8
    move v0, v2

    :goto_8
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    .line 76
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_RUBENS:Z

    if-eqz v0, :cond_1e

    :cond_9
    move v0, v2

    :goto_9
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    .line 78
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_F:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    .line 86
    const-string v0, "persist.audio.mysound"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    move v0, v1

    :goto_a
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->FLAG_SUPPORT_ADAPT_SOUND:Z

    .line 88
    const-string v0, "persist.audio.stereospeaker"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_20

    move v0, v2

    :goto_b
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    .line 93
    const-string v0, "USA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->USA:Z

    .line 95
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->USA:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WMDRM_NOT_SUPPORT:Z

    .line 97
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isSupport_ONLY_OMA_FL()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_OMA_FL_ONLY:Z

    .line 98
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "China"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "china"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "CHINA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    :cond_a
    move v0, v2

    :goto_c
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    .line 102
    const-string v0, "KOREA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->KOREA:Z

    .line 105
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_P:Z

    if-nez v0, :cond_b

    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Video_AddWidgetLayout"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_b
    move v0, v2

    :goto_d
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MAIN_WIDGET_FEATURE:Z

    .line 106
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->hasMultiWindowFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    .line 107
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isFolderType()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    .line 108
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isDualLCD()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    .line 109
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isSupportSmartPause()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    .line 110
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v0, :cond_c

    :cond_c
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE_POPUP_PLAYER:Z

    .line 111
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_ALLSHARE_CONFIG"

    const-string v4, "ALL"

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "NONE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    move v0, v2

    :goto_e
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    .line 113
    const-string v0, "VZW"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    .line 115
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    .line 117
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v0, :cond_d

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    if-eqz v0, :cond_24

    :cond_d
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-eqz v0, :cond_24

    move v0, v2

    :goto_f
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHAGALL_VZW_WIDGET_CONCEPT:Z

    .line 118
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->supportAutoBrightnessDetail()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    .line 120
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isSupportWFD()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WIFI_DISPLAY:Z

    .line 124
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WIFI_DISPLAY:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    .line 125
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Common_SupportUwaApp"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->UWA_CALL:Z

    .line 126
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Common_SupportMirrorCall"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MIRROR_CALL:Z

    .line 129
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SECURITY_DRM_POPUP_PLAY"

    invoke-virtual {v0, v3, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_DRM_AIA_FUNCTION:Z

    .line 131
    const/4 v0, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isMMFWFeatureEnabled(IZ)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    .line 132
    const/4 v0, 0x3

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isMMFWFeatureEnabled(IZ)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_TICK_PLAYBACK:Z

    .line 134
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_VIDEOPLAYER_SUPPORT_APPINAPP"

    invoke-virtual {v0, v3, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    .line 137
    const-string v0, "none"

    sget-object v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v4, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_VIDEO_EDITOR"

    const-string v5, "none"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    move v0, v2

    :goto_10
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR:Z

    .line 140
    const-string v0, "VEFULL"

    sget-object v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v4, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_VIDEO_EDITOR"

    const-string v5, "none"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_26

    move v0, v2

    :goto_11
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR_FULL:Z

    .line 144
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v0, :cond_27

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E:Z

    if-nez v0, :cond_27

    move v0, v2

    :goto_12
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    .line 148
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v0, :cond_28

    move v0, v2

    :goto_13
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_WALL:Z

    .line 154
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E:Z

    if-nez v0, :cond_29

    move v0, v2

    :goto_14
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PHONE_MINI_CTRL_ENABLE:Z

    .line 157
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E:Z

    if-nez v0, :cond_2a

    move v0, v2

    :goto_15
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->BRIGHTENESS_SETTINGS_ENABLE:Z

    .line 159
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_VIDEOPLAYER_HIGH_SPEED_PLAY"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->HIGH_SPEED_PLAY:Z

    .line 161
    const/4 v0, 0x4

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isMMFWFeatureEnabled(IZ)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_ZOOM:Z

    .line 162
    invoke-static {v2, v2}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isMMFWFeatureEnabled(IZ)Z

    move-result v0

    if-eqz v0, :cond_2b

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E:Z

    if-nez v0, :cond_2b

    move v0, v2

    :goto_16
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    .line 164
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "JP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WEATHER_NEWS:Z

    .line 165
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-eqz v0, :cond_2c

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_2c

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v0, :cond_2c

    move v0, v2

    :goto_17
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    .line 168
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->enableMagazineHome()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ENABLE_MAGAZINE_HOME:Z

    .line 170
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_FINGER_AIR_VIEW"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_FINGER_AIR_VIEW:Z

    .line 174
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    .line 175
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    .line 177
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v0, :cond_e

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v0, :cond_e

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KLIMT:Z

    if-eqz v0, :cond_2d

    :cond_e
    move v0, v2

    :goto_18
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->NEW_PREVIEW:Z

    .line 178
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    .line 180
    const-string v0, "ro.product.board"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "smdk"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "ro.product.board"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SMDK"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    :cond_f
    move v0, v2

    :goto_19
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_SMDK_CHIP:Z

    .line 182
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_SMDK_CHIP:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_CHIP:Z

    .line 183
    const-string v0, "universal5420"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_H_BASE_CHIP:Z

    .line 184
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_GOLDEN_CHIP:Z

    if-nez v0, :cond_10

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_JANICE_CHIP:Z

    if-eqz v0, :cond_2f

    :cond_10
    move v0, v2

    :goto_1a
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->STE_CHIP:Z

    .line 185
    const-string v0, "MSM8974"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "MSM8674"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    :cond_11
    move v0, v2

    :goto_1b
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->QUALCOMM_H_BASE_CHIP:Z

    .line 187
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_BAIDU_CLOUD:Z

    .line 189
    const-string v0, "ro.csc.countryiso_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "AU"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "TEL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    move v0, v2

    :goto_1c
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->BIGPONDTV_TELSTRA:Z

    .line 191
    const-string v0, "ro.csc.countryiso_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "AU"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "TEL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    move v0, v2

    :goto_1d
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DOWNLOADMUSICVIDEO_TELSTRA:Z

    .line 194
    const-string v0, "PAP"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WIDGET_LIVE_DEMO_UNIT:Z

    .line 195
    const-string v0, "grande"

    const-string v3, "ro.build.scafe.size"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "tall"

    const-string v3, "ro.build.scafe.size"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    :cond_12
    move v0, v2

    :goto_1e
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    .line 197
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "mega2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    .line 199
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_360:Z

    .line 200
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SETTINGS_MOTION_CONCEPT_2014:Z

    .line 203
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kona"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_KONA:Z

    .line 204
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isSupport2ndScreen()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_2ND_SCREEN:Z

    .line 207
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA_CSFB_STREAMING:Z

    .line 212
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "lt03"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "matisse"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_CHAGALL:Z

    if-eqz v0, :cond_14

    :cond_13
    move v1, v2

    :cond_14
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->LANDSCAPE_ASSISTANTMENU:Z

    return-void

    :cond_15
    move v0, v1

    .line 30
    goto/16 :goto_0

    :cond_16
    move v0, v1

    .line 35
    goto/16 :goto_1

    :cond_17
    move v0, v1

    .line 39
    goto/16 :goto_2

    :cond_18
    move v0, v1

    .line 40
    goto/16 :goto_3

    :cond_19
    move v0, v1

    .line 41
    goto/16 :goto_4

    :cond_1a
    move v0, v1

    .line 43
    goto/16 :goto_5

    :cond_1b
    move v0, v1

    .line 61
    goto/16 :goto_6

    :cond_1c
    move v0, v1

    .line 62
    goto/16 :goto_7

    :cond_1d
    move v0, v1

    .line 74
    goto/16 :goto_8

    :cond_1e
    move v0, v1

    .line 76
    goto/16 :goto_9

    :cond_1f
    move v0, v2

    .line 86
    goto/16 :goto_a

    :cond_20
    move v0, v1

    .line 88
    goto/16 :goto_b

    :cond_21
    move v0, v1

    .line 98
    goto/16 :goto_c

    :cond_22
    move v0, v1

    .line 105
    goto/16 :goto_d

    :cond_23
    move v0, v1

    .line 111
    goto/16 :goto_e

    :cond_24
    move v0, v1

    .line 117
    goto/16 :goto_f

    :cond_25
    move v0, v1

    .line 137
    goto/16 :goto_10

    :cond_26
    move v0, v1

    .line 140
    goto/16 :goto_11

    :cond_27
    move v0, v1

    .line 144
    goto/16 :goto_12

    :cond_28
    move v0, v1

    .line 148
    goto/16 :goto_13

    :cond_29
    move v0, v1

    .line 154
    goto/16 :goto_14

    :cond_2a
    move v0, v1

    .line 157
    goto/16 :goto_15

    :cond_2b
    move v0, v1

    .line 162
    goto/16 :goto_16

    :cond_2c
    move v0, v1

    .line 165
    goto/16 :goto_17

    :cond_2d
    move v0, v1

    .line 177
    goto/16 :goto_18

    :cond_2e
    move v0, v1

    .line 180
    goto/16 :goto_19

    :cond_2f
    move v0, v1

    .line 184
    goto/16 :goto_1a

    :cond_30
    move v0, v1

    .line 185
    goto/16 :goto_1b

    :cond_31
    move v0, v1

    .line 189
    goto/16 :goto_1c

    :cond_32
    move v0, v1

    .line 191
    goto/16 :goto_1d

    :cond_33
    move v0, v1

    .line 195
    goto/16 :goto_1e
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static enableMagazineHome()Z
    .locals 2

    .prologue
    .line 230
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 231
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 232
    const/4 v1, 0x0

    .line 234
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.magazine.home"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static hasMultiWindowFeature()Z
    .locals 2

    .prologue
    .line 251
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 252
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 253
    const/4 v1, 0x0

    .line 256
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.multiwindow.commonui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isDualLCD()Z
    .locals 2

    .prologue
    .line 244
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 245
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 246
    const/4 v1, 0x0

    .line 248
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.dual_lcd"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isFolderType()Z
    .locals 2

    .prologue
    .line 237
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 238
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 239
    const/4 v1, 0x0

    .line 241
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.folder_type"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isGateEnabled()Z
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/util/GateConfig;->isGateLcdtextEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isMMFWFeatureEnabled(IZ)Z
    .locals 4
    .param p0, "feature"    # I
    .param p1, "defaultValue"    # Z

    .prologue
    .line 277
    :try_start_0
    invoke-static {p0}, Landroid/media/SecMMFWConfiguration;->isEnabledFeature(I)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 281
    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .line 278
    .restart local p1    # "defaultValue":Z
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const-string v1, "Feature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NoClassDefFoundError : Please add SecMMFWConfiguration for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static isSupport2ndScreen()Z
    .locals 2

    .prologue
    .line 259
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 260
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 261
    const/4 v1, 0x0

    .line 263
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.cocktailbar"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isSupportSmartPause()Z
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 291
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 292
    const/4 v1, 0x0

    .line 294
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.android.smartface.smart_pause"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isSupportWFD()Z
    .locals 2

    .prologue
    .line 266
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 267
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 268
    const/4 v1, 0x0

    .line 270
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.wfd_support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isSupport_ONLY_OMA_FL()Z
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x1

    .line 226
    .local v0, "OMA_FL_ONLY":Z
    return v0
.end method

.method private static supportAutoBrightnessDetail()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 284
    sget-object v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v5, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_AUTOMATIC_BRIGHTNESS_DETAIL"

    invoke-virtual {v4, v5, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v1

    .line 285
    .local v1, "floatingFeature":Z
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Setting_ConfigAutomaticBrightnessDetail"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "off"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_Setting_ConfigAutomaticBrightnessDetail"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v2

    .line 286
    .local v0, "cscFeature":Z
    :goto_0
    const-string v4, "Feature"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "supportAutoBrightnessDetail : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    return v3

    .end local v0    # "cscFeature":Z
    :cond_2
    move v0, v3

    .line 285
    goto :goto_0
.end method
