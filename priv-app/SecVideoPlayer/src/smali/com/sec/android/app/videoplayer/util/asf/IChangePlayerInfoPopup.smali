.class public abstract Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;
.super Ljava/lang/Object;
.source "IChangePlayerInfoPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# instance fields
.field mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->mContext:Landroid/content/Context;

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    return-void
.end method


# virtual methods
.method public setContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->mContext:Landroid/content/Context;

    .line 13
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 14
    return-void
.end method

.method public abstract show()V
.end method
