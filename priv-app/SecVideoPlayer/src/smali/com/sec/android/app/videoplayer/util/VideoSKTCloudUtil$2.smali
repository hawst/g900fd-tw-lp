.class Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;
.super Ljava/lang/Object;
.source "VideoSKTCloudUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->downloadCloudFile(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field final synthetic val$mUriList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 641
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;->val$mUriList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 643
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;->val$mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 644
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;->val$mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 646
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->access$000(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->download(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 643
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 647
    :catch_0
    move-exception v0

    .line 648
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "VideoSKTCloudUtil"

    const-string v4, "Failed download com.sec.android.cloudagent selectedListUri"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    const-string v3, "VideoSKTCloudUtil"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 652
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method
