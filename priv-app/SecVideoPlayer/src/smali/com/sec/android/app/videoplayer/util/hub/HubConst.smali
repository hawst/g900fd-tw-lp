.class public interface abstract Lcom/sec/android/app/videoplayer/util/hub/HubConst;
.super Ljava/lang/Object;
.source "HubConst.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.videohub"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CUSTOM_DATA_URI:Landroid/net/Uri;

.field public static final DRM_CUSTOM_DATA_APP_ID:Ljava/lang/String; = "app_Id"

.field public static final DRM_CUSTOM_DATA_IMEI:Ljava/lang/String; = "imei"

.field public static final DRM_CUSTOM_DATA_MV_ID:Ljava/lang/String; = "mv_id"

.field public static final DRM_CUSTOM_DATA_SVR_ID:Ljava/lang/String; = "svr_id"

.field public static final DRM_CUSTOM_DATA_TABLE:Ljava/lang/String; = "tb_drm_custom_data_table"

.field public static final DRM_CUSTOM_DATA_USER_GUID:Ljava/lang/String; = "user_guid"

.field public static final MEDIA_HUB:Ljava/lang/String; = "Media_hub"

.field public static final SCHEME:Ljava/lang/String; = "content://"

.field public static final VIDEOHUB_ATTR_TYPE_CODE:Ljava/lang/String; = "video_attr_type_code_2"

.field public static final VIDEOHUB_DATA:Ljava/lang/String; = "_data"

.field public static final VIDEOHUB_DURATION:Ljava/lang/String; = "duration"

.field public static final VIDEOHUB_FHD_FILE:Ljava/lang/String; = "00"

.field public static final VIDEOHUB_FHD_FILE_SIZE:Ljava/lang/String; = "fhd_file_size"

.field public static final VIDEOHUB_HD_FILE:Ljava/lang/String; = "01"

.field public static final VIDEOHUB_HD_FILE_SIZE:Ljava/lang/String; = "hd_file_size"

.field public static final VIDEOHUB_ID:Ljava/lang/String; = "_id"

.field public static final VIDEOHUB_NOT_SUPPORT:Ljava/lang/String; = "N"

.field public static final VIDEOHUB_ORDER_ID:Ljava/lang/String; = "order_id"

.field public static final VIDEOHUB_PRODUCT_ID:Ljava/lang/String; = "product_id"

.field public static final VIDEOHUB_RESUME_POSITION:Ljava/lang/String; = "resumePos"

.field public static final VIDEOHUB_SD_FILE:Ljava/lang/String; = "02"

.field public static final VIDEOHUB_SD_FILE_SIZE:Ljava/lang/String; = "sd_file_size"

.field public static final VIDEOHUB_SUPPORT:Ljava/lang/String; = "Y"

.field public static final VIDEOHUB_SUPPORT_WFD_CODE:Ljava/lang/String; = "wfd_yn"

.field public static final VIDEOHUB_TITLE:Ljava/lang/String; = "title"

.field public static final VIDEOHUB_WATCH_ON_SUPPORT:Ljava/lang/String; = "Y"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    const-string v0, "content://com.samsung.videohub"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    .line 50
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "tb_drm_custom_data_table"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubConst;->CUSTOM_DATA_URI:Landroid/net/Uri;

    return-void
.end method
