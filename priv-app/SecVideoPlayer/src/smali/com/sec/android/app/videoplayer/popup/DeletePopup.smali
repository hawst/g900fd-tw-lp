.class public Lcom/sec/android/app/videoplayer/popup/DeletePopup;
.super Ljava/lang/Object;
.source "DeletePopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# static fields
.field private static final ID:I = -0x1

.field private static final TAG:Ljava/lang/String; = "DeletePopup"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mDialog:Landroid/app/AlertDialog;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 152
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/DeletePopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 162
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/DeletePopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    .line 42
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/popup/DeletePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/DeletePopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 55
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 50
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, -0x1

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected performDeleteFile()V
    .locals 12

    .prologue
    .line 96
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 99
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    .line 102
    .local v3, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isTypeUnknown()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 103
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriById(J)Landroid/net/Uri;

    move-result-object v5

    .line 108
    .local v5, "uri":Landroid/net/Uri;
    :goto_0
    const/4 v2, 0x0

    .line 110
    .local v2, "deleted":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isMovieStoreContent()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 111
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v6

    .line 112
    .local v6, "videoHubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->set(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    .line 113
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getMediaHubPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getHubUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 114
    const-string v7, "DeletePopup"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HUB Path = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getMediaHubPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.samsung.everglades.video.ACTION_DELETE_CONTENT"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    .local v4, "i":Landroid/content/Intent;
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 120
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 142
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v4    # "i":Landroid/content/Intent;
    .end local v6    # "videoHubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v7

    const-string v8, "autoPlay"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v1

    .line 144
    .local v1, "autoNextPlay":Z
    if-eqz v1, :cond_5

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getTotalVideoFileCnt()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_5

    .line 145
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    .line 150
    :goto_2
    return-void

    .line 105
    .end local v1    # "autoNextPlay":Z
    .end local v2    # "deleted":Z
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    .restart local v5    # "uri":Landroid/net/Uri;
    goto/16 :goto_0

    .line 122
    .restart local v2    # "deleted":Z
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 123
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->deleteContent(Landroid/net/Uri;)Z

    move-result v2

    .line 124
    if-eqz v2, :cond_2

    .line 125
    const-string v7, "DeletePopup"

    const-string v8, "Cloud-Content is deleted!"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 127
    :cond_2
    const-string v7, "DeletePopup"

    const-string v8, "Cloud-Content deletion has failed!"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 130
    :cond_3
    const-string v7, "DeletePopup"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Start to delete! file URI="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->deleteVideo(Landroid/net/Uri;)Z

    move-result v2

    .line 132
    const-string v7, "DeletePopup"

    const-string v8, "end of deltetion!"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    if-eqz v2, :cond_4

    .line 134
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_SCAN"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v11, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v7, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 135
    const-string v7, "DeletePopup"

    const-string v8, "sendBroadcast"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 137
    :cond_4
    const-string v7, "DeletePopup"

    const-string v8, "Delete failed!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 148
    .restart local v1    # "autoNextPlay":Z
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v7

    new-instance v8, Landroid/content/Intent;

    const-string v9, "videoplayer.exit"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_2
.end method

.method public showPopup()V
    .locals 4

    .prologue
    const v3, 0x7f0a00a2

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v2, "DeletePopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 69
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0048

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/DeletePopup$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0026

    new-instance v3, Lcom/sec/android/app/videoplayer/popup/DeletePopup$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/DeletePopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/DeletePopup;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 87
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 93
    :cond_0
    return-void
.end method
