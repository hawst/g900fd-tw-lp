.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 677
    const/4 v1, 0x0

    .line 678
    .local v1, "retVal":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 679
    sparse-switch p2, :sswitch_data_0

    .line 711
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 712
    .local v0, "isReturn":Z
    if-eqz v0, :cond_2

    .line 730
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 682
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 684
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_0

    .line 688
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getControllerMode()Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    if-ne v3, v4, :cond_0

    .line 689
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlRightPos()I

    move-result v4

    if-ne v4, v2, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v3, v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setMiniCtrlRightPos(I)V

    .line 692
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 699
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    goto :goto_0

    .line 715
    .restart local v0    # "isReturn":Z
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 717
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 718
    goto :goto_0

    .line 721
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 722
    goto :goto_0

    .line 679
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 682
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 715
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
