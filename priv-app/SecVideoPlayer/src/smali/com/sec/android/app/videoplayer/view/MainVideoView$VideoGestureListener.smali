.class final Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;
.super Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VideoGestureListener"
.end annotation


# static fields
.field private static final SWIPE_MAX_Y_DISTANCE:I = 0x5dc

.field private static final SWIPE_MIN_X_DISTANCE:I = 0x78

.field private static final SWIPE_SEEK_VALUE:I = 0x1388

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x7d0


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;Lcom/sec/android/app/videoplayer/view/MainVideoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/view/MainVideoView$1;

    .prologue
    .line 329
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 404
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    :goto_0
    return v3

    .line 407
    :cond_0
    const-string v1, "MainVideoView"

    const-string v2, "VideoGestureListener - onDoubleTap"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    .line 419
    .local v0, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 427
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 433
    const-string v0, "MainVideoView"

    const-string v1, "VideoGestureListener - onDoubleTapEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/16 v8, 0xbb8

    const/high16 v7, 0x44fa0000    # 2000.0f

    const/high16 v6, 0x42f00000    # 120.0f

    const/4 v5, 0x1

    .line 341
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    .line 343
    .local v2, "util":Lcom/sec/android/app/videoplayer/common/VUtils;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 345
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isOnLongSeekMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v5

    .line 350
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 351
    const-string v3, "MainVideoView"

    const-string v4, "onFling. skip DLNA mode"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 355
    :cond_2
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v3

    if-nez v3, :cond_3

    .line 356
    const-string v3, "MainVideoView"

    const-string v4, "onFling. skip there is no duration info"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isOnLongSeekMode()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 360
    const-string v3, "MainVideoView"

    const-string v4, "onFling. skip long-seek mode"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 365
    :cond_4
    :try_start_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x44bb8000    # 1500.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6

    .line 366
    const-string v3, "MainVideoView"

    const-string v4, "onFling. skip by SWIPE_MAX_Y_DISTANCE"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 386
    :catch_0
    move-exception v3

    .line 390
    :cond_5
    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 392
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    goto :goto_0

    .line 370
    :cond_6
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    .line 373
    .local v1, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v6

    if-lez v3, :cond_7

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v7

    if-lez v3, :cond_7

    .line 374
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v3

    add-int/lit16 v3, v3, -0x1388

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    .line 375
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    if-eqz v3, :cond_5

    .line 376
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->leftFlickView()V

    goto :goto_1

    .line 379
    :cond_7
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v6

    if-lez v3, :cond_8

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v7

    if-lez v3, :cond_8

    .line 380
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v3

    add-int/lit16 v3, v3, 0x1388

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    .line 381
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    if-eqz v3, :cond_5

    .line 382
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->rightFlickView()V

    goto :goto_1

    .line 384
    :cond_8
    const-string v3, "MainVideoView"

    const-string v4, "onFling. skip by SWIPE_MIN_X_DISTANCE"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 393
    .end local v1    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 394
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    goto/16 :goto_0
.end method
