.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;
.super Ljava/lang/Object;
.source "SubtitleColorPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field colorTable:[[I

.field ct:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

.field length:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 1

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->ct:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->ct:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->getColorTable()[[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->colorTable:[[I

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->ct:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->getHueLength()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    .line 683
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 684
    .local v2, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 686
    .local v3, "y":I
    const/4 v0, 0x0

    .line 687
    .local v0, "i":I
    const/4 v1, 0x0

    .line 689
    .local v1, "j":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 758
    :cond_0
    :goto_0
    return v6

    .line 695
    :pswitch_0
    if-ltz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    if-gt v2, v4, :cond_0

    .line 698
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginX()I

    move-result v4

    if-ge v2, v4, :cond_8

    .line 699
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginX()I

    move-result v2

    .line 704
    :cond_1
    :goto_1
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    mul-int/2addr v4, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int v0, v4, v5

    .line 710
    if-ltz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 713
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginY()I

    move-result v4

    if-ge v3, v4, :cond_9

    .line 714
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginY()I

    move-result v3

    .line 719
    :cond_2
    :goto_2
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    mul-int/2addr v4, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int v1, v4, v5

    .line 725
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_a

    .line 726
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 727
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 735
    :cond_3
    :goto_3
    if-gez v0, :cond_4

    .line 736
    const/4 v0, 0x0

    .line 738
    :cond_4
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    if-lt v0, v4, :cond_5

    .line 739
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    add-int/lit8 v0, v4, -0x1

    .line 742
    :cond_5
    if-gez v1, :cond_6

    .line 743
    const/4 v1, 0x0

    .line 745
    :cond_6
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    if-lt v1, v4, :cond_7

    .line 746
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->length:I

    add-int/lit8 v1, v4, -0x1

    .line 749
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x2

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    .line 750
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->setPointerPos(II)V

    .line 751
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->invalidate()V

    .line 752
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->colorTable:[[I

    aget-object v5, v5, v0

    aget v5, v5, v1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateColor(I)V
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$2000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V

    goto/16 :goto_0

    .line 700
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginX()I

    move-result v5

    sub-int/2addr v4, v5

    if-le v2, v4, :cond_1

    .line 701
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginX()I

    move-result v5

    sub-int v2, v4, v5

    goto/16 :goto_1

    .line 715
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginY()I

    move-result v5

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_2

    .line 716
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->getMarginY()I

    move-result v5

    sub-int v3, v4, v5

    goto/16 :goto_2

    .line 729
    :cond_a
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v6, :cond_3

    .line 730
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 731
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_3

    .line 689
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
