.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
.super Ljava/lang/Object;
.source "SubtitlePrefMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mSubtitleMgrInst:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;


# instance fields
.field private PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

.field private mCaptionWinColor:I

.field private mCaptionWinOpacity:I

.field private mContext:Landroid/content/Context;

.field private mFontBGColor:I

.field private mFontBGOpacity:I

.field private mFontColor:I

.field private mFontEdge:I

.field private mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

.field private mFontName:Ljava/lang/String;

.field private mFontOpacity:I

.field private mFontPackageName:Ljava/lang/String;

.field private mFontSize:I

.field private mFontStringName:Ljava/lang/String;

.field private mSplitSubtitleActivation:Z

.field private mSubtitleActivation:Z

.field private mSubtitlePrefChangeListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;

.field private mTextAlignment:I

.field private mWhite:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleActivation:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSplitSubtitleActivation:Z

    .line 37
    const/16 v0, 0x1f

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mTextAlignment:I

    .line 39
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    .line 41
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    .line 43
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    .line 45
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontSize:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontEdge:I

    .line 49
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontColor:I

    .line 51
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontOpacity:I

    .line 53
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    .line 55
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGOpacity:I

    .line 57
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinColor:I

    .line 59
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinOpacity:I

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitlePrefChangeListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;

    .line 63
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mWhite:I

    .line 66
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleMgrInst:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleMgrInst:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    .line 72
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleMgrInst:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    return-object v0
.end method

.method private sendDataChangeNotify(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    .line 503
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitlePrefChangeListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;

    if-eqz v0, :cond_0

    .line 504
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendDataChangeNotify. notify type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitlePrefChangeListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;->onSubtitlePrefDataChanged(I)V

    .line 507
    :cond_0
    return-void
.end method

.method private setWhite()I
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 85
    .local v0, "white":I
    sparse-switch v0, :sswitch_data_0

    .line 90
    const/4 v0, -0x1

    .end local v0    # "white":I
    :sswitch_0
    return v0

    .line 85
    :sswitch_data_0
    .sparse-switch
        -0xa0a0b -> :sswitch_0
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public applyFont(Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 3
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 269
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p2, :cond_0

    .line 270
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFont(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 274
    .local v1, "tf":Landroid/graphics/Typeface;
    if-eqz v1, :cond_0

    .line 276
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 282
    .end local v0    # "i":I
    .end local v1    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void

    .line 270
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public applyTextSize(ILandroid/widget/TextView;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 286
    if-eqz p2, :cond_0

    .line 287
    const/16 v0, 0x16

    .line 288
    .local v0, "size":I
    packed-switch p1, :pswitch_data_0

    .line 301
    :goto_0
    int-to-float v2, v0

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 303
    .local v1, "tf":Landroid/graphics/Typeface;
    if-eqz v1, :cond_0

    .line 304
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 306
    .end local v0    # "size":I
    .end local v1    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void

    .line 290
    .restart local v0    # "size":I
    :pswitch_0
    const/16 v0, 0x12

    .line 291
    goto :goto_0

    .line 294
    :pswitch_1
    const/16 v0, 0x16

    .line 295
    goto :goto_0

    .line 298
    :pswitch_2
    const/16 v0, 0x1c

    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCaptionWinColor()I
    .locals 1

    .prologue
    .line 358
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinColor:I

    return v0
.end method

.method public getCaptionWinOpacity()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinOpacity:I

    return v0
.end method

.method public getColorValue(I)Ljava/lang/String;
    .locals 4
    .param p1, "menu"    # I

    .prologue
    .line 378
    const/4 v0, 0x0

    .line 380
    .local v0, "curColor":I
    const/4 v1, 0x5

    if-ne p1, v1, :cond_0

    .line 381
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontColor:I

    .line 388
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getColorValue. menu : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", color : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    sparse-switch v0, :sswitch_data_0

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    return-object v1

    .line 382
    :cond_0
    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    .line 383
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinColor:I

    goto :goto_0

    .line 385
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    goto :goto_0

    .line 393
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 395
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 397
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0100

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 399
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 401
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 403
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 405
    :sswitch_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 407
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 409
    :sswitch_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 390
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1000000 -> :sswitch_1
        -0xffff01 -> :sswitch_4
        -0xff4500 -> :sswitch_3
        -0xff0001 -> :sswitch_7
        -0xa0a0b -> :sswitch_0
        -0x10000 -> :sswitch_2
        -0xff01 -> :sswitch_6
        -0x100 -> :sswitch_5
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_8
    .end sparse-switch
.end method

.method public getFontBGColor()I
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    return v0
.end method

.method public getFontBGOpacity()I
    .locals 1

    .prologue
    .line 350
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGOpacity:I

    return v0
.end method

.method public getFontColor()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontColor:I

    return v0
.end method

.method public getFontEdge()I
    .locals 1

    .prologue
    .line 330
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontEdge:I

    return v0
.end method

.method public getFontList()[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 444
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    if-eqz v2, :cond_0

    .line 445
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFontList() :: mFontList.getCount() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 459
    :cond_0
    return-object v1

    .line 450
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v2

    new-array v1, v2, [Ljava/lang/String;

    .line 452
    .local v1, "retString":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontName(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontOpacity()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontOpacity:I

    return v0
.end method

.method public getFontPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 362
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontSize:I

    return v0
.end method

.method public getFontStringName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedFont()Ljava/lang/String;
    .locals 4

    .prologue
    .line 483
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0145

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 485
    .local v0, "retStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    if-eqz v2, :cond_0

    .line 486
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 487
    .local v1, "tempString":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 488
    move-object v0, v1

    .line 492
    .end local v1    # "tempString":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getSelectedFontIndex()I
    .locals 6

    .prologue
    .line 463
    const/4 v1, 0x0

    .line 465
    .local v1, "retValue":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    if-eqz v3, :cond_2

    .line 466
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    .line 479
    .end local v1    # "retValue":I
    .local v2, "retValue":I
    :goto_0
    return v2

    .line 470
    .end local v2    # "retValue":I
    .restart local v1    # "retValue":I
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 471
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getTypefaceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 472
    move v1, v0

    .line 473
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSelectedFontIndex() :: return = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "i":I
    :cond_2
    move v2, v1

    .line 479
    .end local v1    # "retValue":I
    .restart local v2    # "retValue":I
    goto :goto_0

    .line 470
    .end local v2    # "retValue":I
    .restart local v0    # "i":I
    .restart local v1    # "retValue":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getSplitSubtitleActivation()Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSplitSubtitleActivation:Z

    return v0
.end method

.method public getSubtitleActivation()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleActivation:Z

    return v0
.end method

.method public getTextAlignment()I
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mTextAlignment:I

    return v0
.end method

.method public getTextAlignmentValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 367
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mTextAlignment:I

    packed-switch v0, :pswitch_data_0

    .line 373
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 369
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0096

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 371
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0105

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getTextEdgeValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 429
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontEdge:I

    packed-switch v0, :pswitch_data_0

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 431
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 433
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 435
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 437
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getTextSizeValue()Ljava/lang/String;
    .locals 2

    .prologue
    const v1, 0x7f0a00a1

    .line 416
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontSize:I

    sparse-switch v0, :sswitch_data_0

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 418
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 420
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 422
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0128

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 416
    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_2
        0x16 -> :sswitch_1
        0x1c -> :sswitch_0
    .end sparse-switch
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 4

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 259
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    if-eqz v1, :cond_0

    .line 260
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTypeface. fontName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", packageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 264
    :cond_0
    return-object v0
.end method

.method public initFontList()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->fontListInit()V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->loadTypefaces()V

    .line 498
    return-void
.end method

.method public initValues()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 95
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    const-string v2, "initValues E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_sec_captioning_enabled"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 99
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleActivation:Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_string"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_pakcagename"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_size"

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontSize:I

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_edge"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontEdge:I

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_text_alignment"

    const/16 v3, 0x1f

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mTextAlignment:I

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_opacity"

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontOpacity:I

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_color"

    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mWhite:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontColor:I

    .line 120
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-eqz v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_backgroundcolor"

    const/high16 v3, -0x1000000

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_background_opacity"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGOpacity:I

    .line 129
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_window_color"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinColor:I

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_window_opacity"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinOpacity:I

    .line 131
    return-void

    .line 101
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleActivation:Z
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleActivation:Z

    .line 104
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 124
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_backgroundcolor"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_background_opacity"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGOpacity:I

    goto :goto_1
.end method

.method public saveSubtitleSetting()V
    .locals 3

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    const-string v1, "saveSubtitleSetting E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_pakcagename"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_string"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_size"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontSize:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_edge"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontEdge:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_text_alignment"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mTextAlignment:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_color"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontColor:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_opacity"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontOpacity:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_backgroundcolor"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_background_opacity"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGOpacity:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_window_color"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinColor:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_window_opacity"

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinOpacity:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 155
    return-void
.end method

.method public setActivation(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const/4 v0, 0x1

    .line 159
    if-ne p1, v0, :cond_0

    .line 161
    .local v0, "active":I
    :goto_0
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitleActivation:Z

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_sec_captioning_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 163
    const/16 v1, 0x190

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 164
    return-void

    .line 159
    .end local v0    # "active":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCaptionWinColor(I)V
    .locals 2
    .param p1, "setColor"    # I

    .prologue
    .line 245
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinColor:I

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_window_color"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 247
    const/16 v0, 0x194

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 248
    return-void
.end method

.method public setCaptionWinOpacity(I)V
    .locals 2
    .param p1, "val"    # I

    .prologue
    .line 251
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mCaptionWinOpacity:I

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_window_opacity"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 253
    const/16 v0, 0x198

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 254
    return-void
.end method

.method public setCtx(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    .line 77
    new-instance v0, Lcom/sec/android/app/videoplayer/flipfont/FontList;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/flipfont/FontList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setWhite()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mWhite:I

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->initValues()V

    .line 81
    return-void
.end method

.method public setFont(Ljava/lang/String;)V
    .locals 4
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 195
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getTypefaceName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontPackageName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontList:Lcom/sec/android/app/videoplayer/flipfont/FontList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontStringName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_pakcagename"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "subtitle_font_string"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/16 v1, 0x19a

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 210
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 195
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setFontBGColor(I)V
    .locals 3
    .param p1, "setColor"    # I

    .prologue
    .line 231
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontBGColor. setColor : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGColor:I

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_backgroundcolor"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 234
    const/16 v0, 0x195

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 235
    return-void
.end method

.method public setFontBGOpacity(I)V
    .locals 3
    .param p1, "val"    # I

    .prologue
    .line 238
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontBGOpacity. val : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontBGOpacity:I

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_background_opacity"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 241
    const/16 v0, 0x197

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 242
    return-void
.end method

.method public setFontColor(I)V
    .locals 2
    .param p1, "setColor"    # I

    .prologue
    .line 219
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontColor:I

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_color"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 221
    const/16 v0, 0x193

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 222
    return-void
.end method

.method public setFontEdge(I)V
    .locals 2
    .param p1, "setEdge"    # I

    .prologue
    .line 213
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontEdge:I

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_edge"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 215
    const/16 v0, 0x191

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 216
    return-void
.end method

.method public setFontEdge(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontEdge()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontEdge(Landroid/widget/TextView;IZ)V

    .line 520
    return-void
.end method

.method public setFontEdge(Landroid/widget/TextView;IZ)V
    .locals 16
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "fontedge"    # I
    .param p3, "isMenuMode"    # Z

    .prologue
    .line 523
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFontEdge() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/high16 v5, -0x1000000

    .line 526
    .local v5, "shadowColor":I
    const/4 v14, -0x1

    .line 527
    .local v14, "shadowEffectColor":I
    move v10, v14

    .line 529
    .local v10, "effectColor":I
    new-instance v15, Landroid/util/TypedValue;

    invoke-direct {v15}, Landroid/util/TypedValue;-><init>()V

    .line 530
    .local v15, "shadowValue":Landroid/util/TypedValue;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080285

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v15, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 532
    new-instance v13, Landroid/util/TypedValue;

    invoke-direct {v13}, Landroid/util/TypedValue;-><init>()V

    .line 533
    .local v13, "glowValue":Landroid/util/TypedValue;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080284

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v13, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 535
    invoke-virtual {v15}, Landroid/util/TypedValue;->getFloat()F

    move-result v8

    .line 536
    .local v8, "shadowSize":F
    invoke-virtual {v13}, Landroid/util/TypedValue;->getFloat()F

    move-result v12

    .line 538
    .local v12, "glowSize":F
    if-eqz p3, :cond_0

    .line 539
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 540
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    .line 541
    move v10, v5

    .line 544
    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v14}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 545
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 547
    packed-switch p2, :pswitch_data_0

    .line 565
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->TAG:Ljava/lang/String;

    const-string v2, "setFontEdge : Why am I Here???"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->stackTrace()V

    .line 569
    :goto_0
    return-void

    .line 549
    :pswitch_0
    const/high16 v2, 0x43070000    # 135.0f

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v3, v8, v1

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    goto :goto_0

    .line 552
    :pswitch_1
    const/high16 v7, -0x3dcc0000    # -45.0f

    const/high16 v9, 0x40800000    # 4.0f

    const/high16 v11, 0x3f800000    # 1.0f

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    goto :goto_0

    .line 555
    :pswitch_2
    const/high16 v7, 0x43070000    # 135.0f

    const/high16 v9, 0x40800000    # 4.0f

    const/high16 v11, 0x3f800000    # 1.0f

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    goto :goto_0

    .line 558
    :pswitch_3
    const v1, 0x3f4ccccd    # 0.8f

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v14, v1}, Landroid/widget/TextView;->addOuterGlowTextEffect(FIF)I

    goto :goto_0

    .line 561
    :pswitch_4
    const/high16 v2, 0x43070000    # 135.0f

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v3, v8, v1

    const/high16 v4, 0x40a00000    # 5.0f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    goto :goto_0

    .line 547
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setFontOpacity(I)V
    .locals 2
    .param p1, "val"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontOpacity:I

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_opacity"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 227
    const/16 v0, 0x196

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 228
    return-void
.end method

.method public setFontPackageName(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontPackageName:Ljava/lang/String;

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_pakcagename"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public setFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontSize:I

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_size"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 180
    const/16 v0, 0x199

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 181
    return-void
.end method

.method public setFontStringName(Ljava/lang/String;)V
    .locals 2
    .param p1, "fontStringName"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mFontStringName:Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_font_string"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public setSplitSubtitleActivation(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSplitSubtitleActivation:Z

    .line 168
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 169
    return-void
.end method

.method public setSubtitlePrefDataChangedListener(Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;

    .prologue
    .line 515
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mSubtitlePrefChangeListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;

    .line 516
    return-void
.end method

.method public setTextAlignment(I)V
    .locals 2
    .param p1, "alignment"    # I

    .prologue
    .line 172
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->mTextAlignment:I

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->PrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "subtitle_text_alignment"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 174
    const/16 v0, 0x192

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->sendDataChangeNotify(I)V

    .line 175
    return-void
.end method
