.class Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;
.super Ljava/lang/Object;
.source "VideoSmartFaceMgr.java"

# interfaces
.implements Lcom/samsung/android/smartface/SmartFaceManager$SmartFaceInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->runSmartPause()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Lcom/samsung/android/smartface/FaceInfo;I)V
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/smartface/FaceInfo;
    .param p2, "service_type"    # I

    .prologue
    .line 110
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 111
    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPrevNeedtoPause:I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$200()I

    move-result v0

    iget v1, p1, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    if-eq v0, v1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$300(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "needToPause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget v0, p1, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    # setter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPrevNeedtoPause:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$202(I)I

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # invokes: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->isBlockSmartPause()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$400(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget v0, p1, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    packed-switch v0, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 118
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPausedbySmartPause:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$600()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPausedbySmartPause:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$602(Z)Z

    .line 120
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const-string v1, "com.sec.android.app.videoplayer"

    const-string v2, "VTSP"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    goto :goto_0

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const-string v1, "com.sec.android.app.videoplayer"

    const-string v2, "VTSP"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/4 v0, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPausedbySmartPause:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$602(Z)Z

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseOrStopPlaying()V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;->this$0:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    # getter for: Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->access$700(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.show.controller.smartpause"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
