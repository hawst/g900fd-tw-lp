.class Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;
.super Ljava/lang/Object;
.source "VideoFlickView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 127
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$400(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationleft:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$500(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickCounter:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$102(Lcom/sec/android/app/videoplayer/view/VideoFlickView;I)I

    .line 116
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 124
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 110
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 130
    return-void
.end method
