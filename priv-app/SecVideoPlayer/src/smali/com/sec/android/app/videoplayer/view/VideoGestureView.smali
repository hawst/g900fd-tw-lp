.class public Lcom/sec/android/app/videoplayer/view/VideoGestureView;
.super Ljava/lang/Object;
.source "VideoGestureView.java"


# static fields
.field private static final EAR_SHOCK_VOLUME_VALUE:I = 0xa

.field private static final TAG:Ljava/lang/String; = "VideoGestureView"


# instance fields
.field private GESTURE_MAX_PROGRESS:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

.field private mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

.field private mBrightnessImage:Landroid/widget/ImageView;

.field private mBrightnessPopup:Landroid/widget/PopupWindow;

.field private final mBrightnessRes:[I

.field private mBrightnessSeekBar:Landroid/widget/SeekBar;

.field private mBrightnessText:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mGestureView:Landroid/view/View;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mVolumeGestureLayout:Landroid/widget/RelativeLayout;

.field private mVolumeImage:Landroid/widget/ImageView;

.field private mVolumePopup:Landroid/widget/PopupWindow;

.field private mVolumeSeekBar:Landroid/widget/SeekBar;

.field private mVolumeText:Landroid/widget/TextView;

.field private mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mWindow:Landroid/view/Window;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    .line 58
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessRes:[I

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 73
    const-string v0, "VideoGestureView"

    const-string v1, "VideoGestureView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 78
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 79
    return-void

    .line 58
    nop

    :array_0
    .array-data 4
        0x7f0201f7
        0x7f0201f8
        0x7f0201f9
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f0201fe
        0x7f0201ff
        0x7f020200
        0x7f020201
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Window;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "window"    # Landroid/view/Window;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mWindow:Landroid/view/Window;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    .line 58
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessRes:[I

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 82
    const-string v0, "VideoGestureView"

    const-string v1, "VideoGestureView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    .line 84
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mWindow:Landroid/view/Window;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 87
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;-><init>(Landroid/content/Context;Landroid/view/Window;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 88
    return-void

    .line 58
    nop

    :array_0
    .array-data 4
        0x7f0201f7
        0x7f0201f8
        0x7f0201f9
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f0201fe
        0x7f0201ff
        0x7f020200
        0x7f020201
    .end array-data
.end method

.method private initGestureView()V
    .locals 4

    .prologue
    const v3, 0x1030004

    const/4 v2, 0x3

    .line 228
    const-string v0, "VideoGestureView"

    const-string v1, "initGestureView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 230
    const-string v0, "VideoGestureView"

    const-string v1, "initGestureView : mGestureView is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 237
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 252
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0171

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0175

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setAudioShockWarningEnabled()V

    goto/16 :goto_0
.end method

.method private setBrightnessBarGone()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 375
    const-string v0, "VideoGestureView"

    const-string v1, "setBrightnessBarGone"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 388
    :cond_1
    return-void
.end method

.method private setBrightnessBarUI(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 290
    const-string v0, "VideoGestureView"

    const-string v1, "setBrightnessBar"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-nez v0, :cond_0

    .line 292
    const-string v0, "VideoGestureView"

    const-string v1, "setBrightnessBar : mVideoAudioUtil is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :goto_0
    return-void

    .line 296
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    if-le p1, v0, :cond_2

    .line 297
    iget p1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 301
    :cond_1
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessText(I)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 298
    :cond_2
    if-gez p1, :cond_1

    .line 299
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private setBrightnessBarVisible()V
    .locals 9

    .prologue
    const v7, 0x7f0d0168

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 328
    const-string v4, "VideoGestureView"

    const-string v6, "setBrightnessBarVisible"

    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 331
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 333
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setVolumeBarGone()V

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 336
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0169

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 339
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d016a

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 340
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d016b

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 341
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d016c

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 344
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v4, :cond_3

    .line 345
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 346
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 347
    .local v2, "paramBrightness":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v6, v7

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 348
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 349
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 350
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 352
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-ne v4, v8, :cond_4

    move v1, v5

    .line 354
    .local v1, "marginTop":I
    :goto_0
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-ne v4, v8, :cond_5

    const/16 v0, 0x13

    .line 356
    .local v0, "gravity":I
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 357
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 358
    .local v3, "windowHeight":I
    iget v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-ge v3, v4, :cond_2

    .line 359
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v8, :cond_2

    .line 360
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTopLocationMultiwindow()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 361
    const/16 v0, 0x33

    .line 365
    :goto_2
    const/4 v1, 0x0

    .line 370
    .end local v3    # "windowHeight":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6, v0, v5, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 372
    .end local v0    # "gravity":I
    .end local v1    # "marginTop":I
    .end local v2    # "paramBrightness":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    return-void

    .line 352
    .restart local v2    # "paramBrightness":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0801a4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    goto :goto_0

    .line 354
    .restart local v1    # "marginTop":I
    :cond_5
    const/16 v0, 0x33

    goto :goto_1

    .line 363
    .restart local v0    # "gravity":I
    .restart local v3    # "windowHeight":I
    :cond_6
    const/16 v0, 0x53

    goto :goto_2
.end method

.method private setBrightnessText(I)V
    .locals 8
    .param p1, "level"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 306
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    int-to-float v3, p1

    iget v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 312
    .local v2, "percent":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 313
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_2

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    const-string v4, "%d"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->brightnessValueLocale()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    :cond_2
    :goto_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->updateBrightnessIcon(I)V

    goto :goto_0

    .line 317
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 318
    .local v0, "mFormatBuilder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 319
    .local v1, "mFormatter":Ljava/util/Formatter;
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 320
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    const-string v4, "%d"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v1, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    invoke-virtual {v1}, Ljava/util/Formatter;->close()V

    goto :goto_1
.end method

.method private setVolumeBarGone()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 444
    const-string v0, "VideoGestureView"

    const-string v1, "setVolumeBarGone"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0170

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0171

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0174

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0175

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0172

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 459
    :cond_1
    return-void
.end method

.method private setVolumeBarVisible()V
    .locals 8

    .prologue
    const v7, 0x7f0d016f

    const/4 v5, 0x0

    .line 391
    const-string v4, "VideoGestureView"

    const-string v6, "setVolumeBarVisible"

    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->dismissVolumePanel()V

    .line 394
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 395
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 397
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessBarGone()V

    .line 398
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 400
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 401
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 402
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0170

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0171

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 404
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0174

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0175

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 406
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0172

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 407
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v6, 0x7f0d0173

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 410
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v4, :cond_3

    .line 411
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 412
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 413
    .local v2, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    add-int/2addr v6, v7

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 414
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 415
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 416
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 418
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v4, :cond_4

    move v1, v5

    .line 419
    .local v1, "marginTop":I
    :goto_0
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v4, :cond_5

    const/16 v0, 0x15

    .line 421
    .local v0, "gravity":I
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 422
    const/4 v3, 0x0

    .line 423
    .local v3, "windowHeight":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mWindow:Landroid/view/Window;

    if-eqz v4, :cond_6

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 428
    :goto_2
    iget v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-ge v3, v4, :cond_2

    .line 429
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x1

    if-ne v4, v6, :cond_2

    .line 430
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTopLocationMultiwindow()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 431
    const/16 v0, 0x35

    .line 435
    :goto_3
    const/4 v1, 0x0

    .line 439
    .end local v3    # "windowHeight":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6, v0, v5, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 441
    .end local v0    # "gravity":I
    .end local v1    # "marginTop":I
    .end local v2    # "rp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    return-void

    .line 418
    .restart local v2    # "rp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0801a4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    goto :goto_0

    .line 419
    .restart local v1    # "marginTop":I
    :cond_5
    const/16 v0, 0x35

    goto :goto_1

    .line 426
    .restart local v0    # "gravity":I
    .restart local v3    # "windowHeight":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v3

    goto :goto_2

    .line 433
    :cond_7
    const/16 v0, 0x55

    goto :goto_3
.end method

.method private setVolumeUI(II)V
    .locals 10
    .param p1, "vol"    # I
    .param p2, "level"    # I

    .prologue
    const/4 v9, 0x0

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .local v1, "mFormatBuilder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v2, v1, v5}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 143
    .local v2, "mFormatter":Ljava/util/Formatter;
    const-string v5, "VideoGestureView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setVolumeUI : currentLevel = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " vol = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 147
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getMaxVolume()I

    move-result v3

    .line 148
    .local v3, "maxVol":I
    if-gez p1, :cond_3

    .line 149
    const/4 p1, 0x0

    .line 154
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_1

    .line 155
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 156
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 159
    :cond_1
    if-nez p1, :cond_4

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0201f6

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 165
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 166
    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setVolume(I)V

    .line 167
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 169
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v4

    .line 170
    .local v4, "volume":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    const-string v6, "%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v2, v6, v7}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    .line 173
    if-gt p1, v4, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v5, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 175
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setVolumeBarVisible()V

    .line 176
    return-void

    .line 150
    .end local v4    # "volume":I
    :cond_3
    if-le p1, v3, :cond_0

    .line 151
    move p1, v3

    goto :goto_0

    .line 162
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020202

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method private updateBrightnessIcon(I)V
    .locals 4
    .param p1, "percent"    # I

    .prologue
    .line 469
    const-string v1, "VideoGestureView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateBrightnessIcon :: percent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 471
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 473
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 476
    :cond_0
    div-int/lit8 v0, p1, 0xa

    .line 478
    .local v0, "id":I
    if-gez v0, :cond_3

    .line 479
    const/4 v0, 0x0

    .line 483
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessRes:[I

    aget v2, v2, v0

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 484
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 486
    .end local v0    # "id":I
    :cond_2
    return-void

    .line 480
    .restart local v0    # "id":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessRes:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_4

    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 481
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessRes:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 91
    const-string v1, "VideoGestureView"

    const-string v2, "addViewTo"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 93
    const-string v1, "VideoGestureView"

    const-string v2, "addViewTo : context is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .end local p1    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 97
    .restart local p1    # "view":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 98
    .local v0, "inflate":Landroid/view/LayoutInflater;
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 99
    const v1, 0x7f030026

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->initGestureView()V

    goto :goto_0
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setVolumeBarGone()V

    .line 224
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessBarGone()V

    .line 225
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 464
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 465
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 466
    return-void
.end method

.method public setAudioShockWarningEnabled()V
    .locals 3

    .prologue
    .line 273
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_1

    .line 274
    const/4 v0, -0x1

    .line 276
    .local v0, "value":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioShockWarningEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    div-int/lit8 v1, v1, 0xf

    invoke-static {}, Landroid/media/AudioManager;->getEarProtectLimitIndex()I

    move-result v2

    mul-int v0, v1, v2

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOverlapPointForDualColor(I)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->invalidate()V

    .line 283
    .end local v0    # "value":I
    :cond_1
    return-void
.end method

.method public setBrightness(I)V
    .locals 6
    .param p1, "level"    # I

    .prologue
    .line 192
    const-string v3, "VideoGestureView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setBrightness E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-nez v3, :cond_0

    .line 207
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 198
    .local v0, "currentLevel":I
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessBarVisible()V

    .line 200
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 201
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v1

    .line 202
    .local v1, "maxVol":I
    add-int/2addr v0, p1

    .line 203
    int-to-float v3, v1

    int-to-float v4, v0

    iget v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float v2, v3, v4

    .line 204
    .local v2, "val":F
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    float-to-int v4, v2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setBrightness(I)I

    .line 206
    .end local v1    # "maxVol":I
    .end local v2    # "val":F
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessBarUI(I)V

    goto :goto_0
.end method

.method public setVolume(I)V
    .locals 7
    .param p1, "level"    # I

    .prologue
    .line 118
    const-string v4, "VideoGestureView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setVolume : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 122
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeSame()V

    .line 136
    :goto_0
    return-void

    .line 127
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v4

    int-to-float v3, v4

    .line 128
    .local v3, "vol":F
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getMaxVolume()I

    move-result v2

    .line 129
    .local v2, "maxVol":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 131
    .local v1, "currentLevel":I
    add-int/2addr v1, p1

    .line 133
    int-to-float v4, v2

    int-to-float v5, v1

    iget v6, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float v3, v4, v5

    .line 135
    float-to-int v4, v3

    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setVolumeUI(II)V

    goto :goto_0
.end method

.method public showBrightness()V
    .locals 6

    .prologue
    .line 179
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    .line 180
    .local v1, "level":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v2

    .line 181
    .local v2, "maxLevel":I
    int-to-float v3, v1

    iget v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    mul-float v0, v3, v4

    .line 183
    .local v0, "currentLevel":F
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->syncBrightnessWithSystemLevel()V

    .line 187
    :cond_0
    float-to-int v3, v0

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessBarUI(I)V

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightnessBarVisible()V

    .line 189
    return-void
.end method

.method public showVolume()V
    .locals 6

    .prologue
    .line 105
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 106
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeSame()V

    .line 115
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v3

    .line 111
    .local v3, "vol":I
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getMaxVolume()I

    move-result v2

    .line 112
    .local v2, "maxVol":I
    int-to-float v4, v3

    iget v5, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    int-to-float v5, v2

    div-float v1, v4, v5

    .line 114
    .local v1, "currentLevel":F
    float-to-int v4, v1

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setVolumeUI(II)V

    goto :goto_0
.end method

.method public syncBrightnessWithSystemLevel()V
    .locals 2

    .prologue
    .line 210
    const-string v0, "VideoGestureView"

    const-string v1, "setSystemBrightness"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-nez v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setSystemBrightnessLevel()V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->resetWindowBrightness()V

    goto :goto_0
.end method
