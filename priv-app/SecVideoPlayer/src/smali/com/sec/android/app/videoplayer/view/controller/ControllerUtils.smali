.class public Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
.super Ljava/lang/Object;
.source "ControllerUtils.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "ControllerUtils"


# instance fields
.field private mAppinAppButton:Landroid/widget/ImageButton;

.field private mAppinAppButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mContext:Landroid/content/Context;

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private mRotationBtn:Landroid/widget/ImageButton;

.field private mRotationBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mRotationBtnTouchListener:Landroid/view/View$OnTouchListener;

.field mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

.field mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 100
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 133
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 254
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    .line 47
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 48
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 49
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateScreen()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateSecondScreen()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->createTalkBackOffDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->openPopupPlayer()V

    return-void
.end method

.method private createTalkBackOffDialog()V
    .locals 4

    .prologue
    .line 305
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_0

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v3, "ControllerUtils"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 309
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 310
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0148

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 312
    const v2, 0x7f0a00dd

    new-instance v3, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$5;-><init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 321
    const v2, 0x7f0a0026

    new-instance v3, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$6;-><init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 327
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 328
    .local v1, "dialog":Landroid/app/AlertDialog;
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 329
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 330
    return-void
.end method

.method private isPhoneView(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 459
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 460
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0148

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    :cond_0
    const/4 v0, 0x1

    .line 466
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openPopupPlayer()V
    .locals 2

    .prologue
    .line 251
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->openPopupPlayer(Landroid/content/Context;)V

    .line 252
    return-void
.end method

.method private toggleRotateScreen()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 188
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getOrientation()I

    move-result v0

    .line 191
    .local v0, "orientation":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 192
    const/16 v1, 0x46

    if-le v0, v1, :cond_1

    const/16 v1, 0x6e

    if-ge v0, v1, :cond_1

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    .line 205
    .end local v0    # "orientation":I
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setUserOrientation(I)V

    .line 207
    :cond_0
    return-void

    .line 195
    .restart local v0    # "orientation":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto :goto_0

    .line 198
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto :goto_0

    .line 201
    .end local v0    # "orientation":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v4

    if-ne v4, v3, :cond_4

    :goto_1
    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method private toggleRotateSecondScreen()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSecondScreenOrientation()I

    move-result v2

    if-ne v2, v1, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateSecondScreen(I)V

    .line 214
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSecondScreenOrientation()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSecondScreenUserOrientation(I)V

    .line 216
    :cond_1
    return-void
.end method

.method private updateSecondScreenAutoRotationBtn()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 89
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGuidedTour()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 98
    :cond_2
    :goto_0
    return-void

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public canCaptureVideoFrame()Z
    .locals 1

    .prologue
    .line 385
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->canCaptureVideoFrame()Z

    move-result v0

    return v0
.end method

.method public captureVideo()V
    .locals 2

    .prologue
    .line 389
    const-string v0, "ControllerUtils"

    const-string v1, "captureVideo E"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getBtnInstance()Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getBtnInstance()Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isProgressDraging()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    const-string v0, "ControllerUtils"

    const-string v1, "captureVideo - Progress bar is Draging; we cannot capture image"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getVideoCaptureView()Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    move-result-object v0

    if-nez v0, :cond_2

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachVideoCaptureView()V

    .line 404
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getVideoCaptureView()Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getVideoCaptureView()Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->captureVideo()V

    goto :goto_0
.end method

.method public commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0xbb8

    .line 333
    const/4 v0, 0x0

    .line 334
    .local v0, "retVal":Z
    packed-switch p1, :pswitch_data_0

    .line 358
    :goto_0
    return v0

    .line 339
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 351
    :goto_1
    const/4 v0, 0x1

    .line 352
    goto :goto_0

    .line 341
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 345
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 334
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 339
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getAutoRotationBtnVisibility()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getPopupPlayerBtn()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    .line 247
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCaptureModeOn()Z
    .locals 3

    .prologue
    .line 379
    const-string v1, "ControllerUtils"

    const-string v2, "isCaptureModeOn"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getCaptureVisibility()I

    move-result v0

    .line 381
    .local v0, "captureMode":I
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->ON:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v12, 0x35

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 431
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    if-nez v8, :cond_1

    .line 455
    :cond_0
    :goto_0
    return v10

    .line 432
    :cond_1
    const/4 v8, 0x2

    new-array v4, v8, [I

    .line 433
    .local v4, "screenPos":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 434
    .local v1, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 435
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 437
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    .line 438
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 439
    .local v2, "height":I
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v5, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 440
    .local v5, "screenWidth":I
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v3, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 442
    .local v3, "screenHeight":I
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 443
    .local v6, "viewStr":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v8, v6, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 445
    .local v0, "cheatSheet":Landroid/widget/Toast;
    aget v8, v4, v10

    add-int/2addr v8, v2

    iget v9, v1, Landroid/graphics/Rect;->bottom:I

    if-ge v8, v9, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->isPhoneView(Landroid/view/View;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 447
    aget v8, v4, v11

    sub-int v8, v5, v8

    div-int/lit8 v9, v7, 0x2

    sub-int/2addr v8, v9

    aget v9, v4, v10

    add-int/2addr v9, v2

    invoke-virtual {v0, v12, v8, v9}, Landroid/widget/Toast;->setGravity(III)V

    .line 454
    :goto_1
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 448
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->isPhoneView(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 449
    aget v8, v4, v11

    sub-int v8, v5, v8

    div-int/lit8 v9, v7, 0x2

    sub-int/2addr v8, v9

    aget v9, v4, v10

    sub-int/2addr v9, v2

    invoke-virtual {v0, v12, v8, v9}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1

    .line 452
    :cond_3
    const/16 v8, 0x51

    iget v9, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v9, v3, v9

    add-int/2addr v9, v2

    invoke-virtual {v0, v8, v11, v9}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1
.end method

.method public setPopupPlayerBtn(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const v1, 0x7f0d0138

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 220
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    .line 222
    :cond_0
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    .line 223
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0148

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 241
    :cond_1
    return-void
.end method

.method public setRotateBtn(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    .line 56
    :cond_0
    const v0, 0x7f0d010d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 65
    :cond_1
    return-void
.end method

.method public updateAutoRotationBtn()V
    .locals 2

    .prologue
    .line 68
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderOpen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateSecondScreenAutoRotationBtn()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGuidedTour()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 75
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 77
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mRotationBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateOneFrameForwardBackwardBtn(Landroid/view/View;)V
    .locals 6
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0d012e

    const v4, 0x7f0d012c

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 412
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_TICK_PLAYBACK:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->isCaptureModeOn()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->canCaptureVideoFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getOneFrameSeekEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isLongSeekMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 417
    const v0, 0x7f0d012b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 418
    const v0, 0x7f0d012d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 420
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 421
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 424
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public updatePopupPlayerBtn(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 362
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    if-nez v2, :cond_0

    .line 376
    :goto_0
    return-void

    .line 365
    :cond_0
    const/16 v1, 0x8

    .line 367
    .local v1, "visibility":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v2

    if-nez v2, :cond_1

    .line 368
    const/4 v1, 0x0

    .line 370
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->disableAppInAppBtn()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v0, 0x1

    .line 371
    .local v0, "enabled":Z
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->invalidate()V

    .line 375
    .end local v0    # "enabled":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mAppinAppButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 370
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 372
    .restart local v0    # "enabled":Z
    :cond_3
    const v2, 0x3ecccccd    # 0.4f

    goto :goto_2
.end method
