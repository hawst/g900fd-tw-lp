.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1630
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private processError(I)I
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    const/16 v0, 0x2714

    .line 1651
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>>>>>>> MediaPlayer Error :: processError = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <<<<<<<<<<<<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    sparse-switch p1, :sswitch_data_0

    move p1, v0

    .line 1678
    .end local p1    # "errorCode":I
    :goto_0
    :sswitch_0
    return p1

    .line 1654
    .restart local p1    # "errorCode":I
    :sswitch_1
    const-string v1, "MiniVideoPlayerService"

    const-string v2, ">>>>>>>>MEDIA_ERROR_SERVER_DIED<<<<<<<<<<<<"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v0

    .line 1655
    goto :goto_0

    .line 1658
    :sswitch_2
    const-string v1, "MiniVideoPlayerService"

    const-string v2, ">>>>>>>> MEDIA_ERROR_ACCESS_TOKEN_EXPIRED <<<<<<<<<<<<"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v0

    .line 1659
    goto :goto_0

    .line 1652
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7fffbffa -> :sswitch_0
        -0x7fffbff9 -> :sswitch_0
        -0x17d4 -> :sswitch_0
        -0x177d -> :sswitch_0
        -0x177c -> :sswitch_0
        -0x177b -> :sswitch_0
        -0x3fc -> :sswitch_2
        -0x3f2 -> :sswitch_0
        -0x3ef -> :sswitch_0
        -0x3ed -> :sswitch_0
        -0x63 -> :sswitch_0
        -0x62 -> :sswitch_0
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1, "arg0"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/16 v6, 0x2714

    const/4 v5, 0x0

    .line 1632
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mErrorListener() - error:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " detail:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1634
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;->processError(I)I

    move-result v0

    .line 1635
    .local v0, "errorState":I
    if-ne v0, v6, :cond_0

    .line 1636
    invoke-direct {p0, p3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;->processError(I)I

    move-result v0

    .line 1638
    :cond_0
    if-ne v0, v6, :cond_1

    .line 1639
    const-string v2, "MiniVideoPlayerService"

    const-string v3, ">>>>>>>> PLAYBACK_UNKNOWNERROR <<<<<<<<<<<<"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getErrorStringResID(I)I

    move-result v1

    .line 1642
    .local v1, "resID":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1643
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1645
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->StopSelf()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 1647
    return v5
.end method
