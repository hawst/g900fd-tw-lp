.class public Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
.super Ljava/lang/Object;
.source "VideoAudioUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$1;,
        Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;
    }
.end annotation


# static fields
.field private static final PLAYBACK_TYPE_LOCAL:I = 0x0

.field private static final PLAYBACK_TYPE_REMOTE:I = 0x1

.field public static final SOUNDALIVE_PRESET_MOVIE:I = 0xf

.field public static final SOUNDALIVE_PRESET_NORMAL:I = 0x0

.field public static final SOUNDALIVE_PRESET_VIDEO_AUTO:I = 0xe

.field public static final SOUNDALIVE_PRESET_VIDEO_MUSIC:I = 0xc

.field public static final SOUNDALIVE_PRESET_VIDEO_NORMAL:I = 0xa

.field public static final SOUNDALIVE_PRESET_VIDEO_VIRT71:I = 0xd

.field public static final SOUNDALIVE_PRESET_VIDEO_VOICE:I = 0xb

.field public static final SOUNDALIVE_PRESET_VIRT71:I = 0x2

.field public static final SOUNDALIVE_PRESET_VOICE:I = 0xe

.field private static final TAG:Ljava/lang/String;

.field private static sAudioUtil:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;


# instance fields
.field private iRemoteControlReceiversregistered:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallback:Landroid/media/session/MediaSession$Callback;

.field private mContext:Landroid/content/Context;

.field private mMediaSession:Landroid/media/session/MediaSession;

.field private mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

.field protected mPlaybackState:Landroid/media/session/PlaybackState;

.field private mRouteTypes:I

.field private mRouter:Landroid/media/MediaRouter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sAudioUtil:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->iRemoteControlReceiversregistered:Z

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mCallback:Landroid/media/session/MediaSession$Callback;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    .line 73
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState()V

    return-void
.end method

.method private createMediaSession()V
    .locals 4

    .prologue
    .line 172
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v1, "createMediaSession E."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 175
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v1, "createMediaSession mMediaSession is Null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    new-instance v0, Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    const-string v2, "com.samsung.android.video.VideoApplication"

    invoke-direct {v0, v1, v2}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    .line 178
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;-><init>(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$1;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mCallback:Landroid/media/session/MediaSession$Callback;

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mCallback:Landroid/media/session/MediaSession$Callback;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;)V

    .line 181
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v0}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    const-wide/16 v2, 0x27f

    invoke-virtual {v0, v2, v3}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState()V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 192
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sAudioUtil:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sAudioUtil:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    .line 79
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sAudioUtil:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    return-object v0
.end method

.method private getPlaybackState(I)I
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v2, 0x1

    .line 709
    const/4 v0, 0x3

    .line 710
    .local v0, "btstate":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 711
    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 712
    const/4 v0, 0x2

    .line 723
    :cond_0
    :goto_0
    return v0

    .line 713
    :cond_1
    if-ne p1, v2, :cond_0

    .line 714
    const/4 v0, 0x1

    goto :goto_0

    .line 717
    :cond_2
    if-ne p1, v2, :cond_3

    .line 718
    const/4 v0, 0x2

    goto :goto_0

    .line 719
    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 720
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isAllSoundOff(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 435
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "all_sound_off"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private releaseMediaSession()V
    .locals 2

    .prologue
    .line 195
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v1, "releaseMediaSession E."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v0, :cond_0

    .line 197
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v1, "releaseMediaSession mMediaSession is NOT Null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    .line 201
    :cond_0
    return-void
.end method

.method private setPlaybackState()V
    .locals 9

    .prologue
    .line 265
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v8

    .line 266
    .local v8, "state":I
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getPlaybackState(I)I

    move-result v1

    .line 267
    .local v1, "btstate":I
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPlaybackState PlaybackState:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    .line 269
    .local v7, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-direct {v0, v2}, Landroid/media/session/PlaybackState$Builder;-><init>(Landroid/media/session/PlaybackState;)V

    .line 270
    .local v0, "stateBuilder":Landroid/media/session/PlaybackState$Builder;
    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    invoke-virtual/range {v0 .. v6}, Landroid/media/session/PlaybackState$Builder;->setState(IJFJ)Landroid/media/session/PlaybackState$Builder;

    .line 271
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/session/PlaybackState$Builder;->setErrorMessage(Ljava/lang/CharSequence;)Landroid/media/session/PlaybackState$Builder;

    .line 272
    invoke-virtual {v0}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    .line 273
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v2, :cond_0

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-virtual {v2, v3}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 276
    :cond_0
    return-void
.end method

.method private setPlaybackState(J)V
    .locals 9
    .param p1, "timeInMs"    # J

    .prologue
    .line 279
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v7

    .line 280
    .local v7, "state":I
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getPlaybackState(I)I

    move-result v1

    .line 281
    .local v1, "btstate":I
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPlaybackState PlaybackState:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-direct {v0, v2}, Landroid/media/session/PlaybackState$Builder;-><init>(Landroid/media/session/PlaybackState;)V

    .line 283
    .local v0, "stateBuilder":Landroid/media/session/PlaybackState$Builder;
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-wide v2, p1

    invoke-virtual/range {v0 .. v6}, Landroid/media/session/PlaybackState$Builder;->setState(IJFJ)Landroid/media/session/PlaybackState$Builder;

    .line 284
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/session/PlaybackState$Builder;->setErrorMessage(Ljava/lang/CharSequence;)Landroid/media/session/PlaybackState$Builder;

    .line 285
    invoke-virtual {v0}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v2, :cond_0

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-virtual {v2, v3}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 289
    :cond_0
    return-void
.end method

.method private setPlaybackState(JZ)V
    .locals 7
    .param p1, "timeInMs"    # J
    .param p3, "isPlaying"    # Z

    .prologue
    .line 292
    const/4 v1, 0x3

    .line 293
    .local v1, "btstate":I
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPlaybackState isPlaying:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    if-nez p3, :cond_0

    .line 295
    const/4 v1, 0x2

    .line 298
    :cond_0
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPlaybackState PlaybackState:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-direct {v0, v2}, Landroid/media/session/PlaybackState$Builder;-><init>(Landroid/media/session/PlaybackState;)V

    .line 300
    .local v0, "stateBuilder":Landroid/media/session/PlaybackState$Builder;
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-wide v2, p1

    invoke-virtual/range {v0 .. v6}, Landroid/media/session/PlaybackState$Builder;->setState(IJFJ)Landroid/media/session/PlaybackState$Builder;

    .line 301
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/session/PlaybackState$Builder;->setErrorMessage(Ljava/lang/CharSequence;)Landroid/media/session/PlaybackState$Builder;

    .line 302
    invoke-virtual {v0}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v2, :cond_1

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mPlaybackState:Landroid/media/session/PlaybackState;

    invoke-virtual {v2, v3}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 306
    :cond_1
    return-void
.end method


# virtual methods
.method public abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    .locals 1
    .param p1, "listener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 325
    const/4 v0, 0x0

    .line 327
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    goto :goto_0
.end method

.method public dismissVolumePanel()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->dismissVolumePanel()V

    .line 352
    :cond_0
    return-void
.end method

.method public gainAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    .locals 3
    .param p1, "listener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 318
    const/4 v0, 0x0

    .line 320
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentVolume()I
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getImplicitVideoVolume()F
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=7;device=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getMaxVolume()I
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getRingerMode()I
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    .line 338
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    goto :goto_0
.end method

.method public initMetaData()V
    .locals 2

    .prologue
    .line 218
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v1, "initMetaData. "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    new-instance v0, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v0}, Landroid/media/MediaMetadata$Builder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    .line 220
    return-void
.end method

.method public isAudioPathBT()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 359
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v2, :cond_1

    .line 369
    :cond_0
    :goto_0
    return v0

    .line 361
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 364
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 366
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit16 v2, v2, 0x380

    if-nez v2, :cond_2

    .line 369
    .local v0, "isBt":Z
    :goto_1
    goto :goto_0

    .line 366
    .end local v0    # "isBt":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isAudioPathBTModeSCO()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 387
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v2, :cond_1

    .line 396
    :cond_0
    :goto_0
    return v0

    .line 389
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 392
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 394
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x20

    if-nez v2, :cond_2

    .line 396
    .local v0, "isBt":Z
    :goto_1
    goto :goto_0

    .line 394
    .end local v0    # "isBt":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isAudioPathEarjack()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v2, :cond_1

    .line 383
    :cond_0
    :goto_0
    return v0

    .line 375
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 380
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v2, v2, 0xc

    if-nez v2, :cond_2

    .line 383
    .local v0, "isEarjack":Z
    :goto_1
    goto :goto_0

    .line 380
    .end local v0    # "isEarjack":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isAudioShockWarningEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    .line 442
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathEarjack()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isExtraSpeakerDockOn()Z
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isExtraSpeakerDockOn()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHDMIConnected()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isHdmiConnected()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMusicActive()Z
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPauseWorkByMotion()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v3, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v1

    .line 141
    :cond_1
    const/4 v0, 0x1

    .line 142
    .local v0, "bRet":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v4, "audioParam;outDevice"

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 144
    .local v2, "path":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 148
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/lit16 v3, v3, 0x180

    if-nez v3, :cond_4

    .line 151
    .local v1, "isBt":Z
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v1, :cond_3

    .line 152
    :cond_2
    const/4 v0, 0x0

    :cond_3
    move v1, v0

    .line 155
    goto :goto_0

    .line 148
    .end local v1    # "isBt":Z
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public isRecordActive()Z
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRingerModeNormal()Z
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWiredConnected()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerRemoteControlReceiver()V
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->iRemoteControlReceiversregistered:Z

    if-nez v0, :cond_0

    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->createMediaSession()V

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->iRemoteControlReceiversregistered:Z

    .line 208
    :cond_0
    return-void
.end method

.method public selectRouteInt(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    .line 407
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    if-nez v4, :cond_0

    .line 408
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v5, "selectRouteInt : mRouter is NULL!!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :goto_0
    return-void

    .line 412
    :cond_0
    const/4 v3, 0x0

    .line 414
    .local v3, "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    if-nez p1, :cond_2

    .line 415
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v3

    .line 427
    :cond_1
    if-eqz v3, :cond_4

    .line 428
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouteTypes:I

    invoke-virtual {v4, v5, v3}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    .line 417
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v0

    .line 418
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 419
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4, v1}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v2

    .line 420
    .local v2, "route":Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouteTypes:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v4

    if-eq v2, v4, :cond_3

    .line 421
    move-object v3, v2

    .line 422
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "selectRouteInt : Route Selected! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 430
    .end local v0    # "N":I
    .end local v1    # "i":I
    .end local v2    # "route":Landroid/media/MediaRouter$RouteInfo;
    :cond_4
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v5, "selectRouteInt : routeInfo is NULL!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendMediaButtonCommand(Ljava/lang/String;)V
    .locals 4
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 446
    if-eqz p1, :cond_0

    .line 447
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 448
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    const-string v1, "command"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 451
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMediaButtonCommand - command:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 453
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v2, "sendMediaButtonCommand - command is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendPlayState(J)V
    .locals 3
    .param p1, "timeInMs"    # J

    .prologue
    .line 255
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendPlayState. E time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState(J)V

    .line 257
    return-void
.end method

.method public sendPlayState(JZ)V
    .locals 3
    .param p1, "timeInMs"    # J
    .param p3, "isPlaying"    # Z

    .prologue
    .line 260
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendPlayState. E time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isPlaying:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState(JZ)V

    .line 262
    return-void
.end method

.method public setAudioPathBT()V
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->selectRouteInt(I)V

    .line 310
    return-void
.end method

.method public setAudioPathDevice()V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->selectRouteInt(I)V

    .line 314
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_1

    .line 86
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    if-nez v0, :cond_2

    .line 88
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    .line 89
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setRouteTypes(I)V

    .line 90
    return-void
.end method

.method public setMetaDataBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "flag"    # Ljava/lang/String;
    .param p2, "bitmapdata"    # Landroid/graphics/Bitmap;

    .prologue
    .line 242
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMetaDataBitmap. bitmapdata = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    .line 245
    :cond_0
    return-void
.end method

.method public setMetaDataInt(Ljava/lang/String;I)V
    .locals 4
    .param p1, "flag"    # Ljava/lang/String;
    .param p2, "intdata"    # I

    .prologue
    .line 236
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMetaDataInt. intdata = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 239
    :cond_0
    return-void
.end method

.method public setMetaDataLong(Ljava/lang/String;J)V
    .locals 4
    .param p1, "flag"    # Ljava/lang/String;
    .param p2, "longdata"    # J

    .prologue
    .line 230
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMetaDataLong. longdata = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 233
    :cond_0
    return-void
.end method

.method public setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "flag"    # Ljava/lang/String;
    .param p2, "stringdata"    # Ljava/lang/String;

    .prologue
    .line 224
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMetaDataString. stringdata = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 227
    :cond_0
    return-void
.end method

.method public setRingerMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 333
    :cond_0
    return-void
.end method

.method public setRouteTypes(I)V
    .locals 0
    .param p1, "types"    # I

    .prologue
    .line 403
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mRouteTypes:I

    .line 404
    return-void
.end method

.method public setVolume(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 110
    :cond_0
    return-void
.end method

.method public unregisterRemoteControlReceiver()V
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->iRemoteControlReceiversregistered:Z

    if-eqz v0, :cond_0

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->releaseMediaSession()V

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->iRemoteControlReceiversregistered:Z

    .line 215
    :cond_0
    return-void
.end method

.method public updateMetadata()V
    .locals 2

    .prologue
    .line 248
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;

    const-string v1, "applyMetaData. "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMetadataBuilder:Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V

    .line 252
    :cond_0
    return-void
.end method

.method public volumeDown()V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 100
    :cond_0
    return-void
.end method

.method public volumeSame()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 105
    :cond_0
    return-void
.end method

.method public volumeUp()V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 95
    :cond_0
    return-void
.end method
