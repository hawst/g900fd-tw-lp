.class interface abstract Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotifcationAction;
.super Ljava/lang/Object;
.source "NotificationMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/NotificationMgr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "NotifcationAction"
.end annotation


# static fields
.field public static final CMD_CLOSE:Ljava/lang/String; = "videoplayer.notification.close"

.field public static final CMD_FF_DOWN:Ljava/lang/String; = "videoplayer.notification.ffdown"

.field public static final CMD_FF_UP:Ljava/lang/String; = "videoplayer.notification.ffup"

.field public static final CMD_LAUNCH:Ljava/lang/String; = "videoplayer.notification.launch"

.field public static final CMD_NEXT:Ljava/lang/String; = "videoplayer.notification.next"

.field public static final CMD_PLAYPAUSE:Ljava/lang/String; = "videoplayer.notification.playpause"

.field public static final CMD_PREV:Ljava/lang/String; = "videoplayer.notification.prev"

.field public static final CMD_REW_DOWN:Ljava/lang/String; = "videoplayer.notification.rewdown"

.field public static final CMD_REW_UP:Ljava/lang/String; = "videoplayer.notification.rewup"

.field public static final CMD_TOGGLE:Ljava/lang/String; = "videoplayer.notification.toggle"

.field public static final VIDEO_REMOTE_REQCODE:I = 0x7010007
