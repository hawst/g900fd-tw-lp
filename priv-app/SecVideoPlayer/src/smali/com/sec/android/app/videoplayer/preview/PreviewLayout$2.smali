.class Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;
.super Ljava/lang/Object;
.source "PreviewLayout.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->addView()Lcom/sec/android/app/videoplayer/preview/Preview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const v2, 0x7f0d008f

    .line 60
    if-eqz p2, :cond_1

    .line 61
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201ed

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->access$000(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->access$000(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;->getChapterIdx()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 65
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201ef

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 69
    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 67
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201ec

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
