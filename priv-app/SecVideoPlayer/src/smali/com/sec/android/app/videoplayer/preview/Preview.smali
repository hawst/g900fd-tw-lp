.class public Lcom/sec/android/app/videoplayer/preview/Preview;
.super Ljava/lang/Object;
.source "Preview.java"


# instance fields
.field private mChapterTime:Landroid/widget/TextView;

.field private mChapterView:Lcom/sec/android/app/videoplayer/videowall/ChapterView;

.field private mNoChapterImage:Landroid/widget/ImageView;

.field private mPreviewLayout:Landroid/widget/LinearLayout;

.field private mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/widget/LinearLayout;Lcom/sec/android/app/videoplayer/videowall/ChapterView;Landroid/widget/ProgressBar;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "preview"    # Landroid/widget/LinearLayout;
    .param p2, "chapterview"    # Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    .param p3, "progress"    # Landroid/widget/ProgressBar;
    .param p4, "nochapterImg"    # Landroid/widget/ImageView;
    .param p5, "chaptertime"    # Landroid/widget/TextView;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mPreviewLayout:Landroid/widget/LinearLayout;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mChapterView:Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    .line 24
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mProgressBar:Landroid/widget/ProgressBar;

    .line 25
    iput-object p4, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mNoChapterImage:Landroid/widget/ImageView;

    .line 26
    iput-object p5, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mChapterTime:Landroid/widget/TextView;

    .line 27
    return-void
.end method


# virtual methods
.method public getChapterTime()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mChapterTime:Landroid/widget/TextView;

    return-object v0
.end method

.method public getChapterView()Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mChapterView:Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    return-object v0
.end method

.method public getNoChapterImage()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mNoChapterImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getPreviewLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mPreviewLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getProgressBar()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/Preview;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method
