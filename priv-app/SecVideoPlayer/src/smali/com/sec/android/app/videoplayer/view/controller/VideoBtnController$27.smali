.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 3024
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x4

    .line 3026
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 3027
    .local v3, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    .line 3029
    .local v4, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 3031
    .local v0, "action":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionX:F
    invoke-static {v5, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4202(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F

    .line 3032
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionY:F
    invoke-static {v5, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4302(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F

    .line 3035
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3100
    :cond_0
    :goto_0
    return v11

    .line 3037
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initDetailSeekViewStub()V
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    .line 3040
    packed-switch v0, :pswitch_data_0

    .line 3097
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I

    move-result v6

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4102(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)I

    goto :goto_0

    .line 3043
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_6

    .line 3045
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v2, v5, v6

    .line 3047
    .local v2, "detailedSeeklyWidth":F
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isMiniControllerMode()Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlRightPos()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 3048
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getRight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getProgress()I

    move-result v7

    mul-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getMax()I

    move-result v7

    div-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v7, v7, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080049

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    sub-float/2addr v7, v8

    float-to-int v7, v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4602(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F

    .line 3061
    :goto_2
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v5, :cond_2

    .line 3062
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d012f

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getX()F

    move-result v1

    .line 3063
    .local v1, "controllerLeftMargin":F
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)F

    move-result v6

    add-float/2addr v6, v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4602(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F

    .line 3066
    .end local v1    # "controllerLeftMargin":F
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    .line 3067
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)F

    move-result v6

    sub-float/2addr v6, v2

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setX(F)V

    .line 3069
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3070
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3055
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getRight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getProgress()I

    move-result v7

    mul-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getMax()I

    move-result v7

    div-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v6, v7

    int-to-float v6, v6

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4602(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F

    goto/16 :goto_2

    .line 3071
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I

    move-result v5

    if-ne v5, v10, :cond_5

    .line 3072
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)F

    move-result v6

    sub-float/2addr v6, v2

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setX(F)V

    .line 3074
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3075
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3077
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3078
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3081
    .end local v2    # "detailedSeeklyWidth":F
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3082
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3089
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3090
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 3040
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
