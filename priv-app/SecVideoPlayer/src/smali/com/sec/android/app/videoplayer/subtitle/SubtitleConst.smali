.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleConst;
.super Ljava/lang/Object;
.source "SubtitleConst.java"


# static fields
.field public static final MAX_TEXT_SIZE:I = 0x1e

.field public static final MIN_TEXT_SIZE:I = 0xf

.field public static final SUBTITLE_ALIGNMENT_CENTER:I = 0x1f

.field public static final SUBTITLE_ALIGNMENT_LEFT:I = 0x1e

.field public static final SUBTITLE_ALIGNMENT_RIGHT:I = 0x20

.field public static final SUBTITLE_CHANGE_ACTIVATION:I = 0x190

.field public static final SUBTITLE_CHANGE_CAPTION_WIN_COLOR:I = 0x194

.field public static final SUBTITLE_CHANGE_CAPTION_WIN_OPACITY:I = 0x198

.field public static final SUBTITLE_CHANGE_FONT:I = 0x19a

.field public static final SUBTITLE_CHANGE_FONT_BG_COLOR:I = 0x195

.field public static final SUBTITLE_CHANGE_FONT_BG_OPACITY:I = 0x197

.field public static final SUBTITLE_CHANGE_FONT_COLOR:I = 0x193

.field public static final SUBTITLE_CHANGE_FONT_EDGE:I = 0x191

.field public static final SUBTITLE_CHANGE_FONT_OPACITY:I = 0x196

.field public static final SUBTITLE_CHANGE_FONT_SIZE:I = 0x199

.field public static final SUBTITLE_CHANGE_TEXT_ALIGNMENT:I = 0x192

.field public static final SUBTITLE_COLOR_BLACK:I = -0x1000000

.field public static final SUBTITLE_COLOR_BLUE:I = -0xffff01

.field public static final SUBTITLE_COLOR_CYAN:I = -0xff0001

.field public static final SUBTITLE_COLOR_GREEN:I = -0xff4500

.field public static final SUBTITLE_COLOR_MAGENTA:I = -0xff01

.field public static final SUBTITLE_COLOR_NONE:I = 0x0

.field public static final SUBTITLE_COLOR_RED:I = -0x10000

.field public static final SUBTITLE_COLOR_WHITE:I = -0x1

.field public static final SUBTITLE_COLOR_WHITE_AMOLED:I = -0xa0a0b

.field public static final SUBTITLE_COLOR_YELLOW:I = -0x100

.field public static final SUBTITLE_EDGE_DEPRESSED:I = 0x2

.field public static final SUBTITLE_EDGE_DROP_SHADOWED:I = 0x4

.field public static final SUBTITLE_EDGE_NONE:I = 0x0

.field public static final SUBTITLE_EDGE_RAISED:I = 0x1

.field public static final SUBTITLE_EDGE_UNIFORM:I = 0x3

.field public static SUBTITLE_FILE_TYPE:[Ljava/lang/String; = null

.field public static final SUBTITLE_FILE_TYPE_NONE:I = 0x64

.field public static final SUBTITLE_FILE_TYPE_SMI:I = 0x65

.field public static final SUBTITLE_FILE_TYPE_SMPTETT:I = 0x68

.field public static final SUBTITLE_FILE_TYPE_SRT:I = 0x66

.field public static final SUBTITLE_FILE_TYPE_SUB:I = 0x67

.field public static final SUBTITLE_FILE_TYPE_WEBVTT:I = 0x69

.field public static final SUBTITLE_OPACITY_MAX:I = 0x64

.field public static final SUBTITLE_OPACITY_MIN:I = 0x0

.field public static final SUBTITLE_OPACITY_VZW_DEFAULT:I = 0x1e

.field public static final SUBTITLE_SIZE_LARGE:I = 0x1c

.field public static final SUBTITLE_SIZE_MEDIUM:I = 0x16

.field public static final SUBTITLE_SIZE_SMALL:I = 0x12

.field public static final SUBTITLE_SPLIT_CHANGE_ACTIVATION:I = 0x19b

.field public static final SUBTITLE_TYPE_INBAND:I = 0x0

.field public static final SUBTITLE_TYPE_OUTBAND:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "None"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "smi"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "srt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "smptett"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "webvtt"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleConst;->SUBTITLE_FILE_TYPE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
