.class public Lcom/sec/android/app/videoplayer/view/VideoCaptureView;
.super Ljava/lang/Object;
.source "VideoCaptureView.java"


# static fields
.field private static final CAPTURE_VIEW_TIMEOUT:I = 0xbb8

.field private static final FINISH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoCaptureView"


# instance fields
.field private mCaptureImage:Landroid/widget/ImageView;

.field private mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

.field private mCaptureView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mImage:Landroid/graphics/Bitmap;

.field private mParentView:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    .line 182
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView$2;-><init>(Lcom/sec/android/app/videoplayer/view/VideoCaptureView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    .line 52
    return-void
.end method

.method private _changeCaptureViewLayout()V
    .locals 14

    .prologue
    .line 212
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 213
    .local v3, "imageWidth":I
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 217
    .local v2, "imageHeight":I
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    if-nez v11, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08003f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 221
    .local v4, "initWidth":F
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08003d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v0, v11

    .line 222
    .local v0, "bottomMargin":I
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080040

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v6, v11

    .line 224
    .local v6, "rightMargin":I
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    check-cast v11, Landroid/app/Activity;

    invoke-virtual {v11}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    .line 226
    .local v7, "view":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 227
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 228
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v10

    .line 229
    .local v10, "windowWidth":I
    if-lez v10, :cond_2

    int-to-float v11, v10

    cmpg-float v11, v11, v4

    if-gez v11, :cond_2

    .line 230
    int-to-float v4, v10

    .line 240
    .end local v10    # "windowWidth":I
    :cond_2
    :goto_1
    if-le v3, v2, :cond_4

    .line 241
    float-to-int v8, v4

    .line 242
    .local v8, "width":I
    int-to-float v11, v3

    int-to-float v12, v2

    div-float/2addr v11, v12

    div-float v11, v4, v11

    float-to-int v1, v11

    .line 248
    .local v1, "height":I
    :goto_2
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 249
    .local v5, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 250
    const/16 v11, 0xc

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 251
    const/16 v11, 0xe

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 252
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08003e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v11, v11

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 264
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-eqz v11, :cond_0

    .line 265
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    const v12, 0x7f0d00e6

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 232
    .end local v1    # "height":I
    .end local v5    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "width":I
    :cond_3
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v9

    .line 233
    .local v9, "windowHeight":I
    int-to-float v11, v3

    int-to-float v12, v2

    div-float/2addr v11, v12

    div-float v11, v4, v11

    float-to-int v1, v11

    .line 234
    .restart local v1    # "height":I
    if-lez v9, :cond_2

    if-ge v9, v1, :cond_2

    .line 235
    int-to-float v11, v9

    int-to-float v12, v2

    int-to-float v13, v3

    div-float/2addr v12, v13

    div-float/2addr v11, v12

    float-to-int v11, v11

    int-to-float v4, v11

    goto :goto_1

    .line 244
    .end local v1    # "height":I
    .end local v9    # "windowHeight":I
    :cond_4
    int-to-float v11, v2

    int-to-float v12, v3

    div-float/2addr v11, v12

    div-float v11, v4, v11

    float-to-int v8, v11

    .line 245
    .restart local v8    # "width":I
    float-to-int v1, v4

    .restart local v1    # "height":I
    goto :goto_2

    .line 254
    .restart local v5    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    const/16 v11, 0xc

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 255
    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 256
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 257
    const/16 v11, 0xb

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 258
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_3

    .line 260
    :cond_6
    const/16 v11, 0xe

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_3
.end method

.method private clearImages()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    .line 273
    :cond_0
    return-void
.end method

.method private createCaptureView()V
    .locals 4

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->updateImageDB()V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->updateCapturePreview()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->show()V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    const-string v0, "VideoCaptureView"

    const-string v1, "createCaptureView : updateCapturePreview Error!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initCaptureImageLayout()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    const v1, 0x7f0d00e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoCaptureView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoCaptureView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    :cond_0
    return-void
.end method

.method private makeCaptureView()V
    .locals 3

    .prologue
    .line 65
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 66
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->initCaptureImageLayout()V

    .line 68
    return-void
.end method

.method private setVisibilityCaptureView(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 159
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    const v2, 0x7f0d00e6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    const v2, 0x7f0d00e7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateCapturePreview()Z
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->_changeCaptureViewLayout()V

    .line 203
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 198
    :cond_0
    const-string v0, "VideoCaptureView"

    const-string v1, "updateCapturePreview - mImage not recycled!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->destroyCaptureView()V

    .line 200
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 62
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 56
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->makeCaptureView()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->hide()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 278
    :cond_0
    return-void
.end method

.method public captureVideo()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    if-nez v0, :cond_2

    .line 98
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->enoughSpace()Z

    move-result v0

    if-nez v0, :cond_3

    .line 101
    const-string v0, "VideoCaptureView"

    const-string v1, "captureVideo. device not have enough space error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a00d9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_0

    .line 106
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->clearImages()V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureUtil:Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->CaptureVideo()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->createCaptureView()V

    goto :goto_0

    .line 114
    :cond_4
    const-string v0, "VideoCaptureView"

    const-string v1, "captureVideo Bitmap returns null!!! Video not captured"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->destroyCaptureView()V

    goto :goto_0
.end method

.method public changeCaptureViewLayout()V
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->_changeCaptureViewLayout()V

    .line 209
    :cond_0
    return-void
.end method

.method public destroyCaptureView()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 133
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->clearImages()V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->hide()V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_2

    .line 146
    :cond_1
    :goto_0
    return-void

    .line 138
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 140
    .local v0, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    const v2, 0x7f0d00e7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x80

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 154
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->setVisibilityCaptureView(I)V

    .line 156
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 284
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 285
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mCaptureView:Landroid/view/View;

    .line 286
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 149
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->setVisibilityCaptureView(I)V

    .line 151
    :cond_0
    return-void
.end method

.method public updateImageDB()V
    .locals 5

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCAN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 174
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a002b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    goto :goto_0
.end method
