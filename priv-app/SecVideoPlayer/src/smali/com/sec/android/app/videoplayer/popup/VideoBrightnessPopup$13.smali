.class Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seek"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 480
    if-eqz p3, :cond_0

    .line 481
    const-string v0, "VideoBrightnessDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChanged - brightness : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v0

    mul-int/lit8 v1, p2, 0x14

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setBrightness(I)I

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1300(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessContentDescription()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1400(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 486
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seek"    # Landroid/widget/SeekBar;

    .prologue
    .line 489
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seek"    # Landroid/widget/SeekBar;

    .prologue
    .line 492
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getAutoBrightDetailLevelInSystemValue()I

    move-result v0

    .line 494
    .local v0, "saveVal":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setBrightness(I)I

    .line 495
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v1

    div-int/lit8 v2, v0, 0x14

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 496
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1300(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 497
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessContentDescription()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1400(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 500
    .end local v0    # "saveVal":I
    :cond_0
    return-void
.end method
