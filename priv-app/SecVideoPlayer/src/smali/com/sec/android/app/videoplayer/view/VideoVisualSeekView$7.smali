.class Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$7;
.super Ljava/lang/Object;
.source "VideoVisualSeekView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->ShowVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$7;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 161
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mInfoListener. info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    sparse-switch p2, :sswitch_data_0

    .line 180
    :sswitch_0
    const/4 v0, 0x0

    return v0

    .line 162
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2bc -> :sswitch_0
        0x320 -> :sswitch_0
        0x321 -> :sswitch_0
        0x3b6 -> :sswitch_0
        0x3b7 -> :sswitch_0
        0x3cc -> :sswitch_0
        0x3cd -> :sswitch_0
    .end sparse-switch
.end method
