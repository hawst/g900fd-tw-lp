.class Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 1430
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v5, 0x0

    const/16 v4, 0xd

    const/4 v3, 0x1

    .line 1432
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPlayingChecker , mPlayingCheckTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v0

    if-gez v0, :cond_0

    .line 1463
    :goto_0
    return-void

    .line 1438
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0x12c

    # -= operator for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$020(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I

    .line 1440
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1441
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    if-lez v0, :cond_1

    .line 1442
    const-string v0, "VideoServiceUtil"

    const-string v1, "mPlayingChecker..playing"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1444
    :cond_1
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPlayingChecker..not playing -- state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1445
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1446
    const-string v0, "VideoServiceUtil"

    const-string v1, "checkLockScreenOn checking again "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 1448
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1451
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v4, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1457
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1453
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 1454
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto :goto_1

    .line 1460
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1461
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
