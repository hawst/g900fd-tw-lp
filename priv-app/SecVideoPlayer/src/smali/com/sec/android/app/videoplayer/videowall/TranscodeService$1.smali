.class Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;
.super Ljava/lang/Object;
.source "TranscodeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 138
    const/4 v8, 0x0

    .line 139
    .local v8, "cnt":I
    const/4 v12, -0x1

    .line 140
    .local v12, "num":I
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "Thread start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->initWakeLock()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$000(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/videowall/FileMgr;->thumbnailDBSync()V

    .line 145
    const/4 v13, 0x0

    .line 148
    .local v13, "videoCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 153
    :goto_0
    if-eqz v13, :cond_0

    .line 154
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v9

    .line 158
    .local v9, "drmUtil":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_useHWDecoder(I)I

    .line 160
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v8, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$200(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v13, v11}, Lcom/sec/android/videowall/FileMgr;->getPathFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$302(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v13, v11}, Lcom/sec/android/videowall/FileMgr;->getOnlyFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$402(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v13, v11}, Lcom/sec/android/videowall/FileMgr;->getVideoId(Landroid/database/Cursor;I)J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$502(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;J)J

    .line 164
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LiveThumbnail for SecVideoList. [videoId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], file [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/android/videowall/FileMgr;->hasThumbnailsforList(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    if-eqz v9, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 168
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "VIDEO_NON_DRM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getDurationTime(Ljava/lang/String;Z)I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$602(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;I)I

    .line 170
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duration time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v0

    if-lez v0, :cond_1

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v4

    const/16 v6, 0xa

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->CreateLiveThumbnailforListView(Ljava/lang/String;Ljava/lang/String;JI)I

    .line 174
    const-wide/16 v0, 0x0

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 160
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 149
    .end local v9    # "drmUtil":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    .end local v11    # "i":I
    :catch_0
    move-exception v10

    .line 150
    .local v10, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoCursor - SQLiteException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 175
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v9    # "drmUtil":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    .restart local v11    # "i":I
    :catch_1
    move-exception v10

    .line 176
    .local v10, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 179
    .end local v10    # "e":Ljava/lang/InterruptedException;
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duration time is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ==> not Live but Static thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 182
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "VIDEO_DRM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 185
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "thumbnail for list livethumbnail is existed!!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 189
    :cond_4
    const/4 v12, 0x0

    :goto_3
    if-ge v12, v8, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->bStop:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$200(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v13, v12}, Lcom/sec/android/videowall/FileMgr;->getPathFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$302(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v13, v12}, Lcom/sec/android/videowall/FileMgr;->getOnlyFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$402(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v13, v12}, Lcom/sec/android/videowall/FileMgr;->getVideoId(Landroid/database/Cursor;I)J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$502(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;J)J

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFilePath:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$702(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;)Ljava/lang/String;

    .line 194
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Chapter preview for SecVideoPlayer. [videoId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], file [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/android/videowall/FileMgr;->hasThumbnailsforChapter(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$100(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 197
    if-eqz v9, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    .line 198
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "VIDEO_NON_DRM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getDurationTime(Ljava/lang/String;Z)I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$602(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;I)I

    .line 200
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duration time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const/4 v6, 0x0

    .line 202
    .local v6, "thumbtime":I
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getShortTimeDuration()I

    move-result v1

    if-lt v0, v1, :cond_6

    .line 203
    const/4 v6, 0x3

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getLongTimeDuration()I

    move-result v1

    if-lt v0, v1, :cond_5

    .line 205
    const/16 v6, 0xa

    .line 207
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mPathName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$300(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFileName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$400(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mVideoId:J
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$500(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$700(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->CreateLiveThumbnailforChapterView(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)I

    .line 209
    const-wide/16 v0, 0x0

    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 189
    .end local v6    # "thumbtime":I
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_3

    .line 210
    .restart local v6    # "thumbtime":I
    :catch_2
    move-exception v10

    .line 211
    .restart local v10    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4

    .line 214
    .end local v10    # "e":Ljava/lang/InterruptedException;
    :cond_6
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duration time is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->mDuration:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$600(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ==> not Live but Static thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 217
    .end local v6    # "thumbtime":I
    :cond_7
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "VIDEO_DRM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 220
    :cond_8
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "thumbnail for chapter preview is existed!!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 224
    :cond_9
    if-eqz v13, :cond_a

    .line 225
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 228
    :cond_a
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->TAG:Ljava/lang/String;

    const-string v1, "Thread end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->releaseWakeLock()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$800(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;)V

    .line 231
    if-ne v12, v8, :cond_b

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->stopSelf()V

    .line 234
    :cond_b
    return-void
.end method
