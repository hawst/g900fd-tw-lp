.class public Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;
.super Landroid/widget/RelativeLayout;
.source "VideoTagBuddyView.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoTagBuddyView"


# instance fields
.field private final LeftMargin:I

.field private final RightMargin:I

.field private final TopMargin:I

.field private final UPDATE_LOCATION:I

.field private final UPDATE_TIME_DATE:I

.field private animationListener:Landroid/view/animation/Animation$AnimationListener;

.field private ctimer_auto:Ljava/util/Timer;

.field private fade_in:Landroid/view/animation/AlphaAnimation;

.field private fade_out:Landroid/view/animation/AlphaAnimation;

.field private mContext:Landroid/content/Context;

.field private mDebuggingStr:[Ljava/lang/String;

.field private mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

.field private mTagBuddyView:Landroid/view/View;

.field private mVideoId:J

.field private text_location_layout:Landroid/widget/RelativeLayout;

.field private text_time_layout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "UPDATE_TIME_DATE"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "UPDATE_WEATHER"

    aput-object v2, v0, v1

    const-string v1, "UPDATE_LOCATION"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mDebuggingStr:[Ljava/lang/String;

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->TopMargin:I

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->LeftMargin:I

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->RightMargin:I

    .line 35
    iput v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->UPDATE_TIME_DATE:I

    .line 37
    iput v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->UPDATE_LOCATION:I

    .line 39
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    .line 41
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mContext:Landroid/content/Context;

    .line 43
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mVideoId:J

    .line 51
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    .line 53
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_time_layout:Landroid/widget/RelativeLayout;

    .line 55
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_location_layout:Landroid/widget/RelativeLayout;

    .line 245
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView$2;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mContext:Landroid/content/Context;

    .line 60
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setTagBuddyUpdateListener(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;)V

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->initTagBuddyView()V

    .line 64
    return-void
.end method

.method private addTagBuddyInfo2View(Ljava/lang/String;I)V
    .locals 9
    .param p1, "info"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getLocationTagShowStatus()Z

    move-result v1

    .line 121
    .local v1, "showLocationTag":Z
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getDateTagShowStatus()Z

    move-result v0

    .line 122
    .local v0, "showDateTag":Z
    const-string v4, "VideoTagBuddyView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addTagBuddyInfo2View : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mDebuggingStr:[Ljava/lang/String;

    aget-object v6, v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", info : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", showLocationTag : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", showDateTag : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    packed-switch p2, :pswitch_data_0

    .line 151
    :goto_0
    :pswitch_0
    return-void

    .line 127
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    const v5, 0x7f0d006d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 129
    .local v3, "text_time":Landroid/widget/TextView;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->timedTextVisibility(I)V

    goto :goto_0

    .line 132
    :cond_1
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->timedTextVisibility(I)V

    goto :goto_0

    .line 138
    .end local v3    # "text_time":Landroid/widget/TextView;
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    const v5, 0x7f0d006f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 140
    .local v2, "text_location":Landroid/widget/TextView;
    if-eqz p1, :cond_2

    if-nez v1, :cond_3

    .line 141
    :cond_2
    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->locationTextVisibility(I)V

    goto :goto_0

    .line 143
    :cond_3
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->locationTextVisibility(I)V

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private configLayout()V
    .locals 10

    .prologue
    const v9, 0x7f0d006a

    const/16 v8, 0xb

    const/16 v7, 0x9

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 83
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 85
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    const v4, 0x7f0d006b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_location_layout:Landroid/widget/RelativeLayout;

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_time_layout:Landroid/widget/RelativeLayout;

    .line 88
    iget v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->TopMargin:I

    invoke-virtual {v2, v5, v3, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_time_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 91
    .local v1, "LP_text_time":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->LeftMargin:I

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 92
    iget v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->RightMargin:I

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 94
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_location_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 96
    .local v0, "LP_text_location":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->LeftMargin:I

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 97
    iget v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->RightMargin:I

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_time_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 100
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 101
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 102
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_location_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 105
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 106
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 107
    invoke-virtual {v0, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08006b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    const v4, 0x7f0d006e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    const v4, 0x7f0d006c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_location_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_time_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    return-void
.end method

.method private initTagBuddyView()V
    .locals 3

    .prologue
    .line 67
    const-string v1, "VideoTagBuddyView"

    const-string v2, "initView"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->removeAllViews()V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 71
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->configLayout()V

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->addView(Landroid/view/View;)V

    .line 75
    return-void
.end method

.method private locationTextVisibility(I)V
    .locals 1
    .param p1, "canVisible"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_location_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 274
    return-void
.end method

.method private timedTextVisibility(I)V
    .locals 1
    .param p1, "canVisible"    # I

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->text_time_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 271
    return-void
.end method


# virtual methods
.method public hideTagBuddy(Z)V
    .locals 5
    .param p1, "showAni"    # Z

    .prologue
    const/4 v4, 0x0

    .line 219
    const-string v0, "VideoTagBuddyView"

    const-string v1, "hideTagBuddy E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->cancelTask()V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 224
    if-eqz p1, :cond_2

    .line 225
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_out:Landroid/view/animation/AlphaAnimation;

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_out:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_out:Landroid/view/animation/AlphaAnimation;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_out:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 229
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->setVisibility(I)V

    .line 236
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_out:Landroid/view/animation/AlphaAnimation;

    .line 237
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_in:Landroid/view/animation/AlphaAnimation;

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 241
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    .line 243
    :cond_1
    return-void

    .line 231
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->setVisibility(I)V

    .line 232
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onTagBuddyTimer()V
    .locals 4

    .prologue
    .line 198
    const-string v1, "VideoTagBuddyView"

    const-string v2, "onTagBuddyTimer E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    .line 201
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView$1;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;)V

    .line 215
    .local v0, "cTask":Ljava/util/TimerTask;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 216
    return-void
.end method

.method public onTagBuddyUpdate()V
    .locals 2

    .prologue
    .line 266
    const-string v0, "VideoTagBuddyView"

    const-string v1, "onTagBuddyUpdate E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->showTabBuddyInfo()V

    .line 268
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyView:Landroid/view/View;

    .line 80
    return-void
.end method

.method public showTabBuddyInfo()V
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mVideoId:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->showTagBuddyInfo(J)V

    .line 155
    return-void
.end method

.method public showTagBuddyInfo(J)V
    .locals 3
    .param p1, "videoId"    # J

    .prologue
    const/4 v2, 0x0

    .line 158
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 159
    const-string v0, "VideoTagBuddyView"

    const-string v1, "showTagBuddyInfo. video Id is invalid."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mVideoId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_1

    .line 164
    const-string v0, "VideoTagBuddyView"

    const-string v1, "showTagBuddyInfo. get tag buddy info new"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->hideTagBuddy(Z)V

    .line 166
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mVideoId:J

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagInfo(J)V

    goto :goto_0

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagBuddyShowStatus()Z

    move-result v0

    if-nez v0, :cond_2

    .line 172
    const-string v0, "VideoTagBuddyView"

    const-string v1, "showTagBuddyInfo. mShowTagShow is false"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getLocationString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->addTagBuddyInfo2View(Ljava/lang/String;I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->mTagBuddyInfo:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getDateString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->addTagBuddyInfo2View(Ljava/lang/String;I)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->startTagBuddyAnimation()V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    if-eqz v0, :cond_3

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->ctimer_auto:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 184
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->onTagBuddyTimer()V

    goto :goto_0
.end method

.method public startTagBuddyAnimation()V
    .locals 4

    .prologue
    .line 189
    const-string v0, "VideoTagBuddyView"

    const-string v1, "startTagBuddyAnimation E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_in:Landroid/view/animation/AlphaAnimation;

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_in:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->fade_in:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 194
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->setVisibility(I)V

    .line 195
    return-void
.end method
