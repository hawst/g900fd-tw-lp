.class public final Lcom/sec/android/cloudagent/CloudStore$API;
.super Ljava/lang/Object;
.source "CloudStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/cloudagent/CloudStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "API"
.end annotation


# static fields
.field public static final KEY_CLOUD_AVAILABLE:Ljava/lang/String; = "cloud_available"

.field public static final KEY_CLOUD_VENDOR:Ljava/lang/String; = "cloud_vendor"

.field public static final KEY_CLOUD_VENDOR_AVAILABLE:Ljava/lang/String; = "cloud_vendor_available"

.field public static final KEY_DOWNLOAD:Ljava/lang/String; = "download"

.field public static final KEY_EXCEPTION:Ljava/lang/String; = "exception"

.field public static final KEY_GETDOWNLOAD_URL:Ljava/lang/String; = "get_download_URL"

.field public static final KEY_GETSTREAMING_URL:Ljava/lang/String; = "get_streaming_URL"

.field public static final KEY_GETTHUMBNAIL:Ljava/lang/String; = "get_thumbnail"

.field public static final KEY_MAKE_AVAILABLE_OFFLINE:Ljava/lang/String; = "make_available_offline"

.field public static final KEY_PREFETCH:Ljava/lang/String; = "prefetch"

.field public static final KEY_PREFETCH_WITH_BLOCKING:Ljava/lang/String; = "prefetch_with_blocking"

.field public static final KEY_REVERT_AVAILABLE_OFFLINE:Ljava/lang/String; = "revert_available_offline"

.field public static final KEY_SHARE_URL:Ljava/lang/String; = "get_share_URL"

.field public static final KEY_SYNC:Ljava/lang/String; = "sync"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static download(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 329
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "download"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 331
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 332
    const-string v2, "exception"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 333
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_0

    .line 334
    iget-object v2, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v2

    .line 336
    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_0
    return-void
.end method

.method public static getCloudVendorName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 426
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "cloud_vendor"

    invoke-virtual {v2, v3, v4, v1, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 427
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 428
    const-string v1, "cloud_vendor"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 430
    :cond_0
    return-object v1
.end method

.method public static getDownloadURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_download_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 307
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 309
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 310
    const-string v4, "DOWNLOAD_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 311
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 320
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 314
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 315
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 316
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 320
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getSharedURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_share_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 256
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 258
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 259
    const-string v4, "SHARE_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 260
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 269
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 263
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 264
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 265
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 269
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 280
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_streaming_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 281
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 283
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 284
    const-string v4, "STREAMING_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 294
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 288
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 289
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 290
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 294
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 229
    const/4 v0, 0x0

    .line 230
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_thumbnail"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 232
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 233
    const-string v4, "ThumbnailBitmap"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 235
    .local v3, "thumbPath":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 244
    .end local v3    # "thumbPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 236
    .restart local v3    # "thumbPath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 237
    .local v2, "throwable":Ljava/lang/Throwable;
    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 239
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCloudAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 377
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "cloud_available"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 378
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 379
    const-string v1, "cloudAvailable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 381
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCloudVendorAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 413
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "cloud_vendor_available"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 414
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 415
    const-string v1, "cloud_vendor_available"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 417
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 356
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "make_available_offline"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 358
    return-void
.end method

.method public static prefetch(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 345
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "prefetch"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 347
    return-void
.end method

.method public static prefetchWithBlocking(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 391
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "prefetch_with_blocking"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 392
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 393
    .local v2, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 394
    const-string v4, "CACHED_PATH"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 396
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 405
    .end local v2    # "path":Ljava/lang/String;
    .local v3, "path":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 399
    .end local v3    # "path":Ljava/lang/String;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 400
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 401
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 405
    .end local v2    # "path":Ljava/lang/String;
    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_0
.end method

.method public static revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 367
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "revert_available_offline"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 369
    return-void
.end method

.method public static sync(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 218
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "sync"

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 219
    return-void
.end method
