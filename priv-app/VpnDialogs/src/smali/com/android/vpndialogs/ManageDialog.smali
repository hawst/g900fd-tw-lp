.class public Lcom/android/vpndialogs/ManageDialog;
.super Lcom/android/internal/app/AlertActivity;
.source "ManageDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/os/Handler$Callback;


# instance fields
.field private mConfig:Lcom/android/internal/net/VpnConfig;

.field private mDataReceived:Landroid/widget/TextView;

.field private mDataRowsHidden:Z

.field private mDataTransmitted:Landroid/widget/TextView;

.field private mDuration:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mService:Landroid/net/IConnectivityManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    return-void
.end method

.method private convertBytesToMB(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 181
    const/4 v2, 0x0

    .line 182
    .local v2, "num":I
    const-string v4, ""

    .line 184
    .local v4, "s":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 185
    const/high16 v0, 0x100000

    .line 186
    .local v0, "div":I
    int-to-float v5, v2

    int-to-float v6, v0

    div-float v3, v5, v6

    .line 187
    .local v3, "preciseNum":F
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v5, "###,###,###,###,##0.00"

    invoke-direct {v1, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 188
    .local v1, "f":Ljava/text/NumberFormat;
    float-to-double v6, v3

    invoke-virtual {v1, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 192
    .end local v0    # "div":I
    .end local v1    # "f":Ljava/text/NumberFormat;
    .end local v3    # "preciseNum":F
    :goto_0
    return-object v4

    .line 189
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private getNumbers()[Ljava/lang/String;
    .locals 8

    .prologue
    .line 196
    const/4 v1, 0x0

    .line 199
    .local v1, "in":Ljava/io/DataInputStream;
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    const-string v7, "/proc/net/dev"

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    .end local v1    # "in":Ljava/io/DataInputStream;
    .local v2, "in":Ljava/io/DataInputStream;
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v7, v7, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x3a

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 203
    .local v5, "prefix":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 204
    .local v3, "line":Ljava/lang/String;
    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 205
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, " +"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 206
    .local v4, "numbers":[Ljava/lang/String;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/16 v6, 0x11

    if-ge v0, v6, :cond_2

    .line 207
    aget-object v6, v4, v0

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    if-nez v6, :cond_1

    .line 218
    :try_start_2
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :goto_1
    move-object v1, v2

    .line 223
    .end local v0    # "i":I
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "numbers":[Ljava/lang/String;
    .end local v5    # "prefix":Ljava/lang/String;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    :goto_2
    return-object v4

    .line 206
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v0    # "i":I
    .restart local v2    # "in":Ljava/io/DataInputStream;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v4    # "numbers":[Ljava/lang/String;
    .restart local v5    # "prefix":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-object v1, v2

    .line 223
    .end local v0    # "i":I
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "numbers":[Ljava/lang/String;
    .end local v5    # "prefix":Ljava/lang/String;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    :goto_3
    const/4 v4, 0x0

    goto :goto_2

    .line 219
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v0    # "i":I
    .restart local v2    # "in":Ljava/io/DataInputStream;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v4    # "numbers":[Ljava/lang/String;
    .restart local v5    # "prefix":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v1, v2

    .line 222
    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    goto :goto_3

    .line 214
    .end local v0    # "i":I
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "numbers":[Ljava/lang/String;
    .end local v5    # "prefix":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 218
    :goto_4
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    .line 219
    :catch_2
    move-exception v6

    goto :goto_3

    .line 217
    :catchall_0
    move-exception v6

    .line 218
    :goto_5
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 221
    :goto_6
    throw v6

    .line 219
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v0    # "i":I
    .restart local v2    # "in":Ljava/io/DataInputStream;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v4    # "numbers":[Ljava/lang/String;
    .restart local v5    # "prefix":Ljava/lang/String;
    :catch_3
    move-exception v6

    goto :goto_1

    .end local v0    # "i":I
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "numbers":[Ljava/lang/String;
    .end local v5    # "prefix":Ljava/lang/String;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    :catch_4
    move-exception v7

    goto :goto_6

    .line 217
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    goto :goto_5

    .line 214
    .end local v1    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    :catch_5
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "in":Ljava/io/DataInputStream;
    goto :goto_4
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 12
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 149
    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 151
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_3

    .line 152
    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-wide v4, v1, Lcom/android/internal/net/VpnConfig;->startTime:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 153
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-wide v6, v1, Lcom/android/internal/net/VpnConfig;->startTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 154
    .local v2, "seconds":J
    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mDuration:Landroid/widget/TextView;

    const-string v4, "%02d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-wide/16 v8, 0xe10

    div-long v8, v2, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide/16 v8, 0x3c

    div-long v8, v2, v8

    const-wide/16 v10, 0x3c

    rem-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-wide/16 v8, 0x3c

    rem-long v8, v2, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    .end local v2    # "seconds":J
    :cond_0
    invoke-direct {p0}, Lcom/android/vpndialogs/ManageDialog;->getNumbers()[Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "numbers":[Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 161
    iget-boolean v1, p0, Lcom/android/vpndialogs/ManageDialog;->mDataRowsHidden:Z

    if-eqz v1, :cond_1

    .line 162
    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Lcom/android/vpndialogs/ManageDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 163
    const v1, 0x7f060006

    invoke-virtual {p0, v1}, Lcom/android/vpndialogs/ManageDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 164
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/vpndialogs/ManageDialog;->mDataRowsHidden:Z

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mDataReceived:Landroid/widget/TextView;

    const v4, 0x7f040011

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-direct {p0, v7}, Lcom/android/vpndialogs/ManageDialog;->convertBytesToMB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/android/vpndialogs/ManageDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mDataTransmitted:Landroid/widget/TextView;

    const v4, 0x7f040011

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x9

    aget-object v7, v0, v7

    invoke-direct {p0, v7}, Lcom/android/vpndialogs/ManageDialog;->convertBytesToMB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/android/vpndialogs/ManageDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/android/vpndialogs/ManageDialog;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    const-wide/16 v6, 0x3e8

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 177
    .end local v0    # "numbers":[Ljava/lang/String;
    :cond_3
    const/4 v1, 0x1

    return v1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 127
    const/4 v3, -0x1

    if-ne p2, v3, :cond_1

    .line 128
    :try_start_0
    iget-object v3, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    invoke-virtual {v3}, Landroid/app/PendingIntent;->send()V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    const/4 v3, -0x3

    if-ne p2, v3, :cond_0

    .line 130
    iget-object v3, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-boolean v3, v3, Lcom/android/internal/net/VpnConfig;->legacy:Z

    if-eqz v3, :cond_2

    .line 131
    iget-object v3, p0, Lcom/android/vpndialogs/ManageDialog;->mService:Landroid/net/IConnectivityManager;

    const-string v4, "[Legacy VPN]"

    const-string v5, "[Legacy VPN]"

    invoke-interface {v3, v4, v5}, Landroid/net/IConnectivityManager;->prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v1

    .line 137
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "VpnManage"

    const-string v4, "onClick"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 139
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 140
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 141
    .local v2, "res":Landroid/content/res/Resources;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f04001a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f04001b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 143
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->finish()V

    goto :goto_0

    .line 133
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/android/vpndialogs/ManageDialog;->mService:Landroid/net/IConnectivityManager;

    iget-object v4, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    const-string v5, "[Legacy VPN]"

    invoke-interface {v3, v4, v5}, Landroid/net/IConnectivityManager;->prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onPause()V

    .line 119
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->finish()V

    .line 122
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 58
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onResume()V

    .line 60
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 61
    const-string v2, "VpnManage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->getCallingPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot start this activity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->finish()V

    .line 114
    :goto_0
    return-void

    .line 68
    :cond_0
    :try_start_0
    const-string v2, "connectivity"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mService:Landroid/net/IConnectivityManager;

    .line 71
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mService:Landroid/net/IConnectivityManager;

    invoke-interface {v2}, Landroid/net/IConnectivityManager;->getVpnConfig()Lcom/android/internal/net/VpnConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    .line 74
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    if-nez v2, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VpnManage"

    const-string v3, "onResume"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->finish()V

    goto :goto_0

    .line 79
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v2, 0x7f030002

    const/4 v3, 0x0

    :try_start_1
    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 80
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v2, v2, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 81
    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_2
    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mDuration:Landroid/widget/TextView;

    .line 84
    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mDataTransmitted:Landroid/widget/TextView;

    .line 85
    const v2, 0x7f060007

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mDataReceived:Landroid/widget/TextView;

    .line 86
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/vpndialogs/ManageDialog;->mDataRowsHidden:Z

    .line 88
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-boolean v2, v2, Lcom/android/internal/net/VpnConfig;->legacy:Z

    if-eqz v2, :cond_5

    .line 89
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v3, 0x108009b

    iput v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    .line 90
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v3, 0x7f040004

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ManageDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 94
    :goto_1
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v2, v2, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    if-eqz v2, :cond_3

    .line 95
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v3, 0x7f040005

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ManageDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    .line 96
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    .line 99
    :cond_3
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v3, 0x7f040006

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ManageDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNeutralButtonText:Ljava/lang/CharSequence;

    .line 100
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNeutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    .line 101
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const/high16 v3, 0x1040000

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ManageDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    .line 102
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    .line 103
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object v1, v2, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    .line 104
    invoke-virtual {p0}, Lcom/android/vpndialogs/ManageDialog;->setupAlert()V

    .line 106
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mHandler:Landroid/os/Handler;

    if-nez v2, :cond_4

    .line 107
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mHandler:Landroid/os/Handler;

    .line 109
    :cond_4
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 92
    :cond_5
    iget-object v2, p0, Lcom/android/vpndialogs/ManageDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iget-object v3, p0, Lcom/android/vpndialogs/ManageDialog;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/android/internal/net/VpnConfig;->getVpnLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
