.class public Lcom/android/vpndialogs/EnterpriseVpnDialog;
.super Landroid/app/Activity;
.source "EnterpriseVpnDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final DBG:Z


# instance fields
.field private configLocal:Lcom/android/internal/net/VpnConfig;

.field linearLayout:Landroid/widget/LinearLayout;

.field private mConfigsReceived:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/net/VpnConfig;",
            ">;"
        }
    .end annotation
.end field

.field private mDataReceived:Landroid/widget/TextView;

.field private mDataReceivedMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mDataTransmitted:Landroid/widget/TextView;

.field private mDataTransmittedMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mDialog:Landroid/app/AlertDialog;

.field private mDuration:Landroid/widget/TextView;

.field private mDurationsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private pm:Landroid/content/pm/PackageManager;

.field private scrollView:Landroid/widget/ScrollView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 76
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    .line 71
    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->pm:Landroid/content/pm/PackageManager;

    .line 72
    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method private convertBytesToMB(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 304
    const/4 v2, 0x0

    .line 305
    .local v2, "num":I
    const-string v4, "0"

    .line 307
    .local v4, "s":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 308
    const/high16 v0, 0x100000

    .line 309
    .local v0, "div":I
    int-to-float v5, v2

    int-to-float v6, v0

    div-float v3, v5, v6

    .line 310
    .local v3, "preciseNum":F
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v5, "###,###,###,###,##0.00"

    invoke-direct {v1, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 311
    .local v1, "f":Ljava/text/NumberFormat;
    float-to-double v6, v3

    invoke-virtual {v1, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 315
    .end local v0    # "div":I
    .end local v1    # "f":Ljava/text/NumberFormat;
    .end local v3    # "preciseNum":F
    :goto_0
    return-object v4

    .line 312
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private getFormattedString(J)Ljava/lang/String;
    .locals 17
    .param p1, "elapsedSeconds"    # J

    .prologue
    .line 284
    const/16 v2, 0x3c

    .line 285
    .local v2, "SECONDS_IN_MIN":I
    const/16 v1, 0xe10

    .line 286
    .local v1, "SECONDS_IN_HOUR":I
    const v0, 0x15180

    .line 288
    .local v0, "SECONDS_IN_DAY":I
    const-wide/32 v12, 0x15180

    div-long v4, p1, v12

    .line 289
    .local v4, "days":J
    const-wide/32 v12, 0x15180

    rem-long p1, p1, v12

    .line 291
    const-wide/16 v12, 0xe10

    div-long v6, p1, v12

    .line 292
    .local v6, "hours":J
    const-wide/16 v12, 0xe10

    rem-long p1, p1, v12

    .line 294
    const-wide/16 v12, 0x3c

    div-long v8, p1, v12

    .line 295
    .local v8, "minutes":J
    const-wide/16 v12, 0x3c

    rem-long v10, p1, v12

    .line 297
    .local v10, "seconds":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f040019

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 299
    .local v3, "szDays":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "%01d "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " %02d:%02d:%02d"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    return-object v12
.end method

.method private getStatistics(Lcom/android/internal/net/VpnConfig;)[Ljava/lang/String;
    .locals 11
    .param p1, "config"    # Lcom/android/internal/net/VpnConfig;

    .prologue
    .line 319
    const/4 v2, 0x0

    .line 322
    .local v2, "in":Ljava/io/DataInputStream;
    :try_start_0
    new-instance v3, Ljava/io/DataInputStream;

    new-instance v7, Ljava/io/FileInputStream;

    const-string v8, "/proc/net/dev"

    invoke-direct {v7, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    .end local v2    # "in":Ljava/io/DataInputStream;
    .local v3, "in":Ljava/io/DataInputStream;
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p1, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 326
    .local v6, "prefix":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 327
    .local v4, "line":Ljava/lang/String;
    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 328
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, " +"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 329
    .local v5, "numbers":[Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "i":I
    const/16 v7, 0x11

    if-ge v1, v7, :cond_1

    .line 344
    :try_start_2
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_0
    move-object v2, v3

    .line 350
    .end local v1    # "i":I
    .end local v3    # "in":Ljava/io/DataInputStream;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "numbers":[Ljava/lang/String;
    .end local v6    # "prefix":Ljava/lang/String;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    :goto_1
    return-object v5

    .line 345
    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "i":I
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "numbers":[Ljava/lang/String;
    .restart local v6    # "prefix":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Ljava/lang/Exception;
    const-string v7, "EnterpriseVpnDialog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 344
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v3

    .line 350
    .end local v1    # "i":I
    .end local v3    # "in":Ljava/io/DataInputStream;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "numbers":[Ljava/lang/String;
    .end local v6    # "prefix":Ljava/lang/String;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    :goto_2
    const/4 v5, 0x0

    goto :goto_1

    .line 345
    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "i":I
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "numbers":[Ljava/lang/String;
    .restart local v6    # "prefix":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 347
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v7, "EnterpriseVpnDialog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 349
    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    goto :goto_2

    .line 338
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i":I
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "numbers":[Ljava/lang/String;
    .end local v6    # "prefix":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 340
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_3
    :try_start_4
    iget-object v7, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->dismiss()V

    .line 341
    const-string v7, "EnterpriseVpnDialog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 344
    :try_start_5
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    .line 345
    :catch_3
    move-exception v0

    .line 347
    const-string v7, "EnterpriseVpnDialog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 343
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 344
    :goto_4
    :try_start_6
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 348
    :goto_5
    throw v7

    .line 345
    :catch_4
    move-exception v0

    .line 347
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "EnterpriseVpnDialog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 343
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    goto :goto_4

    .line 338
    .end local v2    # "in":Ljava/io/DataInputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    :catch_5
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    goto :goto_3
.end method

.method private initializeLayout()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 135
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    .line 136
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 137
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    new-instance v1, Landroid/widget/ScrollView;

    invoke-direct {v1, p0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->scrollView:Landroid/widget/ScrollView;

    .line 140
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    const/16 v2, 0x12c

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 142
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->scrollView:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    return-void
.end method

.method private initializeProfileDetails()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 189
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 190
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/net/VpnConfig;

    iput-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    .line 191
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iput-object v7, v3, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    .line 192
    const v3, 0x7f030003

    invoke-static {p0, v3, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 193
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 197
    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const v3, 0x7f060003

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDuration:Landroid/widget/TextView;

    .line 200
    const v3, 0x7f060005

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataTransmitted:Landroid/widget/TextView;

    .line 201
    const v3, 0x7f060007

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataReceived:Landroid/widget/TextView;

    .line 204
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    invoke-interface {v3, v4, v6, v6}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 205
    .local v0, "app":Landroid/content/pm/ApplicationInfo;
    if-nez v0, :cond_1

    .line 206
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v5

    invoke-interface {v3, v4, v6, v5}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 209
    :cond_1
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iput-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->pm:Landroid/content/pm/PackageManager;

    .line 211
    const v3, 0x7f060009

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    const v3, 0x7f060008

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 213
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 214
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDurationsMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDuration:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataTransmittedMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataTransmitted:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataReceivedMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataReceived:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 219
    .end local v0    # "app":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "view":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private initializeProfileList()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 146
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 147
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/internal/net/VpnConfig;

    iput-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    .line 148
    sget-boolean v4, Lcom/android/vpndialogs/EnterpriseVpnDialog;->DBG:Z

    if-eqz v4, :cond_1

    const-string v4, "EnterpriseVpnDialog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " configLocal.configureIntent  : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v6, v6, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_1
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iput-object v8, v4, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    .line 150
    const v4, 0x7f030004

    invoke-static {p0, v4, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 151
    .local v3, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v4, v4, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 155
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 156
    new-instance v4, Lcom/android/vpndialogs/EnterpriseVpnDialog$1;

    invoke-direct {v4, p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog$1;-><init>(Lcom/android/vpndialogs/EnterpriseVpnDialog;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v5, v5, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    invoke-interface {v4, v5, v7, v7}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 172
    .local v0, "app":Landroid/content/pm/ApplicationInfo;
    if-nez v0, :cond_2

    .line 173
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v5, v5, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v6

    invoke-interface {v4, v5, v7, v6}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 176
    :cond_2
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->pm:Landroid/content/pm/PackageManager;

    .line 177
    const v4, 0x7f060002

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v5, v5, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    const v4, 0x7f060008

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v5}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 179
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 180
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 182
    const v4, 0x7f030001

    invoke-static {p0, v4, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 183
    .local v1, "divider":Landroid/view/View;
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 186
    .end local v0    # "app":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "divider":Landroid/view/View;
    .end local v3    # "view":Landroid/view/View;
    :cond_3
    return-void
.end method

.method private setupHandler()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mHandler:Landroid/os/Handler;

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 226
    return-void
.end method

.method private showDialog()V
    .locals 3

    .prologue
    .line 116
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f04000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    .line 125
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->configLocal:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    const v2, 0x7f040005

    invoke-virtual {p0, v2}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 131
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 132
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 12
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 255
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 256
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 257
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 258
    .local v6, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 259
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/net/VpnConfig;

    .line 260
    .local v0, "configLocal":Lcom/android/internal/net/VpnConfig;
    const/4 v8, 0x0

    iput-object v8, v0, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    .line 261
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDurationsMap:Ljava/util/HashMap;

    iget-object v9, v0, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 262
    .local v3, "durationLocal":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataTransmittedMap:Ljava/util/HashMap;

    iget-object v9, v0, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 263
    .local v2, "dataTransmittedLocal":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataReceivedMap:Ljava/util/HashMap;

    iget-object v9, v0, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 264
    .local v1, "dataReceivedLocal":Landroid/widget/TextView;
    iget-wide v8, v0, Lcom/android/internal/net/VpnConfig;->startTime:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 265
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, v0, Lcom/android/internal/net/VpnConfig;->startTime:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    div-long v4, v8, v10

    .line 266
    .local v4, "elapsedSeconds":J
    invoke-direct {p0, v4, v5}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getFormattedString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    .end local v4    # "elapsedSeconds":J
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getStatistics(Lcom/android/internal/net/VpnConfig;)[Ljava/lang/String;

    move-result-object v7

    .line 269
    .local v7, "numbers":[Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 271
    const v8, 0x7f040010

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x1

    aget-object v11, v7, v11

    invoke-direct {p0, v11}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->convertBytesToMB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x1

    aget-object v11, v7, v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    const v8, 0x7f040010

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x9

    aget-object v11, v7, v11

    invoke-direct {p0, v11}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->convertBytesToMB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/16 v11, 0x9

    aget-object v11, v7, v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    :cond_1
    iget-object v8, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    const-wide/16 v10, 0x3e8

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 280
    .end local v0    # "configLocal":Lcom/android/internal/net/VpnConfig;
    .end local v1    # "dataReceivedLocal":Landroid/widget/TextView;
    .end local v2    # "dataTransmittedLocal":Landroid/widget/TextView;
    .end local v3    # "durationLocal":Landroid/widget/TextView;
    .end local v6    # "iterator":Ljava/util/Iterator;
    .end local v7    # "numbers":[Ljava/lang/String;
    :cond_2
    const/4 v8, 0x1

    return v8
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 240
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 241
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->finish()V

    .line 246
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 231
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 232
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 234
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 236
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 82
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 83
    const-string v1, "EnterpriseVpnDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cannot start this activity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->finish()V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDurationsMap:Ljava/util/HashMap;

    .line 88
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataTransmittedMap:Ljava/util/HashMap;

    .line 89
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mDataReceivedMap:Ljava/util/HashMap;

    .line 92
    :try_start_0
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "config"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    .line 93
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 96
    const-string v1, "EnterpriseVpnDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mConfigsReceived size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog;->mConfigsReceived:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 99
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->initializeLayout()V

    .line 100
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->initializeProfileList()V

    .line 101
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->showDialog()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EnterpriseVpnDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->finish()V

    goto :goto_0

    .line 104
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->initializeLayout()V

    .line 105
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->initializeProfileDetails()V

    .line 106
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->showDialog()V

    .line 107
    invoke-direct {p0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->setupHandler()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
