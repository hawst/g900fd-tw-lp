.class public Lcom/android/vpndialogs/EnterpriseVpnError;
.super Landroid/app/Activity;
.source "EnterpriseVpnError.java"


# instance fields
.field private final DBG:Z

.field private TAG:Ljava/lang/String;

.field private final VPN_ERRORED_PROFILE_INFO:Ljava/lang/String;

.field private final VPN_RETRY_CALLBACK_ACTION:Ljava/lang/String;

.field private builder:Landroid/app/AlertDialog$Builder;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mProfileErroredMsg:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private message:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 21
    const-string v1, "EnterpriseVpnError"

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    .line 22
    const-string v1, "com.samsung.android.mdm.VPN_RETRY_CALLBACK"

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->VPN_RETRY_CALLBACK_ACTION:Ljava/lang/String;

    .line 23
    const-string v1, "vpn_error_profile_info"

    iput-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->VPN_ERRORED_PROFILE_INFO:Ljava/lang/String;

    .line 24
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput-boolean v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->DBG:Z

    .line 27
    iput-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->title:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->message:Ljava/lang/String;

    .line 29
    iput-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->builder:Landroid/app/AlertDialog$Builder;

    .line 30
    iput-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    .line 31
    iput-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mProfileErroredMsg:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/android/vpndialogs/EnterpriseVpnError;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/android/vpndialogs/EnterpriseVpnError;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/vpndialogs/EnterpriseVpnError;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/vpndialogs/EnterpriseVpnError;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/vpndialogs/EnterpriseVpnError;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/vpndialogs/EnterpriseVpnError;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 93
    iget-boolean v4, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->DBG:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    const-string v5, "getErrorMessage()"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_0
    const/4 v0, 0x0

    .line 96
    .local v0, "errorMessage":Ljava/lang/String;
    const/4 v1, 0x0

    .line 98
    .local v1, "errorString":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mProfileErroredMsg:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 99
    .local v3, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mProfileErroredMsg:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "errorString":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 100
    .restart local v1    # "errorString":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v4, "No Error"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 101
    :cond_1
    iget-object v4, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mContext:Landroid/content/Context;

    const v5, 0x7f040016

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 103
    :cond_2
    if-nez v0, :cond_3

    .line 104
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Profile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nError : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\nProfile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nError : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    .end local v3    # "key":Ljava/lang/String;
    :cond_4
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    iget-boolean v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->DBG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 115
    iget-boolean v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->DBG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 119
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 120
    iput-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    .line 122
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 42
    iget-boolean v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->DBG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnError;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mContext:Landroid/content/Context;

    .line 45
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/android/vpndialogs/EnterpriseVpnError;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnError;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "vpn_error_profile_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mProfileErroredMsg:Ljava/util/HashMap;

    .line 47
    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnError;->showErrorDialog()V

    .line 49
    return-void
.end method

.method public showErrorDialog()V
    .locals 3

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->DBG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    const-string v1, "showErrorDialog()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mContext:Landroid/content/Context;

    const v1, 0x7f040015

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->title:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mContext:Landroid/content/Context;

    const v1, 0x7f040014

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->message:Ljava/lang/String;

    .line 57
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "show error message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "show error error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnError;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->builder:Landroid/app/AlertDialog$Builder;

    .line 61
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->builder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/vpndialogs/EnterpriseVpnError;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040018

    new-instance v2, Lcom/android/vpndialogs/EnterpriseVpnError$2;

    invoke-direct {v2, p0}, Lcom/android/vpndialogs/EnterpriseVpnError$2;-><init>(Lcom/android/vpndialogs/EnterpriseVpnError;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040017

    new-instance v2, Lcom/android/vpndialogs/EnterpriseVpnError$1;

    invoke-direct {v2, p0}, Lcom/android/vpndialogs/EnterpriseVpnError$1;-><init>(Lcom/android/vpndialogs/EnterpriseVpnError;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    .line 82
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/vpndialogs/EnterpriseVpnError$3;

    invoke-direct {v1, p0}, Lcom/android/vpndialogs/EnterpriseVpnError$3;-><init>(Lcom/android/vpndialogs/EnterpriseVpnError;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 88
    iget-object v0, p0, Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 90
    return-void
.end method
