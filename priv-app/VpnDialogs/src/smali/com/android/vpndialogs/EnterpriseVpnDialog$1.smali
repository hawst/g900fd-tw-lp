.class Lcom/android/vpndialogs/EnterpriseVpnDialog$1;
.super Ljava/lang/Object;
.source "EnterpriseVpnDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vpndialogs/EnterpriseVpnDialog;->initializeProfileList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vpndialogs/EnterpriseVpnDialog;


# direct methods
.method constructor <init>(Lcom/android/vpndialogs/EnterpriseVpnDialog;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog$1;->this$0:Lcom/android/vpndialogs/EnterpriseVpnDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 159
    iget-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog$1;->this$0:Lcom/android/vpndialogs/EnterpriseVpnDialog;

    invoke-virtual {v2}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->finish()V

    .line 160
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 161
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v1, "mConfigs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/internal/net/VpnConfig;>;"
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/net/VpnConfig;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    const-string v2, "com.android.vpndialogs"

    const-string v3, "com.android.vpndialogs.EnterpriseVpnDialog"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const-string v2, "config"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 165
    const/high16 v2, 0x50800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 167
    iget-object v2, p0, Lcom/android/vpndialogs/EnterpriseVpnDialog$1;->this$0:Lcom/android/vpndialogs/EnterpriseVpnDialog;

    invoke-virtual {v2, v0}, Lcom/android/vpndialogs/EnterpriseVpnDialog;->startActivity(Landroid/content/Intent;)V

    .line 168
    return-void
.end method
