.class Lcom/android/vpndialogs/EnterpriseVpnError$2;
.super Ljava/lang/Object;
.source "EnterpriseVpnError.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vpndialogs/EnterpriseVpnError;->showErrorDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vpndialogs/EnterpriseVpnError;


# direct methods
.method constructor <init>(Lcom/android/vpndialogs/EnterpriseVpnError;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/vpndialogs/EnterpriseVpnError$2;->this$0:Lcom/android/vpndialogs/EnterpriseVpnError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 67
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError$2;->this$0:Lcom/android/vpndialogs/EnterpriseVpnError;

    # getter for: Lcom/android/vpndialogs/EnterpriseVpnError;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/vpndialogs/EnterpriseVpnError;->access$100(Lcom/android/vpndialogs/EnterpriseVpnError;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "try to reconnect vpn"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 69
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.mdm.VPN_RETRY_CALLBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError$2;->this$0:Lcom/android/vpndialogs/EnterpriseVpnError;

    # getter for: Lcom/android/vpndialogs/EnterpriseVpnError;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/vpndialogs/EnterpriseVpnError;->access$200(Lcom/android/vpndialogs/EnterpriseVpnError;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 71
    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError$2;->this$0:Lcom/android/vpndialogs/EnterpriseVpnError;

    # getter for: Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/android/vpndialogs/EnterpriseVpnError;->access$000(Lcom/android/vpndialogs/EnterpriseVpnError;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/vpndialogs/EnterpriseVpnError$2;->this$0:Lcom/android/vpndialogs/EnterpriseVpnError;

    # getter for: Lcom/android/vpndialogs/EnterpriseVpnError;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/android/vpndialogs/EnterpriseVpnError;->access$000(Lcom/android/vpndialogs/EnterpriseVpnError;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->cancel()V

    .line 73
    :cond_0
    return-void
.end method
