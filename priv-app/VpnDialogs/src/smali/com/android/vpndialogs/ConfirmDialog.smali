.class public Lcom/android/vpndialogs/ConfirmDialog;
.super Lcom/android/internal/app/AlertActivity;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/Html$ImageGetter;


# instance fields
.field private mButton:Landroid/widget/Button;

.field private mPackage:Ljava/lang/String;

.field private mService:Landroid/net/IConnectivityManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 92
    const/high16 v1, 0x7f020000

    invoke-virtual {p0, v1}, Lcom/android/vpndialogs/ConfirmDialog;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 93
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 94
    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 104
    :try_start_0
    iget-object v1, p0, Lcom/android/vpndialogs/ConfirmDialog;->mService:Landroid/net/IConnectivityManager;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/vpndialogs/ConfirmDialog;->mPackage:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/net/IConnectivityManager;->prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/vpndialogs/ConfirmDialog;->setResult(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VpnConfirm"

    const-string v2, "onClick"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 7

    .prologue
    .line 53
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onResume()V

    .line 55
    :try_start_0
    invoke-virtual {p0}, Lcom/android/vpndialogs/ConfirmDialog;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mPackage:Ljava/lang/String;

    .line 57
    const-string v2, "connectivity"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mService:Landroid/net/IConnectivityManager;

    .line 60
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mService:Landroid/net/IConnectivityManager;

    iget-object v3, p0, Lcom/android/vpndialogs/ConfirmDialog;->mPackage:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/net/IConnectivityManager;->prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/android/vpndialogs/ConfirmDialog;->setResult(I)V

    .line 62
    invoke-virtual {p0}, Lcom/android/vpndialogs/ConfirmDialog;->finish()V

    .line 87
    :goto_0
    return-void

    .line 66
    :cond_0
    const/high16 v2, 0x7f030000

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 68
    .local v1, "view":Landroid/view/View;
    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f040013

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/vpndialogs/ConfirmDialog;->mPackage:Ljava/lang/String;

    invoke-static {p0, v6}, Lcom/android/internal/net/VpnConfig;->getVpnLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/android/vpndialogs/ConfirmDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, p0, v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v3, 0x7f040012

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ConfirmDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 74
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v3, 0x104000a

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ConfirmDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    .line 75
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    .line 76
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const/high16 v3, 0x1040000

    invoke-virtual {p0, v3}, Lcom/android/vpndialogs/ConfirmDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    .line 77
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object v1, v2, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    .line 78
    invoke-virtual {p0}, Lcom/android/vpndialogs/ConfirmDialog;->setupAlert()V

    .line 80
    invoke-virtual {p0}, Lcom/android/vpndialogs/ConfirmDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    .line 81
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mAlert:Lcom/android/internal/app/AlertController;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    iput-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mButton:Landroid/widget/Button;

    .line 82
    iget-object v2, p0, Lcom/android/vpndialogs/ConfirmDialog;->mButton:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFilterTouchesWhenObscured(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 83
    .end local v1    # "view":Landroid/view/View;
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VpnConfirm"

    const-string v3, "onResume"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    invoke-virtual {p0}, Lcom/android/vpndialogs/ConfirmDialog;->finish()V

    goto :goto_0
.end method
