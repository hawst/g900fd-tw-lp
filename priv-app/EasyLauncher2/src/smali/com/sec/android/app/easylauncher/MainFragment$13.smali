.class Lcom/sec/android/app/easylauncher/MainFragment$13;
.super Ljava/lang/Object;
.source "MainFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/MainFragment;->removeShortCutDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 596
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->access$302(Lcom/sec/android/app/easylauncher/MainFragment;Z)Z

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget v1, v1, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->removeFromFavorites(I)V

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    # getter for: Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/MainFragment;->access$800(Lcom/sec/android/app/easylauncher/MainFragment;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    # setter for: Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;
    invoke-static {v0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->access$802(Lcom/sec/android/app/easylauncher/MainFragment;Landroid/content/Context;)Landroid/content/Context;

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    # getter for: Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/MainFragment;->access$800(Lcom/sec/android/app/easylauncher/MainFragment;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$13;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    # getter for: Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/MainFragment;->access$800(Lcom/sec/android/app/easylauncher/MainFragment;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->loadApps()V

    .line 604
    :cond_1
    return-void
.end method
