.class Lcom/sec/android/app/easylauncher/Launcher$2;
.super Landroid/support/v4/app/ActionBarDrawerToggle;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/Launcher;->setupDrawerView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/Launcher;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/Launcher;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 6
    .param p2, "x0"    # Landroid/app/Activity;
    .param p3, "x1"    # Landroid/support/v4/widget/DrawerLayout;
    .param p4, "x2"    # I
    .param p5, "x3"    # I
    .param p6, "x4"    # I

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/Launcher$2;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 381
    const-string v0, "EasyLauncher.Launcher"

    const-string v1, "onDrawerClosed ----"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$2;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->invalidateOptionsMenu()V

    .line 385
    sget-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$2;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForEditmode(Z)V

    .line 391
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$2;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    # getter for: Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/Launcher;->access$000(Lcom/sec/android/app/easylauncher/Launcher;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 394
    const-string v0, "EasyLauncher.Launcher"

    const-string v1, "onDrawerOpened ----"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$2;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->invalidateOptionsMenu()V

    .line 397
    return-void
.end method
