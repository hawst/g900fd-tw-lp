.class public Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;
.super Ljava/lang/Object;
.source "DefaultAppsLoader.java"


# static fields
.field static final CSC_FILE_DEFAULT_WORKSPACE:Ljava/lang/String; = "/system/csc/easylauncher_default_workspace.xml"

.field private static final DEBUGGABLE:Z

.field private static final TAG_APP_SHORCUT:Ljava/lang/String; = "appshortcut"

.field private static final TAG_APP_WIDGETS:Ljava/lang/String; = "appwidget"

.field private static final TAG_ATTR_CLASSNAME:Ljava/lang/String; = "className"

.field private static final TAG_ATTR_PACKAGENAME:Ljava/lang/String; = "packageName"

.field private static final TAG_ATTR_POSITION:Ljava/lang/String; = "iconPosition"

.field private static final TAG_ATTR_SCREEN:Ljava/lang/String; = "screen"

.field private static final TAG_ATTR_SPANX:Ljava/lang/String; = "spanX"

.field private static final TAG_ATTR_VIEWTYPE:Ljava/lang/String; = "viewType"

.field private static final TAG_ATTR_VISIBILITY:Ljava/lang/String; = "visibility"

.field private static final TAG_EASY_APPS:Ljava/lang/String; = "easyapps"

.field private static final TAG_EASY_VIEWS:Ljava/lang/String; = "easyview"

.field private static final TAG_FAVORITES:Ljava/lang/String; = "favorites"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

.field private fileReader:Ljava/io/FileReader;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 48
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, "EasyLauncher.DefaultAppsLoader"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->TAG:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    .line 81
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->mContext:Landroid/content/Context;

    .line 82
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->doWeHaveDatabase()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->loadXMLIntoDatabase()V

    .line 86
    :cond_0
    return-void
.end method

.method private doWeHaveDatabase()Z
    .locals 4

    .prologue
    .line 89
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    if-nez v1, :cond_0

    .line 90
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getNewRowId()J

    move-result-wide v2

    long-to-int v0, v2

    .line 94
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 95
    const/4 v1, 0x1

    .line 97
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;
    .locals 10
    .param p1, "cscFilePath"    # Ljava/lang/String;

    .prologue
    .line 102
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v1, "cscFileChk":Ljava/io/File;
    const/4 v0, 0x0

    .line 104
    .local v0, "cscFile":Ljava/io/FileReader;
    const/4 v4, 0x0

    .line 106
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-boolean v5, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v5, :cond_0

    .line 107
    const-string v5, "EasyLauncher.DefaultAppsLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " CSC File Path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 111
    :try_start_0
    new-instance v0, Ljava/io/FileReader;

    .end local v0    # "cscFile":Ljava/io/FileReader;
    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    :goto_0
    if-eqz v0, :cond_1

    .line 121
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 122
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 123
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 124
    invoke-interface {v4, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 125
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    .line 136
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_1
    :goto_1
    return-object v4

    .line 112
    .end local v0    # "cscFile":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Ljava/io/FileNotFoundException;
    const/4 v0, 0x0

    .line 115
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 126
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 128
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const/4 v4, 0x0

    .line 129
    :try_start_2
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 131
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    goto :goto_1

    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v5

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    throw v5
.end method


# virtual methods
.method public loadXMLIntoDatabase()V
    .locals 30

    .prologue
    .line 140
    sget-boolean v27, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v27, :cond_0

    .line 141
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "loadXMLIntoDatabase"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->fillEmptyApps()V

    .line 144
    const/16 v27, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    .line 147
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v10

    .line 148
    .local v10, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/16 v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v10, v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 149
    const/16 v16, 0x0

    .local v16, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v6, 0x0

    .line 150
    .local v6, "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v20, 0x0

    .line 151
    .local v20, "resParser":Landroid/content/res/XmlResourceParser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 152
    .local v18, "res":Landroid/content/res/Resources;
    const v19, 0x7f050001

    .line 154
    .local v19, "resId":I
    const-string v27, "/system/csc/easylauncher_default_workspace.xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 155
    if-nez v6, :cond_6

    .line 156
    sget-boolean v27, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v27, :cond_1

    .line 157
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "CSC is not available"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_1
    const v19, 0x7f050001

    .line 159
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v20

    .line 160
    move-object/from16 v16, v20

    .line 167
    :goto_0
    const-string v27, "favorites"

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 168
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    .line 171
    .local v7, "depth":I
    :cond_2
    :goto_1
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v24

    .local v24, "type":I
    const/16 v27, 0x3

    move/from16 v0, v24

    move/from16 v1, v27

    if-ne v0, v1, :cond_3

    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v27

    move/from16 v0, v27

    if-le v0, v7, :cond_4

    :cond_3
    const/16 v27, 0x1

    move/from16 v0, v24

    move/from16 v1, v27

    if-eq v0, v1, :cond_4

    .line 172
    const/16 v27, 0x1

    move/from16 v0, v24

    move/from16 v1, v27

    if-ne v0, v1, :cond_8

    .line 298
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    .line 300
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    .line 306
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v18    # "res":Landroid/content/res/Resources;
    .end local v19    # "resId":I
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v24    # "type":I
    :cond_5
    :goto_2
    return-void

    .line 162
    .restart local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18    # "res":Landroid/content/res/Resources;
    .restart local v19    # "resId":I
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    :cond_6
    :try_start_2
    sget-boolean v27, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v27, :cond_7

    .line 163
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "CSC is available"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_7
    move-object/from16 v16, v6

    goto :goto_0

    .line 176
    .restart local v7    # "depth":I
    .restart local v24    # "type":I
    :cond_8
    const/16 v27, 0x2

    move/from16 v0, v24

    move/from16 v1, v27

    if-ne v0, v1, :cond_2

    .line 180
    const/4 v15, 0x0

    .line 181
    .local v15, "packageName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 182
    .local v5, "className":Ljava/lang/String;
    const/16 v21, 0x0

    .line 183
    .local v21, "screen":I
    const/16 v17, 0x0

    .line 184
    .local v17, "position":I
    const/16 v23, 0x0

    .line 185
    .local v23, "spanX":I
    const/16 v26, 0x0

    .line 186
    .local v26, "visibility":I
    const/16 v25, 0x0

    .line 188
    .local v25, "viewType":Ljava/lang/String;
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 190
    .local v12, "name":Ljava/lang/String;
    if-eqz v12, :cond_2

    .line 191
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v22

    .line 193
    .local v22, "size":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    move/from16 v0, v22

    if-ge v11, v0, :cond_11

    .line 194
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 195
    .local v3, "attrName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "attrValue":Ljava/lang/String;
    if-eqz v3, :cond_9

    const-string v27, "packageName"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 198
    move-object v15, v4

    .line 201
    :cond_9
    if-eqz v3, :cond_a

    const-string v27, "className"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 202
    move-object v5, v4

    .line 205
    :cond_a
    if-eqz v3, :cond_b

    const-string v27, "screen"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 206
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 209
    :cond_b
    if-eqz v3, :cond_c

    const-string v27, "spanX"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 210
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 213
    :cond_c
    if-eqz v3, :cond_d

    const-string v27, "iconPosition"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 214
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 217
    :cond_d
    if-eqz v3, :cond_e

    const-string v27, "viewType"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 218
    move-object/from16 v25, v4

    .line 221
    :cond_e
    if-eqz v3, :cond_f

    const-string v27, "visibility"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 222
    const-string v27, "true"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 223
    const/16 v26, 0x1

    .line 193
    :cond_f
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 225
    :cond_10
    const/16 v26, 0x0

    goto :goto_4

    .line 230
    .end local v3    # "attrName":Ljava/lang/String;
    .end local v4    # "attrValue":Ljava/lang/String;
    :cond_11
    sget-boolean v27, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v27, :cond_12

    .line 231
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Package Name: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", Class name: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", IconPosition: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_12
    const-string v27, "appshortcut"

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_14

    .line 235
    new-instance v13, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-direct {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>()V

    .line 236
    .local v13, "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v13, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 237
    invoke-virtual {v13, v5}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 238
    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setScreen(I)V

    .line 239
    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPosition(I)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateAppInfo(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 280
    .end local v13    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_13
    :goto_5
    const/4 v15, 0x0

    .line 281
    const/4 v5, 0x0

    .line 282
    goto/16 :goto_1

    .line 241
    :cond_14
    const-string v27, "appwidget"

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_15

    .line 242
    new-instance v14, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-direct {v14}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;-><init>()V

    .line 243
    .local v14, "newWidget":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {v14, v15}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPackageName(Ljava/lang/String;)V

    .line 244
    invoke-virtual {v14, v5}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setClassName(Ljava/lang/String;)V

    .line 245
    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 246
    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPosition(I)V

    .line 247
    move/from16 v0, v23

    invoke-virtual {v14, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setSpanX(I)V

    .line 248
    const/16 v27, -0x64

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetid(I)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v14}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->inttializeWidgetsAdded(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 285
    .end local v5    # "className":Ljava/lang/String;
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "i":I
    .end local v12    # "name":Ljava/lang/String;
    .end local v14    # "newWidget":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "position":I
    .end local v18    # "res":Landroid/content/res/Resources;
    .end local v19    # "resId":I
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "screen":I
    .end local v22    # "size":I
    .end local v23    # "spanX":I
    .end local v24    # "type":I
    .end local v25    # "viewType":Ljava/lang/String;
    .end local v26    # "visibility":I
    :catch_0
    move-exception v8

    .line 286
    .local v8, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "Got exception parsing favorites."

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 287
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    .line 300
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 301
    :catch_1
    move-exception v8

    .line 302
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 250
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v5    # "className":Ljava/lang/String;
    .restart local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v7    # "depth":I
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v11    # "i":I
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v15    # "packageName":Ljava/lang/String;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v17    # "position":I
    .restart local v18    # "res":Landroid/content/res/Resources;
    .restart local v19    # "resId":I
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v21    # "screen":I
    .restart local v22    # "size":I
    .restart local v23    # "spanX":I
    .restart local v24    # "type":I
    .restart local v25    # "viewType":Ljava/lang/String;
    .restart local v26    # "visibility":I
    :cond_15
    :try_start_5
    const-string v27, "easyapps"

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_16

    .line 251
    new-instance v2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;

    invoke-direct {v2}, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;-><init>()V

    .line 252
    .local v2, "appinfo":Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    iput-object v15, v2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->packageName:Ljava/lang/String;

    .line 253
    iput-object v5, v2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->className:Ljava/lang/String;

    .line 261
    move/from16 v0, v26

    iput v0, v2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->visibility:I

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->insertInEmptyApps(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;)V

    .line 263
    sget-boolean v27, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v27, :cond_13

    .line 264
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Easy Apps: Package Name: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", Class name: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", IconPosition: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_5

    .line 288
    .end local v2    # "appinfo":Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    .end local v5    # "className":Ljava/lang/String;
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "i":I
    .end local v12    # "name":Ljava/lang/String;
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "position":I
    .end local v18    # "res":Landroid/content/res/Resources;
    .end local v19    # "resId":I
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "screen":I
    .end local v22    # "size":I
    .end local v23    # "spanX":I
    .end local v24    # "type":I
    .end local v25    # "viewType":Ljava/lang/String;
    .end local v26    # "visibility":I
    :catch_2
    move-exception v8

    .line 289
    .local v8, "e":Ljava/io/IOException;
    :try_start_6
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "Got exception parsing favorites."

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 290
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    .line 300
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_2

    .line 301
    :catch_3
    move-exception v8

    .line 302
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 266
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v5    # "className":Ljava/lang/String;
    .restart local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v7    # "depth":I
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v11    # "i":I
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v15    # "packageName":Ljava/lang/String;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v17    # "position":I
    .restart local v18    # "res":Landroid/content/res/Resources;
    .restart local v19    # "resId":I
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v21    # "screen":I
    .restart local v22    # "size":I
    .restart local v23    # "spanX":I
    .restart local v24    # "type":I
    .restart local v25    # "viewType":Ljava/lang/String;
    .restart local v26    # "visibility":I
    :cond_16
    :try_start_8
    const-string v27, "easyview"

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 267
    new-instance v9, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;

    invoke-direct {v9}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;-><init>()V

    .line 268
    .local v9, "easyView":Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;
    invoke-virtual {v9, v15}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setPackageName(Ljava/lang/String;)V

    .line 269
    invoke-virtual {v9, v5}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setClassName(Ljava/lang/String;)V

    .line 270
    move/from16 v0, v21

    invoke-virtual {v9, v0}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setScreen(I)V

    .line 271
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setPosition(I)V

    .line 272
    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setViewType(Ljava/lang/String;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addEasyViewInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;)V

    .line 274
    sget-boolean v27, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->DEBUGGABLE:Z

    if-eqz v27, :cond_13

    .line 275
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Easy Views: Package Name: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", Class name: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", IconPosition: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " , ViewType: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_5

    .line 291
    .end local v5    # "className":Ljava/lang/String;
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v9    # "easyView":Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "i":I
    .end local v12    # "name":Ljava/lang/String;
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "position":I
    .end local v18    # "res":Landroid/content/res/Resources;
    .end local v19    # "resId":I
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "screen":I
    .end local v22    # "size":I
    .end local v23    # "spanX":I
    .end local v24    # "type":I
    .end local v25    # "viewType":Ljava/lang/String;
    .end local v26    # "visibility":I
    :catch_4
    move-exception v8

    .line 292
    .local v8, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_9
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "Got exception parsing favorites."

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 293
    invoke-virtual {v8}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    .line 300
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_2

    .line 301
    :catch_5
    move-exception v8

    .line 302
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 301
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v7    # "depth":I
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18    # "res":Landroid/content/res/Resources;
    .restart local v19    # "resId":I
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v24    # "type":I
    :catch_6
    move-exception v8

    .line 302
    .restart local v8    # "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 294
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v18    # "res":Landroid/content/res/Resources;
    .end local v19    # "resId":I
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v24    # "type":I
    :catch_7
    move-exception v8

    .line 295
    .local v8, "e":Ljava/lang/NullPointerException;
    :try_start_b
    const-string v27, "EasyLauncher.DefaultAppsLoader"

    const-string v28, "Got exception parsing favorites."

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 296
    invoke-virtual {v8}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    .line 300
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/FileReader;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    goto/16 :goto_2

    .line 301
    :catch_8
    move-exception v8

    .line 302
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 298
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v28, v0

    if-eqz v28, :cond_17

    .line 300
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->fileReader:Ljava/io/FileReader;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/io/FileReader;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    .line 303
    :cond_17
    :goto_6
    throw v27

    .line 301
    :catch_9
    move-exception v8

    .line 302
    .restart local v8    # "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6
.end method

.method public unBind()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 311
    :cond_0
    return-void
.end method
