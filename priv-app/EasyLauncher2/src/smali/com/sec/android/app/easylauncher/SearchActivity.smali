.class public Lcom/sec/android/app/easylauncher/SearchActivity;
.super Landroid/app/Activity;
.source "SearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;,
        Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;,
        Lcom/sec/android/app/easylauncher/SearchActivity$Filter;
    }
.end annotation


# instance fields
.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

.field mApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field mBackbutton:Landroid/widget/LinearLayout;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field mSearchEdit:Landroid/widget/SearchView;

.field private mSkippedFirst:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 174
    new-instance v0, Lcom/sec/android/app/easylauncher/SearchActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/SearchActivity$1;-><init>(Lcom/sec/android/app/easylauncher/SearchActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 209
    new-instance v0, Lcom/sec/android/app/easylauncher/SearchActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/SearchActivity$2;-><init>(Lcom/sec/android/app/easylauncher/SearchActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 246
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/SearchActivity;Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/SearchActivity;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/SearchActivity;->addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    return-void
.end method

.method private addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 5
    .param p1, "appInfo"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 237
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 238
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ScreenPosion"

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->APPS_SCREEN_POSITION:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 239
    .local v1, "editScreen":I
    invoke-virtual {p1, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setScreen(I)V

    .line 241
    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateAppInfo(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 242
    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 244
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/SearchActivity;->finish()V

    .line 138
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity;->requestWindowFeature(I)Z

    .line 89
    const v3, 0x7f030009

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity;->setContentView(I)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    .line 91
    .local v0, "appContext":Lcom/sec/android/app/easylauncher/EasyController;
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v3

    if-nez v3, :cond_0

    .line 92
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->loadApps()V

    .line 94
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    .line 95
    const v3, 0x7f0c001b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SearchView;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->requestFocus()Z

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v3, p0}, Landroid/widget/SearchView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v3, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 116
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSkippedFirst:Z

    .line 117
    const v3, 0x7f0c0016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 118
    .local v2, "lv":Landroid/widget/ListView;
    new-instance v3, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-direct {v3, p0, p0}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;-><init>(Lcom/sec/android/app/easylauncher/SearchActivity;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->runFilter()V

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/EasyController;->canRotate()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity;->setRequestedOrientation(I)V

    .line 127
    :cond_1
    const v3, 0x7f0c001a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mBackbutton:Landroid/widget/LinearLayout;

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mBackbutton:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 130
    .local v1, "filter":Landroid/content/IntentFilter;
    sget-object v3, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/easylauncher/SearchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 132
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->unBind()V

    .line 144
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    .line 145
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    .line 146
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mBackbutton:Landroid/widget/LinearLayout;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/SearchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 150
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 59
    const v0, 0x7f040001

    const/high16 v1, 0x7f040000

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/SearchActivity;->overridePendingTransition(II)V

    .line 60
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 61
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSkippedFirst:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->runFilter()V

    .line 157
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity;->mSkippedFirst:Z

    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 67
    const/high16 v0, 0x7f040000

    const v1, 0x7f040002

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/SearchActivity;->overridePendingTransition(II)V

    .line 68
    return-void
.end method
