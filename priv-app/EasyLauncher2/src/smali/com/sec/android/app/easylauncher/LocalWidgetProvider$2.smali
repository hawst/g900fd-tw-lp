.class Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;
.super Ljava/lang/Object;
.source "LocalWidgetProvider.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->addAppWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

.field final synthetic val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    iput-object p2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBindFail()V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setHostView(Landroid/appwidget/AppWidgetHostView;)V

    .line 234
    const-string v0, "EasyLauncher.LocalWidgetProvider"

    const-string v1, "Problem binding widget. This should only happen when installed outside of /system/app and user declined authorization."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-void
.end method

.method public onBindSuccess(I)V
    .locals 3
    .param p1, "appWidgetId"    # I

    .prologue
    .line 219
    # getter for: Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "EasyLauncher.LocalWidgetProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addAppWidget after bindWidgetAllowed WidgetId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetid(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    # getter for: Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;
    invoke-static {v1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->access$100(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->val$Info:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    # invokes: Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->bindTheWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->access$200(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWaitingWidgetBinding:Z

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWaitingWidgetBinding:Z

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;->this$0:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->startloading()V

    .line 229
    :cond_1
    return-void
.end method
