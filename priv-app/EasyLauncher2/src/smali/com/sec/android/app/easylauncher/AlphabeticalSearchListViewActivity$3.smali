.class Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;
.super Ljava/lang/Object;
.source "AlphabeticalSearchListViewActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 389
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v5

    if-nez v5, :cond_1

    .line 390
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 391
    .local v0, "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 392
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 393
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-static {v3, v5}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    .line 414
    .end local v0    # "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EditPosition"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 396
    .local v1, "editposition":I
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "HelpMode"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    # setter for: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpRunning:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$002(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;Z)Z

    .line 398
    if-eq v1, v8, :cond_0

    .line 400
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    const-string v6, "input_method"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 401
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v2, v5, v9}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 402
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 404
    .restart local v0    # "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPosition(I)V

    .line 405
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    # invokes: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    invoke-static {v5, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$100(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 406
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 407
    .local v4, "returnIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v5, v8, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 408
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->finish()V

    .line 410
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpRunning:Z
    invoke-static {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$000(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 411
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/easylauncher/EasyController;->removefromMap(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    goto :goto_0
.end method
