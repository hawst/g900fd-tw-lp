.class Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;
.super Ljava/lang/Object;
.source "AlphabeticalSearchListViewActivity.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$Filter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filterWidgets()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 358
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 359
    .local v2, "out":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const-string v3, ""

    .line 360
    .local v3, "search":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    if-eqz v4, :cond_0

    .line 361
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 362
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    if-eqz v4, :cond_1

    .line 363
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->setSearchText(Ljava/lang/String;)V

    .line 364
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 365
    .local v0, "apps":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 366
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 369
    .end local v0    # "apps":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_3
    return-object v2
.end method
