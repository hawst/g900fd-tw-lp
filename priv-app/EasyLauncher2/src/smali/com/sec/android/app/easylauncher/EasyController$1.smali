.class Lcom/sec/android/app/easylauncher/EasyController$1;
.super Landroid/content/BroadcastReceiver;
.source "EasyController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/EasyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/EasyController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/EasyController;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 284
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "details":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/easylauncher/EasyController;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->access$200()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 286
    const-string v12, "EasyLauncher.EasyController"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Updated Package Details : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    const/4 v6, 0x0

    .line 288
    .local v6, "packagename":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 289
    const-string v12, ":"

    invoke-virtual {v1, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 290
    :cond_1
    if-eqz v6, :cond_8

    array-length v12, v6

    const/4 v13, 0x1

    if-le v12, v13, :cond_8

    const/4 v12, 0x1

    aget-object v12, v6, v12

    if-eqz v12, :cond_8

    .line 291
    const/4 v10, 0x0

    .line 292
    .local v10, "sendIntent":Z
    const/4 v7, 0x0

    .line 293
    .local v7, "removePackage":Z
    const/4 v5, 0x0

    .line 295
    .local v5, "matches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const-string v12, "android.intent.extra.REPLACING"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 297
    .local v8, "replacing":Z
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    const-string v13, "android.intent.action.PACKAGE_CHANGED"

    if-eq v12, v13, :cond_2

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    const-string v13, "android.intent.action.PACKAGE_ADDED"

    if-ne v12, v13, :cond_3

    if-nez v8, :cond_3

    .line 298
    :cond_2
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v12}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v6, v13

    invoke-static {v12, v13}, Lcom/sec/android/app/easylauncher/lib/Lib;->findActivitiesForPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 300
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 301
    const/4 v7, 0x1

    .line 304
    :cond_3
    const-string v12, "EasyLauncher.EasyController"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "removePackage : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    const-string v13, "android.intent.action.PACKAGE_REMOVED"

    if-eq v12, v13, :cond_4

    if-eqz v7, :cond_9

    .line 306
    :cond_4
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v12, v12, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v12}, Ljava/util/concurrent/ConcurrentSkipListMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 307
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 308
    .local v11, "value":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 309
    .local v4, "key":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v6, v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 310
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v12, v12, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v12, v4}, Ljava/util/concurrent/ConcurrentSkipListMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 315
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    .end local v4    # "key":Ljava/lang/String;
    .end local v11    # "value":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_6
    const/4 v10, 0x1

    .line 330
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_7
    :goto_1
    if-eqz v10, :cond_8

    .line 331
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 332
    .local v9, "sendBroadcast":Landroid/content/Intent;
    sget-object v12, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v9, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v12, v9}, Lcom/sec/android/app/easylauncher/EasyController;->sendBroadcast(Landroid/content/Intent;)V

    .line 337
    .end local v5    # "matches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v7    # "removePackage":Z
    .end local v8    # "replacing":Z
    .end local v9    # "sendBroadcast":Landroid/content/Intent;
    .end local v10    # "sendIntent":Z
    :cond_8
    return-void

    .line 316
    .restart local v5    # "matches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v7    # "removePackage":Z
    .restart local v8    # "replacing":Z
    .restart local v10    # "sendIntent":Z
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    const-string v13, "android.intent.action.PACKAGE_CHANGED"

    if-eq v12, v13, :cond_a

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    const-string v13, "android.intent.action.PACKAGE_ADDED"

    if-ne v12, v13, :cond_b

    if-nez v8, :cond_b

    .line 318
    :cond_a
    const-string v12, "EasyLauncher.EasyController"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " has been called for "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v6, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v12}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v12

    if-eqz v12, :cond_7

    if-eqz v5, :cond_7

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_7

    .line 321
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v12}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v13, v13, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-static {v12, v13, v5}, Lcom/sec/android/app/easylauncher/lib/Lib;->getApplicationsWithClass(Landroid/content/Context;Ljava/util/concurrent/ConcurrentSkipListMap;Ljava/util/List;)V

    .line 323
    const/4 v10, 0x1

    goto :goto_1

    .line 327
    :cond_b
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/EasyController$1;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v12}, Lcom/sec/android/app/easylauncher/EasyController;->startAsyncTask()V

    goto :goto_1
.end method
