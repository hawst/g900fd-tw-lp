.class public Lcom/sec/android/app/easylauncher/ScalarAnimator;
.super Ljava/lang/Object;
.source "ScalarAnimator.java"


# instance fields
.field private mCurrent:F

.field private mDefaultDuration:J

.field private mDelay:J

.field private mDelta:F

.field private mDuration:J

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mInverseDuration:F

.field private mIsRunning:Z

.field private mStart:F

.field private mStartTime:J

.field private mStop:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 35
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 37
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 39
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 41
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 59
    return-void
.end method

.method public constructor <init>(J)V
    .locals 5
    .param p1, "defaultDuration"    # J

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 35
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 37
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 39
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 41
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 69
    iput-wide p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 70
    return-void
.end method

.method public constructor <init>(JF)V
    .locals 5
    .param p1, "defaultDuration"    # J
    .param p3, "initialStart"    # F

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 35
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 37
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 39
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 41
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 93
    iput-wide p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 94
    iput p3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    iput p3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 95
    return-void
.end method

.method public constructor <init>(JFLandroid/view/animation/Interpolator;)V
    .locals 5
    .param p1, "defaultDuration"    # J
    .param p3, "initialStart"    # F
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 35
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 37
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 39
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 41
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 124
    iput-wide p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 125
    iput p3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    iput p3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 126
    iput-object p4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 127
    return-void
.end method

.method public constructor <init>(JLandroid/view/animation/Interpolator;)V
    .locals 5
    .param p1, "defaultDuration"    # J
    .param p3, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 35
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 37
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 39
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 41
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 107
    iput-wide p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 108
    iput-object p3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 4
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 35
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 37
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 39
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 41
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 45
    iput v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 80
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 81
    return-void
.end method


# virtual methods
.method public abort()Z
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->abort(F)Z

    move-result v0

    return v0
.end method

.method public abort(F)Z
    .locals 4
    .param p1, "stop"    # F

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->isAnimating()Z

    move-result v0

    .line 152
    .local v0, "aborted":Z
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 153
    iput p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    iput p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 154
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 155
    return v0
.end method

.method public extend(FJ)V
    .locals 4
    .param p1, "stop"    # F
    .param p2, "extendDuration"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 173
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    cmp-long v0, p2, v2

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    if-nez v0, :cond_2

    .line 175
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->start(FFJ)V

    .line 186
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    iput p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 178
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    iget v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 179
    iget-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 180
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    .line 181
    :cond_3
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    goto :goto_0

    .line 183
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    iget-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInverseDuration:F

    goto :goto_0
.end method

.method public get()F
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->get(J)F

    move-result v0

    return v0
.end method

.method public get(J)F
    .locals 11
    .param p1, "currentTime"    # J

    .prologue
    const-wide/16 v8, 0x0

    .line 213
    iget-boolean v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    if-eqz v3, :cond_1

    .line 216
    iget-wide v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStartTime:J

    sub-long v4, p1, v4

    iget-wide v6, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    sub-long v0, v4, v6

    .line 217
    .local v0, "elapsedTime":J
    cmp-long v3, v0, v8

    if-gtz v3, :cond_0

    .line 219
    const-wide/16 v0, 0x0

    .line 220
    iget v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    iput v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 224
    :cond_0
    iget-wide v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    cmp-long v3, v0, v4

    if-ltz v3, :cond_2

    .line 225
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 226
    iget v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    iput v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    .line 240
    .end local v0    # "elapsedTime":J
    :cond_1
    :goto_0
    iget v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    return v3

    .line 227
    .restart local v0    # "elapsedTime":J
    :cond_2
    cmp-long v3, v0, v8

    if-lez v3, :cond_1

    .line 229
    long-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInverseDuration:F

    mul-float v2, v3, v4

    .line 232
    .local v2, "factor":F
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    if-eqz v3, :cond_3

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    .line 234
    :cond_3
    iget v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    iget v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    goto :goto_0
.end method

.method public getFactor(J)F
    .locals 9
    .param p1, "currentTime"    # J

    .prologue
    .line 257
    const/4 v2, 0x0

    .line 258
    .local v2, "factor":F
    iget-wide v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    iget-wide v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStartTime:J

    cmp-long v3, p1, v4

    if-lez v3, :cond_0

    .line 259
    iget-wide v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStartTime:J

    sub-long v0, p1, v4

    .line 260
    .local v0, "elapsedTime":J
    iget-wide v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    cmp-long v3, v0, v4

    if-ltz v3, :cond_2

    .line 261
    const/high16 v2, 0x3f800000    # 1.0f

    .line 267
    .end local v0    # "elapsedTime":J
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    if-eqz v3, :cond_1

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    .line 269
    :cond_1
    return v2

    .line 263
    .restart local v0    # "elapsedTime":J
    :cond_2
    long-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInverseDuration:F

    mul-float v2, v3, v4

    goto :goto_0
.end method

.method public getRemaining()F
    .locals 2

    .prologue
    .line 279
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    iget v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    return v0
.end method

.method public setDefaultDuration(J)V
    .locals 1
    .param p1, "defaultDuration"    # J

    .prologue
    .line 369
    iput-wide p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    .line 370
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 0
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 381
    return-void
.end method

.method public start(FF)V
    .locals 2
    .param p1, "start"    # F
    .param p2, "stop"    # F

    .prologue
    .line 304
    iget-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->start(FFJ)V

    .line 305
    return-void
.end method

.method public start(FFJ)V
    .locals 9
    .param p1, "start"    # F
    .param p2, "stop"    # F
    .param p3, "duration"    # J

    .prologue
    .line 324
    const-wide/16 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->start(FFJJ)V

    .line 325
    return-void
.end method

.method public start(FFJJ)V
    .locals 5
    .param p1, "start"    # F
    .param p2, "stop"    # F
    .param p3, "duration"    # J
    .param p5, "delay"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 345
    iput-wide p5, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelay:J

    .line 346
    iput p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    iput p1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    .line 347
    iput p2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    .line 348
    iput-wide p3, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 349
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    iget v1, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStart:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    .line 350
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDelta:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 351
    :cond_0
    iput-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    .line 356
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    .line 357
    return-void

    .line 353
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    iget-wide v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDuration:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mInverseDuration:F

    .line 354
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStartTime:J

    goto :goto_0
.end method

.method public to(F)V
    .locals 2
    .param p1, "stop"    # F

    .prologue
    .line 394
    iget-wide v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mDefaultDuration:J

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->to(FJ)V

    .line 395
    return-void
.end method

.method public to(FJ)V
    .locals 6
    .param p1, "stop"    # F
    .param p2, "duration"    # J

    .prologue
    .line 412
    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->to(FJJ)V

    .line 413
    return-void
.end method

.method public to(FJJ)V
    .locals 8
    .param p1, "stop"    # F
    .param p2, "duration"    # J
    .param p4, "delay"    # J

    .prologue
    const/4 v1, 0x0

    .line 431
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 432
    iget v2, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mCurrent:F

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->start(FFJJ)V

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    iget v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mStop:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/ScalarAnimator;->mIsRunning:Z

    if-eqz v0, :cond_0

    .line 437
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->abort(F)Z

    goto :goto_0
.end method
