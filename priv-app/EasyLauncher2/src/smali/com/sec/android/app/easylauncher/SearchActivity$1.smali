.class Lcom/sec/android/app/easylauncher/SearchActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/SearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/SearchActivity;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 178
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    .line 179
    .local v0, "appContext":Lcom/sec/android/app/easylauncher/EasyController;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    if-eqz v1, :cond_1

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    if-nez v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v1, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$1;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->runFilter()V

    .line 186
    :cond_1
    return-void
.end method
