.class public Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;
.super Landroid/app/Activity;
.source "AlphabeticalListViewActivity.java"


# static fields
.field public static final PREFS_NAME:Ljava/lang/String; = "FavoriteAppsList"

.field private static final REQUEST_TO_SEARCHVIEW:I = 0x65

.field public static final TAG:Ljava/lang/String; = "EasyLauncher.AlphabeticalListViewActivity"


# instance fields
.field private apps:Landroid/widget/ListView;

.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mActivityName:Landroid/content/ComponentName;

.field mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

.field public mAppData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppMarketIntent:Landroid/content/Intent;

.field private mContext:Landroid/content/Context;

.field private mFreshActivity:Z

.field mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

.field private mHelpRunning:Z

.field mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

.field mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    .line 154
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$1;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 259
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    return-void
.end method

.method private addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 5
    .param p1, "appInfo"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 293
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 294
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    .line 296
    :cond_0
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 297
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ScreenPosion"

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->APPS_SCREEN_POSITION:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 298
    .local v1, "editScreen":I
    invoke-virtual {p1, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setScreen(I)V

    .line 300
    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateAppInfo(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 301
    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 303
    :cond_1
    return-void
.end method

.method private populateData()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->apps:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 255
    const v0, 0x7f0c0016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->apps:Landroid/widget/ListView;

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->apps:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 257
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 216
    const/16 v1, 0x65

    if-ne p1, v1, :cond_0

    .line 218
    if-ne p2, v2, :cond_0

    .line 219
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 220
    .local v0, "returnIntent":Landroid/content/Intent;
    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->finish()V

    .line 224
    .end local v0    # "returnIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 245
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 247
    return-void
.end method

.method public onClickAppMarketButton()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    const-string v1, "app market"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 230
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mFreshActivity:Z

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    .line 108
    const v2, 0x7f030004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->setContentView(I)V

    .line 110
    const v2, 0x7f0c0015

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    .line 111
    new-instance v2, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/EasyController;->loadApps()V

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    if-eqz v2, :cond_1

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->setAdapter(Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;)V

    .line 122
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "HelpMode"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z

    .line 124
    new-instance v3, Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/easylauncher/HelpGuider;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    .line 126
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z

    if-eqz v2, :cond_2

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addapps(I)V

    .line 130
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/EasyController;->canRotate()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 131
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->setRequestedOrientation(I)V

    .line 134
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.APP_MARKET"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 138
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mActivityName:Landroid/content/ComponentName;

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mActivityName:Landroid/content/ComponentName;

    if-eqz v2, :cond_4

    .line 140
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    .line 143
    :cond_4
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 144
    .local v0, "filter":Landroid/content/IntentFilter;
    sget-object v2, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146
    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 147
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 148
    const-string v2, "android.intent.action.PACKAGE_INSTALL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->populateData()V

    .line 152
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 173
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 176
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    const v1, 0x7f0b0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 178
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->unBind()V

    .line 309
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->apps:Landroid/widget/ListView;

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 311
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 313
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    .line 314
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 315
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 191
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 208
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 193
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->onClickAppMarketButton()V

    goto :goto_0

    .line 197
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 199
    .local v0, "intent":Landroid/content/Intent;
    const-class v2, Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 200
    const/16 v2, 0x65

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 202
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 203
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x7f0c005c
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 240
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 241
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 51
    const v0, 0x7f040001

    const/high16 v1, 0x7f040000

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->overridePendingTransition(II)V

    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 53
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 69
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/high16 v2, 0x7f040000

    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 58
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mFreshActivity:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 59
    const v0, 0x7f040001

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->overridePendingTransition(II)V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mFreshActivity:Z

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    const v0, 0x7f040002

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "tag"    # Ljava/lang/Object;

    .prologue
    .line 233
    invoke-static {p1, p0}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    .line 234
    const/4 v0, 0x0

    return v0
.end method
