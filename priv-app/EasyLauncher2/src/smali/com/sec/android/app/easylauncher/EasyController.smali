.class public Lcom/sec/android/app/easylauncher/EasyController;
.super Landroid/app/Application;
.source "EasyController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;,
        Lcom/sec/android/app/easylauncher/EasyController$MyComparator;
    }
.end annotation


# static fields
.field public static ACTION_UPDATE_DATA:Ljava/lang/String; = null

.field public static APPS_FRAGMENT_DEFAULT_APPS_COUNT:I = 0x0

.field public static APPS_SCREEN_POSITION:I = 0x0

.field public static CENTER_FRAGMENT_APPS_COUNT:I = 0x0

.field public static CENTRE_SCREEN_POSITION:I = 0x0

.field public static CONTACT_SCREEN_POSITION:I = 0x0

.field public static CONTACT_WIDGET_POSITION:I = 0x0

.field private static final DEBUGGABLE:Z

.field public static MAX_PAGE_COUNT:I = 0x0

.field public static final PREFS:Ljava/lang/String; = "com.sec.android.app.easylauncher.prefs"

.field public static final PREFS_CONTACT_EDIT:Ljava/lang/String; = "PrefContactEdit"

.field private static Productname:Ljava/lang/String; = null

.field public static START_POSITION_WIDGET:I = 0x0

.field public static START_POSITION_WIDGET_CENTRE_SCREEN:I = 0x0

.field public static final TAG:Ljava/lang/String; = "EasyLauncher.EasyController"

.field public static mCurrentLanguage:Ljava/lang/String;

.field private static mEasyController:Lcom/sec/android/app/easylauncher/EasyController;

.field public static mIconDpi:I

.field public static mListIconDpi:I

.field private static mPageOnOff:[Z

.field static reRunTask:Z

.field private static sCollator:Ljava/text/Collator;


# instance fields
.field public PAGE_APPS1:I

.field PAGE_APPS2:I

.field PAGE_APPS3:I

.field PAGE_CONTACTS1:I

.field PAGE_CONTACTS2:I

.field PAGE_HOME:I

.field private cpuBooster:Landroid/os/DVFSHelper;

.field private doesMoreApps_exist:Z

.field mDefaultHomeApps:Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;

.field private mDefaultWidgetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation
.end field

.field mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

.field mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentSkipListMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private prefs_page_onoff:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/EasyController;->DEBUGGABLE:Z

    .line 41
    const-string v0, "UPdateListData"

    sput-object v0, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    .line 45
    sput-boolean v1, Lcom/sec/android/app/easylauncher/EasyController;->reRunTask:Z

    .line 55
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/easylauncher/EasyController;->mCurrentLanguage:Ljava/lang/String;

    .line 57
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/easylauncher/EasyController;->sCollator:Ljava/text/Collator;

    .line 61
    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->APPS_FRAGMENT_DEFAULT_APPS_COUNT:I

    .line 63
    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->CENTER_FRAGMENT_APPS_COUNT:I

    .line 76
    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET:I

    .line 91
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    .line 104
    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    new-array v0, v0, [Z

    sput-object v0, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->cpuBooster:Landroid/os/DVFSHelper;

    .line 84
    iput v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_CONTACTS1:I

    .line 85
    iput v3, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_CONTACTS2:I

    .line 86
    iput v4, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_HOME:I

    .line 87
    iput v5, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_APPS1:I

    .line 88
    iput v6, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_APPS2:I

    .line 89
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_APPS3:I

    .line 93
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact1_onoff"

    aput-object v1, v0, v2

    const-string v1, "contact2_onoff"

    aput-object v1, v0, v3

    const-string v1, "home_onoff"

    aput-object v1, v0, v4

    const-string v1, "apps1_onoff"

    aput-object v1, v0, v5

    const-string v1, "apps2_onoff"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "apps3_onoff"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->prefs_page_onoff:[Ljava/lang/String;

    .line 279
    new-instance v0, Lcom/sec/android/app/easylauncher/EasyController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/EasyController$1;-><init>(Lcom/sec/android/app/easylauncher/EasyController;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 111
    sput-object p0, Lcom/sec/android/app/easylauncher/EasyController;->mEasyController:Lcom/sec/android/app/easylauncher/EasyController;

    .line 112
    return-void
.end method

.method static synthetic access$100()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/easylauncher/EasyController;->sCollator:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lcom/sec/android/app/easylauncher/EasyController;->DEBUGGABLE:Z

    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/easylauncher/EasyController;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/sec/android/app/easylauncher/EasyController;->mEasyController:Lcom/sec/android/app/easylauncher/EasyController;

    return-object v0
.end method


# virtual methods
.method public addAppToMap(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 4
    .param p1, "newApp"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v2, v0, p1}, Ljava/util/concurrent/ConcurrentSkipListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 173
    .local v1, "sendBroadcast":Landroid/content/Intent;
    sget-object v2, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/EasyController;->sendBroadcast(Landroid/content/Intent;)V

    .line 175
    return-void
.end method

.method public canRotate()Z
    .locals 2

    .prologue
    .line 454
    sget-object v0, Lcom/sec/android/app/easylauncher/EasyController;->Productname:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 455
    sget-object v0, Lcom/sec/android/app/easylauncher/EasyController;->Productname:Ljava/lang/String;

    const-string v1, "mega"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    .line 458
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCpuBoosterObject()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->cpuBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method public getDefaultWidgetList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mDefaultWidgetList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMoreAppsListState()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->doesMoreApps_exist:Z

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 384
    const/4 v1, 0x0

    .line 385
    .local v1, "screenCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v0, v2, :cond_1

    .line 386
    sget-object v2, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 387
    add-int/lit8 v1, v1, 0x1

    .line 385
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 391
    :cond_1
    return v1
.end method

.method public getPageOnOff()[Z
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    return-object v0
.end method

.method loadApps()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 163
    new-instance v0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;-><init>(Lcom/sec/android/app/easylauncher/EasyController;Lcom/sec/android/app/easylauncher/EasyController$1;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 165
    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/EasyController;->setMoreAppsListState(Z)V

    .line 166
    return-void
.end method

.method public loadPageOnOff()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 424
    const/4 v2, -0x1

    .line 426
    .local v2, "onOff":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.easylauncher.prefs"

    invoke-virtual {v4, v5, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 429
    .local v3, "prefs":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v1, v4, :cond_6

    .line 431
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/EasyController;->prefs_page_onoff:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 433
    if-eq v2, v6, :cond_0

    if-nez v2, :cond_5

    .line 434
    :cond_0
    sget-object v4, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aput-boolean v8, v4, v1

    .line 440
    :goto_1
    iget v4, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_HOME:I

    if-eq v1, v4, :cond_1

    iget v4, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_APPS1:I

    if-ne v1, v4, :cond_2

    :cond_1
    sget-object v4, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aget-boolean v4, v4, v1

    if-eqz v4, :cond_3

    :cond_2
    iget v4, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_CONTACTS2:I

    if-ne v1, v4, :cond_4

    if-ne v2, v6, :cond_4

    .line 442
    :cond_3
    sget-object v4, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aput-boolean v7, v4, v1

    .line 444
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 446
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/EasyController;->prefs_page_onoff:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 447
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 429
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 437
    :cond_5
    sget-object v4, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aput-boolean v7, v4, v1

    goto :goto_1

    .line 450
    :cond_6
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/res/Configuration;

    .prologue
    .line 218
    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 219
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 220
    .local v0, "locale":Ljava/util/Locale;
    sget-boolean v1, Lcom/sec/android/app/easylauncher/EasyController;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 221
    const-string v1, "EasyLauncher.EasyController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCurrentLanguage"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/easylauncher/EasyController;->mCurrentLanguage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_0
    sget-object v1, Lcom/sec/android/app/easylauncher/EasyController;->mCurrentLanguage:Ljava/lang/String;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/easylauncher/EasyController;->mCurrentLanguage:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 223
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/easylauncher/EasyController;->sCollator:Ljava/text/Collator;

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentSkipListMap;->clear()V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->startAsyncTask()V

    .line 226
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/easylauncher/EasyController;->mCurrentLanguage:Ljava/lang/String;

    .line 229
    :cond_1
    return-void
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 127
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->setIconDpi()I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->mIconDpi:I

    .line 129
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController;->mDefaultHomeApps:Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;

    .line 130
    new-instance v1, Ljava/util/concurrent/ConcurrentSkipListMap;

    new-instance v2, Lcom/sec/android/app/easylauncher/EasyController$MyComparator;

    invoke-direct {v2}, Lcom/sec/android/app/easylauncher/EasyController$MyComparator;-><init>()V

    invoke-direct {v1, v2}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>(Ljava/util/Comparator;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 132
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v1, "android.intent.action.PACKAGE_INSTALL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/easylauncher/EasyController;->mCurrentLanguage:Ljava/lang/String;

    .line 140
    new-instance v1, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController;->cpuBooster:Landroid/os/DVFSHelper;

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->APPS_FRAGMENT_DEFAULT_APPS_COUNT:I

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->CENTER_FRAGMENT_APPS_COUNT:I

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->CONTACT_SCREEN_POSITION:I

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->APPS_SCREEN_POSITION:I

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->CONTACT_WIDGET_POSITION:I

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET_CENTRE_SCREEN:I

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET:I

    .line 156
    const-string v1, "ro.build.product"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/easylauncher/EasyController;->Productname:Ljava/lang/String;

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->loadPageOnOff()V

    .line 160
    return-void
.end method

.method public onTerminate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->clear()V

    .line 192
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mDefaultHomeApps:Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mDefaultHomeApps:Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;->unBind()V

    .line 195
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->mDefaultHomeApps:Lcom/sec/android/app/easylauncher/lib/DefaultAppsLoader;

    .line 196
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 197
    return-void
.end method

.method removefromMap(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 3
    .param p1, "newApp"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    return-void
.end method

.method public setDefaultWidgetList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/WidgetInfo;>;"
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyController;->mDefaultWidgetList:Ljava/util/ArrayList;

    .line 209
    return-void
.end method

.method public setIconDpi()I
    .locals 9

    .prologue
    const/16 v2, 0xf0

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 243
    .local v0, "appRes":Landroid/content/res/Resources;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    .line 245
    .local v4, "sysRes":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    sput v7, Lcom/sec/android/app/easylauncher/EasyController;->mListIconDpi:I

    .line 246
    if-nez v0, :cond_0

    .line 276
    :goto_0
    return v2

    .line 249
    :cond_0
    const v7, 0x7f070055

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 250
    .local v6, "targetIconSize":I
    const/high16 v7, 0x1050000

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 251
    .local v3, "stdIconSize":I
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 252
    .local v1, "dm":Landroid/util/DisplayMetrics;
    if-ne v6, v3, :cond_1

    .line 255
    iget v2, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .local v2, "iconDpi":I
    goto :goto_0

    .line 260
    .end local v2    # "iconDpi":I
    :cond_1
    int-to-float v7, v6

    int-to-float v8, v3

    div-float/2addr v7, v8

    iget v8, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    float-to-int v5, v7

    .line 261
    .local v5, "targetDPI":I
    const/16 v7, 0x78

    if-gt v5, v7, :cond_2

    .line 262
    const/16 v2, 0x78

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 263
    .end local v2    # "iconDpi":I
    :cond_2
    const/16 v7, 0xa0

    if-gt v5, v7, :cond_3

    .line 264
    const/16 v2, 0xa0

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 265
    .end local v2    # "iconDpi":I
    :cond_3
    if-gt v5, v2, :cond_4

    .line 266
    const/16 v2, 0xf0

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 267
    .end local v2    # "iconDpi":I
    :cond_4
    const/16 v7, 0x140

    if-gt v5, v7, :cond_5

    .line 268
    const/16 v2, 0x140

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 269
    .end local v2    # "iconDpi":I
    :cond_5
    const/16 v7, 0x1e0

    if-gt v5, v7, :cond_6

    .line 270
    const/16 v2, 0x1e0

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 272
    .end local v2    # "iconDpi":I
    :cond_6
    const/16 v2, 0x280

    .restart local v2    # "iconDpi":I
    goto :goto_0
.end method

.method public setMoreAppsListState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/EasyController;->doesMoreApps_exist:Z

    .line 201
    return-void
.end method

.method public setPageOnOff(IZ)V
    .locals 6
    .param p1, "pageType"    # I
    .param p2, "onOff"    # Z

    .prologue
    const/4 v5, 0x0

    .line 399
    iget v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_HOME:I

    if-eq p1, v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_APPS1:I

    if-ne p1, v2, :cond_2

    :cond_0
    if-nez p2, :cond_2

    .line 420
    :cond_1
    :goto_0
    return-void

    .line 403
    :cond_2
    const-string v2, "EasyLauncher.EasyController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPageOnOff pageType"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " old : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aget-boolean v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " new : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    sget-object v2, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aget-boolean v2, v2, p1

    if-eq v2, p2, :cond_1

    .line 406
    sget-object v2, Lcom/sec/android/app/easylauncher/EasyController;->mPageOnOff:[Z

    aput-boolean p2, v2, p1

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.easylauncher.prefs"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 409
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 411
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p2, :cond_3

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->prefs_page_onoff:[Ljava/lang/String;

    aget-object v2, v2, p1

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 417
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 415
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyController;->prefs_page_onoff:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1
.end method

.method startAsyncTask()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 345
    :cond_0
    sput-boolean v2, Lcom/sec/android/app/easylauncher/EasyController;->reRunTask:Z

    .line 351
    :goto_0
    return-void

    .line 347
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/EasyController;->setMoreAppsListState(Z)V

    .line 348
    new-instance v0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;-><init>(Lcom/sec/android/app/easylauncher/EasyController;Lcom/sec/android/app/easylauncher/EasyController$1;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
