.class Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;
.super Ljava/lang/Object;
.source "AppsGridAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/AppsGridAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AppsGridAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AppsGridAdapter;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 222
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 224
    .local v0, "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    # getter for: Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->access$000(Lcom/sec/android/app/easylauncher/AppsGridAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f09000b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 226
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_2

    .line 237
    :cond_1
    :goto_0
    return v4

    .line 229
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iput-object p1, v1, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mTouchView:Landroid/view/View;

    .line 231
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_3

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mTouchView:Landroid/view/View;

    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 235
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mTouchView:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
