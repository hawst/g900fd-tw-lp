.class public Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;
.super Landroid/appwidget/AppWidgetHost;
.source "LauncherAppWidgetHost.java"


# instance fields
.field mLauncher:Lcom/sec/android/app/easylauncher/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hostId"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    .line 29
    check-cast p1, Lcom/sec/android/app/easylauncher/Launcher;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;->mLauncher:Lcom/sec/android/app/easylauncher/Launcher;

    .line 30
    return-void
.end method


# virtual methods
.method protected onCreateView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "appWidget"    # Landroid/appwidget/AppWidgetProviderInfo;

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;

    invoke-direct {v0, p1}, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected onProvidersChanged()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;->mLauncher:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->widgetBindPackagesUpdated()V

    .line 42
    return-void
.end method
