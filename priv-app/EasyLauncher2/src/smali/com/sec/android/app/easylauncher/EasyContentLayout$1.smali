.class Lcom/sec/android/app/easylauncher/EasyContentLayout$1;
.super Ljava/lang/Object;
.source "EasyContentLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/EasyContentLayout;->createDragView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/EasyContentLayout;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    # getter for: Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->access$000(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    # getter for: Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->access$000(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;->addView()V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    # getter for: Lcom/sec/android/app/easylauncher/EasyContentLayout;->wm:Landroid/view/WindowManager;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->access$200(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    # getter for: Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v2}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->access$100(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;->this$0:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragImage:Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->access$302(Lcom/sec/android/app/easylauncher/EasyContentLayout;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 161
    :cond_1
    return-void
.end method
