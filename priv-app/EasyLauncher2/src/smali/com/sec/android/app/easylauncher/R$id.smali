.class public final Lcom/sec/android/app/easylauncher/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_search:I = 0x7f0c005e

.field public static final actionbar_edit_image:I = 0x7f0c0003

.field public static final actionbar_title_edit:I = 0x7f0c0000

.field public static final actionbar_widget_frame_layout:I = 0x7f0c0002

.field public static final apps_grid:I = 0x7f0c0023

.field public static final apps_grid_cover:I = 0x7f0c0028

.field public static final apps_list:I = 0x7f0c0016

.field public static final badge_count:I = 0x7f0c0022

.field public static final badge_count_background:I = 0x7f0c0021

.field public static final blank_space:I = 0x7f0c0027

.field public static final bottom_layout:I = 0x7f0c0029

.field public static final contact_widget_holder:I = 0x7f0c002b

.field public static final contactfragment_talkback:I = 0x7f0c002a

.field public static final defaultapps:I = 0x7f0c004b

.field public static final delete:I = 0x7f0c0035

.field public static final delete_drag_bar:I = 0x7f0c0034

.field public static final divider:I = 0x7f0c0014

.field public static final drawer:I = 0x7f0c0047

.field public static final drawer_content:I = 0x7f0c0033

.field public static final drawer_icon1:I = 0x7f0c0007

.field public static final drawer_icon2:I = 0x7f0c0008

.field public static final drawer_icon3:I = 0x7f0c0009

.field public static final drawer_icon4:I = 0x7f0c000a

.field public static final drawer_icon5:I = 0x7f0c000b

.field public static final drawer_icon6:I = 0x7f0c000c

.field public static final drawer_icon_indicator:I = 0x7f0c0006

.field public static final drawer_item_checkbox:I = 0x7f0c0012

.field public static final drawer_item_title:I = 0x7f0c0013

.field public static final drawer_layout:I = 0x7f0c0032

.field public static final edit_shortcut:I = 0x7f0c000f

.field public static final edit_shortcut_tv:I = 0x7f0c0010

.field public static final edit_sign:I = 0x7f0c0020

.field public static final editmode:I = 0x7f0c0045

.field public static final editmode_layout:I = 0x7f0c0044

.field public static final grid_cover:I = 0x7f0c0053

.field public static final grid_item_secret:I = 0x7f0c001f

.field public static final guide_bubble_summary:I = 0x7f0c0030

.field public static final guide_down_pointer:I = 0x7f0c0031

.field public static final guide_hand_pointer:I = 0x7f0c002d

.field public static final guide_root:I = 0x7f0c002c

.field public static final guide_up_pointer:I = 0x7f0c002f

.field public static final icon:I = 0x7f0c001d

.field public static final index_sel_view:I = 0x7f0c0017

.field public static final index_view_root:I = 0x7f0c0018

.field public static final index_view_sub_1:I = 0x7f0c0019

.field public static final indicator1:I = 0x7f0c0039

.field public static final indicator1_layout:I = 0x7f0c0038

.field public static final indicator2:I = 0x7f0c003b

.field public static final indicator2_layout:I = 0x7f0c003a

.field public static final indicator3:I = 0x7f0c003d

.field public static final indicator3_layout:I = 0x7f0c003c

.field public static final indicator4:I = 0x7f0c003f

.field public static final indicator4_layout:I = 0x7f0c003e

.field public static final indicator5:I = 0x7f0c0041

.field public static final indicator5_layout:I = 0x7f0c0040

.field public static final indicator6:I = 0x7f0c0043

.field public static final indicator6_layout:I = 0x7f0c0042

.field public static final left_drawer:I = 0x7f0c0046

.field public static final left_drawer_holder:I = 0x7f0c0005

.field public static final left_drawer_list:I = 0x7f0c000d

.field public static final left_drawer_space_holder:I = 0x7f0c0004

.field public static final light_widget_frame_layout:I = 0x7f0c0055

.field public static final list_item_icon:I = 0x7f0c0024

.field public static final list_item_secret:I = 0x7f0c0025

.field public static final list_item_text:I = 0x7f0c0026

.field public static final listcontainer:I = 0x7f0c0015

.field public static final menu_selected_back_down:I = 0x7f0c001a

.field public static final no_result_text:I = 0x7f0c001c

.field public static final options_home_edit:I = 0x7f0c0057

.field public static final options_home_help:I = 0x7f0c0058

.field public static final options_home_notification:I = 0x7f0c005a

.field public static final options_home_search:I = 0x7f0c0059

.field public static final options_home_settings:I = 0x7f0c005b

.field public static final options_menu_market:I = 0x7f0c005d

.field public static final options_menu_search:I = 0x7f0c005c

.field public static final page_hide_button:I = 0x7f0c004a

.field public static final page_show_button:I = 0x7f0c0049

.field public static final page_show_hide:I = 0x7f0c0048

.field public static final pageindicator:I = 0x7f0c0037

.field public static final pager:I = 0x7f0c0036

.field public static final search_input:I = 0x7f0c001b

.field public static final set_wallpaper:I = 0x7f0c0011

.field public static final settings:I = 0x7f0c000e

.field public static final text_box:I = 0x7f0c002e

.field public static final title:I = 0x7f0c001e

.field public static final widget_edit_sign1:I = 0x7f0c004e

.field public static final widget_edit_sign2:I = 0x7f0c0050

.field public static final widget_edit_sign3:I = 0x7f0c0052

.field public static final widget_frame_layout:I = 0x7f0c004d

.field public static final widget_frame_layout1:I = 0x7f0c004f

.field public static final widget_frame_layout2:I = 0x7f0c0051

.field public static final widget_layout:I = 0x7f0c0054

.field public static final widget_list:I = 0x7f0c0056

.field public static final widgetholder:I = 0x7f0c0001

.field public static final widgetholder1:I = 0x7f0c004c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
