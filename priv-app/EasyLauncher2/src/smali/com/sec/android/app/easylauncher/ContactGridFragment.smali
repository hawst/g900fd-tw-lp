.class public Lcom/sec/android/app/easylauncher/ContactGridFragment;
.super Landroid/support/v4/app/Fragment;
.source "ContactGridFragment.java"


# static fields
.field private static final ACTION_EASY_CONTACT:Ljava/lang/String; = "com.sec.android.app.easylauncher.EASY_CONTACT"

.field private static final ACTION_EASY_HELP_CONTACT:Ljava/lang/String; = "android.action.easymodecontactswidget.HELP_RESULT"

.field private static final DEBUGGABLE:Z

.field static mEditDisable:Z

.field private static mStateRecovered:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field private mApp:Lcom/sec/android/app/easylauncher/EasyController;

.field mContactUpdated:Z

.field private mContactWidgetHolder:Landroid/widget/LinearLayout;

.field private mContactWidgetId:I

.field mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

.field private mPagePosition:I

.field private mPref:Landroid/content/SharedPreferences;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field v:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 43
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->DEBUGGABLE:Z

    .line 51
    sput-boolean v1, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mStateRecovered:Z

    .line 58
    sput-boolean v2, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mEditDisable:Z

    return-void

    :cond_0
    move v0, v2

    .line 43
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 41
    const-string v0, "EasyLauncher.ContactFragment"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->TAG:Ljava/lang/String;

    .line 152
    new-instance v0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;-><init>(Lcom/sec/android/app/easylauncher/ContactGridFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 75
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->DEBUGGABLE:Z

    return v0
.end method

.method private unBind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetHolder:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 353
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetHolder:Landroid/widget/LinearLayout;

    .line 354
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    .line 355
    return-void
.end method


# virtual methods
.method addWidget()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, -0x2

    const/4 v9, 0x0

    .line 188
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    if-nez v7, :cond_0

    .line 237
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v7

    iget-object v2, v7, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .line 193
    .local v2, "localwidgetprovider":Lcom/sec/android/app/easylauncher/LocalWidgetProvider;
    const/4 v0, 0x0

    .line 195
    .local v0, "hostView":Landroid/appwidget/AppWidgetHostView;
    iget v7, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPagePosition:I

    sget v8, Lcom/sec/android/app/easylauncher/EasyController;->CONTACT_WIDGET_POSITION:I

    invoke-virtual {v2, v7, v8}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v4

    .line 197
    .local v4, "mContactsWidgetInfo":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    const v8, 0x7f0c002b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 198
    .local v1, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 199
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 202
    .local v3, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v4, :cond_1

    .line 203
    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getHostView()Landroid/appwidget/AppWidgetHostView;

    move-result-object v0

    .line 204
    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetId:I

    .line 207
    :cond_1
    if-nez v0, :cond_3

    .line 210
    if-eqz v4, :cond_2

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v7

    const v8, 0x7f03000c

    invoke-static {v7, v8, v11}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 217
    .local v5, "mErrorView":Landroid/view/View;
    :goto_1
    invoke-virtual {v1, v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 218
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 214
    .end local v5    # "mErrorView":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v7

    const v8, 0x7f03000b

    invoke-static {v7, v8, v11}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .restart local v5    # "mErrorView":Landroid/view/View;
    goto :goto_1

    .line 220
    .end local v5    # "mErrorView":Landroid/view/View;
    :cond_3
    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getDefaultPadding()Landroid/graphics/Rect;

    move-result-object v6

    .line 221
    .local v6, "r":Landroid/graphics/Rect;
    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 225
    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 226
    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 227
    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 228
    invoke-virtual {v0, v3}, Landroid/appwidget/AppWidgetHostView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0700b8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget v8, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0700b9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iget v10, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v7, v8, v9, v10}, Landroid/appwidget/AppWidgetHostView;->setPadding(IIII)V

    .line 234
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method addWidget(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    if-nez v0, :cond_0

    .line 180
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mActivity:Landroid/app/Activity;

    .line 183
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->addWidget()V

    .line 184
    return-void
.end method

.method public dismissHelpDialog()V
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 329
    :cond_0
    return-void
.end method

.method public getEditMode()Z
    .locals 1

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v0

    return v0
.end method

.method getLauncher()Lcom/sec/android/app/easylauncher/Launcher;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    .line 146
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 136
    const-string v0, "EasyLauncher.ContactFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----------onAttach : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mActivity:Landroid/app/Activity;

    .line 138
    return-void
.end method

.method public onBackPressed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    if-nez v0, :cond_0

    .line 285
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mActivity:Landroid/app/Activity;

    .line 288
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(Z)V

    .line 292
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    if-eqz p1, :cond_0

    .line 86
    const-string v1, "mPagePosition"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPagePosition:I

    .line 87
    const-string v1, "EasyLauncher.ContactFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----------mPagePosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPagePosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 92
    .local v0, "screenfilter":Landroid/content/IntentFilter;
    const-string v1, "android.action.easymodecontactswidget.HELP_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    .end local v0    # "screenfilter":Landroid/content/IntentFilter;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    const-string v2, "com.sec.android.app.easylauncher.prefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/easylauncher/EasyController;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPref:Landroid/content/SharedPreferences;

    .line 98
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 102
    const v2, 0x7f03000d

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    const v3, 0x7f0c002b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetHolder:Landroid/widget/LinearLayout;

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->addWidget()V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPref:Landroid/content/SharedPreferences;

    const-string v3, "PrefContactEdit"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 110
    .local v0, "prefEdit":Z
    if-nez p3, :cond_0

    if-eqz v0, :cond_0

    .line 111
    sput-boolean v5, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mStateRecovered:Z

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 113
    .local v1, "prefsEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "PrefContactEdit"

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getEditMode()Z

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 114
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 115
    sget-boolean v2, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mStateRecovered:Z

    if-nez v2, :cond_0

    .line 116
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(Z)V

    .line 120
    .end local v1    # "prefsEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    new-instance v3, Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    check-cast v2, Landroid/view/ViewGroup;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/app/easylauncher/HelpGuider;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 124
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(Z)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addcontact_step1()V

    .line 129
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->v:Landroid/view/View;

    return-object v2
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 359
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->unBind()V

    .line 360
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 361
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 343
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 344
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 270
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 271
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 244
    if-eqz p1, :cond_1

    .line 245
    sget-boolean v1, Lcom/sec/android/app/easylauncher/ContactGridFragment;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 246
    const-string v1, "EasyLauncher.ContactFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSaveInstanceState :ContactEditMode :-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getEditMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_0
    const-string v1, "mPagePosition"

    iget v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPagePosition:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 251
    .local v0, "prefsEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "PrefContactEdit"

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getEditMode()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 252
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 254
    .end local v0    # "prefsEditor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 276
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 277
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 339
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 259
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 260
    if-eqz p1, :cond_0

    .line 261
    const-string v0, "mPagePosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPagePosition:I

    .line 262
    sget-boolean v0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 263
    const-string v0, "EasyLauncher.ContactFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewStateRestored:mEditDisable :-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mEditDisable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public setScreenPosion(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mPagePosition:I

    .line 79
    return-void
.end method

.method startContactEditMode(Z)V
    .locals 4
    .param p1, "editMode"    # Z

    .prologue
    .line 305
    const-string v1, "EasyLauncher.ContactFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startContactEditMode mContactWidgetId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " editMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 308
    .local v0, "mEditContacts":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.easylauncher.EASY_CONTACT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    const-string v1, "contactwidgetid"

    iget v2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mContactWidgetId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 311
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 312
    const-string v1, "mode"

    const-string v2, "help_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    const-string v1, "editState"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 320
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/easylauncher/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 321
    return-void

    .line 316
    :cond_0
    const-string v1, "mode"

    const-string v2, "edit_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    const-string v1, "editState"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method startContactEditMode(ZLandroid/app/Activity;)V
    .locals 1
    .param p1, "editMode"    # Z
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    if-nez v0, :cond_0

    .line 296
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mActivity:Landroid/app/Activity;

    .line 299
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(Z)V

    .line 300
    return-void
.end method
