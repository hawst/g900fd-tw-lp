.class public final Lcom/sec/android/app/easylauncher/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final actionbar_height:I = 0x7f070000

.field public static final app_fragement_moreapps_text_size:I = 0x7f070001

.field public static final app_page_left_right_padding:I = 0x7f070002

.field public static final app_widget_padding_bottom:I = 0x7f070003

.field public static final app_widget_padding_left:I = 0x7f070004

.field public static final app_widget_padding_right:I = 0x7f070005

.field public static final app_widget_padding_top:I = 0x7f070006

.field public static final apps_bottom_layout_margin_bottom:I = 0x7f070007

.field public static final apps_bottom_layout_margin_left_right:I = 0x7f070008

.field public static final apps_bottom_layout_margin_top:I = 0x7f070009

.field public static final apps_grid_cover_margin_left_right:I = 0x7f07000a

.field public static final apps_grid_editsign_top_margin:I = 0x7f07000b

.field public static final apps_grid_horizontal_spacing:I = 0x7f07000c

.field public static final apps_grid_image_top_margin:I = 0x7f07000d

.field public static final apps_grid_margin_bottom:I = 0x7f07000e

.field public static final apps_grid_margin_top:I = 0x7f07000f

.field public static final apps_grid_title_margin_top:I = 0x7f070010

.field public static final apps_grid_vertical_spacing:I = 0x7f070011

.field public static final apps_item_secret_icon_margin_bottom:I = 0x7f070012

.field public static final apps_item_secret_icon_size:I = 0x7f070013

.field public static final apps_more_apps_height:I = 0x7f070014

.field public static final apps_more_apps_image_margin_right:I = 0x7f070015

.field public static final apps_more_apps_text_size:I = 0x7f070016

.field public static final apps_more_apps_width:I = 0x7f070017

.field public static final apps_top_black_space_height:I = 0x7f070018

.field public static final apps_top_black_space_margin_bottom:I = 0x7f070019

.field public static final badge_count_padding_bottom:I = 0x7f07001a

.field public static final badge_icon_padding_right:I = 0x7f07001b

.field public static final box_padding:I = 0x7f07001c

.field public static final centre_page_left_right_padding:I = 0x7f07001d

.field public static final clock_ampm_fontsize:I = 0x7f0700d2

.field public static final clock_ampm_height:I = 0x7f0700d3

.field public static final clock_ampm_margin_left:I = 0x7f0700d4

.field public static final clock_ampm_margin_top:I = 0x7f0700d5

.field public static final clock_ampm_singledigit_margin_left:I = 0x7f0700d6

.field public static final clock_ampm_width:I = 0x7f0700d7

.field public static final clock_time_colon_height:I = 0x7f0700d8

.field public static final clock_time_colon_width:I = 0x7f0700d9

.field public static final clock_time_margin_bottom:I = 0x7f0700da

.field public static final clock_time_margin_left:I = 0x7f0700db

.field public static final clock_time_margin_right:I = 0x7f0700dc

.field public static final clock_time_margin_top:I = 0x7f0700dd

.field public static final clock_time_number_height:I = 0x7f0700de

.field public static final clock_time_number_width:I = 0x7f0700df

.field public static final clock_time_singledigit_margin_left:I = 0x7f0700e0

.field public static final divider_height:I = 0x7f07001e

.field public static final drawer_icon_height:I = 0x7f07001f

.field public static final drawer_icon_horizontal_gap:I = 0x7f070020

.field public static final drawer_icon_margin_bottom:I = 0x7f070021

.field public static final drawer_icon_margin_left:I = 0x7f070022

.field public static final drawer_icon_margin_top:I = 0x7f070023

.field public static final drawer_icon_width:I = 0x7f070024

.field public static final drawer_layout_margin_top:I = 0x7f070025

.field public static final drawer_list_divider_height:I = 0x7f070026

.field public static final drawer_list_height:I = 0x7f070027

.field public static final drawer_list_icon_size:I = 0x7f070028

.field public static final drawer_list_padding:I = 0x7f070029

.field public static final drawer_list_text_size:I = 0x7f07002a

.field public static final drawer_panel_width:I = 0x7f07002b

.field public static final drawer_right_arrow_padding:I = 0x7f07002c

.field public static final drift_slop:I = 0x7f07002d

.field public static final easy_allapps_image_height:I = 0x7f07002e

.field public static final easy_allapps_image_margin_horizontal:I = 0x7f07002f

.field public static final easy_allapps_image_width:I = 0x7f070030

.field public static final easy_allapps_list_item_height:I = 0x7f070031

.field public static final easy_allapps_search_back_height:I = 0x7f070032

.field public static final easy_allapps_search_back_margin_left:I = 0x7f070033

.field public static final easy_allapps_search_back_width:I = 0x7f070034

.field public static final easy_appsfragment_moreapps_linespacing:I = 0x7f070035

.field public static final easy_appsfragment_moreapps_text_height:I = 0x7f070036

.field public static final easy_appwidget_error_height:I = 0x7f070037

.field public static final easy_appwidget_error_width:I = 0x7f070038

.field public static final easy_contactfragment_marginTop:I = 0x7f070039

.field public static final easy_delete_drag_bar_height:I = 0x7f07003a

.field public static final easy_done_button_height:I = 0x7f07003b

.field public static final easy_done_button_marginbottom:I = 0x7f07003c

.field public static final easy_done_button_marginleft:I = 0x7f07003d

.field public static final easy_done_button_marginright:I = 0x7f07003e

.field public static final easy_done_button_textsize:I = 0x7f07003f

.field public static final easy_edit_layout_margin_left:I = 0x7f070040

.field public static final easy_edit_page_margin:I = 0x7f070041

.field public static final easy_page_show_hide_button_button_height:I = 0x7f070042

.field public static final easy_page_show_hide_button_button_width:I = 0x7f070043

.field public static final easy_page_show_hide_button_gap:I = 0x7f070044

.field public static final easy_page_show_hide_button_marginleft:I = 0x7f070045

.field public static final easy_page_show_hide_button_marginright:I = 0x7f070046

.field public static final easy_page_show_hide_layout_margin_top:I = 0x7f070047

.field public static final easy_popup_list_item_height:I = 0x7f070048

.field public static final easy_popup_list_item_icon_height:I = 0x7f070049

.field public static final easy_popup_list_item_icon_marginleft:I = 0x7f07004a

.field public static final easy_popup_list_item_icon_padding:I = 0x7f07004b

.field public static final easy_popup_list_item_icon_width:I = 0x7f07004c

.field public static final easy_popup_list_item_title_paddingleft:I = 0x7f07004d

.field public static final easy_popup_list_item_title_paddingright:I = 0x7f07004e

.field public static final easy_popup_list_item_title_textsize:I = 0x7f07004f

.field public static final easy_widget_list_item_height:I = 0x7f070050

.field public static final easy_widget_list_item_icon_height:I = 0x7f070051

.field public static final easy_widget_list_item_icon_width:I = 0x7f070052

.field public static final easy_widget_list_item_text_height:I = 0x7f070053

.field public static final edit_icon_padding_right:I = 0x7f070054

.field public static final grid_app_icon_size:I = 0x7f070055

.field public static final grid_cover_horizontal_spacing:I = 0x7f070056

.field public static final grid_cover_margin_bottom:I = 0x7f070057

.field public static final grid_cover_margin_top:I = 0x7f070058

.field public static final grid_cover_vertical_spacing:I = 0x7f070059

.field public static final grid_cover_width:I = 0x7f07005a

.field public static final help_hub_add_apps2_bubble_summary_margin_bottom:I = 0x7f07005b

.field public static final help_hub_add_apps_bubble_summary_margin_bottom:I = 0x7f07005c

.field public static final help_hub_add_apps_bubble_summary_margin_right:I = 0x7f07005d

.field public static final help_hub_add_apps_bubble_summary_margin_top:I = 0x7f07005e

.field public static final help_hub_add_apps_image_margin_right:I = 0x7f07005f

.field public static final help_hub_add_apps_image_margin_top:I = 0x7f070060

.field public static final help_hub_add_apps_pointer_up_margin_right:I = 0x7f070061

.field public static final help_hub_add_apps_pointer_up_margin_top:I = 0x7f070062

.field public static final help_hub_add_contact_bubble_summary_margin_bottom:I = 0x7f070063

.field public static final help_hub_add_contact_bubble_summary_margin_left:I = 0x7f070064

.field public static final help_hub_add_contact_bubble_summary_margin_top:I = 0x7f070065

.field public static final help_hub_add_contact_image_margin_left:I = 0x7f070066

.field public static final help_hub_add_contact_image_margin_top:I = 0x7f070067

.field public static final help_hub_add_contact_pointer_up_margin_left:I = 0x7f070068

.field public static final help_hub_add_contact_pointer_up_margin_top:I = 0x7f070069

.field public static final help_hub_guide_text_padding_bottom:I = 0x7f07006a

.field public static final help_hub_guide_text_padding_left:I = 0x7f07006b

.field public static final help_hub_guide_text_padding_right:I = 0x7f07006c

.field public static final help_hub_guide_text_padding_top:I = 0x7f07006d

.field public static final help_hub_guide_text_size:I = 0x7f07006e

.field public static final help_hub_guide_text_width:I = 0x7f07006f

.field public static final help_hub_guide_text_width2:I = 0x7f070070

.field public static final help_hub_guide_textbox_padding_bottom:I = 0x7f070071

.field public static final help_hub_guide_textbox_padding_left:I = 0x7f070072

.field public static final help_hub_guide_textbox_padding_right:I = 0x7f070073

.field public static final help_hub_guide_textbox_padding_top:I = 0x7f070074

.field public static final help_hub_remove_apps1_bubble_summary_margin_left:I = 0x7f070075

.field public static final help_hub_remove_apps1_bubble_summary_margin_top:I = 0x7f070076

.field public static final help_hub_remove_apps1_down_pointer_margin_left:I = 0x7f070077

.field public static final help_hub_remove_apps1_down_pointer_margin_top:I = 0x7f070078

.field public static final help_hub_remove_apps1_image_margin_left:I = 0x7f070079

.field public static final help_hub_remove_apps1_image_margin_top:I = 0x7f07007a

.field public static final help_hub_remove_apps1_quickedit_bubble_summary_margin_right:I = 0x7f07007b

.field public static final help_hub_remove_apps1_quickedit_bubble_summary_margin_top:I = 0x7f07007c

.field public static final help_hub_remove_apps1_quickedit_image_margin_right:I = 0x7f07007d

.field public static final help_hub_remove_apps1_quickedit_image_margin_top:I = 0x7f07007e

.field public static final help_hub_remove_apps1_quickedit_pointer_up_margin_right:I = 0x7f07007f

.field public static final help_hub_remove_apps1_quickedit_pointer_up_margin_top:I = 0x7f070080

.field public static final help_hub_remove_apps3_bubble_summary_margin_left:I = 0x7f070081

.field public static final help_hub_remove_apps3_bubble_summary_margin_top:I = 0x7f070082

.field public static final help_hub_remove_apps3_down_pointer_margin_left:I = 0x7f070083

.field public static final help_hub_remove_apps3_down_pointer_margin_top:I = 0x7f070084

.field public static final help_hub_remove_apps3_image_margin_left:I = 0x7f070085

.field public static final help_hub_remove_apps3_image_margin_top:I = 0x7f070086

.field public static final index_level_0_cell_height:I = 0x7f070087

.field public static final index_level_0_font_size:I = 0x7f070088

.field public static final index_level_0_padding_bottom:I = 0x7f070089

.field public static final index_level_0_padding_left:I = 0x7f07008a

.field public static final index_level_0_padding_right:I = 0x7f07008b

.field public static final index_level_0_padding_top:I = 0x7f07008c

.field public static final index_level_0_width:I = 0x7f07008d

.field public static final index_level_1_cell_height:I = 0x7f07008e

.field public static final index_level_1_font_size:I = 0x7f07008f

.field public static final index_level_1_padding_bottom:I = 0x7f070090

.field public static final index_level_1_padding_left:I = 0x7f070091

.field public static final index_level_1_padding_right:I = 0x7f070092

.field public static final index_level_1_padding_top:I = 0x7f070093

.field public static final index_level_1_width:I = 0x7f070094

.field public static final item_space:I = 0x7f070095

.field public static final item_space_left:I = 0x7f070096

.field public static final item_space_medium:I = 0x7f070097

.field public static final item_space_right:I = 0x7f070098

.field public static final item_space_small:I = 0x7f070099

.field public static final item_space_top:I = 0x7f07009a

.field public static final list_app_icon_size:I = 0x7f07009b

.field public static final list_item_divider_height:I = 0x7f07009c

.field public static final list_item_icon_height:I = 0x7f07009d

.field public static final list_item_icon_margin_left:I = 0x7f07009e

.field public static final list_item_icon_margin_top:I = 0x7f07009f

.field public static final list_item_icon_width:I = 0x7f0700a0

.field public static final list_item_secret_icon_margin_right:I = 0x7f0700a1

.field public static final list_item_secret_icon_size:I = 0x7f0700a2

.field public static final list_item_text_drawable_padding:I = 0x7f0700a3

.field public static final list_item_text_height:I = 0x7f0700a4

.field public static final list_item_text_padding_left:I = 0x7f0700a5

.field public static final list_item_text_padding_right:I = 0x7f0700a6

.field public static final list_item_text_size:I = 0x7f0700a7

.field public static final page_indicator_height:I = 0x7f0700a8

.field public static final page_indicator_horizontal_gap:I = 0x7f0700a9

.field public static final page_indicator_layout_height:I = 0x7f0700aa

.field public static final page_indicator_layout_width:I = 0x7f0700ab

.field public static final page_indicator_margin_bottom:I = 0x7f0700ac

.field public static final page_indicator_margin_bottom_editmode:I = 0x7f0700ad

.field public static final page_indicator_margin_bottom_quick_editmode:I = 0x7f0700ae

.field public static final page_indicator_width:I = 0x7f0700af

.field public static final popup_font_size:I = 0x7f0700b0

.field public static final popup_height:I = 0x7f0700b1

.field public static final popup_height_inc:I = 0x7f0700b2

.field public static final popup_width:I = 0x7f0700b3

.field public static final popup_width_inc:I = 0x7f0700b4

.field public static final search_input_titlebar_left_padding:I = 0x7f0700b5

.field public static final search_input_titlebar_text_size:I = 0x7f0700b6

.field public static final search_input_titlebar_top_padding:I = 0x7f0700b7

.field public static final senior_margin_left_large:I = 0x7f0700b8

.field public static final senior_margin_right_large:I = 0x7f0700b9

.field public static final shealth_widget_height:I = 0x7f0700ba

.field public static final shealth_widget_width:I = 0x7f0700bb

.field public static final statusbar_height:I = 0x7f0700bc

.field public static final talkback_text_item_height:I = 0x7f0700bd

.field public static final talkback_text_item_width:I = 0x7f0700be

.field public static final touch_slop:I = 0x7f0700bf

.field public static final widget_appfragment_frame_width:I = 0x7f0700c0

.field public static final widget_appfragment_holder_height:I = 0x7f0700c1

.field public static final widget_frame1_width:I = 0x7f0700c2

.field public static final widget_frame2_margin_left:I = 0x7f0700c3

.field public static final widget_frame2_margin_top:I = 0x7f0700c4

.field public static final widget_frame2_width:I = 0x7f0700c5

.field public static final widget_frame3_height:I = 0x7f0700c6

.field public static final widget_frame3_margin_top:I = 0x7f0700e1

.field public static final widget_frame3_width:I = 0x7f0700c7

.field public static final widget_frame4_height:I = 0x7f0700c8

.field public static final widget_frame4_margin_top:I = 0x7f0700c9

.field public static final widget_holder2_left_padding:I = 0x7f0700ca

.field public static final widget_holder_height:I = 0x7f0700cb

.field public static final widget_holder_left_padding:I = 0x7f0700cc

.field public static final widget_icon_height:I = 0x7f0700cd

.field public static final widget_icon_marginleft:I = 0x7f0700ce

.field public static final widget_icon_width:I = 0x7f0700cf

.field public static final widget_launcher_widget_holder_height:I = 0x7f0700d0

.field public static final widget_weather_margin_bottom:I = 0x7f0700d1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
