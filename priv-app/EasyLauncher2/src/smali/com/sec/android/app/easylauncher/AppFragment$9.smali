.class Lcom/sec/android/app/easylauncher/AppFragment$9;
.super Ljava/lang/Object;
.source "AppFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/AppFragment;->removeShortCutDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AppFragment;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/easylauncher/AppFragment;->access$202(Lcom/sec/android/app/easylauncher/AppFragment;Z)Z

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->setHelpResult(Z)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget v1, v1, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->removeData(I)V

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget v1, v1, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->removeFromFavorites(I)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # getter for: Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/AppFragment;->access$500(Lcom/sec/android/app/easylauncher/AppFragment;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$9;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # getter for: Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/AppFragment;->access$500(Lcom/sec/android/app/easylauncher/AppFragment;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->loadApps()V

    goto :goto_0
.end method
