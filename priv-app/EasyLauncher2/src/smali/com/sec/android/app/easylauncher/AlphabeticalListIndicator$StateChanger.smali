.class Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;
.super Ljava/lang/Object;
.source "AlphabeticalListIndicator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StateChanger"
.end annotation


# instance fields
.field private mRunning:Z

.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;)V
    .locals 1

    .prologue
    .line 585
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->mRunning:Z

    return-void
.end method


# virtual methods
.method public abort()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 603
    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->mRunning:Z

    if-nez v1, :cond_0

    .line 608
    :goto_0
    return v0

    .line 606
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 607
    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->mRunning:Z

    .line 608
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 595
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->mRunning:Z

    return v0
.end method

.method public postToQueue(I)V
    .locals 4
    .param p1, "aDelay"    # I

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    int-to-long v2, p1

    invoke-virtual {v0, p0, v2, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->mRunning:Z

    .line 592
    :cond_0
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;->mRunning:Z

    .line 600
    return-void
.end method
