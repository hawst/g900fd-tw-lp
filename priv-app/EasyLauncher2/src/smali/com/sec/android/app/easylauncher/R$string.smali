.class public final Lcom/sec/android/app/easylauncher/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final chooser_wallpaper:I = 0x7f090000

.field public static final content_description_hand:I = 0x7f090001

.field public static final drawer_close:I = 0x7f090002

.field public static final drawer_open:I = 0x7f090003

.field public static final easy_launcher_alphabetical_list_indicator_indexes:I = 0x7f090004

.field public static final easy_launcher_app_name:I = 0x7f090005

.field public static final easy_launcher_appfragment_delete_dialog_body:I = 0x7f090006

.field public static final easy_launcher_appfragment_delete_dialog_title:I = 0x7f090007

.field public static final easy_launcher_appfragment_grid_add:I = 0x7f090008

.field public static final easy_launcher_appfragment_grid_addpage:I = 0x7f090009

.field public static final easy_launcher_appfragment_indicator:I = 0x7f09000a

.field public static final easy_launcher_appfragment_more_apps:I = 0x7f09000b

.field public static final easy_launcher_appfragment_remove_dialog_body:I = 0x7f09000c

.field public static final easy_launcher_appfragment_remove_dialog_title:I = 0x7f09000d

.field public static final easy_launcher_appfragment_show_applications_list:I = 0x7f09000e

.field public static final easy_launcher_centrefragment_layout_dummytext:I = 0x7f09000f

.field public static final easy_launcher_contact_grid_add_contact:I = 0x7f090010

.field public static final easy_launcher_contact_item_contact_type:I = 0x7f090011

.field public static final easy_launcher_contactfragment_indicator:I = 0x7f090012

.field public static final easy_launcher_contactfragment_remove_dialog_body:I = 0x7f090013

.field public static final easy_launcher_contactfragment_remove_dialog_title:I = 0x7f090014

.field public static final easy_launcher_contactgrid_layout_call_log:I = 0x7f090015

.field public static final easy_launcher_contactgrid_layout_call_log_verizon:I = 0x7f090016

.field public static final easy_launcher_contactgrid_layout_contacts:I = 0x7f090017

.field public static final easy_launcher_contacts_edit_done:I = 0x7f090018

.field public static final easy_launcher_delete_text:I = 0x7f090019

.field public static final easy_launcher_delete_title:I = 0x7f09001a

.field public static final easy_launcher_dialog_cancel_button:I = 0x7f09001b

.field public static final easy_launcher_dialog_ok_button:I = 0x7f09001c

.field public static final easy_launcher_drawer_app1:I = 0x7f09001d

.field public static final easy_launcher_drawer_app2:I = 0x7f09001e

.field public static final easy_launcher_drawer_app3:I = 0x7f09001f

.field public static final easy_launcher_drawer_favoritecontact1:I = 0x7f090020

.field public static final easy_launcher_drawer_favoritecontact2:I = 0x7f090021

.field public static final easy_launcher_drawer_home:I = 0x7f090022

.field public static final easy_launcher_edit_shortcut:I = 0x7f090023

.field public static final easy_launcher_gadget_error_text:I = 0x7f090024

.field public static final easy_launcher_grid_item_space:I = 0x7f090025

.field public static final easy_launcher_homescreen_settings:I = 0x7f090026

.field public static final easy_launcher_main_app_grid_item_app_name:I = 0x7f090027

.field public static final easy_launcher_mainfragment_indicator:I = 0x7f090028

.field public static final easy_launcher_options_home_edit:I = 0x7f090029

.field public static final easy_launcher_options_menu_edit:I = 0x7f09002a

.field public static final easy_launcher_options_menu_help:I = 0x7f09002b

.field public static final easy_launcher_options_menu_market:I = 0x7f09002c

.field public static final easy_launcher_options_menu_notifications:I = 0x7f09002d

.field public static final easy_launcher_options_menu_search:I = 0x7f09002e

.field public static final easy_launcher_options_menu_settings:I = 0x7f09002f

.field public static final easy_launcher_page_hide:I = 0x7f090030

.field public static final easy_launcher_page_show:I = 0x7f090031

.field public static final easy_launcher_photo_gallery_done:I = 0x7f090032

.field public static final easy_launcher_photo_loading:I = 0x7f090033

.field public static final easy_launcher_remove_widget_dialog_body:I = 0x7f090034

.field public static final easy_launcher_search_apps:I = 0x7f090035

.field public static final easy_launcher_setwallpaper:I = 0x7f090036

.field public static final easy_launcher_time_am:I = 0x7f090037

.field public static final easy_launcher_time_pm:I = 0x7f090038

.field public static final easy_launcher_tts_add_app:I = 0x7f090039

.field public static final easy_launcher_tts_button:I = 0x7f09003a

.field public static final easy_launcher_tts_double_tap:I = 0x7f09003b

.field public static final easy_launcher_tts_hide_page:I = 0x7f09003c

.field public static final easy_launcher_tts_home:I = 0x7f09003d

.field public static final easy_launcher_tts_item_added:I = 0x7f09003e

.field public static final easy_launcher_tts_list:I = 0x7f09003f

.field public static final easy_launcher_tts_of:I = 0x7f090040

.field public static final easy_launcher_tts_shortcut:I = 0x7f090041

.field public static final easy_launcher_tts_show_page:I = 0x7f090042

.field public static final easy_launcher_tts_tap_to_delete:I = 0x7f090043

.field public static final easy_launcher_tts_tap_to_remove:I = 0x7f090044

.field public static final guide_add_shortcut_complete:I = 0x7f090045

.field public static final guide_remove_item_complete:I = 0x7f090046

.field public static final guide_tap_add_button:I = 0x7f090047

.field public static final guide_tap_appitem:I = 0x7f090048

.field public static final guide_tap_contact_add_button:I = 0x7f090049

.field public static final guide_tap_done_button:I = 0x7f09004a

.field public static final guide_tap_edit_shortcut:I = 0x7f09004b

.field public static final guide_tap_remove_button:I = 0x7f09004c

.field public static final navigate_up_tts:I = 0x7f09004d

.field public static final new_item:I = 0x7f09004e

.field public static final new_items:I = 0x7f09004f

.field public static final no_search_results:I = 0x7f090050

.field public static final stms_version:I = 0x7f090051


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
