.class public Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;
.super Ljava/lang/Object;
.source "EasyViewInfo.java"


# instance fields
.field private mClassName:Ljava/lang/String;

.field private mIntent:Landroid/content/Intent;

.field private mPackageName:Ljava/lang/String;

.field private mViewId:I

.field private position:I

.field private screen:I

.field private viewType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->position:I

    return v0
.end method

.method public getScreen()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->screen:I

    return v0
.end method

.method public getViewId()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mViewId:I

    return v0
.end method

.method public getViewType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->viewType:Ljava/lang/String;

    return-object v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mClassName:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mIntent:Landroid/content/Intent;

    .line 88
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mPackageName:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->position:I

    .line 48
    return-void
.end method

.method public setScreen(I)V
    .locals 0
    .param p1, "screen"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->screen:I

    .line 56
    return-void
.end method

.method public setViewId(I)V
    .locals 0
    .param p1, "widgetid"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mViewId:I

    .line 64
    return-void
.end method

.method public setViewType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->viewType:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public unbind()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 99
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mPackageName:Ljava/lang/String;

    .line 100
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mClassName:Ljava/lang/String;

    .line 101
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->mIntent:Landroid/content/Intent;

    .line 102
    return-void
.end method
