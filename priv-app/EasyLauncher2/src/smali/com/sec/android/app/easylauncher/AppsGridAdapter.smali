.class public Lcom/sec/android/app/easylauncher/AppsGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppsGridAdapter.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field mAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field mDragIndex:I

.field mFragment:Landroid/support/v4/app/Fragment;

.field mTouchView:Landroid/view/View;

.field mWidgetIndex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 46
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    const-string v0, "EasyLauncher.AppsGridAdapter"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mTouchView:Landroid/view/View;

    .line 273
    iput v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mDragIndex:I

    .line 288
    iput v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mWidgetIndex:I

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "f"    # Landroid/support/v4/app/Fragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;",
            "Landroid/support/v4/app/Fragment;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const/4 v1, -0x1

    .line 49
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    const-string v0, "EasyLauncher.AppsGridAdapter"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mTouchView:Landroid/view/View;

    .line 273
    iput v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mDragIndex:I

    .line 288
    iput v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mWidgetIndex:I

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    .line 53
    iput-object p3, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mFragment:Landroid/support/v4/app/Fragment;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/AppsGridAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method getEditable(Landroid/view/View;I)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 279
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 281
    .local v0, "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getEditable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    :cond_0
    const/4 v1, 0x0

    .line 285
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 81
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getTouchView()Landroid/view/View;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mTouchView:Landroid/view/View;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 86
    const/4 v9, 0x0

    .line 87
    .local v9, "imageView":Landroid/widget/ImageView;
    const/4 v7, 0x0

    .line 88
    .local v7, "editImage":Landroid/widget/ImageView;
    const/4 v13, 0x0

    .line 89
    .local v13, "title":Landroid/widget/TextView;
    const/4 v2, 0x0

    .line 91
    .local v2, "badgeCount":Landroid/widget/TextView;
    const/4 v11, 0x0

    .line 93
    .local v11, "mBadgeCount":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v14}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/Launcher;

    .line 95
    .local v1, "activity":Lcom/sec/android/app/easylauncher/Launcher;
    if-nez p2, :cond_0

    .line 96
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const-string v15, "layout_inflater"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/LayoutInflater;

    const v15, 0x7f030006

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 99
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    .line 104
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 105
    .local v10, "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const v14, 0x7f0c001d

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Landroid/widget/ImageView;

    move-object v9, v0

    .line 106
    const v14, 0x7f0c001e

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Landroid/widget/TextView;

    move-object v13, v0

    .line 107
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v14, 0x7f0c0020

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Landroid/widget/ImageView;

    move-object v7, v0

    .line 112
    const v14, 0x7f0c0022

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Landroid/widget/TextView;

    move-object v2, v0

    .line 115
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-virtual {v14}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getEditable()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 118
    const/4 v14, 0x0

    invoke-virtual {v7, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    const/16 v14, 0x8

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 151
    :cond_1
    :goto_0
    const/4 v5, 0x0

    .line 153
    .local v5, "description":Ljava/lang/String;
    if-lez v11, :cond_6

    .line 154
    const/4 v14, 0x1

    if-le v11, v14, :cond_5

    .line 155
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09004f

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 159
    :goto_1
    invoke-virtual {v13, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 200
    :goto_2
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v14

    if-eqz v14, :cond_e

    .line 201
    new-instance v14, Lcom/sec/android/app/easylauncher/AppsGridAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter$1;-><init>(Lcom/sec/android/app/easylauncher/AppsGridAdapter;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 243
    :goto_3
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setVisibility(I)V

    .line 244
    const-string v14, "empty"

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mDragIndex:I

    move/from16 v0, p1

    if-eq v14, v0, :cond_2

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mWidgetIndex:I

    move/from16 v0, p1

    if-ne v14, v0, :cond_3

    .line 245
    :cond_2
    const/4 v14, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setVisibility(I)V

    .line 248
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 257
    .end local v5    # "description":Ljava/lang/String;
    .end local v10    # "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :goto_4
    return-object p2

    .line 122
    .restart local v10    # "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_4
    const/4 v14, 0x4

    invoke-virtual {v7, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f090008

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 124
    .local v8, "emptyAppName":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 125
    const/16 v14, 0x8

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    const-string v14, " "

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const/16 v14, 0x11

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 130
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v14

    if-eqz v14, :cond_1

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 133
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->getBadgeCache()Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    move-result-object v14

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->getBadgeCount(Landroid/content/ComponentName;)I

    move-result v4

    .line 137
    .local v4, "count":I
    move v11, v4

    .line 138
    if-lez v4, :cond_1

    .line 139
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 142
    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 143
    .local v12, "stringCount":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    const/16 v14, 0x11

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 145
    const-string v14, ","

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 249
    .end local v4    # "count":I
    .end local v8    # "emptyAppName":Ljava/lang/String;
    .end local v10    # "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v12    # "stringCount":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 252
    .local v6, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v14, "EasyLauncher.AppsGridAdapter"

    invoke-virtual {v6}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-virtual {v6}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_4

    .line 157
    .end local v6    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v5    # "description":Ljava/lang/String;
    .restart local v10    # "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_5
    :try_start_1
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09004e

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 162
    :cond_6
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_9

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_9

    .line 163
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 164
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getEditable()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 165
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f090043

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 175
    :cond_7
    :goto_5
    invoke-virtual {v13, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 170
    :cond_8
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 176
    :cond_9
    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_a

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_a

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v14

    const-string v15, "Add"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 177
    :cond_a
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v14

    if-eqz v14, :cond_b

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isTalkBackOn()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 178
    const-string v14, ""

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 182
    :cond_b
    const-string v14, " "

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f090039

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f09003a

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 184
    .local v3, "contentDescription":Ljava/lang/String;
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 185
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 189
    .end local v3    # "contentDescription":Ljava/lang/String;
    :cond_c
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v14

    if-eqz v14, :cond_d

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isTalkBackOn()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 190
    const-string v14, ""

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 193
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f09000b

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 217
    :cond_e
    new-instance v14, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter$2;-><init>(Lcom/sec/android/app/easylauncher/AppsGridAdapter;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 61
    return-void
.end method

.method removeData(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 263
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 264
    .local v1, "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 266
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f090008

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "emptyAppName":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 270
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 271
    return-void
.end method

.method setDragIndex(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 275
    iput p1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mDragIndex:I

    .line 276
    return-void
.end method

.method setWidgetIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 290
    iput p1, p0, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->mWidgetIndex:I

    .line 291
    return-void
.end method
