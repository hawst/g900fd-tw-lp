.class public Lcom/sec/android/app/easylauncher/lib/Lib;
.super Ljava/lang/Object;
.source "Lib.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    }
.end annotation


# static fields
.field private static final DEBUGGABLE:Z

.field public static final EASY_LAUNCHER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.easylauncher"

.field public static final EASY_LAUNCHER_SEARCH_ACTIVITY:Ljava/lang/String; = ".SearchActivity"

.field public static final SEC_LAUNCHER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.launcher"

.field private static final TAG:Ljava/lang/String; = "Easylauncher.Lib"

.field static easySettingsClass:Ljava/lang/String;

.field static easySettingsPackage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 54
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/lib/Lib;->DEBUGGABLE:Z

    .line 62
    const-string v0, "com.sec.android.easysettings"

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/Lib;->easySettingsPackage:Ljava/lang/String;

    .line 64
    const-string v0, "com.sec.android.easysettings.EasySettingsHomeActivity"

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/Lib;->easySettingsClass:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static checkInInvisibileApps(Lcom/sec/android/app/easylauncher/lib/AppInfo;Ljava/util/ArrayList;)Z
    .locals 4
    .param p0, "newApp"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "invisiblePackages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 134
    .local v1, "removeList":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 137
    const/4 v2, 0x1

    .line 140
    .end local v1    # "removeList":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static expandStatusBar(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    if-nez p0, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 259
    .local v0, "currentApiVersion":I
    const-string v5, "statusbar"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 260
    .local v3, "service":Ljava/lang/Object;
    const-string v5, "android.app.StatusBarManager"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 261
    .local v4, "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/16 v5, 0x10

    if-gt v0, v5, :cond_2

    .line 262
    const-string v5, "expand"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 263
    .local v2, "expand":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 268
    .end local v0    # "currentApiVersion":I
    .end local v2    # "expand":Ljava/lang/reflect/Method;
    .end local v3    # "service":Ljava/lang/Object;
    .end local v4    # "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v5, Lcom/sec/android/app/easylauncher/lib/Lib;->DEBUGGABLE:Z

    if-eqz v5, :cond_0

    .line 271
    const-string v5, "Easylauncher.Lib"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 265
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "currentApiVersion":I
    .restart local v3    # "service":Ljava/lang/Object;
    .restart local v4    # "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    :try_start_1
    const-string v5, "expandNotificationsPanel"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 266
    .restart local v2    # "expand":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static findActivitiesForPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 285
    .local v1, "mPkgMgr":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 286
    .local v2, "mainIntent":Landroid/content/Intent;
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 290
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v0, :cond_0

    .line 291
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 293
    :cond_0
    return-object v0
.end method

.method public static getApplicationsWithClass(Landroid/content/Context;Ljava/util/concurrent/ConcurrentSkipListMap;Ljava/util/List;)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ConcurrentSkipListMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "appData":Ljava/util/concurrent/ConcurrentSkipListMap;, "Ljava/util/concurrent/ConcurrentSkipListMap<Ljava/lang/String;Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    .local p2, "updateApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 210
    :cond_0
    return-void

    .line 150
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 151
    .local v11, "mPm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, Lcom/sec/android/app/easylauncher/lib/Lib;->getEasyAppsToShow(Landroid/content/Context;Ljava/util/concurrent/ConcurrentSkipListMap;Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v9

    .line 152
    .local v9, "invisiblePackages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    sget-boolean v15, Lcom/sec/android/app/easylauncher/lib/Lib;->DEBUGGABLE:Z

    if-eqz v15, :cond_2

    .line 153
    const-string v15, "Easylauncher.Lib"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Size Of Invisible list...."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_2
    const/4 v6, 0x0

    .line 155
    .local v6, "hide":Z
    move-object/from16 v3, p2

    .line 157
    .local v3, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v3, :cond_3

    .line 158
    new-instance v12, Landroid/content/Intent;

    const-string v15, "android.intent.action.MAIN"

    invoke-direct {v12, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 159
    .local v12, "mainIntent":Landroid/content/Intent;
    const-string v15, "android.intent.category.LAUNCHER"

    invoke-virtual {v12, v15}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    const/4 v15, 0x0

    invoke-virtual {v11, v12, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 164
    .end local v12    # "mainIntent":Landroid/content/Intent;
    :cond_3
    const/4 v4, 0x0

    .line 166
    .local v4, "dr":Landroid/graphics/drawable/Drawable;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 168
    .local v8, "info":Landroid/content/pm/ResolveInfo;
    :try_start_0
    iget-object v2, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 169
    .local v2, "activity":Landroid/content/pm/ActivityInfo;
    const/4 v6, 0x0

    .line 170
    iget-object v15, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v11, v15}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v14

    .line 172
    .local v14, "res":Landroid/content/res/Resources;
    new-instance v13, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-direct {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>()V

    .line 173
    .local v13, "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    iget-object v15, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 174
    new-instance v15, Landroid/content/ComponentName;

    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setComponentName(Landroid/content/ComponentName;)V

    .line 175
    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 176
    iget-object v15, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v15}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setIntent(Landroid/content/Intent;)V

    .line 177
    iget v15, v2, Landroid/content/pm/ActivityInfo;->icon:I

    if-eqz v15, :cond_5

    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v15

    :goto_1
    invoke-virtual {v11, v15}, Landroid/content/pm/PackageManager;->getCSCPackageItemIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 178
    if-eqz v4, :cond_6

    .line 179
    invoke-virtual {v13, v4}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 180
    const/4 v4, 0x0

    .line 186
    :goto_2
    invoke-virtual {v2, v11}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v15

    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 187
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 188
    .local v10, "key":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "com.sec.android.app.easylauncher"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 190
    invoke-static {v13, v9}, Lcom/sec/android/app/easylauncher/lib/Lib;->checkInInvisibileApps(Lcom/sec/android/app/easylauncher/lib/AppInfo;Ljava/util/ArrayList;)Z

    move-result v6

    .line 191
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Ljava/util/concurrent/ConcurrentSkipListMap;->containsKey(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 192
    if-eqz v6, :cond_4

    .line 193
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Ljava/util/concurrent/ConcurrentSkipListMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 204
    .end local v2    # "activity":Landroid/content/pm/ActivityInfo;
    .end local v10    # "key":Ljava/lang/String;
    .end local v13    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v14    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v5

    .line 205
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 177
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v2    # "activity":Landroid/content/pm/ActivityInfo;
    .restart local v13    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .restart local v14    # "res":Landroid/content/res/Resources;
    :cond_5
    :try_start_1
    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v15

    goto :goto_1

    .line 182
    :cond_6
    sget-boolean v15, Lcom/sec/android/app/easylauncher/lib/Lib;->DEBUGGABLE:Z

    if-eqz v15, :cond_7

    .line 183
    const-string v15, "Easylauncher.Lib"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "setListAppIcon newApp.getPackageName()"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v13}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_7
    invoke-virtual {v8}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setListAppIcon(Landroid/content/res/Resources;I)V

    goto :goto_2

    .line 196
    .restart local v10    # "key":Ljava/lang/String;
    :cond_8
    if-nez v6, :cond_4

    .line 197
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v13}, Ljava/util/concurrent/ConcurrentSkipListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static getEasyAppsToShow(Landroid/content/Context;Ljava/util/concurrent/ConcurrentSkipListMap;Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "mPm"    # Landroid/content/pm/PackageManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ConcurrentSkipListMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;",
            "Landroid/content/pm/PackageManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "appData":Ljava/util/concurrent/ConcurrentSkipListMap;, "Ljava/util/concurrent/ConcurrentSkipListMap<Ljava/lang/String;Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    new-instance v9, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 71
    .local v9, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v15, "invisiblePackages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const/4 v6, 0x0

    .line 76
    .local v6, "dr":Landroid/graphics/drawable/Drawable;
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 82
    .local v14, "intent":Landroid/content/Intent;
    const-string v19, "favorites"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getUserHomescreenApps(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 83
    const-string v19, "Easylauncher.Lib"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getEasyAppsToShow invisiblePackages : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v0, v9, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getEasyApps(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v8

    .line 87
    .local v8, "easyAppsInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;

    .line 88
    .local v13, "info":Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    iget-object v0, v13, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v13, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->className:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const/16 v19, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v14, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 90
    .local v11, "i":Landroid/content/pm/ResolveInfo;
    if-eqz v11, :cond_0

    .line 91
    iget-object v4, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 92
    .local v4, "activity":Landroid/content/pm/ActivityInfo;
    if-eqz v4, :cond_0

    .line 94
    :try_start_0
    new-instance v5, Landroid/content/ComponentName;

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v5, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .local v5, "cn":Landroid/content/ComponentName;
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v18

    .line 97
    .local v18, "res":Landroid/content/res/Resources;
    new-instance v17, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v20

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 99
    .local v17, "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    iget v0, v4, Landroid/content/pm/ActivityInfo;->icon:I

    move/from16 v19, v0

    if-eqz v19, :cond_1

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v19

    :goto_1
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getCSCPackageItemIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 100
    if-eqz v6, :cond_2

    .line 101
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 102
    const/4 v6, 0x0

    .line 106
    :goto_2
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 107
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setComponentName(Landroid/content/ComponentName;)V

    .line 109
    iget v0, v13, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->visibility:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 110
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 111
    .local v16, "key":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v0, v15}, Lcom/sec/android/app/easylauncher/lib/Lib;->checkInInvisibileApps(Lcom/sec/android/app/easylauncher/lib/AppInfo;Ljava/util/ArrayList;)Z

    move-result v10

    .line 112
    .local v10, "hide":Z
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentSkipListMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_0

    if-nez v10, :cond_0

    .line 113
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentSkipListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 118
    .end local v5    # "cn":Landroid/content/ComponentName;
    .end local v10    # "hide":Z
    .end local v16    # "key":Ljava/lang/String;
    .end local v17    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v18    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v7

    .line 120
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 99
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5    # "cn":Landroid/content/ComponentName;
    .restart local v17    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .restart local v18    # "res":Landroid/content/res/Resources;
    :cond_1
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v19

    goto :goto_1

    .line 104
    :cond_2
    invoke-virtual {v4}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v19

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/content/res/Resources;I)V

    goto :goto_2

    .line 116
    :cond_3
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 125
    .end local v4    # "activity":Landroid/content/pm/ActivityInfo;
    .end local v5    # "cn":Landroid/content/ComponentName;
    .end local v11    # "i":Landroid/content/pm/ResolveInfo;
    .end local v13    # "info":Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    .end local v17    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v18    # "res":Landroid/content/res/Resources;
    :cond_4
    if-eqz v9, :cond_5

    .line 126
    invoke-virtual {v9}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 127
    :cond_5
    return-object v15
.end method

.method public static getSettingsInfo(Landroid/content/Context;)Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 215
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 216
    .local v4, "mPm":Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/ComponentName;

    sget-object v5, Lcom/sec/android/app/easylauncher/lib/Lib;->easySettingsPackage:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/app/easylauncher/lib/Lib;->easySettingsClass:Ljava/lang/String;

    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    .local v1, "cn":Landroid/content/ComponentName;
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>()V

    .line 219
    .local v0, "appInfo":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const/16 v5, 0x20

    :try_start_0
    invoke-virtual {v4, v1, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v3

    .line 220
    .local v3, "info":Landroid/content/pm/ActivityInfo;
    invoke-virtual {v3, v4}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 221
    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setComponentName(Landroid/content/ComponentName;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    .end local v3    # "info":Landroid/content/pm/ActivityInfo;
    :goto_0
    return-object v0

    .line 222
    :catch_0
    move-exception v2

    .line 223
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "Easylauncher.Lib"

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static launchApp(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 4
    .param p0, "dvfsLockIntent"    # Landroid/content/Intent;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 230
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    const/high16 v1, 0x10200000

    invoke-virtual {p0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 234
    const/high16 v1, 0x10000

    invoke-virtual {p0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 243
    :try_start_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 252
    :goto_0
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v1, "MENU"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to launch. tag= intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 246
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "MENU"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Launcher does not have the permission to launch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "or use the exported attribute for this activity. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static startSearchIntent(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 276
    const-string v2, "search"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 278
    .local v0, "searchManager":Landroid/app/SearchManager;
    const/4 v2, 0x0

    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v5, 0x1

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 279
    return-void
.end method
