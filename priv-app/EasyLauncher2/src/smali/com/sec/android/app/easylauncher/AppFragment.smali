.class public Lcom/sec/android/app/easylauncher/AppFragment;
.super Landroid/support/v4/app/Fragment;
.source "AppFragment.java"


# static fields
.field private static final DEBUGGABLE:Z

.field private static final REQUEST:I = 0x64


# instance fields
.field private final TAG:Ljava/lang/String;

.field private isDialogOpen:Z

.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field longClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mActivity:Landroid/app/Activity;

.field mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

.field mAllAppsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppCount:I

.field mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

.field mAppsGrid:Landroid/widget/GridView;

.field private mContext:Landroid/content/Context;

.field private mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

.field mEditDialog:Landroid/app/AlertDialog;

.field mEditPosition:I

.field private mHandler:Landroid/os/Handler;

.field mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

.field mMainView:Landroid/view/View;

.field private mPagePosition:I

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

.field private onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 68
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 66
    const-string v0, "EasyLauncher.AppFragment"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->TAG:Ljava/lang/String;

    .line 72
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    .line 74
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHandler:Landroid/os/Handler;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppCount:I

    .line 179
    new-instance v0, Lcom/sec/android/app/easylauncher/AppFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AppFragment$2;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 353
    new-instance v0, Lcom/sec/android/app/easylauncher/AppFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AppFragment$5;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->longClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 386
    new-instance v0, Lcom/sec/android/app/easylauncher/AppFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AppFragment$6;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    .line 405
    new-instance v0, Lcom/sec/android/app/easylauncher/AppFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AppFragment$7;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    .line 430
    new-instance v0, Lcom/sec/android/app/easylauncher/AppFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AppFragment$8;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/AppFragment;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/AppFragment;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/AppFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/easylauncher/AppFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/easylauncher/AppFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->removeShortCutDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/easylauncher/AppFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->showAppsList()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/easylauncher/AppFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600()Z
    .locals 1

    .prologue
    .line 50
    sget-boolean v0, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    return v0
.end method

.method private removeShortCutDialog()V
    .locals 3

    .prologue
    .line 484
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 485
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 486
    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 487
    const v1, 0x7f09001c

    new-instance v2, Lcom/sec/android/app/easylauncher/AppFragment$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/AppFragment$9;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 509
    const v1, 0x7f09001b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/AppFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/easylauncher/AppFragment$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/AppFragment$10;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 517
    new-instance v1, Lcom/sec/android/app/easylauncher/AppFragment$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/AppFragment$11;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 525
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditDialog:Landroid/app/AlertDialog;

    .line 526
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 527
    return-void
.end method

.method private showAppsList()V
    .locals 3

    .prologue
    .line 530
    sget-boolean v1, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 531
    const-string v1, "EasyLauncher.AppFragment"

    const-string v2, "Show apps list "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->getMoreAppsMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 555
    :goto_0
    return-void

    .line 538
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 540
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 541
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 543
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/Launcher;->setMoreAppsMode(Z)V

    .line 545
    iget v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    .line 546
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 549
    :cond_2
    const-string v1, "EditPosition"

    iget v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 550
    const-string v1, "ScreenPosion"

    iget v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 551
    const-string v1, "HelpMode"

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 552
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/AppFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private unBind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    new-instance v1, Lcom/sec/android/app/easylauncher/AppFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/AppFragment$4;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;->post(Ljava/lang/Runnable;)V

    .line 334
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    .line 335
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    .line 336
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditDialog:Landroid/app/AlertDialog;

    .line 337
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 342
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    .line 343
    return-void
.end method


# virtual methods
.method dismisPopUps()V
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 559
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 561
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditDialog:Landroid/app/AlertDialog;

    .line 564
    :cond_0
    return-void
.end method

.method public getEditMode()Z
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v0

    return v0
.end method

.method getLauncher()Lcom/sec/android/app/easylauncher/Launcher;
    .locals 1

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    .line 579
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    goto :goto_0
.end method

.method getPageCount()I
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getPageCount()I

    move-result v0

    return v0
.end method

.method public isLastScreen()Z
    .locals 2

    .prologue
    .line 588
    iget v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 589
    const/4 v0, 0x1

    .line 591
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 232
    const/16 v0, 0x64

    if-ne p1, v0, :cond_2

    .line 234
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->startLoading()V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    const-string v0, "EasyLauncher.AppFragment"

    const-string v1, "onActivityResult Called isEasyHelpAppRunning"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget v1, v0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 245
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    const v1, 0x7f090045

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpToast(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/easylauncher/AppFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/AppFragment$3;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 259
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->sayByTalkback(Ljava/lang/String;)V

    .line 268
    :cond_2
    :goto_0
    return-void

    .line 263
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addapps(I)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 568
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 569
    const-string v0, "EasyLauncher.AppFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----------onAttach : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mActivity:Landroid/app/Activity;

    .line 571
    return-void
.end method

.method public onBackPressed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    if-nez v0, :cond_0

    .line 289
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mActivity:Landroid/app/Activity;

    .line 292
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->refreshView()V

    .line 295
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 98
    const-string v1, "EasyLauncher.AppFragment"

    const-string v2, "-----------onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    .line 101
    if-eqz p1, :cond_0

    .line 102
    const-string v1, "mPagePosition"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    .line 103
    const-string v1, "EasyLauncher.AppFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----------mPagePosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_0
    sget v1, Lcom/sec/android/app/easylauncher/EasyController;->APPS_FRAGMENT_DEFAULT_APPS_COUNT:I

    iput v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppCount:I

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAllAppsList:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    .local v0, "filter":Landroid/content/IntentFilter;
    sget-object v1, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 114
    new-instance v1, Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAllAppsList:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, p0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/support/v4/app/Fragment;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    .line 116
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAllAppsList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppCount:I

    iget v5, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;-><init>(Landroid/content/Context;Ljava/util/ArrayList;II)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    .line 118
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    const v1, 0x7f03000a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 125
    .local v0, "v":Landroid/view/View;
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mMainView:Landroid/view/View;

    .line 127
    const-string v1, "EasyLauncher.AppFragment"

    const-string v2, "-----------onCreateView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    if-eqz p3, :cond_1

    .line 129
    const-string v1, "IsDialogOpen"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    if-eqz v1, :cond_0

    .line 132
    const-string v1, "mEditPosition"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    .line 133
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->removeShortCutDialog()V

    .line 135
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    if-eqz v1, :cond_1

    .line 136
    const-string v1, "EasyLauncher.AppFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateView:AppsEditMode :-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_1
    const v1, 0x7f0c0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/app/easylauncher/Launcher;->mUseLongClickRemove:Z

    if-eqz v1, :cond_2

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->longClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    new-instance v2, Lcom/sec/android/app/easylauncher/AppFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/AppFragment$1;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 167
    new-instance v2, Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object v1, v0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/easylauncher/HelpGuider;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_3

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addapps(I)V

    .line 175
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->startLoading()V

    .line 176
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->unBind()V

    .line 349
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 350
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 320
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 321
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 300
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 303
    sget-boolean v0, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 304
    const-string v0, "EasyLauncher.AppFragment"

    const-string v1, "onPause Called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isLongClickEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/AppFragment;->setupLongClickView(IZ)V

    .line 310
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 273
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->setMoreAppsMode(Z)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 281
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 203
    if-eqz p1, :cond_1

    .line 204
    sget-boolean v0, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "EasyLauncher.AppFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState:mEditMode -"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_0
    const-string v0, "IsDialogOpen"

    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    const-string v0, "mEditPosition"

    iget v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 208
    const-string v0, "mPagePosition"

    iget v1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 210
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 314
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 316
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 215
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 216
    if-eqz p1, :cond_0

    .line 218
    const-string v0, "IsDialogOpen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z

    .line 219
    const-string v0, "mEditPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    .line 220
    const-string v0, "mPagePosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    .line 221
    sget-boolean v0, Lcom/sec/android/app/easylauncher/AppFragment;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "EasyLauncher.AppFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewStateRestored:mEditMode - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPagePosition : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    return-void
.end method

.method public refreshView()V
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 599
    :cond_0
    return-void
.end method

.method public setScreenPosion(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mPagePosition:I

    .line 92
    return-void
.end method

.method setupLongClickView(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "longClickMode"    # Z

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->setDragIndex(I)V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 426
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForLongClickMode(Z)V

    .line 427
    return-void
.end method

.method startLoading()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->loadFromFavorites()V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 197
    :cond_1
    return-void
.end method

.method public updateBadge()V
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppsGrid:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/easylauncher/AppFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/AppFragment$12;-><init>(Lcom/sec/android/app/easylauncher/AppFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 615
    :cond_0
    return-void
.end method
