.class Lcom/sec/android/app/easylauncher/MainFragment$10;
.super Ljava/lang/Object;
.source "MainFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/MainFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 499
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iput p3, v3, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    .line 501
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 502
    .local v0, "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const/4 v1, 0x0

    .line 504
    .local v1, "cn":Landroid/content/ComponentName;
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 510
    if-eqz v0, :cond_2

    .line 511
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    .line 514
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/MainFragment;->getEditMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 515
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    # getter for: Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/MainFragment;->access$300(Lcom/sec/android/app/easylauncher/MainFragment;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 519
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getEditable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 523
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    const/4 v4, 0x1

    # setter for: Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/easylauncher/MainFragment;->access$302(Lcom/sec/android/app/easylauncher/MainFragment;Z)Z

    .line 524
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    # invokes: Lcom/sec/android/app/easylauncher/MainFragment;->removeShortCutDialog()V
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/MainFragment;->access$400(Lcom/sec/android/app/easylauncher/MainFragment;)V

    goto :goto_0

    .line 527
    :cond_3
    if-nez v1, :cond_4

    .line 528
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    # invokes: Lcom/sec/android/app/easylauncher/MainFragment;->showAppsList()V
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/MainFragment;->access$500(Lcom/sec/android/app/easylauncher/MainFragment;)V

    goto :goto_0

    .line 531
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 534
    :cond_5
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 535
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 536
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment$10;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
