.class public final Lcom/sec/android/app/easylauncher/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final bgFullHeight:I = 0x7f01001a

.field public static final cellHeight:I = 0x7f01000b

.field public static final cellSepLineColor:I = 0x7f010011

.field public static final cellSepLineMarginLeft:I = 0x7f010013

.field public static final cellSepLineMarginRight:I = 0x7f010014

.field public static final cellSepLineThick:I = 0x7f010012

.field public static final className:I = 0x7f010020

.field public static final firstCharColor:I = 0x7f010005

.field public static final firstCharPreMatches:I = 0x7f010009

.field public static final iconPosition:I = 0x7f010023

.field public static final indexGravity:I = 0x7f010000

.field public static final indexNormalBg:I = 0x7f01000c

.field public static final indexSelBg:I = 0x7f01000d

.field public static final indexSelItemBg:I = 0x7f01000e

.field public static final indexStr:I = 0x7f010017

.field public static final isFloating:I = 0x7f010019

.field public static final overflowGrpSepChar:I = 0x7f010008

.field public static final packageName:I = 0x7f010021

.field public static final popupHeight:I = 0x7f010002

.field public static final popupHeightInc:I = 0x7f010004

.field public static final popupWidth:I = 0x7f010001

.field public static final popupWidthInc:I = 0x7f010003

.field public static final pressDelay:I = 0x7f010006

.field public static final screen:I = 0x7f010022

.field public static final selTextColor:I = 0x7f010010

.field public static final spanX:I = 0x7f010024

.field public static final textBg:I = 0x7f01001b

.field public static final textCharPaddingLeft:I = 0x7f01001e

.field public static final textCharPaddingRight:I = 0x7f01001f

.field public static final textCharSpacingStrategy:I = 0x7f01001c

.field public static final textCharSpacingWidth:I = 0x7f01001d

.field public static final textColor:I = 0x7f01000f

.field public static final textFontSize:I = 0x7f010018

.field public static final textTypeface:I = 0x7f010015

.field public static final textTypefaceStyle:I = 0x7f010016

.field public static final textUppercase:I = 0x7f01000a

.field public static final unpressDelay:I = 0x7f010007

.field public static final viewType:I = 0x7f010026

.field public static final visibility:I = 0x7f010025


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
