.class Lcom/sec/android/app/easylauncher/AppFragment$8;
.super Ljava/lang/Object;
.source "AppFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AppFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AppFragment;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 434
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 435
    .local v0, "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const/4 v1, 0x0

    .line 437
    .local v1, "cn":Landroid/content/ComponentName;
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 439
    if-eqz v0, :cond_0

    .line 440
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    .line 443
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 444
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 445
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    sget-boolean v3, Lcom/sec/android/app/easylauncher/Launcher;->mIsEasyHelpComplete:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v3, v3, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget v3, v3, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    add-int/lit8 v3, v3, -0x1

    if-le p3, v3, :cond_3

    .line 480
    :cond_1
    :goto_0
    return-void

    .line 450
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v3, v3, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget v3, v3, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    if-ne p3, v3, :cond_1

    if-nez v1, :cond_1

    .line 456
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iput p3, v3, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    .line 458
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 459
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # getter for: Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->access$200(Lcom/sec/android/app/easylauncher/AppFragment;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 462
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    const/4 v4, 0x1

    # setter for: Lcom/sec/android/app/easylauncher/AppFragment;->isDialogOpen:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/easylauncher/AppFragment;->access$202(Lcom/sec/android/app/easylauncher/AppFragment;Z)Z

    .line 463
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # invokes: Lcom/sec/android/app/easylauncher/AppFragment;->removeShortCutDialog()V
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->access$300(Lcom/sec/android/app/easylauncher/AppFragment;)V

    goto :goto_0

    .line 466
    :cond_4
    if-nez v1, :cond_5

    .line 467
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # invokes: Lcom/sec/android/app/easylauncher/AppFragment;->showAppsList()V
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->access$400(Lcom/sec/android/app/easylauncher/AppFragment;)V

    goto :goto_0

    .line 469
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v3

    if-nez v3, :cond_1

    .line 473
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 474
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 475
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$8;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
