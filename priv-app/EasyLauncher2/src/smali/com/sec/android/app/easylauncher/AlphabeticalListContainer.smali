.class public Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;
.super Landroid/view/ViewGroup;
.source "AlphabeticalListContainer.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field private static final FASTSCROLLINDEX_GRAVITY_LEFT:I = 0x0

.field private static final FASTSCROLLINDEX_GRAVITY_RIGHT:I = 0x1


# instance fields
.field private final mCollator:Ljava/text/Collator;

.field private mCurrentFastScrollIndexView:I

.field private final mFastScrollIndexGravity:I

.field private final mFastScrollIndexGrpBoundary:Landroid/graphics/RectF;

.field private mFastScrollIndexViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;",
            ">;"
        }
    .end annotation
.end field

.field private mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

.field private final mFirstCharColor:I

.field private final mLastMovePt:Landroid/graphics/PointF;

.field private mLastRawTouch:Landroid/graphics/PointF;

.field private mLastSelLen:I

.field private final mListBestMatchIndices:Landroid/util/SparseIntArray;

.field private final mListExactMatchIndices:Landroid/util/SparseIntArray;

.field private mListView:Landroid/widget/ListView;

.field private final mPopupHeight:F

.field private final mPopupHeightInc:F

.field private final mPopupWidth:F

.field private final mPopupWidthInc:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGrpBoundary:Landroid/graphics/RectF;

    .line 49
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastMovePt:Landroid/graphics/PointF;

    .line 51
    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastSelLen:I

    .line 53
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListBestMatchIndices:Landroid/util/SparseIntArray;

    .line 55
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListExactMatchIndices:Landroid/util/SparseIntArray;

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    .line 59
    iput-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    .line 61
    iput-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    .line 69
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastRawTouch:Landroid/graphics/PointF;

    .line 71
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCollator:Ljava/text/Collator;

    .line 76
    sget-object v1, Lcom/sec/android/app/easylauncher/R$styleable;->AlphabeticalListContainer:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 77
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 78
    iput v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGravity:I

    .line 83
    :goto_0
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupWidth:F

    .line 84
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupHeight:F

    .line 85
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupWidthInc:F

    .line 86
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupHeightInc:F

    .line 87
    const/4 v1, 0x5

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFirstCharColor:I

    .line 90
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 91
    return-void

    .line 80
    :cond_0
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGravity:I

    goto :goto_0
.end method

.method private clearIndexMatches()V
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListBestMatchIndices:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListExactMatchIndices:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 613
    return-void
.end method

.method private feedPtToIndexView(ILandroid/graphics/PointF;)V
    .locals 7
    .param p1, "aViewIndex"    # I
    .param p2, "aPt"    # Landroid/graphics/PointF;

    .prologue
    .line 245
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 246
    .local v1, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 247
    .local v3, "relPt":Landroid/graphics/PointF;
    invoke-virtual {v3, p2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 248
    iget v5, v3, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getLeft()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/PointF;->x:F

    .line 249
    iget v5, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getTop()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/PointF;->y:F

    .line 250
    invoke-virtual {v1, p2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->feedPt(Landroid/graphics/PointF;)I

    move-result v4

    .line 251
    .local v4, "res":I
    packed-switch v4, :pswitch_data_0

    .line 270
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 254
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getSelection(I)Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "currentSel":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 256
    invoke-direct {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->updatePopup(Ljava/lang/String;)V

    .line 257
    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v2, 0x1

    .line 258
    .local v2, "moveToTop":Z
    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->updateList(ZLjava/lang/String;)V

    goto :goto_0

    .line 257
    .end local v2    # "moveToTop":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 262
    .end local v0    # "currentSel":Ljava/lang/String;
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    if-eqz v5, :cond_0

    .line 263
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setVisibility(I)V

    goto :goto_0

    .line 251
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getMatchingIndex(Ljava/lang/String;IIZZ)I
    .locals 10
    .param p1, "aStr"    # Ljava/lang/String;
    .param p2, "aAdapterStartIndex"    # I
    .param p3, "aAdapterEndIndex"    # I
    .param p4, "aExact"    # Z
    .param p5, "aAccountForExpandedGroups"    # Z

    .prologue
    const/4 v6, -0x1

    .line 171
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_1

    move v3, v6

    .line 201
    :cond_0
    :goto_0
    return v3

    .line 175
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    .line 176
    .local v0, "adapter":Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->getCount()I

    move-result v7

    add-int/lit8 v5, v7, -0x1

    .line 177
    .local v5, "len":I
    if-ltz p2, :cond_2

    if-le p2, v5, :cond_3

    .line 178
    :cond_2
    const/4 p2, 0x0

    .line 179
    :cond_3
    if-ltz p3, :cond_4

    if-le p3, v5, :cond_5

    .line 180
    :cond_4
    move p3, v5

    .line 182
    :cond_5
    const/4 v1, -0x1

    .local v1, "cmpRes":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 183
    .local v2, "curLen":I
    move v3, p2

    .local v3, "i":I
    :goto_1
    if-gt v3, p3, :cond_8

    .line 184
    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v4

    .line 185
    .local v4, "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_7

    .line 183
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 187
    :cond_7
    if-eqz p4, :cond_9

    .line 188
    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v2, v7, :cond_6

    .line 190
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCollator:Ljava/text/Collator;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, p1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 194
    :goto_2
    if-ltz v1, :cond_6

    .line 198
    .end local v4    # "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_8
    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    move v3, v6

    .line 201
    goto :goto_0

    .line 192
    .restart local v4    # "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCollator:Ljava/text/Collator;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, p1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_2
.end method

.method private getNextLevelIndexStr(ILjava/lang/String;Z)Ljava/lang/String;
    .locals 18
    .param p1, "aViewIndex"    # I
    .param p2, "currentSel"    # Ljava/lang/String;
    .param p3, "isUppercase"    # Z

    .prologue
    .line 110
    if-nez p2, :cond_0

    .line 111
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getSelection(I)Ljava/lang/String;

    move-result-object p2

    .line 112
    if-nez p2, :cond_0

    .line 113
    const/4 v1, 0x0

    .line 166
    :goto_0
    return-object v1

    .line 115
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v12

    .line 117
    .local v12, "firstChar":C
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListExactMatchIndices:Landroid/util/SparseIntArray;

    const/4 v2, -0x1

    invoke-virtual {v1, v12, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v14

    .line 118
    .local v14, "index":I
    const/4 v1, -0x1

    if-ne v1, v14, :cond_2

    .line 119
    invoke-static {v12}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getMatchingIndex(Ljava/lang/String;IIZZ)I

    move-result v14

    .line 120
    const/4 v1, -0x1

    if-ne v1, v14, :cond_1

    .line 121
    const/4 v1, 0x0

    goto :goto_0

    .line 123
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListExactMatchIndices:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v12, v14}, Landroid/util/SparseIntArray;->put(II)V

    .line 126
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    .line 127
    .local v7, "adapter":Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->getCount()I

    move-result v17

    .local v17, "len":I
    const/4 v10, -0x1

    .local v10, "cmpRes":I
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v11

    .line 130
    .local v11, "curLen":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .local v15, "indexStr":Ljava/lang/StringBuilder;
    move v13, v14

    .local v13, "i":I
    :goto_1
    move/from16 v0, v17

    if-ge v13, v0, :cond_5

    .line 132
    invoke-virtual {v7, v13}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v16

    .line 136
    .local v16, "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v11, :cond_4

    .line 131
    :cond_3
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 139
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCollator:Ljava/text/Collator;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v1, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 140
    if-lez v10, :cond_6

    .line 163
    .end local v16    # "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_5
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_9

    .line 164
    const/4 v1, 0x0

    goto :goto_0

    .line 142
    .restart local v16    # "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_6
    if-ltz v10, :cond_3

    .line 145
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 146
    .local v8, "ch":C
    if-eqz p3, :cond_8

    .line 147
    invoke-static {v8}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v8

    .line 148
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    .line 149
    .local v9, "check":Ljava/lang/String;
    const/4 v1, -0x1

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 151
    invoke-static {v8}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v8

    .line 152
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    .line 153
    const/4 v1, -0x1

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 160
    :cond_7
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 156
    .end local v9    # "check":Ljava/lang/String;
    :cond_8
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    .line 157
    .restart local v9    # "check":Ljava/lang/String;
    const/4 v1, -0x1

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_7

    goto :goto_2

    .line 166
    .end local v8    # "ch":C
    .end local v9    # "check":Ljava/lang/String;
    .end local v16    # "item":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_9
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private getSelection(I)Ljava/lang/String;
    .locals 5
    .param p1, "aLevel"    # I

    .prologue
    .line 94
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 98
    .local v3, "res":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-gt v1, p1, :cond_1

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 100
    .local v2, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCurrentChar()Ljava/lang/Character;

    move-result-object v0

    .line 101
    .local v0, "ch":Ljava/lang/Character;
    if-nez v0, :cond_0

    .line 102
    const/4 v4, 0x0

    .line 106
    .end local v0    # "ch":Ljava/lang/Character;
    .end local v2    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :goto_1
    return-object v4

    .line 104
    .restart local v0    # "ch":Ljava/lang/Character;
    .restart local v2    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 98
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 106
    .end local v0    # "ch":Ljava/lang/Character;
    .end local v2    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private handleMove(Landroid/graphics/PointF;)Z
    .locals 7
    .param p1, "aPt"    # Landroid/graphics/PointF;

    .prologue
    const/4 v6, 0x1

    .line 359
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 362
    .local v2, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    const/4 v0, 0x0

    .line 363
    .local v0, "dir":I
    iget v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGravity:I

    packed-switch v4, :pswitch_data_0

    .line 381
    :cond_0
    :goto_0
    if-ne v0, v6, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastMovePt:Landroid/graphics/PointF;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->Contains(Landroid/graphics/PointF;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 386
    const/4 v0, 0x0

    .line 389
    :cond_1
    if-gez v0, :cond_7

    .line 390
    iget v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_2

    .line 391
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    check-cast v2, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 392
    .restart local v2    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->Contains(Landroid/graphics/PointF;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 393
    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    .line 417
    .end local v1    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastMovePt:Landroid/graphics/PointF;

    invoke-virtual {v4, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 418
    iget v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    invoke-direct {p0, v4, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->feedPtToIndexView(ILandroid/graphics/PointF;)V

    .line 419
    return v6

    .line 365
    :pswitch_0
    iget v4, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getLeft()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 366
    const/4 v0, 0x1

    goto :goto_0

    .line 367
    :cond_3
    iget v4, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getRight()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 368
    const/4 v0, -0x1

    goto :goto_0

    .line 372
    :pswitch_1
    iget v4, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getRight()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 373
    const/4 v0, 0x1

    goto :goto_0

    .line 374
    :cond_4
    iget v4, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getLeft()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 375
    const/4 v0, -0x1

    goto :goto_0

    .line 396
    .restart local v1    # "i":I
    :cond_5
    if-lez v1, :cond_6

    .line 397
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setVisibility(I)V

    .line 398
    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->clearCurrentChar()V

    .line 390
    :cond_6
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 401
    .end local v1    # "i":I
    :cond_7
    if-lez v0, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isPreMatching()Z

    move-result v4

    if-nez v4, :cond_2

    .line 406
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 407
    .local v3, "size":I
    iget v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    add-int/lit8 v1, v4, 0x1

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v3, :cond_2

    .line 408
    const/4 v4, 0x0

    invoke-direct {p0, p1, v4, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->showNextLevel(Landroid/graphics/PointF;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 411
    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    .line 412
    invoke-virtual {v2, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->Contains(Landroid/graphics/PointF;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 407
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleUp()Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, -0x1

    .line 305
    iget v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 307
    .local v1, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->clearCurrentChar()V

    .line 308
    if-lez v0, :cond_0

    .line 309
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setPressedImmediate(Z)V

    .line 310
    invoke-virtual {v1, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setVisibility(I)V

    .line 305
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 312
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->unPressDelayed()Z

    goto :goto_1

    .line 315
    .end local v1    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    if-eqz v2, :cond_2

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setVisibility(I)V

    .line 318
    :cond_2
    iput v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    .line 319
    iput v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastSelLen:I

    .line 320
    const/4 v2, 0x1

    return v2
.end method

.method private showNextLevel(Landroid/graphics/PointF;Ljava/lang/String;I)Z
    .locals 9
    .param p1, "aPt"    # Landroid/graphics/PointF;
    .param p2, "aCurrentStr"    # Ljava/lang/String;
    .param p3, "aViewIndex"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 324
    if-ge p3, v7, :cond_0

    move v5, v6

    .line 355
    :goto_0
    return v5

    .line 327
    :cond_0
    add-int/lit8 v3, p3, -0x1

    .line 328
    .local v3, "prevLevel":I
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 329
    .local v0, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isTextUppercase()Z

    move-result v5

    invoke-direct {p0, v3, p2, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getNextLevelIndexStr(ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 331
    .local v2, "nextLevelStr":Ljava/lang/String;
    if-nez v2, :cond_1

    move v5, v6

    .line 332
    goto :goto_0

    .line 335
    :cond_1
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    .line 336
    .local v4, "relPt":Landroid/graphics/PointF;
    invoke-virtual {v4, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 337
    iget v5, v4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getLeft()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v5, v8

    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 338
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getMidYForCurrentChar()F

    move-result v1

    .line 339
    .local v1, "indexY":F
    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v5, v1, v5

    if-eqz v5, :cond_2

    .line 340
    iput v1, v4, Landroid/graphics/PointF;->y:F

    .line 345
    :goto_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v8, "CscFeature_Launcher_DisableFastScrollIndex"

    invoke-virtual {v5, v8}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 347
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setVisibility(I)V

    .line 353
    :goto_2
    invoke-virtual {v0, v7}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setPressedImmediate(Z)V

    .line 354
    invoke-virtual {v0, v2, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setIndexStr(Ljava/lang/String;Landroid/graphics/PointF;)V

    move v5, v7

    .line 355
    goto :goto_0

    .line 342
    :cond_2
    iget v5, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getTop()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v5, v8

    iput v5, v4, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 350
    :cond_3
    invoke-virtual {v0, v6}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setVisibility(I)V

    goto :goto_2
.end method

.method private updateList(ZLjava/lang/String;)V
    .locals 12
    .param p1, "aMoveToTop"    # Z
    .param p2, "aSelStr"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 205
    const/4 v7, -0x1

    .line 206
    .local v7, "index":I
    if-eqz p1, :cond_0

    .line 207
    const/4 v7, 0x0

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 219
    return-void

    .line 209
    :cond_0
    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 210
    .local v11, "firstChar":C
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListBestMatchIndices:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v11, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v7

    .line 211
    if-ne v7, v2, :cond_1

    .line 212
    invoke-static {v11}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getMatchingIndex(Ljava/lang/String;IIZZ)I

    move-result v7

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListBestMatchIndices:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v11, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 215
    :cond_1
    const/4 v10, 0x1

    move-object v5, p0

    move-object v6, p2

    move v8, v2

    move v9, v4

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getMatchingIndex(Ljava/lang/String;IIZZ)I

    move-result v7

    goto :goto_0
.end method

.method private updatePopup(Ljava/lang/String;)V
    .locals 9
    .param p1, "aSelStr"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 222
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    if-nez v3, :cond_0

    .line 242
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 226
    .local v1, "newLen":I
    iget v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastSelLen:I

    if-eq v1, v3, :cond_2

    .line 227
    new-instance v2, Landroid/graphics/PointF;

    iget v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupWidth:F

    add-int/lit8 v4, v1, -0x1

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupWidthInc:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupHeight:F

    add-int/lit8 v5, v1, -0x1

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mPopupHeightInc:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 229
    .local v2, "newSize":Landroid/graphics/PointF;
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setDrawBounds(Landroid/graphics/PointF;)V

    .line 230
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->clearStyles()V

    .line 231
    if-le v1, v8, :cond_1

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->getTextColor()I

    move-result v0

    .line 233
    .local v0, "defColor":I
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    iget v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFirstCharColor:I

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v7, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setStyle(ILandroid/text/style/CharacterStyle;)V

    .line 234
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v8, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setStyle(ILandroid/text/style/CharacterStyle;)V

    .line 236
    .end local v0    # "defColor":I
    :cond_1
    iput v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastSelLen:I

    .line 238
    .end local v2    # "newSize":Landroid/graphics/PointF;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_3

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setVisibility(I)V

    .line 241
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateViewIndexes()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 616
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->clearIndexMatches()V

    .line 617
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 618
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 619
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 620
    .local v1, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->clearCurrentChar()V

    .line 621
    if-lez v0, :cond_0

    .line 622
    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setIndexStr(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 618
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 624
    .end local v1    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_1
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 447
    invoke-static {p0, p1}, Lcom/sec/android/app/easylauncher/Utilities;->onViewDraw(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 448
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 449
    return-void
.end method

.method public getAdapter()Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    .line 602
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastRawTouchPoint()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastRawTouch:Landroid/graphics/PointF;

    return-object v0
.end method

.method public notifyDataChanged()V
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->notifyDataSetChanged()V

    .line 587
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->updateViewIndexes()V

    .line 589
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 547
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 548
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getChildCount()I

    move-result v0

    .line 554
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 555
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 556
    .local v2, "view":Landroid/view/View;
    instance-of v3, v2, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    if-eqz v3, :cond_1

    .line 557
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    move-object v3, v2

    check-cast v3, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Launcher_DisableFastScrollIndex"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 560
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 554
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 562
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    if-nez v3, :cond_0

    instance-of v3, v2, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    if-eqz v3, :cond_0

    move-object v3, v2

    .line 563
    check-cast v3, Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollTextView:Lcom/sec/android/app/easylauncher/AlphabeticalListTextPopup;

    .line 565
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Launcher_DisableFastScrollIndex"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 567
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 572
    .end local v2    # "view":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 573
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setAllowWiggle(Z)V

    .line 576
    :cond_3
    const v3, 0x7f0c0016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    .line 580
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 582
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "aEv"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 274
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v4, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-eqz v4, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v2

    .line 277
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastRawTouch:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 278
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {v1, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 279
    .local v1, "pt":Landroid/graphics/PointF;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGrpBoundary:Landroid/graphics/RectF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 286
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 287
    .local v0, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->Contains(Landroid/graphics/PointF;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 290
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mLastSelLen:I

    .line 291
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->feedPtToIndexView(ILandroid/graphics/PointF;)V

    .line 293
    iput v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    .line 294
    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setPressedImmediate(Z)V

    move v2, v3

    .line 295
    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 26
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 453
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getPaddingLeft()I

    move-result v18

    .line 454
    .local v18, "padLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getPaddingTop()I

    move-result v19

    .line 455
    .local v19, "padTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getMeasuredWidth()I

    move-result v21

    sub-int v21, v21, v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getPaddingRight()I

    move-result v22

    sub-int v6, v21, v22

    .line 456
    .local v6, "availWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getMeasuredHeight()I

    move-result v21

    sub-int v21, v21, v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getPaddingBottom()I

    move-result v22

    sub-int v4, v21, v22

    .line 457
    .local v4, "availHeight":I
    new-instance v5, Landroid/graphics/RectF;

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v22, v0

    add-int v23, v18, v6

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    add-int v24, v19, v4

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 460
    .local v5, "availRect":Landroid/graphics/RectF;
    if-eqz p1, :cond_0

    .line 461
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->handleUp()Z

    .line 464
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGravity:I

    move/from16 v21, v0

    if-nez v21, :cond_2

    move/from16 v14, v18

    .line 467
    .local v14, "indexViewOffset":I
    :goto_0
    const/16 v20, 0x0

    .line 470
    .local v20, "totalIndexViewsWidth":I
    move/from16 v16, v18

    .line 471
    .local v16, "listViewLeft":I
    add-int v17, v18, v6

    .line 473
    .local v17, "listViewRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getChildCount()I

    move-result v12

    .line 474
    .local v12, "count":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v12, :cond_5

    .line 475
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 476
    .local v9, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    if-ne v9, v0, :cond_3

    .line 474
    :cond_1
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .end local v9    # "child":Landroid/view/View;
    .end local v12    # "count":I
    .end local v13    # "i":I
    .end local v14    # "indexViewOffset":I
    .end local v16    # "listViewLeft":I
    .end local v17    # "listViewRight":I
    .end local v20    # "totalIndexViewsWidth":I
    :cond_2
    move v14, v6

    .line 464
    goto :goto_0

    .line 483
    .restart local v9    # "child":Landroid/view/View;
    .restart local v12    # "count":I
    .restart local v13    # "i":I
    .restart local v14    # "indexViewOffset":I
    .restart local v16    # "listViewLeft":I
    .restart local v17    # "listViewRight":I
    .restart local v20    # "totalIndexViewsWidth":I
    :cond_3
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    .line 484
    .local v11, "childWidth":I
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 486
    .local v10, "childHeight":I
    instance-of v0, v9, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    move/from16 v21, v0

    if-eqz v21, :cond_4

    move-object v15, v9

    .line 487
    check-cast v15, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 493
    .local v15, "indicator":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    add-int v20, v20, v11

    .line 494
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGravity:I

    move/from16 v21, v0

    packed-switch v21, :pswitch_data_0

    goto :goto_2

    .line 496
    :pswitch_0
    add-int v21, v14, v11

    add-int v22, v19, v10

    move/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v15, v14, v0, v1, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->layout(IIII)V

    .line 498
    add-int/2addr v14, v11

    .line 499
    invoke-virtual {v15}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isFloating()Z

    move-result v21

    if-nez v21, :cond_1

    .line 500
    move/from16 v16, v14

    goto :goto_2

    .line 504
    :pswitch_1
    sub-int v21, v14, v11

    add-int v22, v19, v10

    move/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v22

    invoke-virtual {v15, v0, v1, v14, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->layout(IIII)V

    .line 506
    sub-int/2addr v14, v11

    .line 507
    invoke-virtual {v15}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isFloating()Z

    move-result v21

    if-nez v21, :cond_1

    .line 508
    move/from16 v17, v14

    goto :goto_2

    .line 517
    .end local v15    # "indicator":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_4
    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v22

    int-to-float v0, v11

    move/from16 v23, v0

    sub-float v22, v22, v23

    const/high16 v23, 0x40000000    # 2.0f

    div-float v22, v22, v23

    add-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v7, v0

    .line 518
    .local v7, "centeredLeft":I
    iget v0, v5, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v22

    int-to-float v0, v10

    move/from16 v23, v0

    sub-float v22, v22, v23

    const/high16 v23, 0x40000000    # 2.0f

    div-float v22, v22, v23

    add-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v8, v0

    .line 519
    .local v8, "centeredTop":I
    add-int v21, v7, v11

    add-int v22, v8, v10

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v9, v7, v8, v0, v1}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_2

    .line 523
    .end local v7    # "centeredLeft":I
    .end local v8    # "centeredTop":I
    .end local v9    # "child":Landroid/view/View;
    .end local v10    # "childHeight":I
    .end local v11    # "childWidth":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v10

    .line 524
    .restart local v10    # "childHeight":I
    iget v0, v5, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v22

    int-to-float v0, v10

    move/from16 v23, v0

    sub-float v22, v22, v23

    const/high16 v23, 0x40000000    # 2.0f

    div-float v22, v22, v23

    add-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v8, v0

    .line 525
    .restart local v8    # "centeredTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    move-object/from16 v21, v0

    add-int v22, v8, v10

    move-object/from16 v0, v21

    move/from16 v1, v16

    move/from16 v2, v17

    move/from16 v3, v22

    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/widget/ListView;->layout(IIII)V

    .line 531
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGravity:I

    move/from16 v21, v0

    packed-switch v21, :pswitch_data_1

    .line 543
    :goto_3
    return-void

    .line 533
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGrpBoundary:Landroid/graphics/RectF;

    move-object/from16 v21, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v24, v0

    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    invoke-virtual/range {v21 .. v25}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_3

    .line 537
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexGrpBoundary:Landroid/graphics/RectF;

    move-object/from16 v21, v0

    iget v0, v5, Landroid/graphics/RectF;->right:F

    move/from16 v22, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v23, v0

    iget v0, v5, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    invoke-virtual/range {v21 .. v25}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_3

    .line 494
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 531
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 441
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 442
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->measureChildren(II)V

    .line 443
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 628
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 631
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mFastScrollIndexViews:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;

    .line 633
    .local v0, "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    if-nez p2, :cond_1

    .line 634
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->unPressDelayed()Z

    .line 639
    .end local v0    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_0
    :goto_0
    return-void

    .line 636
    .restart local v0    # "indexView":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->pressDelayed()Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "aEv"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 424
    iget v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mCurrentFastScrollIndexView:I

    if-gez v1, :cond_0

    .line 436
    :goto_0
    return v0

    .line 427
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 434
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 430
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->handleUp()Z

    move-result v0

    goto :goto_0

    .line 432
    :pswitch_2
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->handleMove(Landroid/graphics/PointF;)Z

    move-result v0

    goto :goto_0

    .line 427
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setAdapter(Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 594
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->updateViewIndexes()V

    .line 596
    :cond_0
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 608
    :cond_0
    return-void
.end method
