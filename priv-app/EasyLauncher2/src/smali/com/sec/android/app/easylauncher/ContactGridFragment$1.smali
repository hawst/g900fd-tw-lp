.class Lcom/sec/android/app/easylauncher/ContactGridFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "ContactGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/ContactGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/ContactGridFragment;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;->this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 157
    const-string v1, "android.action.easymodecontactswidget.HELP_RESULT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;->this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    if-nez v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    const-string v1, "result_success"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 163
    .local v0, "helpResult":Z
    # getter for: Lcom/sec/android/app/easylauncher/ContactGridFragment;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->access$000()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    const-string v1, "EasyLauncher.ContactFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mReceiver:helpResult changes value:-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_2
    if-eqz v0, :cond_3

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;->this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(Z)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;->this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/easylauncher/Launcher;->setHelpResult(Z)V

    goto :goto_0

    .line 171
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;->this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(Z)V

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/ContactGridFragment$1;->this$0:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/ContactGridFragment;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addcontact_step1()V

    goto :goto_0
.end method
