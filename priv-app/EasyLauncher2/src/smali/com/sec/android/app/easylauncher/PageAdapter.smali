.class public Lcom/sec/android/app/easylauncher/PageAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "PageAdapter.java"


# static fields
.field static final APP_PAGE_COUNT:I = 0x3

.field static final CONTACTS_PAGE_COUNT:I = 0x2

.field static final HELP_PAGE_COUNT:I = 0x2

.field static final PAGE_COUNT:I = 0x6

.field private static fragmentTags:[Ljava/lang/String;


# instance fields
.field final HELP_APPS_POSITION:I

.field final HELP_CONTACT_POSITION:I

.field final PAGE_APPS1:I

.field final PAGE_APPS2:I

.field final PAGE_APPS3:I

.field final PAGE_CONTACTS1:I

.field final PAGE_CONTACTS2:I

.field final PAGE_HELP_APPS:I

.field final PAGE_HELP_CONTACT:I

.field final PAGE_HOME:I

.field mActivity:Lcom/sec/android/app/easylauncher/Launcher;

.field mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

.field mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

.field mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

.field mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

.field mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/easylauncher/PageAdapter;->fragmentTags:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 3
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 44
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 20
    new-array v0, v1, [Lcom/sec/android/app/easylauncher/ContactGridFragment;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .line 22
    new-array v0, v2, [Lcom/sec/android/app/easylauncher/AppFragment;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_CONTACTS1:I

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_CONTACTS2:I

    .line 32
    iput v1, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_HOME:I

    .line 33
    iput v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_APPS1:I

    .line 34
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_APPS2:I

    .line 35
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_APPS3:I

    .line 37
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_HELP_APPS:I

    .line 38
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_HELP_CONTACT:I

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->HELP_APPS_POSITION:I

    .line 41
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->HELP_CONTACT_POSITION:I

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Lcom/sec/android/app/easylauncher/Launcher;)V
    .locals 3
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "activity"    # Lcom/sec/android/app/easylauncher/Launcher;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 49
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 20
    new-array v0, v1, [Lcom/sec/android/app/easylauncher/ContactGridFragment;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .line 22
    new-array v0, v2, [Lcom/sec/android/app/easylauncher/AppFragment;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_CONTACTS1:I

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_CONTACTS2:I

    .line 32
    iput v1, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_HOME:I

    .line 33
    iput v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_APPS1:I

    .line 34
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_APPS2:I

    .line 35
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_APPS3:I

    .line 37
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_HELP_APPS:I

    .line 38
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->PAGE_HELP_CONTACT:I

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->HELP_APPS_POSITION:I

    .line 41
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->HELP_CONTACT_POSITION:I

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    .line 52
    return-void
.end method

.method private getFragmentTag(I)Ljava/lang/String;
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 55
    const-string v0, ""

    .line 56
    .local v0, "tag":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/easylauncher/PageAdapter;->fragmentTags:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 57
    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 258
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->getPageCount()I

    move-result v0

    .line 260
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 261
    const/4 v0, 0x1

    .line 267
    .end local v0    # "count":I
    :cond_0
    :goto_0
    return v0

    .line 264
    .restart local v0    # "count":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    iget-boolean v1, v1, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public getEditIndex(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 327
    const/4 v0, 0x0

    .line 328
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v2

    .line 330
    .local v2, "onoff":[Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x6

    if-ge v1, v3, :cond_2

    .line 331
    aget-boolean v3, v2, v1

    if-eqz v3, :cond_1

    .line 332
    if-ne v0, p1, :cond_0

    .line 339
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 335
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 330
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 339
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getIndex(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 306
    const/4 v0, 0x0

    .line 307
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v2

    .line 309
    .local v2, "onoff":[Z
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    iget-boolean v3, v3, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    .end local p1    # "position":I
    :goto_0
    return p1

    .line 313
    .restart local p1    # "position":I
    :cond_0
    aget-boolean v3, v2, p1

    if-nez v3, :cond_1

    .line 314
    const/4 p1, -0x1

    goto :goto_0

    .line 317
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p1, :cond_3

    .line 318
    aget-boolean v3, v2, v1

    if-eqz v3, :cond_2

    .line 319
    add-int/lit8 v0, v0, 0x1

    .line 317
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move p1, v0

    .line 323
    goto :goto_0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getPosition(I)I

    move-result v0

    .line 143
    .local v0, "currPosition":I
    const-string v2, "PageAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getItem position: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " currPosition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    move v0, p1

    .line 148
    packed-switch p1, :pswitch_data_0

    .line 177
    :pswitch_0
    add-int/lit8 v1, v0, -0x3

    .line 179
    .local v1, "index":I
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    aget-object v2, v2, v1

    if-nez v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    new-instance v3, Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-direct {v3}, Lcom/sec/android/app/easylauncher/AppFragment;-><init>()V

    aput-object v3, v2, v1

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/sec/android/app/easylauncher/AppFragment;->setScreenPosion(I)V

    .line 183
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    aget-object v2, v2, v1

    .end local v1    # "index":I
    :goto_0
    return-object v2

    .line 151
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    new-instance v3, Lcom/sec/android/app/easylauncher/ContactGridFragment;

    invoke-direct {v3}, Lcom/sec/android/app/easylauncher/ContactGridFragment;-><init>()V

    aput-object v3, v2, v0

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    aget-object v2, v2, v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->setScreenPosion(I)V

    .line 155
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    aget-object v2, v2, v0

    goto :goto_0

    .line 158
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-nez v2, :cond_2

    .line 159
    new-instance v2, Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {v2}, Lcom/sec/android/app/easylauncher/MainFragment;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    .line 160
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    goto :goto_0

    .line 163
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    if-nez v2, :cond_3

    .line 164
    new-instance v2, Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-direct {v2}, Lcom/sec/android/app/easylauncher/AppFragment;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/AppFragment;->setScreenPosion(I)V

    .line 167
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    goto :goto_0

    .line 170
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    if-nez v2, :cond_4

    .line 171
    new-instance v2, Lcom/sec/android/app/easylauncher/ContactGridFragment;

    invoke-direct {v2}, Lcom/sec/android/app/easylauncher/ContactGridFragment;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    const/4 v3, -0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->setScreenPosion(I)V

    .line 174
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getItemPosition(Ljava/lang/Object;I)I
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "position"    # I

    .prologue
    .line 232
    const/4 v0, 0x0

    .line 234
    .local v0, "currPosition":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 236
    const-string v1, "PageAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getItemPosition: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " currPosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    if-ne v0, p2, :cond_0

    .line 240
    const/4 v0, -0x1

    .line 242
    .end local v0    # "currPosition":I
    :cond_0
    return v0
.end method

.method public getMainHomeIndex()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    .line 289
    const/4 v0, 0x0

    .line 290
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v2

    .line 292
    .local v2, "onoff":[Z
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    iget-boolean v4, v4, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 302
    :goto_0
    return v3

    .line 296
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 297
    aget-boolean v4, v2, v1

    if-eqz v4, :cond_1

    .line 298
    add-int/lit8 v0, v0, 0x1

    .line 296
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v3, v0

    .line 302
    goto :goto_0
.end method

.method getPosition(I)I
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x6

    .line 61
    const/4 v0, 0x0

    .line 62
    .local v0, "count":I
    const/4 v1, 0x0

    .line 63
    .local v1, "currPosition":I
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v3

    .line 65
    .local v3, "onoff":[Z
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 66
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    iget v5, v5, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 67
    const/4 p1, 0x7

    .line 87
    .end local p1    # "position":I
    :cond_0
    :goto_0
    return p1

    .restart local p1    # "position":I
    :cond_1
    move p1, v4

    .line 69
    goto :goto_0

    .line 73
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    iget-boolean v5, v5, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v5

    if-nez v5, :cond_0

    .line 77
    :cond_3
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_4

    .line 78
    aget-boolean v5, v3, v2

    if-eqz v5, :cond_6

    .line 79
    if-ne v0, p1, :cond_5

    .line 80
    move v1, v2

    :cond_4
    move p1, v1

    .line 87
    goto :goto_0

    .line 83
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 77
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method getPosition(Ljava/lang/Object;)I
    .locals 8
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x2

    .line 189
    const/4 v0, 0x0

    .line 190
    .local v0, "count":I
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v3

    .line 192
    .local v3, "onoff":[Z
    const/4 v4, 0x0

    .line 194
    .local v4, "pageShow":Z
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 195
    add-int/lit8 v0, v0, 0x1

    .line 198
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    iget-boolean v5, v5, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 199
    const/4 v4, 0x1

    .line 202
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_5

    .line 203
    aget-boolean v5, v3, v2

    if-nez v5, :cond_2

    if-eqz v4, :cond_4

    .line 204
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    add-int/lit8 v6, v2, 0x0

    aget-object v5, v5, v6

    if-ne p1, v5, :cond_3

    move v1, v0

    .line 227
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_1
    return v1

    .line 207
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 202
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 211
    :cond_5
    aget-boolean v5, v3, v7

    if-nez v5, :cond_6

    if-eqz v4, :cond_8

    .line 212
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-ne p1, v5, :cond_7

    move v1, v0

    .line 213
    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_1

    .line 215
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_7
    add-int/lit8 v0, v0, 0x1

    .line 218
    :cond_8
    const/4 v2, 0x3

    :goto_2
    const/4 v5, 0x6

    if-ge v2, v5, :cond_c

    .line 219
    aget-boolean v5, v3, v2

    if-nez v5, :cond_9

    if-eqz v4, :cond_b

    .line 220
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    add-int/lit8 v6, v2, -0x3

    aget-object v5, v5, v6

    if-ne p1, v5, :cond_a

    move v1, v0

    .line 221
    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_1

    .line 223
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_a
    add-int/lit8 v0, v0, 0x1

    .line 218
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_c
    move v1, v0

    .line 227
    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 92
    invoke-virtual {p0, p2}, Lcom/sec/android/app/easylauncher/PageAdapter;->getPosition(I)I

    move-result v0

    .line 95
    .local v0, "currPosition":I
    invoke-super {p0, p1, v0}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 98
    .local v1, "f":Landroid/support/v4/app/Fragment;
    sget-object v3, Lcom/sec/android/app/easylauncher/PageAdapter;->fragmentTags:[Ljava/lang/String;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 100
    const-string v3, "PageAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "instantiateItem position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " fragmentTags[position]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/easylauncher/PageAdapter;->fragmentTags:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const-string v3, "PageAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "instantiateItem  currPosition: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    packed-switch v0, :pswitch_data_0

    .line 128
    :pswitch_0
    add-int/lit8 v2, v0, -0x3

    .line 130
    .local v2, "index":I
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    aget-object v3, v3, v2

    if-nez v3, :cond_0

    .line 131
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    move-object v3, v1

    check-cast v3, Lcom/sec/android/app/easylauncher/AppFragment;

    aput-object v3, v4, v2

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0}, Lcom/sec/android/app/easylauncher/AppFragment;->setScreenPosion(I)V

    .line 137
    .end local v2    # "index":I
    :cond_0
    :goto_0
    return-object v1

    .line 108
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    aget-object v3, v3, v0

    if-nez v3, :cond_0

    .line 109
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    move-object v3, v1

    check-cast v3, Lcom/sec/android/app/easylauncher/ContactGridFragment;

    aput-object v3, v4, v0

    goto :goto_0

    .line 113
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-nez v3, :cond_0

    move-object v3, v1

    .line 114
    check-cast v3, Lcom/sec/android/app/easylauncher/MainFragment;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    goto :goto_0

    .line 118
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    if-nez v3, :cond_0

    move-object v3, v1

    .line 119
    check-cast v3, Lcom/sec/android/app/easylauncher/AppFragment;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    goto :goto_0

    .line 123
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    if-nez v3, :cond_0

    move-object v3, v1

    .line 124
    check-cast v3, Lcom/sec/android/app/easylauncher/ContactGridFragment;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpContactFragment:Lcom/sec/android/app/easylauncher/ContactGridFragment;

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public isScreenShowing(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v0

    .line 346
    .local v0, "onoff":[Z
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mActivity:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 354
    :cond_0
    :goto_0
    return v1

    .line 350
    :cond_1
    aget-boolean v2, v0, p1

    if-nez v2, :cond_0

    .line 354
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public notifyRefreshFragment()V
    .locals 5

    .prologue
    .line 272
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-eqz v4, :cond_0

    .line 273
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/MainFragment;->refreshView()V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v2, v0, v1

    .line 277
    .local v2, "item":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    if-eqz v2, :cond_1

    .line 278
    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->refreshView()V

    .line 276
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 281
    .end local v2    # "item":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/AppFragment;
    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v2, v0, v1

    .line 282
    .local v2, "item":Lcom/sec/android/app/easylauncher/AppFragment;
    if-eqz v2, :cond_3

    .line 283
    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AppFragment;->refreshView()V

    .line 281
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 286
    .end local v2    # "item":Lcom/sec/android/app/easylauncher/AppFragment;
    :cond_4
    return-void
.end method

.method public unBind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 248
    const-string v0, "PageAdapter"

    const-string v1, "----------unBind: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    .line 250
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .line 251
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .line 252
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    .line 253
    return-void
.end method
