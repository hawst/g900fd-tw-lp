.class Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;
.super Ljava/lang/Object;
.source "HelpAnimatedDialog.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;->this$0:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 121
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 122
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 123
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;->this$0:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    # getter for: Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouchListener:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->access$400(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;->this$0:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    # getter for: Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouchListener:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->access$400(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;->onTouch()V

    .line 129
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 130
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 133
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 134
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 136
    :cond_2
    const/4 v0, 0x0

    return v0
.end method
