.class Lcom/sec/android/app/easylauncher/MainFragment$5;
.super Landroid/os/Handler;
.source "MainFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/MainFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$5;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 291
    # getter for: Lcom/sec/android/app/easylauncher/MainFragment;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/MainFragment;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "EasyLauncher.MainFragment"

    const-string v1, " mAppsloaderHandler message recieved now invalidating the view"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$5;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$5;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$5;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_2

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$5;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidate()V

    .line 297
    :cond_2
    return-void
.end method
