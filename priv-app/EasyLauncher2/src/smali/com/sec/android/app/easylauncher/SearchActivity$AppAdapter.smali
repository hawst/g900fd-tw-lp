.class Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppAdapter"
.end annotation


# instance fields
.field mCategory:I

.field mContext:Landroid/content/Context;

.field mFilter:Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;

.field mSearchText:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

.field toShow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/easylauncher/SearchActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 259
    new-instance v0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;

    invoke-direct {v0, p1}, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;-><init>(Lcom/sec/android/app/easylauncher/SearchActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mFilter:Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;

    .line 260
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mContext:Landroid/content/Context;

    .line 261
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    .line 262
    return-void
.end method

.method private displayHighlightedName(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 6
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "appName"    # Ljava/lang/String;

    .prologue
    .line 273
    const/4 v2, 0x0

    .line 274
    .local v2, "startIndex":I
    const/4 v0, 0x0

    .line 276
    .local v0, "endIndex":I
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 277
    .local v1, "highLightText":Landroid/text/Spannable;
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 278
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int v0, v2, v3

    .line 281
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 282
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->isIndianMatra(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_0
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-interface {v1, v3, v2, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 292
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 359
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 365
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parnet"    # Landroid/view/ViewGroup;

    .prologue
    .line 336
    if-nez p2, :cond_0

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/SearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 338
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030008

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 341
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v4, 0x7f0c0026

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 342
    .local v3, "title":Landroid/widget/TextView;
    const v4, 0x7f0c0024

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 343
    .local v1, "icon":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 345
    .local v0, "appInfo":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 346
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 353
    return-object p2

    .line 348
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->displayHighlightedName(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isIndianMatra(C)Z
    .locals 2
    .param p1, "c"    # C

    .prologue
    const/4 v0, 0x1

    .line 301
    const/16 v1, 0x900

    if-lt p1, v1, :cond_0

    const/16 v1, 0x903

    if-le p1, v1, :cond_2

    :cond_0
    const/16 v1, 0x93a

    if-lt p1, v1, :cond_1

    const/16 v1, 0x94f

    if-le p1, v1, :cond_2

    :cond_1
    const/16 v1, 0x951

    if-lt p1, v1, :cond_3

    const/16 v1, 0x957

    if-gt p1, v1, :cond_3

    .line 328
    :cond_2
    :goto_0
    return v0

    .line 304
    :cond_3
    const/16 v1, 0x981

    if-lt p1, v1, :cond_4

    const/16 v1, 0x983

    if-le p1, v1, :cond_2

    :cond_4
    const/16 v1, 0x9bc

    if-lt p1, v1, :cond_5

    const/16 v1, 0x9d7

    if-le p1, v1, :cond_2

    :cond_5
    const/16 v1, 0x9e2

    if-lt p1, v1, :cond_6

    const/16 v1, 0x9e3

    if-le p1, v1, :cond_2

    .line 307
    :cond_6
    const/16 v1, 0xb82

    if-lt p1, v1, :cond_7

    const/16 v1, 0xb83

    if-le p1, v1, :cond_2

    :cond_7
    const/16 v1, 0xbbe

    if-lt p1, v1, :cond_8

    const/16 v1, 0xbcd

    if-le p1, v1, :cond_2

    .line 310
    :cond_8
    const/16 v1, 0xc01

    if-lt p1, v1, :cond_9

    const/16 v1, 0xc03

    if-le p1, v1, :cond_2

    :cond_9
    const/16 v1, 0xc3e

    if-lt p1, v1, :cond_a

    const/16 v1, 0xc56

    if-le p1, v1, :cond_2

    :cond_a
    const/16 v1, 0xc62

    if-lt p1, v1, :cond_b

    const/16 v1, 0xc63

    if-le p1, v1, :cond_2

    .line 313
    :cond_b
    const/16 v1, 0xc82

    if-lt p1, v1, :cond_c

    const/16 v1, 0xc83

    if-le p1, v1, :cond_2

    :cond_c
    const/16 v1, 0xcbc

    if-lt p1, v1, :cond_d

    const/16 v1, 0xcd6

    if-le p1, v1, :cond_2

    :cond_d
    const/16 v1, 0xce2

    if-lt p1, v1, :cond_e

    const/16 v1, 0xce3

    if-le p1, v1, :cond_2

    .line 316
    :cond_e
    const/16 v1, 0xa01

    if-lt p1, v1, :cond_f

    const/16 v1, 0xa03

    if-le p1, v1, :cond_2

    :cond_f
    const/16 v1, 0xa3c

    if-lt p1, v1, :cond_10

    const/16 v1, 0xa51

    if-le p1, v1, :cond_2

    :cond_10
    const/16 v1, 0xa70

    if-lt p1, v1, :cond_11

    const/16 v1, 0xa71

    if-le p1, v1, :cond_2

    .line 319
    :cond_11
    const/16 v1, 0xa81

    if-lt p1, v1, :cond_12

    const/16 v1, 0xa83

    if-le p1, v1, :cond_2

    :cond_12
    const/16 v1, 0xabc

    if-lt p1, v1, :cond_13

    const/16 v1, 0xacd

    if-le p1, v1, :cond_2

    :cond_13
    const/16 v1, 0xae2

    if-lt p1, v1, :cond_14

    const/16 v1, 0xae3

    if-le p1, v1, :cond_2

    .line 322
    :cond_14
    const/16 v1, 0xd02

    if-lt p1, v1, :cond_15

    const/16 v1, 0xd03

    if-le p1, v1, :cond_2

    :cond_15
    const/16 v1, 0xd3e

    if-lt p1, v1, :cond_16

    const/16 v1, 0xd57

    if-le p1, v1, :cond_2

    :cond_16
    const/16 v1, 0xd62

    if-lt p1, v1, :cond_17

    const/16 v1, 0xd63

    if-le p1, v1, :cond_2

    .line 325
    :cond_17
    const/16 v1, 0xd82

    if-lt p1, v1, :cond_18

    const/16 v1, 0xd83

    if-le p1, v1, :cond_2

    :cond_18
    const/16 v1, 0xdca

    if-lt p1, v1, :cond_19

    const/16 v1, 0xdf3

    if-le p1, v1, :cond_2

    .line 328
    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public runFilter()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mFilter:Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->filterWidgets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->notifyDataSetChanged()V

    .line 298
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 0
    .param p1, "search"    # Ljava/lang/String;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    .line 270
    return-void
.end method

.method unBind()V
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mFilter:Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;

    .line 370
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 373
    :cond_0
    return-void
.end method
