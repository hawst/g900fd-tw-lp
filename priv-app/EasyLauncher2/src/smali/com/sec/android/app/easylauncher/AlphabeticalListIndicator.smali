.class public Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;
.super Landroid/view/View;
.source "AlphabeticalListIndicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$1;,
        Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;,
        Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;,
        Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$StateChanger;,
        Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;
    }
.end annotation


# static fields
.field private static final GRP_SIZE:I = 0x3

.field public static final INDEX_CHANGED:I = 0x1

.field public static final INDEX_CHANGED_AND_PREMATCH:I = 0x2

.field public static final INDEX_INVALID:I = 0x5

.field public static final INDEX_NOT_PROCESSED:I = 0x4

.field public static final INDEX_NO_CHANGE:I = 0x3

.field private static final INVALID_WORKING_STR:I = -0x1

.field private static final PT_ABOVE_FIRST_CHAR:I = -0x2

.field private static final PT_BELOW_LAST_CHAR:I = -0x3


# instance fields
.field private mAllowWiggle:Z

.field private mAlphaAnim:Lcom/sec/android/app/easylauncher/ScalarAnimator;

.field private mBgFullHeight:Z

.field private mCellHeight:F

.field private mCellSepLineMarginLeft:F

.field private mCellSepLineMarginRight:F

.field private mCellSepLinePaint:Landroid/graphics/Paint;

.field private mCellSepLineThick:F

.field private mCurrentIndex:I

.field private mDrawBounds:Landroid/graphics/RectF;

.field private mFirstCharPreMatches:Z

.field private mIndexStr:Ljava/lang/String;

.field private mIsFloating:Z

.field private mNormalBg:Landroid/graphics/drawable/Drawable;

.field private mOverflowGrpCharMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;",
            ">;"
        }
    .end annotation
.end field

.field private mOverflowGrpSepChar:C

.field private mOverflowGrpSepCharBounds:Landroid/graphics/Rect;

.field private mPressDelay:I

.field private mSelBg:Landroid/graphics/drawable/Drawable;

.field private mSelItemBg:Landroid/graphics/drawable/Drawable;

.field private mSelTextColor:I

.field private mTextColor:I

.field private mTextMetrics:Landroid/graphics/Paint$FontMetrics;

.field private mTextPaint:Landroid/text/TextPaint;

.field private mTextUppercase:Z

.field private mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

.field private mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

.field private mUnpressDelay:I

.field private mWorkingStr:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 93
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 49
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellHeight:F

    .line 51
    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIsFloating:Z

    .line 53
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    .line 57
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mPressDelay:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mUnpressDelay:I

    .line 62
    const/16 v0, 0x2e

    iput-char v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextUppercase:Z

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAllowWiggle:Z

    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mBgFullHeight:Z

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepCharBounds:Landroid/graphics/Rect;

    .line 69
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    .line 71
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    .line 73
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    .line 75
    new-instance v0, Lcom/sec/android/app/easylauncher/ScalarAnimator;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/ScalarAnimator;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAlphaAnim:Lcom/sec/android/app/easylauncher/ScalarAnimator;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    .line 81
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLinePaint:Landroid/graphics/Paint;

    .line 83
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    iput v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginLeft:F

    iput v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginRight:F

    .line 90
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 97
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    const/high16 v4, -0x40800000    # -1.0f

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellHeight:F

    .line 51
    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIsFloating:Z

    .line 53
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    .line 55
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    .line 57
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4}, Landroid/text/TextPaint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    .line 59
    iput v9, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    const/16 v4, 0xc8

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mPressDelay:I

    const/16 v4, 0x3e8

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mUnpressDelay:I

    .line 62
    const/16 v4, 0x2e

    iput-char v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    .line 64
    iput-boolean v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    iput-boolean v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextUppercase:Z

    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAllowWiggle:Z

    iput-boolean v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mBgFullHeight:Z

    .line 67
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepCharBounds:Landroid/graphics/Rect;

    .line 69
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    .line 71
    new-instance v4, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    invoke-direct {v4, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;)V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    .line 73
    new-instance v4, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    invoke-direct {v4, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;)V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    .line 75
    new-instance v4, Lcom/sec/android/app/easylauncher/ScalarAnimator;

    invoke-direct {v4}, Lcom/sec/android/app/easylauncher/ScalarAnimator;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAlphaAnim:Lcom/sec/android/app/easylauncher/ScalarAnimator;

    .line 77
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    .line 79
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    .line 81
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLinePaint:Landroid/graphics/Paint;

    .line 83
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    iput v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginLeft:F

    iput v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginRight:F

    .line 90
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    .line 99
    sget-object v4, Lcom/sec/android/app/easylauncher/R$styleable;->AlphabeticalListIndicator:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 101
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v4, 0xc8

    invoke-virtual {v0, v7, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mPressDelay:I

    .line 102
    const/16 v4, 0x3e8

    invoke-virtual {v0, v8, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mUnpressDelay:I

    .line 103
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    .line 105
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextUppercase:Z

    .line 106
    const/16 v4, 0x14

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mBgFullHeight:Z

    .line 107
    const/4 v4, 0x5

    const/high16 v5, -0x40800000    # -1.0f

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellHeight:F

    .line 108
    const/16 v4, 0x13

    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIsFloating:Z

    .line 109
    const/16 v4, 0xb

    invoke-virtual {v0, v4, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 111
    .local v1, "cellSepLineColor":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    .line 114
    const/16 v4, 0xd

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginLeft:F

    .line 116
    const/16 v4, 0xe

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginRight:F

    .line 118
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "temp":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 120
    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iput-char v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    .line 122
    :cond_0
    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    .line 123
    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    .line 124
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    .line 125
    const/16 v4, 0x9

    invoke-virtual {v0, v4, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextColor:I

    .line 126
    const/16 v4, 0xa

    invoke-virtual {v0, v4, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelTextColor:I

    .line 136
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v8}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    const/16 v5, 0x12

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    .line 142
    new-array v2, v8, [C

    .line 143
    .local v2, "ch":[C
    iget-char v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    aput-char v4, v2, v7

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    array-length v5, v2

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepCharBounds:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v7, v5, v6}, Landroid/text/TextPaint;->getTextBounds([CIILandroid/graphics/Rect;)V

    .line 145
    const/16 v4, 0x11

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    .line 147
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 148
    return-void
.end method

.method private addGroupRange(Ljava/lang/String;Ljava/lang/StringBuffer;II)Z
    .locals 2
    .param p1, "aIndexStr"    # Ljava/lang/String;
    .param p2, "aWorkingStr"    # Ljava/lang/StringBuffer;
    .param p3, "aStartIndex"    # I
    .param p4, "aEndIndex"    # I

    .prologue
    .line 393
    move v0, p3

    .local v0, "i":I
    :goto_0
    if-gt v0, p4, :cond_0

    .line 394
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 393
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 396
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method private addOverflowGroupRange(Ljava/lang/String;Ljava/lang/StringBuffer;II)Z
    .locals 3
    .param p1, "aIndexStr"    # Ljava/lang/String;
    .param p2, "aWorkingStr"    # Ljava/lang/StringBuffer;
    .param p3, "aStartIndex"    # I
    .param p4, "aEndIndex"    # I

    .prologue
    .line 375
    invoke-virtual {p1, p3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 376
    iget-char v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 377
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 378
    .local v1, "workingIndex":I
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;

    .line 379
    .local v0, "range":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;
    if-nez v0, :cond_0

    .line 380
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;

    .end local v0    # "range":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;
    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$1;)V

    .line 381
    .restart local v0    # "range":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 383
    :cond_0
    add-int/lit8 v2, p3, 0x1

    iput v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mStartIndex:I

    .line 384
    add-int/lit8 v2, p4, -0x1

    iput v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mEndIndex:I

    .line 385
    const/4 v2, -0x1

    iput v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mCurrentIndex:I

    .line 387
    invoke-virtual {p1, p4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 388
    const/4 v2, 0x1

    return v2
.end method

.method private compute(Ljava/lang/String;Ljava/lang/StringBuffer;Landroid/graphics/RectF;Landroid/graphics/PointF;Z)Z
    .locals 7
    .param p1, "aIndexStr"    # Ljava/lang/String;
    .param p2, "aWorkingStr"    # Ljava/lang/StringBuffer;
    .param p3, "aDrawBounds"    # Landroid/graphics/RectF;
    .param p4, "aTouchPt"    # Landroid/graphics/PointF;
    .param p5, "aAllowWiggle"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCharsWouldFit(F)I

    move-result v2

    .line 542
    .local v2, "charsWouldFit":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_0

    if-eqz p5, :cond_0

    .line 546
    add-int/lit8 v2, v2, -0x1

    .line 548
    :cond_0
    if-ge v2, v5, :cond_2

    .line 571
    :cond_1
    :goto_0
    return v4

    .line 551
    :cond_2
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->ensureCapacity(I)V

    .line 552
    invoke-direct {p0, p1, p2, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->computeWorkingStr(Ljava/lang/String;Ljava/lang/StringBuffer;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v0

    .line 556
    .local v0, "cellHt":F
    const/high16 v6, 0x40000000    # 2.0f

    div-float v1, v0, v6

    .line 560
    .local v1, "cellHtHalf":F
    iget v6, p4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v1

    iput v6, p4, Landroid/graphics/PointF;->y:F

    .line 561
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    invoke-direct {p0, v6, v2, p3, p4}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->computeBounds(IILandroid/graphics/RectF;Landroid/graphics/PointF;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 565
    invoke-direct {p0, p3, p4, p2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellIndexForPt(Landroid/graphics/RectF;Landroid/graphics/PointF;Ljava/lang/StringBuffer;)I

    move-result v3

    .line 566
    .local v3, "touchIndex":I
    if-eqz p5, :cond_3

    invoke-direct {p0, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isCellIndexValid(I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    iget-char v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    if-ne v4, v6, :cond_3

    .line 568
    iget v4, p3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v0

    iput v4, p3, Landroid/graphics/RectF;->top:F

    .line 569
    iget v4, p3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v0

    iput v4, p3, Landroid/graphics/RectF;->bottom:F

    :cond_3
    move v4, v5

    .line 571
    goto :goto_0
.end method

.method private computeBounds(IILandroid/graphics/RectF;Landroid/graphics/PointF;)Z
    .locals 13
    .param p1, "aLength"    # I
    .param p2, "aCapacity"    # I
    .param p3, "aBounds"    # Landroid/graphics/RectF;
    .param p4, "aTouchPt"    # Landroid/graphics/PointF;

    .prologue
    .line 484
    new-instance v7, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingLeft()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingRight()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingBottom()I

    move-result v12

    int-to-float v12, v12

    invoke-direct {v7, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 486
    .local v7, "padding":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getHeight()I

    move-result v9

    int-to-float v5, v9

    .line 487
    .local v5, "ht":F
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v2

    .line 488
    .local v2, "cellHt":F
    int-to-float v9, p1

    mul-float v4, v9, v2

    .line 489
    .local v4, "fitHt":F
    iget v9, v7, Landroid/graphics/RectF;->bottom:F

    iget v10, v7, Landroid/graphics/RectF;->top:F

    add-float v6, v9, v10

    .line 490
    .local v6, "padHt":F
    sub-float v1, v5, v6

    .line 492
    .local v1, "availHt":F
    iget v9, v7, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->left:F

    .line 493
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getWidth()I

    move-result v9

    int-to-float v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    sub-float/2addr v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->right:F

    .line 494
    iget-boolean v9, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mBgFullHeight:Z

    if-eqz v9, :cond_0

    .line 495
    const/4 v9, 0x0

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->top:F

    .line 496
    move-object/from16 v0, p3

    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 497
    const/4 v9, 0x1

    .line 523
    :goto_0
    return v9

    .line 499
    :cond_0
    move-object/from16 v0, p4

    iget v9, v0, Landroid/graphics/PointF;->y:F

    iget v10, v7, Landroid/graphics/RectF;->top:F

    cmpg-float v9, v9, v10

    if-gez v9, :cond_1

    .line 500
    const/4 v9, 0x0

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->top:F

    .line 501
    move-object/from16 v0, p3

    iget v9, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v6

    add-float/2addr v9, v4

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->bottom:F

    .line 502
    const/4 v9, 0x1

    goto :goto_0

    .line 504
    :cond_1
    iget v9, v7, Landroid/graphics/RectF;->top:F

    add-float v8, v9, v1

    .line 505
    .local v8, "unpaddedBottom":F
    move-object/from16 v0, p4

    iget v9, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v9, v9, v8

    if-lez v9, :cond_2

    .line 506
    move-object/from16 v0, p3

    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 507
    move-object/from16 v0, p3

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    add-float v10, v4, v6

    sub-float/2addr v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->top:F

    .line 508
    const/4 v9, 0x1

    goto :goto_0

    .line 510
    :cond_2
    move-object/from16 v0, p4

    iget v9, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v9}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCharsWouldFit(F)I

    move-result v3

    .line 511
    .local v3, "charsWouldFitFromY":I
    if-gt p1, v3, :cond_3

    .line 512
    move-object/from16 v0, p4

    iget v9, v0, Landroid/graphics/PointF;->y:F

    iget v10, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->top:F

    .line 513
    move-object/from16 v0, p3

    iget v9, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v6

    add-float/2addr v9, v4

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->bottom:F

    .line 514
    const/4 v9, 0x1

    goto :goto_0

    .line 516
    :cond_3
    if-lez v3, :cond_4

    .line 517
    move-object/from16 v0, p4

    iget v9, v0, Landroid/graphics/PointF;->y:F

    int-to-float v10, v3

    mul-float/2addr v10, v2

    add-float/2addr v9, v10

    iget v10, v7, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->bottom:F

    .line 518
    move-object/from16 v0, p3

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    add-float v10, v4, v6

    sub-float/2addr v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->top:F

    .line 519
    const/4 v9, 0x1

    goto :goto_0

    .line 521
    :cond_4
    move-object/from16 v0, p3

    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 522
    move-object/from16 v0, p3

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    add-float v10, v4, v6

    sub-float/2addr v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/RectF;->top:F

    .line 523
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method private computeWorkingStr(Ljava/lang/String;Ljava/lang/StringBuffer;I)Z
    .locals 12
    .param p1, "aIndexStr"    # Ljava/lang/String;
    .param p2, "aWorkingStr"    # Ljava/lang/StringBuffer;
    .param p3, "aCapacity"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 405
    iget-object v11, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    invoke-virtual {v11}, Landroid/util/SparseArray;->clear()V

    .line 406
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 407
    .local v4, "len":I
    move v7, p3

    .line 408
    .local v7, "numWouldFit":I
    if-gt v4, v7, :cond_0

    .line 409
    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 445
    :goto_0
    return v9

    .line 412
    :cond_0
    const/4 v5, 0x3

    .line 413
    .local v5, "minNeeded":I
    iget-boolean v11, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    if-eqz v11, :cond_1

    .line 414
    add-int/lit8 v5, v5, 0x1

    .line 416
    :cond_1
    if-ge v7, v5, :cond_2

    move v9, v10

    .line 417
    goto :goto_0

    .line 419
    :cond_2
    const/4 v8, 0x0

    .line 420
    .local v8, "startIndex":I
    iget-boolean v11, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    if-eqz v11, :cond_3

    .line 421
    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 422
    add-int/lit8 v8, v8, 0x1

    .line 423
    add-int/lit8 v7, v7, -0x1

    .line 425
    :cond_3
    int-to-float v10, v7

    const/high16 v11, 0x40400000    # 3.0f

    div-float/2addr v10, v11

    float-to-int v6, v10

    .line 426
    .local v6, "numPossibleGrps":I
    sub-int v10, v4, v8

    int-to-float v10, v10

    int-to-float v11, v6

    div-float/2addr v10, v11

    float-to-int v0, v10

    .line 431
    .local v0, "charsPerGrp":I
    add-int/lit8 v2, v6, -0x2

    .local v2, "i":I
    move v3, v8

    .local v3, "index":I
    :goto_1
    if-ltz v2, :cond_5

    .line 432
    add-int/lit8 v10, v0, -0x1

    add-int v1, v3, v10

    .line 433
    .local v1, "endIndex":I
    const/4 v10, 0x3

    if-ne v10, v0, :cond_4

    .line 434
    invoke-direct {p0, p1, p2, v3, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->addGroupRange(Ljava/lang/String;Ljava/lang/StringBuffer;II)Z

    .line 438
    :goto_2
    add-int/lit8 v3, v1, 0x1

    .line 431
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 436
    :cond_4
    invoke-direct {p0, p1, p2, v3, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->addOverflowGroupRange(Ljava/lang/String;Ljava/lang/StringBuffer;II)Z

    goto :goto_2

    .line 443
    .end local v1    # "endIndex":I
    :cond_5
    add-int/lit8 v1, v4, -0x1

    .line 444
    .restart local v1    # "endIndex":I
    invoke-direct {p0, p1, p2, v3, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->addOverflowGroupRange(Ljava/lang/String;Ljava/lang/StringBuffer;II)Z

    goto :goto_0
.end method

.method private drawChar(Landroid/graphics/Canvas;Landroid/graphics/Rect;FCFFFZ)V
    .locals 16
    .param p1, "aCanvas"    # Landroid/graphics/Canvas;
    .param p2, "aPadding"    # Landroid/graphics/Rect;
    .param p3, "aWidth"    # F
    .param p4, "aCh"    # C
    .param p5, "aPosX"    # F
    .param p6, "aPosY"    # F
    .param p7, "aCellHt"    # F
    .param p8, "aDrawSepLine"    # Z

    .prologue
    .line 289
    const/4 v1, 0x1

    new-array v2, v1, [C

    .line 290
    .local v2, "ch":[C
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v1, v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float v13, v1, v3

    .line 291
    .local v13, "aTxtHt":F
    sub-float v1, p7, v13

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    sub-float/2addr v1, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v15, v1, v3

    .line 293
    .local v15, "bottomGap":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float/2addr v1, v15

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    add-float/2addr v1, v3

    sub-float v6, p6, v1

    .line 294
    .local v6, "drawY":F
    const/4 v1, 0x0

    aput-char p4, v2, v1

    .line 295
    const/4 v1, 0x0

    aget-char v1, v2, v1

    move-object/from16 v0, p0

    iget-char v3, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    if-ne v1, v3, :cond_2

    .line 296
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepCharBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v14

    .line 297
    .local v14, "bndsHt":I
    int-to-float v1, v14

    sub-float/2addr v6, v1

    .line 303
    .end local v14    # "bndsHt":I
    :cond_0
    :goto_0
    const/4 v3, 0x0

    array-length v4, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    move/from16 v5, p5

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    .line 304
    if-eqz p8, :cond_1

    .line 305
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginLeft:F

    add-float v8, v1, v3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    sub-float v9, p6, v1

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    add-float v1, v1, p3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginRight:F

    sub-float v10, v1, v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLinePaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    move/from16 v11, p6

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 308
    :cond_1
    return-void

    .line 299
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextUppercase:Z

    if-eqz v1, :cond_0

    .line 300
    const/4 v1, 0x0

    const/4 v3, 0x0

    aget-char v3, v2, v3

    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v3

    aput-char v3, v2, v1

    goto :goto_0
.end method

.method private getCellIndexForPt(Landroid/graphics/RectF;Landroid/graphics/PointF;Ljava/lang/StringBuffer;)I
    .locals 6
    .param p1, "aBounds"    # Landroid/graphics/RectF;
    .param p2, "aPt"    # Landroid/graphics/PointF;
    .param p3, "aWorkingStr"    # Ljava/lang/StringBuffer;

    .prologue
    .line 463
    invoke-virtual {p3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 464
    .local v1, "len":I
    const/4 v4, 0x1

    if-ge v1, v4, :cond_1

    .line 465
    const/4 v0, -0x1

    .line 476
    :cond_0
    :goto_0
    return v0

    .line 467
    :cond_1
    iget v4, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    add-float v3, v4, v5

    .line 468
    .local v3, "top":F
    iget v4, p2, Landroid/graphics/PointF;->y:F

    sub-float v2, v4, v3

    .line 469
    .local v2, "relY":F
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v4

    div-float v4, v2, v4

    float-to-int v0, v4

    .line 470
    .local v0, "index":I
    if-gez v0, :cond_2

    .line 471
    const/4 v0, -0x2

    goto :goto_0

    .line 473
    :cond_2
    if-lt v0, v1, :cond_0

    .line 474
    const/4 v0, -0x3

    goto :goto_0
.end method

.method private getCharsWouldFit(F)I
    .locals 5
    .param p1, "aYOffset"    # F

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, p1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingBottom()I

    move-result v4

    int-to-float v4, v4

    sub-float v0, v3, v4

    .line 528
    .local v0, "availHt":F
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v1

    .line 529
    .local v1, "cellHt":F
    div-float v3, v0, v1

    float-to-int v2, v3

    .line 530
    .local v2, "charsWouldFit":I
    return v2
.end method

.method private isCellIndexValid(I)Z
    .locals 1
    .param p1, "aChar"    # I

    .prologue
    .line 480
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tryUpdateCellIndexForOverflowGrp(ILandroid/graphics/RectF;Landroid/graphics/PointF;)Z
    .locals 14
    .param p1, "aCurrentIndex"    # I
    .param p2, "aBounds"    # Landroid/graphics/RectF;
    .param p3, "aPt"    # Landroid/graphics/PointF;

    .prologue
    .line 205
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-virtual {v12, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    .line 206
    .local v3, "ch":C
    iget-char v12, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    if-eq v3, v12, :cond_0

    .line 207
    const/4 v12, 0x0

    .line 229
    :goto_0
    return v12

    .line 209
    :cond_0
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    invoke-virtual {v12, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;

    .line 210
    .local v7, "range":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;
    if-eqz v7, :cond_1

    iget v12, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mEndIndex:I

    iget v13, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mStartIndex:I

    if-gt v12, v13, :cond_2

    .line 211
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 213
    :cond_2
    iget v12, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mEndIndex:I

    iget v13, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mStartIndex:I

    sub-int/2addr v12, v13

    add-int/lit8 v9, v12, 0x1

    .line 214
    .local v9, "rangeLen":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v1

    .line 215
    .local v1, "cellHt":F
    int-to-float v12, v9

    div-float v6, v1, v12

    .line 216
    .local v6, "pxPerChar":F
    const/4 v12, 0x0

    cmpg-float v12, v6, v12

    if-gez v12, :cond_3

    .line 217
    const/4 v12, 0x0

    goto :goto_0

    .line 219
    :cond_3
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v13

    int-to-float v13, v13

    add-float v11, v12, v13

    .line 220
    .local v11, "top":F
    move-object/from16 v0, p3

    iget v12, v0, Landroid/graphics/PointF;->y:F

    sub-float v10, v12, v11

    .line 221
    .local v10, "relY":F
    int-to-float v12, p1

    mul-float v2, v12, v1

    .line 222
    .local v2, "cellTop":F
    sub-float v5, v10, v2

    .line 223
    .local v5, "pxInsideCell":F
    div-float v12, v5, v6

    float-to-int v4, v12

    .line 224
    .local v4, "indexInsideCell":I
    iget v12, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mStartIndex:I

    add-int v8, v12, v4

    .line 225
    .local v8, "rangeCurrIndex":I
    iget v12, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mCurrentIndex:I

    if-ne v8, v12, :cond_4

    .line 226
    const/4 v12, 0x0

    goto :goto_0

    .line 228
    :cond_4
    iput v8, v7, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mCurrentIndex:I

    .line 229
    const/4 v12, 0x1

    goto :goto_0
.end method


# virtual methods
.method public Contains(Landroid/graphics/PointF;)Z
    .locals 5
    .param p1, "aPt"    # Landroid/graphics/PointF;

    .prologue
    .line 279
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 280
    .local v0, "bounds":Landroid/graphics/RectF;
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    return v1
.end method

.method public clearCurrentChar()V
    .locals 1

    .prologue
    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->invalidate()V

    .line 258
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 312
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/easylauncher/Utilities;->onViewDraw(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 313
    invoke-super/range {p0 .. p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 315
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingLeft()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingRight()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingBottom()I

    move-result v6

    invoke-direct {v3, v1, v2, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 318
    .local v3, "padding":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 370
    :cond_0
    return-void

    .line 322
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v8

    .line 324
    .local v8, "cellHt":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    .local v13, "len":I
    const/4 v14, -0x1

    .line 325
    .local v14, "oneBeforeCurrent":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getWidth()I

    move-result v1

    iget v2, v3, Landroid/graphics/Rect;->left:I

    iget v5, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    sub-int/2addr v1, v2

    int-to-float v4, v1

    .line 326
    .local v4, "width":F
    new-instance v15, Landroid/graphics/PointF;

    iget v1, v3, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginLeft:F

    add-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginLeft:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineMarginRight:F

    add-float/2addr v2, v5

    sub-float v2, v4, v2

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v2, v5

    invoke-direct {v15, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 330
    .local v15, "pos":Landroid/graphics/PointF;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 331
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_6

    .line 332
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 336
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-ltz v1, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-ge v1, v13, :cond_3

    .line 337
    iget v1, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    mul-float/2addr v2, v8

    add-float v16, v1, v2

    .line 338
    .local v16, "y":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v11, v8, v1

    .line 339
    .local v11, "halfCellHt":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    .line 340
    .local v10, "bounds":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    iget v2, v10, Landroid/graphics/Rect;->left:I

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v5, v11

    sub-float v5, v16, v5

    float-to-int v5, v5

    iget v6, v10, Landroid/graphics/Rect;->right:I

    add-float v7, v16, v11

    float-to-int v7, v7

    invoke-virtual {v1, v2, v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 342
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 343
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    add-int/lit8 v14, v1, -0x1

    .line 352
    .end local v10    # "bounds":Landroid/graphics/Rect;
    .end local v11    # "halfCellHt":F
    .end local v16    # "y":F
    :cond_3
    :goto_1
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    if-ge v12, v13, :cond_0

    .line 353
    add-int/lit8 v1, v13, -0x1

    if-lt v12, v1, :cond_4

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mBgFullHeight:Z

    if-eqz v1, :cond_8

    :cond_4
    const/4 v9, 0x1

    .line 354
    .local v9, "drawCellSep":Z
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 355
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-ne v12, v1, :cond_a

    .line 356
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelTextColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 357
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_4
    and-int/2addr v9, v1

    .line 361
    :goto_5
    if-ne v12, v14, :cond_5

    .line 362
    const/4 v9, 0x0

    .line 367
    :cond_5
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    iget v6, v15, Landroid/graphics/PointF;->x:F

    iget v1, v15, Landroid/graphics/PointF;->y:F

    add-int/lit8 v2, v12, 0x1

    int-to-float v2, v2

    mul-float/2addr v2, v8

    add-float v7, v1, v2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->drawChar(Landroid/graphics/Canvas;Landroid/graphics/Rect;FCFFFZ)V

    .line 352
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 333
    .end local v9    # "drawCellSep":Z
    .end local v12    # "i":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 334
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 346
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 347
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 353
    .restart local v12    # "i":I
    :cond_8
    const/4 v9, 0x0

    goto :goto_3

    .line 357
    .restart local v9    # "drawCellSep":Z
    :cond_9
    const/4 v1, 0x0

    goto :goto_4

    .line 359
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_5

    .line 365
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_6
.end method

.method public feedPt(Landroid/graphics/PointF;)I
    .locals 4
    .param p1, "aPt"    # Landroid/graphics/PointF;

    .prologue
    const/4 v1, 0x1

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-direct {p0, v2, p1, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellIndexForPt(Landroid/graphics/RectF;Landroid/graphics/PointF;Ljava/lang/StringBuffer;)I

    move-result v0

    .line 234
    .local v0, "charIndex":I
    invoke-direct {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isCellIndexValid(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 235
    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mBgFullHeight:Z

    if-eqz v1, :cond_1

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->clearCurrentChar()V

    .line 237
    const/4 v1, 0x5

    .line 252
    :cond_0
    :goto_0
    return v1

    .line 239
    :cond_1
    const/4 v1, 0x4

    goto :goto_0

    .line 241
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    invoke-direct {p0, v0, v2, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->tryUpdateCellIndexForOverflowGrp(ILandroid/graphics/RectF;Landroid/graphics/PointF;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    iget v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-ne v0, v2, :cond_3

    .line 245
    const/4 v1, 0x3

    goto :goto_0

    .line 247
    :cond_3
    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->invalidate()V

    .line 249
    iget v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    if-eqz v2, :cond_0

    .line 250
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public getCellHeight()F
    .locals 2

    .prologue
    .line 198
    iget v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellHeight:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellSepLineThick:F

    add-float/2addr v0, v1

    .line 201
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCellHeight:F

    goto :goto_0
.end method

.method public getCurrentChar()Ljava/lang/Character;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 261
    iget v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-object v2

    .line 264
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    iget v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 265
    .local v0, "ch":C
    iget-char v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpSepChar:C

    if-eq v0, v3, :cond_2

    .line 266
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    goto :goto_0

    .line 268
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mOverflowGrpCharMap:Landroid/util/SparseArray;

    iget v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;

    .line 269
    .local v1, "range":Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;
    if-eqz v1, :cond_0

    .line 272
    iget v3, v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mCurrentIndex:I

    iget v4, v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mStartIndex:I

    if-lt v3, v4, :cond_0

    iget v3, v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mCurrentIndex:I

    iget v4, v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mEndIndex:I

    if-gt v3, v4, :cond_0

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    iget v3, v1, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$OverflowGrpRange;->mCurrentIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    goto :goto_0
.end method

.method public getMidYForCurrentChar()F
    .locals 3

    .prologue
    .line 449
    iget v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 450
    const/high16 v1, -0x40800000    # -1.0f

    .line 453
    :goto_0
    return v1

    .line 452
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getCellHeight()F

    move-result v0

    .line 453
    .local v0, "cellHt":F
    iget v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    add-float/2addr v1, v2

    goto :goto_0
.end method

.method public isFloating()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIsFloating:Z

    return v0
.end method

.method public isPreMatching()Z
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mFirstCharPreMatches:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTextUppercase()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mTextUppercase:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 576
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 580
    if-eqz p1, :cond_0

    .line 581
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->updateBounds(Landroid/graphics/PointF;)Z

    .line 583
    :cond_0
    return-void
.end method

.method public pressDelayed()Z
    .locals 3

    .prologue
    .line 641
    const/4 v0, 0x0

    .line 642
    .local v0, "res":Z
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;->abort()Z

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isPressed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 644
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    iget v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mPressDelay:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;->postToQueue(I)V

    .line 645
    const/4 v0, 0x1

    .line 647
    :cond_0
    return v0
.end method

.method public setAllowWiggle(Z)V
    .locals 0
    .param p1, "aWiggle"    # Z

    .prologue
    .line 194
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAllowWiggle:Z

    .line 195
    return-void
.end method

.method public setIndexStr(Ljava/lang/String;Landroid/graphics/PointF;)V
    .locals 0
    .param p1, "aStr"    # Ljava/lang/String;
    .param p2, "aPt"    # Landroid/graphics/PointF;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    .line 152
    invoke-virtual {p0, p2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->updateBounds(Landroid/graphics/PointF;)Z

    .line 153
    return-void
.end method

.method public setPressedImmediate(Z)V
    .locals 2
    .param p1, "aPressed"    # Z

    .prologue
    .line 651
    const/4 v0, 0x0

    .line 652
    .local v0, "mustRedraw":Z
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAlphaAnim:Lcom/sec/android/app/easylauncher/ScalarAnimator;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/ScalarAnimator;->abort()Z

    move-result v1

    or-int/2addr v0, v1

    .line 653
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;->abort()Z

    move-result v1

    or-int/2addr v0, v1

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;->abort()Z

    move-result v1

    or-int/2addr v0, v1

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isPressed()Z

    move-result v1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    or-int/2addr v0, v1

    .line 656
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->setPressed(Z)V

    .line 657
    if-eqz v0, :cond_0

    .line 658
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->invalidate()V

    .line 660
    :cond_0
    return-void

    .line 655
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unPressDelayed()Z
    .locals 3

    .prologue
    .line 631
    const/4 v0, 0x0

    .line 632
    .local v0, "res":Z
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$PressRunnable;->abort()Z

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mToUnPressedState:Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;

    iget v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mUnpressDelay:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator$UnPressRunnable;->postToQueue(I)V

    .line 635
    const/4 v0, 0x1

    .line 637
    :cond_0
    return v0
.end method

.method public updateBounds(Landroid/graphics/PointF;)Z
    .locals 7
    .param p1, "aPt"    # Landroid/graphics/PointF;

    .prologue
    const/4 v6, 0x0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 165
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mCurrentIndex:I

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v6

    .line 190
    :goto_0
    return v0

    .line 169
    :cond_0
    if-nez p1, :cond_1

    .line 170
    new-instance p1, Landroid/graphics/PointF;

    .end local p1    # "aPt":Landroid/graphics/PointF;
    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    .line 172
    .restart local p1    # "aPt":Landroid/graphics/PointF;
    :cond_1
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 173
    .local v3, "drawBounds":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mIndexStr:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mWorkingStr:Ljava/lang/StringBuffer;

    iget-boolean v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mAllowWiggle:Z

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->compute(Ljava/lang/String;Ljava/lang/StringBuffer;Landroid/graphics/RectF;Landroid/graphics/PointF;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mNormalBg:Landroid/graphics/drawable/Drawable;

    iget v1, v3, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v3, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v4, v3, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelBg:Landroid/graphics/drawable/Drawable;

    iget v1, v3, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v3, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v4, v3, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 182
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mSelItemBg:Landroid/graphics/drawable/Drawable;

    iget v1, v3, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v3, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v4, v3, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->mDrawBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalListIndicator;->invalidate()V

    .line 188
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v6

    .line 190
    goto :goto_0
.end method
