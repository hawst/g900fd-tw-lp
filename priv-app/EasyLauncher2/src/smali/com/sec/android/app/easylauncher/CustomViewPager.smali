.class public Lcom/sec/android/app/easylauncher/CustomViewPager;
.super Landroid/view/ViewGroup;
.source "CustomViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;,
        Lcom/sec/android/app/easylauncher/CustomViewPager$SimpleOnPageChangeListener;,
        Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;,
        Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    }
.end annotation


# static fields
.field private static final COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = false

.field private static final DEFAULT_OFFSCREEN_PAGES:I = 0x2

.field private static final INVALID_POINTER:I = -0x1

.field private static final MAX_SETTLE_DURATION:I = 0x258

.field public static final SCROLL_STATE_DRAGGING:I = 0x1

.field public static final SCROLL_STATE_IDLE:I = 0x0

.field public static final SCROLL_STATE_SETTLING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "EasyLauncher.CustomViewPager"

.field private static final TRANSITION_PIVOT:F = 0.5f

.field private static final USE_CACHE:Z

.field private static cpuBooster:Landroid/os/DVFSHelper;

.field private static final sInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private USE_SET_INTEGRATOR_HAPTIC:Z

.field private mActivePointerId:I

.field private mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

.field private mBaseLineFlingVelocity:F

.field private mChildHeightMeasureSpec:I

.field private mChildWidthMeasureSpec:I

.field private mCurItem:I

.field mEditMode:Z

.field private mEditModePageMargin:I

.field private mFakeDragBeginTime:J

.field private mFakeDragging:Z

.field private mFirstLayout:Z

.field private mFlingVelocityInfluence:F

.field private mInLayout:Z

.field private mInitialMotionX:F

.field private mIsBeingDragged:Z

.field private mIsUnableToDrag:Z

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mMarginDrawable:Landroid/graphics/drawable/Drawable;

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mObserver:Landroid/database/DataSetObserver;

.field private mOffscreenPageLimit:I

.field private mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

.field protected mPageBackgroundAlpha:F

.field private mPageChangeEnded:Z

.field private mPageMargin:I

.field protected mPageNone:F

.field mPageTransforms:Z

.field protected mPageZoom:F

.field private mPopulatePending:Z

.field private mRestoredAdapterState:Landroid/os/Parcelable;

.field private mRestoredClassLoader:Ljava/lang/ClassLoader;

.field private mRestoredCurItem:I

.field private mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mScrollState:I

.field private mScroller:Landroid/widget/Scroller;

.field private mScrolling:Z

.field private mScrollingCacheEnabled:Z

.field private mScrollingEnabled:Z

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private motionTrackingIsCurrent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/easylauncher/CustomViewPager$1;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/CustomViewPager$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->COMPARATOR:Ljava/util/Comparator;

    .line 91
    new-instance v0, Lcom/sec/android/app/easylauncher/CustomViewPager$2;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/CustomViewPager$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->sInterpolator:Landroid/view/animation/Interpolator;

    .line 187
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->cpuBooster:Landroid/os/DVFSHelper;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 281
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_FRAMEWORK_ENABLE_INTEGRATOR_HAPTIC"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->USE_SET_INTEGRATOR_HAPTIC:Z

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    .line 106
    iput v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredCurItem:I

    .line 108
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 110
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    .line 117
    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    .line 119
    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditModePageMargin:I

    .line 135
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    .line 156
    iput v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mActivePointerId:I

    .line 185
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFirstLayout:Z

    .line 208
    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollState:I

    .line 212
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingEnabled:Z

    .line 214
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageChangeEnded:Z

    .line 1599
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageNone:F

    .line 1600
    const v0, 0x3f59999a    # 0.85f

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageZoom:F

    .line 1602
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageBackgroundAlpha:F

    .line 1606
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageTransforms:Z

    .line 2098
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditMode:Z

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->initViewPager()V

    .line 283
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 286
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_FRAMEWORK_ENABLE_INTEGRATOR_HAPTIC"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->USE_SET_INTEGRATOR_HAPTIC:Z

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    .line 106
    iput v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredCurItem:I

    .line 108
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 110
    iput-object v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    .line 117
    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    .line 119
    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditModePageMargin:I

    .line 135
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    .line 156
    iput v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mActivePointerId:I

    .line 185
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFirstLayout:Z

    .line 208
    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollState:I

    .line 212
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingEnabled:Z

    .line 214
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageChangeEnded:Z

    .line 1599
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageNone:F

    .line 1600
    const v0, 0x3f59999a    # 0.85f

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageZoom:F

    .line 1602
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageBackgroundAlpha:F

    .line 1606
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageTransforms:Z

    .line 2098
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditMode:Z

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->initViewPager()V

    .line 288
    return-void
.end method

.method private completeScroll()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1082
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrolling:Z

    .line 1083
    .local v2, "needPopulate":Z
    if-eqz v2, :cond_2

    .line 1085
    invoke-direct {p0, v8}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    .line 1086
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1087
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v3

    .line 1088
    .local v3, "oldX":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v4

    .line 1089
    .local v4, "oldY":I
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    .line 1090
    .local v5, "x":I
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    .line 1091
    .local v6, "y":I
    if-ne v3, v5, :cond_0

    if-eq v4, v6, :cond_1

    .line 1092
    :cond_0
    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    .line 1094
    :cond_1
    invoke-direct {p0, v8}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollState(I)V

    .line 1096
    .end local v3    # "oldX":I
    .end local v4    # "oldY":I
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    .line 1097
    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrolling:Z

    .line 1098
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_4

    .line 1099
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    .line 1100
    .local v1, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    iget-boolean v7, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->scrolling:Z

    if-eqz v7, :cond_3

    .line 1101
    const/4 v2, 0x1

    .line 1102
    iput-boolean v8, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->scrolling:Z

    .line 1098
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1105
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_4
    if-eqz v2, :cond_5

    .line 1106
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageChangeEnded:Z

    .line 1107
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    .line 1109
    :cond_5
    return-void
.end method

.method private endDrag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1789
    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    .line 1790
    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsUnableToDrag:Z

    .line 1792
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1793
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1794
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1796
    :cond_0
    return-void
.end method

.method static mix(FFF)F
    .locals 2
    .param p0, "x"    # F
    .param p1, "y"    # F
    .param p2, "mix"    # F

    .prologue
    .line 1595
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    mul-float/2addr v0, p0

    mul-float v1, p1, p2

    add-float/2addr v0, v1

    return v0
.end method

.method private recomputeScrollPosition(IIII)V
    .locals 11
    .param p1, "width"    # I
    .param p2, "oldWidth"    # I
    .param p3, "margin"    # I
    .param p4, "oldMargin"    # I

    .prologue
    const/4 v2, 0x0

    .line 981
    add-int v10, p1, p3

    .line 982
    .local v10, "widthWithMargin":I
    if-lez p2, :cond_1

    .line 983
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v7

    .line 984
    .local v7, "oldScrollPos":I
    add-int v8, p2, p4

    .line 985
    .local v8, "oldwwm":I
    div-int v6, v7, v8

    .line 986
    .local v6, "oldScrollItem":I
    rem-int v0, v7, v8

    int-to-float v0, v0

    int-to-float v3, v8

    div-float v9, v0, v3

    .line 987
    .local v9, "scrollOffset":F
    int-to-float v0, v6

    add-float/2addr v0, v9

    int-to-float v3, v10

    mul-float/2addr v0, v3

    float-to-int v1, v0

    .line 988
    .local v1, "scrollPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    .line 989
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 992
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->timePassed()I

    move-result v3

    sub-int v5, v0, v3

    .line 993
    .local v5, "newDuration":I
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    mul-int/2addr v3, v10

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1002
    .end local v5    # "newDuration":I
    .end local v6    # "oldScrollItem":I
    .end local v7    # "oldScrollPos":I
    .end local v8    # "oldwwm":I
    .end local v9    # "scrollOffset":F
    :cond_0
    :goto_0
    return-void

    .line 996
    .end local v1    # "scrollPos":I
    :cond_1
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    mul-int v1, v0, v10

    .line 997
    .restart local v1    # "scrollPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 998
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->completeScroll()V

    .line 999
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    goto :goto_0
.end method

.method private releaseVelocityTracker()V
    .locals 1

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1308
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1309
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1311
    :cond_0
    return-void
.end method

.method private setScrollState(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 314
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollState:I

    if-ne v0, p1, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iput p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollState:I

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    goto :goto_0
.end method

.method private setScrollingCacheEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1799
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingCacheEnabled:Z

    if-eq v0, p1, :cond_0

    .line 1800
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingCacheEnabled:Z

    .line 1811
    :cond_0
    return-void
.end method

.method private updateMotionTracking(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1116
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionY:F

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1122
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    .line 1123
    return-void
.end method


# virtual methods
.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 7
    .param p2, "direction"    # I
    .param p3, "focusableMode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1967
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    if-nez p1, :cond_1

    .line 2006
    :cond_0
    :goto_0
    return-void

    .line 1970
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1972
    .local v2, "focusableCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getDescendantFocusability()I

    move-result v1

    .line 1974
    .local v1, "descendantFocusability":I
    const/high16 v5, 0x60000

    if-eq v1, v5, :cond_3

    .line 1975
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 1976
    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1977
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    .line 1978
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v4

    .line 1979
    .local v4, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    if-eqz v4, :cond_2

    iget v5, v4, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v5, v6, :cond_2

    .line 1980
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1975
    .end local v4    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1991
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "i":I
    :cond_3
    const/high16 v5, 0x40000

    if-ne v1, v5, :cond_4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v2, v5, :cond_0

    .line 1996
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->isFocusable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1999
    and-int/lit8 v5, p3, 0x1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->isInTouchMode()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->isFocusableInTouchMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2004
    :cond_5
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method addNewItem(II)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "index"    # I

    .prologue
    .line 624
    new-instance v0, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;-><init>()V

    .line 625
    .local v0, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    iput p1, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    .line 626
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1, p0, p1}, Lcom/sec/android/app/easylauncher/PageAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    .line 627
    if-gez p2, :cond_0

    .line 628
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    :goto_0
    return-void

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2016
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 2017
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2018
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 2019
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v2

    .line 2020
    .local v2, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    if-eqz v2, :cond_0

    iget v3, v2, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v3, v4, :cond_0

    .line 2021
    invoke-virtual {v0, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 2016
    .end local v2    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2025
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 894
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInLayout:Z

    if-eqz v0, :cond_0

    .line 895
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 896
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mChildWidthMeasureSpec:I

    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mChildHeightMeasureSpec:I

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 908
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public arrowScroll(I)Z
    .locals 6
    .param p1, "direction"    # I

    .prologue
    const/16 v5, 0x42

    const/16 v4, 0x11

    .line 1904
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1905
    .local v0, "currentFocused":Landroid/view/View;
    if-ne v0, p0, :cond_0

    .line 1906
    const/4 v0, 0x0

    .line 1908
    :cond_0
    const/4 v1, 0x0

    .line 1910
    .local v1, "handled":Z
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v3

    invoke-virtual {v3, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 1911
    .local v2, "nextFocused":Landroid/view/View;
    if-eqz v2, :cond_7

    if-eq v2, v0, :cond_7

    .line 1912
    if-ne p1, v4, :cond_4

    .line 1916
    if-eqz v0, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 1917
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->pageLeft()Z

    move-result v1

    .line 1940
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 1941
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->playSoundEffect(I)V

    .line 1943
    :cond_2
    return v1

    .line 1919
    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v1

    goto :goto_0

    .line 1921
    :cond_4
    if-ne p1, v5, :cond_6

    .line 1925
    if-eqz v0, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    if-gt v3, v4, :cond_5

    .line 1926
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->pageRight()Z

    move-result v1

    goto :goto_0

    .line 1928
    :cond_5
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v1

    goto :goto_0

    .line 1930
    :cond_6
    const/16 v3, 0x21

    if-ne p1, v3, :cond_1

    .line 1931
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v1

    goto :goto_0

    .line 1933
    :cond_7
    if-eq p1, v4, :cond_8

    const/4 v3, 0x1

    if-ne p1, v3, :cond_9

    .line 1935
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->pageLeft()Z

    move-result v1

    goto :goto_0

    .line 1936
    :cond_9
    if-eq p1, v5, :cond_a

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 1938
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->pageRight()Z

    move-result v1

    goto :goto_0
.end method

.method public beginFakeDrag()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1671
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    if-eqz v2, :cond_0

    .line 1687
    :goto_0
    return v4

    .line 1674
    :cond_0
    iput-boolean v9, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragging:Z

    .line 1675
    invoke-direct {p0, v9}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollState(I)V

    .line 1676
    iput v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    iput v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    .line 1677
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v2, :cond_1

    .line 1678
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1682
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .local v0, "time":J
    move-wide v2, v0

    move v6, v5

    move v7, v4

    .line 1683
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 1684
    .local v8, "ev":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v8}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1685
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 1686
    iput-wide v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragBeginTime:J

    move v4, v9

    .line 1687
    goto :goto_0

    .line 1680
    .end local v0    # "time":J
    .end local v8    # "ev":Landroid/view/MotionEvent;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1
.end method

.method protected canScroll(Landroid/view/View;ZIII)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "checkV"    # Z
    .param p3, "dx"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    .line 1825
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v7, p1

    .line 1826
    check-cast v7, Landroid/view/ViewGroup;

    .line 1827
    .local v7, "group":Landroid/view/ViewGroup;
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v9

    .line 1828
    .local v9, "scrollX":I
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v10

    .line 1829
    .local v10, "scrollY":I
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    .line 1832
    .local v6, "count":I
    add-int/lit8 v8, v6, -0x1

    .local v8, "i":I
    :goto_0
    if-ltz v8, :cond_1

    .line 1835
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1836
    .local v1, "child":Landroid/view/View;
    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt v0, v2, :cond_0

    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge v0, v2, :cond_0

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_0

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->canScroll(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842
    const/4 v0, 0x1

    .line 1847
    .end local v1    # "child":Landroid/view/View;
    .end local v6    # "count":I
    .end local v7    # "group":Landroid/view/ViewGroup;
    .end local v8    # "i":I
    .end local v9    # "scrollX":I
    .end local v10    # "scrollY":I
    :goto_1
    return v0

    .line 1832
    .restart local v1    # "child":Landroid/view/View;
    .restart local v6    # "count":I
    .restart local v7    # "group":Landroid/view/ViewGroup;
    .restart local v8    # "i":I
    .restart local v9    # "scrollX":I
    .restart local v10    # "scrollY":I
    :cond_0
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 1847
    .end local v1    # "child":Landroid/view/View;
    .end local v6    # "count":I
    .end local v7    # "group":Landroid/view/ViewGroup;
    .end local v8    # "i":I
    .end local v9    # "scrollX":I
    .end local v10    # "scrollY":I
    :cond_1
    if-eqz p2, :cond_2

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->canScrollHorizontally(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public computeScroll()V
    .locals 10

    .prologue
    .line 1046
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->isFinished()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1047
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1050
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v2

    .line 1051
    .local v2, "oldX":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v3

    .line 1052
    .local v3, "oldY":I
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->getCurrX()I

    move-result v6

    .line 1053
    .local v6, "x":I
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->getCurrY()I

    move-result v7

    .line 1055
    .local v7, "y":I
    if-ne v2, v6, :cond_0

    if-eq v3, v7, :cond_1

    .line 1056
    :cond_0
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    .line 1059
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    if-eqz v8, :cond_2

    .line 1060
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3

    div-int/lit8 v5, v8, 0x4

    .line 1062
    .local v5, "widthWithMargin":I
    div-int v4, v6, v5

    .line 1066
    .local v4, "position":I
    rem-int v1, v6, v5

    .line 1067
    .local v1, "offsetPixels":I
    int-to-float v8, v1

    int-to-float v9, v5

    div-float v0, v8, v9

    .line 1068
    .local v0, "offset":F
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    invoke-interface {v8, v4, v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 1072
    .end local v0    # "offset":F
    .end local v1    # "offsetPixels":I
    .end local v4    # "position":I
    .end local v5    # "widthWithMargin":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    .line 1079
    .end local v2    # "oldX":I
    .end local v3    # "oldY":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :goto_0
    return-void

    .line 1078
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->completeScroll()V

    goto :goto_0
.end method

.method dataSetChanged()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 639
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v8

    if-eq v7, v8, :cond_1

    move v2, v5

    .line 640
    .local v2, "needPopulate":Z
    :goto_0
    const/4 v3, -0x1

    .line 642
    .local v3, "newCurrItem":I
    const-string v7, "EasyLauncher.CustomViewPager"

    const-string v8, "  ---- dataSetChanged"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_5

    .line 645
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    .line 647
    .local v1, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v8, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    iget v9, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/easylauncher/PageAdapter;->getItemPosition(Ljava/lang/Object;I)I

    move-result v4

    .line 649
    .local v4, "newPos":I
    const/4 v7, -0x1

    if-ne v4, v7, :cond_2

    .line 644
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .end local v2    # "needPopulate":Z
    .end local v3    # "newCurrItem":I
    .end local v4    # "newPos":I
    :cond_1
    move v2, v6

    .line 639
    goto :goto_0

    .line 653
    .restart local v0    # "i":I
    .restart local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .restart local v2    # "needPopulate":Z
    .restart local v3    # "newCurrItem":I
    .restart local v4    # "newPos":I
    :cond_2
    const/4 v7, -0x2

    if-ne v4, v7, :cond_3

    .line 654
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 655
    add-int/lit8 v0, v0, -0x1

    .line 656
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget v8, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget-object v9, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    invoke-virtual {v7, p0, v8, v9}, Lcom/sec/android/app/easylauncher/PageAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 657
    const/4 v2, 0x1

    .line 659
    iget v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    iget v8, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    if-ne v7, v8, :cond_0

    .line 661
    iget v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    iget-object v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_2

    .line 666
    :cond_3
    iget v7, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    if-eq v7, v4, :cond_0

    .line 667
    iget v7, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v7, v8, :cond_4

    .line 669
    move v3, v4

    .line 672
    :cond_4
    iput v4, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    .line 673
    const/4 v2, 0x1

    goto :goto_2

    .line 677
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .end local v4    # "newPos":I
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    sget-object v8, Lcom/sec/android/app/easylauncher/CustomViewPager;->COMPARATOR:Ljava/util/Comparator;

    invoke-static {v7, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 679
    if-ltz v3, :cond_6

    .line 681
    invoke-virtual {p0, v3, v6, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    .line 682
    const/4 v2, 0x1

    .line 685
    :cond_6
    const-string v5, "EasyLauncher.CustomViewPager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "  ---- dataSetChanged mItems.size():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "needPopulate"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    if-eqz v2, :cond_7

    .line 687
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->requestLayout()V

    .line 691
    :cond_7
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1567
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v0

    .line 1569
    .local v0, "childCount":I
    iget-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditMode:Z

    if-eqz v3, :cond_0

    .line 1570
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageTransforms:Z

    .line 1572
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1573
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1575
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p0, v2, v5, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->transformPage(Landroid/view/View;ZI)V

    .line 1572
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1580
    .end local v1    # "i":I
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageTransforms:Z

    if-eqz v3, :cond_1

    .line 1581
    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageTransforms:Z

    .line 1583
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 1584
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1585
    .restart local v2    # "v":Landroid/view/View;
    invoke-virtual {p0, v2, v4, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->transformPage(Landroid/view/View;ZI)V

    .line 1583
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1590
    .end local v1    # "i":I
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1591
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1852
    const/4 v0, 0x0

    .line 1855
    .local v0, "result":Z
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1856
    if-nez v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1857
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1858
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1866
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1867
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1870
    :cond_1
    return v0

    .line 1860
    :pswitch_0
    const/16 v1, 0x21

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 1858
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 2067
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v1

    .line 2068
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 2069
    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2070
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 2071
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v3

    .line 2072
    .local v3, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    if-eqz v3, :cond_0

    iget v4, v3, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v4, v5, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2074
    const/4 v4, 0x1

    .line 2079
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :goto_1
    return v4

    .line 2068
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2079
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method distanceInfluenceForSnapDuration(F)F
    .locals 4
    .param p1, "f"    # F

    .prologue
    .line 565
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float/2addr p1, v0

    .line 566
    float-to-double v0, p1

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float p1, v0

    .line 567
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x1

    .line 1504
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 1505
    const/4 v2, 0x0

    .line 1507
    .local v2, "needsInvalidate":Z
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v3

    .line 1508
    .local v3, "overScrollMode":I
    if-eqz v3, :cond_0

    if-ne v3, v1, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v6

    if-le v6, v1, :cond_5

    .line 1511
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1512
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 1513
    .local v4, "restoreCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingBottom()I

    move-result v7

    sub-int v0, v6, v7

    .line 1515
    .local v0, "height":I
    const/high16 v6, 0x43870000    # 270.0f

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1516
    neg-int v6, v0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1517
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v7

    invoke-virtual {v6, v0, v7}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 1518
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    .line 1519
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1521
    .end local v0    # "height":I
    .end local v4    # "restoreCount":I
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1522
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 1523
    .restart local v4    # "restoreCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v5

    .line 1524
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingBottom()I

    move-result v7

    sub-int v0, v6, v7

    .line 1525
    .restart local v0    # "height":I
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v1

    .line 1527
    .local v1, "itemCount":I
    :cond_2
    const/high16 v6, 0x42b40000    # 90.0f

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1528
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingTop()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    neg-int v7, v1

    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v8, v5

    mul-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1529
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6, v0, v5}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 1530
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    .line 1531
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1538
    .end local v0    # "height":I
    .end local v1    # "itemCount":I
    .end local v4    # "restoreCount":I
    .end local v5    # "width":I
    :cond_3
    :goto_0
    if-eqz v2, :cond_4

    .line 1540
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    .line 1542
    :cond_4
    return-void

    .line 1534
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    .line 1535
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v6}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 550
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    .line 552
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 555
    :cond_0
    return-void
.end method

.method public endFakeDrag()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1697
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragging:Z

    if-nez v2, :cond_0

    .line 1698
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1701
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1702
    .local v1, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v2, 0x3e8

    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMaximumVelocity:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1703
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mActivePointerId:I

    invoke-static {v1, v2}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v2

    float-to-int v0, v2

    .line 1705
    .local v0, "initialVelocity":I
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    .line 1706
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMinimumVelocity:I

    if-gt v2, v3, :cond_1

    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 1708
    :cond_1
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 1709
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2, v5, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    .line 1716
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->endDrag()V

    .line 1718
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragging:Z

    .line 1719
    return-void

    .line 1711
    :cond_2
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2, v5, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    goto :goto_0

    .line 1714
    :cond_3
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    invoke-virtual {p0, v2, v5, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    goto :goto_0
.end method

.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 1882
    const/4 v0, 0x0

    .line 1883
    .local v0, "handled":Z
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1884
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1900
    :cond_0
    :goto_0
    return v0

    .line 1886
    :sswitch_0
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->arrowScroll(I)Z

    move-result v0

    .line 1887
    goto :goto_0

    .line 1889
    :sswitch_1
    const/16 v1, 0x42

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->arrowScroll(I)Z

    move-result v0

    .line 1890
    goto :goto_0

    .line 1892
    :sswitch_2
    invoke-static {p1}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1893
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 1894
    :cond_1
    invoke-static {p1, v2}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1895
    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 1884
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x3d -> :sswitch_2
    .end sparse-switch
.end method

.method public fakeDragBy(F)V
    .locals 19
    .param p1, "xOffset"    # F

    .prologue
    .line 1730
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragging:Z

    if-nez v2, :cond_0

    .line 1731
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1734
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    add-float v2, v2, p1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1735
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    sub-float v16, v2, p1

    .line 1736
    .local v16, "scrollX":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v17

    .line 1737
    .local v17, "width":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int v18, v17, v2

    .line 1739
    .local v18, "widthWithMargin":I
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/lit8 v3, v3, -0x1

    mul-int v3, v3, v18

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v11, v2

    .line 1740
    .local v11, "leftBound":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int v2, v2, v18

    int-to-float v15, v2

    .line 1741
    .local v15, "rightBound":F
    cmpg-float v2, v16, v11

    if-gez v2, :cond_3

    .line 1742
    move/from16 v16, v11

    .line 1747
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    move/from16 v0, v16

    float-to-int v3, v0

    int-to-float v3, v3

    sub-float v3, v16, v3

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1748
    move/from16 v0, v16

    float-to-int v2, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    .line 1749
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    if-eqz v2, :cond_2

    .line 1750
    move/from16 v0, v16

    float-to-int v2, v0

    div-int v12, v2, v18

    .line 1751
    .local v12, "position":I
    move/from16 v0, v16

    float-to-int v2, v0

    rem-int v14, v2, v18

    .line 1752
    .local v14, "positionOffsetPixels":I
    int-to-float v2, v14

    move/from16 v0, v18

    int-to-float v3, v0

    div-float v13, v2, v3

    .line 1753
    .local v13, "positionOffset":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    invoke-interface {v2, v12, v13, v14}, Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 1757
    .end local v12    # "position":I
    .end local v13    # "positionOffset":F
    .end local v14    # "positionOffsetPixels":I
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 1758
    .local v4, "time":J
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragBeginTime:J

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v10

    .line 1760
    .local v10, "ev":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v10}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1761
    invoke-virtual {v10}, Landroid/view/MotionEvent;->recycle()V

    .line 1762
    return-void

    .line 1743
    .end local v4    # "time":J
    .end local v10    # "ev":Landroid/view/MotionEvent;
    :cond_3
    cmpl-float v2, v16, v15

    if-lez v2, :cond_1

    .line 1744
    move/from16 v16, v15

    goto :goto_0
.end method

.method public getAdapter()Lcom/sec/android/app/easylauncher/PageAdapter;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    return-object v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    return v0
.end method

.method public getOffscreenPageLimit()I
    .locals 1

    .prologue
    .line 459
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    return v0
.end method

.method public getPageMargin()I
    .locals 1

    .prologue
    .line 518
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    return v0
.end method

.method infoForAnyChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 922
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .local v0, "parent":Landroid/view/ViewParent;
    if-eq v0, p0, :cond_2

    .line 923
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 924
    :cond_0
    const/4 v1, 0x0

    .line 928
    :goto_1
    return-object v1

    :cond_1
    move-object p1, v0

    .line 926
    check-cast p1, Landroid/view/View;

    goto :goto_0

    .line 928
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v1

    goto :goto_1
.end method

.method infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .locals 4
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 911
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 912
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    .line 913
    .local v1, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v3, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 917
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :goto_1
    return-object v1

    .line 911
    .restart local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 917
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method initViewPager()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 291
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setWillNotDraw(Z)V

    .line 292
    const/high16 v3, 0x40000

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setDescendantFocusability(I)V

    .line 293
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setFocusable(Z)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 295
    .local v1, "context":Landroid/content/Context;
    new-instance v3, Landroid/widget/Scroller;

    sget-object v4, Lcom/sec/android/app/easylauncher/CustomViewPager;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v3, v1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    .line 296
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 297
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    const/16 v3, 0x20

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mTouchSlop:I

    .line 300
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMinimumVelocity:I

    .line 301
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMaximumVelocity:I

    .line 302
    new-instance v3, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 303
    new-instance v3, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 305
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->density:F

    .line 306
    .local v2, "density":F
    const v3, 0x451c4000    # 2500.0f

    mul-float/2addr v3, v2

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mBaseLineFlingVelocity:F

    .line 307
    const v3, 0x3ecccccd    # 0.4f

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFlingVelocityInfluence:F

    .line 308
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageChangeEnded:Z

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070041

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditModePageMargin:I

    .line 311
    return-void
.end method

.method public isFakeDragging()Z
    .locals 1

    .prologue
    .line 1773
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFakeDragging:Z

    return v0
.end method

.method public isHapticFeedbackExtraOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2087
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "haptic_feedback_extra"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2088
    .local v0, "sIsHapticFeedbackExtraOn":Z
    :goto_0
    return v0

    .end local v0    # "sIsHapticFeedbackExtraOn":Z
    :cond_0
    move v0, v1

    .line 2087
    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 933
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 934
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFirstLayout:Z

    .line 935
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1546
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1549
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    .line 1550
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v2

    .line 1551
    .local v2, "scrollX":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v3

    .line 1552
    .local v3, "width":I
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v4, v3

    rem-int v1, v2, v4

    .line 1553
    .local v1, "offset":I
    if-eqz v1, :cond_0

    .line 1556
    sub-int v4, v2, v1

    add-int v0, v4, v3

    .line 1557
    .local v0, "left":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    iget v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v6, v0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getHeight()I

    move-result v7

    invoke-virtual {v4, v0, v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1558
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1561
    .end local v0    # "left":I
    .end local v1    # "offset":I
    .end local v2    # "scrollX":I
    .end local v3    # "width":I
    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x0

    .line 1471
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 1472
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1499
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_0
    return v2

    .line 1478
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 1479
    const/4 v1, 0x0

    .line 1480
    .local v1, "vscroll":F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 1485
    .local v0, "hscroll":F
    :goto_1
    cmpl-float v2, v0, v3

    if-nez v2, :cond_1

    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    .line 1486
    :cond_1
    cmpl-float v2, v0, v3

    if-gtz v2, :cond_2

    cmpl-float v2, v1, v3

    if-lez v2, :cond_4

    .line 1487
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->pageRight()Z

    .line 1491
    :goto_2
    const/4 v2, 0x1

    goto :goto_0

    .line 1482
    .end local v0    # "hscroll":F
    .end local v1    # "vscroll":F
    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    neg-float v1, v2

    .line 1483
    .restart local v1    # "vscroll":F
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .restart local v0    # "hscroll":F
    goto :goto_1

    .line 1489
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->pageLeft()Z

    goto :goto_2

    .line 1472
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 1132
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    if-nez v0, :cond_0

    .line 1133
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->updateMotionTracking(Landroid/view/MotionEvent;)V

    .line 1135
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    .line 1136
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1138
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1140
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v6, v0, 0xff

    .line 1141
    .local v6, "action":I
    sget-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->cpuBooster:Landroid/os/DVFSHelper;

    if-nez v0, :cond_2

    .line 1142
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getCpuBoosterObject()Landroid/os/DVFSHelper;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->cpuBooster:Landroid/os/DVFSHelper;

    .line 1143
    :cond_2
    sget-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->cpuBooster:Landroid/os/DVFSHelper;

    const-string v1, "Launcher_touch"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 1148
    const/4 v0, 0x3

    if-eq v6, v0, :cond_3

    if-ne v6, v12, :cond_5

    .line 1152
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    .line 1153
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsUnableToDrag:Z

    .line 1154
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mActivePointerId:I

    .line 1155
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->releaseVelocityTracker()V

    .line 1156
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    .line 1157
    sget-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->cpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 1303
    :cond_4
    :goto_0
    return v2

    .line 1161
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingEnabled:Z

    if-eqz v0, :cond_4

    .line 1171
    packed-switch v6, :pswitch_data_0

    .line 1303
    :cond_6
    :goto_1
    :pswitch_0
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    goto :goto_0

    .line 1174
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    .line 1179
    .local v8, "x":F
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    sub-float v7, v8, v0

    .line 1180
    .local v7, "dx":F
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 1184
    .local v9, "xDiff":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    .line 1185
    .local v10, "y":F
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionY:F

    sub-float v0, v10, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1194
    .local v11, "yDiff":F
    float-to-int v3, v7

    float-to-int v4, v8

    float-to-int v5, v10

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->canScroll(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1198
    iput v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    iput v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    .line 1199
    iput v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionY:F

    .line 1200
    iput-boolean v12, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsUnableToDrag:Z

    goto :goto_0

    .line 1203
    :cond_7
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_d

    .line 1207
    iput-boolean v12, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    .line 1209
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-nez v0, :cond_8

    cmpl-float v0, v7, v13

    if-gtz v0, :cond_9

    :cond_8
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_b

    cmpg-float v0, v7, v13

    if-gez v0, :cond_b

    .line 1212
    :cond_9
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->USE_SET_INTEGRATOR_HAPTIC:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->isHapticFeedbackExtraOn()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1213
    const/16 v0, 0x55a0

    invoke-virtual {p0, v0, v12}, Lcom/sec/android/app/easylauncher/CustomViewPager;->performHapticFeedback(II)Z

    .line 1223
    :cond_a
    :goto_2
    invoke-direct {p0, v12}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollState(I)V

    .line 1224
    cmpl-float v0, v7, v13

    if-lez v0, :cond_c

    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mTouchSlop:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_3
    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1226
    invoke-direct {p0, v12}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_1

    .line 1218
    :cond_b
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->USE_SET_INTEGRATOR_HAPTIC:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->isHapticFeedbackExtraOn()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1219
    const/16 v0, 0x558c

    invoke-virtual {p0, v0, v12}, Lcom/sec/android/app/easylauncher/CustomViewPager;->performHapticFeedback(II)Z

    goto :goto_2

    .line 1224
    :cond_c
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mTouchSlop:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_3

    .line 1228
    :cond_d
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_6

    goto/16 :goto_1

    .line 1253
    .end local v7    # "dx":F
    .end local v8    # "x":F
    .end local v9    # "xDiff":F
    .end local v10    # "y":F
    .end local v11    # "yDiff":F
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->cpuBooster:Landroid/os/DVFSHelper;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 1254
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    .line 1257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionY:F

    .line 1258
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsUnableToDrag:Z

    .line 1259
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    goto/16 :goto_1

    .line 1280
    :pswitch_3
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    .line 1281
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->releaseVelocityTracker()V

    goto/16 :goto_1

    .line 1287
    :pswitch_4
    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    .line 1288
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->releaseVelocityTracker()V

    goto/16 :goto_1

    .line 1171
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 1006
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInLayout:Z

    .line 1007
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    .line 1008
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInLayout:Z

    .line 1010
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v3

    .line 1011
    .local v3, "count":I
    sub-int v7, p4, p2

    .line 1013
    .local v7, "width":I
    iget-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->USE_SET_INTEGRATOR_HAPTIC:Z

    if-eqz v8, :cond_0

    iget-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageChangeEnded:Z

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->isHapticFeedbackExtraOn()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1014
    const/16 v8, 0x5596

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/easylauncher/CustomViewPager;->performHapticFeedback(II)Z

    .line 1016
    :cond_0
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageChangeEnded:Z

    .line 1017
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_2

    .line 1018
    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1020
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v5

    .local v5, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    if-eqz v5, :cond_1

    .line 1021
    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v8, v7

    iget v9, v5, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    mul-int v6, v8, v9

    .line 1022
    .local v6, "loff":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingLeft()I

    move-result v8

    add-int v1, v8, v6

    .line 1023
    .local v1, "childLeft":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingTop()I

    move-result v2

    .line 1028
    .local v2, "childTop":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v2

    invoke-virtual {v0, v1, v2, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 1017
    .end local v1    # "childLeft":I
    .end local v2    # "childTop":I
    .end local v5    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .end local v6    # "loff":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1039
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFirstLayout:Z

    .line 1040
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 944
    invoke-static {v5, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getDefaultSize(II)I

    move-result v3

    invoke-static {v5, p2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getDefaultSize(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setMeasuredDimension(II)V

    .line 948
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mChildWidthMeasureSpec:I

    .line 950
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mChildHeightMeasureSpec:I

    .line 954
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInLayout:Z

    .line 955
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    .line 956
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInLayout:Z

    .line 959
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v2

    .line 960
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 961
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 962
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 965
    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mChildWidthMeasureSpec:I

    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mChildHeightMeasureSpec:I

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 960
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 968
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 9
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2035
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v1

    .line 2036
    .local v1, "count":I
    and-int/lit8 v7, p1, 0x2

    if-eqz v7, :cond_0

    .line 2037
    const/4 v6, 0x0

    .line 2038
    .local v6, "index":I
    const/4 v5, 0x1

    .line 2039
    .local v5, "increment":I
    move v2, v1

    .line 2045
    .local v2, "end":I
    :goto_0
    move v3, v6

    .local v3, "i":I
    :goto_1
    if-eq v3, v2, :cond_2

    .line 2046
    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2047
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 2048
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v4

    .line 2049
    .local v4, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    if-eqz v4, :cond_1

    iget v7, v4, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v7, v8, :cond_1

    .line 2050
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2051
    const/4 v7, 0x1

    .line 2056
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :goto_2
    return v7

    .line 2041
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v5    # "increment":I
    .end local v6    # "index":I
    :cond_0
    add-int/lit8 v6, v1, -0x1

    .line 2042
    .restart local v6    # "index":I
    const/4 v5, -0x1

    .line 2043
    .restart local v5    # "increment":I
    const/4 v2, -0x1

    .restart local v2    # "end":I
    goto :goto_0

    .line 2045
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    :cond_1
    add-int/2addr v3, v5

    goto :goto_1

    .line 2056
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 874
    instance-of v1, p1, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;

    if-nez v1, :cond_0

    .line 875
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 890
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 879
    check-cast v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;

    .line 880
    .local v0, "ss":Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 882
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v1, :cond_1

    .line 883
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->adapterState:Landroid/os/Parcelable;

    iget-object v3, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 884
    iget v1, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->position:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    goto :goto_0

    .line 886
    :cond_1
    iget v1, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->position:I

    iput v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredCurItem:I

    .line 887
    iget-object v1, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->adapterState:Landroid/os/Parcelable;

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 888
    iget-object v1, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->loader:Ljava/lang/ClassLoader;

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 863
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 864
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 865
    .local v0, "ss":Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    iput v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->position:I

    .line 866
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v2, :cond_0

    .line 867
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/PageAdapter;->saveState()Landroid/os/Parcelable;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/app/easylauncher/CustomViewPager$SavedState;->adapterState:Landroid/os/Parcelable;

    .line 869
    :cond_0
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 972
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 975
    if-eq p1, p3, :cond_0

    .line 976
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->recomputeScrollPosition(IIII)V

    .line 978
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 27
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1315
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    move/from16 v25, v0

    if-nez v25, :cond_0

    .line 1316
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->updateMotionTracking(Landroid/view/MotionEvent;)V

    .line 1318
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v25

    if-nez v25, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v25

    if-eqz v25, :cond_1

    .line 1322
    const/16 v25, 0x0

    .line 1466
    :goto_0
    return v25

    .line 1325
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    move-object/from16 v25, v0

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v25

    if-nez v25, :cond_3

    .line 1327
    :cond_2
    const/16 v25, 0x0

    goto :goto_0

    .line 1330
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingEnabled:Z

    move/from16 v25, v0

    if-nez v25, :cond_4

    .line 1331
    const/16 v25, 0x0

    goto :goto_0

    .line 1333
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v25, v0

    if-nez v25, :cond_5

    .line 1334
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1336
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1338
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 1339
    .local v3, "action":I
    const/4 v9, 0x0

    .line 1341
    .local v9, "needsInvalidate":Z
    and-int/lit16 v0, v3, 0xff

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_0

    .line 1463
    :cond_6
    :goto_1
    :pswitch_0
    if-eqz v9, :cond_7

    .line 1464
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    .line 1466
    :cond_7
    const/16 v25, 0x1

    goto :goto_0

    .line 1347
    :pswitch_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->completeScroll()V

    .line 1350
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mInitialMotionX:F

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    goto :goto_1

    .line 1355
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    move/from16 v25, v0

    if-nez v25, :cond_8

    .line 1357
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 1358
    .local v21, "x":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    move/from16 v25, v0

    sub-float v25, v21, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v22

    .line 1359
    .local v22, "xDiff":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v23

    .line 1360
    .local v23, "y":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionY:F

    move/from16 v25, v0

    sub-float v25, v23, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v24

    .line 1365
    .local v24, "yDiff":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mTouchSlop:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    cmpl-float v25, v22, v25

    if-lez v25, :cond_8

    cmpl-float v25, v22, v24

    if-lez v25, :cond_8

    .line 1369
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    .line 1370
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1371
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollState(I)V

    .line 1372
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    .line 1376
    .end local v21    # "x":F
    .end local v22    # "xDiff":F
    .end local v23    # "y":F
    .end local v24    # "yDiff":F
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    move/from16 v25, v0

    if-eqz v25, :cond_6

    .line 1381
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 1382
    .restart local v21    # "x":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    move/from16 v25, v0

    sub-float v5, v25, v21

    .line 1383
    .local v5, "deltaX":F
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1384
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v25

    move/from16 v0, v25

    int-to-float v11, v0

    .line 1385
    .local v11, "oldScrollX":F
    add-float v17, v11, v5

    .line 1386
    .local v17, "scrollX":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v19

    .line 1387
    .local v19, "width":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    move/from16 v25, v0

    add-int v20, v19, v25

    .line 1389
    .local v20, "widthWithMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v25

    add-int/lit8 v7, v25, -0x1

    .line 1390
    .local v7, "lastItemIndex":I
    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    mul-int v26, v26, v20

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->max(II)I

    move-result v25

    move/from16 v0, v25

    int-to-float v8, v0

    .line 1391
    .local v8, "leftBound":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v25

    mul-int v25, v25, v20

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v16, v0

    .line 1393
    .local v16, "rightBound":F
    cmpg-float v25, v17, v8

    if-gez v25, :cond_b

    .line 1394
    const/16 v25, 0x0

    cmpl-float v25, v8, v25

    if-nez v25, :cond_9

    .line 1395
    move/from16 v0, v17

    neg-float v12, v0

    .line 1396
    .local v12, "over":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v25, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v26, v12, v26

    invoke-virtual/range {v25 .. v26}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v9

    .line 1398
    .end local v12    # "over":F
    :cond_9
    move/from16 v17, v8

    .line 1407
    :cond_a
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    move/from16 v25, v0

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    sub-float v26, v17, v26

    add-float v25, v25, v26

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1408
    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    .line 1409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    move-object/from16 v25, v0

    if-eqz v25, :cond_6

    .line 1410
    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v25, v0

    div-int v13, v25, v20

    .line 1411
    .local v13, "position":I
    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v25, v0

    rem-int v15, v25, v20

    .line 1412
    .local v15, "positionOffsetPixels":I
    int-to-float v0, v15

    move/from16 v25, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v14, v25, v26

    .line 1413
    .local v14, "positionOffset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v13, v14, v15}, Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    goto/16 :goto_1

    .line 1399
    .end local v13    # "position":I
    .end local v14    # "positionOffset":F
    .end local v15    # "positionOffsetPixels":I
    :cond_b
    cmpl-float v25, v17, v16

    if-lez v25, :cond_a

    .line 1400
    mul-int v25, v7, v20

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    cmpl-float v25, v16, v25

    if-nez v25, :cond_c

    .line 1401
    sub-float v12, v17, v16

    .line 1402
    .restart local v12    # "over":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v25, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v26, v12, v26

    invoke-virtual/range {v25 .. v26}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v9

    .line 1404
    .end local v12    # "over":F
    :cond_c
    move/from16 v17, v16

    goto :goto_2

    .line 1420
    .end local v5    # "deltaX":F
    .end local v7    # "lastItemIndex":I
    .end local v8    # "leftBound":F
    .end local v11    # "oldScrollX":F
    .end local v16    # "rightBound":F
    .end local v17    # "scrollX":F
    .end local v19    # "width":I
    .end local v20    # "widthWithMargin":I
    .end local v21    # "x":F
    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mIsBeingDragged:Z

    move/from16 v25, v0

    if-eqz v25, :cond_d

    .line 1421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    .line 1422
    .local v18, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v25, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMaximumVelocity:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1423
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mActivePointerId:I

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-static {v0, v1}, Landroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v25

    move/from16 v0, v25

    float-to-int v6, v0

    .line 1425
    .local v6, "initialVelocity":I
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    .line 1426
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    move/from16 v26, v0

    add-int v20, v25, v26

    .line 1427
    .restart local v20    # "widthWithMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v17

    .line 1428
    .local v17, "scrollX":I
    div-int v4, v17, v20

    .line 1429
    .local v4, "currentPage":I
    if-lez v6, :cond_e

    move v10, v4

    .line 1430
    .local v10, "nextPage":I
    :goto_3
    const/16 v25, 0x1

    const/16 v26, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v10, v1, v2, v6}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZI)V

    .line 1432
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mActivePointerId:I

    .line 1433
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->endDrag()V

    .line 1434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v26

    or-int v9, v25, v26

    .line 1436
    .end local v4    # "currentPage":I
    .end local v6    # "initialVelocity":I
    .end local v10    # "nextPage":I
    .end local v17    # "scrollX":I
    .end local v18    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v20    # "widthWithMargin":I
    :cond_d
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 1429
    .restart local v4    # "currentPage":I
    .restart local v6    # "initialVelocity":I
    .restart local v17    # "scrollX":I
    .restart local v18    # "velocityTracker":Landroid/view/VelocityTracker;
    .restart local v20    # "widthWithMargin":I
    :cond_e
    add-int/lit8 v10, v4, 0x1

    goto :goto_3

    .line 1451
    .end local v4    # "currentPage":I
    .end local v6    # "initialVelocity":I
    .end local v17    # "scrollX":I
    .end local v18    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v20    # "widthWithMargin":I
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 1453
    .restart local v21    # "x":F
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1454
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 1459
    .end local v21    # "x":F
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->mLastMotionX:F

    .line 1460
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/easylauncher/CustomViewPager;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 1341
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method pageLeft()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1947
    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-lez v1, :cond_0

    .line 1948
    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(IZ)V

    .line 1951
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method pageRight()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1955
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 1956
    iget v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(IZ)V

    .line 1959
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method populate()V
    .locals 15

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x0

    .line 694
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-nez v10, :cond_1

    .line 811
    :cond_0
    return-void

    .line 702
    :cond_1
    iget-boolean v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    if-nez v10, :cond_0

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 715
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v10, p0}, Lcom/sec/android/app/easylauncher/PageAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 717
    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    .line 718
    .local v8, "pageLimit":I
    const/4 v10, 0x0

    iget v13, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    sub-int/2addr v13, v8

    invoke-static {v10, v13}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 719
    .local v9, "startPos":I
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v0

    .line 720
    .local v0, "N":I
    add-int/lit8 v10, v0, -0x1

    iget v13, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/2addr v13, v8

    invoke-static {v10, v13}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 726
    .local v4, "endPos":I
    const/4 v7, -0x1

    .line 728
    .local v7, "lastPos":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_6

    .line 729
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    .line 730
    .local v6, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    iget v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    if-lt v10, v9, :cond_2

    iget v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    if-le v10, v4, :cond_4

    :cond_2
    iget-boolean v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->scrolling:Z

    if-nez v10, :cond_4

    .line 733
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 734
    add-int/lit8 v5, v5, -0x1

    .line 735
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget v13, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget-object v14, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    invoke-virtual {v10, p0, v13, v14}, Lcom/sec/android/app/easylauncher/PageAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 752
    :cond_3
    iget v7, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    .line 728
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 736
    :cond_4
    if-ge v7, v4, :cond_3

    iget v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    if-le v10, v9, :cond_3

    .line 740
    add-int/lit8 v7, v7, 0x1

    .line 741
    if-ge v7, v9, :cond_5

    .line 742
    move v7, v9

    .line 744
    :cond_5
    :goto_1
    if-gt v7, v4, :cond_3

    iget v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    if-ge v7, v10, :cond_3

    .line 747
    invoke-virtual {p0, v7, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->addNewItem(II)V

    .line 748
    add-int/lit8 v7, v7, 0x1

    .line 749
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 756
    .end local v6    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_7

    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    iget-object v13, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    iget v7, v10, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    .line 757
    :goto_2
    if-ge v7, v4, :cond_9

    .line 758
    add-int/lit8 v7, v7, 0x1

    .line 759
    if-le v7, v9, :cond_8

    .line 760
    :goto_3
    if-gt v7, v4, :cond_9

    .line 763
    invoke-virtual {p0, v7, v12}, Lcom/sec/android/app/easylauncher/CustomViewPager;->addNewItem(II)V

    .line 764
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_7
    move v7, v12

    .line 756
    goto :goto_2

    :cond_8
    move v7, v9

    .line 759
    goto :goto_3

    .line 775
    :cond_9
    const/4 v2, 0x0

    .line 776
    .local v2, "curItem":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    const/4 v5, 0x0

    :goto_4
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_a

    .line 777
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    iget v10, v10, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v12, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v10, v12, :cond_e

    .line 778
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "curItem":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    check-cast v2, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    .line 782
    .restart local v2    # "curItem":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_a
    iget-object v12, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget v13, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-eqz v2, :cond_f

    iget-object v10, v2, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    :goto_5
    invoke-virtual {v12, p0, v13, v10}, Lcom/sec/android/app/easylauncher/PageAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 784
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v10, p0}, Lcom/sec/android/app/easylauncher/PageAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 786
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->hasFocus()Z

    move-result v10

    if-eqz v10, :cond_c

    .line 787
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->findFocus()Landroid/view/View;

    move-result-object v3

    .line 788
    .local v3, "currentFocused":Landroid/view/View;
    if-eqz v3, :cond_10

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForAnyChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v6

    .line 789
    .restart local v6    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :goto_6
    if-eqz v6, :cond_b

    iget v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v12, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-eq v10, v12, :cond_c

    .line 790
    :cond_b
    const/4 v5, 0x0

    :goto_7
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v10

    if-ge v5, v10, :cond_c

    .line 791
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 792
    .local v1, "child":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v6

    .line 793
    if-eqz v6, :cond_11

    iget v10, v6, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget v12, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v10, v12, :cond_11

    .line 794
    const/4 v10, 0x2

    invoke-virtual {v1, v10}, Landroid/view/View;->requestFocus(I)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 803
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "currentFocused":Landroid/view/View;
    .end local v6    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_c
    const/4 v5, 0x0

    :goto_8
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v10

    if-ge v5, v10, :cond_0

    .line 804
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 805
    .restart local v1    # "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v12, 0x7f02001b

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 806
    iget-boolean v10, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditMode:Z

    if-nez v10, :cond_d

    .line 807
    invoke-virtual {v1, v11}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 803
    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 776
    .end local v1    # "child":Landroid/view/View;
    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    :cond_f
    move-object v10, v11

    .line 782
    goto :goto_5

    .restart local v3    # "currentFocused":Landroid/view/View;
    :cond_10
    move-object v6, v11

    .line 788
    goto :goto_6

    .line 790
    .restart local v1    # "child":Landroid/view/View;
    .restart local v6    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_11
    add-int/lit8 v5, v5, 0x1

    goto :goto_7
.end method

.method public setAdapter(Lcom/sec/android/app/easylauncher/PageAdapter;)V
    .locals 7
    .param p1, "pageAdapter"    # Lcom/sec/android/app/easylauncher/PageAdapter;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v2, :cond_1

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/easylauncher/PageAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 329
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 330
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    .line 331
    .local v1, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget v3, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    iget-object v4, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->object:Ljava/lang/Object;

    invoke-virtual {v2, p0, v3, v4}, Lcom/sec/android/app/easylauncher/PageAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/easylauncher/PageAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->removeAllViews()V

    .line 336
    iput v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    .line 337
    invoke-virtual {p0, v5, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    .line 340
    .end local v0    # "i":I
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v2, :cond_3

    .line 343
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mObserver:Landroid/database/DataSetObserver;

    if-nez v2, :cond_2

    .line 344
    new-instance v2, Lcom/sec/android/app/easylauncher/CustomViewPager$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/CustomViewPager$3;-><init>(Lcom/sec/android/app/easylauncher/CustomViewPager;)V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mObserver:Landroid/database/DataSetObserver;

    .line 353
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 356
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    .line 357
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredCurItem:I

    if-ltz v2, :cond_4

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/easylauncher/PageAdapter;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 359
    iget v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredCurItem:I

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v5, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    .line 360
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredCurItem:I

    .line 361
    iput-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 362
    iput-object v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    .line 367
    :cond_3
    :goto_1
    return-void

    .line 364
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    goto :goto_1
.end method

.method public setCurrentItem(I)V
    .locals 2
    .param p1, "item"    # I

    .prologue
    const/4 v1, 0x0

    .line 381
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    .line 382
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFirstLayout:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    .line 383
    return-void

    :cond_0
    move v0, v1

    .line 382
    goto :goto_0
.end method

.method public setCurrentItem(IZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    const/4 v0, 0x0

    .line 393
    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPopulatePending:Z

    .line 394
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZ)V

    .line 395
    return-void
.end method

.method setCurrentItemInternal(IZZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z
    .param p3, "always"    # Z

    .prologue
    .line 402
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItemInternal(IZZI)V

    .line 403
    return-void
.end method

.method setCurrentItemInternal(IZZI)V
    .locals 7
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z
    .param p3, "always"    # Z
    .param p4, "velocity"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 406
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v4

    if-gtz v4, :cond_2

    .line 407
    :cond_0
    invoke-direct {p0, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    .line 445
    :cond_1
    :goto_0
    return-void

    .line 410
    :cond_2
    if-nez p3, :cond_3

    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-ne v4, p1, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_3

    .line 411
    invoke-direct {p0, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_0

    .line 415
    :cond_3
    if-gez p1, :cond_6

    .line 416
    const/4 p1, 0x0

    .line 420
    :cond_4
    :goto_1
    iget v3, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    .line 421
    .local v3, "pageLimit":I
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    add-int/2addr v4, v3

    if-gt p1, v4, :cond_5

    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    sub-int/2addr v4, v3

    if-ge p1, v4, :cond_7

    .line 425
    :cond_5
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_7

    .line 426
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    iput-boolean v1, v4, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->scrolling:Z

    .line 425
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 417
    .end local v2    # "i":I
    .end local v3    # "pageLimit":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v4

    if-lt p1, v4, :cond_4

    .line 418
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v4

    add-int/lit8 p1, v4, -0x1

    goto :goto_1

    .line 429
    .restart local v3    # "pageLimit":I
    :cond_7
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    if-eq v4, p1, :cond_8

    .line 430
    .local v1, "dispatchSelected":Z
    :goto_3
    iput p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mCurItem:I

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v4

    iget v6, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v4, v6

    mul-int v0, v4, p1

    .line 433
    .local v0, "destX":I
    if-eqz p2, :cond_9

    .line 434
    invoke-virtual {p0, v0, v5, p4}, Lcom/sec/android/app/easylauncher/CustomViewPager;->smoothScrollTo(III)V

    .line 435
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    if-eqz v4, :cond_1

    .line 436
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    invoke-interface {v4, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;->onPageSelected(I)V

    goto :goto_0

    .end local v0    # "destX":I
    .end local v1    # "dispatchSelected":Z
    :cond_8
    move v1, v5

    .line 429
    goto :goto_3

    .line 439
    .restart local v0    # "destX":I
    .restart local v1    # "dispatchSelected":Z
    :cond_9
    if-eqz v1, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    if-eqz v4, :cond_a

    .line 440
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    invoke-interface {v4, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 442
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->completeScroll()V

    .line 443
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->scrollTo(II)V

    goto :goto_0
.end method

.method public setEditMode(Z)V
    .locals 1
    .param p1, "edit"    # Z

    .prologue
    .line 2101
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditMode:Z

    .line 2103
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditMode:Z

    if-eqz v0, :cond_0

    .line 2104
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mEditModePageMargin:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageMargin(I)V

    .line 2107
    :goto_0
    return-void

    .line 2106
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageMargin(I)V

    goto :goto_0
.end method

.method public setOffscreenPageLimit(I)V
    .locals 4
    .param p1, "limit"    # I

    .prologue
    const/4 v3, 0x2

    .line 483
    if-ge p1, v3, :cond_0

    .line 484
    const-string v0, "EasyLauncher.CustomViewPager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requested offscreen page limit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too small; defaulting to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    const/4 p1, 0x2

    .line 488
    :cond_0
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    if-eq p1, v0, :cond_1

    .line 489
    iput p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOffscreenPageLimit:I

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->populate()V

    .line 492
    :cond_1
    return-void
.end method

.method public setOnPageChangeListener(Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    .prologue
    .line 448
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mOnPageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    .line 449
    return-void
.end method

.method public setPageMargin(I)V
    .locals 2
    .param p1, "marginPixels"    # I

    .prologue
    .line 503
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    .line 504
    .local v0, "oldMargin":I
    iput p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v1

    .line 507
    .local v1, "width":I
    invoke-direct {p0, v1, v1, p1, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->recomputeScrollPosition(IIII)V

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->requestLayout()V

    .line 510
    return-void
.end method

.method public setPageMarginDrawable(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 541
    return-void
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    .line 528
    if-eqz p1, :cond_0

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->refreshDrawableState()V

    .line 530
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setWillNotDraw(Z)V

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    .line 532
    return-void

    .line 530
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPageScrollEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 2083
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrollingEnabled:Z

    .line 2084
    return-void
.end method

.method smoothScrollTo(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 577
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->smoothScrollTo(III)V

    .line 578
    return-void
.end method

.method smoothScrollTo(III)V
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "velocity"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 589
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 591
    invoke-direct {p0, v7}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    .line 621
    :goto_0
    return-void

    .line 594
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollX()I

    move-result v1

    .line 595
    .local v1, "sx":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getScrollY()I

    move-result v2

    .line 596
    .local v2, "sy":I
    sub-int v3, p1, v1

    .line 597
    .local v3, "dx":I
    sub-int v4, p2, v2

    .line 598
    .local v4, "dy":I
    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 599
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->completeScroll()V

    .line 600
    invoke-direct {p0, v7}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollState(I)V

    goto :goto_0

    .line 604
    :cond_1
    invoke-direct {p0, v8}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollingCacheEnabled(Z)V

    .line 605
    iput-boolean v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScrolling:Z

    .line 606
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setScrollState(I)V

    .line 608
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getWidth()I

    move-result v7

    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageMargin:I

    add-int/2addr v7, v8

    int-to-float v7, v7

    div-float v6, v0, v7

    .line 609
    .local v6, "pageDelta":F
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, v6

    float-to-int v5, v0

    .line 611
    .local v5, "duration":I
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    .line 612
    if-lez p3, :cond_2

    .line 613
    int-to-float v0, v5

    int-to-float v7, v5

    int-to-float v8, p3

    iget v9, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mBaseLineFlingVelocity:F

    div-float/2addr v8, v9

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mFlingVelocityInfluence:F

    mul-float/2addr v7, v8

    add-float/2addr v0, v7

    float-to-int v5, v0

    .line 617
    :goto_1
    const/16 v0, 0x258

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 620
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    goto :goto_0

    .line 615
    :cond_2
    add-int/lit8 v5, v5, 0x64

    goto :goto_1
.end method

.method transformPage(Landroid/view/View;ZI)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "trans"    # Z
    .param p3, "index"    # I

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v8, 0x0

    .line 1610
    if-eqz p2, :cond_1

    .line 1611
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 1612
    .local v3, "pageWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 1614
    .local v2, "pageHeight":I
    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationX(F)V

    .line 1615
    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationY(F)V

    .line 1617
    int-to-float v4, v3

    mul-float/2addr v4, v6

    int-to-float v5, v3

    mul-float/2addr v5, v6

    int-to-float v6, p3

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->getChildCount()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/easylauncher/CustomViewPager;->mix(FFF)F

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setPivotX(F)V

    .line 1618
    int-to-float v4, v2

    const/high16 v5, 0x40a00000    # 5.0f

    div-float/2addr v4, v5

    invoke-virtual {p1, v4}, Landroid/view/View;->setPivotY(F)V

    .line 1620
    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    .line 1622
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageZoom:F

    invoke-virtual {p1, v4}, Landroid/view/View;->setScaleX(F)V

    .line 1623
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageZoom:F

    invoke-virtual {p1, v4}, Landroid/view/View;->setScaleY(F)V

    .line 1625
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->infoForChild(Landroid/view/View;)Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;

    move-result-object v1

    .line 1626
    .local v1, "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1627
    .local v0, "alpha":F
    if-eqz v1, :cond_0

    .line 1628
    const-string v4, "EasyLauncher.CustomViewPager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " transformPage ii.position : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget v5, v1, Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;->position:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/easylauncher/PageAdapter;->isScreenShowing(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1632
    iget v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageBackgroundAlpha:F

    .line 1636
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1650
    .end local v0    # "alpha":F
    .end local v1    # "ii":Lcom/sec/android/app/easylauncher/CustomViewPager$ItemInfo;
    .end local v2    # "pageHeight":I
    .end local v3    # "pageWidth":I
    :goto_0
    return-void

    .line 1641
    :cond_1
    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotX(F)V

    .line 1642
    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotY(F)V

    .line 1644
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageNone:F

    invoke-virtual {p1, v4}, Landroid/view/View;->setScaleX(F)V

    .line 1645
    iget v4, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mPageNone:F

    invoke-virtual {p1, v4}, Landroid/view/View;->setScaleY(F)V

    .line 1647
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p1, v4}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 545
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/CustomViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
