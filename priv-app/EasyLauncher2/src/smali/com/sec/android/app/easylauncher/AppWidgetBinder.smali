.class public Lcom/sec/android/app/easylauncher/AppWidgetBinder;
.super Ljava/lang/Object;
.source "AppWidgetBinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;
    }
.end annotation


# static fields
.field private static final REQUEST_APPWIDGET_BIND:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AppWidgetBinder"


# instance fields
.field private mHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

.field private mLauncher:Lcom/sec/android/app/easylauncher/Launcher;

.field private mManager:Landroid/appwidget/AppWidgetManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/easylauncher/Launcher;Landroid/appwidget/AppWidgetManager;Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;)V
    .locals 0
    .param p1, "launcher"    # Lcom/sec/android/app/easylauncher/Launcher;
    .param p2, "manager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "host"    # Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mLauncher:Lcom/sec/android/app/easylauncher/Launcher;

    .line 52
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mManager:Landroid/appwidget/AppWidgetManager;

    .line 53
    iput-object p3, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/AppWidgetBinder;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;
    .param p2, "x2"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->onBindSuccessful(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/AppWidgetBinder;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;
    .param p2, "x2"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->onBindFailed(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V

    return-void
.end method

.method private onBindFailed(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;
    .param p2, "widgetId"    # I

    .prologue
    .line 110
    if-eqz p1, :cond_0

    .line 111
    invoke-interface {p1}, Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;->onBindFail()V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;->deleteAppWidgetId(I)V

    .line 113
    return-void
.end method

.method private onBindSuccessful(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;
    .param p2, "widgetId"    # I

    .prologue
    .line 100
    if-eqz p1, :cond_0

    .line 101
    invoke-interface {p1, p2}, Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;->onBindSuccess(I)V

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public allocateBindNew(Landroid/content/ComponentName;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;)Z
    .locals 5
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "callback"    # Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;

    .prologue
    .line 63
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;->allocateAppWidgetId()I

    move-result v1

    .line 64
    .local v1, "widgetId":I
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v2, v1, p1}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->onBindSuccessful(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V

    .line 66
    const/4 v2, 0x1

    .line 90
    :goto_0
    return v2

    .line 70
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.appwidget.action.APPWIDGET_BIND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 72
    const-string v2, "appWidgetProvider"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mLauncher:Lcom/sec/android/app/easylauncher/Launcher;

    if-eqz v2, :cond_1

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->mLauncher:Lcom/sec/android/app/easylauncher/Launcher;

    const/4 v3, 0x2

    new-instance v4, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;

    invoke-direct {v4, p0, p2, v1}, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;-><init>(Lcom/sec/android/app/easylauncher/AppWidgetBinder;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V

    invoke-virtual {v2, v0, v3, v4}, Lcom/sec/android/app/easylauncher/Launcher;->startActivityForResult(Landroid/content/Intent;ILcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;)V

    .line 90
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 88
    :cond_1
    const-string v2, "AppWidgetBinder"

    const-string v3, "AppWidgetBinder.allocateBindNew: launcher == null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
