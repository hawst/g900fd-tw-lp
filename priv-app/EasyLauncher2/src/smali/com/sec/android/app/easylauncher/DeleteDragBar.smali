.class public Lcom/sec/android/app/easylauncher/DeleteDragBar;
.super Landroid/widget/FrameLayout;
.source "DeleteDragBar.java"


# static fields
.field private static sTempRect:Landroid/graphics/Rect;


# instance fields
.field private mDeleteTextView:Landroid/widget/TextView;

.field public onContentDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/DeleteDragBar;->sTempRect:Landroid/graphics/Rect;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    new-instance v0, Lcom/sec/android/app/easylauncher/DeleteDragBar$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/DeleteDragBar$1;-><init>(Lcom/sec/android/app/easylauncher/DeleteDragBar;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DeleteDragBar;->onContentDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/DeleteDragBar;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/DeleteDragBar;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setStates(ZZZ)V

    return-void
.end method

.method private setStates(ZZZ)V
    .locals 1
    .param p1, "enabled"    # Z
    .param p2, "selected"    # Z
    .param p3, "activated"    # Z

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setEnabled(Z)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DeleteDragBar;->mDeleteTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 67
    invoke-virtual {p0, p2}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setSelected(Z)V

    .line 68
    invoke-virtual {p0, p3}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setActivated(Z)V

    .line 69
    return-void
.end method


# virtual methods
.method public checkOver(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/easylauncher/DeleteDragBar;->sTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 54
    sget-object v0, Lcom/sec/android/app/easylauncher/DeleteDragBar;->sTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x1

    .line 58
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 48
    const v0, 0x7f0c0035

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DeleteDragBar;->mDeleteTextView:Landroid/widget/TextView;

    .line 49
    return-void
.end method

.method public resetTrashCan()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setStates(ZZZ)V

    .line 77
    return-void
.end method
