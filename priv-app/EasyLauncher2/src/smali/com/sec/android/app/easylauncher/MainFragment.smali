.class public Lcom/sec/android/app/easylauncher/MainFragment;
.super Landroid/support/v4/app/Fragment;
.source "MainFragment.java"


# static fields
.field public static final API_LEVEL_CHECK:I = 0x10

.field private static final DEBUGGABLE:Z

.field private static final REQUEST:I = 0x64

.field private static final REQUEST_WIDGET:I = 0x65

.field protected static final RESULT_LOAD_IMAGE:I = 0x0

.field protected static final RESULT_LOAD_MULTIPLE_IMAGE:I = 0x1

.field protected static final RESULT_WIDGET_ALLOW:I = 0x3


# instance fields
.field private final TAG:Ljava/lang/String;

.field favAppClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private isDialogOpen:Z

.field longClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mActivity:Landroid/app/Activity;

.field mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

.field mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

.field mAppsGrid:Landroid/widget/GridView;

.field mAppsloaderHandler:Landroid/os/Handler;

.field mAssistiveLightWidget:Landroid/view/View;

.field mAssistiveLightWidgetIndex:I

.field mAssistiveLightWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

.field mBlankView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field mCurrentPage:I

.field public mDefaultApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

.field mDummyText:Landroid/widget/TextView;

.field mEditDialog:Landroid/app/AlertDialog;

.field mEditPosition:I

.field private mErrorView:Landroid/view/View;

.field mMainView:Landroid/view/View;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field mSettingsInfo:Lcom/sec/android/app/easylauncher/lib/AppInfo;

.field mSpanX:I

.field mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

.field mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

.field private onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

.field private onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

.field widgetAddListener:Landroid/view/View$OnClickListener;

.field widgetListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 77
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/MainFragment;->DEBUGGABLE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 67
    const-string v0, "EasyLauncher.MainFragment"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->TAG:Ljava/lang/String;

    .line 98
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    .line 106
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidgetIndex:I

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    .line 240
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$3;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 288
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$5;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsloaderHandler:Landroid/os/Handler;

    .line 420
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$7;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->longClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 453
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$8;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    .line 472
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$9;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    .line 497
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$10;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->favAppClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 543
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$11;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->widgetListener:Landroid/view/View$OnClickListener;

    .line 557
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$12;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->widgetAddListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 65
    sget-boolean v0, Lcom/sec/android/app/easylauncher/MainFragment;->DEBUGGABLE:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/MainFragment;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/MainFragment;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/easylauncher/MainFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/easylauncher/MainFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->removeShortCutDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->showAppsList()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->removeWidgetDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->showWidgetList()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/easylauncher/MainFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/easylauncher/MainFragment;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MainFragment;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method private addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V
    .locals 8
    .param p1, "WidgetInfo"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .param p2, "widgetFrameLayout"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 339
    const/4 v0, 0x0

    .line 340
    .local v0, "hostView":Landroid/appwidget/AppWidgetHostView;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v4, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 341
    .local v1, "layout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 342
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 344
    .local v2, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_0

    .line 345
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getHostView()Landroid/appwidget/AppWidgetHostView;

    move-result-object v0

    .line 347
    :cond_0
    if-nez v0, :cond_2

    .line 348
    if-eqz p1, :cond_1

    .line 349
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/easylauncher/MainFragment;->convertDpToPixel(FLandroid/content/Context;)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v4

    const v5, 0x7f03000c

    invoke-static {v4, v5, v7}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mErrorView:Landroid/view/View;

    .line 358
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mErrorView:Landroid/view/View;

    invoke-virtual {v1, v4, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 375
    :goto_1
    return-void

    .line 354
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v4

    const v5, 0x7f03000b

    invoke-static {v4, v5, v7}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mErrorView:Landroid/view/View;

    goto :goto_0

    .line 362
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getDefaultPadding()Landroid/graphics/Rect;

    move-result-object v3

    .line 364
    .local v3, "r":Landroid/graphics/Rect;
    iget v4, v3, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 365
    iget v4, v3, Landroid/graphics/Rect;->right:I

    neg-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 366
    iget v4, v3, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 367
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    neg-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 368
    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetHostView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 370
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public static convertDpToPixel(FLandroid/content/Context;)F
    .locals 5
    .param p0, "dp"    # F
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 378
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 379
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 380
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    const/high16 v4, 0x43200000    # 160.0f

    div-float/2addr v3, v4

    mul-float v1, p0, v3

    .line 381
    .local v1, "px":F
    return v1
.end method

.method private removeShortCutDialog()V
    .locals 3

    .prologue
    .line 590
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 591
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 592
    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 593
    const v1, 0x7f09001c

    new-instance v2, Lcom/sec/android/app/easylauncher/MainFragment$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/MainFragment$13;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 606
    const v1, 0x7f09001b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/easylauncher/MainFragment$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/MainFragment$14;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 614
    new-instance v1, Lcom/sec/android/app/easylauncher/MainFragment$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/MainFragment$15;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 622
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 624
    return-void
.end method

.method private removeWidgetDialog()V
    .locals 3

    .prologue
    .line 627
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 628
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 629
    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 630
    const v1, 0x7f09001c

    new-instance v2, Lcom/sec/android/app/easylauncher/MainFragment$16;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/MainFragment$16;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 644
    const v1, 0x7f09001b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/easylauncher/MainFragment$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/easylauncher/MainFragment$17;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 652
    new-instance v1, Lcom/sec/android/app/easylauncher/MainFragment$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/MainFragment$18;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 660
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    .line 661
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 662
    return-void
.end method

.method private showAppsList()V
    .locals 3

    .prologue
    .line 762
    sget-boolean v1, Lcom/sec/android/app/easylauncher/MainFragment;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 763
    const-string v1, "EasyLauncher.MainFragment"

    const-string v2, "Show apps list "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->getMoreAppsMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 781
    :goto_0
    return-void

    .line 770
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/Launcher;->setMoreAppsMode(Z)V

    .line 772
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 774
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 775
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 777
    const-string v1, "EditPosition"

    iget v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 778
    const-string v1, "ScreenPosion"

    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 779
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private showWidgetList()V
    .locals 3

    .prologue
    .line 785
    sget-boolean v1, Lcom/sec/android/app/easylauncher/MainFragment;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 786
    const-string v1, "EasyLauncher.MainFragment"

    const-string v2, "Show widget list "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 789
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 791
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 792
    const-string v1, "EditPosition"

    iget v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 793
    const-string v1, "ScreenPosion"

    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 794
    const-string v1, "WidgetSize"

    iget v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mSpanX:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 796
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 798
    return-void
.end method

.method private unBind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 392
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    new-instance v1, Lcom/sec/android/app/easylauncher/MainFragment$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/MainFragment$6;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;->post(Ljava/lang/Runnable;)V

    .line 403
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z

    .line 405
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    .line 406
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    .line 407
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mErrorView:Landroid/view/View;

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 412
    return-void
.end method


# virtual methods
.method addWidget(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    if-nez v0, :cond_0

    .line 302
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mActivity:Landroid/app/Activity;

    .line 305
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidgets()V

    .line 306
    return-void
.end method

.method addWidgets()V
    .locals 8

    .prologue
    const v7, 0x7f0c0055

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 310
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    if-nez v3, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v3

    iget-object v0, v3, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .line 315
    .local v0, "localwidgetprovider":Lcom/sec/android/app/easylauncher/LocalWidgetProvider;
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    sget v5, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET:I

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v4

    aput-object v4, v3, v2

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    aget-object v3, v3, v2

    const v4, 0x7f0c004d

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    sget v5, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v4

    aput-object v4, v3, v1

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    aget-object v3, v3, v1

    const v4, 0x7f0c004f

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    sget v5, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET:I

    add-int/lit8 v5, v5, 0x2

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v4

    aput-object v4, v3, v6

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    aget-object v3, v3, v6

    const v4, 0x7f0c0051

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v1, :cond_2

    .line 326
    .local v1, "supprotWidget":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 327
    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    sget v3, Lcom/sec/android/app/easylauncher/EasyController;->START_POSITION_WIDGET:I

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-direct {p0, v2, v7}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V

    .line 331
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v2, :cond_0

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    .line 333
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iget v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidgetIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->setWidgetIndex(I)V

    goto/16 :goto_0

    .end local v1    # "supprotWidget":Z
    :cond_2
    move v1, v2

    .line 324
    goto :goto_1
.end method

.method dismisPopUps()V
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 802
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 804
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditDialog:Landroid/app/AlertDialog;

    .line 807
    :cond_0
    return-void
.end method

.method public getEditMode()Z
    .locals 1

    .prologue
    .line 741
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v0

    return v0
.end method

.method getLauncher()Lcom/sec/android/app/easylauncher/Launcher;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    .line 224
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    goto :goto_0
.end method

.method public getWidgetProvider()Lcom/sec/android/app/easylauncher/LocalWidgetProvider;
    .locals 1

    .prologue
    .line 745
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 707
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 708
    packed-switch p1, :pswitch_data_0

    .line 725
    :cond_0
    :goto_0
    return-void

    .line 710
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/Launcher;->sayByTalkback(Ljava/lang/String;)V

    .line 712
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->startloading()V

    goto :goto_0

    .line 716
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getWidgetProvider()Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    move-result-object v1

    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    iget v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidgetInfoFromDb(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v0

    .line 717
    .local v0, "widgetInfo":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    if-eqz v0, :cond_1

    .line 718
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getWidgetProvider()Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 719
    const-string v1, "EasyLauncher.MainFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REQUEST_WIDGET widgetInfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidgets()V

    goto :goto_0

    .line 708
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 213
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 214
    const-string v0, "EasyLauncher.MainFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----------onAttach : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mActivity:Landroid/app/Activity;

    .line 216
    return-void
.end method

.method public onBackPressed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 749
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    if-nez v0, :cond_0

    .line 750
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mActivity:Landroid/app/Activity;

    .line 753
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 755
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->refreshView()V

    .line 758
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/easylauncher/lib/Lib;->getSettingsInfo(Landroid/content/Context;)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mSettingsInfo:Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mDefaultApps:Ljava/util/ArrayList;

    .line 133
    new-instance v1, Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mDefaultApps:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, p0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/support/v4/app/Fragment;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    .line 135
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mDefaultApps:Ljava/util/ArrayList;

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->CENTER_FRAGMENT_APPS_COUNT:I

    sget v5, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;-><init>(Landroid/content/Context;Ljava/util/ArrayList;II)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    .line 139
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 140
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 142
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/Context;->setTheme(I)V

    .line 146
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 151
    const v0, 0x7f030017

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mContext:Landroid/content/Context;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    const v1, 0x7f0c0027

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mBlankView:Landroid/view/View;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    const v1, 0x7f0c004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setHapticFeedbackEnabled(Z)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->favAppClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/easylauncher/Launcher;->mUseLongClickRemove:Z

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->longClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/easylauncher/MainFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/MainFragment$1;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/easylauncher/MainFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/MainFragment$2;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidgets()V

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->startloading()V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 416
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->unBind()V

    .line 417
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 418
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 386
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 388
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 665
    sparse-switch p1, :sswitch_data_0

    .line 699
    const/4 v0, 0x0

    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 672
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 673
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 674
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->requestFocus()Z

    .line 676
    const/16 v1, 0x16

    if-ne p1, v1, :cond_2

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_0

    .line 680
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_0

    .line 686
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 687
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 688
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mBlankView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 689
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mBlankView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 691
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 693
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->clearFocus()V

    goto :goto_0

    .line 665
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
        0x16 -> :sswitch_1
        0x52 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 231
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isLongClickEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/MainFragment;->setupLongClickView(IZ)V

    .line 237
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 238
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 730
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 732
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->setMoreAppsMode(Z)V

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 738
    :cond_1
    return-void
.end method

.method public refreshView()V
    .locals 1

    .prologue
    .line 811
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 813
    :cond_0
    return-void
.end method

.method setWidgetInfo(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 566
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 587
    :goto_0
    :pswitch_0
    return-void

    .line 568
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 569
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    .line 570
    iput v2, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mSpanX:I

    goto :goto_0

    .line 574
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 575
    const/16 v0, 0x65

    iput v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    .line 576
    iput v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mSpanX:I

    goto :goto_0

    .line 580
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfoList:[Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 581
    const/16 v0, 0x66

    iput v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    .line 582
    iput v1, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mSpanX:I

    goto :goto_0

    .line 566
    :pswitch_data_0
    .packed-switch 0x7f0c004d
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method setupLongClickView(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "longClickMode"    # Z

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->setDragIndex(I)V

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 493
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForLongClickMode(Z)V

    .line 494
    return-void
.end method

.method startloading()V
    .locals 2

    .prologue
    .line 274
    new-instance v0, Lcom/sec/android/app/easylauncher/MainFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MainFragment$4;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    .line 284
    .local v0, "runnable":Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 285
    .local v1, "widgetloading":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 286
    return-void
.end method

.method public updateBadge()V
    .locals 2

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/easylauncher/MainFragment$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/MainFragment$19;-><init>(Lcom/sec/android/app/easylauncher/MainFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 826
    :cond_0
    return-void
.end method
