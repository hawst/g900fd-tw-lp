.class public Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MenuWidgetListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WidgetListAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 372
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 380
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->mContext:Landroid/content/Context;

    .line 381
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 364
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->getItem(I)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 401
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 407
    if-nez p2, :cond_0

    .line 408
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->mContext:Landroid/content/Context;

    const-string v14, "layout_inflater"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    const v14, 0x7f030019

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v13, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 413
    :cond_0
    const/4 v3, 0x0

    .line 415
    .local v3, "dr":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v13, v13, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 416
    .local v7, "item":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    if-eqz v7, :cond_2

    .line 417
    const v13, 0x7f0c0024

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 418
    .local v5, "imageView":Landroid/widget/ImageView;
    const v13, 0x7f0c0026

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 420
    .local v11, "textView":Landroid/widget/TextView;
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    if-nez v13, :cond_1

    .line 421
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 422
    .local v9, "packageName":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "className":Ljava/lang/String;
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, v9, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    .local v2, "componentName":Landroid/content/ComponentName;
    new-instance v6, Landroid/content/pm/ActivityInfo;

    invoke-direct {v6}, Landroid/content/pm/ActivityInfo;-><init>()V

    .line 429
    .local v6, "info":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 445
    .local v8, "mPm":Landroid/content/pm/PackageManager;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->mContext:Landroid/content/Context;

    const/4 v14, 0x3

    invoke-virtual {v13, v9, v14}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v12

    .line 447
    .local v12, "widgetContext":Landroid/content/Context;
    if-eqz v12, :cond_3

    .line 448
    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 449
    .local v10, "res":Landroid/content/res/Resources;
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetPreview()I

    move-result v13

    invoke-virtual {v7, v10, v13}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetIcon(Landroid/content/res/Resources;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    .end local v1    # "className":Ljava/lang/String;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v6    # "info":Landroid/content/pm/ActivityInfo;
    .end local v8    # "mPm":Landroid/content/pm/PackageManager;
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "res":Landroid/content/res/Resources;
    .end local v12    # "widgetContext":Landroid/content/Context;
    :cond_1
    :goto_0
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    invoke-virtual {v5, v13}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460
    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetLabel()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    .end local v5    # "imageView":Landroid/widget/ImageView;
    .end local v11    # "textView":Landroid/widget/TextView;
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 465
    return-object p2

    .line 451
    .restart local v1    # "className":Ljava/lang/String;
    .restart local v2    # "componentName":Landroid/content/ComponentName;
    .restart local v5    # "imageView":Landroid/widget/ImageView;
    .restart local v6    # "info":Landroid/content/pm/ActivityInfo;
    .restart local v8    # "mPm":Landroid/content/pm/PackageManager;
    .restart local v9    # "packageName":Ljava/lang/String;
    .restart local v11    # "textView":Landroid/widget/TextView;
    .restart local v12    # "widgetContext":Landroid/content/Context;
    :cond_3
    :try_start_1
    const-string v13, "EasyLauncher.MenuWidgetListActivity"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "loadWidgetPreview() : Unable to find package "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 453
    .end local v12    # "widgetContext":Landroid/content/Context;
    :catch_0
    move-exception v4

    .line 454
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v13, "EasyLauncher.MenuWidgetListActivity"

    const-string v14, "loadWidgetPreview() : Unable to find package"

    invoke-static {v13, v14, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 386
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 387
    return-void
.end method

.method removeData(I)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 470
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 472
    .local v0, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    return-object v0
.end method

.method public unBind()V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 377
    :cond_0
    return-void
.end method
