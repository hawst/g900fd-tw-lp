.class Lcom/sec/android/app/easylauncher/DynamicShadowMixin;
.super Ljava/lang/Object;
.source "DynamicShadowMixin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/DynamicShadowMixin$Holder;
    }
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mDestRect:Landroid/graphics/Rect;

.field private mPaint:Landroid/graphics/Paint;

.field private mShadowBitmapDirty:Z

.field private mSourceRect:Landroid/graphics/Rect;

.field private mView:Landroid/view/View;

.field private mViewToInvalidateOnShadowDirty:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-string v0, "drawglfunction"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mSourceRect:Landroid/graphics/Rect;

    .line 15
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mDestRect:Landroid/graphics/Rect;

    .line 17
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mPaint:Landroid/graphics/Paint;

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mShadowBitmapDirty:Z

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mViewToInvalidateOnShadowDirty:Landroid/view/View;

    .line 25
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mView:Landroid/view/View;

    .line 33
    return-void
.end method

.method private align4(I)I
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 82
    add-int/lit8 v0, p1, 0x3

    and-int/lit8 v0, v0, -0x4

    return v0
.end method

.method public static native nGaussianBlur(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Z
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 36
    iget-object v13, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mView:Landroid/view/View;

    .line 37
    .local v13, "v":Landroid/view/View;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Landroid/graphics/Canvas$EdgeType;->AA:Landroid/graphics/Canvas$EdgeType;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->quickReject(FFFFLandroid/graphics/Canvas$EdgeType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v6

    .line 41
    .local v6, "alpha":F
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mShadowBitmapDirty:Z

    if-eqz v0, :cond_3

    .line 42
    const/4 v8, 0x3

    .line 43
    .local v8, "border":I
    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x6

    invoke-direct {p0, v0}, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->align4(I)I

    move-result v10

    .line 44
    .local v10, "neededWidth":I
    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x6

    invoke-direct {p0, v0}, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->align4(I)I

    move-result v9

    .line 45
    .local v9, "neededHeight":I
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v10, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, v9, :cond_2

    .line 47
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v9, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    .line 48
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    .line 51
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    .line 52
    .local v7, "b":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 54
    const/high16 v12, 0x3f000000    # 0.5f

    .line 55
    .local v12, "scale":F
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, v8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v2, v8

    invoke-virtual {v0, v8, v8, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v13}, Landroid/view/View;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v13}, Landroid/view/View;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v8

    int-to-float v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0, v12, v12}, Landroid/graphics/Canvas;->scale(FF)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mView:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 67
    invoke-static {v7, v7}, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->nGaussianBlur(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Z

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mSourceRect:Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 70
    const/16 v11, 0x8

    .line 71
    .local v11, "offset":I
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mDestRect:Landroid/graphics/Rect;

    neg-int v1, v8

    mul-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x6

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v11

    add-int/lit8 v4, v4, 0x6

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mShadowBitmapDirty:Z

    .line 77
    .end local v7    # "b":Landroid/graphics/Bitmap;
    .end local v8    # "border":I
    .end local v9    # "neededHeight":I
    .end local v10    # "neededWidth":I
    .end local v11    # "offset":I
    .end local v12    # "scale":F
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x43160000    # 150.0f

    mul-float/2addr v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mSourceRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mDestRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mShadowBitmapDirty:Z

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mViewToInvalidateOnShadowDirty:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mViewToInvalidateOnShadowDirty:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 93
    :cond_0
    return-void
.end method

.method public setViewToInvalidate(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/DynamicShadowMixin;->mViewToInvalidateOnShadowDirty:Landroid/view/View;

    .line 101
    return-void
.end method
