.class Lcom/sec/android/app/easylauncher/MainFragment$16;
.super Ljava/lang/Object;
.source "MainFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/MainFragment;->removeWidgetDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 633
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/easylauncher/MainFragment;->isDialogOpen:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/easylauncher/MainFragment;->access$302(Lcom/sec/android/app/easylauncher/MainFragment;Z)Z

    .line 635
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v0

    .line 637
    .local v0, "widgetId":I
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/MainFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v2, v2, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->removeWidgetFromHomescreen(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/MainFragment;->getWidgetProvider()Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v2, v2, Lcom/sec/android/app/easylauncher/MainFragment;->mWidgetInfo:Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->deleteWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$16;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidgets()V

    .line 642
    return-void
.end method
