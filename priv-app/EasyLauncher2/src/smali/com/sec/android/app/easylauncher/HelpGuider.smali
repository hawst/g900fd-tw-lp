.class public Lcom/sec/android/app/easylauncher/HelpGuider;
.super Ljava/lang/Object;
.source "HelpGuider.java"


# instance fields
.field layout:Landroid/view/ViewGroup;

.field mActivity:Landroid/app/Activity;

.field private mGoalAnimationView:Landroid/view/View;

.field mIsHelpDialogVisible:Z

.field mMainView:Landroid/view/ViewGroup;

.field menuLayout:Landroid/view/View;

.field private wm:Landroid/view/WindowManager;

.field wmLayout:Landroid/view/View;

.field private wmParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "mainView"    # Landroid/view/ViewGroup;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    .line 51
    return-void
.end method


# virtual methods
.method public dismissShowHelpDialog()V
    .locals 3

    .prologue
    .line 331
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    if-eqz v1, :cond_1

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmLayout:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmLayout:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 334
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmLayout:Landroid/view/View;

    .line 342
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 345
    :cond_1
    return-void

    .line 337
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 338
    .local v0, "childcount":I
    if-eqz v0, :cond_0

    .line 339
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto :goto_0
.end method

.method public showHelpDialog_addapps(I)V
    .locals 1
    .param p1, "step"    # I

    .prologue
    .line 57
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addapps_step1()V

    .line 66
    :goto_0
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addapps_step2()V

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    goto :goto_0
.end method

.method public showHelpDialog_addapps_step1()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 73
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 76
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03000f

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 78
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 79
    .local v2, "pointer":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 81
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    .line 82
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 84
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 85
    .local v3, "textBox":Landroid/widget/RelativeLayout;
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider$1;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/app/easylauncher/HelpGuider$1;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->start()V

    .line 93
    const v4, 0x3f19999a    # 0.6f

    const v5, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v4, v5, v3}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 95
    iput-boolean v6, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 97
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "pointer":Landroid/widget/ImageView;
    .end local v3    # "textBox":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method public showHelpDialog_addapps_step2()V
    .locals 7

    .prologue
    const v6, 0x7f090048

    const/4 v5, 0x1

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 108
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030010

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v4, 0x7f0c002e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 111
    .local v2, "textBox":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    new-instance v3, Lcom/sec/android/app/easylauncher/HelpGuider$2;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/easylauncher/HelpGuider$2;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/TextView;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>()V

    .line 122
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    const v3, 0x3f19999a    # 0.6f

    const v4, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v3, v4, v2}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 124
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 126
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "textBox":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public showHelpDialog_addcontact_step1()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 295
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 298
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030011

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 300
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 301
    .local v2, "pointer":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 303
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    .line 304
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 306
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/easylauncher/HelpGuider$8;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->setOnImageTouchListener(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;)V

    .line 313
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 314
    .local v3, "textBox":Landroid/widget/RelativeLayout;
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider$9;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/app/easylauncher/HelpGuider$9;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->start()V

    .line 322
    const v4, 0x3f19999a    # 0.6f

    const v5, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v4, v5, v3}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 324
    iput-boolean v6, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 326
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "pointer":Landroid/widget/ImageView;
    .end local v3    # "textBox":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method public showHelpDialog_removeapps(I)V
    .locals 1
    .param p1, "step"    # I

    .prologue
    .line 132
    if-nez p1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_removeapps_step1_quickedit()V

    .line 147
    :goto_0
    return-void

    .line 135
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_removeapps_step1()V

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_removeapps_step2()V

    goto :goto_0

    .line 141
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_removeapps_step3()V

    goto :goto_0

    .line 145
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    goto :goto_0
.end method

.method public showHelpDialog_removeapps_step1()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const v6, 0x3f4ccccd    # 0.8f

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 208
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 211
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030012

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c0031

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 213
    .local v2, "pointer":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    .line 216
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 218
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 220
    .local v3, "textBox":Landroid/widget/RelativeLayout;
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider$5;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/app/easylauncher/HelpGuider$5;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->start()V

    .line 228
    invoke-virtual {v0, v6, v6, v3}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 230
    iput-boolean v7, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 233
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "pointer":Landroid/widget/ImageView;
    .end local v3    # "textBox":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method public showHelpDialog_removeapps_step1_quickedit()V
    .locals 7

    .prologue
    const/4 v6, -0x2

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 155
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    .line 156
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 157
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wm:Landroid/view/WindowManager;

    .line 159
    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v4}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    .line 160
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v5, 0x33

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 162
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 163
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v5, -0x3

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 164
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v5, 0x8

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 167
    const v4, 0x7f030013

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wm:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 171
    .local v2, "pointer":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    .line 174
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mGoalAnimationView:Landroid/view/View;

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 175
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/easylauncher/HelpGuider$3;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->setOnImageTouchListener(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;)V

    .line 185
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v5, 0x7f0c002e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 186
    .local v3, "textBox":Landroid/widget/RelativeLayout;
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider$4;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/app/easylauncher/HelpGuider$4;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->start()V

    .line 194
    const v4, 0x3f19999a    # 0.6f

    const v5, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v4, v5, v3}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 196
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->wmLayout:Landroid/view/View;

    .line 198
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 201
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "pointer":Landroid/widget/ImageView;
    .end local v3    # "textBox":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method public showHelpDialog_removeapps_step2()V
    .locals 7

    .prologue
    const v6, 0x7f09004c

    const/4 v5, 0x1

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 243
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030010

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 245
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v4, 0x7f0c002e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 246
    .local v2, "textBox":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 249
    new-instance v3, Lcom/sec/android/app/easylauncher/HelpGuider$6;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/easylauncher/HelpGuider$6;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/TextView;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>()V

    .line 257
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    const v3, 0x3f19999a    # 0.6f

    const v4, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v3, v4, v2}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 259
    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 262
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "textBox":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public showHelpDialog_removeapps_step3()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const v5, 0x3f4ccccd    # 0.8f

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mActivity:Landroid/app/Activity;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 271
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030014

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->menuLayout:Landroid/view/View;

    const v4, 0x7f0c002e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 275
    .local v2, "textBox":Landroid/widget/RelativeLayout;
    new-instance v3, Lcom/sec/android/app/easylauncher/HelpGuider$7;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/easylauncher/HelpGuider$7;-><init>(Lcom/sec/android/app/easylauncher/HelpGuider;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;-><init>()V

    .line 283
    .local v0, "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    invoke-virtual {v0, v5, v5, v2}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 285
    iput-boolean v6, p0, Lcom/sec/android/app/easylauncher/HelpGuider;->mIsHelpDialogVisible:Z

    .line 288
    .end local v0    # "animdialog":Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "textBox":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method
