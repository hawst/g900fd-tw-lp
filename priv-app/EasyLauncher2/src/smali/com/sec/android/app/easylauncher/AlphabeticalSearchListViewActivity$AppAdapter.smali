.class Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;
.super Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;
.source "AlphabeticalSearchListViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppAdapter"
.end annotation


# instance fields
.field mCategory:I

.field mContext:Landroid/content/Context;

.field mFilter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;

.field mSearchText:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

.field toShow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 468
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;-><init>()V

    .line 469
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;

    invoke-direct {v0, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mFilter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;

    .line 470
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mContext:Landroid/content/Context;

    .line 471
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    .line 472
    return-void
.end method

.method private displayHighlightedName(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 8
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "appName"    # Ljava/lang/String;

    .prologue
    .line 483
    const/4 v3, -0x1

    .line 484
    .local v3, "indexOf":I
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 486
    .local v1, "highlightStrLength":I
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 487
    .local v0, "highLightText":Landroid/text/Spannable;
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    invoke-static {v5, p2, v6}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v2

    .line 488
    .local v2, "iQueryForIndian":[C
    if-eqz v2, :cond_0

    .line 489
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([C)V

    .line 490
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 491
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 495
    .end local v4    # "s":Ljava/lang/String;
    :goto_0
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060002

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v6, v3, v1

    const/4 v7, 0x0

    invoke-interface {v0, v5, v3, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 496
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 497
    return-void

    .line 493
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 561
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parnet"    # Landroid/view/ViewGroup;

    .prologue
    .line 530
    if-nez p2, :cond_0

    .line 531
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 532
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030008

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 535
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v4, 0x7f0c0026

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 536
    .local v3, "title":Landroid/widget/TextView;
    const v4, 0x7f0c0024

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 537
    .local v1, "icon":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 539
    .local v0, "appInfo":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 541
    const/16 v4, 0x15

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 543
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 549
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 551
    return-object p2

    .line 545
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->displayHighlightedName(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method removeData(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 565
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 566
    .local v0, "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-super {p0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->notifyDataSetChanged()V

    .line 567
    return-object v0
.end method

.method public runFilter()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mFilter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;->filterWidgets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->notifyDataSetChanged()V

    .line 504
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Launcher_DisableFastScrollIndex"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mIndexView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$200(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 514
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mNoResultTextView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$300(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 523
    :cond_1
    :goto_1
    return-void

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mIndexView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$200(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mNoResultTextView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->access$300(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 0
    .param p1, "search"    # Ljava/lang/String;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    .line 480
    return-void
.end method

.method public unBind()V
    .locals 1

    .prologue
    .line 571
    invoke-super {p0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->unBind()V

    .line 573
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mFilter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;

    .line 574
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->mSearchText:Ljava/lang/String;

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 577
    :cond_0
    return-void
.end method
