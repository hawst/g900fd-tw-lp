.class public Lcom/sec/android/app/easylauncher/lib/AppDBUtils;
.super Ljava/lang/Object;
.source "AppDBUtils.java"


# static fields
.field private static final DEBUGGABLE:Z

.field static magnifierAppPackage:Ljava/lang/String;


# instance fields
.field private final TAG:Ljava/lang/String;

.field mAppCount:I

.field mAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field mAppPosition:I

.field public mAppShortcutCount:I

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 36
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    .line 47
    const-string v0, "com.sec.android.app.magnifier"

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->magnifierAppPackage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "EasyLauncher.AppUtils"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->TAG:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "appCount"    # I
    .param p4, "screen"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "EasyLauncher.AppUtils"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->TAG:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    .line 58
    iput p3, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    .line 59
    iput p4, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppPosition:I

    .line 61
    return-void
.end method

.method private loadCurrentApp(Lcom/sec/android/app/easylauncher/lib/AppInfo;)Z
    .locals 13
    .param p1, "newApp"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    const/4 v9, 0x0

    .line 112
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 113
    .local v6, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPosition()I

    move-result v7

    .line 115
    .local v7, "position":I
    const/4 v2, 0x0

    .line 117
    .local v2, "dr":Landroid/graphics/drawable/Drawable;
    iget v10, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    if-lt v7, v10, :cond_0

    .line 178
    :goto_0
    return v9

    .line 121
    :cond_0
    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    const-string v10, ""

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, ""

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 123
    :cond_1
    invoke-direct {p0, v7}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->setEmptyIcon(I)V

    goto :goto_0

    .line 127
    :cond_2
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v6, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .local v1, "componentName":Landroid/content/ComponentName;
    new-instance v4, Landroid/content/pm/ActivityInfo;

    invoke-direct {v4}, Landroid/content/pm/ActivityInfo;-><init>()V

    .line 133
    .local v4, "info":Landroid/content/pm/ActivityInfo;
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 137
    .local v5, "mPm":Landroid/content/pm/PackageManager;
    const/16 v10, 0x20

    :try_start_0
    invoke-virtual {v5, v1, v10}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v4

    .line 139
    invoke-virtual {v5, v1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v8

    .line 150
    .local v8, "res":Landroid/content/res/Resources;
    if-eqz v2, :cond_5

    .line 151
    invoke-virtual {p1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 152
    const/4 v2, 0x0

    .line 156
    :goto_1
    invoke-virtual {p1, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setComponentName(Landroid/content/ComponentName;)V

    .line 157
    invoke-virtual {v4, v5}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 159
    sget-object v10, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->magnifierAppPackage:Ljava/lang/String;

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 160
    const/4 v10, 0x0

    invoke-virtual {p1, v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setEditable(Z)V

    .line 163
    :cond_3
    sget-boolean v10, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    if-eqz v10, :cond_4

    .line 164
    const-string v10, "EasyLauncher.AppUtils"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " FavoriteItem: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPosition()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "newapp:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPosition()I

    move-result v11

    invoke-virtual {v10, v11, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 178
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 154
    :cond_5
    invoke-virtual {v4}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v10

    invoke-virtual {p1, v8, v10}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/content/res/Resources;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 169
    .end local v8    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v3

    .line 170
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-boolean v10, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    if-eqz v10, :cond_6

    .line 171
    const-string v10, "EasyLauncher.AppUtils"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Package Name Not Found for  : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_6
    invoke-direct {p0, v7}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->setEmptyIcon(I)V

    goto/16 :goto_0
.end method

.method private setEmptyIcon(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->getEmptyApp()Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 260
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->addEmptyEntryToDB(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 261
    return-void
.end method


# virtual methods
.method addEmptyEntryToDB(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 193
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-direct {v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>()V

    .line 195
    .local v1, "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 196
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 197
    iget v2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppPosition:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setScreen(I)V

    .line 198
    invoke-virtual {v1, p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPosition(I)V

    .line 200
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 202
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateAppInfo(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 204
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 207
    :cond_0
    return-object v1
.end method

.method public addWidgetToHomescreen(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V
    .locals 4
    .param p1, "widgetInfo"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .param p2, "screen"    # I

    .prologue
    .line 265
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 266
    const-string v1, "EasyLauncher.AppUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addWidgetToHomescreen  widgetInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 269
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    if-eqz v0, :cond_1

    .line 270
    invoke-virtual {p1, p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 271
    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I

    .line 273
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 276
    :cond_1
    return-void
.end method

.method getEmptyApp()Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 212
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    const-string v2, ""

    invoke-direct {v0, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>(Ljava/lang/String;)V

    .line 214
    .local v0, "empty":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 215
    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setComponentName(Landroid/content/ComponentName;)V

    .line 216
    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 219
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x7f02001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 224
    return-object v0
.end method

.method public loadFromFavorites()V
    .locals 10

    .prologue
    .line 65
    const/4 v1, 0x0

    .line 67
    .local v1, "i":I
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    if-nez v7, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 72
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 73
    .local v0, "easyProviderLocal":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v6, "userAppsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const-string v7, "favorites"

    iget v8, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppPosition:I

    iget v9, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    invoke-virtual {v0, v7, v8, v9}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getUserSelectedApps(Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v6

    .line 78
    const/4 v1, 0x0

    :goto_1
    iget v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    if-ge v1, v7, :cond_3

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->getEmptyApp()Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v5

    .line 82
    .local v5, "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    iget v8, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppPosition:I

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/easylauncher/EasyController;

    iget v7, v7, Lcom/sec/android/app/easylauncher/EasyController;->PAGE_APPS1:I

    if-lt v8, v7, :cond_2

    iget v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    add-int/lit8 v7, v7, -0x1

    if-ne v1, v7, :cond_2

    .line 83
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020010

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 84
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    const v8, 0x7f09000b

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "moreAppsName":Ljava/lang/String;
    invoke-virtual {v5, v4}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 87
    .end local v4    # "moreAppsName":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 90
    .end local v5    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_3
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    .line 92
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_0

    .line 96
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 97
    .local v3, "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-direct {p0, v3}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->loadCurrentApp(Lcom/sec/android/app/easylauncher/lib/AppInfo;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 98
    iget v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    goto :goto_2

    .line 102
    .end local v3    # "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_5
    const-string v7, "EasyLauncher.AppUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Apps count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppShortcutCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  mAppPosition : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppPosition:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    goto/16 :goto_0
.end method

.method public removeAppByPackageChanged(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 292
    sget-boolean v2, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    if-eqz v2, :cond_0

    .line 293
    const-string v2, "EasyLauncher.AppUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeAppByPackageChanged\tpackageName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 298
    .local v0, "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 299
    invoke-direct {p0, v1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->setEmptyIcon(I)V

    .line 303
    .end local v0    # "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_1
    return-void

    .line 295
    .restart local v0    # "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public removeFromFavorites(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 183
    sget-boolean v0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "EasyLauncher.AppUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Curent postion mEditPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/EasyController;->addAppToMap(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 187
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->setEmptyIcon(I)V

    .line 190
    return-void
.end method

.method public removeWidgetFromHomescreen(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 4
    .param p1, "widgetInfo"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 280
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 281
    const-string v1, "EasyLauncher.AppUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeWidgetFromHomescreen  widgetInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_0
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 284
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->deleteWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I

    .line 286
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 289
    :cond_1
    return-void
.end method

.method setEmptyApp()V
    .locals 5

    .prologue
    .line 229
    const/4 v2, -0x1

    .line 231
    .local v2, "lastIndex":I
    iget v3, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 234
    .local v0, "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const-string v3, ""

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 235
    move v2, v1

    .line 240
    .end local v0    # "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_0
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 241
    const/4 v2, 0x1

    .line 247
    :goto_1
    move v1, v2

    :goto_2
    iget v3, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_3

    .line 248
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 250
    .restart local v0    # "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const-string v3, "empty"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 247
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 231
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 244
    .end local v0    # "app":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x2

    goto :goto_1

    .line 254
    :cond_3
    return-void
.end method
