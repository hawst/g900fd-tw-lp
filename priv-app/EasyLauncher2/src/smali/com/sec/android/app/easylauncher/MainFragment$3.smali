.class Lcom/sec/android/app/easylauncher/MainFragment$3;
.super Landroid/content/BroadcastReceiver;
.source "MainFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/MainFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$3;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    .line 245
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "details":Ljava/lang/String;
    const/4 v2, 0x0

    .line 247
    .local v2, "packagename":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 248
    .local v3, "removePackage":Z
    const/4 v1, 0x0

    .line 250
    .local v1, "matches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    .line 251
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 253
    :cond_0
    # getter for: Lcom/sec/android/app/easylauncher/MainFragment;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/MainFragment;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 254
    const-string v4, "EasyLauncher.MainFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mReceiver : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :cond_1
    if-eqz v2, :cond_4

    array-length v4, v2

    if-le v4, v7, :cond_4

    aget-object v4, v2, v7

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment$3;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/MainFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    if-eqz v4, :cond_4

    .line 257
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.PACKAGE_CHANGED"

    if-ne v4, v5, :cond_2

    .line 258
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment$3;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    aget-object v5, v2, v7

    invoke-static {v4, v5}, Lcom/sec/android/app/easylauncher/lib/Lib;->findActivitiesForPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 260
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 261
    const/4 v3, 0x1

    .line 264
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    if-eq v4, v5, :cond_3

    if-eqz v3, :cond_4

    .line 265
    :cond_3
    const-string v4, "EasyLauncher.MainFragment"

    const-string v5, "---- app removed from main screen"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/MainFragment$3;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/MainFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    aget-object v5, v2, v7

    invoke-virtual {v4, v5}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->removeAppByPackageChanged(Ljava/lang/String;)V

    .line 270
    :cond_4
    return-void
.end method
