.class Lcom/sec/android/app/easylauncher/MainFragment$2;
.super Ljava/lang/Object;
.source "MainFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/MainFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$2;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$2;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$2;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppsGrid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->clearFocus()V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$2;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$2;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAssistiveLightWidget:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 200
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 190
    return-void
.end method
