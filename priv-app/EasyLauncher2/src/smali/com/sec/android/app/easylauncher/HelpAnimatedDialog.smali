.class public Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;
.super Ljava/lang/Object;
.source "HelpAnimatedDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;
    }
.end annotation


# static fields
.field private static mGoalAnimationView:Landroid/view/View;


# instance fields
.field private flashCount:I

.field private mFadingAnimation:Landroid/view/animation/Animation;

.field private mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFlashingAnimation:Landroid/view/animation/Animation;

.field private mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mImageTouch:Landroid/view/View$OnTouchListener;

.field private mImageTouchListener:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->flashCount:I

    .line 64
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$1;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 80
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$2;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 100
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$3;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 117
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouch:Landroid/view/View$OnTouchListener;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->flashCount:I

    .line 64
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$1;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 80
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$2;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 100
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$3;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 117
    new-instance v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$4;-><init>(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouch:Landroid/view/View$OnTouchListener;

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->init(Landroid/content/Context;Landroid/view/View;)V

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$100()Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->flashCount:I

    return v0
.end method

.method static synthetic access$208(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->flashCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->flashCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;)Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouchListener:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;

    return-object v0
.end method

.method private init(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 48
    sput-object p2, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    .line 49
    sget-object v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouch:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 51
    const v0, 0x7f040005

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 55
    :cond_0
    const v0, 0x7f040003

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 59
    :cond_1
    const v0, 0x7f040004

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 62
    :cond_2
    return-void
.end method

.method public static setAlpha()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 158
    sget-object v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 162
    :cond_0
    return-void
.end method


# virtual methods
.method public setOnImageTouchListener(Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mImageTouchListener:Lcom/sec/android/app/easylauncher/HelpAnimatedDialog$onImageTouchListener;

    .line 168
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 142
    sget-object v0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 144
    return-void
.end method

.method public startZoom(FFLandroid/view/View;)V
    .locals 9
    .param p1, "pivotX"    # F
    .param p2, "pivotY"    # F
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 147
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, p1

    move v7, v5

    move v8, p2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 150
    .local v0, "animation":Landroid/view/animation/ScaleAnimation;
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 151
    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 152
    invoke-virtual {p3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 153
    invoke-virtual {v0}, Landroid/view/animation/ScaleAnimation;->start()V

    .line 155
    return-void
.end method
