.class public Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
.super Landroid/app/Activity;
.source "AlphabeticalSearchListViewActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;,
        Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$SearchFilter;,
        Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$Filter;
    }
.end annotation


# static fields
.field public static final PREFS_NAME:Ljava/lang/String; = "FavoriteAppsList"

.field public static final TAG:Ljava/lang/String; = "EasyLauncher.AlphabeticalSearchListViewActivity"


# instance fields
.field private apps:Landroid/widget/ListView;

.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mActivityName:Landroid/content/ComponentName;

.field mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

.field public mAppData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppMarketIntent:Landroid/content/Intent;

.field mApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field mBackbutton:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mFreshActivity:Z

.field mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

.field private mHelpRunning:Z

.field private mIndexView:Landroid/view/View;

.field mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

.field private mNoResultTextView:Landroid/view/View;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field mSearchEdit:Landroid/widget/SearchView;

.field private mSkippedFirst:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    .line 334
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 384
    new-instance v0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$3;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 456
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpRunning:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpRunning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mIndexView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mNoResultTextView:Landroid/view/View;

    return-object v0
.end method

.method private addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 5
    .param p1, "appInfo"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 418
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 419
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    .line 421
    :cond_0
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 422
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ScreenPosion"

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->APPS_SCREEN_POSITION:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 423
    .local v1, "editScreen":I
    invoke-virtual {p1, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setScreen(I)V

    .line 425
    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateAppInfo(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 426
    if-eqz v0, :cond_1

    .line 427
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 428
    :cond_1
    return-void
.end method

.method private populateData()V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->apps:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 380
    const v0, 0x7f0c0016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->apps:Landroid/widget/ListView;

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->apps:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 382
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 453
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 454
    return-void
.end method

.method public onClickAppMarketButton()V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    const-string v1, "app market"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 296
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 149
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 150
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mFreshActivity:Z

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    .line 152
    const v5, 0x7f030005

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->setContentView(I)V

    .line 154
    const v5, 0x7f0c0018

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mIndexView:Landroid/view/View;

    .line 155
    const v5, 0x7f0c001c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mNoResultTextView:Landroid/view/View;

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    .line 158
    .local v0, "appContext":Lcom/sec/android/app/easylauncher/EasyController;
    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mApps:Ljava/util/ArrayList;

    .line 159
    const v5, 0x7f0c001b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/SearchView;

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    .line 162
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v5, p0}, Landroid/widget/SearchView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 163
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v5, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 165
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    const-string v6, ""

    invoke-virtual {v5, v6, v7}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 167
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v5, v7}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 168
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v5}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 169
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v5, v7}, Landroid/widget/SearchView;->setFocusable(Z)V

    .line 170
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v5}, Landroid/widget/SearchView;->clearFocus()V

    .line 187
    iput-boolean v7, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSkippedFirst:Z

    .line 189
    const v5, 0x7f0c0015

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    .line 190
    new-instance v5, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-direct {v5, p0, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    .line 191
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->runFilter()V

    .line 202
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->requestFocus()Z

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v5

    if-nez v5, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/EasyController;->loadApps()V

    .line 212
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    if-eqz v5, :cond_1

    .line 213
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->setAdapter(Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;)V

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "HelpMode"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpRunning:Z

    .line 217
    new-instance v6, Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-direct {v6, p0, v5}, Lcom/sec/android/app/easylauncher/HelpGuider;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    iput-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    .line 219
    iget-boolean v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpRunning:Z

    if-eqz v5, :cond_2

    .line 220
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_addapps(I)V

    .line 226
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 227
    .local v4, "window":Landroid/view/Window;
    if-eqz v4, :cond_3

    .line 228
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 229
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v5, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 230
    invoke-virtual {v4, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 233
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_3
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "android.intent.category.APP_MARKET"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 237
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mActivityName:Landroid/content/ComponentName;

    .line 238
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mActivityName:Landroid/content/ComponentName;

    if-eqz v5, :cond_4

    .line 239
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAppMarketIntent:Landroid/content/Intent;

    .line 242
    :cond_4
    const v5, 0x7f0c001a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mBackbutton:Landroid/widget/LinearLayout;

    .line 243
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mBackbutton:Landroid/widget/LinearLayout;

    new-instance v6, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$1;-><init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 254
    .local v1, "filter":Landroid/content/IntentFilter;
    sget-object v5, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 255
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string v5, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 257
    const-string v5, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    const-string v5, "android.intent.action.PACKAGE_INSTALL"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 259
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->populateData()V

    .line 264
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 269
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 273
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    const v1, 0x7f0b0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 275
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->unBind()V

    .line 435
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->apps:Landroid/widget/ListView;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 437
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 439
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    .line 440
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 441
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 331
    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 282
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 288
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 284
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->onClickAppMarketButton()V

    .line 285
    const/4 v0, 0x1

    goto :goto_0

    .line 282
    :pswitch_data_0
    .packed-switch 0x7f0c005d
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 73
    const v0, 0x7f040001

    const/high16 v1, 0x7f040000

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->overridePendingTransition(II)V

    .line 74
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 75
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSkippedFirst:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->runFilter()V

    .line 308
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSkippedFirst:Z

    .line 310
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 4
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    if-eqz v1, :cond_1

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 319
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->clearFocus()V

    .line 325
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return v3
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 97
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/high16 v3, 0x7f040000

    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mFreshActivity:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 81
    const v1, 0x7f040001

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->overridePendingTransition(II)V

    .line 82
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mFreshActivity:Z

    .line 86
    :goto_0
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 87
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->clearFocus()V

    .line 91
    :cond_0
    return-void

    .line 84
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    const v1, 0x7f040002

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "tag"    # Ljava/lang/Object;

    .prologue
    .line 299
    invoke-static {p1, p0}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    .line 300
    const/4 v0, 0x0

    return v0
.end method
