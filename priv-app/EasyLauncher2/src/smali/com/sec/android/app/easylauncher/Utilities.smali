.class final Lcom/sec/android/app/easylauncher/Utilities;
.super Ljava/lang/Object;
.source "Utilities.java"


# static fields
.field public static final DEBUG_VIEWS_AND_SIZES:Z

.field private static mBgPaint:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field private static sBlackPaint:Landroid/graphics/Paint;

.field private static final sBlurPaint:Landroid/graphics/Paint;

.field private static final sCanvas:Landroid/graphics/Canvas;

.field static sColorIndex:I

.field static sColors:[I

.field private static final sDisabledPaint:Landroid/graphics/Paint;

.field private static final sGlowColorFocusedPaint:Landroid/graphics/Paint;

.field private static final sGlowColorPressedPaint:Landroid/graphics/Paint;

.field private static sIconHeight:I

.field private static sIconWidth:I

.field private static final sOldBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, -0x1

    .line 54
    sput v0, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    .line 56
    sput v0, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlurPaint:Landroid/graphics/Paint;

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorPressedPaint:Landroid/graphics/Paint;

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorFocusedPaint:Landroid/graphics/Paint;

    .line 64
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sDisabledPaint:Landroid/graphics/Paint;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sOldBounds:Landroid/graphics/Rect;

    .line 68
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->mBgPaint:Ljava/util/HashMap;

    .line 74
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    .line 77
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    new-instance v1, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v3}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 81
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sColors:[I

    .line 85
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/easylauncher/Utilities;->sColorIndex:I

    return-void

    .line 81
    :array_0
    .array-data 4
        -0x10000
        -0xff0100
        -0xffff01
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static centerChildInParent(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 3
    .param p0, "dest"    # Landroid/graphics/RectF;
    .param p1, "sizeParent"    # Landroid/graphics/PointF;
    .param p2, "sizeChild"    # Landroid/graphics/PointF;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 300
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, Landroid/graphics/RectF;->left:F

    .line 301
    iget v0, p1, Landroid/graphics/PointF;->y:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, Landroid/graphics/RectF;->top:F

    .line 302
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Landroid/graphics/RectF;->right:F

    .line 303
    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    .line 304
    return-void
.end method

.method public static centerChildInParent(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/PointF;)V
    .locals 4
    .param p0, "dest"    # Landroid/graphics/RectF;
    .param p1, "sizeParent"    # Landroid/graphics/RectF;
    .param p2, "sizeChild"    # Landroid/graphics/PointF;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 307
    iget v0, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget v2, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Landroid/graphics/RectF;->left:F

    .line 308
    iget v0, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Landroid/graphics/RectF;->top:F

    .line 309
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Landroid/graphics/RectF;->right:F

    .line 310
    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    .line 311
    return-void
.end method

.method static createIconBitmap(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x0

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v11, 0x0

    .line 175
    sget-object v9, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    monitor-enter v9

    .line 176
    :try_start_0
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    const/4 v10, -0x1

    if-ne v8, v10, :cond_0

    .line 177
    invoke-static {p1}, Lcom/sec/android/app/easylauncher/Utilities;->initStatics(Landroid/content/Context;)V

    .line 179
    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    instance-of v8, p0, Landroid/graphics/drawable/PaintDrawable;

    if-eqz v8, :cond_4

    move-object v4, p0

    .line 181
    check-cast v4, Landroid/graphics/drawable/PaintDrawable;

    .line 182
    .local v4, "painter":Landroid/graphics/drawable/PaintDrawable;
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    invoke-virtual {v4, v8}, Landroid/graphics/drawable/PaintDrawable;->setIntrinsicWidth(I)V

    .line 183
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    invoke-virtual {v4, v8}, Landroid/graphics/drawable/PaintDrawable;->setIntrinsicHeight(I)V

    .line 191
    .end local v4    # "painter":Landroid/graphics/drawable/PaintDrawable;
    :cond_1
    :goto_0
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sOldBounds:Landroid/graphics/Rect;

    invoke-virtual {p0, v8}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 192
    const/high16 v5, 0x3f800000    # 1.0f

    .line 193
    .local v5, "scale":F
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 194
    .local v7, "sourceWidth":I
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    .line 195
    .local v6, "sourceHeight":I
    if-lez v7, :cond_2

    if-gtz v6, :cond_5

    .line 196
    :cond_2
    sget v7, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    .line 197
    sget v6, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    .line 198
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    sget v9, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    invoke-virtual {p0, v11, v11, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 213
    :cond_3
    :goto_1
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    sub-int v2, v8, v7

    .line 214
    .local v2, "dx":I
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    sub-int v3, v8, v6

    .line 216
    .local v3, "dy":I
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    sget v9, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 217
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v8, v13}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 218
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 219
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    int-to-float v9, v2

    mul-float/2addr v9, v12

    int-to-float v10, v3

    mul-float/2addr v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 220
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v8, v5, v5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 221
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p0, v8}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 222
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sOldBounds:Landroid/graphics/Rect;

    invoke-virtual {p0, v8}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 223
    sget-object v8, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v8, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    return-object v0

    .line 179
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "dx":I
    .end local v3    # "dy":I
    .end local v5    # "scale":F
    .end local v6    # "sourceHeight":I
    .end local v7    # "sourceWidth":I
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 184
    :cond_4
    instance-of v8, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v8, :cond_1

    move-object v1, p0

    .line 186
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 187
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v8

    if-nez v8, :cond_1

    .line 188
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(Landroid/util/DisplayMetrics;)V

    goto :goto_0

    .line 201
    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v5    # "scale":F
    .restart local v6    # "sourceHeight":I
    .restart local v7    # "sourceWidth":I
    :cond_5
    invoke-virtual {p0, v11, v11, v7, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 202
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    if-gt v7, v8, :cond_6

    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    if-le v6, v8, :cond_3

    .line 204
    :cond_6
    sget v8, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    int-to-float v8, v8

    int-to-float v9, v7

    div-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    int-to-float v9, v9

    int-to-float v10, v6

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 206
    int-to-float v8, v7

    mul-float/2addr v8, v5

    float-to-int v7, v8

    .line 207
    int-to-float v8, v6

    mul-float/2addr v8, v5

    float-to-int v6, v8

    goto :goto_1
.end method

.method static drawSelectedAllAppsBitmap(Landroid/graphics/Canvas;IIZLandroid/graphics/Bitmap;)V
    .locals 8
    .param p0, "dest"    # Landroid/graphics/Canvas;
    .param p1, "destWidth"    # I
    .param p2, "destHeight"    # I
    .param p3, "pressed"    # Z
    .param p4, "src"    # Landroid/graphics/Bitmap;

    .prologue
    .line 89
    sget-object v5, Lcom/sec/android/app/easylauncher/Utilities;->sCanvas:Landroid/graphics/Canvas;

    monitor-enter v5

    .line 90
    :try_start_0
    sget v4, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_0

    .line 95
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v6, "Assertion failed: Utilities not initialized"

    invoke-direct {v4, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 109
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 98
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v4, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 100
    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 101
    .local v3, "xy":[I
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sBlurPaint:Landroid/graphics/Paint;

    invoke-virtual {p4, v4, v3}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 103
    .local v0, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int v4, p1, v4

    div-int/lit8 v1, v4, 0x2

    .line 104
    .local v1, "px":I
    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int v4, p2, v4

    div-int/lit8 v2, v4, 0x2

    .line 105
    .local v2, "py":I
    const/4 v4, 0x0

    aget v4, v3, v4

    add-int/2addr v4, v1

    int-to-float v6, v4

    const/4 v4, 0x1

    aget v4, v3, v4

    add-int/2addr v4, v2

    int-to-float v7, v4

    if-eqz p3, :cond_1

    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorPressedPaint:Landroid/graphics/Paint;

    :goto_0
    invoke-virtual {p0, v0, v6, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 108
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 109
    monitor-exit v5

    .line 110
    return-void

    .line 105
    :cond_1
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorFocusedPaint:Landroid/graphics/Paint;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static drawVerticalLine(Landroid/view/View;Landroid/graphics/Canvas;I)V
    .locals 8
    .param p0, "view"    # Landroid/view/View;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "xOffset"    # I

    .prologue
    .line 314
    invoke-static {}, Lcom/sec/android/app/easylauncher/Utilities;->ensureBlackPaint()V

    .line 315
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v6

    .line 316
    .local v6, "sx":I
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v7

    .line 317
    .local v7, "sy":I
    add-int v0, v6, p2

    add-int/lit8 v0, v0, -0x1

    int-to-float v1, v0

    int-to-float v2, v7

    add-int v0, v6, p2

    add-int/lit8 v0, v0, 0x1

    int-to-float v3, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v7

    int-to-float v4, v0

    sget-object v5, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 318
    return-void
.end method

.method private static ensureBlackPaint()V
    .locals 2

    .prologue
    .line 271
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    .line 273
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 274
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 276
    :cond_0
    return-void
.end method

.method private static ensureViewBgPaint(Landroid/view/View;)Landroid/graphics/Paint;
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 260
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 261
    .local v1, "id":Ljava/lang/Integer;
    sget-object v2, Lcom/sec/android/app/easylauncher/Utilities;->mBgPaint:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    .line 262
    .local v0, "bgPaint":Landroid/graphics/Paint;
    if-nez v0, :cond_0

    .line 263
    new-instance v0, Landroid/graphics/Paint;

    .end local v0    # "bgPaint":Landroid/graphics/Paint;
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 264
    .restart local v0    # "bgPaint":Landroid/graphics/Paint;
    const/16 v2, 0x7f

    const/16 v3, 0x9b

    const/16 v4, 0x64

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/easylauncher/Utilities;->getRandomColor(III)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 265
    sget-object v2, Lcom/sec/android/app/easylauncher/Utilities;->mBgPaint:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    :cond_0
    return-object v0
.end method

.method static generateRandomId()I
    .locals 4

    .prologue
    .line 143
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    return v0
.end method

.method static getIconHeight()I
    .locals 1

    .prologue
    .line 366
    sget v0, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    return v0
.end method

.method public static getLocationWithRespectTo([ILandroid/view/ViewParent;Landroid/view/View;)V
    .locals 9
    .param p0, "location"    # [I
    .param p1, "parent"    # Landroid/view/ViewParent;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x2

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 332
    if-eqz p0, :cond_0

    array-length v4, p0

    if-ge v4, v5, :cond_1

    .line 333
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "location must be an array of two integers"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 336
    :cond_1
    new-array v1, v5, [F

    .line 338
    .local v1, "position":[F
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    aput v4, v1, v6

    .line 339
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v4, v4

    aput v4, v1, v7

    .line 341
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 342
    .local v3, "viewParent":Landroid/view/ViewParent;
    :goto_0
    instance-of v4, v3, Landroid/view/View;

    if-eqz v4, :cond_3

    if-eq v3, p1, :cond_3

    move-object v2, v3

    .line 343
    check-cast v2, Landroid/view/View;

    .line 345
    .local v2, "view":Landroid/view/View;
    aget v4, v1, v6

    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    aput v4, v1, v6

    .line 346
    aget v4, v1, v7

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    aput v4, v1, v7

    .line 347
    invoke-virtual {v2}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 348
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v4

    if-nez v4, :cond_2

    .line 349
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 351
    :cond_2
    aget v4, v1, v6

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v1, v6

    .line 352
    aget v4, v1, v7

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v1, v7

    .line 354
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 355
    goto :goto_0

    .line 361
    .end local v0    # "matrix":Landroid/graphics/Matrix;
    .end local v2    # "view":Landroid/view/View;
    :cond_3
    aget v4, v1, v6

    add-float/2addr v4, v8

    float-to-int v4, v4

    aput v4, p0, v6

    .line 362
    aget v4, v1, v7

    add-float/2addr v4, v8

    float-to-int v4, v4

    aput v4, p0, v7

    .line 363
    return-void
.end method

.method public static getRandomColor(III)I
    .locals 4
    .param p0, "alpha"    # I
    .param p1, "base"    # I
    .param p2, "randomizer"    # I

    .prologue
    .line 253
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3, p2}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int v2, v3, p1

    .line 254
    .local v2, "red":I
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3, p2}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int v1, v3, p1

    .line 255
    .local v1, "green":I
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3, p2}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int v0, v3, p1

    .line 256
    .local v0, "blue":I
    invoke-static {p0, v2, v1, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    return v3
.end method

.method public static getScreenSize(Landroid/content/Context;Landroid/graphics/Point;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 381
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 382
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 383
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p1, Landroid/graphics/Point;->x:I

    .line 384
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p1, Landroid/graphics/Point;->y:I

    .line 388
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v3

    if-nez v3, :cond_0

    .line 389
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 398
    .local v0, "density":F
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 414
    .end local v0    # "density":F
    :cond_0
    :goto_0
    return-void

    .line 402
    .restart local v0    # "density":F
    :pswitch_0
    iget v3, p1, Landroid/graphics/Point;->y:I

    const/high16 v4, 0x42400000    # 48.0f

    mul-float/2addr v4, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 407
    :pswitch_1
    iget v3, p1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x42280000    # 42.0f

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Point;->x:I

    goto :goto_0

    .line 398
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static initStatics(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v9, 0x1e

    const/4 v8, 0x0

    .line 234
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 235
    .local v3, "resources":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 236
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    .line 238
    .local v1, "density":F
    const v4, 0x7f07009b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/sec/android/app/easylauncher/Utilities;->sIconHeight:I

    sput v4, Lcom/sec/android/app/easylauncher/Utilities;->sIconWidth:I

    .line 240
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sBlurPaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/BlurMaskFilter;

    const/high16 v6, 0x40a00000    # 5.0f

    mul-float/2addr v6, v1

    sget-object v7, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v5, v6, v7}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 241
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorPressedPaint:Landroid/graphics/Paint;

    const/16 v5, -0x3d00

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 242
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorPressedPaint:Landroid/graphics/Paint;

    invoke-static {v8, v9}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 243
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorFocusedPaint:Landroid/graphics/Paint;

    const/16 v5, -0x7200

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 244
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sGlowColorFocusedPaint:Landroid/graphics/Paint;

    invoke-static {v8, v9}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 246
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 247
    .local v0, "cm":Landroid/graphics/ColorMatrix;
    const v4, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v4}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 248
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sDisabledPaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v5, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 249
    sget-object v4, Lcom/sec/android/app/easylauncher/Utilities;->sDisabledPaint:Landroid/graphics/Paint;

    const/16 v5, 0x88

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 250
    return-void
.end method

.method public static onDestroy()V
    .locals 1

    .prologue
    .line 321
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->mBgPaint:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 322
    return-void
.end method

.method public static onViewDraw(Landroid/view/View;Landroid/graphics/Canvas;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 296
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/easylauncher/Utilities;->onViewDraw(Landroid/view/View;Landroid/graphics/Canvas;Z)V

    .line 297
    return-void
.end method

.method public static onViewDraw(Landroid/view/View;Landroid/graphics/Canvas;Z)V
    .locals 13
    .param p0, "view"    # Landroid/view/View;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "force"    # Z

    .prologue
    .line 279
    if-eqz p2, :cond_0

    .line 281
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v11

    .line 282
    .local v11, "x":I
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v12

    .line 283
    .local v12, "y":I
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v10

    .line 284
    .local v10, "w":I
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 285
    .local v7, "h":I
    invoke-static {p0}, Lcom/sec/android/app/easylauncher/Utilities;->ensureViewBgPaint(Landroid/view/View;)Landroid/graphics/Paint;

    move-result-object v5

    .line 286
    .local v5, "bgPaint":Landroid/graphics/Paint;
    int-to-float v1, v11

    int-to-float v2, v12

    add-int v0, v11, v10

    int-to-float v3, v0

    add-int v0, v12, v7

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 287
    invoke-static {}, Lcom/sec/android/app/easylauncher/Utilities;->ensureBlackPaint()V

    .line 288
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    .line 289
    .local v6, "className":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    sget-object v1, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    add-float/2addr v0, v1

    sget-object v1, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v1

    add-float v8, v0, v1

    .line 290
    .local v8, "txtHt":F
    sget-object v0, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    .line 291
    .local v9, "txtWd":F
    int-to-float v0, v11

    int-to-float v1, v10

    sub-float/2addr v1, v9

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    add-int v1, v12, v7

    int-to-float v1, v1

    sub-float/2addr v1, v8

    sget-object v2, Lcom/sec/android/app/easylauncher/Utilities;->sBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 293
    .end local v5    # "bgPaint":Landroid/graphics/Paint;
    .end local v6    # "className":Ljava/lang/String;
    .end local v7    # "h":I
    .end local v8    # "txtHt":F
    .end local v9    # "txtWd":F
    .end local v10    # "w":I
    .end local v11    # "x":I
    .end local v12    # "y":I
    :cond_0
    return-void
.end method

.method public static renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    .locals 8
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    .line 147
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/easylauncher/Utilities;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V

    .line 148
    return-void
.end method

.method public static renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIF)V
    .locals 8
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "scale"    # F

    .prologue
    .line 152
    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/easylauncher/Utilities;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V

    .line 153
    return-void
.end method

.method public static renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V
    .locals 4
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "scale"    # F
    .param p7, "multiplyColor"    # I

    .prologue
    .line 157
    if-eqz p1, :cond_1

    .line 158
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 159
    .local v0, "c":Landroid/graphics/Canvas;
    invoke-virtual {v0, p6, p6}, Landroid/graphics/Canvas;->scale(FF)V

    .line 160
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 161
    .local v1, "oldBounds":Landroid/graphics/Rect;
    add-int v2, p2, p4

    add-int v3, p3, p5

    invoke-virtual {p0, p2, p3, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 162
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 163
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 164
    const/4 v2, -0x1

    if-eq p7, v2, :cond_0

    .line 165
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p7, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 167
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 169
    .end local v0    # "c":Landroid/graphics/Canvas;
    .end local v1    # "oldBounds":Landroid/graphics/Rect;
    :cond_1
    return-void
.end method

.method static roundToPow2(I)I
    .locals 3
    .param p0, "n"    # I

    .prologue
    .line 125
    move v1, p0

    .line 126
    .local v1, "orig":I
    shr-int/lit8 p0, p0, 0x1

    .line 127
    const/high16 v0, 0x8000000

    .line 128
    .local v0, "mask":I
    :goto_0
    if-eqz v0, :cond_0

    and-int v2, p0, v0

    if-nez v2, :cond_0

    .line 129
    shr-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 132
    or-int/2addr p0, v0

    .line 133
    shr-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 135
    :cond_1
    add-int/lit8 p0, p0, 0x1

    .line 136
    if-eq p0, v1, :cond_2

    .line 137
    shl-int/lit8 p0, p0, 0x1

    .line 139
    :cond_2
    return p0
.end method
