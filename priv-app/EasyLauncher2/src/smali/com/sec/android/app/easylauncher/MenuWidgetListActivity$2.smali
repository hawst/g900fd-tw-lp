.class Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;
.super Ljava/lang/Object;
.source "MenuWidgetListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, -0x1

    .line 309
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v3, v3, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    if-nez v3, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EditPosition"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 316
    .local v0, "editposition":I
    if-eq v0, v5, :cond_0

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    iget-object v3, v3, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    invoke-virtual {v3, p3}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->removeData(I)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v2

    .line 320
    .local v2, "widgetItem":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {v2, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPosition(I)V

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    # invokes: Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->addWidgetToScreen(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    invoke-static {v3, v2}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->access$000(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 323
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 324
    .local v1, "returnIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-virtual {v3, v5, v1}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->setResult(ILandroid/content/Intent;)V

    .line 326
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;->this$0:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->finish()V

    goto :goto_0
.end method
