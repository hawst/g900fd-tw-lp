.class public final Lcom/sec/android/app/easylauncher/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final btn_done:I = 0x7f020000

.field public static final btn_moreapps_calllog:I = 0x7f020001

.field public static final btn_showandhide_hide:I = 0x7f020002

.field public static final btn_showandhide_select:I = 0x7f020003

.field public static final delete_drag_bar:I = 0x7f020004

.field public static final dragbar_ic_delete:I = 0x7f020005

.field public static final drawer_icon_app_off:I = 0x7f020006

.field public static final drawer_icon_app_on:I = 0x7f020007

.field public static final drawer_icon_contacts_off:I = 0x7f020008

.field public static final drawer_icon_contacts_on:I = 0x7f020009

.field public static final drawer_icon_home_off:I = 0x7f02000a

.field public static final drawer_icon_home_on:I = 0x7f02000b

.field public static final drawer_list_item_selector:I = 0x7f02000c

.field public static final easy_btn_foc:I = 0x7f02000d

.field public static final easy_btn_nor:I = 0x7f02000e

.field public static final easy_btn_press:I = 0x7f02000f

.field public static final easy_ic_moreapps:I = 0x7f020010

.field public static final easy_main_home:I = 0x7f020011

.field public static final easy_main_homeon:I = 0x7f020012

.field public static final easy_main_ic_add:I = 0x7f020013

.field public static final easy_main_ic_add_press:I = 0x7f020014

.field public static final easy_main_ic_badge:I = 0x7f020015

.field public static final easy_main_ic_delete:I = 0x7f020016

.field public static final easy_main_ic_delete_press:I = 0x7f020017

.field public static final easy_main_pointer:I = 0x7f020018

.field public static final easy_main_pointeron:I = 0x7f020019

.field public static final easy_press_bg:I = 0x7f02001a

.field public static final edit_bg:I = 0x7f02001b

.field public static final focused_bg:I = 0x7f02001c

.field public static final grid_apps_add:I = 0x7f02001d

.field public static final grid_apps_background:I = 0x7f02001e

.field public static final grid_apps_delete:I = 0x7f02001f

.field public static final help_popup_picker_b_c:I = 0x7f020020

.field public static final help_popup_picker_bg_w_01:I = 0x7f020021

.field public static final help_popup_picker_t_c:I = 0x7f020022

.field public static final help_tap:I = 0x7f020023

.field public static final home_edit_top_bg:I = 0x7f020024

.field public static final home_edit_top_bg_delete:I = 0x7f020025

.field public static final home_edit_top_ic_delete:I = 0x7f020026

.field public static final home_edit_top_ic_delete_focus:I = 0x7f020027

.field public static final ic_edit:I = 0x7f020028

.field public static final ic_launcher_home:I = 0x7f020029

.field public static final indicator_bg:I = 0x7f02002a

.field public static final mux_drawer_bg:I = 0x7f02002b

.field public static final mux_drawer_divline:I = 0x7f02002c

.field public static final personal_list_ic:I = 0x7f02002d

.field public static final tw_ab_transparent_light_holo:I = 0x7f02002e

.field public static final tw_action_bar_icon_edit:I = 0x7f02002f

.field public static final tw_action_bar_icon_edit_disabled_holo_dark:I = 0x7f020030

.field public static final tw_action_bar_icon_edit_holo_dark:I = 0x7f020031

.field public static final tw_action_bar_icon_notification:I = 0x7f020032

.field public static final tw_action_bar_icon_notification_disabled_holo_light:I = 0x7f020033

.field public static final tw_action_bar_icon_notification_holo_light:I = 0x7f020034

.field public static final tw_action_bar_icon_playstore:I = 0x7f020035

.field public static final tw_action_bar_icon_playstore_disabled_holo_dark:I = 0x7f020036

.field public static final tw_action_bar_icon_playstore_holo_dark:I = 0x7f020037

.field public static final tw_action_bar_icon_search:I = 0x7f020038

.field public static final tw_action_bar_icon_search_disabled_holo_dark:I = 0x7f020039

.field public static final tw_action_bar_icon_search_holo_dark:I = 0x7f02003a

.field public static final tw_action_bar_icon_setting:I = 0x7f02003b

.field public static final tw_action_bar_icon_setting_disabled_holo_dark:I = 0x7f02003c

.field public static final tw_action_bar_icon_setting_holo_dark:I = 0x7f02003d

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f02003e

.field public static final tw_action_item_background_selected_holo_dark:I = 0x7f02003f

.field public static final tw_background_light:I = 0x7f020040

.field public static final tw_btn_check_holo_dark:I = 0x7f020041

.field public static final tw_btn_check_off_disabled_focused_holo_dark:I = 0x7f020042

.field public static final tw_btn_check_off_disabled_holo_dark:I = 0x7f020043

.field public static final tw_btn_check_off_focused_holo_dark:I = 0x7f020044

.field public static final tw_btn_check_off_holo_dark:I = 0x7f020045

.field public static final tw_btn_check_off_pressed_holo_dark:I = 0x7f020046

.field public static final tw_btn_check_on_disabled_focused_holo_dark:I = 0x7f020047

.field public static final tw_btn_check_on_disabled_holo_dark:I = 0x7f020048

.field public static final tw_btn_check_on_focused_holo_dark:I = 0x7f020049

.field public static final tw_btn_check_on_holo_dark:I = 0x7f02004a

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f02004b

.field public static final tw_btn_default_focused_holo_light:I = 0x7f02004c

.field public static final tw_btn_default_normal_holo_light:I = 0x7f02004d

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f02004e

.field public static final tw_ic_ab_drawer_holo_dark:I = 0x7f02004f

.field public static final tw_list_divider_holo_light:I = 0x7f020050

.field public static final tw_scroll_popup_bg_holo_light:I = 0x7f020051

.field public static final tw_scrollbar_handle_bg_holo_light:I = 0x7f020052

.field public static final tw_scrollbar_handle_bg_pressed_holo_light:I = 0x7f020053

.field public static final tw_scrollbar_handle_top_bg_default_holo_light:I = 0x7f020054

.field public static final tw_scrollbar_handle_top_bg_holo_light:I = 0x7f020055


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
