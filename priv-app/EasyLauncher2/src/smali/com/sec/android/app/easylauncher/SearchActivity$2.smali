.class Lcom/sec/android/app/easylauncher/SearchActivity$2;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/SearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/SearchActivity;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, -0x1

    .line 214
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/SearchActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v5

    if-nez v5, :cond_1

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 216
    .local v0, "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 217
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 218
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-static {v3, v5}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    .line 233
    .end local v0    # "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EditPosition"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 221
    .local v1, "editposition":I
    if-eq v1, v7, :cond_0

    .line 223
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    const-string v6, "input_method"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/easylauncher/SearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 224
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 225
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->toShow:Ljava/util/ArrayList;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 226
    .restart local v0    # "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPosition(I)V

    .line 227
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    # invokes: Lcom/sec/android/app/easylauncher/SearchActivity;->addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    invoke-static {v5, v0}, Lcom/sec/android/app/easylauncher/SearchActivity;->access$000(Lcom/sec/android/app/easylauncher/SearchActivity;Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 228
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 229
    .local v4, "returnIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v5, v7, v4}, Lcom/sec/android/app/easylauncher/SearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 230
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/SearchActivity;->finish()V

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/SearchActivity$2;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/easylauncher/EasyController;->removefromMap(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    goto :goto_0
.end method
