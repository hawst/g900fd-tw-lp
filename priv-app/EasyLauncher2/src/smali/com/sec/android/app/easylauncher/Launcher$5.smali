.class Lcom/sec/android/app/easylauncher/Launcher$5;
.super Ljava/lang/Object;
.source "Launcher.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/Launcher;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/Launcher;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/Launcher$5;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 571
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 566
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 552
    sput p1, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$5;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher$5;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090040

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher$5;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v5, v5, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->sayByTalkback(Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$5;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicator(I)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher$5;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->setPageShowHideButton(I)V

    .line 559
    return-void
.end method
