.class public Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MenuAppsListAdapter.java"


# instance fields
.field public mApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mContext:Landroid/content/Context;

    .line 53
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    .line 55
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    .line 56
    .local v0, "appContext":Lcom/sec/android/app/easylauncher/EasyController;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getMoreAppsListState()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->fillData()V

    .line 59
    :cond_0
    return-void
.end method

.method private fillData()V
    .locals 3

    .prologue
    .line 136
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    if-nez v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    .line 139
    .local v0, "appContext":Lcom/sec/android/app/easylauncher/EasyController;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private setListItemData(Lcom/sec/android/app/easylauncher/lib/AppInfo;Landroid/view/View;I)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .param p2, "listItem"    # Landroid/view/View;
    .param p3, "position"    # I

    .prologue
    .line 70
    const/4 v1, 0x0

    .line 72
    .local v1, "dr":Landroid/graphics/drawable/Drawable;
    if-eqz p1, :cond_2

    .line 73
    const v4, 0x7f0c0024

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 74
    .local v2, "imageView":Landroid/widget/ImageView;
    const v4, 0x7f0c0026

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 75
    .local v3, "textView":Landroid/widget/TextView;
    if-eqz v2, :cond_0

    .line 76
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 82
    if-eqz v1, :cond_3

    .line 83
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 84
    const/4 v1, 0x0

    .line 91
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "description":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_1

    .line 94
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f09003f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    :cond_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 98
    .end local v0    # "description":Ljava/lang/String;
    .end local v2    # "imageView":Landroid/widget/ImageView;
    .end local v3    # "textView":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {p2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 99
    return-void

    .line 86
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    .restart local v3    # "textView":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 113
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 119
    if-nez p2, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f030008

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-direct {p0, v0, p2, p1}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->setListItemData(Lcom/sec/android/app/easylauncher/lib/AppInfo;Landroid/view/View;I)V

    .line 125
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->fillData()V

    .line 65
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 66
    return-void
.end method

.method removeData(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 130
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 131
    .local v0, "info":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 132
    return-object v0
.end method

.method public unBind()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->mApps:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 49
    :cond_0
    return-void
.end method
