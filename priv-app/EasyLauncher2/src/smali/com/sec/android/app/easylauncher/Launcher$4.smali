.class Lcom/sec/android/app/easylauncher/Launcher$4;
.super Landroid/database/ContentObserver;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/Launcher;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/Launcher;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .prologue
    .line 530
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    # getter for: Lcom/sec/android/app/easylauncher/Launcher;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;
    invoke-static {v4}, Lcom/sec/android/app/easylauncher/Launcher;->access$200(Lcom/sec/android/app/easylauncher/Launcher;)Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 531
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    # getter for: Lcom/sec/android/app/easylauncher/Launcher;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;
    invoke-static {v4}, Lcom/sec/android/app/easylauncher/Launcher;->access$200(Lcom/sec/android/app/easylauncher/Launcher;)Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->updateBadgeCounts()V

    .line 533
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v4, :cond_3

    .line 534
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-eqz v4, :cond_1

    .line 535
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/MainFragment;->updateBadge()V

    .line 538
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    if-eqz v4, :cond_3

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher$4;->this$0:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/AppFragment;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v2, v0, v1

    .line 540
    .local v2, "item":Lcom/sec/android/app/easylauncher/AppFragment;
    if-eqz v2, :cond_2

    .line 541
    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AppFragment;->updateBadge()V

    .line 539
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 545
    .end local v0    # "arr$":[Lcom/sec/android/app/easylauncher/AppFragment;
    .end local v1    # "i$":I
    .end local v2    # "item":Lcom/sec/android/app/easylauncher/AppFragment;
    .end local v3    # "len$":I
    :cond_3
    return-void
.end method
