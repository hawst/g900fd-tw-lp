.class public Lcom/sec/android/app/easylauncher/lib/BadgeCache;
.super Ljava/lang/Object;
.source "BadgeCache.java"


# static fields
.field private static final BADGE_COLUMNS:[Ljava/lang/String;

.field public static final BADGE_URI:Landroid/net/Uri;

.field private static final DEBUGGABLE:Z

.field private static final INITIAL_BADGE_CAPACITY:I = 0x14

.field private static final TAG:Ljava/lang/String; = "BadgeCache"

.field public static final USING_BADGE_APPS_LIST:[Ljava/lang/String;

.field private static final ZERO:Ljava/lang/Integer;

.field private static mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;


# instance fields
.field private final mApp:Landroid/content/Context;

.field private mBadges:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 39
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->DEBUGGABLE:Z

    .line 41
    const-string v0, "content://com.sec.badge/apps"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->BADGE_URI:Landroid/net/Uri;

    .line 43
    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "package"

    aput-object v3, v0, v1

    const-string v3, "class"

    aput-object v3, v0, v2

    const-string v3, "badgecount"

    aput-object v3, v0, v4

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->BADGE_COLUMNS:[Ljava/lang/String;

    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->ZERO:Ljava/lang/Integer;

    .line 62
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "com.android.mms"

    aput-object v3, v0, v1

    const-string v1, "com.android.email"

    aput-object v1, v0, v2

    const-string v1, "com.android.contacts"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.samsungapps"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "com.samsung.contacts"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.android.jcontacts"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->USING_BADGE_APPS_LIST:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 39
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "app"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    .line 80
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mApp:Landroid/content/Context;

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->updateBadgeCounts()V

    .line 82
    return-void
.end method

.method public static getBadgeCache(Landroid/content/Context;)Lcom/sec/android/app/easylauncher/lib/BadgeCache;
    .locals 1
    .param p0, "app"    # Landroid/content/Context;

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/lib/BadgeCache;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    .line 72
    :cond_0
    sget-object v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    return-object v0
.end method


# virtual methods
.method public getBadgeCount(Landroid/content/ComponentName;)I
    .locals 5
    .param p1, "component"    # Landroid/content/ComponentName;

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 95
    .local v0, "count":I
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 96
    .local v1, "value":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 99
    sget-boolean v2, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->DEBUGGABLE:Z

    if-eqz v2, :cond_0

    .line 100
    const-string v2, "BadgeCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBadgeCount: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    return v0
.end method

.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 176
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    .line 177
    sput-object v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    .line 178
    return-void
.end method

.method public updateBadgeCounts()V
    .locals 17

    .prologue
    .line 113
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 114
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 115
    const-string v1, "BadgeCache"

    const-string v2, "updateBadgeCounts: "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mApp:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->BADGE_URI:Landroid/net/Uri;

    sget-object v3, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->BADGE_COLUMNS:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 118
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_9

    .line 121
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 122
    .local v12, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    sget-object v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->ZERO:Ljava/lang/Integer;

    invoke-interface {v12, v1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 125
    .end local v12    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    :cond_1
    sget-object v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->USING_BADGE_APPS_LIST:[Ljava/lang/String;

    array-length v1, v1

    new-array v15, v1, [Z

    .line 129
    .local v15, "isAlreadyExist":[Z
    :cond_2
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 130
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 131
    .local v16, "pkgName":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 132
    .local v10, "className":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 134
    .local v8, "badgeCount":I
    if-eqz v16, :cond_2

    .line 136
    const/4 v7, -0x1

    .line 137
    .local v7, "appIndex":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    sget-object v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->USING_BADGE_APPS_LIST:[Ljava/lang/String;

    array-length v1, v1

    if-ge v13, v1, :cond_3

    .line 138
    sget-object v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->USING_BADGE_APPS_LIST:[Ljava/lang/String;

    aget-object v1, v1, v13

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 139
    move v7, v13

    .line 144
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->DEBUGGABLE:Z

    if-eqz v1, :cond_4

    .line 145
    const-string v1, "BadgeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1. updateBadgeCounts: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_4
    if-eqz v10, :cond_5

    if-lez v8, :cond_5

    .line 149
    if-ltz v7, :cond_7

    aget-boolean v1, v15, v7

    if-eqz v1, :cond_7

    .line 150
    const-string v1, "BadgeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2. Do not updateBadgeCounts!!, multiple data for appIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_5
    :goto_3
    if-ltz v7, :cond_2

    .line 164
    const/4 v1, 0x1

    aput-boolean v1, v15, v7

    goto :goto_1

    .line 137
    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 153
    :cond_7
    new-instance v11, Landroid/content/ComponentName;

    move-object/from16 v0, v16

    invoke-direct {v11, v0, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .local v11, "cn":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->mBadges:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->DEBUGGABLE:Z

    if-eqz v1, :cond_5

    .line 156
    const-string v1, "BadgeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateBadgeCounts inside: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 169
    .end local v7    # "appIndex":I
    .end local v8    # "badgeCount":I
    .end local v10    # "className":Ljava/lang/String;
    .end local v11    # "cn":Landroid/content/ComponentName;
    .end local v13    # "i":I
    .end local v16    # "pkgName":Ljava/lang/String;
    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 172
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "isAlreadyExist":[Z
    :cond_9
    return-void
.end method
