.class Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;
.super Ljava/lang/Object;
.source "AlphabeticalListViewActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, -0x1

    .line 263
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    if-nez v4, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v4

    if-nez v4, :cond_2

    .line 266
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->getItem(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v0

    .line 267
    .local v0, "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 268
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 269
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-static {v2, v4}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 272
    .end local v0    # "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "EditPosition"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 273
    .local v1, "editposition":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "HelpMode"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    # setter for: Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->access$002(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;Z)Z

    .line 275
    if-eq v1, v8, :cond_0

    .line 277
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->removeData(I)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v0

    .line 278
    .restart local v0    # "appItem":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPosition(I)V

    .line 279
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    # invokes: Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->addNewAppToAppScreen(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    invoke-static {v4, v0}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->access$100(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    .line 280
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 281
    .local v3, "returnIntent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-virtual {v4, v8, v3}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->finish()V

    .line 284
    const-string v4, "EasyLauncher.AlphabeticalListViewActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mHelpRunning : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z
    invoke-static {v6}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->access$000(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    # getter for: Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->mHelpRunning:Z
    invoke-static {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->access$000(Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 286
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AlphabeticalListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/easylauncher/EasyController;->removefromMap(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V

    goto/16 :goto_0
.end method
