.class public Lcom/sec/android/app/easylauncher/EasyContentLayout;
.super Landroid/widget/RelativeLayout;
.source "EasyContentLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;,
        Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;,
        Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field mDragImage:Landroid/widget/ImageView;

.field private mDragView:Landroid/widget/ImageView;

.field mLongClickMode:Z

.field private mOnAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

.field private mOnDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

.field private mOnDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

.field private mSelectItemIndex:I

.field private mTouchPosition:Landroid/graphics/Point;

.field private mTouchPositionOffset:Landroid/graphics/Point;

.field private wm:Landroid/view/WindowManager;

.field private wmParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    const-string v0, "EasyLauncher.EasyContentLayout"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->TAG:Ljava/lang/String;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mSelectItemIndex:I

    .line 41
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->init(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-string v0, "EasyLauncher.EasyContentLayout"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->TAG:Ljava/lang/String;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mSelectItemIndex:I

    .line 41
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->init(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const-string v0, "EasyLauncher.EasyContentLayout"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->TAG:Ljava/lang/String;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mSelectItemIndex:I

    .line 41
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->init(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/EasyContentLayout;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/EasyContentLayout;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/EasyContentLayout;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/EasyContentLayout;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wm:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/easylauncher/EasyContentLayout;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/EasyContentLayout;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    return-object p1
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x2

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mContext:Landroid/content/Context;

    .line 71
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wm:Landroid/view/WindowManager;

    .line 73
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 78
    return-void
.end method

.method private onDrag(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int v1, p1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int v1, p2, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;->drag(II)V

    goto :goto_0
.end method

.method private onDrop(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 185
    const-string v0, "EasyLauncher.EasyContentLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDrop mDragView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 195
    :goto_0
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;->drop(II)V

    .line 194
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->removeDragView()V

    goto :goto_0
.end method

.method private removeDragView()V
    .locals 2

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    .line 205
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelDrag()V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->removeDragView()V

    .line 209
    return-void
.end method

.method createDragView(Landroid/view/View;)V
    .locals 7
    .param p1, "item"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 132
    invoke-virtual {p1, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 135
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragImage:Landroid/widget/ImageView;

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 141
    const-string v1, "EasyLauncher.EasyContentLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createDragView mTouchPosition.x: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mTouchPosition.y : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPositionOffset:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 145
    const-string v1, "EasyLauncher.EasyContentLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createDragView wmParams.x: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " wmParams.y : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iput-boolean v6, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    .line 149
    new-instance v1, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/EasyContentLayout$1;-><init>(Lcom/sec/android/app/easylauncher/EasyContentLayout;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 164
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 166
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 84
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 87
    .local v1, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 97
    :goto_0
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mLongClickMode:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mDragView:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 98
    :cond_0
    const/4 v2, 0x1

    .line 100
    :goto_1
    return v2

    .line 89
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mTouchPosition:Landroid/graphics/Point;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 100
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 107
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 111
    .local v1, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 127
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2

    .line 113
    :pswitch_0
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->onDrag(II)V

    goto :goto_0

    .line 117
    :pswitch_1
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->onDrop(II)V

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->removeDragView()V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setOnAddViewListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    .line 227
    return-void
.end method

.method public setOnDragListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

    .line 223
    return-void
.end method

.method public setOnDropListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mOnDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    .line 218
    return-void
.end method

.method public setSelectItemIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 212
    iput p1, p0, Lcom/sec/android/app/easylauncher/EasyContentLayout;->mSelectItemIndex:I

    .line 213
    return-void
.end method
