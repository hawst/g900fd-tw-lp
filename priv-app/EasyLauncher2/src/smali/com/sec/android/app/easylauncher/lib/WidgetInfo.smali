.class public Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
.super Ljava/lang/Object;
.source "WidgetInfo.java"


# instance fields
.field private className:Ljava/lang/String;

.field private defaultPadding:Landroid/graphics/Rect;

.field private hostView:Landroid/appwidget/AppWidgetHostView;

.field private mWidgetIcon:Landroid/graphics/drawable/Drawable;

.field private packageName:Ljava/lang/String;

.field private position:I

.field private screen:I

.field private spanX:I

.field private widgetProviderInfo:Landroid/appwidget/AppWidgetProviderInfo;

.field private widgetid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultPadding()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->defaultPadding:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getHostView()Landroid/appwidget/AppWidgetHostView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->position:I

    return v0
.end method

.method public getScreen()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->screen:I

    return v0
.end method

.method public getSpanX()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->spanX:I

    return v0
.end method

.method public getWidgetIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->mWidgetIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getWidgetLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetProviderInfo:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getWidgetPreview()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetProviderInfo:Landroid/appwidget/AppWidgetProviderInfo;

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    return v0
.end method

.method public getWidgetProviderInfo()Landroid/appwidget/AppWidgetProviderInfo;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetProviderInfo:Landroid/appwidget/AppWidgetProviderInfo;

    return-object v0
.end method

.method public getWidgetid()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetid:I

    return v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->className:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setDefaultPadding(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "defaultPadding"    # Landroid/graphics/Rect;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->defaultPadding:Landroid/graphics/Rect;

    .line 126
    return-void
.end method

.method public setHostView(Landroid/appwidget/AppWidgetHostView;)V
    .locals 0
    .param p1, "hostView"    # Landroid/appwidget/AppWidgetHostView;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    .line 118
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->packageName:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->position:I

    .line 62
    return-void
.end method

.method public setScreen(I)V
    .locals 0
    .param p1, "screen"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->screen:I

    .line 70
    return-void
.end method

.method public setSpanX(I)V
    .locals 0
    .param p1, "spanX"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->spanX:I

    .line 86
    return-void
.end method

.method public setWidgetIcon(Landroid/content/res/Resources;I)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I

    .prologue
    .line 141
    const/4 v1, 0x0

    .line 144
    .local v1, "newIcon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->mListIconDpi:I

    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 160
    :goto_0
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->mWidgetIcon:Landroid/graphics/drawable/Drawable;

    .line 161
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/ArithmeticException;
    invoke-virtual {v0}, Ljava/lang/ArithmeticException;->printStackTrace()V

    .line 150
    const/16 v2, 0xf0

    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 159
    goto :goto_0

    .line 152
    .end local v0    # "e":Ljava/lang/ArithmeticException;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public setWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 0
    .param p1, "widgetProviderInfo"    # Landroid/appwidget/AppWidgetProviderInfo;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetProviderInfo:Landroid/appwidget/AppWidgetProviderInfo;

    .line 110
    return-void
.end method

.method public setWidgetid(I)V
    .locals 0
    .param p1, "widgetid"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetid:I

    .line 78
    return-void
.end method

.method public unbind()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 164
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->packageName:Ljava/lang/String;

    .line 165
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->className:Ljava/lang/String;

    .line 166
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->widgetProviderInfo:Landroid/appwidget/AppWidgetProviderInfo;

    .line 167
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    .line 168
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->defaultPadding:Landroid/graphics/Rect;

    .line 169
    return-void
.end method
