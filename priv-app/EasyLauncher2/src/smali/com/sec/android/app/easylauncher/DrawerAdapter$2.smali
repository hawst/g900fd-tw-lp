.class Lcom/sec/android/app/easylauncher/DrawerAdapter$2;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/DrawerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/DrawerAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/DrawerAdapter;I)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    iput p2, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "v"    # Landroid/widget/CompoundButton;
    .param p2, "value"    # Z

    .prologue
    .line 128
    const-string v0, "DrawerAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "position:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;->val$position:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " v:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;->this$0:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    # getter for: Lcom/sec/android/app/easylauncher/DrawerAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/easylauncher/DrawerAdapter;->access$000(Lcom/sec/android/app/easylauncher/DrawerAdapter;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/Launcher;

    iget v1, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;->val$position:I

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/easylauncher/Launcher;->setPageShow(IZ)V

    .line 133
    return-void
.end method
