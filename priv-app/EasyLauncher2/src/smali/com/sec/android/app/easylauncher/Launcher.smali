.class public Lcom/sec/android/app/easylauncher/Launcher;
.super Landroid/support/v4/app/FragmentActivity;
.source "Launcher.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;
    }
.end annotation


# static fields
.field private static final ACTION_ENTER_EASYLAUNCHER:Ljava/lang/String; = "com.android.launcher.action.ENTER_EASYLAUNCHER"

.field private static final ACTION_HOME_MODE:Ljava/lang/String; = "com.android.launcher.action.HOME_MODE_CHANGE"

.field private static final DEBUGGABLE:Z

.field public static final DEFAULT_WIDGET_ID:I = -0x64

.field private static final EXTRA_LAUNCHER_ACTION:Ljava/lang/String; = "sec.android.intent.extra.LAUNCHER_ACTION"

.field private static final HELP_ACTIVITY_TYPE:Ljava/lang/String; = "help_activity_type"

.field private static final LAUNCHER_ACTION_ALL_APPS:Ljava/lang/String; = "com.android.launcher2.ALL_APPS"

.field private static final REQUEST_APPWIDGET_BIND:I = 0x2

.field static final REQUEST_PICK_WALLPAPER:I = 0x1

.field private static final SETTINGS_SYSTEMUI_TRANSPARENCY:Ljava/lang/String; = "android.wallpaper.settings_systemui_transparency"

.field public static final STATUS_BAR_TRANSLUCENT:I = 0x40000000

.field private static final SYSTEMUI_TRANSPARENCY:Z = true

.field public static final SYSTEM_UI_TRANSPARENT:I = 0x8000

.field public static final TAG:Ljava/lang/String; = "EasyLauncher.Launcher"

.field private static final WALLPAPER_SCREENS_SPAN:F = 2.0f

.field public static mEditMode:Z

.field public static mIsEasyHelpComplete:Z

.field public static mLongClickEditMode:Z

.field public static mMoreAppsMode:Z

.field public static mPosition:I

.field public static mScreenCount:I


# instance fields
.field final HELP_ADD_APPS:I

.field final HELP_ADD_CONTACTS:I

.field final HELP_NONE:I

.field final HELP_REMOVE_APPS:I

.field private drawerIconId:[I

.field drawerIconImageView:[Landroid/widget/ImageView;

.field private isOnCreateExec:Z

.field private isPaused:Z

.field mActionbarTitle:Landroid/widget/TextView;

.field mApp:Lcom/sec/android/app/easylauncher/EasyController;

.field private mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

.field private mBadgeObserver:Landroid/database/ContentObserver;

.field mCallback:Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;

.field mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

.field mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

.field mDoneButton:Landroid/widget/Button;

.field private mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

.field private mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mDrawerList:Landroid/widget/ListView;

.field private mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

.field private mDrawerView:Landroid/widget/LinearLayout;

.field mEditShort:Landroid/view/View;

.field mEditShortTv:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

.field public mHelpMode:I

.field public mHelpStep:I

.field private mIsExternalHelpActivity:Z

.field public mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

.field private mOptionsMenu:Landroid/view/Menu;

.field mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

.field mPageHide:Landroid/widget/TextView;

.field mPageShow:Landroid/widget/TextView;

.field mPageShowHide:Landroid/view/View;

.field mSetWallpaper:Landroid/view/View;

.field private mSettingsInfo:Lcom/sec/android/app/easylauncher/lib/AppInfo;

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field mUseDrawerMenu:Z

.field mUseLongClickRemove:Z

.field private mWallpaperHeight:I

.field private mWallpaperManager:Landroid/app/WallpaperManager;

.field private mWallpaperWidth:I

.field pageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

.field pageIndicatorContainer:Landroid/widget/LinearLayout;

.field private pageIndicatorId:[I

.field private pageIndicatorLayoutId:[I

.field pageIndicatorLayoutView:[Landroid/view/View;

.field pageIndicatorTextView:[Landroid/widget/TextView;

.field pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

.field widgetHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    .line 103
    sput-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 104
    sput-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mLongClickEditMode:Z

    .line 105
    sput-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    .line 154
    sput-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mIsEasyHelpComplete:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x6

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 86
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->isOnCreateExec:Z

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHandler:Landroid/os/Handler;

    .line 114
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorId:[I

    .line 117
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutId:[I

    .line 124
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconId:[I

    .line 148
    iput v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->HELP_NONE:I

    .line 149
    iput v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->HELP_ADD_APPS:I

    .line 150
    iput v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->HELP_REMOVE_APPS:I

    .line 151
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->HELP_ADD_CONTACTS:I

    .line 153
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mIsExternalHelpActivity:Z

    .line 155
    iput v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    .line 156
    iput v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpStep:I

    .line 160
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseLongClickRemove:Z

    .line 161
    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    .line 170
    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->isPaused:Z

    .line 517
    new-instance v0, Lcom/sec/android/app/easylauncher/Launcher$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/Launcher$3;-><init>(Lcom/sec/android/app/easylauncher/Launcher;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->widgetHandler:Landroid/os/Handler;

    .line 527
    new-instance v0, Lcom/sec/android/app/easylauncher/Launcher$4;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/easylauncher/Launcher$4;-><init>(Lcom/sec/android/app/easylauncher/Launcher;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeObserver:Landroid/database/ContentObserver;

    .line 548
    new-instance v0, Lcom/sec/android/app/easylauncher/Launcher$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/Launcher$5;-><init>(Lcom/sec/android/app/easylauncher/Launcher;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    .line 1452
    return-void

    .line 114
    :array_0
    .array-data 4
        0x7f0c0039
        0x7f0c003b
        0x7f0c003d
        0x7f0c003f
        0x7f0c0041
        0x7f0c0043
    .end array-data

    .line 117
    :array_1
    .array-data 4
        0x7f0c0038
        0x7f0c003a
        0x7f0c003c
        0x7f0c003e
        0x7f0c0040
        0x7f0c0042
    .end array-data

    .line 124
    :array_2
    .array-data 4
        0x7f0c0007
        0x7f0c0008
        0x7f0c0009
        0x7f0c000a
        0x7f0c000b
        0x7f0c000c
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/Launcher;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/Launcher;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/Launcher;)Lcom/sec/android/app/easylauncher/lib/BadgeCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/Launcher;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    return-object v0
.end method

.method private getLaunchIntentForHelpHub()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1860
    const/4 v1, 0x0

    .line 1861
    .local v1, "intent":Landroid/content/Intent;
    iget-boolean v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mIsExternalHelpActivity:Z

    if-eqz v4, :cond_1

    .line 1863
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1864
    .end local v1    # "intent":Landroid/content/Intent;
    .local v2, "intent":Landroid/content/Intent;
    :try_start_1
    const-string v4, "helphub:section"

    const-string v5, "homescreen"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1865
    const/high16 v4, 0x10200000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1867
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mIsExternalHelpActivity:Z
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v1, v2

    .line 1892
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-object v1

    .line 1868
    :catch_0
    move-exception v0

    .line 1869
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    if-eqz v4, :cond_0

    .line 1870
    const-string v4, "EasyLauncher.Launcher"

    const-string v5, "Unable to launch  intent= com.samsung.helphub.HELP"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1875
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v5, 0x258

    if-ge v4, v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/EasyController;->canRotate()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1877
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1878
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.samsung.helphub"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1880
    goto :goto_0

    .line 1881
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1882
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    :try_start_3
    const-string v4, "com.samsung.helphub"

    const-string v5, "com.samsung.helphub.HelpHubSecondDepthActivity"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1884
    const/high16 v4, 0x10200000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    move-object v1, v2

    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 1887
    :catch_1
    move-exception v0

    .line 1888
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_2
    sget-boolean v4, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    if-eqz v4, :cond_0

    .line 1889
    const-string v4, "EasyLauncher.Launcher"

    const-string v5, "Unable to launch HelpHub Activity"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1887
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_2

    .line 1868
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_3
    move-exception v0

    move-object v1, v2

    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_1
.end method

.method private setIndicatorTransparency()V
    .locals 4

    .prologue
    .line 1460
    const/16 v0, 0x400

    .line 1461
    .local v0, "visibility":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1463
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1465
    .local v1, "wmLp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x7beff800

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1471
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->updateLayout(Z)V

    .line 1482
    return-void
.end method

.method private setWindowBackground(Z)V
    .locals 2
    .param p1, "editMode"    # Z

    .prologue
    .line 861
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 863
    .local v0, "wmLp":Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_0

    .line 864
    const v1, 0x3e99999a    # 0.3f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 865
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 873
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 874
    return-void

    .line 869
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 870
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method private setupActionBarHomeView()V
    .locals 10

    .prologue
    .line 323
    const v8, 0x102002c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 324
    .local v4, "homeView":Landroid/view/View;
    if-nez v4, :cond_0

    .line 350
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionbarContainer()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 330
    .local v0, "actionbarContainer":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0700bc

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 332
    .local v5, "indiHeight":I
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 333
    .local v1, "aniLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 334
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 338
    .end local v1    # "aniLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "indiHeight":I
    :cond_1
    const/high16 v8, 0x7f0c0000

    invoke-virtual {p0, v8}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/easylauncher/Launcher;->mActionbarTitle:Landroid/widget/TextView;

    .line 339
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/Launcher;->mActionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v8, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09002a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 342
    .local v3, "edit":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09003a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 343
    .local v2, "button":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 344
    .local v6, "pageDescription":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/easylauncher/Launcher;->mActionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 346
    const v8, 0x7f0c0003

    invoke-virtual {p0, v8}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 347
    .local v7, "v":Landroid/view/View;
    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->addWidget()V

    goto :goto_0
.end method

.method private startAppsList()V
    .locals 3

    .prologue
    .line 1411
    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 1412
    const-string v1, "EasyLauncher.Launcher"

    const-string v2, "startAppsList apps list "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1414
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1415
    .local v0, "i":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1416
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1418
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->setMoreAppsMode(Z)V

    .line 1420
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1421
    return-void
.end method

.method private unBind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1999
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v1, :cond_0

    .line 2000
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/PageAdapter;->unBind()V

    .line 2002
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    .line 2004
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    if-eqz v1, :cond_1

    .line 2005
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->unBind()V

    .line 2007
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->widgetHandler:Landroid/os/Handler;

    .line 2008
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .line 2009
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    .line 2011
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2012
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2014
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    .line 2015
    return-void
.end method


# virtual methods
.method addWidget()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 1753
    const/4 v1, 0x0

    .line 1754
    .local v1, "hostView":Landroid/appwidget/AppWidgetHostView;
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    const/16 v7, 0x64

    invoke-virtual {v6, v8, v7}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    move-result-object v0

    .line 1756
    .local v0, "WidgetInfo":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    const v6, 0x7f0c0002

    invoke-virtual {p0, v6}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 1757
    .local v2, "layout":Landroid/widget/RelativeLayout;
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1759
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1762
    .local v3, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v0, :cond_0

    .line 1763
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getHostView()Landroid/appwidget/AppWidgetHostView;

    move-result-object v1

    .line 1765
    :cond_0
    if-nez v1, :cond_2

    .line 1769
    if-eqz v0, :cond_1

    .line 1770
    const v6, 0x7f03000c

    invoke-static {p0, v6, v10}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 1777
    .local v4, "mErrorView":Landroid/view/View;
    :goto_0
    invoke-virtual {v2, v4, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1778
    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1791
    .end local v4    # "mErrorView":Landroid/view/View;
    :goto_1
    return-void

    .line 1773
    :cond_1
    const v6, 0x7f03000b

    invoke-static {p0, v6, v10}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .restart local v4    # "mErrorView":Landroid/view/View;
    goto :goto_0

    .line 1780
    .end local v4    # "mErrorView":Landroid/view/View;
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getDefaultPadding()Landroid/graphics/Rect;

    move-result-object v5

    .line 1782
    .local v5, "r":Landroid/graphics/Rect;
    iget v6, v5, Landroid/graphics/Rect;->left:I

    neg-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1783
    iget v6, v5, Landroid/graphics/Rect;->right:I

    neg-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1784
    iget v6, v5, Landroid/graphics/Rect;->top:I

    neg-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1785
    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    neg-int v6, v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1786
    invoke-virtual {v1, v3}, Landroid/appwidget/AppWidgetHostView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1788
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1789
    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method closePopupMenu()V
    .locals 5

    .prologue
    .line 1926
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-nez v4, :cond_1

    .line 1942
    :cond_0
    :goto_0
    return-void

    .line 1929
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-eqz v4, :cond_2

    .line 1930
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/MainFragment;->dismisPopUps()V

    .line 1933
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/AppFragment;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v2, v0, v1

    .line 1934
    .local v2, "item":Lcom/sec/android/app/easylauncher/AppFragment;
    if-eqz v2, :cond_3

    .line 1935
    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AppFragment;->dismisPopUps()V

    .line 1933
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1939
    .end local v2    # "item":Lcom/sec/android/app/easylauncher/AppFragment;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    if-eqz v4, :cond_0

    .line 1940
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mHelpAppFragment:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/AppFragment;->dismisPopUps()V

    goto :goto_0
.end method

.method public createDragView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->createDragView(Landroid/view/View;)V

    .line 885
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 1589
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 1590
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1642
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1645
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 1624
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 1628
    :sswitch_2
    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1631
    const/4 v0, 0x0

    goto :goto_0

    .line 1636
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-eqz v0, :cond_1

    sget v0, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1637
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/easylauncher/MainFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1645
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1590
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x13 -> :sswitch_3
        0x14 -> :sswitch_3
        0x16 -> :sswitch_3
        0x52 -> :sswitch_2
        0x60 -> :sswitch_0
        0x61 -> :sswitch_0
        0x62 -> :sswitch_0
        0x63 -> :sswitch_0
        0x64 -> :sswitch_0
        0x65 -> :sswitch_0
        0x66 -> :sswitch_0
        0x67 -> :sswitch_0
        0x68 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6a -> :sswitch_0
        0x6b -> :sswitch_0
        0x6c -> :sswitch_0
        0x6d -> :sswitch_0
        0x6e -> :sswitch_0
        0xbc -> :sswitch_0
        0xbd -> :sswitch_0
        0xbe -> :sswitch_0
        0xbf -> :sswitch_0
        0xc0 -> :sswitch_0
        0xc1 -> :sswitch_0
        0xc2 -> :sswitch_0
        0xc3 -> :sswitch_0
        0xc4 -> :sswitch_0
        0xc5 -> :sswitch_0
        0xc6 -> :sswitch_0
        0xc7 -> :sswitch_0
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_0
        0xca -> :sswitch_0
        0xcb -> :sswitch_0
    .end sparse-switch
.end method

.method public exitHelp(Z)V
    .locals 3
    .param p1, "isBackPressed"    # Z

    .prologue
    .line 1845
    const-string v1, "EasyLauncher.Launcher"

    const-string v2, "exitHelp"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1846
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    .line 1848
    if-eqz p1, :cond_0

    .line 1849
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getLaunchIntentForHelpHub()Landroid/content/Intent;

    move-result-object v0

    .line 1850
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 1851
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1855
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForHelpmode()V

    .line 1856
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mIsEasyHelpComplete:Z

    .line 1857
    return-void
.end method

.method getActionbarContainer()Landroid/view/View;
    .locals 6

    .prologue
    .line 353
    const/4 v0, 0x0

    .line 355
    .local v0, "actionbarContainer":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "action_bar_container"

    const-string v4, "id"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 356
    .local v1, "resID":I
    if-eqz v1, :cond_0

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object v0, v2

    check-cast v0, Landroid/view/ViewGroup;

    .line 360
    :cond_0
    return-object v0
.end method

.method public getBadgeCache()Lcom/sec/android/app/easylauncher/lib/BadgeCache;
    .locals 1

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    return-object v0
.end method

.method public getMoreAppsMode()Z
    .locals 1

    .prologue
    .line 1665
    sget-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    return v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 1714
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getPageCount()I

    move-result v0

    return v0
.end method

.method public getPageOnOff()[Z
    .locals 1

    .prologue
    .line 1710
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getPageOnOff()[Z

    move-result-object v0

    return-object v0
.end method

.method isDeleteShortcut(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 888
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->resetTrashCan()V

    .line 889
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->checkOver(II)Z

    move-result v0

    return v0
.end method

.method isEasyHelpAppRunning()Z
    .locals 2

    .prologue
    .line 1896
    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEditModeTrue()Z
    .locals 1

    .prologue
    .line 1649
    sget-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    return v0
.end method

.method public isLongClickEditMode()Z
    .locals 1

    .prologue
    .line 1657
    sget-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->mLongClickEditMode:Z

    return v0
.end method

.method public isTalkBackOn()Z
    .locals 2

    .prologue
    .line 1723
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1724
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1725
    const/4 v1, 0x1

    .line 1728
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1428
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1430
    packed-switch p1, :pswitch_data_0

    .line 1444
    :cond_0
    :goto_0
    return-void

    .line 1432
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 1437
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mCallback:Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;

    if-eqz v0, :cond_0

    .line 1438
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mCallback:Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 1430
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1190
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1191
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1194
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    if-nez v4, :cond_2

    .line 1224
    :cond_1
    :goto_0
    return-void

    .line 1197
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-eqz v4, :cond_3

    .line 1198
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/easylauncher/MainFragment;->onBackPressed(Landroid/app/Activity;)V

    .line 1201
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    if-eqz v4, :cond_5

    .line 1202
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v2, v0, v1

    .line 1203
    .local v2, "item":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    if-eqz v2, :cond_4

    .line 1204
    invoke-virtual {v2, p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->onBackPressed(Landroid/app/Activity;)V

    .line 1202
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1208
    .end local v0    # "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    .end local v1    # "i$":I
    .end local v2    # "item":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    .end local v3    # "len$":I
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mAppFragment:[Lcom/sec/android/app/easylauncher/AppFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/AppFragment;
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v2, v0, v1

    .line 1209
    .local v2, "item":Lcom/sec/android/app/easylauncher/AppFragment;
    if-eqz v2, :cond_6

    .line 1210
    invoke-virtual {v2, p0}, Lcom/sec/android/app/easylauncher/AppFragment;->onBackPressed(Landroid/app/Activity;)V

    .line 1208
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1213
    .end local v2    # "item":Lcom/sec/android/app/easylauncher/AppFragment;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1214
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/Launcher;->exitHelp(Z)V

    goto :goto_0

    .line 1218
    :cond_8
    sget-boolean v4, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    if-eqz v4, :cond_1

    .line 1219
    sput-boolean v6, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 1221
    invoke-virtual {p0, v6}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForEditmode(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1129
    const/4 v1, 0x0

    .line 1131
    .local v1, "show":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1176
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v0, v2, :cond_1

    .line 1177
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorId:[I

    aget v2, v2, v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutId:[I

    aget v2, v2, v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 1178
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(IZ)V

    .line 1185
    .end local v0    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 1135
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->startEditMode(I)V

    goto :goto_1

    .line 1140
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1144
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v2, :cond_2

    .line 1145
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1148
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->startSetWallpaper()V

    goto :goto_1

    .line 1152
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1153
    sget-boolean v2, Lcom/sec/android/app/easylauncher/Launcher;->mIsEasyHelpComplete:Z

    if-eqz v2, :cond_1

    .line 1154
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpDialog(I)V

    .line 1156
    const v2, 0x7f090046

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpToast(I)V

    .line 1158
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setExitHelpMode()V

    goto :goto_1

    .line 1162
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1163
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->onBackPressed()V

    goto :goto_1

    .line 1168
    :sswitch_3
    const/4 v1, 0x1

    .line 1170
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/EasyController;

    sget v3, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/easylauncher/EasyController;->setPageOnOff(IZ)V

    .line 1172
    sget v2, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->setPageShowHideButton(I)V

    goto :goto_1

    .line 1176
    .restart local v0    # "i":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1131
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c0000 -> :sswitch_0
        0x7f0c0003 -> :sswitch_0
        0x7f0c000f -> :sswitch_0
        0x7f0c0011 -> :sswitch_1
        0x7f0c0045 -> :sswitch_2
        0x7f0c0049 -> :sswitch_3
        0x7f0c004a -> :sswitch_4
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 481
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 486
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 179
    if-eqz p1, :cond_0

    .line 180
    const-string v3, "mEditMode"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 181
    const-string v3, "mPosition"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    .line 182
    const-string v3, "mHelpMode"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    .line 183
    const-string v3, "EasyLauncher.Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "-----------mEditMode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mPosition : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mHelpMode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    const-string v3, "EasyLauncher.Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mPosition : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mHelpMode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f08000e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v4, :cond_5

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseLongClickRemove:Z

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f08000d

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v4, :cond_6

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    .line 193
    iget-boolean v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v3, :cond_7

    .line 194
    const v3, 0x7f030015

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->setContentView(I)V

    .line 205
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v6, 0x258

    if-ge v3, v6, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/EasyController;->canRotate()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 207
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->setRequestedOrientation(I)V

    .line 210
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/EasyController;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    .line 212
    new-instance v3, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->widgetHandler:Landroid/os/Handler;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getDefaultWidgetList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/easylauncher/EasyController;->setDefaultWidgetList(Ljava/util/ArrayList;)V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/EasyController;->setMoreAppsListState(Z)V

    .line 224
    const v3, 0x7f0c0036

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/CustomViewPager;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    .line 225
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setIndicatorTransparency()V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 232
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    new-instance v3, Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-direct {v3, v0, p0}, Lcom/sec/android/app/easylauncher/PageAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/sec/android/app/easylauncher/Launcher;)V

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    .line 235
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setAdapter(Lcom/sec/android/app/easylauncher/PageAdapter;)V

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v5

    invoke-virtual {v3, v5, v4}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(IZ)V

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageChangeListener:Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setOnPageChangeListener(Lcom/sec/android/app/easylauncher/CustomViewPager$OnPageChangeListener;)V

    .line 238
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v5, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setOffscreenPageLimit(I)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    new-instance v5, Lcom/sec/android/app/easylauncher/Launcher$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/easylauncher/Launcher$1;-><init>(Lcom/sec/android/app/easylauncher/Launcher;)V

    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 249
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v3

    sput v3, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    .line 252
    const v3, 0x7f0c0037

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorContainer:Landroid/widget/LinearLayout;

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    if-nez v3, :cond_3

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v3, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    new-array v3, v3, [Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    .line 258
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutView:[Landroid/view/View;

    if-nez v3, :cond_4

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v3, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    new-array v3, v3, [Landroid/view/View;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutView:[Landroid/view/View;

    .line 262
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v3, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v1, v3, :cond_8

    .line 263
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorId:[I

    aget v3, v3, v1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v5, v1

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v3, v3, v1

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutView:[Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutId:[I

    aget v5, v5, v1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v1

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v0    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v1    # "i":I
    :cond_5
    move v3, v5

    .line 190
    goto/16 :goto_0

    :cond_6
    move v3, v5

    .line 191
    goto/16 :goto_1

    .line 196
    :cond_7
    const v3, 0x7f030016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->setContentView(I)V

    .line 198
    const v3, 0x7f0c0048

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShowHide:Landroid/view/View;

    .line 199
    const v3, 0x7f0c0049

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    .line 200
    const v3, 0x7f0c004a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 270
    .restart local v0    # "fm":Landroid/support/v4/app/FragmentManager;
    .restart local v1    # "i":I
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicatorVisibility()V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicator(I)V

    .line 274
    const v3, 0x7f0c0045

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDoneButton:Landroid/widget/Button;

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    const v3, 0x7f0c0034

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/DeleteDragBar;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

    .line 280
    const v3, 0x7f0c0033

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/EasyContentLayout;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    .line 282
    iget-boolean v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v3, :cond_9

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setupDrawerView()V

    .line 289
    :goto_4
    sget-boolean v3, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForEditmode(Z)V

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/easylauncher/lib/Lib;->getSettingsInfo(Landroid/content/Context;)Lcom/sec/android/app/easylauncher/lib/AppInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mSettingsInfo:Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->getBadgeCache(Landroid/content/Context;)Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeCache:Lcom/sec/android/app/easylauncher/lib/BadgeCache;

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 311
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/app/easylauncher/lib/BadgeCache;->BADGE_URI:Landroid/net/Uri;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mBadgeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 314
    new-instance v4, Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/app/easylauncher/HelpGuider;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    .line 319
    return-void

    .line 286
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setupActionBarHomeView()V

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1340
    const-string v0, "EasyLauncher.Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateOptionsMenu mEditMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1341
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1343
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1344
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mOptionsMenu:Landroid/view/Menu;

    .line 1346
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 2020
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/Launcher;->unBind()V

    .line 2021
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 2022
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2023
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v7, 0x12

    const/4 v6, 0x7

    .line 1547
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1548
    .local v1, "handled":Z
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1550
    .local v0, "config":Landroid/content/res/Configuration;
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 1551
    if-lt p1, v6, :cond_3

    if-gt p1, v7, :cond_3

    .line 1553
    const/4 v3, 0x0

    .line 1554
    .local v3, "myIntentDial":Landroid/content/Intent;
    if-lt p1, v6, :cond_1

    const/16 v4, 0x10

    if-gt p1, v4, :cond_1

    .line 1556
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "myIntentDial":Landroid/content/Intent;
    const-string v4, "android.intent.action.DIAL"

    const-string v5, "tel:"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1569
    .restart local v3    # "myIntentDial":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1583
    .end local v3    # "myIntentDial":Landroid/content/Intent;
    :cond_0
    :goto_1
    return v1

    .line 1561
    .restart local v3    # "myIntentDial":Landroid/content/Intent;
    :cond_1
    if-ne p1, v7, :cond_2

    .line 1562
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "myIntentDial":Landroid/content/Intent;
    const-string v4, "android.intent.action.DIAL"

    const-string v5, "tel:"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1564
    .restart local v3    # "myIntentDial":Landroid/content/Intent;
    const-string v4, "isPoundKey"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 1566
    :cond_2
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "myIntentDial":Landroid/content/Intent;
    const-string v4, "android.intent.action.DIAL"

    const-string v5, "tel:"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v3    # "myIntentDial":Landroid/content/Intent;
    goto :goto_0

    .line 1572
    .end local v3    # "myIntentDial":Landroid/content/Intent;
    :cond_3
    const/16 v4, 0x1b

    if-ne p1, v4, :cond_4

    .line 1573
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1575
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v4, 0x4000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1576
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1577
    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1578
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    const/16 v4, 0x1c

    if-ne p1, v4, :cond_0

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v3, 0x400000

    const/4 v4, 0x0

    .line 1946
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1947
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    if-nez v1, :cond_1

    .line 1996
    :cond_0
    :goto_0
    return-void

    .line 1950
    :cond_1
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1951
    const-string v1, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1953
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "homescreen:guide_mode"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1955
    const-string v1, "EasyLauncher.Launcher"

    const-string v2, " help mode intent"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1956
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->startHelpMode(Landroid/content/Intent;)V

    goto :goto_0

    .line 1958
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/2addr v1, v3

    if-ne v1, v3, :cond_3

    if-eqz v0, :cond_0

    .line 1960
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1961
    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_4

    .line 1962
    const-string v1, "EasyLauncher.Launcher"

    const-string v2, " help mode is true"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    :cond_4
    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/Launcher;->exitHelp(Z)V

    .line 1967
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1968
    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_6

    .line 1969
    const-string v1, "EasyLauncher.Launcher"

    const-string v2, " edit mode is true"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1971
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->onBackPressed()V

    .line 1975
    :cond_7
    const-string v1, "EasyLauncher.Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent getMainHomeIndex mMoreAppsMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1976
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    .line 1978
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->closePopupMenu()V

    .line 1982
    if-eqz v0, :cond_9

    const-string v1, "com.android.launcher2.ALL_APPS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1983
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/Launcher;->startAppsList()V

    .line 1984
    const-string v1, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1991
    :cond_8
    :goto_1
    sput-boolean v4, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    goto/16 :goto_0

    .line 1987
    :cond_9
    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    if-nez v1, :cond_8

    .line 1988
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(IZ)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 1360
    iget-boolean v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v2, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1392
    :cond_0
    :goto_0
    return v1

    .line 1364
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1390
    const/4 v1, 0x0

    goto :goto_0

    .line 1366
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/easylauncher/lib/Lib;->expandStatusBar(Landroid/content/Context;)V

    goto :goto_0

    .line 1370
    :pswitch_1
    iget v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    if-ne v2, v3, :cond_0

    .line 1371
    const v2, 0x7f0c0057

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->startEditMode(I)V

    goto :goto_0

    .line 1375
    :pswitch_2
    invoke-static {p0}, Lcom/sec/android/app/easylauncher/lib/Lib;->startSearchIntent(Landroid/content/Context;)V

    goto :goto_0

    .line 1379
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1380
    .local v0, "launchIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mSettingsInfo:Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1381
    invoke-static {v0, p0}, Lcom/sec/android/app/easylauncher/lib/Lib;->launchApp(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1385
    .end local v0    # "launchIntent":Landroid/content/Intent;
    :pswitch_4
    iget v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    if-ne v2, v3, :cond_0

    .line 1386
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->startOnlineHelp()V

    goto :goto_0

    .line 1364
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0057
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x1

    .line 1397
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->isPaused:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpStep:I

    if-ne v0, v1, :cond_0

    .line 1398
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->exitHelp(Z)V

    .line 1400
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1287
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->isPaused:Z

    .line 1289
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 1290
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1295
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->setSystemUiTransparency(Z)V

    .line 1300
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 1301
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 472
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v4/app/ActionBarDrawerToggle;->syncState()V

    .line 477
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1351
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 1353
    const-string v0, "EasyLauncher.Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPrepareOptionsMenu mEditMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1237
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 1238
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->isPaused:Z

    .line 1240
    sget-boolean v2, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v2, :cond_0

    .line 1241
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v4}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 1245
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    if-ne v2, v4, :cond_1

    iget v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpStep:I

    if-ne v2, v4, :cond_1

    .line 1246
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->openOptionsMenu()V

    .line 1251
    :cond_1
    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/Launcher;->setSystemUiTransparency(Z)V

    .line 1256
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1257
    .local v1, "homeMode":Landroid/content/Intent;
    const-string v2, "com.android.launcher.action.HOME_MODE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1258
    const-string v2, "currentMode"

    const-string v3, "easylauncher"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1259
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 1262
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1263
    .local v0, "easyMode":Landroid/content/Intent;
    const-string v2, "com.android.launcher.action.ENTER_EASYLAUNCHER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1264
    const-string v2, "easyMode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1265
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 1282
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1328
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1329
    if-eqz p1, :cond_0

    .line 1330
    const-string v0, "EasyLauncher.Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState:mEditMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mHelpMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1332
    const-string v0, "mEditMode"

    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1333
    const-string v0, "mPosition"

    sget v1, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1334
    const-string v0, "mHelpMode"

    iget v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1336
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1228
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/sec/android/app/easylauncher/Launcher;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 1231
    return v1
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1305
    const-string v0, "EasyLauncher.Launcher"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1307
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 1309
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    .line 1310
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 1314
    const-string v1, "EasyLauncher.Launcher"

    const-string v2, "onStop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1317
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1318
    .local v0, "easyMode":Landroid/content/Intent;
    const-string v1, "com.android.launcher.action.ENTER_EASYLAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1319
    const-string v1, "easyMode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1320
    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 1322
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 1323
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 1534
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onWindowFocusChanged(Z)V

    .line 1535
    if-eqz p1, :cond_0

    .line 1543
    :cond_0
    return-void
.end method

.method public sayByTalkback(Ljava/lang/String;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1733
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isTalkBackOn()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1750
    :cond_0
    :goto_0
    return-void

    .line 1736
    :cond_1
    const-string v3, "accessibility"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1738
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1739
    const/16 v2, 0x20

    .line 1741
    .local v2, "eventType":I
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 1742
    .local v1, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1743
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 1744
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 1745
    const-string v3, ""

    invoke-virtual {v1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setBeforeText(Ljava/lang/CharSequence;)V

    .line 1746
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1748
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public selectCurrentPage(I)V
    .locals 3
    .param p1, "pageType"    # I

    .prologue
    .line 1698
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getIndex(I)I

    move-result v0

    .line 1700
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1701
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(I)V

    .line 1703
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_0

    .line 1704
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1707
    :cond_0
    return-void
.end method

.method setDrawerIcon(IZ)V
    .locals 6
    .param p1, "pageType"    # I
    .param p2, "show"    # Z

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v2

    .line 430
    .local v2, "onOff":[Z
    const/4 v3, 0x0

    .local v3, "start":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    .line 432
    .local v0, "end":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-eq p1, v4, :cond_0

    .line 433
    move v3, p1

    .line 434
    add-int/lit8 v0, v3, 0x1

    .line 437
    :cond_0
    move v1, v3

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 438
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    if-nez v4, :cond_2

    .line 464
    :cond_1
    return-void

    .line 442
    :cond_2
    if-eqz v1, :cond_3

    const/4 v4, 0x1

    if-ne v1, v4, :cond_5

    .line 443
    :cond_3
    aget-boolean v4, v2, v1

    if-eqz v4, :cond_4

    .line 444
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const v5, 0x7f020009

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 437
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 446
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const v5, 0x7f020008

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 448
    :cond_5
    const/4 v4, 0x2

    if-ne v1, v4, :cond_7

    .line 449
    aget-boolean v4, v2, v1

    if-eqz v4, :cond_6

    .line 450
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const v5, 0x7f02000b

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 452
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const v5, 0x7f02000a

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 455
    :cond_7
    aget-boolean v4, v2, v1

    if-eqz v4, :cond_8

    .line 456
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const v5, 0x7f020007

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 458
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const v5, 0x7f020006

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public setEditMode(Z)V
    .locals 0
    .param p1, "editMode"    # Z

    .prologue
    .line 1653
    sput-boolean p1, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 1654
    return-void
.end method

.method public setExitHelpMode()V
    .locals 4

    .prologue
    .line 1917
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/easylauncher/Launcher$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/Launcher$6;-><init>(Lcom/sec/android/app/easylauncher/Launcher;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1923
    return-void
.end method

.method setHelpResult(Z)V
    .locals 2
    .param p1, "complete"    # Z

    .prologue
    .line 1900
    sput-boolean p1, Lcom/sec/android/app/easylauncher/Launcher;->mIsEasyHelpComplete:Z

    .line 1902
    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1903
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpDialog(I)V

    .line 1904
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v0, :cond_0

    .line 1905
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setExitHelpMode()V

    .line 1910
    :cond_0
    :goto_0
    return-void

    .line 1907
    :cond_1
    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1908
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setExitHelpMode()V

    goto :goto_0
.end method

.method public setMoreAppsMode(Z)V
    .locals 0
    .param p1, "moreAppsMode"    # Z

    .prologue
    .line 1661
    sput-boolean p1, Lcom/sec/android/app/easylauncher/Launcher;->mMoreAppsMode:Z

    .line 1662
    return-void
.end method

.method public setOnListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;)V
    .locals 1
    .param p1, "dropListener"    # Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;
    .param p2, "addViewListener"    # Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    .prologue
    .line 879
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->setOnDropListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;)V

    .line 880
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->setOnAddViewListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;)V

    .line 881
    return-void
.end method

.method setPageIndicator(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 636
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v1, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v0, v1, :cond_0

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    .line 665
    :cond_0
    return-void

    .line 640
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/widget/TextView;->clearFocus()V

    .line 642
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const v2, 0x7f020011

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 649
    :goto_1
    if-ne v0, p1, :cond_2

    .line 650
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getMainHomeIndex()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 651
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const v2, 0x7f020012

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 657
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    .line 658
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isTalkBackOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 636
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 646
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const v2, 0x7f020018

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1

    .line 654
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const v2, 0x7f020019

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public setPageIndicatorVisibility()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 609
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v1, v4, :cond_1

    .line 610
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 611
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 612
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutView:[Landroid/view/View;

    aget-object v4, v4, v1

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 620
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090040

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 621
    .local v2, "page":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09003b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 622
    .local v0, "action":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 623
    .local v3, "pageDescription":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 609
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 615
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "page":Ljava/lang/String;
    .end local v3    # "pageDescription":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorTextView:[Landroid/widget/TextView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorLayoutView:[Landroid/view/View;

    aget-object v4, v4, v1

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 625
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 626
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v4, v9}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageScrollEnable(Z)V

    .line 628
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 629
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v4, v8}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageScrollEnable(Z)V

    .line 632
    :cond_2
    return-void
.end method

.method public setPageShow(IZ)V
    .locals 5
    .param p1, "pageType"    # I
    .param p2, "show"    # Z

    .prologue
    .line 1669
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/easylauncher/PageAdapter;->getIndex(I)I

    move-result v0

    .line 1671
    .local v0, "index":I
    sget v2, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    if-ne v2, v0, :cond_1

    const/4 v1, 0x1

    .line 1673
    .local v1, "pageChange":Z
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/app/easylauncher/EasyController;->setPageOnOff(IZ)V

    .line 1675
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyDataSetChanged()V

    .line 1676
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicatorVisibility()V

    .line 1678
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/easylauncher/Launcher;->setDrawerIcon(IZ)V

    .line 1680
    const-string v2, "EasyLauncher.Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "2 setPageShow Launcher.mPosition : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " index : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1687
    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    .line 1688
    if-lez v0, :cond_2

    .line 1689
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(I)V

    .line 1695
    :cond_0
    :goto_1
    return-void

    .line 1671
    .end local v1    # "pageChange":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1691
    .restart local v1    # "pageChange":Z
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(I)V

    goto :goto_1
.end method

.method setPageShowHideButton(I)V
    .locals 14
    .param p1, "position"    # I

    .prologue
    const v13, 0x7f090030

    const v12, 0x7f020003

    const/4 v11, 0x4

    const/4 v10, 0x0

    const v9, 0x7f020002

    .line 668
    iget-boolean v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v7, :cond_0

    sget-boolean v7, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    if-nez v7, :cond_1

    .line 736
    :cond_0
    :goto_0
    return-void

    .line 672
    :cond_1
    const/high16 v2, -0x1000000

    .line 673
    .local v2, "TEXT_COLOR_SHOW":I
    const v1, -0xa0a0b

    .line 674
    .local v1, "TEXT_COLOR_NORMAL":I
    const v0, -0x660a0a0b

    .line 676
    .local v0, "TEXT_COLOR_DIM":I
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 677
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 679
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 680
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 682
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x2

    if-eq p1, v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x3

    if-ne p1, v7, :cond_3

    .line 683
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 684
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 685
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 686
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 690
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 691
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 693
    const v7, 0x7f09003a

    invoke-virtual {p0, v7}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 697
    .local v3, "button":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v4

    .line 698
    .local v4, "onOff":[Z
    aget-boolean v7, v4, p1

    if-eqz v7, :cond_4

    .line 701
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 702
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 704
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 705
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 707
    invoke-virtual {p0, v13}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 708
    .local v6, "text":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f09003c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 710
    .local v5, "pageDescription":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 712
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f090031

    invoke-virtual {p0, v8}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 713
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 734
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    goto/16 :goto_0

    .line 718
    .end local v5    # "pageDescription":Ljava/lang/String;
    .end local v6    # "text":Ljava/lang/String;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 719
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 721
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 722
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 724
    const v7, 0x7f090031

    invoke-virtual {p0, v7}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 725
    .restart local v6    # "text":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f090042

    invoke-virtual {p0, v8}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 727
    .restart local v5    # "pageDescription":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShow:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 729
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v13}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 730
    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageHide:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method setSystemUiTransparency(Z)V
    .locals 8
    .param p1, "bTransparent"    # Z

    .prologue
    const v7, 0x7fffffff

    const/high16 v6, 0x40000000    # 2.0f

    const v5, -0x8001

    .line 1490
    const-string v3, "statusbar"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/StatusBarManager;

    iput-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mStatusBarManager:Landroid/app/StatusBarManager;

    .line 1491
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-nez v3, :cond_0

    .line 1529
    :goto_0
    return-void

    .line 1494
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 1496
    .local v2, "wmLp":Landroid/view/WindowManager$LayoutParams;
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 1497
    const/4 v1, -0x1

    .line 1499
    .local v1, "wallpaper_transparent":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android.wallpaper.settings_systemui_transparency"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1507
    :goto_1
    if-eqz v1, :cond_1

    .line 1508
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v4, -0x80000000

    or-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1510
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const v4, -0x40000001    # -1.9999999f

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1511
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const v4, 0x8000

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1528
    .end local v1    # "wallpaper_transparent":I
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0

    .line 1501
    .restart local v1    # "wallpaper_transparent":I
    :catch_0
    move-exception v0

    .line 1502
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v3, "EasyLauncher.Launcher"

    const-string v4, "setSystemUiTransparency.SettingNotFoundException : set as TRUE"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1504
    const/4 v1, 0x1

    goto :goto_1

    .line 1514
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v3, v7

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1516
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1517
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_2

    .line 1522
    .end local v1    # "wallpaper_transparent":I
    :cond_2
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v3, v7

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1524
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1525
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_2
.end method

.method setupDrawerView()V
    .locals 8

    .prologue
    .line 365
    const v0, 0x7f0c0032

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 366
    const v0, 0x7f0c000d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerList:Landroid/widget/ListView;

    .line 367
    const v0, 0x7f0c0046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    .line 369
    new-instance v0, Lcom/sec/android/app/easylauncher/DrawerAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/DrawerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 374
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setupActionBarHomeView()V

    .line 376
    new-instance v0, Lcom/sec/android/app/easylauncher/Launcher$2;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v4, 0x7f02004f

    const v5, 0x7f090003

    const v6, 0x7f090002

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/easylauncher/Launcher$2;-><init>(Lcom/sec/android/app/easylauncher/Launcher;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f0c000f

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mEditShort:Landroid/view/View;

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mEditShortTv:Landroid/widget/TextView;

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mEditShort:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f0c0011

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mSetWallpaper:Landroid/view/View;

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mSetWallpaper:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    .line 418
    :cond_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    if-ge v7, v0, :cond_1

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconImageView:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->drawerIconId:[I

    aget v2, v2, v7

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v7

    .line 418
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mApp:Lcom/sec/android/app/easylauncher/EasyController;

    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->MAX_PAGE_COUNT:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->setDrawerIcon(IZ)V

    .line 424
    return-void
.end method

.method setupViewForEditmode(Z)V
    .locals 4
    .param p1, "editMode"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v0, :cond_1

    .line 740
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForPageEdit(Z)V

    .line 784
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->updatePageIndicatorLayout(Z)V

    .line 746
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->setWindowBackground(Z)V

    .line 748
    if-eqz p1, :cond_2

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setEditMode(Z)V

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyRefreshFragment()V

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mEditShortTv:Landroid/widget/TextView;

    const v1, -0x949495

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 758
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 760
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 762
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 764
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpDialog(I)V

    goto :goto_0

    .line 766
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setEditMode(Z)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDoneButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mEditShortTv:Landroid/widget/TextView;

    const v1, -0xa0a0b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 774
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 776
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 779
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method

.method setupViewForHelpmode()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 958
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyDataSetChanged()V

    .line 959
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageScrollEnable(Z)V

    .line 961
    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    packed-switch v0, :pswitch_data_0

    .line 1014
    :cond_0
    :goto_0
    return-void

    .line 964
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 966
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mActionbarTitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 968
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0

    .line 974
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v0, :cond_3

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    if-eqz v0, :cond_1

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/DrawerAdapter;->notifyDataSetChanged()V

    .line 978
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_2

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 985
    :cond_2
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpDialog(I)V

    goto :goto_0

    .line 983
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->openOptionsMenu()V

    goto :goto_1

    .line 989
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/HelpGuider;->dismissShowHelpDialog()V

    .line 991
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->closeOptionsMenu()V

    .line 993
    sput-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 995
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v0, :cond_4

    .line 996
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 998
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mActionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1000
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_5

    .line 1001
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1003
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    if-eqz v0, :cond_6

    .line 1004
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerAdapter:Lcom/sec/android/app/easylauncher/DrawerAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/DrawerAdapter;->notifyDataSetChanged()V

    .line 1007
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyDataSetChanged()V

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageScrollEnable(Z)V

    .line 1010
    sget-boolean v0, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForEditmode(Z)V

    goto :goto_0

    .line 961
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setupViewForLongClickMode(Z)V
    .locals 6
    .param p1, "longClick"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 894
    sget-boolean v1, Lcom/sec/android/app/easylauncher/Launcher;->mLongClickEditMode:Z

    if-ne v1, p1, :cond_1

    .line 954
    :cond_0
    :goto_0
    return-void

    .line 898
    :cond_1
    sput-boolean p1, Lcom/sec/android/app/easylauncher/Launcher;->mLongClickEditMode:Z

    .line 903
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionbarContainer()Landroid/view/View;

    move-result-object v0

    .line 905
    .local v0, "actionbarContainer":Landroid/view/View;
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->setWindowBackground(Z)V

    .line 907
    if-eqz p1, :cond_3

    .line 910
    if-eqz v0, :cond_2

    .line 911
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 914
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setVisibility(I)V

    .line 916
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

    iget-object v2, v2, Lcom/sec/android/app/easylauncher/DeleteDragBar;->onContentDragListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->setOnDragListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDragListener;)V

    .line 919
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 921
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setEditMode(Z)V

    .line 923
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageScrollEnable(Z)V

    .line 925
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_0

    .line 926
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v4}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0

    .line 932
    :cond_3
    if-eqz v0, :cond_4

    .line 933
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 936
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDeleteDragBar:Lcom/sec/android/app/easylauncher/DeleteDragBar;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/easylauncher/DeleteDragBar;->setVisibility(I)V

    .line 938
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 940
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setEditMode(Z)V

    .line 942
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setPageScrollEnable(Z)V

    .line 944
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    .line 946
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_5

    .line 947
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 950
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mContentLayout:Lcom/sec/android/app/easylauncher/EasyContentLayout;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/EasyContentLayout;->cancelDrag()V

    goto :goto_0
.end method

.method setupViewForPageEdit(Z)V
    .locals 11
    .param p1, "editMode"    # Z

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 788
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->updateLayout(Z)V

    .line 789
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->updatePageIndicatorLayout(Z)V

    .line 794
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getActionbarContainer()Landroid/view/View;

    move-result-object v0

    .line 796
    .local v0, "actionbarContainer":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    sget v7, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {v6, v7}, Lcom/sec/android/app/easylauncher/PageAdapter;->getEditIndex(I)I

    move-result v5

    .line 798
    .local v5, "position":I
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyDataSetChanged()V

    .line 800
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicatorVisibility()V

    .line 802
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/Launcher;->setWindowBackground(Z)V

    .line 804
    if-eqz p1, :cond_4

    .line 806
    if-eqz v0, :cond_0

    .line 807
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 810
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 811
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShowHide:Landroid/view/View;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 817
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setEditMode(Z)V

    .line 819
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(I)V

    .line 820
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicator(I)V

    .line 822
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyRefreshFragment()V

    .line 824
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/Launcher;->setPageShowHideButton(I)V

    .line 826
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v6, v6, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    if-eqz v6, :cond_3

    .line 827
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v1, v6, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .local v1, "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v2, v1, v3

    .line 828
    .local v2, "contacts":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    if-eqz v2, :cond_1

    .line 829
    invoke-virtual {v2, v9, p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(ZLandroid/app/Activity;)V

    .line 827
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 814
    .end local v1    # "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    .end local v2    # "contacts":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShowHide:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 833
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p0, v6}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpDialog(I)V

    .line 857
    :goto_2
    return-void

    .line 836
    :cond_4
    if-eqz v0, :cond_5

    .line 837
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 840
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageShowHide:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 842
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setEditMode(Z)V

    .line 844
    sget v6, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/easylauncher/PageAdapter;->getCount()I

    move-result v7

    if-lt v6, v7, :cond_6

    .line 845
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    sget v7, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/easylauncher/CustomViewPager;->setCurrentItem(I)V

    .line 851
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/PageAdapter;->notifyRefreshFragment()V

    .line 853
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->pager:Lcom/sec/android/app/easylauncher/CustomViewPager;

    invoke-virtual {v6}, Lcom/sec/android/app/easylauncher/CustomViewPager;->invalidate()V

    goto :goto_2

    .line 848
    :cond_6
    sget v6, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    invoke-virtual {p0, v6}, Lcom/sec/android/app/easylauncher/Launcher;->setPageIndicator(I)V

    goto :goto_3
.end method

.method showHelpDialog(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 1048
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    if-nez v0, :cond_1

    .line 1049
    :cond_0
    const-string v0, "EasyLauncher.Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHelpGuider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    :goto_0
    :pswitch_0
    return-void

    .line 1053
    :cond_1
    iput p1, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpStep:I

    .line 1055
    iget v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1060
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-nez v0, :cond_2

    .line 1061
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 1062
    const/4 p1, 0x0

    .line 1070
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpGuider:Lcom/sec/android/app/easylauncher/HelpGuider;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/HelpGuider;->showHelpDialog_removeapps(I)V

    goto :goto_0

    .line 1064
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 1065
    const/4 p1, 0x4

    .line 1066
    const v0, 0x7f090046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpToast(I)V

    goto :goto_1

    .line 1055
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method showHelpToast(I)V
    .locals 1
    .param p1, "string"    # I

    .prologue
    .line 1913
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1914
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I
    .param p3, "callback"    # Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;

    .prologue
    .line 1448
    iput-object p3, p0, Lcom/sec/android/app/easylauncher/Launcher;->mCallback:Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;

    .line 1449
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/easylauncher/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1450
    return-void
.end method

.method startEditMode(I)V
    .locals 7
    .param p1, "id"    # I

    .prologue
    const/4 v6, 0x1

    .line 1082
    sparse-switch p1, :sswitch_data_0

    .line 1125
    :cond_0
    :goto_0
    return-void

    .line 1086
    :sswitch_0
    iget-boolean v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v4, :cond_2

    .line 1087
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1088
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    goto :goto_0

    .line 1090
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    goto :goto_0

    .line 1095
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1096
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/Launcher;->showHelpDialog(I)V

    .line 1100
    :cond_3
    :sswitch_1
    sget-boolean v4, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    if-nez v4, :cond_0

    .line 1104
    sput-boolean v6, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 1106
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    if-eqz v4, :cond_5

    .line 1107
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_5

    aget-object v1, v0, v2

    .line 1108
    .local v1, "contacts":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    if-eqz v1, :cond_4

    .line 1109
    invoke-virtual {v1, v6, p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->startContactEditMode(ZLandroid/app/Activity;)V

    .line 1107
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1113
    .end local v0    # "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    .end local v1    # "contacts":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v4, :cond_6

    .line 1114
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1115
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v4, v6}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0

    .line 1118
    :cond_6
    invoke-virtual {p0, v6}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForEditmode(Z)V

    goto :goto_0

    .line 1082
    :sswitch_data_0
    .sparse-switch
        0x7f0c0000 -> :sswitch_0
        0x7f0c0003 -> :sswitch_0
        0x7f0c000f -> :sswitch_1
        0x7f0c0057 -> :sswitch_0
    .end sparse-switch
.end method

.method startHelpMode(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1017
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1018
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/Launcher;->exitHelp(Z)V

    .line 1021
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1022
    sput-boolean v5, Lcom/sec/android/app/easylauncher/Launcher;->mEditMode:Z

    .line 1023
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForEditmode(Z)V

    .line 1026
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->closePopupMenu()V

    .line 1028
    const-string v2, "help_activity_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1029
    .local v0, "helpActivityName":Ljava/lang/String;
    const-string v2, "ExternalHelpActivity"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1030
    iput-boolean v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mIsExternalHelpActivity:Z

    .line 1033
    :cond_2
    const-string v2, "homescreen:guide_mode"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1034
    .local v1, "helpMode":Ljava/lang/String;
    const-string v2, "EasyLauncher.Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "helpMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    const-string v2, "addapps"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1036
    iput v5, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    .line 1043
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->setupViewForHelpmode()V

    .line 1045
    return-void

    .line 1037
    :cond_3
    const-string v2, "remove_items"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1038
    iput v6, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    goto :goto_0

    .line 1040
    :cond_4
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/easylauncher/Launcher;->mHelpMode:I

    goto :goto_0
.end method

.method public startOnlineHelp()V
    .locals 7

    .prologue
    .line 1798
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.samsung.helphub"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1799
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v1, v4, 0xa

    .line 1800
    .local v1, "helpVersionCode":I
    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 1841
    .end local v1    # "helpVersionCode":I
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-void

    .line 1805
    .restart local v1    # "helpVersionCode":I
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 1810
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1811
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "helphub:section"

    const-string v5, "easy_mode"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812
    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1836
    .end local v1    # "helpVersionCode":I
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1838
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1814
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "helpVersionCode":I
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_2
    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 1822
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1823
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v4, "helphub:appid"

    const-string v5, "easy_mode"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1834
    invoke-virtual {p0, v3}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method startSetWallpaper()V
    .locals 2

    .prologue
    .line 1404
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1405
    .local v0, "pickWallpaper":Landroid/content/Intent;
    const/high16 v1, 0x7f090000

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1408
    return-void
.end method

.method startWidgetBinding()V
    .locals 5

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->addWidget()V

    .line 491
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    if-nez v4, :cond_1

    .line 504
    :cond_0
    return-void

    .line 494
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    if-eqz v4, :cond_2

    .line 495
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mainFragment:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/easylauncher/MainFragment;->addWidget(Landroid/app/Activity;)V

    .line 498
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    if-eqz v4, :cond_0

    .line 499
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    iget-object v0, v4, Lcom/sec/android/app/easylauncher/PageAdapter;->mContactGridFragment:[Lcom/sec/android/app/easylauncher/ContactGridFragment;

    .local v0, "arr$":[Lcom/sec/android/app/easylauncher/ContactGridFragment;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 500
    .local v2, "item":Lcom/sec/android/app/easylauncher/ContactGridFragment;
    if-eqz v2, :cond_3

    .line 501
    invoke-virtual {v2, p0}, Lcom/sec/android/app/easylauncher/ContactGridFragment;->addWidget(Landroid/app/Activity;)V

    .line 499
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public updateLayout(Z)V
    .locals 7
    .param p1, "editMode"    # Z

    .prologue
    const/high16 v6, 0x7f070000

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700bc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    add-int v2, v4, v5

    .line 593
    .local v2, "indiHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 595
    .local v0, "actionbarHeight":I
    const v4, 0x7f0c0033

    invoke-virtual {p0, v4}, Lcom/sec/android/app/easylauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 596
    .local v3, "layout":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 597
    .local v1, "aniLp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p1, :cond_0

    .line 598
    const/4 v4, 0x0

    iput v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 604
    :goto_0
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 605
    return-void

    .line 601
    :cond_0
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_0
.end method

.method public updatePageIndicatorLayout(Z)V
    .locals 6
    .param p1, "editMode"    # Z

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 576
    .local v1, "margin":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700ad

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 578
    .local v2, "marginEdit":I
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 580
    .local v3, "marginQuickEdit":I
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 582
    .local v0, "aniLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-boolean v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->mUseDrawerMenu:Z

    if-eqz v4, :cond_1

    .line 583
    if-eqz p1, :cond_0

    .end local v2    # "marginEdit":I
    :goto_0
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 588
    .end local v3    # "marginQuickEdit":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/Launcher;->pageIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 589
    return-void

    .restart local v2    # "marginEdit":I
    .restart local v3    # "marginQuickEdit":I
    :cond_0
    move v2, v1

    .line 583
    goto :goto_0

    .line 585
    :cond_1
    if-eqz p1, :cond_2

    .end local v3    # "marginQuickEdit":I
    :goto_2
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_1

    .restart local v3    # "marginQuickEdit":I
    :cond_2
    move v3, v1

    goto :goto_2
.end method

.method public widgetBindPackagesUpdated()V
    .locals 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/Launcher;->mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidgetsFromDb()V

    .line 510
    const-string v0, "EasyLauncher.Launcher"

    const-string v1, "widgetBindPackagesUpdated: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/Launcher;->startWidgetBinding()V

    .line 514
    :cond_0
    return-void
.end method
