.class Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "AlphabeticalSearchListViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 338
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/EasyController;

    .line 339
    .local v0, "appContext":Lcom/sec/android/app/easylauncher/EasyController;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    if-eqz v1, :cond_1

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mApps:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mApps:Ljava/util/ArrayList;

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mApps:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentSkipListMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 344
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mAdapter:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$AppAdapter;->runFilter()V

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getAdapter()Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity$2;->this$0:Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/AlphabeticalSearchListViewActivity;->mListView:Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/AlphabeticalListContainer;->getAdapter()Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/MenuAppsListAdapter;->notifyDataSetChanged()V

    .line 350
    :cond_1
    return-void
.end method
