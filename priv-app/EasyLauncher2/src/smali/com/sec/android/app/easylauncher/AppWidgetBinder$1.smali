.class Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;
.super Ljava/lang/Object;
.source "AppWidgetBinder.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/Launcher$ActivityResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/easylauncher/AppWidgetBinder;->allocateBindNew(Landroid/content/ComponentName;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

.field final synthetic val$callback:Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;

.field final synthetic val$widgetId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AppWidgetBinder;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->this$0:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

    iput-object p2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->val$callback:Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;

    iput p3, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->val$widgetId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 77
    const-string v0, "AppWidgetBinder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "allocateBindNew sdata : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 79
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->this$0:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->val$callback:Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;

    iget v2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->val$widgetId:I

    # invokes: Lcom/sec/android/app/easylauncher/AppWidgetBinder;->onBindSuccessful(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->access$000(Lcom/sec/android/app/easylauncher/AppWidgetBinder;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->this$0:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->val$callback:Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;

    iget v2, p0, Lcom/sec/android/app/easylauncher/AppWidgetBinder$1;->val$widgetId:I

    # invokes: Lcom/sec/android/app/easylauncher/AppWidgetBinder;->onBindFailed(Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->access$100(Lcom/sec/android/app/easylauncher/AppWidgetBinder;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;I)V

    goto :goto_0

    .line 84
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AppWidgetBinder.allocateBindNew: invalid requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
