.class Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/SearchActivity$Filter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/SearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/SearchActivity;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filterWidgets()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v2, "out":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const-string v3, ""

    .line 196
    .local v3, "search":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    if-eqz v4, :cond_0

    .line 197
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/SearchActivity;->mSearchEdit:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 198
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    if-eqz v4, :cond_1

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/SearchActivity;->mAdapter:Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/easylauncher/SearchActivity$AppAdapter;->setSearchText(Ljava/lang/String;)V

    .line 200
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/SearchActivity$SearchFilter;->this$0:Lcom/sec/android/app/easylauncher/SearchActivity;

    iget-object v4, v4, Lcom/sec/android/app/easylauncher/SearchActivity;->mApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .line 201
    .local v0, "apps":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 202
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 205
    .end local v0    # "apps":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_3
    return-object v2
.end method
