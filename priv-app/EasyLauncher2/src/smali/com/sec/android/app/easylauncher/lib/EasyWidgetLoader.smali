.class public Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;
.super Ljava/lang/Object;
.source "EasyWidgetLoader.java"


# static fields
.field static final CSC_FILE_DEFAULT_WORKSPACE:Ljava/lang/String; = "/system/csc/easylauncher_default_widgetlist.xml"

.field private static final DEBUGGABLE:Z

.field private static final TAG_APP_WIDGETS:Ljava/lang/String; = "appwidget"

.field private static final TAG_ATTR_CLASSNAME:Ljava/lang/String; = "className"

.field private static final TAG_ATTR_PACKAGENAME:Ljava/lang/String; = "packageName"

.field private static final TAG_ATTR_POSITION:Ljava/lang/String; = "iconPosition"

.field private static final TAG_ATTR_SCREEN:Ljava/lang/String; = "screen"

.field private static final TAG_ATTR_SPANX:Ljava/lang/String; = "spanX"

.field private static final TAG_ATTR_VIEWTYPE:Ljava/lang/String; = "viewType"

.field private static final TAG_ATTR_VISIBILITY:Ljava/lang/String; = "visibility"

.field private static final TAG_FAVORITES:Ljava/lang/String; = "favorites"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field mWidgetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 47
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->DEBUGGABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EasyLauncher.EasyWidgetLoader"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->TAG:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->mWidgetList:Ljava/util/ArrayList;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->mContext:Landroid/content/Context;

    .line 74
    return-void
.end method

.method private getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;
    .locals 10
    .param p1, "cscFilePath"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v1, "cscFileChk":Ljava/io/File;
    const/4 v0, 0x0

    .line 79
    .local v0, "cscFile":Ljava/io/FileReader;
    const/4 v4, 0x0

    .line 81
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-boolean v5, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->DEBUGGABLE:Z

    if-eqz v5, :cond_0

    .line 82
    const-string v5, "EasyLauncher.EasyWidgetLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " CSC File Path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 86
    :try_start_0
    new-instance v0, Ljava/io/FileReader;

    .end local v0    # "cscFile":Ljava/io/FileReader;
    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    :goto_0
    if-eqz v0, :cond_1

    .line 96
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 97
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 98
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 99
    invoke-interface {v4, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 100
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 116
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_1
    :goto_1
    return-object v4

    .line 87
    .end local v0    # "cscFile":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 89
    .local v2, "e":Ljava/io/FileNotFoundException;
    const/4 v0, 0x0

    .line 90
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 108
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catch_1
    move-exception v2

    .line 110
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 101
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catch_2
    move-exception v2

    .line 103
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const/4 v4, 0x0

    .line 104
    :try_start_3
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 107
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    .line 108
    :catch_3
    move-exception v2

    .line 110
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 106
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 107
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 111
    :goto_2
    throw v5

    .line 108
    :catch_4
    move-exception v2

    .line 110
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public getEasyWidgetList()Ljava/util/ArrayList;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-boolean v25, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->DEBUGGABLE:Z

    if-eqz v25, :cond_0

    .line 121
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "loadWidgetListFromXML"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_0
    const/4 v9, 0x0

    .line 126
    .local v9, "fileReader":Ljava/io/FileReader;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v8

    .line 127
    .local v8, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v8, v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 128
    const/4 v14, 0x0

    .local v14, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v5, 0x0

    .line 129
    .local v5, "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v18, 0x0

    .line 130
    .local v18, "resParser":Landroid/content/res/XmlResourceParser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 131
    .local v16, "res":Landroid/content/res/Resources;
    const/high16 v17, 0x7f050000

    .line 134
    .local v17, "resId":I
    if-nez v5, :cond_6

    .line 135
    sget-boolean v25, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->DEBUGGABLE:Z

    if-eqz v25, :cond_1

    .line 136
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "CSC is not available"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_1
    const/high16 v17, 0x7f050000

    .line 138
    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v18

    .line 139
    move-object/from16 v14, v18

    .line 146
    .end local v14    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_0
    const-string v25, "favorites"

    move-object/from16 v0, v25

    invoke-static {v14, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 147
    invoke-interface {v14}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    .line 150
    .local v6, "depth":I
    :cond_2
    :goto_1
    invoke-interface {v14}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v22

    .local v22, "type":I
    const/16 v25, 0x3

    move/from16 v0, v22

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    invoke-interface {v14}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v25

    move/from16 v0, v25

    if-le v0, v6, :cond_4

    :cond_3
    const/16 v25, 0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_4

    .line 151
    const/16 v25, 0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 242
    :cond_4
    if-eqz v9, :cond_5

    .line 244
    :try_start_1
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 251
    .end local v5    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v6    # "depth":I
    .end local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "res":Landroid/content/res/Resources;
    .end local v17    # "resId":I
    .end local v18    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v22    # "type":I
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->mWidgetList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    return-object v25

    .line 141
    .restart local v5    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v14    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v16    # "res":Landroid/content/res/Resources;
    .restart local v17    # "resId":I
    .restart local v18    # "resParser":Landroid/content/res/XmlResourceParser;
    :cond_6
    :try_start_2
    sget-boolean v25, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->DEBUGGABLE:Z

    if-eqz v25, :cond_7

    .line 142
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "CSC is available"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_7
    move-object v14, v5

    .local v14, "parser":Ljava/lang/Object;
    goto :goto_0

    .line 155
    .end local v14    # "parser":Ljava/lang/Object;
    .restart local v6    # "depth":I
    .restart local v22    # "type":I
    :cond_8
    const/16 v25, 0x2

    move/from16 v0, v22

    move/from16 v1, v25

    if-ne v0, v1, :cond_2

    .line 159
    const/4 v13, 0x0

    .line 160
    .local v13, "packageName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 161
    .local v4, "className":Ljava/lang/String;
    const/16 v19, 0x0

    .line 162
    .local v19, "screen":I
    const/4 v15, 0x0

    .line 163
    .local v15, "position":I
    const/16 v21, 0x0

    .line 164
    .local v21, "spanX":I
    const/16 v24, 0x0

    .line 165
    .local v24, "visibility":I
    const/16 v23, 0x0

    .line 167
    .local v23, "viewType":Ljava/lang/String;
    invoke-interface {v14}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    .line 169
    .local v11, "name":Ljava/lang/String;
    if-eqz v11, :cond_2

    .line 170
    invoke-interface {v14}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v20

    .line 172
    .local v20, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    move/from16 v0, v20

    if-ge v10, v0, :cond_11

    .line 173
    invoke-interface {v14, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "attrName":Ljava/lang/String;
    invoke-interface {v14, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "attrValue":Ljava/lang/String;
    if-eqz v2, :cond_9

    const-string v25, "packageName"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 177
    move-object v13, v3

    .line 180
    :cond_9
    if-eqz v2, :cond_a

    const-string v25, "className"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 181
    move-object v4, v3

    .line 184
    :cond_a
    if-eqz v2, :cond_b

    const-string v25, "screen"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 185
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 188
    :cond_b
    if-eqz v2, :cond_c

    const-string v25, "spanX"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 189
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 192
    :cond_c
    if-eqz v2, :cond_d

    const-string v25, "iconPosition"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 193
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 196
    :cond_d
    if-eqz v2, :cond_e

    const-string v25, "viewType"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 197
    move-object/from16 v23, v3

    .line 200
    :cond_e
    if-eqz v2, :cond_f

    const-string v25, "visibility"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 201
    const-string v25, "true"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 202
    const/16 v24, 0x1

    .line 172
    :cond_f
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 204
    :cond_10
    const/16 v24, 0x0

    goto :goto_4

    .line 209
    .end local v2    # "attrName":Ljava/lang/String;
    .end local v3    # "attrValue":Ljava/lang/String;
    :cond_11
    sget-boolean v25, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->DEBUGGABLE:Z

    if-eqz v25, :cond_12

    .line 210
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Package Name: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", Class name: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", IconPosition: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_12
    const-string v25, "appwidget"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 214
    new-instance v12, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-direct {v12}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;-><init>()V

    .line 215
    .local v12, "newWidget":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {v12, v13}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPackageName(Ljava/lang/String;)V

    .line 216
    invoke-virtual {v12, v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setClassName(Ljava/lang/String;)V

    .line 217
    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 218
    invoke-virtual {v12, v15}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPosition(I)V

    .line 219
    move/from16 v0, v21

    invoke-virtual {v12, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setSpanX(I)V

    .line 220
    const/16 v25, -0x64

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetid(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;->mWidgetList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 224
    .end local v12    # "newWidget":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :cond_13
    const/4 v13, 0x0

    .line 225
    const/4 v4, 0x0

    .line 226
    goto/16 :goto_1

    .line 245
    .end local v4    # "className":Ljava/lang/String;
    .end local v10    # "i":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v13    # "packageName":Ljava/lang/String;
    .end local v15    # "position":I
    .end local v19    # "screen":I
    .end local v20    # "size":I
    .end local v21    # "spanX":I
    .end local v23    # "viewType":Ljava/lang/String;
    .end local v24    # "visibility":I
    :catch_0
    move-exception v7

    .line 246
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 229
    .end local v5    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v6    # "depth":I
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "res":Landroid/content/res/Resources;
    .end local v17    # "resId":I
    .end local v18    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v22    # "type":I
    :catch_1
    move-exception v7

    .line 230
    .local v7, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "Got exception parsing favorites."

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 231
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 242
    if-eqz v9, :cond_5

    .line 244
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 245
    :catch_2
    move-exception v7

    .line 246
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 232
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v7

    .line 233
    .local v7, "e":Ljava/io/IOException;
    :try_start_5
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "Got exception parsing favorites."

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 234
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 242
    if-eqz v9, :cond_5

    .line 244
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_2

    .line 245
    :catch_4
    move-exception v7

    .line 246
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 235
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v7

    .line 236
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_7
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "Got exception parsing favorites."

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 237
    invoke-virtual {v7}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 242
    if-eqz v9, :cond_5

    .line 244
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_2

    .line 245
    :catch_6
    move-exception v7

    .line 246
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 238
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v7

    .line 239
    .local v7, "e":Ljava/lang/NullPointerException;
    :try_start_9
    const-string v25, "EasyLauncher.EasyWidgetLoader"

    const-string v26, "Got exception parsing favorites."

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 240
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 242
    if-eqz v9, :cond_5

    .line 244
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    goto/16 :goto_2

    .line 245
    :catch_8
    move-exception v7

    .line 246
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 242
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v25

    if-eqz v9, :cond_14

    .line 244
    :try_start_b
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_9

    .line 247
    :cond_14
    :goto_5
    throw v25

    .line 245
    :catch_9
    move-exception v7

    .line 246
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5
.end method
