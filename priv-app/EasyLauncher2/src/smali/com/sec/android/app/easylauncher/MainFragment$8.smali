.class Lcom/sec/android/app/easylauncher/MainFragment$8;
.super Ljava/lang/Object;
.source "MainFragment.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/MainFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/MainFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/MainFragment;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/easylauncher/MainFragment;->setupLongClickView(IZ)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/MainFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/easylauncher/Launcher;->isDeleteShortcut(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    const-string v0, "EasyLauncher.MainFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delete  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget v2, v2, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget v1, v1, Lcom/sec/android/app/easylauncher/MainFragment;->mEditPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->removeFromFavorites(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MainFragment$8;->this$0:Lcom/sec/android/app/easylauncher/MainFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/MainFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 469
    :cond_0
    return-void
.end method
