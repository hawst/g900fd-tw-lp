.class public Lcom/sec/android/app/easylauncher/lib/AppInfo;
.super Ljava/lang/Object;
.source "AppInfo.java"


# instance fields
.field private editable:Z

.field private mAppIcon:Landroid/graphics/drawable/Drawable;

.field private mAppName:Ljava/lang/String;

.field private mClassName:Ljava/lang/String;

.field private mComponentName:Landroid/content/ComponentName;

.field private mIntent:Landroid/content/Intent;

.field private mPackageName:Ljava/lang/String;

.field private position:I

.field private screen:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->editable:Z

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->editable:Z

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppName:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->editable:Z

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mPackageName:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mClassName:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mIntent:Landroid/content/Intent;

    .line 64
    return-void
.end method

.method private checkForIconDpi()V
    .locals 1

    .prologue
    .line 171
    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->mIconDpi:I

    if-gtz v0, :cond_0

    .line 172
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->setIconDpi()I

    .line 175
    :cond_0
    return-void
.end method


# virtual methods
.method public getAppIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mComponentName:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getEditable()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->editable:Z

    return v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->position:I

    return v0
.end method

.method public getScreen()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->screen:I

    return v0
.end method

.method public setAppIcon(Landroid/content/res/Resources;I)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I

    .prologue
    .line 150
    const/4 v1, 0x0

    .line 151
    .local v1, "newIcon":Landroid/graphics/drawable/Drawable;
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->checkForIconDpi()V

    .line 153
    :try_start_0
    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->mIconDpi:I

    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 166
    :goto_0
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppIcon:Landroid/graphics/drawable/Drawable;

    .line 167
    return-void

    .line 154
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/ArithmeticException;
    invoke-virtual {v0}, Ljava/lang/ArithmeticException;->printStackTrace()V

    .line 158
    const/16 v2, 0xf0

    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 165
    goto :goto_0

    .line 159
    .end local v0    # "e":Ljava/lang/ArithmeticException;
    :catch_1
    move-exception v0

    .line 161
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAppIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "mAppIcon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppIcon:Landroid/graphics/drawable/Drawable;

    .line 179
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppName:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newClass"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mClassName:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setComponentName(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "mComponentName"    # Landroid/content/ComponentName;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mComponentName:Landroid/content/ComponentName;

    .line 213
    return-void
.end method

.method public setEditable(Z)V
    .locals 0
    .param p1, "editable"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->editable:Z

    .line 118
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mIntent:Landroid/content/Intent;

    .line 192
    return-void
.end method

.method public setIntent(Ljava/lang/String;)V
    .locals 2
    .param p1, "intent"    # Ljava/lang/String;

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 198
    :try_start_0
    invoke-static {p1}, Landroid/content/Intent;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mIntent:Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_0
.end method

.method public setListAppIcon(Landroid/content/res/Resources;I)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I

    .prologue
    .line 130
    const/4 v1, 0x0

    .line 131
    .local v1, "newIcon":Landroid/graphics/drawable/Drawable;
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->checkForIconDpi()V

    .line 133
    :try_start_0
    sget v2, Lcom/sec/android/app/easylauncher/EasyController;->mListIconDpi:I

    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 146
    :goto_0
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppIcon:Landroid/graphics/drawable/Drawable;

    .line 147
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/ArithmeticException;
    invoke-virtual {v0}, Ljava/lang/ArithmeticException;->printStackTrace()V

    .line 138
    const/16 v2, 0xf0

    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 145
    goto :goto_0

    .line 139
    .end local v0    # "e":Ljava/lang/ArithmeticException;
    :catch_1
    move-exception v0

    .line 141
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newPkg"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mPackageName:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->position:I

    .line 86
    return-void
.end method

.method public setScreen(I)V
    .locals 0
    .param p1, "scr"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->screen:I

    .line 78
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Item(title = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " componentName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unbind()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 222
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppName:Ljava/lang/String;

    .line 223
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mPackageName:Ljava/lang/String;

    .line 224
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mClassName:Ljava/lang/String;

    .line 225
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mAppIcon:Landroid/graphics/drawable/Drawable;

    .line 226
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mIntent:Landroid/content/Intent;

    .line 227
    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/AppInfo;->mComponentName:Landroid/content/ComponentName;

    .line 229
    return-void
.end method
