.class public Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;
.super Landroid/appwidget/AppWidgetHostView;
.source "LauncherAppWidgetHostView.java"


# instance fields
.field mContext:Lcom/sec/android/app/easylauncher/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;)V

    .line 33
    check-cast p1, Lcom/sec/android/app/easylauncher/Launcher;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;->mContext:Lcom/sec/android/app/easylauncher/Launcher;

    .line 34
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;->mContext:Lcom/sec/android/app/easylauncher/Launcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;->mContext:Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/Launcher;->isEditModeTrue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;->mContext:Lcom/sec/android/app/easylauncher/Launcher;

    sget v0, Lcom/sec/android/app/easylauncher/Launcher;->mPosition:I

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHostView;->mContext:Lcom/sec/android/app/easylauncher/Launcher;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/Launcher;->mPageAdapter:Lcom/sec/android/app/easylauncher/PageAdapter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 46
    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    .line 51
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 62
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
