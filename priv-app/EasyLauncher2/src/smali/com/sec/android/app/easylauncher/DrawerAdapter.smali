.class public Lcom/sec/android/app/easylauncher/DrawerAdapter;
.super Landroid/widget/BaseAdapter;
.source "DrawerAdapter.java"


# instance fields
.field TEXT_COLOR_DISABLED:I

.field TEXT_COLOR_NORMAL:I

.field TEXT_COLOR_SELECTED:I

.field private mContext:Landroid/content/Context;

.field mInflater:Landroid/view/LayoutInflater;

.field mPageShow:[Z

.field private pageName:[I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 38
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->TEXT_COLOR_NORMAL:I

    .line 39
    const v0, -0xd05937

    iput v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->TEXT_COLOR_SELECTED:I

    .line 40
    const v0, -0x949495

    iput v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->TEXT_COLOR_DISABLED:I

    .line 42
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->pageName:[I

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mContext:Landroid/content/Context;

    .line 53
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 55
    return-void

    .line 42
    nop

    :array_0
    .array-data 4
        0x7f090020
        0x7f090021
        0x7f090022
        0x7f09001d
        0x7f09001e
        0x7f09001f
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/DrawerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/DrawerAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x6

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 69
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0c0013

    const v7, 0x7f0c0012

    const/4 v6, 0x0

    .line 77
    const/4 v3, 0x0

    .line 80
    .local v3, "title":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030003

    invoke-virtual {v4, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 82
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "title":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 83
    .restart local v3    # "title":Landroid/widget/TextView;
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 87
    .local v0, "checkBox":Landroid/widget/CheckBox;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/Launcher;->getPageOnOff()[Z

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mPageShow:[Z

    .line 89
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "title":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 90
    .restart local v3    # "title":Landroid/widget/TextView;
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    check-cast v0, Landroid/widget/CheckBox;

    .line 93
    .restart local v0    # "checkBox":Landroid/widget/CheckBox;
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->pageName:[I

    aget v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "page":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mPageShow:[Z

    aget-boolean v4, v4, p1

    if-eqz v4, :cond_2

    .line 97
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    :goto_0
    const/4 v1, 0x1

    .line 104
    .local v1, "enabled":Z
    const/4 v4, 0x2

    if-eq p1, v4, :cond_0

    const/4 v4, 0x3

    if-ne p1, v4, :cond_1

    .line 105
    :cond_0
    const/4 v1, 0x0

    .line 106
    iget v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->TEXT_COLOR_SELECTED:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 111
    iget-object v4, p0, Lcom/sec/android/app/easylauncher/DrawerAdapter;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/easylauncher/Launcher;

    invoke-virtual {v4}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 112
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 113
    invoke-virtual {p2, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    :goto_1
    return-object p2

    .line 99
    .end local v1    # "enabled":Z
    :cond_2
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 116
    .restart local v1    # "enabled":Z
    :cond_3
    new-instance v4, Lcom/sec/android/app/easylauncher/DrawerAdapter$1;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/app/easylauncher/DrawerAdapter$1;-><init>(Lcom/sec/android/app/easylauncher/DrawerAdapter;I)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    new-instance v4, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/app/easylauncher/DrawerAdapter$2;-><init>(Lcom/sec/android/app/easylauncher/DrawerAdapter;I)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_1
.end method
