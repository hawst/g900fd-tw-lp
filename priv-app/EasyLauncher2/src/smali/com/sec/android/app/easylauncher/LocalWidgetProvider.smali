.class public Lcom/sec/android/app/easylauncher/LocalWidgetProvider;
.super Ljava/lang/Object;
.source "LocalWidgetProvider.java"


# static fields
.field private static final DEBUGGABLE:Z

.field static mLocalWidgetProvider:Lcom/sec/android/app/easylauncher/LocalWidgetProvider;


# instance fields
.field private APPWIDGET_HOST_ID:I

.field private final TAG:Ljava/lang/String;

.field private easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

.field private mAppWidgetBinder:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

.field private mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

.field private mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field mContext:Landroid/content/Context;

.field private mDefaultWidgetInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

.field private mDeleteWidgetId:I

.field mEasyWidgetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation
.end field

.field mEasyWidgetLoader:Lcom/sec/android/app/easylauncher/lib/EasyWidgetLoader;

.field mWaitingWidgetBinding:Z

.field mWidgetHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 33
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->DEBUGGABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/16 v0, 0x7f5

    iput v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->APPWIDGET_HOST_ID:I

    .line 35
    const-string v0, "EasyLauncher.LocalWidgetProvider"

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->TAG:Ljava/lang/String;

    .line 39
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    invoke-direct {v0}, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWaitingWidgetBinding:Z

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWidgetHandler:Landroid/os/Handler;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 59
    new-instance v0, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->APPWIDGET_HOST_ID:I

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    .line 66
    new-instance v0, Lcom/sec/android/app/easylauncher/AppWidgetBinder;

    check-cast p1, Lcom/sec/android/app/easylauncher/Launcher;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;-><init>(Lcom/sec/android/app/easylauncher/Launcher;Landroid/appwidget/AppWidgetManager;Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetBinder:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->getWidgetsFromDb()V

    .line 69
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 18
    sget-boolean v0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->DEBUGGABLE:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)Landroid/appwidget/AppWidgetManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/LocalWidgetProvider;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->bindTheWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDeleteWidgetId:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/LocalWidgetProvider;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/LocalWidgetProvider;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    return-object p1
.end method

.method private bindTheWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 5
    .param p1, "Info"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 204
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetProviderInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v0

    .line 206
    .local v0, "hostView":Landroid/appwidget/AppWidgetHostView;
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;->startListening()V

    .line 207
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetProviderInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    .line 208
    invoke-virtual {p1, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setHostView(Landroid/appwidget/AppWidgetHostView;)V

    .line 209
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->setWidgetInfo(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 210
    return-void
.end method

.method private setWidgetInfo(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 3
    .param p1, "widget"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 249
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    if-nez v1, :cond_0

    .line 250
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    .line 253
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateWidgetId(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I

    move-result v0

    .line 254
    .local v0, "count":I
    if-gtz v0, :cond_1

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, v2, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->inttializeWidgetsAdded(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 256
    :cond_1
    return-void
.end method


# virtual methods
.method CheckForBinding(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 4
    .param p1, "Info"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 187
    .local v0, "providerInfo":Landroid/appwidget/AppWidgetProviderInfo;
    sget-boolean v1, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 188
    const-string v1, "EasyLauncher.LocalWidgetProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CheckForBinding WidgetId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " providerInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_0
    if-nez v0, :cond_2

    .line 191
    iget-boolean v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWaitingWidgetBinding:Z

    if-eqz v1, :cond_1

    .line 200
    :goto_0
    return-void

    .line 195
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->addAppWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    goto :goto_0

    .line 198
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->bindTheWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    goto :goto_0
.end method

.method public addAppWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 4
    .param p1, "Info"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 213
    const/4 v0, 0x0

    .line 215
    .local v0, "bindSuccess":Z
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetBinder:Lcom/sec/android/app/easylauncher/AppWidgetBinder;

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetProviderInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    new-instance v3, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$2;-><init>(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/easylauncher/AppWidgetBinder;->allocateBindNew(Landroid/content/ComponentName;Lcom/sec/android/app/easylauncher/AppWidgetBinder$Callback;)Z

    move-result v0

    .line 242
    if-nez v0, :cond_0

    .line 243
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWaitingWidgetBinding:Z

    .line 245
    :cond_0
    return-void
.end method

.method addWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 5
    .param p1, "widgetInfo"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 288
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 289
    .local v1, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    const-string v2, "EasyLauncher.LocalWidgetProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addWidget info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " widgetInfo: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    const-string v2, "EasyLauncher.LocalWidgetProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-----------------:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 293
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPosition(I)V

    .line 294
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->CheckForBinding(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 298
    .end local v1    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :cond_1
    return-void
.end method

.method deleteWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)V
    .locals 5
    .param p1, "widgetInfo"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .param p2, "widgetId"    # I

    .prologue
    const/4 v4, -0x1

    .line 301
    iput p2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDeleteWidgetId:I

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    if-eqz v2, :cond_0

    .line 304
    new-instance v2, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$3;

    const-string v3, "deleteAppWidgetId"

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$3;-><init>(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$3;->start()V

    .line 312
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 313
    .local v1, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 314
    invoke-virtual {v1, v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 315
    const/16 v2, -0x64

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetid(I)V

    .line 316
    invoke-virtual {v1, v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPosition(I)V

    .line 320
    .end local v1    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :cond_2
    return-void
.end method

.method getDefaultWidgetList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    return-object v0
.end method

.method getScreenForPackage(Ljava/lang/String;)I
    .locals 3
    .param p1, "PackageName"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 111
    .local v1, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v2

    .line 116
    .end local v1    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :goto_0
    return v2

    :cond_1
    const/4 v2, -0x1

    goto :goto_0
.end method

.method getWidget(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .locals 3
    .param p1, "screen"    # I
    .param p2, "position"    # I

    .prologue
    .line 90
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 92
    .local v1, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 93
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetProviderInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 94
    invoke-virtual {p0, v1}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->CheckForBinding(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    .line 100
    .end local v1    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getWidgetInfoFromDb(II)Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .locals 3
    .param p1, "screen"    # I
    .param p2, "position"    # I

    .prologue
    .line 271
    monitor-enter p0

    const/4 v0, 0x0

    .line 273
    .local v0, "widgetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/WidgetInfo;>;"
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    if-nez v1, :cond_0

    .line 274
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    .line 276
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    const-string v2, "favorites"

    invoke-virtual {v1, v2, p1, p2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getWidgetInfo(Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 279
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getWidgetsFromDb()V
    .locals 9

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    if-nez v6, :cond_0

    .line 122
    new-instance v6, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v7, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    .line 124
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    const-string v7, "favorites"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getWidgetInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v0, "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v6}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v0

    .line 130
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 132
    .local v4, "padding":Landroid/graphics/Rect;
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDefaultWidgetInfo:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 134
    .local v3, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/appwidget/AppWidgetProviderInfo;

    .line 135
    .local v5, "providerinfo":Landroid/appwidget/AppWidgetProviderInfo;
    iget-object v6, v5, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 137
    sget-boolean v6, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->DEBUGGABLE:Z

    if-eqz v6, :cond_3

    .line 138
    const-string v6, "EasyLauncher.LocalWidgetProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Widget Povider Info"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_3
    invoke-virtual {v3, v5}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 140
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    iget-object v7, v5, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-static {v6, v7, v4}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    .line 142
    invoke-virtual {v3, v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setDefaultPadding(Landroid/graphics/Rect;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 121
    .end local v0    # "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .end local v4    # "padding":Landroid/graphics/Rect;
    .end local v5    # "providerinfo":Landroid/appwidget/AppWidgetProviderInfo;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 181
    .restart local v0    # "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    .restart local v4    # "padding":Landroid/graphics/Rect;
    :cond_4
    monitor-exit p0

    return-void
.end method

.method startloading()V
    .locals 4

    .prologue
    .line 73
    const-string v2, "EasyLauncher.LocalWidgetProvider"

    const-string v3, "startloading"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    new-instance v0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$1;-><init>(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)V

    .line 85
    .local v0, "runnable":Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 86
    .local v1, "widgetloading":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 87
    return-void
.end method

.method unBind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->easyProvider:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mDeferredHandler:Lcom/sec/android/app/easylauncher/lib/DeferredHandler;

    new-instance v1, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/easylauncher/LocalWidgetProvider$4;-><init>(Lcom/sec/android/app/easylauncher/LocalWidgetProvider;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/DeferredHandler;->post(Ljava/lang/Runnable;)V

    .line 334
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 335
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mAppWidgetHost:Lcom/sec/android/app/easylauncher/LauncherAppWidgetHost;

    .line 336
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mWidgetHandler:Landroid/os/Handler;

    .line 337
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/LocalWidgetProvider;->mContext:Landroid/content/Context;

    .line 338
    return-void
.end method
