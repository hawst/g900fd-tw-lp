.class public final Lcom/sec/android/app/easylauncher/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AlphabeticalListContainer:[I

.field public static final AlphabeticalListContainer_firstCharColor:I = 0x5

.field public static final AlphabeticalListContainer_indexGravity:I = 0x0

.field public static final AlphabeticalListContainer_popupHeight:I = 0x2

.field public static final AlphabeticalListContainer_popupHeightInc:I = 0x4

.field public static final AlphabeticalListContainer_popupWidth:I = 0x1

.field public static final AlphabeticalListContainer_popupWidthInc:I = 0x3

.field public static final AlphabeticalListIndicator:[I

.field public static final AlphabeticalListIndicator_bgFullHeight:I = 0x14

.field public static final AlphabeticalListIndicator_cellHeight:I = 0x5

.field public static final AlphabeticalListIndicator_cellSepLineColor:I = 0xb

.field public static final AlphabeticalListIndicator_cellSepLineMarginLeft:I = 0xd

.field public static final AlphabeticalListIndicator_cellSepLineMarginRight:I = 0xe

.field public static final AlphabeticalListIndicator_cellSepLineThick:I = 0xc

.field public static final AlphabeticalListIndicator_firstCharPreMatches:I = 0x3

.field public static final AlphabeticalListIndicator_indexNormalBg:I = 0x6

.field public static final AlphabeticalListIndicator_indexSelBg:I = 0x7

.field public static final AlphabeticalListIndicator_indexSelItemBg:I = 0x8

.field public static final AlphabeticalListIndicator_indexStr:I = 0x11

.field public static final AlphabeticalListIndicator_isFloating:I = 0x13

.field public static final AlphabeticalListIndicator_overflowGrpSepChar:I = 0x2

.field public static final AlphabeticalListIndicator_pressDelay:I = 0x0

.field public static final AlphabeticalListIndicator_selTextColor:I = 0xa

.field public static final AlphabeticalListIndicator_textColor:I = 0x9

.field public static final AlphabeticalListIndicator_textFontSize:I = 0x12

.field public static final AlphabeticalListIndicator_textTypeface:I = 0xf

.field public static final AlphabeticalListIndicator_textTypefaceStyle:I = 0x10

.field public static final AlphabeticalListIndicator_textUppercase:I = 0x4

.field public static final AlphabeticalListIndicator_unpressDelay:I = 0x1

.field public static final AlphabeticalListTextPopup:[I

.field public static final AlphabeticalListTextPopup_textBg:I = 0x5

.field public static final AlphabeticalListTextPopup_textCharPaddingLeft:I = 0x8

.field public static final AlphabeticalListTextPopup_textCharPaddingRight:I = 0x9

.field public static final AlphabeticalListTextPopup_textCharSpacingStrategy:I = 0x6

.field public static final AlphabeticalListTextPopup_textCharSpacingWidth:I = 0x7

.field public static final AlphabeticalListTextPopup_textColor:I = 0x1

.field public static final AlphabeticalListTextPopup_textFontSize:I = 0x4

.field public static final AlphabeticalListTextPopup_textTypeface:I = 0x2

.field public static final AlphabeticalListTextPopup_textTypefaceStyle:I = 0x3

.field public static final AlphabeticalListTextPopup_textUppercase:I = 0x0

.field public static final appshortcut:[I

.field public static final appshortcut_className:I = 0x0

.field public static final appshortcut_iconPosition:I = 0x3

.field public static final appshortcut_packageName:I = 0x1

.field public static final appshortcut_screen:I = 0x2

.field public static final appshortcut_spanX:I = 0x4

.field public static final easyapps:[I

.field public static final easyapps_visibility:I

.field public static final easyview:[I

.field public static final easyview_viewType:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 949
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/easylauncher/R$styleable;->AlphabeticalListContainer:[I

    .line 1096
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/easylauncher/R$styleable;->AlphabeticalListIndicator:[I

    .line 1425
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/easylauncher/R$styleable;->AlphabeticalListTextPopup:[I

    .line 1591
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/easylauncher/R$styleable;->appshortcut:[I

    .line 1675
    new-array v0, v3, [I

    const v1, 0x7f010025

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/easylauncher/R$styleable;->easyapps:[I

    .line 1702
    new-array v0, v3, [I

    const v1, 0x7f010026

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/easylauncher/R$styleable;->easyview:[I

    return-void

    .line 949
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
    .end array-data

    .line 1096
    :array_1
    .array-data 4
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
    .end array-data

    .line 1425
    :array_2
    .array-data 4
        0x7f01000a
        0x7f01000f
        0x7f010015
        0x7f010016
        0x7f010018
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
    .end array-data

    .line 1591
    :array_3
    .array-data 4
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
