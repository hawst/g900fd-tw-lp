.class Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;
.super Landroid/os/AsyncTask;
.source "EasyController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/EasyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadApplicationLists"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/EasyController;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/easylauncher/EasyController;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/easylauncher/EasyController;Lcom/sec/android/app/easylauncher/EasyController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/easylauncher/EasyController;
    .param p2, "x1"    # Lcom/sec/android/app/easylauncher/EasyController$1;

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;-><init>(Lcom/sec/android/app/easylauncher/EasyController;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 353
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/EasyController;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/EasyController;->mOtherAppsList:Ljava/util/concurrent/ConcurrentSkipListMap;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/easylauncher/lib/Lib;->getApplicationsWithClass(Landroid/content/Context;Ljava/util/concurrent/ConcurrentSkipListMap;Ljava/util/List;)V

    .line 358
    const-string v0, "Executed"

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 353
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 364
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 365
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/easylauncher/EasyController;->sendBroadcast(Landroid/content/Intent;)V

    .line 367
    sget-boolean v1, Lcom/sec/android/app/easylauncher/EasyController;->reRunTask:Z

    if-ne v1, v5, :cond_0

    .line 368
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    new-instance v2, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    invoke-direct {v2, v3}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;-><init>(Lcom/sec/android/app/easylauncher/EasyController;)V

    iput-object v2, v1, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->this$0:Lcom/sec/android/app/easylauncher/EasyController;

    iget-object v1, v1, Lcom/sec/android/app/easylauncher/EasyController;->mLoadApplication:Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 370
    sput-boolean v4, Lcom/sec/android/app/easylauncher/EasyController;->reRunTask:Z

    .line 372
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 353
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/EasyController$LoadApplicationLists;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 380
    return-void
.end method
