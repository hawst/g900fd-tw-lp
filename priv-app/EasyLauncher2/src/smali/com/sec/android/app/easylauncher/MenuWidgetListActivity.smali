.class public Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;
.super Landroid/app/Activity;
.source "MenuWidgetListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;
    }
.end annotation


# static fields
.field private static final REQUEST_TO_SEARCHVIEW:I = 0x65

.field public static final TAG:Ljava/lang/String; = "EasyLauncher.MenuWidgetListActivity"


# instance fields
.field private apps:Landroid/widget/ListView;

.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mActivityName:Landroid/content/ComponentName;

.field mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

.field private mAppMarketIntent:Landroid/content/Intent;

.field private mContext:Landroid/content/Context;

.field private mFreshActivity:Z

.field mListView:Landroid/widget/ListView;

.field private mPosition:I

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private mScreen:I

.field private mSpanX:I

.field mWidgetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAppMarketIntent:Landroid/content/Intent;

    .line 204
    new-instance v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$1;-><init>(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 305
    new-instance v0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$2;-><init>(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 364
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;
    .param p1, "x1"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->addWidgetToScreen(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V

    return-void
.end method

.method private addWidgetToScreen(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 5
    .param p1, "widgetInfo"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 332
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 333
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    .line 335
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ScreenPosion"

    sget v4, Lcom/sec/android/app/easylauncher/EasyController;->APPS_SCREEN_POSITION:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 337
    .local v1, "editScreen":I
    invoke-virtual {p1, v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 343
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;-><init>(Landroid/content/Context;)V

    .line 345
    .local v0, "easyProvider":Lcom/sec/android/app/easylauncher/lib/EasyProvider;
    if-eqz v0, :cond_1

    .line 346
    invoke-virtual {v0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I

    .line 347
    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->closeDatabase()V

    .line 350
    :cond_1
    return-void
.end method


# virtual methods
.method makeWidgetList()Z
    .locals 6

    .prologue
    .line 170
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/EasyController;->getDefaultWidgetList()Ljava/util/ArrayList;

    move-result-object v2

    .line 172
    .local v2, "widgetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/WidgetInfo;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 173
    .local v1, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    const-string v3, "EasyLauncher.MenuWidgetListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  getSpanX() :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getSpanX()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSpanX:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mSpanX:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mPosition : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mPosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetProviderInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getSpanX()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mSpanX:I

    if-ne v3, v4, :cond_0

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    .end local v1    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 180
    :cond_2
    const/4 v3, 0x0

    .line 183
    :goto_1
    return v3

    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 108
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    iput-boolean v6, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mFreshActivity:Z

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EditPosition"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mPosition:I

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ScreenPosion"

    const/16 v4, -0x64

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mScreen:I

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "WidgetSize"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mSpanX:I

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->makeWidgetList()Z

    .line 119
    const v2, 0x7f03001a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->setContentView(I)V

    .line 121
    const v2, 0x7f0c0056

    invoke-virtual {p0, v2}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mListView:Landroid/widget/ListView;

    .line 122
    new-instance v2, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;-><init>(Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/easylauncher/EasyController;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/EasyController;->canRotate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    invoke-virtual {p0, v5}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->setRequestedOrientation(I)V

    .line 131
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.APP_MARKET"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 135
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mActivityName:Landroid/content/ComponentName;

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mActivityName:Landroid/content/ComponentName;

    if-eqz v2, :cond_1

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAppMarketIntent:Landroid/content/Intent;

    .line 139
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 140
    .local v0, "filter":Landroid/content/IntentFilter;
    sget-object v2, Lcom/sec/android/app/easylauncher/EasyController;->ACTION_UPDATE_DATA:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 142
    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 143
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    const-string v2, "android.intent.action.PACKAGE_INSTALL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 148
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->finish()V

    .line 151
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;->unBind()V

    .line 356
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->apps:Landroid/widget/ListView;

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 358
    invoke-static {}, Lcom/sec/android/app/easylauncher/EasyController;->getInstance()Lcom/sec/android/app/easylauncher/EasyController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 360
    iput-object v2, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mAdapter:Lcom/sec/android/app/easylauncher/MenuWidgetListActivity$WidgetListAdapter;

    .line 361
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 362
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 57
    const v0, 0x7f040001

    const/high16 v1, 0x7f040000

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->overridePendingTransition(II)V

    .line 58
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 59
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 75
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/high16 v2, 0x7f040000

    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mFreshActivity:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 65
    const v0, 0x7f040001

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->overridePendingTransition(II)V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mFreshActivity:Z

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    const v0, 0x7f040002

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method public declared-synchronized setWidgetProviderInfo()V
    .locals 7

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v0

    .line 192
    .local v0, "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    iget-object v5, p0, Lcom/sec/android/app/easylauncher/MenuWidgetListActivity;->mWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .line 194
    .local v3, "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/appwidget/AppWidgetProviderInfo;

    .line 195
    .local v4, "providerinfo":Landroid/appwidget/AppWidgetProviderInfo;
    iget-object v5, v4, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 196
    invoke-virtual {v3, v4}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 189
    .end local v0    # "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .end local v4    # "providerinfo":Landroid/appwidget/AppWidgetProviderInfo;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 202
    .restart local v0    # "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    :cond_2
    monitor-exit p0

    return-void
.end method
