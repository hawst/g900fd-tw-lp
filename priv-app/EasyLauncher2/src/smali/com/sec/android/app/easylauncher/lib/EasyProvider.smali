.class public Lcom/sec/android/app/easylauncher/lib/EasyProvider;
.super Landroid/content/ContentProvider;
.source "EasyProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "easylauncher.db"

.field private static final DATABASE_VERSION:I = 0xb

.field private static final DEBUGGABLE:Z

.field public static final TABLE_EASYAPPS:Ljava/lang/String; = "easyapps"

.field public static final TABLE_EASYVIEWS:Ljava/lang/String; = "easyviews"

.field public static final TABLE_FAVORITES:Ljava/lang/String; = "favorites"

.field private static final TAG:Ljava/lang/String; = "EasyLauncher.EasyProvider"


# instance fields
.field public database:Landroid/database/sqlite/SQLiteDatabase;

.field private mContext:Landroid/content/Context;

.field private mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 58
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mContext:Landroid/content/Context;

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    if-nez v1, :cond_0

    .line 77
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;-><init>(Lcom/sec/android/app/easylauncher/lib/EasyProvider;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "EasyLauncher.EasyProvider"

    const-string v2, "Error in Creating Database"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EasyLauncher.EasyProvider"

    const-string v2, "Unknown error occurred during Database creation"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 58
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v1, :cond_1

    .line 93
    :cond_0
    new-instance v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;-><init>(Lcom/sec/android/app/easylauncher/lib/EasyProvider;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 95
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v1, :cond_1

    .line 96
    const-string v1, "EasyLauncher.EasyProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Database Created: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 106
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mContext:Landroid/content/Context;

    .line 107
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "EasyLauncher.EasyProvider"

    const-string v2, "Error in Creating Database"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 102
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EasyLauncher.EasyProvider"

    const-string v2, "Unknown error occurred during Database creation"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 45
    sget-boolean v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    return v0
.end method

.method public static final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "firstElementName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 836
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .local v0, "type":I
    if-eq v0, v2, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 840
    :cond_1
    if-eq v0, v2, :cond_2

    .line 841
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "No start tag found"

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 844
    :cond_2
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 845
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected start tag: found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 848
    :cond_3
    return-void
.end method

.method private delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "tblName"    # Ljava/lang/String;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 427
    if-nez p2, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 432
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;-><init>(Lcom/sec/android/app/easylauncher/lib/EasyProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    return-object v0
.end method

.method private initializeFavorites(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/AppInfo;IZ)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "app"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .param p3, "position"    # I
    .param p4, "packageExists"    # Z

    .prologue
    .line 610
    const/4 v5, 0x5

    new-array v0, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "screen"

    aput-object v6, v0, v5

    const/4 v5, 0x1

    const-string v6, "position"

    aput-object v6, v0, v5

    const/4 v5, 0x2

    const-string v6, "packageName"

    aput-object v6, v0, v5

    const/4 v5, 0x3

    const-string v6, "className"

    aput-object v6, v0, v5

    const/4 v5, 0x4

    const-string v6, "intent"

    aput-object v6, v0, v5

    .line 613
    .local v0, "availablePackages":[Ljava/lang/String;
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "screen"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "position"

    aput-object v6, v4, v5

    .line 617
    .local v4, "unavailablePackages":[Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getScreen()I

    move-result v2

    .line 620
    .local v2, "screen":I
    const-string v3, "favorites"

    .line 621
    .local v3, "tableName":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v5, :cond_0

    .line 622
    const-string v5, "EasyLauncher.EasyProvider"

    const-string v6, " Inside initializeDatabase"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    :cond_0
    if-nez p4, :cond_2

    .line 625
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    .line 628
    .local v1, "data":[Ljava/lang/String;
    invoke-virtual {p0, p1, v3, v4, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 629
    sget-boolean v5, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v5, :cond_1

    .line 630
    const-string v5, "EasyLauncher.EasyProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initializeDatabase: Packages not available: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    :cond_1
    :goto_0
    return-void

    .line 632
    .end local v1    # "data":[Ljava/lang/String;
    :cond_2
    const/4 v5, 0x5

    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    const/4 v5, 0x2

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    const/4 v5, 0x3

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    const/4 v5, 0x4

    const-string v6, ""

    aput-object v6, v1, v5

    .line 636
    .restart local v1    # "data":[Ljava/lang/String;
    invoke-virtual {p0, p1, v3, v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addEasyViewInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;)V
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "info"    # Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 818
    sget-boolean v2, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v2, :cond_0

    .line 819
    const-string v2, "EasyLauncher.EasyProvider"

    const-string v3, " Inside addEasyViewInfo()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    :cond_0
    const/4 v2, 0x5

    new-array v0, v2, [Ljava/lang/String;

    const-string v2, "screen"

    aput-object v2, v0, v4

    const-string v2, "position"

    aput-object v2, v0, v5

    const-string v2, "packageName"

    aput-object v2, v0, v6

    const-string v2, "className"

    aput-object v2, v0, v7

    const-string v2, "viewType"

    aput-object v2, v0, v8

    .line 823
    .local v0, "Columns":[Ljava/lang/String;
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getScreen()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getClassName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getViewType()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 827
    .local v1, "data":[Ljava/lang/String;
    const-string v2, "easyviews"

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 828
    sget-boolean v2, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v2, :cond_1

    .line 829
    const-string v2, "EasyLauncher.EasyProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getViewType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has been added"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    :cond_1
    return-void
.end method

.method public addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tblName"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "values"    # [Ljava/lang/String;

    .prologue
    .line 568
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 569
    .local v2, "rowValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p3

    if-ge v1, v3, :cond_0

    .line 570
    aget-object v3, p3, v1

    aget-object v4, p4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 573
    :cond_0
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 577
    :goto_1
    return-void

    .line 574
    :catch_0
    move-exception v0

    .line 575
    .local v0, "ee":Ljava/lang/Exception;
    const-string v3, "EasyLauncher.EasyProvider"

    const-string v4, "DB ERROR"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public closeDatabase()V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 388
    return-void
.end method

.method public createEasyApps(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 447
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE if not exists "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rowID INTEGER PRIMARY KEY autoincrement, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packageName TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " className TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " visibility INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " intent TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " appIcon BLOB); "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 451
    .local v0, "tableStatement":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 452
    const-string v1, "EasyLauncher.EasyProvider"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->createTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 454
    return-void
.end method

.method public createEasyViews(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 457
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE if not exists "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rowID INTEGER PRIMARY KEY autoincrement, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " screen INTEGER, position INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " viewType TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packageName TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " className TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " intent TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " appIcon BLOB); "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 462
    .local v0, "tableStatement":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 463
    const-string v1, "EasyLauncher.EasyProvider"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->createTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method public createFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 435
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE if not exists "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rowID INTEGER PRIMARY KEY autoincrement, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " screen INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cellX INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cellY INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " spanX INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " spanY INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " appWidgetID INTEGER, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packageName TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " className TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " intent TEXT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " appIcon BLOB,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unique (screen, position) ); "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "tableStatement":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 442
    const-string v1, "EasyLauncher.EasyProvider"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->createTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method public createTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableQuery"    # Ljava/lang/String;

    .prologue
    .line 392
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 400
    :goto_0
    return-void

    .line 393
    :catch_0
    move-exception v0

    .line 394
    .local v0, "e":Landroid/database/SQLException;
    const-string v1, "EasyLauncher.EasyProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in creating Table => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0

    .line 396
    .end local v0    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v0

    .line 397
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EasyLauncher.EasyProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown Exception in creating Table => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method public deleteWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I
    .locals 6
    .param p1, "widget"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 723
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 725
    .local v1, "values":Landroid/content/ContentValues;
    const-string v3, "packageName"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    const-string v3, "classname"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    const-string v3, "appWidgetID"

    const/16 v4, -0x64

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " screen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 730
    .local v2, "where":Ljava/lang/String;
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 731
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Query"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "favorites"

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v0

    .line 734
    .local v0, "count":I
    return v0
.end method

.method public deletefavoritesdata(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "where"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 420
    sget-boolean v0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 421
    const-string v0, "EasyLauncher.EasyProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Where condition :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_0
    const-string v0, "favorites"

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 424
    return-void
.end method

.method public doWeHaveFavoriteApps(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "screen"    # I

    .prologue
    const/4 v2, 0x0

    .line 511
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "select count(*) from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " where packageName !="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and screen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 514
    .local v0, "c":Landroid/database/Cursor;
    const/4 v1, -0x1

    .line 515
    .local v1, "count":I
    if-eqz v0, :cond_1

    .line 516
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 517
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 519
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 522
    :cond_1
    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    .line 523
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: could not query max id from table "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 525
    :cond_2
    if-lez v1, :cond_3

    .line 526
    const/4 v2, 0x1

    .line 528
    :cond_3
    return v2
.end method

.method fillEmptyApps()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 580
    const/4 v1, 0x0

    .line 581
    .local v1, "loopIndex":I
    new-array v3, v9, [Ljava/lang/String;

    const-string v6, "screen"

    aput-object v6, v3, v7

    const-string v6, "position"

    aput-object v6, v3, v8

    .line 584
    .local v3, "unavailablePackages":[Ljava/lang/String;
    const-string v2, "favorites"

    .line 585
    .local v2, "tableName":Ljava/lang/String;
    sget v6, Lcom/sec/android/app/easylauncher/EasyController;->APPS_SCREEN_POSITION:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 586
    .local v4, "user_Apps":Ljava/lang/String;
    new-array v5, v9, [Ljava/lang/String;

    .line 587
    .local v5, "values_user_Apps":[Ljava/lang/String;
    aput-object v4, v5, v7

    .line 589
    const/4 v1, 0x0

    :goto_0
    sget v6, Lcom/sec/android/app/easylauncher/EasyController;->APPS_FRAGMENT_DEFAULT_APPS_COUNT:I

    if-ge v1, v6, :cond_0

    .line 590
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 591
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v6, v2, v3, v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 589
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 593
    :cond_0
    sget v6, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 594
    .local v0, "default_Apps":Ljava/lang/String;
    aput-object v0, v5, v7

    .line 596
    const/4 v1, 0x0

    :goto_1
    sget v6, Lcom/sec/android/app/easylauncher/EasyController;->CENTER_FRAGMENT_APPS_COUNT:I

    if-ge v1, v6, :cond_1

    .line 597
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 598
    iget-object v6, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v6, v2, v3, v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 596
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 601
    :cond_1
    return-void
.end method

.method public getEasyApps(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 799
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 800
    .local v1, "easyAppsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;>;"
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "packageName"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "className"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "visibility"

    aput-object v3, v0, v2

    .line 803
    .local v0, "Columns":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    move-result-object v2

    const-string v3, "easyapps"

    invoke-virtual {v2, p1, v3, v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getEasyAppsList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 804
    return-object v1
.end method

.method public getNewRowId()J
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getMaxPosition()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 480
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserHomescreenApps(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "table"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 554
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "screen"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string v1, "position"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string v1, "packageName"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "className"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "intent"

    aput-object v1, v3, v0

    .line 558
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, ""

    .line 559
    .local v4, "where":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " screen >="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 561
    const-string v5, "position "

    .line 562
    .local v5, "orderBy":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getAppsList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 563
    .local v6, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    return-object v6
.end method

.method public getUserSelectedApps(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 7
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "screen"    # I
    .param p3, "AppsCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 536
    .local v6, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "screen"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string v1, "position"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string v1, "packageName"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "className"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "intent"

    aput-object v1, v3, v0

    .line 540
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, ""

    .line 541
    .local v4, "where":Ljava/lang/String;
    sget v0, Lcom/sec/android/app/easylauncher/EasyController;->CENTRE_SCREEN_POSITION:I

    if-ne p2, v0, :cond_0

    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " screen ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and position<6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 545
    :goto_0
    const-string v5, "position "

    .line 546
    .local v5, "orderBy":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getAppsList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 547
    return-object v6

    .line 544
    .end local v5    # "orderBy":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " screen ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getWidgetInfo(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "table"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 782
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 783
    .local v6, "widgetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/WidgetInfo;>;"
    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "screen"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string v1, "position"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string v1, "appWidgetID"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "packageName"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "className"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "spanX"

    aput-object v1, v3, v0

    .line 790
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, ""

    .line 791
    .local v4, "where":Ljava/lang/String;
    const-string v4, " appWidgetID is not null"

    .line 792
    const-string v5, "screen "

    .line 794
    .local v5, "orderBy":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getWidgetList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 795
    return-object v6
.end method

.method public getWidgetInfo(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 7
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "screen"    # I
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 740
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 741
    .local v6, "widgetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/WidgetInfo;>;"
    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "screen"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string v1, "position"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string v1, "appWidgetID"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "packageName"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "className"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "spanX"

    aput-object v1, v3, v0

    .line 748
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, ""

    .line 749
    .local v4, "where":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 751
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getWidgetList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 752
    return-object v6
.end method

.method public insert(Ljava/lang/String;)J
    .locals 4
    .param p1, "stmt"    # Ljava/lang/String;

    .prologue
    .line 403
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 406
    .local v0, "dbStatement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    return-wide v2
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;

    .prologue
    .line 486
    const/4 v0, 0x0

    return-object v0
.end method

.method public insertInEmptyApps(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "info"    # Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 808
    new-array v0, v6, [Ljava/lang/String;

    const-string v2, "packageName"

    aput-object v2, v0, v3

    const-string v2, "className"

    aput-object v2, v0, v4

    const-string v2, "visibility"

    aput-object v2, v0, v5

    .line 811
    .local v0, "Columns":[Ljava/lang/String;
    new-array v1, v6, [Ljava/lang/String;

    iget-object v2, p2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->packageName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->className:Ljava/lang/String;

    aput-object v2, v1, v4

    iget v2, p2, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->visibility:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 814
    .local v1, "data":[Ljava/lang/String;
    const-string v2, "easyapps"

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 815
    return-void
.end method

.method public inttializeWidgetsAdded(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "info"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 668
    const/4 v3, 0x6

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "screen"

    aput-object v3, v0, v5

    const-string v3, "position"

    aput-object v3, v0, v6

    const-string v3, "appWidgetID"

    aput-object v3, v0, v7

    const-string v3, "packageName"

    aput-object v3, v0, v8

    const-string v3, "className"

    aput-object v3, v0, v9

    const/4 v3, 0x5

    const-string v4, "spanX"

    aput-object v4, v0, v3

    .line 672
    .local v0, "Columns":[Ljava/lang/String;
    const-string v2, "favorites"

    .line 673
    .local v2, "tableName":Ljava/lang/String;
    const/4 v3, 0x6

    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v6

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v7

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v8

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getClassName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v9

    const/4 v3, 0x5

    invoke-virtual {p2}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getSpanX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 678
    .local v1, "data":[Ljava/lang/String;
    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->addRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 680
    return-void
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 492
    invoke-direct {p0}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->getDatabaseHelper()Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->mEasyHelper:Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;

    .line 493
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # [Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;

    .prologue
    .line 499
    const/4 v0, 0x0

    return-object v0
.end method

.method public setNewAppInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/AppInfo;IZ)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "app"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;
    .param p3, "position"    # I
    .param p4, "packageExists"    # Z

    .prologue
    .line 604
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->initializeFavorites(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/AppInfo;IZ)V

    .line 605
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 505
    const/4 v0, 0x0

    return v0
.end method

.method public updateAppInfo(Lcom/sec/android/app/easylauncher/lib/AppInfo;)V
    .locals 6
    .param p1, "app"    # Lcom/sec/android/app/easylauncher/lib/AppInfo;

    .prologue
    .line 641
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 642
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inside Update App Info, position :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 644
    .local v1, "values":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 645
    const-string v3, ""

    invoke-virtual {p1, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 646
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 647
    const-string v3, ""

    invoke-virtual {p1, v3}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 649
    :cond_2
    const-string v3, "packageName"

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const-string v3, "classname"

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 653
    const-string v3, "intent"

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " screen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getScreen()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 656
    .local v2, "where":Ljava/lang/String;
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_4

    .line 657
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Query"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "favorites"

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v0

    .line 659
    .local v0, "count":I
    if-gtz v0, :cond_5

    .line 660
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->getPosition()I

    move-result v4

    const/4 v5, 0x1

    invoke-direct {p0, v3, p1, v4, v5}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->initializeFavorites(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/app/easylauncher/lib/AppInfo;IZ)V

    .line 662
    :cond_5
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_6

    .line 663
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updated Rows: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    :cond_6
    return-void
.end method

.method public updateTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)I
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "whereCondition"    # Ljava/lang/String;

    .prologue
    .line 413
    const/4 v1, 0x0

    invoke-virtual {p1, p2, p3, p4, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 414
    .local v0, "count":I
    sget-boolean v1, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 415
    const-string v1, "EasyLauncher.EasyProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updated Rows: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_0
    return v0
.end method

.method public updateWidget(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I
    .locals 6
    .param p1, "widget"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 708
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 710
    .local v1, "values":Landroid/content/ContentValues;
    const-string v3, "appWidgetID"

    const/16 v4, -0x64

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    const-string v3, "packageName"

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    const-string v3, "classname"

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " screen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 715
    .local v2, "where":Ljava/lang/String;
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 716
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Query"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "favorites"

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v0

    .line 719
    .local v0, "count":I
    return v0
.end method

.method public updateWidgetId(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;)I
    .locals 6
    .param p1, "widget"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    .prologue
    .line 683
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 685
    .local v1, "values":Landroid/content/ContentValues;
    const-string v3, "appWidgetID"

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getWidgetid()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " screen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 687
    .local v2, "where":Ljava/lang/String;
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 688
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Query"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "favorites"

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v0

    .line 690
    .local v0, "count":I
    return v0
.end method

.method public updateWidgetScreenInfo(Lcom/sec/android/app/easylauncher/lib/WidgetInfo;I)I
    .locals 6
    .param p1, "widget"    # Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    .param p2, "screen"    # I

    .prologue
    .line 694
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 696
    .local v1, "values":Landroid/content/ContentValues;
    const-string v3, "appWidgetID"

    const/16 v4, -0x64

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    const-string v3, "screen"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " screen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getScreen()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 700
    .local v2, "where":Ljava/lang/String;
    sget-boolean v3, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 701
    const-string v3, "EasyLauncher.EasyProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Query"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "favorites"

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v0

    .line 704
    .local v0, "count":I
    return v0
.end method
