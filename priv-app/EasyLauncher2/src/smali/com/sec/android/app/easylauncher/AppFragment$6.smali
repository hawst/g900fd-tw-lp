.class Lcom/sec/android/app/easylauncher/AppFragment$6;
.super Ljava/lang/Object;
.source "AppFragment.java"

# interfaces
.implements Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AppFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AppFragment;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/easylauncher/AppFragment;->setupLongClickView(IZ)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/easylauncher/Launcher;->isDeleteShortcut(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    const-string v0, "EasyLauncher.AppFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delete  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget v2, v2, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAppUtil:Lcom/sec/android/app/easylauncher/lib/AppDBUtils;

    iget-object v1, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget v1, v1, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/easylauncher/lib/AppDBUtils;->removeFromFavorites(I)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/AppFragment$6;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v0, v0, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->notifyDataSetChanged()V

    .line 402
    :cond_0
    return-void
.end method
