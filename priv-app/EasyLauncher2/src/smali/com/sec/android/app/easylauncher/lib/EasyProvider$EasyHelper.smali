.class Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "EasyProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/lib/EasyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EasyHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/easylauncher/lib/EasyProvider;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    .line 136
    const-string v0, "easylauncher.db"

    const/4 v1, 0x0

    const/16 v2, 0xb

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 137
    # getter for: Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const-string v0, "EasyLauncher.EasyProvider"

    const-string v1, "Database helper Created"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    return-void
.end method

.method private initializeMaxId(Ljava/lang/String;I)I
    .locals 6
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "screen"    # I

    .prologue
    const/4 v5, 0x0

    .line 192
    const/4 v2, 0x2

    if-ne p2, v2, :cond_2

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, v2, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select ifnull(max(position),0) from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " where screen=2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 201
    .local v0, "c":Landroid/database/Cursor;
    :goto_0
    const/4 v1, -0x1

    .line 202
    .local v1, "position":I
    if-eqz v0, :cond_1

    .line 203
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 206
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 209
    :cond_1
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 210
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: could not query max id from table "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 196
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "position":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    iget-object v2, v2, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->database:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select ifnull(max(position),0) from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " where screen=0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .restart local v0    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 212
    .restart local v1    # "position":I
    :cond_3
    return v1
.end method


# virtual methods
.method public getAppsList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "where"    # Ljava/lang/String;
    .param p5, "orderBy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v8, "appsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/AppInfo;>;"
    const/4 v9, 0x0

    .line 222
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p4

    move-object/from16 v7, p5

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 224
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    :cond_0
    new-instance v11, Lcom/sec/android/app/easylauncher/lib/AppInfo;

    invoke-direct {v11}, Lcom/sec/android/app/easylauncher/lib/AppInfo;-><init>()V

    .line 227
    .local v11, "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setScreen(I)V

    .line 228
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPosition(I)V

    .line 229
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 230
    const/4 v0, 0x3

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 231
    const/4 v0, 0x4

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/AppInfo;->setIntent(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    .end local v11    # "newApp":Lcom/sec/android/app/easylauncher/lib/AppInfo;
    :cond_1
    if-eqz v9, :cond_2

    .line 237
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :cond_2
    if-eqz v9, :cond_3

    .line 247
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 250
    :cond_3
    :goto_0
    return-object v8

    .line 239
    :catch_0
    move-exception v10

    .line 240
    .local v10, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v0, "EasyLauncher.EasyProvider"

    invoke-virtual {v10}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {v10}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    if-eqz v9, :cond_3

    .line 247
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 242
    .end local v10    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v10

    .line 243
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "EasyLauncher.EasyProvider"

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    if-eqz v9, :cond_3

    .line 247
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 246
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_4

    .line 247
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getEasyAppsList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .local v11, "easyAppsInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;>;"
    const/4 v9, 0x0

    .line 260
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 262
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    :cond_0
    new-instance v8, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;

    invoke-direct {v8}, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;-><init>()V

    .line 265
    .local v8, "appinfo":Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->packageName:Ljava/lang/String;

    .line 266
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->className:Ljava/lang/String;

    .line 267
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v8, Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;->visibility:I

    .line 268
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    .end local v8    # "appinfo":Lcom/sec/android/app/easylauncher/lib/Lib$EasyAppInfo;
    :cond_1
    if-eqz v9, :cond_2

    .line 273
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    :cond_2
    if-eqz v9, :cond_3

    .line 282
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_3
    :goto_0
    return-object v11

    .line 274
    :catch_0
    move-exception v10

    .line 275
    .local v10, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v0, "EasyLauncher.EasyProvider"

    invoke-virtual {v10}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-virtual {v10}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281
    if-eqz v9, :cond_3

    .line 282
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 277
    .end local v10    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v10

    .line 278
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "EasyLauncher.EasyProvider"

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 281
    if-eqz v9, :cond_3

    .line 282
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 281
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_4

    .line 282
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getEasyViewInfo(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;ILjava/lang/String;)Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "screen"    # I
    .param p4, "viewType"    # Ljava/lang/String;

    .prologue
    .line 302
    new-instance v2, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;

    invoke-direct {v2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;-><init>()V

    .line 303
    .local v2, "easyAppsInfo":Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "viewType = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 304
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x0

    .line 307
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "select screen, position, className, packageName, viewType from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " where screen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 311
    # getter for: Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 312
    const-string v4, "EasyLauncher.EasyProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DB Query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_0
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 315
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setScreen(I)V

    .line 316
    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setPosition(I)V

    .line 317
    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setClassName(Ljava/lang/String;)V

    .line 318
    const/4 v4, 0x3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setPackageName(Ljava/lang/String;)V

    .line 319
    const/4 v4, 0x4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->setViewType(Ljava/lang/String;)V

    .line 320
    # getter for: Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 321
    const-string v4, "EasyLauncher.EasyProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " View Type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getViewType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_2
    # getter for: Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 323
    const-string v4, "EasyLauncher.EasyProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Clock Info: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/lib/EasyViewInfo;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 326
    :cond_4
    if-eqz v0, :cond_5

    .line 327
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :cond_5
    if-eqz v0, :cond_6

    .line 336
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 339
    :cond_6
    :goto_0
    return-object v2

    .line 328
    :catch_0
    move-exception v1

    .line 329
    .local v1, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v4, "EasyLauncher.EasyProvider"

    invoke-virtual {v1}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    if-eqz v0, :cond_6

    .line 336
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 331
    .end local v1    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v1

    .line 332
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "EasyLauncher.EasyProvider"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 335
    if-eqz v0, :cond_6

    .line 336
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 335
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_7

    .line 336
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4
.end method

.method public getMaxPosition()I
    .locals 2

    .prologue
    .line 172
    const-string v0, "favorites"

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->initializeMaxId(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getWidgetList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "where"    # Ljava/lang/String;
    .param p5, "orderBy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/easylauncher/lib/WidgetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v12, "widgetInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/easylauncher/lib/WidgetInfo;>;"
    const/4 v8, 0x0

    .line 349
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v7, p5

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 351
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 355
    .local v10, "screen":I
    new-instance v11, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;

    invoke-direct {v11}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;-><init>()V

    .line 356
    .local v11, "widget":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    invoke-virtual {v11, v10}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setScreen(I)V

    .line 357
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPosition(I)V

    .line 358
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setWidgetid(I)V

    .line 359
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setPackageName(Ljava/lang/String;)V

    .line 360
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setClassName(Ljava/lang/String;)V

    .line 361
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/sec/android/app/easylauncher/lib/WidgetInfo;->setSpanX(I)V

    .line 362
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    .end local v10    # "screen":I
    .end local v11    # "widget":Lcom/sec/android/app/easylauncher/lib/WidgetInfo;
    :cond_1
    if-eqz v8, :cond_2

    .line 367
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :cond_2
    if-eqz v8, :cond_3

    .line 376
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 379
    :cond_3
    :goto_0
    return-object v12

    .line 368
    :catch_0
    move-exception v9

    .line 369
    .local v9, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v0, "EasyLauncher.EasyProvider"

    invoke-virtual {v9}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {v9}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    if-eqz v8, :cond_3

    .line 376
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 371
    .end local v9    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v9

    .line 372
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "EasyLauncher.EasyProvider"

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 375
    if-eqz v8, :cond_3

    .line 376
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 375
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 376
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 144
    # getter for: Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "EasyLauncher.EasyProvider"

    const-string v1, "creating new launcher database"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    const-string v1, "favorites"

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->createFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    const-string v1, "easyapps"

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->createEasyApps(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->this$0:Lcom/sec/android/app/easylauncher/lib/EasyProvider;

    const-string v1, "easyviews"

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->createEasyViews(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 156
    # getter for: Lcom/sec/android/app/easylauncher/lib/EasyProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/sec/android/app/easylauncher/lib/EasyProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "EasyLauncher.EasyProvider"

    const-string v1, "Database onUpgrade"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/easylauncher/lib/EasyProvider$EasyHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 160
    return-void
.end method
