.class Lcom/sec/android/app/easylauncher/AppFragment$5;
.super Ljava/lang/Object;
.source "AppFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/easylauncher/AppFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/easylauncher/AppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/easylauncher/AppFragment;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 356
    const-string v3, "EasyLauncher.AppFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onItemLongClick position : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v3, v3, Lcom/sec/android/app/easylauncher/AppFragment;->mAdapter:Lcom/sec/android/app/easylauncher/AppsGridAdapter;

    invoke-virtual {v3, p2, p3}, Lcom/sec/android/app/easylauncher/AppsGridAdapter;->getEditable(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getEditMode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/easylauncher/Launcher;->isEasyHelpAppRunning()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    .line 381
    .end local p2    # "view":Landroid/view/View;
    :goto_0
    return v1

    .line 362
    .restart local p2    # "view":Landroid/view/View;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iget-object v3, v3, Lcom/sec/android/app/easylauncher/AppFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 364
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    iput p3, v3, Lcom/sec/android/app/easylauncher/AppFragment;->mEditPosition:I

    .line 368
    move-object v0, p2

    .line 370
    .local v0, "dragView":Landroid/view/View;
    instance-of v3, p2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    .line 371
    check-cast p2, Landroid/view/ViewGroup;

    .end local p2    # "view":Landroid/view/View;
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 375
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/easylauncher/AppFragment;->setupLongClickView(IZ)V

    .line 377
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/easylauncher/Launcher;->createDragView(Landroid/view/View;)V

    .line 378
    iget-object v2, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/easylauncher/AppFragment;->getLauncher()Lcom/sec/android/app/easylauncher/Launcher;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # getter for: Lcom/sec/android/app/easylauncher/AppFragment;->onContentDropListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;
    invoke-static {v3}, Lcom/sec/android/app/easylauncher/AppFragment;->access$000(Lcom/sec/android/app/easylauncher/AppFragment;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/easylauncher/AppFragment$5;->this$0:Lcom/sec/android/app/easylauncher/AppFragment;

    # getter for: Lcom/sec/android/app/easylauncher/AppFragment;->onContentAddViewListener:Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;
    invoke-static {v4}, Lcom/sec/android/app/easylauncher/AppFragment;->access$100(Lcom/sec/android/app/easylauncher/AppFragment;)Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/easylauncher/Launcher;->setOnListener(Lcom/sec/android/app/easylauncher/EasyContentLayout$OnDropListener;Lcom/sec/android/app/easylauncher/EasyContentLayout$OnAddViewListener;)V

    goto :goto_0
.end method
