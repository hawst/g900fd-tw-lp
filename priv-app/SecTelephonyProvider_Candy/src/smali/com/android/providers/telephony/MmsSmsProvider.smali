.class public Lcom/android/providers/telephony/MmsSmsProvider;
.super Landroid/content/ContentProvider;
.source "MmsSmsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;,
        Lcom/android/providers/telephony/MmsSmsProvider$TimeChecker;
    }
.end annotation


# static fields
.field private static final CANONICAL_ADDRESSES_COLUMNS_1:[Ljava/lang/String;

.field private static final CANONICAL_ADDRESSES_COLUMNS_2:[Ljava/lang/String;

.field private static final EMPTY_STRING_ARRAY:[Ljava/lang/String;

.field private static final FTS_TOKEN_SEPARATOR_RE:Ljava/util/regex/Pattern;

.field private static final FT_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final FT_ONLY_COLUMNS:[Ljava/lang/String;

.field private static final ID_PROJECTION:[Ljava/lang/String;

.field private static final IM_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final IM_ONLY_COLUMNS:[Ljava/lang/String;

.field private static final IM_THREADS_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final IM_THREADS_ONLY_COLUMNS:[Ljava/lang/String;

.field private static MIN_MATCH_CHINA:I

.field private static final MMS_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MMS_ONLY_COLUMNS:[Ljava/lang/String;

.field private static final MMS_SMS_COLUMNS:[Ljava/lang/String;

.field private static final NORMAL_THREADS_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static NORMAL_THREAD_ID_PROJECTION:[Ljava/lang/String;

.field public static final PHONE_EX:Ljava/util/regex/Pattern;

.field private static final PROJECTION_PHONE:[Ljava/lang/String;

.field private static final SEARCH_STRING:[Ljava/lang/String;

.field private static final SMS_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SMS_ONLY_COLUMNS:[Ljava/lang/String;

.field public static final SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

.field private static final THREADS_COLUMNS:[Ljava/lang/String;

.field private static final THREADS_COMMON_COLUMNS:[Ljava/lang/String;

.field private static THREAD_ID_PROJECTION:[Ljava/lang/String;

.field private static final UNION_COLUMNS:[Ljava/lang/String;

.field private static final UNION_THREADS_COLUMNS:[Ljava/lang/String;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;

.field private static final WPM_COLUMNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final WPM_ONLY_COLUMNS:[Ljava/lang/String;

.field private static mEnableChnFolderView:Z

.field private static mEnableMmsSubjectConcept:Z

.field private static mEnableMmsTransactionCustomize4Korea:Z

.field private static mEnableMultiDraftMsgBox:Z

.field public static sEnableBlackBirdRcs:Z

.field private static sEnableCpm:Z

.field public static sEnableFreeMessage:Z


# instance fields
.field private final DEBUG_LOG:Z

.field private EMAIL_FILTER_URI:Landroid/net/Uri;

.field private final MMS_DB_FILE_NAME:Ljava/lang/String;

.field private final MMS_DB_FILE_PATH:Ljava/lang/String;

.field private final MMS_PART_FILES_DIRECTORY_NAME:Ljava/lang/String;

.field private final MMS_PART_FILES_DIRECTORY_PATH:Ljava/lang/String;

.field private final SECRET_MODE_CHANGED:I

.field private final SECRET_MODE_OFF:Ljava/lang/String;

.field private final SECRET_MODE_ON:Ljava/lang/String;

.field private mCliDigit:I

.field private mFreeMessageModeReceiver:Landroid/content/BroadcastReceiver;

.field private mHandler:Landroid/os/Handler;

.field public mIsEnableFingerPrintService:Z

.field private mIsSecretMode:Z

.field private mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private mSecretModeReceiver:Landroid/content/BroadcastReceiver;

.field private mUseCutomCliDigit:Z

.field private mUseStrictPhoneNumberComparation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 122
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 285
    const-string v0, "content://mms-sms/spam-filter"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

    .line 301
    const/16 v0, 0xb

    sput v0, Lcom/android/providers/telephony/MmsSmsProvider;->MIN_MATCH_CHINA:I

    .line 307
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "date_sent"

    aput-object v1, v0, v6

    const-string v1, "read"

    aput-object v1, v0, v7

    const-string v1, "thread_id"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "locked"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sim_imsi"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sub_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    .line 313
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ct_cls"

    aput-object v1, v0, v4

    const-string v1, "ct_l"

    aput-object v1, v0, v5

    const-string v1, "ct_t"

    aput-object v1, v0, v6

    const-string v1, "d_rpt"

    aput-object v1, v0, v7

    const-string v1, "exp"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "m_cls"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "m_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "m_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pri"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "read_status"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "resp_st"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "resp_txt"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "retr_st"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "retr_txt_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "rpt_a"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "rr"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "tr_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "text_only"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "sim_slot"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "spam_report"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "safe_message"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_ONLY_COLUMNS:[Ljava/lang/String;

    .line 326
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "address"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "person"

    aput-object v1, v0, v6

    const-string v1, "reply_path_present"

    aput-object v1, v0, v7

    const-string v1, "service_center"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "error_code"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "callback_number"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "pri"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "teleservice_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "link_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "svc_cmd"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "svc_cmd_content"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "sim_slot"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "spam_report"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "safe_message"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_ONLY_COLUMNS:[Ljava/lang/String;

    .line 333
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "body"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "href"

    aput-object v1, v0, v6

    const-string v1, "si_id"

    aput-object v1, v0, v7

    const-string v1, "created"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "si_expires"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "action"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sim_slot"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_ONLY_COLUMNS:[Ljava/lang/String;

    .line 339
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "transaction_id"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "address"

    aput-object v1, v0, v6

    const-string v1, "hidden"

    aput-object v1, v0, v7

    const-string v1, "display_notification_status"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "session_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "imdn_message_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "delivered_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "rcsdb_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "content_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "remote_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "user_alias"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "displayed_counter"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "service_type"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "recipients"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->IM_ONLY_COLUMNS:[Ljava/lang/String;

    .line 346
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "transaction_id"

    aput-object v1, v0, v4

    const-string v1, "bytes_transf"

    aput-object v1, v0, v5

    const-string v1, "file_name"

    aput-object v1, v0, v6

    const-string v1, "file_path"

    aput-object v1, v0, v7

    const-string v1, "file_size"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "thumbnail_path"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "cancel_reason"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "display_notification_status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "user_alias"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "reject_reason"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "chat_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "rcsdb_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "delivered_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "remote_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "media_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "address"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "session_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "displayed_counter"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "service_type"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "recipients"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->FT_ONLY_COLUMNS:[Ljava/lang/String;

    .line 356
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "recipient_ids"

    aput-object v1, v0, v6

    const-string v1, "message_count"

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COLUMNS:[Ljava/lang/String;

    .line 364
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "message_count"

    aput-object v1, v0, v6

    const-string v1, "recipient_ids"

    aput-object v1, v0, v7

    const-string v1, "snippet"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "snippet_cs"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "error"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "has_attachment"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "alert_expired"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "reply_all"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "group_snippet"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "display_recipient_ids"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "translate_mode"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COMMON_COLUMNS:[Ljava/lang/String;

    .line 372
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "normal_thread_id"

    aput-object v1, v0, v4

    const-string v1, "session_id"

    aput-object v1, v0, v5

    const-string v1, "read_status"

    aput-object v1, v0, v6

    const-string v1, "im_type"

    aput-object v1, v0, v7

    const-string v1, "alias"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "opened"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_ONLY_COLUMNS:[Ljava/lang/String;

    .line 376
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COMMON_COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_THREADS_COLUMNS:[Ljava/lang/String;

    .line 380
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "address"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->CANONICAL_ADDRESSES_COLUMNS_1:[Ljava/lang/String;

    .line 383
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "address"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->CANONICAL_ADDRESSES_COLUMNS_2:[Ljava/lang/String;

    .line 389
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->IM_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->FT_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_COLUMNS:[Ljava/lang/String;

    .line 402
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    .line 405
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    .line 408
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    .line 412
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    .line 413
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    .line 414
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_COLUMNS:Ljava/util/Set;

    .line 415
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->NORMAL_THREADS_COLUMNS:Ljava/util/Set;

    .line 421
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "address"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->ID_PROJECTION:[Ljava/lang/String;

    .line 423
    new-array v0, v4, [Ljava/lang/String;

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    .line 425
    new-array v0, v5, [Ljava/lang/String;

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->SEARCH_STRING:[Ljava/lang/String;

    .line 535
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 536
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "complete-conversations"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 537
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "complete-conversations-including-drafts"

    const/16 v3, 0x26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 538
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "complete-conversations-including-drafts/#"

    const/16 v3, 0x27

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 540
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations/#"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 542
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations/#/recipients"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 546
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations/#/subject"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 551
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations/obsolete"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 553
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/byphone/*"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 560
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "threadID"

    invoke-virtual {v0, v1, v2, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 563
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "canonical-address/#"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 566
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "canonical-addresses"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 568
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "search"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 569
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "searchSuggest"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 580
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "pending"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 583
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "undelivered"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 587
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "notifications"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 589
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "draft"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 591
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "locked"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 593
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "locked/#"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 595
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messageIdToThread"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 598
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "msgbackup"

    const/16 v3, 0x1f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 599
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "msgrestore"

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 602
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allLocked"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 604
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/bystring"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 605
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/bySearchSuggest"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 608
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/suggestsearch"

    const/16 v3, 0x67

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 609
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/totalsearch"

    const/16 v3, 0x68

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 613
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/search_suggest_regex_query"

    const/16 v3, 0x69

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 614
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "messages/search_suggest_regex_query/#"

    const/16 v3, 0x69

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 617
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "wap-push-messages"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 618
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "wap-push-messages/#"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 622
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "reload"

    const/16 v3, 0x3e9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 626
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "db_synchronization"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 630
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allinmessage"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 632
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alloutmessage"

    const/16 v3, 0x29

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 634
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allsentmessage"

    const/16 v3, 0x2a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 636
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alldraft"

    const/16 v3, 0x2b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 638
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allwappush"

    const/16 v3, 0x2c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 640
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allinsim1message"

    const/16 v3, 0x2d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 641
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allinsim2message"

    const/16 v3, 0x2e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 642
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allinsmsmessage"

    const/16 v3, 0x2f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 643
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allinmmsmessage"

    const/16 v3, 0x30

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 645
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alloutsim1message"

    const/16 v3, 0x35

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 646
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alloutsim2message"

    const/16 v3, 0x36

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 647
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alloutsmsmessage"

    const/16 v3, 0x37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 648
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alloutmmsmessage"

    const/16 v3, 0x38

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 650
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allsentsim1message"

    const/16 v3, 0x31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 651
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allsentsim2message"

    const/16 v3, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 652
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allsentsmsmessage"

    const/16 v3, 0x33

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 653
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allsentmmsmessage"

    const/16 v3, 0x34

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 655
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alldraftsmsmessage"

    const/16 v3, 0x39

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 656
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "alldraftmmsmessage"

    const/16 v3, 0x3a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 657
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "allinsentmessage"

    const/16 v3, 0x3b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 662
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "cmas"

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 666
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "last_msg_time/#"

    const/16 v3, 0x23

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 669
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "threads"

    const/16 v3, 0x25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 671
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "spam-filter"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 672
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "spam-filter/#"

    const/16 v3, 0x191

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 673
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "spam-messages"

    const/16 v3, 0x192

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 674
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations/byAddress"

    const/16 v3, 0x1c2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 675
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-conversations/byAddress"

    const/16 v3, 0x1c3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 677
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "draft-messages"

    const/16 v3, 0x24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 680
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "datacreateldu"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 683
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "datacreateunpack"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 685
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "update_threads"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 687
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "unread-messages/first-text"

    const/16 v3, 0x2bd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 689
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "update_thread/#"

    const/16 v3, 0x259

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 691
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "hover_body"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 693
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "hover_pdu"

    const/16 v3, 0x385

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 696
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "ftcontents"

    const/16 v3, 0x834

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 697
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "thread-by-sessionId"

    const/16 v3, 0x835

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 698
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-threads"

    const/16 v3, 0x836

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 699
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "im-info-by-thread"

    const/16 v3, 0x837

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 700
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "im-info-by-sessionid"

    const/16 v3, 0x838

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 701
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "im-threads"

    const/16 v3, 0x839

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 702
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-complete-conversations"

    const/16 v3, 0x83a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 703
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-complete-conversations-including-drafts"

    const/16 v3, 0x83b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 704
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-complete-conversations-including-drafts/#"

    const/16 v3, 0x83c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 705
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-conversations/#"

    const/16 v3, 0x83d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 706
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-conversations_imft/#"

    const/16 v3, 0x83e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 707
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "undelivered_integrated"

    const/16 v3, 0x83f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 708
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "mark-as-read-conversations_imft/"

    const/16 v3, 0x840

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 709
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-draft/#"

    const/16 v3, 0x841

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 710
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "im-info-by-transactionid"

    const/16 v3, 0x842

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 711
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "update-imtype-by-closed-group"

    const/16 v3, 0x843

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 712
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "integrated-conversations/limit"

    const/16 v3, 0x844

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 713
    sget-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "has-im-thread"

    const/16 v3, 0x845

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 716
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->initializeColumnSets()V

    .line 728
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMultiDraftMsgBox:Z

    .line 730
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    .line 732
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    .line 734
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableChnFolderView:Z

    .line 743
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableFreeMessage:Z

    .line 744
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableBlackBirdRcs:Z

    .line 745
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    .line 2448
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->THREAD_ID_PROJECTION:[Ljava/lang/String;

    .line 2449
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "normal_thread_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->NORMAL_THREAD_ID_PROJECTION:[Ljava/lang/String;

    .line 3391
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->PROJECTION_PHONE:[Ljava/lang/String;

    .line 3456
    const-string v0, "[^\u0080-\uffff\\p{Alnum}_]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->FTS_TOKEN_SEPARATOR_RE:Ljava/util/regex/Pattern;

    .line 6322
    const-string v0, "(\\*\\d{2}\\#)?(\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/providers/telephony/MmsSmsProvider;->PHONE_EX:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 479
    const-string v0, "mmssms.db"

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_DB_FILE_NAME:Ljava/lang/String;

    .line 482
    const-string v0, "/data/data/com.android.providers.telephony/databases/"

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_DB_FILE_PATH:Ljava/lang/String;

    .line 483
    const-string v0, "app_parts/"

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_PART_FILES_DIRECTORY_NAME:Ljava/lang/String;

    .line 484
    const-string v0, "/data/data/com.android.providers.telephony/app_parts/"

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_PART_FILES_DIRECTORY_PATH:Ljava/lang/String;

    .line 724
    iput v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    .line 725
    iput-boolean v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    .line 737
    iput-boolean v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsEnableFingerPrintService:Z

    .line 738
    const-string v0, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->SECRET_MODE_ON:Ljava/lang/String;

    .line 739
    const-string v0, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->SECRET_MODE_OFF:Ljava/lang/String;

    .line 740
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->SECRET_MODE_CHANGED:I

    .line 741
    iput-boolean v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsSecretMode:Z

    .line 2874
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->DEBUG_LOG:Z

    .line 3382
    const-string v0, "content://com.android.contacts/data/phone_emails/filter"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->EMAIL_FILTER_URI:Landroid/net/Uri;

    .line 6263
    new-instance v0, Lcom/android/providers/telephony/MmsSmsProvider$1;

    invoke-direct {v0, p0}, Lcom/android/providers/telephony/MmsSmsProvider$1;-><init>(Lcom/android/providers/telephony/MmsSmsProvider;)V

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mHandler:Landroid/os/Handler;

    .line 6303
    new-instance v0, Lcom/android/providers/telephony/MmsSmsProvider$2;

    invoke-direct {v0, p0}, Lcom/android/providers/telephony/MmsSmsProvider$2;-><init>(Lcom/android/providers/telephony/MmsSmsProvider;)V

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 6702
    new-instance v0, Lcom/android/providers/telephony/MmsSmsProvider$3;

    invoke-direct {v0, p0}, Lcom/android/providers/telephony/MmsSmsProvider$3;-><init>(Lcom/android/providers/telephony/MmsSmsProvider;)V

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mFreeMessageModeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private SelectionForDelMMS(ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "uriMatcher"    # I
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    .line 4958
    const-string v0, "msg_box=1"

    .line 4960
    .local v0, "selectionForDelMMS":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 5006
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 5007
    const-string v1, "TP/MmsSmsProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectionForDelMMS : selectionForDelMMS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5012
    :goto_1
    return-object v0

    .line 4963
    :pswitch_1
    const-string v0, "msg_box=1"

    .line 4964
    goto :goto_0

    .line 4967
    :pswitch_2
    const-string v0, "msg_box=2"

    .line 4968
    goto :goto_0

    .line 4971
    :pswitch_3
    const-string v0, "msg_box=4"

    .line 4972
    goto :goto_0

    .line 4975
    :pswitch_4
    const-string v0, "msg_box=3"

    .line 4976
    goto :goto_0

    .line 4978
    :pswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(msg_box=1 OR msg_box=2) AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4979
    goto :goto_0

    .line 4981
    :pswitch_6
    const-string v0, "msg_box=1 AND sim_slot=0"

    .line 4982
    goto :goto_0

    .line 4984
    :pswitch_7
    const-string v0, "msg_box=1 AND sim_slot=1"

    .line 4985
    goto :goto_0

    .line 4987
    :pswitch_8
    const-string v0, "msg_box=2 AND sim_slot=0"

    .line 4988
    goto :goto_0

    .line 4990
    :pswitch_9
    const-string v0, "msg_box=2 AND sim_slot=1"

    .line 4991
    goto :goto_0

    .line 4993
    :pswitch_a
    const-string v0, "msg_box=4 AND sim_slot=0"

    .line 4994
    goto :goto_0

    .line 4996
    :pswitch_b
    const-string v0, "msg_box=4 AND sim_slot=1"

    .line 4997
    goto :goto_0

    .line 5002
    :pswitch_c
    const/4 v0, 0x0

    goto :goto_0

    .line 5009
    :cond_0
    const-string v1, "TP/MmsSmsProvider"

    const-string v2, "SelectionForDelMMS : selectionForDelMMS = null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 4960
    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_c
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_c
        :pswitch_2
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_3
        :pswitch_c
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private SelectionForDelSMS(ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "uriMatcher"    # I
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    .line 4899
    const-string v0, "type=1"

    .line 4901
    .local v0, "selectionForDelSMS":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 4948
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 4949
    const-string v1, "TP/MmsSmsProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectionForDelSMS : selectionForDelSMS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4954
    :goto_1
    return-object v0

    .line 4904
    :pswitch_1
    const-string v0, "type=1"

    .line 4905
    goto :goto_0

    .line 4908
    :pswitch_2
    const-string v0, "type=2"

    .line 4909
    goto :goto_0

    .line 4912
    :pswitch_3
    const-string v0, "(type=4 OR type=5 OR type=6)"

    .line 4913
    goto :goto_0

    .line 4916
    :pswitch_4
    const-string v0, "type=3"

    .line 4917
    goto :goto_0

    .line 4919
    :pswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(type=1 OR type=2) AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4920
    goto :goto_0

    .line 4923
    :pswitch_6
    const-string v0, "type=1 AND sim_slot=0"

    .line 4924
    goto :goto_0

    .line 4926
    :pswitch_7
    const-string v0, "type=1 AND sim_slot=1"

    .line 4927
    goto :goto_0

    .line 4929
    :pswitch_8
    const-string v0, "(type=4 OR type=5 OR type=6) AND sim_slot=0"

    .line 4930
    goto :goto_0

    .line 4932
    :pswitch_9
    const-string v0, "(type=4 OR type=5 OR type=6) AND sim_slot=1"

    .line 4933
    goto :goto_0

    .line 4935
    :pswitch_a
    const-string v0, "type=2 AND sim_slot=0"

    .line 4936
    goto :goto_0

    .line 4938
    :pswitch_b
    const-string v0, "type=2 AND sim_slot=1"

    .line 4939
    goto :goto_0

    .line 4944
    :pswitch_c
    const/4 v0, 0x0

    goto :goto_0

    .line 4951
    :cond_0
    const-string v1, "TP/MmsSmsProvider"

    const-string v2, "SelectionForDelSMS : selectionForDelSMS = null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 4901
    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_c
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_c
        :pswitch_8
        :pswitch_9
        :pswitch_3
        :pswitch_c
        :pswitch_4
        :pswitch_c
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/android/providers/telephony/MmsSmsProvider;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/telephony/MmsSmsProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/android/providers/telephony/MmsSmsProvider;->updateSecretMode(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/providers/telephony/MmsSmsProvider;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/telephony/MmsSmsProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/android/providers/telephony/MmsSmsProvider;->scheduleSecretModeChanged(Z)V

    return-void
.end method

.method private static buildConversationQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 31
    .param p0, "projection"    # [Ljava/lang/String;
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 4458
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 4460
    .local v21, "mmsProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4461
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v25, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v25 .. v25}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4463
    .local v25, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4464
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4465
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4467
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinSmsAndSerivceCategory()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4469
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 4470
    .local v24, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 4471
    .local v20, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v20

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 4472
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v24

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v19

    .line 4474
    .local v19, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4475
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "pdu._id"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4476
    const-string v2, "err_type"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4478
    const-string v2, "msg_box != 3"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 4480
    .local v22, "mmsSelection":Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const-string v7, "(msg_box != 3 AND (m_type = 128 OR m_type = 132 OR m_type = 130))"

    move-object/from16 v0, v22

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 4487
    .local v23, "mmsSubQuery":Ljava/lang/String;
    new-instance v8, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v8, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4488
    .local v8, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4489
    const-string v2, "group_type"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4491
    const-string v2, "service_category"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4493
    const-string v6, "transport_type"

    const/4 v9, 0x0

    const-string v10, "sms"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, v25

    move-object/from16 v7, v19

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 4499
    .local v26, "smsSubQuery":Ljava/lang/String;
    new-instance v9, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v9}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4500
    .local v9, "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4501
    const-string v2, "wpm"

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4502
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v29

    .line 4503
    .local v29, "wpmColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v29

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 4504
    .local v11, "innerWpmProjection":[Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x0

    const-string v14, "wpm"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v15, p1

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 4509
    .local v30, "wpmSubQuery":Ljava/lang/String;
    new-instance v28, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v28 .. v28}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4510
    .local v28, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4512
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v26, v2, v5

    const/4 v5, 0x1

    aput-object v23, v2, v5

    const/4 v5, 0x2

    aput-object v30, v2, v5

    invoke-static/range {p2 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 4516
    .local v27, "unionQuery":Ljava/lang/String;
    new-instance v12, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v12}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4518
    .local v12, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4520
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    move-object/from16 v13, v24

    move-object/from16 v17, p2

    invoke-virtual/range {v12 .. v18}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static buildIntegratedConversationIMnFTQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p0, "projection"    # [Ljava/lang/String;
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 4614
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 4615
    .local v18, "sColumns":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4616
    .local v1, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4617
    const-string v2, "im"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4618
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 4619
    .local v16, "imColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 4620
    .local v3, "innerImProjection":[Ljava/lang/String;
    const-string v2, "transport_type"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/4 v5, 0x0

    const-string v6, "im"

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 4624
    .local v17, "imSubQuery":Ljava/lang/String;
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4625
    .local v4, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4626
    const-string v2, "ft"

    invoke-virtual {v4, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4627
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 4628
    .local v14, "ftColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    invoke-static {v14, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v6

    .line 4629
    .local v6, "innerftProjection":[Ljava/lang/String;
    const-string v5, "transport_type"

    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    const/4 v8, 0x0

    const-string v9, "ft"

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v10, p1

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 4633
    .local v15, "ftSubQuery":Ljava/lang/String;
    new-instance v20, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v20 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4634
    .local v20, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4636
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v17, v2, v5

    const/4 v5, 0x1

    aput-object v15, v2, v5

    invoke-static/range {p2 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v5, v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4640
    .local v19, "unionQuery":Ljava/lang/String;
    new-instance v7, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v7}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4642
    .local v7, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4644
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, v18

    move-object/from16 v12, p2

    invoke-virtual/range {v7 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static buildIntegratedConversationQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 41
    .param p0, "projection"    # [Ljava/lang/String;
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 4527
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 4529
    .local v31, "mmsProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4530
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v35, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v35 .. v35}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4532
    .local v35, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4533
    const/4 v2, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4534
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4536
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinSmsAndSerivceCategory()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4538
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v34

    .line 4539
    .local v34, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 4540
    .local v30, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v30

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 4541
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v34

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v29

    .line 4543
    .local v29, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4544
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "pdu._id"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4545
    const-string v2, "err_type"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4547
    const-string v2, "msg_box != 3"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 4549
    .local v32, "mmsSelection":Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const-string v7, "(msg_box != 3 AND (m_type = 128 OR m_type = 132 OR m_type = 130))"

    move-object/from16 v0, v32

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 4556
    .local v33, "mmsSubQuery":Ljava/lang/String;
    new-instance v8, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v8, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4557
    .local v8, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4558
    const-string v2, "group_type"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4560
    const-string v2, "service_category"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4562
    const-string v6, "transport_type"

    const/4 v9, 0x0

    const-string v10, "sms"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, v35

    move-object/from16 v7, v29

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 4568
    .local v36, "smsSubQuery":Ljava/lang/String;
    new-instance v9, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v9}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4569
    .local v9, "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4570
    const-string v2, "wpm"

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4571
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v39

    .line 4572
    .local v39, "wpmColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v39

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 4573
    .local v11, "innerWpmProjection":[Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x0

    const-string v14, "wpm"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v15, p1

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 4578
    .local v40, "wpmSubQuery":Ljava/lang/String;
    new-instance v12, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v12}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4579
    .local v12, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4580
    const-string v2, "im"

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4581
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 4582
    .local v27, "imColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 4583
    .local v14, "innerImProjection":[Ljava/lang/String;
    const-string v13, "transport_type"

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/16 v16, 0x0

    const-string v17, "im"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v12 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 4587
    .local v28, "imSubQuery":Ljava/lang/String;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4588
    .local v15, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4589
    const-string v2, "ft"

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4590
    invoke-static/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v25

    .line 4591
    .local v25, "ftColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v17

    .line 4592
    .local v17, "innerftProjection":[Ljava/lang/String;
    const-string v16, "transport_type"

    sget-object v18, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    const/16 v19, 0x0

    const-string v20, "ft"

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v21, p1

    invoke-virtual/range {v15 .. v23}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 4596
    .local v26, "ftSubQuery":Ljava/lang/String;
    new-instance v38, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v38 .. v38}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4597
    .local v38, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4599
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v36, v2, v5

    const/4 v5, 0x1

    aput-object v33, v2, v5

    const/4 v5, 0x2

    aput-object v40, v2, v5

    const/4 v5, 0x3

    aput-object v28, v2, v5

    const/4 v5, 0x4

    aput-object v26, v2, v5

    invoke-static/range {p2 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 4603
    .local v37, "unionQuery":Ljava/lang/String;
    new-instance v18, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v18 .. v18}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4605
    .local v18, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4607
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v24, 0x0

    move-object/from16 v19, v34

    move-object/from16 v23, p2

    invoke-virtual/range {v18 .. v24}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private buildWPMSubQuery([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 6351
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6352
    .local v0, "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6353
    const-string v1, "wpm"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6354
    invoke-static {p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 6355
    .local v9, "wpmColumns":[Ljava/lang/String;
    invoke-static {v9, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 6356
    .local v2, "innerWpmProjection":[Ljava/lang/String;
    const-string v1, "transport_type"

    sget-object v3, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    const/4 v4, 0x0

    const-string v5, "wpm"

    move-object v6, p2

    move-object v8, v7

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private checkOrCreateDirectory(Ljava/lang/String;Z)Z
    .locals 8
    .param p1, "_directoryPath"    # Ljava/lang/String;
    .param p2, "_setPermission"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 5693
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5695
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 5696
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 5698
    .local v3, "partFiles":[Ljava/io/File;
    if-nez v3, :cond_0

    .line 5721
    .end local v3    # "partFiles":[Ljava/io/File;
    :goto_0
    return v4

    .line 5701
    .restart local v3    # "partFiles":[Ljava/io/File;
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_1

    .line 5702
    aget-object v4, v3, v2

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 5701
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v4, v5

    .line 5704
    goto :goto_0

    .line 5708
    .end local v2    # "i":I
    .end local v3    # "partFiles":[Ljava/io/File;
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 5709
    if-eqz p2, :cond_3

    .line 5710
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5711
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Ljava/io/File;->setWritable(ZZ)Z

    .line 5712
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    :cond_3
    move v4, v5

    .line 5714
    goto :goto_0

    .line 5716
    :cond_4
    const-string v5, "TP/MmsSmsProvider"

    const-string v6, "directory create fail"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5719
    :catch_0
    move-exception v1

    .line 5720
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v5}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method

.method private static concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "selection1"    # Ljava/lang/String;
    .param p1, "selection2"    # Ljava/lang/String;

    .prologue
    .line 2346
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2351
    .end local p1    # "selection2":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 2348
    .restart local p1    # "selection2":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p0

    .line 2349
    goto :goto_0

    .line 2351
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static containsAlpha(Ljava/lang/String;)Z
    .locals 5
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1364
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 1376
    :cond_0
    :goto_0
    return v3

    .line 1367
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 1369
    .local v2, "len":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 1370
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1372
    .local v0, "c":C
    const/16 v4, 0x61

    if-gt v4, v0, :cond_2

    const/16 v4, 0x7a

    if-le v0, v4, :cond_3

    :cond_2
    const/16 v4, 0x41

    if-gt v4, v0, :cond_4

    const/16 v4, 0x5a

    if-gt v0, v4, :cond_4

    .line 1373
    :cond_3
    const/4 v3, 0x1

    goto :goto_0

    .line 1369
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p0, "old"    # [Ljava/lang/String;

    .prologue
    .line 4289
    array-length v2, p0

    new-array v1, v2, [Ljava/lang/String;

    .line 4290
    .local v1, "newProjection":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 4291
    aget-object v2, p0, v0

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4292
    const-string v2, "pdu._id"

    aput-object v2, v1, v0

    .line 4290
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4294
    :cond_0
    aget-object v2, p0, v0

    aput-object v2, v1, v0

    goto :goto_1

    .line 4297
    :cond_1
    return-object v1
.end method

.method private dbReload()V
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 1358
    iget-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 1359
    return-void
.end method

.method private deleteAllBoxList(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "selection"    # Ljava/lang/String;

    .prologue
    .line 5016
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 5018
    .local v0, "token":J
    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/providers/telephony/MmsSmsProvider;->deleteAllBoxListInner(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 5020
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method private deleteAllBoxListInner(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;)I
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "selection"    # Ljava/lang/String;

    .prologue
    .line 5025
    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v24

    .line 5026
    .local v24, "uriMatcher":I
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList(), uri="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5028
    const-string v7, "type=1"

    .line 5029
    .local v7, "selectionForDelSMS":Ljava/lang/String;
    const-string v20, "msg_box=1"

    .line 5031
    .local v20, "selectionForDelMMS":Ljava/lang/String;
    const/4 v10, 0x0

    .line 5032
    .local v10, "affectedRows":I
    const/16 v19, 0x0

    .line 5033
    .local v19, "mmsAffectedRows":I
    const/16 v21, 0x0

    .line 5034
    .local v21, "smsAffectedRows":I
    const/16 v25, 0x0

    .line 5035
    .local v25, "wpmAffectedRows":I
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v22

    .line 5036
    .local v22, "startT":J
    const/4 v11, 0x0

    .line 5037
    .local v11, "allSmsCount":I
    const/4 v12, 0x0

    .line 5038
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "group_id"

    aput-object v5, v6, v4

    .line 5039
    .local v6, "projection":[Ljava/lang/String;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 5041
    .local v16, "groupIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->SelectionForDelSMS(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 5042
    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->SelectionForDelMMS(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 5045
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "deleteAllBoxList 1)begin db.delete(sms)"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5047
    const/16 v4, 0x2c

    move/from16 v0, v24

    if-eq v0, v4, :cond_9

    .line 5048
    if-eqz v7, :cond_7

    .line 5049
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete from sms where  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and _id in (select _id from sms where  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " limit 100)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 5050
    .local v14, "deleteSms":Ljava/lang/String;
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList 2)  deleteSms ="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5051
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 5052
    if-eqz v12, :cond_3

    .line 5053
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 5055
    :cond_0
    add-int/lit8 v11, v11, 0x1

    .line 5056
    const-string v4, "group_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 5057
    .local v15, "groupId":Ljava/lang/String;
    if-eqz v15, :cond_1

    const-string v4, ""

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 5058
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5060
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 5062
    .end local v15    # "groupId":Ljava/lang/String;
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 5064
    :cond_3
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList 2)  allSmsCount ="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5066
    move/from16 v21, v11

    .line 5068
    :cond_4
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5069
    add-int/lit8 v21, v21, -0x64

    .line 5070
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList 2)"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v22

    const-wide/32 v26, 0xf4240

    div-long v8, v8, v26

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ",end db.delete(sms),affected:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5071
    if-gtz v21, :cond_4

    .line 5073
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 5074
    const-string v13, "delete from sms where "

    .line 5075
    .local v13, "deleteGroup":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " group_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 5076
    const/16 v17, 0x1

    .local v17, "i":I
    :goto_0
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_5

    .line 5077
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " or group_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 5076
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 5079
    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5082
    .end local v13    # "deleteGroup":Ljava/lang/String;
    .end local v17    # "i":I
    :cond_6
    move v10, v11

    .line 5086
    .end local v14    # "deleteSms":Ljava/lang/String;
    :cond_7
    if-eqz v20, :cond_8

    .line 5087
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "deleteAllBoxList 3)begin MmsProvider.deleteMessages"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5088
    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    move-object/from16 v3, p3

    invoke-static {v0, v1, v2, v4, v3}, Lcom/android/providers/telephony/MmsProvider;->deleteAllMessages(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Landroid/net/Uri;)I

    move-result v19

    .line 5089
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList 4)"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v22

    const-wide/32 v26, 0xf4240

    div-long v8, v8, v26

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ",end MmsProvider.deleteMessages,affected:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5097
    :cond_8
    :goto_1
    add-int v4, v19, v25

    add-int/2addr v10, v4

    .line 5098
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList 7  deletedRows = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5100
    new-instance v18, Landroid/content/Intent;

    const-string v4, "com.sec.android.widget.festivalmemory.intent.action.ACTION_DATASET_CHANGED"

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5101
    .local v18, "intent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 5104
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateAllThreads(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    .line 5105
    return v10

    .line 5093
    .end local v18    # "intent":Landroid/content/Intent;
    :cond_9
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "deleteAllBoxList 5)begin db.delete(wpm)"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5094
    const-string v4, "wpm"

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v25

    .line 5095
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteAllBoxList 6)"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v22

    const-wide/32 v26, 0xf4240

    div-long v8, v8, v26

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ",end db.delete(wpm), affected:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private deleteConversation(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 4873
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 4874
    .local v8, "threadId":Ljava/lang/String;
    const-string v0, "TP/MmsSmsProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteConversation threadId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4875
    const/4 v0, 0x1

    new-array v9, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v8, v9, v0

    .line 4877
    .local v9, "whereArg":[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 4878
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "thread_id = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4880
    .local v7, "finalSelection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 4882
    .local v6, "deletedRows":I
    const-string v0, "sms"

    invoke-static {v7}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceTypeFieldNameForSms(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v6, v0

    .line 4883
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v7}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceTypeFieldNameForMms(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v3, p3

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lcom/android/providers/telephony/MmsProvider;->deleteMessages(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Landroid/net/Uri;[J)I

    move-result v0

    add-int/2addr v6, v0

    .line 4884
    const-string v0, "wpm"

    invoke-static {v7}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceTypeFieldNameForSms(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v6, v0

    .line 4887
    const-string v0, "all"

    const-string v2, "type"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4888
    const-string v0, "ft"

    const-string v2, "thread_id"

    const-string v3, "all"

    invoke-static {v1, v0, v2, v9, v3}, Lcom/android/providers/telephony/ImProvider;->deleteDirectory(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)I

    .line 4889
    const-string v0, "im"

    invoke-static {v7}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceTypeFieldNameForSms(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v6, v0

    .line 4890
    const-string v0, "ft"

    invoke-static {v7}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceTypeFieldNameForSms(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v6, v0

    .line 4894
    :cond_0
    return v6
.end method

.method private directoryCopy(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 10
    .param p1, "_sourceDirectoryPath"    # Ljava/lang/String;
    .param p2, "_targetDirectoryPath"    # Ljava/lang/String;
    .param p3, "_setPermission"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 5658
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5660
    .local v2, "sourceDirectory":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 5661
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 5663
    .local v0, "files":[Ljava/lang/String;
    if-nez v0, :cond_1

    .line 5689
    .end local v0    # "files":[Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 5666
    .restart local v0    # "files":[Ljava/lang/String;
    :cond_1
    array-length v7, v0

    if-lez v7, :cond_2

    .line 5667
    invoke-direct {p0, p2, p3}, Lcom/android/providers/telephony/MmsSmsProvider;->checkOrCreateDirectory(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 5671
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v7, v0

    if-ge v1, v7, :cond_4

    .line 5672
    new-instance v3, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5674
    .local v3, "sourceFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_3

    .line 5675
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v0, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/android/providers/telephony/MmsSmsProvider;->fileCopy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 5678
    if-eqz p3, :cond_3

    .line 5679
    new-instance v4, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5681
    .local v4, "targetFile":Ljava/io/File;
    invoke-virtual {v4, v6, v6}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5682
    invoke-virtual {v4, v6, v6}, Ljava/io/File;->setWritable(ZZ)Z

    .line 5683
    invoke-virtual {v4, v6, v6}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 5671
    .end local v4    # "targetFile":Ljava/io/File;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "files":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v3    # "sourceFile":Ljava/io/File;
    :cond_4
    move v5, v6

    .line 5689
    goto :goto_0
.end method

.method private getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;
    .locals 8
    .param p2, "createThread"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1635
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 1637
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1638
    .local v0, "address":Ljava/lang/String;
    const-string v6, "insert-address-token"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1640
    const-string v6, "ro.csc.sales_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1641
    .local v5, "sCurrentSalesCode":Ljava/lang/String;
    const-string v6, "VZW"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "ATT"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1642
    :cond_1
    invoke-direct {p0, v0, p2}, Lcom/android/providers/telephony/MmsSmsProvider;->getSingleAddressIdForVZW(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 1647
    .local v2, "id":J
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-nez v6, :cond_2

    const-string v6, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1648
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1644
    .end local v2    # "id":J
    :cond_3
    invoke-direct {p0, v0, p2}, Lcom/android/providers/telephony/MmsSmsProvider;->getSingleAddressId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .restart local v2    # "id":J
    goto :goto_1

    .line 1650
    :cond_4
    const-string v6, "TP/MmsSmsProvider"

    const-string v7, "getAddressIds: address ID not found for "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1654
    .end local v0    # "address":Ljava/lang/String;
    .end local v2    # "id":J
    .end local v5    # "sCurrentSalesCode":Ljava/lang/String;
    :cond_5
    return-object v4
.end method

.method private getAllLockedMessage([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 26
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;

    .prologue
    .line 2788
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2789
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v22, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v22 .. v22}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2791
    .local v22, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v19, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v19 .. v19}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2792
    .local v19, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2795
    .local v15, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v2, "pdu"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2796
    const-string v2, "sms"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2797
    const-string v2, "im"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2798
    const-string v2, "ft"

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2800
    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "thread_id"

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const-string v4, "null as session_id"

    aput-object v4, v3, v2

    .line 2801
    .local v3, "idMmsUnionColumn":[Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v2, 0x0

    const-string v4, "thread_id"

    aput-object v4, v18, v2

    const/4 v2, 0x1

    const-string v4, "null as session_id"

    aput-object v4, v18, v2

    .line 2802
    .local v18, "idSmsUnionColumn":[Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v2, 0x0

    const-string v4, "thread_id"

    aput-object v4, v17, v2

    const/4 v2, 0x1

    const-string v4, "session_id"

    aput-object v4, v17, v2

    .line 2804
    .local v17, "idImUnionColumn":[Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v6, "mms"

    const-string v9, "thread_id"

    const-string v10, "locked=1"

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 2810
    .local v21, "mmsSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v9, "sms"

    const-string v12, "thread_id"

    const-string v13, "locked=1"

    move-object/from16 v4, v22

    move-object/from16 v6, v18

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 2817
    .local v23, "smsSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v9, "im"

    const-string v12, "thread_id"

    const-string v13, "locked=1"

    move-object/from16 v4, v19

    move-object/from16 v6, v17

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 2823
    .local v20, "imSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v9, "ft"

    const-string v12, "thread_id"

    const-string v13, "locked=1"

    move-object v4, v15

    move-object/from16 v6, v17

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2830
    .local v16, "ftSubQuery":Ljava/lang/String;
    new-instance v25, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v25 .. v25}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2832
    .local v25, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 2835
    const/16 v24, 0x0

    .line 2836
    .local v24, "unionQuery":Ljava/lang/String;
    const-string v2, "all"

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2837
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v21, v2, v4

    const/4 v4, 0x1

    aput-object v23, v2, v4

    const/4 v4, 0x2

    aput-object v20, v2, v4

    const/4 v4, 0x3

    aput-object v16, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 2843
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 2845
    .local v14, "cursor":Landroid/database/Cursor;
    const-string v2, "MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get all locked, cursor count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2846
    return-object v14

    .line 2840
    .end local v14    # "cursor":Landroid/database/Cursor;
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v21, v2, v4

    const/4 v4, 0x1

    aput-object v23, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    goto :goto_0
.end method

.method private getBubbleByIntegratedSearchAllData(Z)Landroid/database/Cursor;
    .locals 9
    .param p1, "returnTF"    # Z

    .prologue
    .line 3840
    if-nez p1, :cond_0

    .line 3841
    const/4 v7, 0x0

    .line 3891
    :goto_0
    return-object v7

    .line 3843
    :cond_0
    const-string v3, ""

    .line 3845
    .local v3, "mmsQuery":Ljava/lang/String;
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v7, :cond_1

    .line 3846
    const-string v3, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, NULL AS cl, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs, display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct=\'text/plain\') AND ((text!=\'<null>\') OR (sub !=\'\'))"

    .line 3857
    :goto_1
    const-string v4, "SELECT sms.date * 1 AS normalized_date, sms._id AS _id, \'sms\' AS transport_type, body as text, NULL AS sub, NULL AS cl,address, recipient_ids, thread_id, \'1\' AS listorder, sms.type AS type, NULL AS sub_cs,display_recipient_ids FROM sms, threads WHERE (sms.thread_id=threads._id) AND (hidden=0) "

    .line 3862
    .local v4, "smsQuery":Ljava/lang/String;
    const-string v6, "SELECT wpm.date * 1 AS normalized_date, wpm._id AS _id, \'wpm\' AS transport_type, body as text, NULL AS sub, NULL AS cl,NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, wpm.type AS type, NULL AS sub_cs,display_recipient_ids FROM wpm, threads WHERE (wpm.thread_id=threads._id) AND (hidden=0)"

    .line 3867
    .local v6, "wpmQuery":Ljava/lang/String;
    const-string v0, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, cl,(select address from addr where type=\'137\' AND msg_id=pdu._id AND pdu.msg_box=\'1\') AS address,recipient_ids, thread_id, \'2\' AS listorder, pdu.msg_box AS type, sub_cs,display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct!=\'application/smil\') AND (ct!=\'text/plain\')"

    .line 3874
    .local v0, "AttachmentQuery":Ljava/lang/String;
    const-string v2, "SELECT im.date * 1 AS normalized_date, im._id AS _id, \'im\' AS transport_type, body as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, im.type AS type, NULL AS sub_cs, display_recipient_ids FROM im, im_threads WHERE (im.thread_id=im_threads.normal_thread_id) AND (im.message_type = 0 OR im.message_type = 5 OR im.message_type = 6) AND (hidden=0) "

    .line 3881
    .local v2, "imQuery":Ljava/lang/String;
    const-string v1, "SELECT ft.date * 1 AS normalized_date, ft._id AS _id, \'ft\' AS transport_type, file_name as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, ft.type AS type, NULL AS sub_cs, display_recipient_ids FROM ft, im_threads WHERE (ft.thread_id=im_threads.normal_thread_id) AND (file_name LIKE ?) AND (hidden=0) "

    .line 3889
    .local v1, "ftQuery":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " UNION "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " UNION "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " UNION "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " UNION "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " UNION "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ORDER BY listorder, normalized_date desc limit 200"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3891
    .local v5, "unionQuery":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    sget-object v8, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_0

    .line 3850
    .end local v0    # "AttachmentQuery":Ljava/lang/String;
    .end local v1    # "ftQuery":Ljava/lang/String;
    .end local v2    # "imQuery":Ljava/lang/String;
    .end local v4    # "smsQuery":Ljava/lang/String;
    .end local v5    # "unionQuery":Ljava/lang/String;
    .end local v6    # "wpmQuery":Ljava/lang/String;
    :cond_1
    const-string v3, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, NULL AS cl,(select address from addr where type=\'137\' AND msg_id=pdu._id AND pdu.msg_box=\'1\') AS address,recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs,display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct=\'text/plain\') AND ((text!=\'<null>\') OR (sub !=\'\'))"

    goto :goto_1
.end method

.method private getBubblesByIntegratedSearch([Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 24
    .param p1, "selectionArgs"    # [Ljava/lang/String;
    .param p2, "returnTF"    # Z

    .prologue
    .line 3643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 3645
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez p2, :cond_0

    .line 3646
    const/16 v22, 0x0

    .line 3835
    :goto_0
    return-object v22

    .line 3648
    :cond_0
    const-string v8, ""

    .line 3649
    .local v8, "mmsQuery":Ljava/lang/String;
    const/16 v18, 0x0

    .line 3650
    .local v18, "whereMMS":Ljava/lang/String;
    const/16 v19, 0x0

    .line 3651
    .local v19, "whereSMS":Ljava/lang/String;
    const/16 v20, 0x0

    .line 3652
    .local v20, "whereWPM":Ljava/lang/String;
    const/4 v15, 0x0

    .line 3653
    .local v15, "whereAttachment":Ljava/lang/String;
    const/16 v17, 0x0

    .line 3654
    .local v17, "whereIm":Ljava/lang/String;
    const/16 v16, 0x0

    .line 3656
    .local v16, "whereFt":Ljava/lang/String;
    move-object/from16 v9, p1

    .line 3657
    .local v9, "result":[Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 3660
    .local v10, "selectinos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 3663
    .local v12, "sub":Ljava/lang/String;
    sget-boolean v22, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v22, :cond_2

    .line 3664
    const-string v18, "(text LIKE ?)"

    .line 3665
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3685
    :goto_1
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_6

    .line 3686
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_2
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_6

    .line 3687
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_1

    .line 3688
    sget-boolean v22, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v22, :cond_4

    .line 3689
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (text LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 3690
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3686
    :cond_1
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 3667
    .end local v6    # "i":I
    :cond_2
    const-string v18, "((text LIKE ?) OR (sub LIKE ?))"

    .line 3668
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3672
    :try_start_0
    new-instance v13, Ljava/lang/String;

    const/16 v22, 0x0

    aget-object v22, v9, v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    const-string v23, "iso-8859-1"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v13, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v12    # "sub":Ljava/lang/String;
    .local v13, "sub":Ljava/lang/String;
    move-object v12, v13

    .line 3677
    .end local v13    # "sub":Ljava/lang/String;
    .restart local v12    # "sub":Ljava/lang/String;
    :goto_4
    if-nez v12, :cond_3

    .line 3678
    const/16 v22, 0x0

    aget-object v12, v9, v22

    .line 3681
    :cond_3
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 3673
    :catch_0
    move-exception v4

    .line 3674
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const-string v22, "TP/MmsSmsProvider"

    const-string v23, "ISO_8859_1 must be supported!"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 3692
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v6    # "i":I
    :cond_4
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND ((text LIKE ?) OR (sub LIKE ?))"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 3693
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3697
    :try_start_1
    new-instance v13, Ljava/lang/String;

    aget-object v22, v9, v6

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    const-string v23, "iso-8859-1"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v13, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v12    # "sub":Ljava/lang/String;
    .restart local v13    # "sub":Ljava/lang/String;
    move-object v12, v13

    .line 3702
    .end local v13    # "sub":Ljava/lang/String;
    .restart local v12    # "sub":Ljava/lang/String;
    :goto_5
    if-nez v12, :cond_5

    .line 3703
    aget-object v12, v9, v6

    .line 3706
    :cond_5
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 3698
    :catch_1
    move-exception v4

    .line 3699
    .restart local v4    # "e":Ljava/io/UnsupportedEncodingException;
    const-string v22, "TP/MmsSmsProvider"

    const-string v23, "ISO_8859_1 must be supported!"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 3713
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v6    # "i":I
    :cond_6
    const-string v19, "(body LIKE ?)"

    .line 3714
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3716
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_8

    .line 3717
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_6
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_8

    .line 3718
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_7

    .line 3719
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (body LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 3720
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3717
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 3725
    .end local v6    # "i":I
    :cond_8
    const-string v20, "(body LIKE ?)"

    .line 3726
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3728
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_a

    .line 3729
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_7
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_a

    .line 3730
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_9

    .line 3731
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (body LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 3732
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3729
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 3738
    .end local v6    # "i":I
    :cond_a
    const-string v17, "(body LIKE ?)"

    .line 3739
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3741
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_c

    .line 3742
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_8
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_c

    .line 3743
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_b

    .line 3744
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (body LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 3745
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3742
    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 3750
    .end local v6    # "i":I
    :cond_c
    const-string v16, "(file_name LIKE ?)"

    .line 3751
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3753
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_e

    .line 3754
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_9
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_e

    .line 3755
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_d

    .line 3756
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (file_name LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 3757
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3754
    :cond_d
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 3763
    .end local v6    # "i":I
    :cond_e
    sget-boolean v22, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v22, :cond_10

    .line 3764
    const-string v15, "(cl LIKE ?)"

    .line 3765
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/telephony/MmsSmsProvider;->toIsoString([B)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3767
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_12

    .line 3768
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_a
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_12

    .line 3769
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_f

    .line 3770
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (cl LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 3771
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/telephony/MmsSmsProvider;->toIsoString([B)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3768
    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    .line 3776
    .end local v6    # "i":I
    :cond_10
    const-string v15, "(cl LIKE ?)"

    .line 3777
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x0

    aget-object v23, v9, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3779
    array-length v0, v9

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_12

    .line 3780
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_b
    array-length v0, v9

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v6, v0, :cond_12

    .line 3781
    rem-int/lit8 v22, v6, 0x2

    if-nez v22, :cond_11

    .line 3782
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (cl LIKE ?)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 3783
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v9, v6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3780
    :cond_11
    add-int/lit8 v6, v6, 0x1

    goto :goto_b

    .line 3789
    .end local v6    # "i":I
    :cond_12
    sget-boolean v22, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v22, :cond_13

    .line 3790
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, NULL AS cl, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs, display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct!=\'application/smil\') AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3801
    :goto_c
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT sms.date * 1 AS normalized_date, sms._id AS _id, \'sms\' AS transport_type, body as text, NULL AS sub, NULL AS cl,address, recipient_ids, thread_id, \'1\' AS listorder, sms.type AS type, NULL AS sub_cs,display_recipient_ids FROM sms, threads WHERE (sms.thread_id=threads._id) AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (hidden=0) "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 3806
    .local v11, "smsQuery":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT wpm.date * 1 AS normalized_date, wpm._id AS _id, \'wpm\' AS transport_type, body as text, NULL AS sub, NULL AS cl,NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, wpm.type AS type, NULL AS sub_cs,display_recipient_ids FROM wpm, threads WHERE (wpm.thread_id=threads._id) AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (hidden=0)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 3811
    .local v21, "wpmQuery":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, cl,(select address from addr where type=\'137\' AND msg_id=pdu._id AND pdu.msg_box=\'1\') AS address,recipient_ids, thread_id, \'2\' AS listorder, pdu.msg_box AS type, sub_cs,display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct!=\'application/smil\') AND (ct!=\'text/plain\') AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3818
    .local v2, "AttachmentQuery":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT im.date * 1 AS normalized_date, im._id AS _id, \'im\' AS transport_type, body as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, im.type AS type, NULL AS sub_cs, display_recipient_ids FROM im, im_threads WHERE (im.thread_id=im_threads.normal_thread_id) AND (im.message_type = 0 OR im.message_type = 5 OR im.message_type = 6) AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (hidden=0) "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 3825
    .local v7, "imQuery":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT ft.date * 1 AS normalized_date, ft._id AS _id, \'ft\' AS transport_type, file_name as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, ft.type AS type, NULL AS sub_cs, display_recipient_ids FROM ft, im_threads WHERE (ft.thread_id=im_threads.normal_thread_id) AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND (hidden=0) "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3833
    .local v5, "ftQuery":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " UNION "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " UNION "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " UNION "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " UNION "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " UNION "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ORDER BY listorder, normalized_date desc"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 3835
    .local v14, "unionQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v23

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [Ljava/lang/String;

    check-cast v22, [Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v14, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto/16 :goto_0

    .line 3794
    .end local v2    # "AttachmentQuery":Ljava/lang/String;
    .end local v5    # "ftQuery":Ljava/lang/String;
    .end local v7    # "imQuery":Ljava/lang/String;
    .end local v11    # "smsQuery":Ljava/lang/String;
    .end local v14    # "unionQuery":Ljava/lang/String;
    .end local v21    # "wpmQuery":Ljava/lang/String;
    :cond_13
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, NULL AS cl,(select address from addr where type=\'137\' AND msg_id=pdu._id AND pdu.msg_box=\'1\') AS address,recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs,display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct!=\'application/smil\') AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_c
.end method

.method private getBubblesBySearch(Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 18
    .param p1, "searchString"    # Ljava/lang/String;
    .param p2, "returnTF"    # Z

    .prologue
    .line 3552
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3554
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez p2, :cond_0

    .line 3555
    const/4 v15, 0x0

    .line 3635
    :goto_0
    return-object v15

    .line 3558
    :cond_0
    const/4 v11, 0x0

    .line 3560
    .local v11, "sub":Ljava/lang/String;
    :try_start_0
    new-instance v12, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    const-string v16, "iso-8859-1"

    move-object/from16 v0, v16

    invoke-direct {v12, v15, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v11    # "sub":Ljava/lang/String;
    .local v12, "sub":Ljava/lang/String;
    move-object v11, v12

    .line 3565
    .end local v12    # "sub":Ljava/lang/String;
    .restart local v11    # "sub":Ljava/lang/String;
    :goto_1
    if-nez v11, :cond_1

    .line 3566
    move-object/from16 v11, p1

    .line 3570
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3571
    .local v3, "findString":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 3572
    .local v7, "mSearchString":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3573
    .local v4, "findSubString":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3575
    .local v8, "mSearchSubString":Ljava/lang/String;
    const-string v9, ""

    .line 3577
    .local v9, "mmsQuery":Ljava/lang/String;
    sget-boolean v15, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v15, :cond_3

    .line 3578
    const-string v9, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, cl, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs, display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct=\'text/plain\') AND ((text LIKE ?)) "

    .line 3597
    :goto_2
    const-string v10, "SELECT sms.date * 1 AS normalized_date, sms._id AS _id, \'sms\' AS transport_type, body as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, sms.type AS type, NULL AS sub_cs, display_recipient_ids FROM sms, threads WHERE (sms.thread_id=threads._id) AND (body LIKE ?) AND (hidden=0) "

    .line 3601
    .local v10, "smsQuery":Ljava/lang/String;
    const-string v14, "SELECT wpm.date * 1 AS normalized_date, wpm._id AS _id, \'wpm\' AS transport_type, body as text, NULL AS sub, NULL AS cl, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, wpm.type AS type, NULL AS sub_cs, display_recipient_ids FROM wpm, threads WHERE (wpm.thread_id=threads._id) AND (body LIKE ?) AND (hidden=0) "

    .line 3605
    .local v14, "wpmQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsEnableFingerPrintService:Z

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsSecretMode:Z

    if-nez v15, :cond_2

    .line 3606
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "AND pdu.thread_id NOT IN(select _id from threads where secret_mode<>0)"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3607
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " AND thread_id NOT IN(select _id from threads where secret_mode<>0)"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 3608
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " AND thread_id NOT IN(select _id from threads where secret_mode<>0)"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 3612
    :cond_2
    const-string v6, "SELECT im.date * 1 AS normalized_date, im._id AS _id, \'im\' AS transport_type, body as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, im.type AS type, NULL AS sub_cs, display_recipient_ids FROM im, im_threads WHERE (im.thread_id=im_threads.normal_thread_id) AND (im.message_type = 0 OR im.message_type = 5 OR im.message_type = 6) AND (body LIKE ?) AND (hidden=0) "

    .line 3619
    .local v6, "imQuery":Ljava/lang/String;
    const-string v5, "SELECT ft.date * 1 AS normalized_date, ft._id AS _id, \'ft\' AS transport_type, file_name as text, NULL AS sub, NULL AS cl, address, recipient_ids, thread_id, \'1\' AS listorder, ft.type AS type, NULL AS sub_cs, display_recipient_ids FROM ft, im_threads WHERE (ft.thread_id=im_threads.normal_thread_id) AND (file_name LIKE ?) AND (hidden=0) "

    .line 3627
    .local v5, "ftQuery":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " UNION "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " UNION "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " UNION "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " UNION "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " ORDER BY listorder, normalized_date desc"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 3630
    .local v13, "unionQuery":Ljava/lang/String;
    sget-boolean v15, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v15, :cond_4

    .line 3631
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    const/16 v16, 0x5

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v7, v16, v17

    const/16 v17, 0x1

    aput-object v7, v16, v17

    const/16 v17, 0x2

    aput-object v7, v16, v17

    const/16 v17, 0x3

    aput-object v7, v16, v17

    const/16 v17, 0x4

    aput-object v7, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v15, v13, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    goto/16 :goto_0

    .line 3561
    .end local v3    # "findString":Ljava/lang/String;
    .end local v4    # "findSubString":Ljava/lang/String;
    .end local v5    # "ftQuery":Ljava/lang/String;
    .end local v6    # "imQuery":Ljava/lang/String;
    .end local v7    # "mSearchString":Ljava/lang/String;
    .end local v8    # "mSearchSubString":Ljava/lang/String;
    .end local v9    # "mmsQuery":Ljava/lang/String;
    .end local v10    # "smsQuery":Ljava/lang/String;
    .end local v13    # "unionQuery":Ljava/lang/String;
    .end local v14    # "wpmQuery":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 3562
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    const-string v15, "TP/MmsSmsProvider"

    const-string v16, "ISO_8859_1 must be supported!"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 3589
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v3    # "findString":Ljava/lang/String;
    .restart local v4    # "findSubString":Ljava/lang/String;
    .restart local v7    # "mSearchString":Ljava/lang/String;
    .restart local v8    # "mSearchSubString":Ljava/lang/String;
    .restart local v9    # "mmsQuery":Ljava/lang/String;
    :cond_3
    const-string v9, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, cl,(select address from addr where type=\'137\' AND msg_id=pdu._id AND pdu.msg_box=\'1\') AS address, recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs, display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct=\'text/plain\') AND ((text LIKE ?) OR (sub LIKE ?)) "

    goto/16 :goto_2

    .line 3635
    .restart local v5    # "ftQuery":Ljava/lang/String;
    .restart local v6    # "imQuery":Ljava/lang/String;
    .restart local v10    # "smsQuery":Ljava/lang/String;
    .restart local v13    # "unionQuery":Ljava/lang/String;
    .restart local v14    # "wpmQuery":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    const/16 v16, 0x6

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v7, v16, v17

    const/16 v17, 0x1

    aput-object v7, v16, v17

    const/16 v17, 0x2

    aput-object v7, v16, v17

    const/16 v17, 0x3

    aput-object v7, v16, v17

    const/16 v17, 0x4

    aput-object v7, v16, v17

    const/16 v17, 0x5

    aput-object v7, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v15, v13, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    goto/16 :goto_0
.end method

.method private getCheckImThread()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 4256
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4258
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "select _id from im_threads limit 1"

    .line 4260
    .local v1, "query":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v3, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getCmasAlertType(Ljava/util/List;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2312
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 2313
    .local v0, "AlertType":I
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCmasAlertType is = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2315
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2316
    .local v2, "address":Ljava/lang/String;
    const-string v4, "#CMAS#Presidential"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "#Emergency Alert#Presidential"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2317
    :cond_0
    const/16 v0, 0x64

    move v1, v0

    .line 2342
    .end local v0    # "AlertType":I
    .end local v2    # "address":Ljava/lang/String;
    .local v1, "AlertType":I
    :goto_0
    return v1

    .line 2319
    .end local v1    # "AlertType":I
    .restart local v0    # "AlertType":I
    .restart local v2    # "address":Ljava/lang/String;
    :cond_1
    const-string v4, "#CMAS#Extreme"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "#Emergency Alert#Extreme"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2320
    :cond_2
    const/16 v0, 0x65

    move v1, v0

    .line 2321
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0

    .line 2322
    .end local v1    # "AlertType":I
    .restart local v0    # "AlertType":I
    :cond_3
    const-string v4, "#CMAS#Severe"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "#Emergency Alert#Severe"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2323
    :cond_4
    const/16 v0, 0x66

    move v1, v0

    .line 2324
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0

    .line 2325
    .end local v1    # "AlertType":I
    .restart local v0    # "AlertType":I
    :cond_5
    const-string v4, "#CMAS#Amber"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "#Emergency Alert#Amber"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2326
    :cond_6
    const/16 v0, 0x67

    move v1, v0

    .line 2327
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0

    .line 2328
    .end local v1    # "AlertType":I
    .restart local v0    # "AlertType":I
    :cond_7
    const-string v4, "#CMAS#Test"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "#Emergency Alert#Test"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2329
    :cond_8
    const/16 v0, 0x68

    move v1, v0

    .line 2330
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0

    .line 2333
    .end local v1    # "AlertType":I
    .restart local v0    # "AlertType":I
    :cond_9
    const-string v4, "#CMAS#CMASALL"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "#Emergency Alert#Alerts"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2334
    :cond_a
    const/16 v0, 0x6e

    move v1, v0

    .line 2335
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0

    .end local v1    # "AlertType":I
    .restart local v0    # "AlertType":I
    :cond_b
    move v1, v0

    .line 2339
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0

    .end local v1    # "AlertType":I
    .end local v2    # "address":Ljava/lang/String;
    .restart local v0    # "AlertType":I
    :cond_c
    move v1, v0

    .line 2342
    .end local v0    # "AlertType":I
    .restart local v1    # "AlertType":I
    goto :goto_0
.end method

.method private getCompleteConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2997
    invoke-static {p1, p2, p3}, Lcom/android/providers/telephony/MmsSmsProvider;->buildConversationQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2999
    .local v0, "unionQuery":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private getCompleteConversationsIncludingDrafts([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 34
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .param p4, "mmsId"    # Ljava/lang/String;

    .prologue
    .line 3016
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 3018
    .local v24, "mmsProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3019
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v28, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v28 .. v28}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3021
    .local v28, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3022
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3023
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3024
    const-string v2, "sms"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3026
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 3027
    .local v27, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 3028
    .local v23, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v23

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 3029
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v22

    .line 3031
    .local v22, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3032
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "pdu._id"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3033
    const-string v2, "err_type"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3035
    const-string v2, "(m_type = 128 OR m_type = 132 OR m_type = 130)"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3037
    .local v7, "mmsSelection":Ljava/lang/String;
    const-string v25, ""

    .line 3038
    .local v25, "mmsSubQuery":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3039
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 3053
    :goto_0
    new-instance v11, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v11, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3054
    .local v11, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v11, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3055
    const-string v2, "group_type"

    invoke-interface {v11, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3058
    const-string v9, "transport_type"

    const/4 v12, 0x0

    const-string v13, "sms"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v8, v28

    move-object/from16 v10, v22

    move-object/from16 v14, p2

    invoke-virtual/range {v8 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 3064
    .local v29, "smsSubQuery":Ljava/lang/String;
    new-instance v12, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v12}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3065
    .local v12, "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3066
    const-string v2, "wpm"

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3067
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v32

    .line 3068
    .local v32, "wpmColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v32

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 3069
    .local v14, "innerWpmProjection":[Ljava/lang/String;
    const-string v13, "transport_type"

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    const/16 v16, 0x0

    const-string v17, "wpm"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v18, p2

    invoke-virtual/range {v12 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 3074
    .local v33, "wpmSubQuery":Ljava/lang/String;
    new-instance v31, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v31 .. v31}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3075
    .local v31, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3077
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v29, v2, v5

    const/4 v5, 0x1

    aput-object v25, v2, v5

    const/4 v5, 0x2

    aput-object v33, v2, v5

    invoke-static/range {p3 .. p3}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 3081
    .local v30, "unionQuery":Ljava/lang/String;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3083
    .local v15, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3085
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    move-object/from16 v16, v27

    move-object/from16 v20, p3

    invoke-virtual/range {v15 .. v21}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 3087
    .local v26, "munionQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 3045
    .end local v11    # "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v12    # "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v14    # "innerWpmProjection":[Ljava/lang/String;
    .end local v15    # "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v26    # "munionQuery":Ljava/lang/String;
    .end local v29    # "smsSubQuery":Ljava/lang/String;
    .end local v30    # "unionQuery":Ljava/lang/String;
    .end local v31    # "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v32    # "wpmColumns":[Ljava/lang/String;
    .end local v33    # "wpmSubQuery":Ljava/lang/String;
    :cond_0
    const-string v9, "transport_type"

    const/4 v12, 0x0

    const-string v13, "mms"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pdu._id!="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v8, v1

    move-object v10, v3

    move-object v11, v4

    invoke-virtual/range {v8 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_0
.end method

.method private getConversationByAddress(Ljava/lang/String;[Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 14
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "integrated"    # Z

    .prologue
    .line 6475
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6476
    const/4 v2, 0x0

    .line 6497
    :goto_0
    return-object v2

    .line 6478
    :cond_0
    const-wide/16 v10, 0x0

    .line 6479
    .local v10, "recipientId":J
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 6480
    .local v12, "sCurrentSalesCode":Ljava/lang/String;
    const-string v2, "VZW"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ATT"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 6481
    :cond_1
    const-string v2, "false"

    invoke-direct {p0, p1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getSingleAddressIdForVZW(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v10

    .line 6486
    :goto_1
    const-string v2, "TP/MmsSmsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getConversationByAddress: recipientId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6487
    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-gtz v2, :cond_3

    .line 6488
    const/4 v2, 0x0

    goto :goto_0

    .line 6483
    :cond_2
    const-string v2, "false"

    invoke-direct {p0, p1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getSingleAddressId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v10

    goto :goto_1

    .line 6490
    :cond_3
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 6491
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz p3, :cond_4

    .line 6492
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WHERE recipient_ids="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 6493
    .local v13, "selection":Ljava/lang/String;
    const-string v2, " (SELECT _id, date, sum(message_count) as message_count, sum(xms_count) as xms_count, sum(im_count) as im_count, recipient_ids, snippet, snippet_cs, read, error, type, has_attachment, sum(unread_count) as unread_count , message_type, display_recipient_ids, translate_mode, safe_message, SUM(xms_thread_id) AS xms_thread_id, SUM(im_thread_id) AS im_thread_id FROM  (SELECT normal_thread_id as _id, date, message_count, 0 as xms_count, message_count as im_count, recipient_ids, snippet, snippet_cs, read, error, 0 as type, has_attachment, unread_count, message_type, display_recipient_ids, translate_mode, 0 as safe_message, NULL AS xms_thread_id, normal_thread_id AS im_thread_id FROM im_threads %s  UNION  SELECT _id, date, message_count, message_count as xms_count, 0 as im_count, recipient_ids, snippet, snippet_cs, read, error, type, has_attachment, unread_count, message_type, display_recipient_ids, translate_mode, safe_message, _id AS xms_thread_id, NULL AS im_thread_id FROM threads %s  ) GROUP by _id ORDER BY date)  LEFT JOIN (SELECT normal_thread_id, im_type, session_id, read_status, alias, opened FROM im_threads) on _id = normal_thread_id"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    const/4 v4, 0x1

    aput-object v13, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 6494
    .local v1, "table":Ljava/lang/String;
    const-string v3, "recipient_ids=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_0

    .line 6497
    .end local v1    # "table":Ljava/lang/String;
    .end local v13    # "selection":Ljava/lang/String;
    :cond_4
    const-string v3, "threads"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recipient_ids="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v0

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_0
.end method

.method private getConversationById(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "threadIdString"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 4179
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 4186
    .local v9, "extraSelection":Ljava/lang/String;
    invoke-static {p3, v9}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4187
    .local v3, "finalSelection":Ljava/lang/String;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4188
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-static {p2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullThreadsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 4190
    .local v2, "columns":[Ljava/lang/String;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4191
    const-string v1, "threads"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4192
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .end local v0    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v3    # "finalSelection":Ljava/lang/String;
    .end local v9    # "extraSelection":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 4180
    :catch_0
    move-exception v8

    .line 4181
    .local v8, "exception":Ljava/lang/NumberFormatException;
    const-string v1, "TP/MmsSmsProvider"

    const-string v4, "Thread ID must be a Long."

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getConversationMessages(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "threadIdString"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 3214
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3220
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3222
    .local v1, "finalSelection":Ljava/lang/String;
    invoke-static {p2, v1, p4}, Lcom/android/providers/telephony/MmsSmsProvider;->buildConversationQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3224
    .local v2, "unionQuery":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .end local v1    # "finalSelection":Ljava/lang/String;
    .end local v2    # "unionQuery":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 3215
    :catch_0
    move-exception v0

    .line 3216
    .local v0, "exception":Ljava/lang/NumberFormatException;
    const-string v3, "TP/MmsSmsProvider"

    const-string v4, "Thread ID must be a Long."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3217
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2624
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2625
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v17, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v17 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2627
    .local v17, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v2, "pdu"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2628
    const-string v2, "sms"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2630
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 2631
    .local v13, "columns":[Ljava/lang/String;
    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_COLUMNS:[Ljava/lang/String;

    const/16 v5, 0x3e8

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 2633
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 2635
    .local v14, "innerSmsProjection":[Ljava/lang/String;
    const-string v2, "transport_type"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    const/4 v5, 0x1

    const-string v6, "mms"

    const-string v7, "(msg_box != 3 AND (m_type = 128 OR m_type = 132 OR m_type = 130))"

    move-object/from16 v0, p2

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "thread_id"

    const-string v9, "date = MAX(date)"

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2640
    .local v15, "mmsSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    const/4 v8, 0x1

    const-string v9, "sms"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "thread_id"

    const-string v12, "date = MAX(date)"

    move-object/from16 v4, v17

    move-object v6, v14

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2645
    .local v18, "smsSubQuery":Ljava/lang/String;
    new-instance v20, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v20 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2647
    .local v20, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 2649
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v15, v2, v5

    const/4 v5, 0x1

    aput-object v18, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2652
    .local v19, "unionQuery":Ljava/lang/String;
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2654
    .local v4, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2656
    const/4 v6, 0x0

    const-string v7, "tid"

    const-string v8, "normalized_date = MAX(normalized_date)"

    const/4 v10, 0x0

    move-object v5, v13

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2660
    .local v16, "outerQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getCustomCliDigitAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 801
    iget-boolean v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    if-gt v0, v1, :cond_1

    .line 804
    .end local p1    # "address":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "address":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getDraftMessages([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 28
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 5988
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 5990
    .local v20, "mmsProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 5991
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v24, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v24 .. v24}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 5992
    .local v24, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 5994
    .local v15, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 5995
    const/4 v2, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 5996
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 5997
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 5998
    const-string v2, "sms"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 5999
    const-string v2, "im"

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6001
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 6002
    .local v23, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 6003
    .local v19, "mmsColumns":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 6004
    .local v14, "imColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 6005
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v18

    .line 6006
    .local v18, "innerSmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    invoke-static {v14, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v17

    .line 6008
    .local v17, "innerImProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 6009
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "pdu._id"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6010
    const-string v2, "err_type"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6012
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v7, p2

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 6017
    .local v21, "mmsSubQuery":Ljava/lang/String;
    const-string v6, "transport_type"

    sget-object v8, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    const/4 v9, 0x0

    const-string v10, "sms"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, v24

    move-object/from16 v7, v18

    move-object/from16 v11, p2

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 6022
    .local v25, "smsSubQuery":Ljava/lang/String;
    const-string v6, "transport_type"

    sget-object v8, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/4 v9, 0x0

    const-string v10, "im"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v15

    move-object/from16 v7, v17

    move-object/from16 v11, p2

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 6027
    .local v16, "imSubQuery":Ljava/lang/String;
    new-instance v27, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v27 .. v27}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6028
    .local v27, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6030
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v25, v2, v6

    const/4 v6, 0x1

    aput-object v21, v2, v6

    const/4 v6, 0x2

    aput-object v16, v2, v6

    invoke-static/range {p3 .. p3}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v6, v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 6034
    .local v26, "unionQuery":Ljava/lang/String;
    new-instance v5, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v5}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6036
    .local v5, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6038
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object/from16 v6, v23

    move-object/from16 v10, p3

    invoke-virtual/range {v5 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 6039
    .local v22, "rawQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v6, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getDraftThread([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2555
    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v2

    const/4 v2, 0x1

    const-string v5, "thread_id"

    aput-object v5, v3, v2

    .line 2556
    .local v3, "innerProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2557
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v17, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v17 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2558
    .local v17, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v13, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v13}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2560
    .local v13, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v2, "pdu"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2561
    const-string v2, "sms"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2562
    const-string v2, "im"

    invoke-virtual {v13, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2564
    const-string v2, "transport_type"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    const/4 v5, 0x1

    const-string v6, "mms"

    const-string v7, "msg_box=3"

    move-object/from16 v0, p2

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2569
    .local v15, "mmsSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    const/4 v8, 0x1

    const-string v9, "sms"

    const-string v2, "type=3"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v4, v17

    move-object v6, v3

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2575
    .local v18, "smsSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/4 v8, 0x1

    const-string v9, "im"

    const-string v2, "type=3"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v4, v13

    move-object v6, v3

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2581
    .local v14, "imSubQuery":Ljava/lang/String;
    new-instance v20, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v20 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2583
    .local v20, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 2585
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v15, v2, v5

    const/4 v5, 0x1

    aput-object v18, v2, v5

    const/4 v5, 0x2

    aput-object v14, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2588
    .local v19, "unionQuery":Ljava/lang/String;
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2590
    .local v4, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2592
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p1

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2595
    .local v16, "outerQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getFTContents([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 4199
    const-string v1, "TP/MmsSmsProvider"

    const-string v3, "getFTContents"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4200
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4201
    .local v0, "QueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "ft"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4203
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v2

    move-object v6, v2

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private getFirstLockedMessage([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 25
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;

    .prologue
    .line 2705
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2706
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v20, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v20 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2708
    .local v20, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v17, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v17 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2709
    .local v17, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2712
    .local v15, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v2, "pdu"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2713
    const-string v2, "sms"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2715
    const-string v2, "im"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2716
    const-string v2, "ft"

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2719
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v2

    .line 2722
    .local v3, "idColumn":[Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v6, "mms"

    const-string v8, "_id"

    const-string v9, "locked=1"

    move-object/from16 v7, p2

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2728
    .local v19, "mmsSubQuery":Ljava/lang/String;
    const-string v5, "transport_type"

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v9, "sms"

    const-string v11, "_id"

    const-string v12, "locked=1"

    move-object/from16 v4, v20

    move-object v6, v3

    move-object/from16 v10, p2

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 2735
    .local v21, "smsSubQuery":Ljava/lang/String;
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2736
    .local v4, "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v2, "wpm"

    invoke-virtual {v4, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2737
    const-string v5, "transport_type"

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v9, "wpm"

    const-string v11, "_id"

    const-string v12, "locked=1"

    move-object v6, v3

    move-object/from16 v10, p2

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 2745
    .local v24, "wpmSubQuery":Ljava/lang/String;
    const-string v6, "transport_type"

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v10, "im"

    const-string v12, "_id"

    const-string v13, "locked=1"

    move-object/from16 v5, v17

    move-object v7, v3

    move-object/from16 v11, p2

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2750
    .local v18, "imSubQuery":Ljava/lang/String;
    const-string v6, "transport_type"

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v10, "ft"

    const-string v12, "_id"

    const-string v13, "locked=1"

    move-object v5, v15

    move-object v7, v3

    move-object/from16 v11, p2

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2757
    .local v16, "ftSubQuery":Ljava/lang/String;
    new-instance v23, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v23 .. v23}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2759
    .local v23, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 2762
    const/16 v22, 0x0

    .line 2763
    .local v22, "unionQuery":Ljava/lang/String;
    const-string v2, "all"

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2764
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v19, v2, v5

    const/4 v5, 0x1

    aput-object v21, v2, v5

    const/4 v5, 0x2

    aput-object v24, v2, v5

    const/4 v5, 0x3

    aput-object v18, v2, v5

    const/4 v5, 0x4

    aput-object v16, v2, v5

    const/4 v5, 0x0

    const-string v6, "1"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 2770
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 2773
    .local v14, "cursor":Landroid/database/Cursor;
    const-string v2, "MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get first locked, cursor count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2774
    return-object v14

    .line 2767
    .end local v14    # "cursor":Landroid/database/Cursor;
    :cond_0
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v19, v2, v5

    const/4 v5, 0x1

    aput-object v21, v2, v5

    const/4 v5, 0x2

    aput-object v24, v2, v5

    const/4 v5, 0x0

    const-string v6, "1"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    goto :goto_0
.end method

.method private getFolderviewMessage([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 16
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .param p5, "boxType"    # I

    .prologue
    .line 2878
    const-string v12, "MmsSmsProvider"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getFolderviewMessage(), boxType="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2880
    const/4 v1, 0x0

    .line 2883
    .local v1, "cursor":Landroid/database/Cursor;
    const-string v10, "transport_type,_id,thread_id,address,sub,sub_cs,body,date,date_sent,m_type,read,msg_box,locked,reserved,hidden,group_id,group_type, sim_slot, sim_imsi"

    .line 2884
    .local v10, "unionprojection":Ljava/lang/String;
    const-string v7, "\'sms\' AS transport_type,_id,thread_id,address,null sub,null sub_cs,body,date,sms.date_sent date_sent,null m_type,read,sms.type msg_box,locked,reserved,hidden,group_id,group_type, sim_slot, sim_imsi"

    .line 2885
    .local v7, "smsProjection":Ljava/lang/String;
    const-string v2, "\'mms\' AS transport_type,pdu._id _id,pdu.thread_id thread_id,null address,pdu.sub sub,pdu.sub_cs sub_cs,null body,pdu.date*1000 date,null date_sent,pdu.m_type m_type,pdu.read read,pdu.msg_box msg_box,pdu.locked locked,pdu.reserved reserved,pdu.hidden hidden,null group_id,null group_type,pdu.sim_slot sim_slot,pdu.sim_imsi sim_imsi"

    .line 2886
    .local v2, "mmsProjection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2887
    .local v8, "smsQuery":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2888
    .local v6, "smsGroupQuery":Ljava/lang/String;
    const/4 v3, 0x0

    .line 2889
    .local v3, "mmsQuery":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2890
    .local v5, "rawQuery":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2891
    .local v9, "smsSelection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2893
    .local v4, "mmsSelection":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 2894
    const-string v12, "sim_slot"

    const-string v13, "sms.sim_slot"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 2895
    const-string v12, "sim_slot"

    const-string v13, "pdu.sim_slot"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 2898
    :cond_0
    sparse-switch p5, :sswitch_data_0

    .line 2987
    :goto_0
    return-object v1

    .line 2900
    :sswitch_0
    if-eqz p2, :cond_1

    .line 2901
    const-string v12, "SELECT %s FROM sms WHERE (sms.type = %d AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2902
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND (pdu.m_type = 132 OR pdu.m_type = 130) AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2907
    :goto_1
    const-string v12, "SELECT %s FROM (%s UNION %s ORDER BY %s)"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    aput-object v8, v13, v14

    const/4 v14, 0x2

    aput-object v3, v13, v14

    const/4 v14, 0x3

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2908
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    sget-object v13, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v12, v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2909
    goto :goto_0

    .line 2904
    :cond_1
    const-string v12, "SELECT %s FROM sms WHERE sms.type = %d"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2905
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND (pdu.m_type = 132 OR pdu.m_type = 130))"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2913
    :sswitch_1
    if-eqz p2, :cond_2

    .line 2914
    const-string v12, "SELECT %s FROM sms WHERE ((group_id IS NULL) AND ((sms.type = %d) OR (sms.type = %d) OR (sms.type = %d)) AND %s)"

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x5

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    const/4 v15, 0x6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x4

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2915
    const-string v12, "SELECT %s FROM sms WHERE ((group_id NOT NULL) AND ((sms.group_type = %d) OR (sms.group_type = %d) OR (sms.group_type = %d)) AND %s)"

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x5

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    const/4 v15, 0x6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x4

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2916
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND pdu.m_type = 128 AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2922
    :goto_2
    const-string v12, "SELECT %s FROM (%s UNION %s UNION %s ORDER BY %s)"

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    aput-object v8, v13, v14

    const/4 v14, 0x2

    aput-object v6, v13, v14

    const/4 v14, 0x3

    aput-object v3, v13, v14

    const/4 v14, 0x4

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2924
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    sget-object v13, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v12, v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2925
    goto/16 :goto_0

    .line 2918
    :cond_2
    const-string v12, "SELECT %s FROM sms WHERE ((group_id IS NULL) AND ((sms.type = %d) OR (sms.type = %d) OR (sms.type = %d)))"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x5

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    const/4 v15, 0x6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2919
    const-string v12, "SELECT %s FROM sms WHERE ((group_id NOT NULL) AND ((sms.group_type = %d) OR (sms.group_type = %d) OR (sms.group_type = %d)))"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x5

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    const/4 v15, 0x6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2920
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND pdu.m_type = 128)"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2928
    :sswitch_2
    if-eqz p2, :cond_3

    .line 2929
    const-string v12, "SELECT %s FROM sms WHERE ((group_id IS NULL) AND (sms.type = %d) AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2930
    const-string v12, "SELECT %s FROM sms WHERE ((group_id NOT NULL) AND (sms.group_type = %d) AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2931
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND pdu.m_type = 128 AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2937
    :goto_3
    const-string v12, "SELECT %s FROM (%s UNION %s UNION %s  ORDER BY %s)"

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    aput-object v8, v13, v14

    const/4 v14, 0x2

    aput-object v6, v13, v14

    const/4 v14, 0x3

    aput-object v3, v13, v14

    const/4 v14, 0x4

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2939
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    sget-object v13, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v12, v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2940
    goto/16 :goto_0

    .line 2933
    :cond_3
    const-string v12, "SELECT %s FROM sms WHERE ((group_id IS NULL) AND (sms.type = %d))"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2934
    const-string v12, "SELECT %s FROM sms WHERE ((group_id NOT NULL) AND (sms.group_type = %d))"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2935
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND pdu.m_type = 128)"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2943
    :sswitch_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ",m_id"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2944
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ",null m_id"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2945
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ",pdu.m_id m_id"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2947
    if-eqz p2, :cond_4

    .line 2948
    const-string v12, "SELECT %s FROM sms WHERE (sms.type = %d AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2949
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND pdu.m_type = 128 AND pdu.thread_id!=9223372036854775806 AND %s)"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2954
    :goto_4
    const-string v12, "SELECT %s FROM (%s UNION %s ORDER BY %s)"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    aput-object v8, v13, v14

    const/4 v14, 0x2

    aput-object v3, v13, v14

    const/4 v14, 0x3

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2956
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    sget-object v13, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v12, v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2957
    goto/16 :goto_0

    .line 2951
    :cond_4
    const-string v12, "SELECT %s FROM sms WHERE sms.type = %d"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2952
    const-string v12, "SELECT %s FROM pdu WHERE (pdu.msg_box=%d AND pdu.m_type = 128 AND pdu.thread_id!=9223372036854775806)"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 2960
    :sswitch_4
    const-string v10, "\'sms\' AS transport_type,_id,thread_id,null address,null sub,null sub_cs,body,date,date_sent,null m_type,read,null msg_box,locked,null reserved,null hidden,null group_id,null group_type, wpm.sim_slot sim_slot, wpm.sim_imsi sim_imsi"

    .line 2961
    if-eqz p2, :cond_5

    .line 2962
    const-string v12, "sim_slot"

    const-string v13, "wpm.sim_slot"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 2963
    .local v11, "wpmSelection":Ljava/lang/String;
    const-string v12, "SELECT %s FROM wpm WHERE %s ORDER BY %s"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v11, v13, v14

    const/4 v14, 0x1

    aput-object v10, v13, v14

    const/4 v14, 0x2

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2968
    .end local v11    # "wpmSelection":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    sget-object v13, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v12, v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2969
    goto/16 :goto_0

    .line 2965
    :cond_5
    const-string v12, "SELECT %s FROM wpm ORDER BY %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 2972
    :sswitch_5
    if-eqz p2, :cond_6

    .line 2973
    const-string v12, "SELECT %s FROM sms WHERE ((sms.type = %d OR (hidden = 0 AND sms.type = %d)) AND %s)"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2974
    const-string v12, "SELECT %s FROM pdu WHERE (((pdu.msg_box=%d AND (pdu.m_type = 132 OR pdu.m_type = 130)) OR (pdu.msg_box=%d AND pdu.m_type = 128)) AND %s)"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2980
    :goto_6
    const-string v12, "SELECT %s FROM (%s UNION %s ORDER BY %s)"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    aput-object v8, v13, v14

    const/4 v14, 0x2

    aput-object v3, v13, v14

    const/4 v14, 0x3

    const-string v15, "date DESC"

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2981
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    sget-object v13, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v12, v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2982
    goto/16 :goto_0

    .line 2977
    :cond_6
    const-string v12, "SELECT %s FROM sms WHERE (sms.type = %d OR (hidden = 0 AND sms.type = %d))"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2978
    const-string v12, "SELECT %s FROM pdu WHERE ((pdu.msg_box=%d AND (pdu.m_type = 132 OR pdu.m_type = 130)) OR (pdu.msg_box=%d AND pdu.m_type = 128))"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    .line 2898
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2a -> :sswitch_2
        0x2b -> :sswitch_3
        0x2c -> :sswitch_4
        0x3b -> :sswitch_5
    .end sparse-switch
.end method

.method private getImConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 4209
    const-string v1, "TP/MmsSmsProvider"

    const-string v2, "getImConversations"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4210
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4212
    .local v0, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "im_threads"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, p3

    move-object v6, v3

    .line 4214
    invoke-virtual/range {v0 .. v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4217
    .local v7, "queryString":Ljava/lang/String;
    const-string v1, "TP/MmsSmsProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getImConversations queryString="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 4219
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v1, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private getImThreadInfoBySessionId(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 4248
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4250
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "SELECT _id, normal_thread_id, recipient_ids, display_recipient_ids, read_status, opened, alias, im_type FROM im_threads WHERE session_id=?"

    .line 4252
    .local v1, "query":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getImThreadInfoByThreadId(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "threadId"    # Ljava/lang/String;

    .prologue
    .line 4233
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4234
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz p1, :cond_0

    .line 4235
    const-string v1, "SELECT session_id, normal_thread_id, date, recipient_ids, display_recipient_ids, message_count, snippet, read_status, opened, alias, im_type FROM im_threads WHERE normal_thread_id=?"

    .line 4237
    .local v1, "query":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4243
    :goto_0
    return-object v2

    .line 4241
    .end local v1    # "query":Ljava/lang/String;
    :cond_0
    const-string v1, "SELECT session_id, normal_thread_id, date, recipient_ids, display_recipient_ids, message_count, snippet, read_status, opened, alias, im_type FROM im_threads"

    .line 4243
    .restart local v1    # "query":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0
.end method

.method private getImTransactionIdBySessionId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "transactionid"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "chatType"    # Ljava/lang/String;

    .prologue
    .line 4264
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4265
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 4267
    .local v1, "query":Ljava/lang/String;
    const-string v2, "FT"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4268
    const-string v1, "SELECT thread_id FROM ft WHERE transaction_id=? and address=?"

    .line 4272
    :goto_0
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 4270
    :cond_0
    const-string v1, "SELECT thread_id FROM im WHERE transaction_id=? and address=?"

    goto :goto_0
.end method

.method private getIntegratedCompleteConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 3004
    invoke-static {p1, p2, p3}, Lcom/android/providers/telephony/MmsSmsProvider;->buildIntegratedConversationQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3006
    .local v0, "unionQuery":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private getIntegratedCompleteConversationsIncludingDrafts([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 44
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .param p4, "mmsId"    # Ljava/lang/String;

    .prologue
    .line 3093
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v34

    .line 3095
    .local v34, "mmsProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3096
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v38, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v38 .. v38}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3098
    .local v38, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3099
    const/4 v2, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3100
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3101
    const-string v2, "sms"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3103
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v37

    .line 3104
    .local v37, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v33

    .line 3105
    .local v33, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v33

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 3106
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v37

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v32

    .line 3108
    .local v32, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3109
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "pdu._id"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3110
    const-string v2, "err_type"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3112
    const-string v2, "(m_type = 128 OR m_type = 132 OR m_type = 130)"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3114
    .local v7, "mmsSelection":Ljava/lang/String;
    const-string v35, ""

    .line 3115
    .local v35, "mmsSubQuery":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3116
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 3130
    :goto_0
    new-instance v11, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v11, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3131
    .local v11, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v11, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3132
    const-string v2, "group_type"

    invoke-interface {v11, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3135
    const-string v9, "transport_type"

    const/4 v12, 0x0

    const-string v13, "sms"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v8, v38

    move-object/from16 v10, v32

    move-object/from16 v14, p2

    invoke-virtual/range {v8 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    .line 3141
    .local v39, "smsSubQuery":Ljava/lang/String;
    new-instance v12, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v12}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3142
    .local v12, "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3143
    const-string v2, "wpm"

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3144
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v42

    .line 3145
    .local v42, "wpmColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v42

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 3146
    .local v14, "innerWpmProjection":[Ljava/lang/String;
    const-string v13, "transport_type"

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    const/16 v16, 0x0

    const-string v17, "wpm"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v18, p2

    invoke-virtual/range {v12 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 3151
    .local v43, "wpmSubQuery":Ljava/lang/String;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3152
    .local v15, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3153
    const-string v2, "im"

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3154
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 3155
    .local v30, "imColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v30

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v17

    .line 3156
    .local v17, "innerimProjection":[Ljava/lang/String;
    const-string v16, "transport_type"

    sget-object v18, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/16 v19, 0x0

    const-string v20, "im"

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v21, p2

    invoke-virtual/range {v15 .. v23}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 3160
    .local v31, "imSubQuery":Ljava/lang/String;
    new-instance v18, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v18 .. v18}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3161
    .local v18, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3162
    const-string v2, "ft"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3163
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v28

    .line 3164
    .local v28, "ftColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v20

    .line 3165
    .local v20, "innerftProjection":[Ljava/lang/String;
    const-string v19, "transport_type"

    sget-object v21, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    const/16 v22, 0x0

    const-string v23, "ft"

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v24, p2

    invoke-virtual/range {v18 .. v26}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 3169
    .local v29, "ftSubQuery":Ljava/lang/String;
    new-instance v41, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v41 .. v41}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3170
    .local v41, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v41

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3172
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v39, v2, v5

    const/4 v5, 0x1

    aput-object v35, v2, v5

    const/4 v5, 0x2

    aput-object v43, v2, v5

    const/4 v5, 0x3

    aput-object v31, v2, v5

    const/4 v5, 0x4

    aput-object v29, v2, v5

    invoke-static/range {p3 .. p3}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v41

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 3178
    .local v40, "unionQuery":Ljava/lang/String;
    new-instance v21, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v21 .. v21}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3180
    .local v21, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3182
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v27, 0x0

    move-object/from16 v22, v37

    move-object/from16 v26, p3

    invoke-virtual/range {v21 .. v27}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 3184
    .local v36, "munionQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 3122
    .end local v11    # "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v12    # "wpmQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v14    # "innerWpmProjection":[Ljava/lang/String;
    .end local v15    # "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v17    # "innerimProjection":[Ljava/lang/String;
    .end local v18    # "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v20    # "innerftProjection":[Ljava/lang/String;
    .end local v21    # "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v28    # "ftColumns":[Ljava/lang/String;
    .end local v29    # "ftSubQuery":Ljava/lang/String;
    .end local v30    # "imColumns":[Ljava/lang/String;
    .end local v31    # "imSubQuery":Ljava/lang/String;
    .end local v36    # "munionQuery":Ljava/lang/String;
    .end local v39    # "smsSubQuery":Ljava/lang/String;
    .end local v40    # "unionQuery":Ljava/lang/String;
    .end local v41    # "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v42    # "wpmColumns":[Ljava/lang/String;
    .end local v43    # "wpmSubQuery":Ljava/lang/String;
    :cond_0
    const-string v9, "transport_type"

    const/4 v12, 0x0

    const-string v13, "mms"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pdu._id!="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v8, v1

    move-object v10, v3

    move-object v11, v4

    invoke-virtual/range {v8 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_0
.end method

.method private getIntegratedConversationIMnFT(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "threadIdString"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 3249
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3257
    .local v1, "finalSelection":Ljava/lang/String;
    invoke-static {p2, v1, p4}, Lcom/android/providers/telephony/MmsSmsProvider;->buildIntegratedConversationIMnFTQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3259
    .local v2, "unionQuery":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .end local v1    # "finalSelection":Ljava/lang/String;
    .end local v2    # "unionQuery":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 3250
    :catch_0
    move-exception v0

    .line 3251
    .local v0, "exception":Ljava/lang/NumberFormatException;
    const-string v3, "TP/MmsSmsProvider"

    const-string v4, "Thread ID must be a Long."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3252
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getIntegratedConversationMessages(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "threadIdString"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 3232
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3238
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3240
    .local v1, "finalSelection":Ljava/lang/String;
    invoke-static {p2, v1, p4}, Lcom/android/providers/telephony/MmsSmsProvider;->buildIntegratedConversationQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3242
    .local v2, "unionQuery":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .end local v1    # "finalSelection":Ljava/lang/String;
    .end local v2    # "unionQuery":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 3233
    :catch_0
    move-exception v0

    .line 3234
    .local v0, "exception":Ljava/lang/NumberFormatException;
    const-string v3, "TP/MmsSmsProvider"

    const-string v4, "Thread ID must be a Long."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3235
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getIntegratedConversationsLimit([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2453
    const/4 v14, 0x0

    .line 2454
    .local v14, "limit":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2455
    const-string v3, "LIMIT"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    .line 2456
    .local v15, "offset":I
    if-lez v15, :cond_0

    .line 2457
    move-object/from16 v0, p4

    invoke-virtual {v0, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 2460
    .end local v15    # "offset":I
    :cond_0
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2461
    const-string v3, "TP/MmsSmsProvider"

    const-string v4, "getIntegratedConversationsLimit has no limit"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2462
    const/4 v3, 0x0

    .line 2525
    :goto_0
    return-object v3

    .line 2464
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2466
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 2467
    .local v11, "ids":Ljava/lang/StringBuilder;
    const-string v3, "threads"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->THREAD_ID_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "date DESC "

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 2468
    .local v18, "threadCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_4

    .line 2470
    :goto_1
    :try_start_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2471
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 2472
    const-string v3, ", "

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2474
    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2477
    :catchall_0
    move-exception v3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 2481
    :cond_4
    const-string v3, "im_threads"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->NORMAL_THREAD_ID_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "date DESC "

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 2482
    .local v12, "imThreadCursor":Landroid/database/Cursor;
    if-eqz v12, :cond_7

    .line 2484
    :goto_2
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2485
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 2486
    const-string v3, ", "

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2488
    :cond_5
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 2491
    :catchall_1
    move-exception v3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2495
    :cond_7
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-nez v3, :cond_8

    .line 2496
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2498
    :cond_8
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2499
    .local v10, "idList":Ljava/lang/String;
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "idList="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WHERE normal_thread_id IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 2502
    .local v13, "imThreadSelection":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WHERE _id IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 2503
    .local v19, "threadSelection":Ljava/lang/String;
    const-string v3, " (SELECT _id, date, sum(message_count) as message_count, sum(xms_count) as xms_count, sum(im_count) as im_count, recipient_ids, snippet, snippet_cs, read, error, type, has_attachment, sum(unread_count) as unread_count , message_type, display_recipient_ids, translate_mode, safe_message, SUM(xms_thread_id) AS xms_thread_id, SUM(im_thread_id) AS im_thread_id FROM  (SELECT normal_thread_id as _id, date, message_count, 0 as xms_count, message_count as im_count, recipient_ids, snippet, snippet_cs, read, error, 0 as type, has_attachment, unread_count, message_type, display_recipient_ids, translate_mode, 0 as safe_message, NULL AS xms_thread_id, normal_thread_id AS im_thread_id FROM im_threads %s  UNION  SELECT _id, date, message_count, message_count as xms_count, 0 as im_count, recipient_ids, snippet, snippet_cs, read, error, type, has_attachment, unread_count, message_type, display_recipient_ids, translate_mode, safe_message, _id AS xms_thread_id, NULL AS im_thread_id FROM threads %s  ) GROUP by _id ORDER BY date)  LEFT JOIN (SELECT normal_thread_id, im_type, session_id, read_status, alias, opened FROM im_threads) on _id = normal_thread_id"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v13, v4, v5

    const/4 v5, 0x1

    aput-object v19, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 2505
    .local v17, "table":Ljava/lang/String;
    new-instance v16, Ljava/lang/StringBuilder;

    const/16 v3, 0x78

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2506
    .local v16, "query":Ljava/lang/StringBuilder;
    const-string v3, "SELECT "

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2507
    if-eqz p1, :cond_b

    move-object/from16 v0, p1

    array-length v3, v0

    if-eqz v3, :cond_b

    .line 2508
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendColumns(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    .line 2512
    :goto_3
    const-string v3, "FROM "

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2513
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2515
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 2516
    const-string v3, " WHERE "

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2517
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2520
    :cond_9
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 2521
    const-string v3, " ORDER BY "

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2522
    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2525
    :cond_a
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_0

    .line 2510
    :cond_b
    const-string v3, "* "

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method private getIntegratedDraft(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "threadId"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 4223
    iget-object v4, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4225
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT \'sms\' AS transport_type, body, date, reserved, pri, type from sms where type = 3 and thread_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4226
    .local v2, "smsQuery":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT \'im\' AS transport_type, body, date, reserved, NULL AS pri, type from im where type = 3 and thread_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4228
    .local v1, "imQuery":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UNION "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4229
    .local v3, "unionQuery":Ljava/lang/String;
    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    return-object v4
.end method

.method private getIntegratedSimpleConversations([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2425
    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2427
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x78

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2428
    .local v1, "query":Ljava/lang/StringBuilder;
    const-string v2, "SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2429
    if-eqz p1, :cond_2

    array-length v2, p1

    if-eqz v2, :cond_2

    .line 2430
    invoke-static {v1, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendColumns(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    .line 2434
    :goto_0
    const-string v2, "FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2435
    const-string v2, " (SELECT _id, date, sum(message_count) as message_count, sum(xms_count) as xms_count, sum(im_count) as im_count, recipient_ids, snippet, snippet_cs, read, error, type, has_attachment, sum(unread_count) as unread_count , message_type, display_recipient_ids, translate_mode, safe_message, SUM(xms_thread_id) AS xms_thread_id, SUM(im_thread_id) AS im_thread_id FROM  (SELECT normal_thread_id as _id, date, message_count, 0 as xms_count, message_count as im_count, recipient_ids, snippet, snippet_cs, read, error, 0 as type, has_attachment, unread_count, message_type, display_recipient_ids, translate_mode, 0 as safe_message, NULL AS xms_thread_id, normal_thread_id AS im_thread_id FROM im_threads  UNION  SELECT _id, date, message_count, message_count as xms_count, 0 as im_count, recipient_ids, snippet, snippet_cs, read, error, type, has_attachment, unread_count, message_type, display_recipient_ids, translate_mode, safe_message, _id AS xms_thread_id, NULL AS im_thread_id FROM threads  ) GROUP by _id ORDER BY date)  LEFT JOIN (SELECT normal_thread_id, im_type, session_id, read_status, alias, opened FROM im_threads) on _id = normal_thread_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2437
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2438
    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2439
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2441
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2442
    const-string v2, " ORDER BY "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2443
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2445
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 2432
    :cond_2
    const-string v2, "* "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getIntegratedSpamMessages([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 36
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 3324
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3325
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v32, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v32 .. v32}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3326
    .local v32, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3327
    const/4 v2, 0x1

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3328
    const-string v2, "spam_pdu"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3329
    const-string v2, "spam_sms"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3330
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 3331
    .local v31, "smsColumns":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 3332
    .local v27, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v27

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 3333
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v26

    .line 3334
    .local v26, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3335
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "msg_box != 3"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 3337
    .local v28, "mmsSelection":Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const-string v7, "(msg_box != 3 AND (m_type = 128 OR m_type = 132 OR m_type = 130))"

    move-object/from16 v0, v28

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 3342
    .local v29, "mmsSubQuery":Ljava/lang/String;
    new-instance v8, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v8, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3343
    .local v8, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3344
    const-string v2, "group_type"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3345
    const-string v6, "transport_type"

    const/4 v9, 0x0

    const-string v10, "sms"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, v32

    move-object/from16 v7, v26

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 3350
    .local v33, "smsSubQuery":Ljava/lang/String;
    new-instance v9, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v9}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3351
    .local v9, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3352
    const-string v2, "spam_im"

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3353
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 3354
    .local v24, "imColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v24

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 3355
    .local v11, "innerImProjection":[Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x0

    const-string v14, "im"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v15, p2

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 3359
    .local v25, "imSubQuery":Ljava/lang/String;
    new-instance v12, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v12}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3360
    .local v12, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3361
    const-string v2, "spam_ft"

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3362
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v22

    .line 3363
    .local v22, "ftColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 3364
    .local v14, "innerftProjection":[Ljava/lang/String;
    const-string v13, "transport_type"

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    const/16 v16, 0x0

    const-string v17, "ft"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v18, p2

    invoke-virtual/range {v12 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 3368
    .local v23, "ftSubQuery":Ljava/lang/String;
    new-instance v35, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v35 .. v35}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3369
    .local v35, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3370
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v33, v2, v5

    const/4 v5, 0x1

    aput-object v29, v2, v5

    const/4 v5, 0x2

    aput-object v25, v2, v5

    const/4 v5, 0x3

    aput-object v23, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 3373
    .local v34, "unionQuery":Ljava/lang/String;
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3374
    .local v15, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3375
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    move-object/from16 v16, v31

    move-object/from16 v20, p3

    invoke-virtual/range {v15 .. v21}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 3377
    .local v30, "query":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getIntegratedUndeliveredMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 50
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 4359
    const-string v3, "TP/MmsSmsProvider"

    const-string v6, "getIntegratedUndeliveredMessages "

    invoke-static {v3, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4361
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v42

    .line 4363
    .local v42, "mmsProjection":[Ljava/lang/String;
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4364
    .local v2, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v46, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v46 .. v46}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4366
    .local v46, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4367
    const-string v3, "sms"

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4369
    const-string v3, "msg_box = 4"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4371
    .local v8, "finalMmsSelection":Ljava/lang/String;
    const-string v3, "(type = 4 OR type = 5)"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 4378
    .local v15, "finalSmsSelection":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v45

    .line 4379
    .local v45, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v42 .. v42}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v41

    .line 4380
    .local v41, "mmsColumns":[Ljava/lang/String;
    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-direct {v0, v1, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 4382
    .local v4, "innerMmsProjection":[Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 4385
    .local v11, "innerSmsProjection":[Ljava/lang/String;
    new-instance v5, Ljava/util/HashSet;

    sget-object v3, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v5, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4386
    .local v5, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v3, "pdu._id"

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4387
    const-string v3, "err_type"

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4388
    const-string v3, "transport_type"

    const/4 v6, 0x1

    const-string v7, "mms"

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 4392
    .local v43, "mmsSubQuery":Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x1

    const-string v14, "sms"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v9, v46

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    .line 4397
    .local v47, "smsSubQuery":Ljava/lang/String;
    new-instance v16, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v16 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4398
    .local v16, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4399
    const-string v3, "im"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4400
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v39

    .line 4401
    .local v39, "imColumns":[Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v18

    .line 4402
    .local v18, "innerImProjection":[Ljava/lang/String;
    const-string v3, "type = 5"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 4404
    .local v22, "finalImSelection":Ljava/lang/String;
    const-string v17, "transport_type"

    sget-object v19, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/16 v20, 0x1

    const-string v21, "im"

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v16 .. v24}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 4408
    .local v40, "imSubQuery":Ljava/lang/String;
    const-string v3, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getUndeliveredMessages imSubQuery="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 4410
    new-instance v23, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v23 .. v23}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4411
    .local v23, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4412
    const-string v3, "ft"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4413
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v37

    .line 4414
    .local v37, "ftColumns":[Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v25

    .line 4415
    .local v25, "innerFtProjection":[Ljava/lang/String;
    const-string v3, "status = 3 AND cancel_reason > 1"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 4418
    .local v29, "finalFTSelection":Ljava/lang/String;
    const-string v24, "transport_type"

    sget-object v26, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    const/16 v27, 0x1

    const-string v28, "ft"

    const/16 v30, 0x0

    const/16 v31, 0x0

    invoke-virtual/range {v23 .. v31}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 4422
    .local v38, "ftSubQuery":Ljava/lang/String;
    const-string v3, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getUndeliveredMessages ftSubQuery="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 4424
    new-instance v49, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v49 .. v49}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4426
    .local v49, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x1

    move-object/from16 v0, v49

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4428
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v47, v3, v6

    const/4 v6, 0x1

    aput-object v43, v3, v6

    const/4 v6, 0x2

    aput-object v40, v3, v6

    const/4 v6, 0x3

    aput-object v38, v3, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v6, v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 4431
    .local v48, "unionQuery":Ljava/lang/String;
    new-instance v30, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v30 .. v30}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4433
    .local v30, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "("

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4435
    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v36, 0x0

    move-object/from16 v31, v45

    move-object/from16 v35, p4

    invoke-virtual/range {v30 .. v36}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 4438
    .local v44, "outerQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v6, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-virtual {v3, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method private getMessagesByPhoneNumber(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 24
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 4129
    invoke-static/range {p1 .. p1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 4130
    .local v18, "escapedPhoneNumber":Ljava/lang/String;
    const-string v3, "pdu._id = matching_addresses.address_msg_id"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4134
    .local v8, "finalMmsSelection":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(address="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " OR PHONE_NUMBERS_EQUAL(address, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseStrictPhoneNumberComparation:Z

    if-eqz v3, :cond_0

    const-string v3, ", 1))"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 4140
    .local v15, "finalSmsSelection":Ljava/lang/String;
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4141
    .local v2, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v20, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v20 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4143
    .local v20, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4144
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4145
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pdu, (SELECT msg_id AS address_msg_id FROM addr WHERE (address="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " OR PHONE_NUMBERS_EQUAL(addr.address, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseStrictPhoneNumberComparation:Z

    if-eqz v3, :cond_1

    const-string v3, ", 1))) "

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "AS matching_addresses"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4153
    const-string v3, "sms"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4155
    invoke-static/range {p2 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 4156
    .local v4, "columns":[Ljava/lang/String;
    const-string v3, "transport_type"

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    const/4 v6, 0x0

    const-string v7, "mms"

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4159
    .local v19, "mmsSubQuery":Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x0

    const-string v14, "sms"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v9, v20

    move-object v11, v4

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 4162
    .local v21, "smsSubQuery":Ljava/lang/String;
    new-instance v23, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v23 .. v23}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4164
    .local v23, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4166
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v19, v3, v5

    const/4 v5, 0x1

    aput-object v21, v3, v5

    const/4 v5, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 4169
    .local v22, "unionQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3

    .line 4134
    .end local v2    # "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v4    # "columns":[Ljava/lang/String;
    .end local v15    # "finalSmsSelection":Ljava/lang/String;
    .end local v19    # "mmsSubQuery":Ljava/lang/String;
    .end local v20    # "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v21    # "smsSubQuery":Ljava/lang/String;
    .end local v22    # "unionQuery":Ljava/lang/String;
    .end local v23    # "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_0
    const-string v3, ", 0))"

    goto/16 :goto_0

    .line 4145
    .restart local v2    # "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v15    # "finalSmsSelection":Ljava/lang/String;
    .restart local v20    # "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_1
    const-string v3, ", 0))) "

    goto :goto_1
.end method

.method private getMessagesBySearchString(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 31
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 3896
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 3898
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    .line 3899
    .local v10, "cons":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/telephony/MmsSmsProvider;->EMAIL_FILTER_URI:Landroid/net/Uri;

    invoke-static {v10}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3900
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 3901
    .local v18, "mSearchString":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(address LIKE \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    const-string v4, "\'\'"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 3902
    .local v26, "threadWhereString":Ljava/lang/String;
    const-string v24, ""

    .line 3903
    .local v24, "stringWhere":Ljava/lang/String;
    const-string v23, ""

    .line 3907
    .local v23, "stringLimit":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 3908
    .local v14, "ftstring":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->FTS_TOKEN_SEPARATOR_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v10}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v7

    .local v7, "arr$":[Ljava/lang/String;
    array-length v0, v7

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_0
    move/from16 v0, v17

    if-ge v15, v0, :cond_1

    aget-object v27, v7, v15

    .line 3909
    .local v27, "token":Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3910
    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3908
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 3915
    .end local v27    # "token":Ljava/lang/String;
    :cond_1
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v3, 0x3

    if-ge v1, v3, :cond_2

    .line 3916
    const-string v23, " limit 20 "

    .line 3920
    :cond_2
    const/16 v21, 0x0

    .line 3922
    .local v21, "phoneCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/android/providers/telephony/MmsSmsProvider;->PROJECTION_PHONE:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "times_contacted DESC,sort_key,data2"

    move-object/from16 v0, v30

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 3924
    const-string v1, "TP/MmsSmsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "phoneCursor count ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3926
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    .line 3927
    :cond_3
    :goto_1
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3928
    const/4 v1, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCustomCliDigitAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3930
    .local v20, "number":Ljava/lang/String;
    if-eqz v20, :cond_3

    .line 3931
    const/16 v1, 0x28

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_4

    const/16 v1, 0x29

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_4

    const/16 v1, 0x2d

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_4

    const/16 v1, 0x20

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_5

    .line 3932
    :cond_4
    invoke-static/range {v20 .. v20}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3937
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " or PHONE_NUMBERS_EQUAL(address, \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseStrictPhoneNumberComparation:Z

    if-eqz v1, :cond_6

    const-string v1, "\', 1)"

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto :goto_1

    :cond_6
    const-string v1, "\', 0)"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 3944
    .end local v20    # "number":Ljava/lang/String;
    :cond_7
    if-eqz v21, :cond_8

    .line 3945
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 3948
    :cond_8
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 3950
    const/4 v8, 0x0

    .line 3952
    .local v8, "canonicalCursor":Landroid/database/Cursor;
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select _id, address from canonical_addresses WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " limit 500"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3953
    .local v9, "canonicalQuery":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v11, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3955
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_b

    .line 3956
    const-string v1, "TP/MmsSmsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "canonicalCursor count ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3958
    :goto_4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 3959
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "(\' \'||recipient_ids||\' \' like \'% "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " %\' and address="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") or "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v24

    goto :goto_4

    .line 3941
    .end local v8    # "canonicalCursor":Landroid/database/Cursor;
    .end local v9    # "canonicalQuery":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 3942
    .local v12, "ex":Ljava/lang/Exception;
    :try_start_2
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v12, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3944
    if-eqz v21, :cond_8

    .line 3945
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 3944
    .end local v12    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v21, :cond_9

    .line 3945
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v1

    .line 3963
    .restart local v8    # "canonicalCursor":Landroid/database/Cursor;
    .restart local v9    # "canonicalQuery":Ljava/lang/String;
    :cond_a
    const/4 v1, 0x0

    :try_start_3
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    move-object/from16 v0, v24

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v24

    .line 3971
    :cond_b
    if-eqz v8, :cond_c

    .line 3972
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3976
    .end local v9    # "canonicalQuery":Ljava/lang/String;
    :cond_c
    :goto_5
    const-string v25, ""

    .line 3977
    .local v25, "threadQuery":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 3978
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " UNION select date AS normalized_date, threads._id AS _id, \'thread\' AS transport_type, snippet as text, NULL AS sub, replace(address,\'-\',\'\') AS address, recipient_ids, threads._id AS thread_id, \'0\' AS listorder, \'\' AS type, snippet_cs AS sub_cs, display_recipient_ids FROM threads, canonical_addresses WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v24

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "GROUP BY threads._id "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 3985
    :goto_6
    const-string v19, "SELECT pdu.date * 1000 AS normalized_date, pdu._id AS _id, \'mms\' AS transport_type, text, sub, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, pdu.msg_box AS type, sub_cs, display_recipient_ids FROM pdu, part, threads WHERE (pdu._id=part.mid) AND (pdu.thread_id=threads._id) AND (ct=\'text/plain\') AND ((text LIKE ?) OR (sub LIKE ?)) "

    .line 3989
    .local v19, "mmsQuery":Ljava/lang/String;
    const-string v22, "SELECT sms.date * 1 AS normalized_date, sms._id AS _id, \'sms\' AS transport_type, body as text, NULL AS sub, address, recipient_ids, thread_id, \'1\' AS listorder, sms.type AS type, NULL AS sub_cs, display_recipient_ids FROM sms, threads WHERE (sms.thread_id=threads._id) AND (body LIKE ?) AND (hidden=0) "

    .line 3993
    .local v22, "smsQuery":Ljava/lang/String;
    const-string v29, "SELECT wpm.date * 1 AS normalized_date, wpm._id AS _id, \'wpm\' AS transport_type, body as text, NULL AS sub, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, wpm.type AS type, NULL AS sub_cs, display_recipient_ids FROM wpm, threads WHERE (wpm.thread_id=threads._id) AND (body LIKE ?) AND (hidden=0) "

    .line 3997
    .local v29, "wpmQuery":Ljava/lang/String;
    const-string v16, "SELECT im.date * 1 AS normalized_date, im._id AS _id, \'im\' AS transport_type, body as text, NULL AS sub, NULL AS address, recipient_ids, thread_id, \'1\' AS listorder, im.type AS type, NULL AS sub_cs, display_recipient_ids FROM im, im_threads WHERE (im.thread_id=im_threads.normal_thread_id) AND (im.message_type = 0 OR im.message_type = 5 OR im.message_type = 6) AND (body LIKE ?) AND (hidden=0) "

    .line 4003
    .local v16, "imQuery":Ljava/lang/String;
    const-string v13, "SELECT ft.date * 1 AS normalized_date, ft._id AS _id, \'ft\' AS transport_type, file_name as text, NULL AS sub, NULL AS address, NULL AS recipient_ids, thread_id, \'1\' AS listorder, ft.type AS type, NULL AS sub_cs, NULL AS display_recipient_ids FROM ft, im_threads WHERE (ft.thread_id = im_threads.normal_thread_id) AND (file_name LIKE ?) AND (hidden=0)"

    .line 4008
    .local v13, "ftQuery":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v25

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ORDER BY listorder, normalized_date desc"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 4011
    .local v28, "unionQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v18, v3, v4

    const/4 v4, 0x1

    aput-object v18, v3, v4

    const/4 v4, 0x2

    aput-object v18, v3, v4

    const/4 v4, 0x3

    aput-object v18, v3, v4

    const/4 v4, 0x4

    aput-object v18, v3, v4

    const/4 v4, 0x5

    aput-object v18, v3, v4

    move-object/from16 v0, v28

    invoke-virtual {v1, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    .line 3967
    .end local v13    # "ftQuery":Ljava/lang/String;
    .end local v16    # "imQuery":Ljava/lang/String;
    .end local v19    # "mmsQuery":Ljava/lang/String;
    .end local v22    # "smsQuery":Ljava/lang/String;
    .end local v25    # "threadQuery":Ljava/lang/String;
    .end local v28    # "unionQuery":Ljava/lang/String;
    .end local v29    # "wpmQuery":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 3968
    .restart local v12    # "ex":Ljava/lang/Exception;
    :try_start_4
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v12, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3971
    if-eqz v8, :cond_c

    .line 3972
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_5

    .line 3971
    .end local v12    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    if-eqz v8, :cond_d

    .line 3972
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v1

    .line 3982
    .restart local v25    # "threadQuery":Ljava/lang/String;
    :cond_e
    const-string v1, "TP/MmsSmsProvider"

    const-string v3, "No need to search threads table"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6
.end method

.method private getMiniConversations([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2665
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2666
    .local v0, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2668
    .local v2, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v3, "select threads._id, ( select date from        (select body, date from sms where type!=3 and thread_id=threads._id        union select sub, date*1000 from pdu where msg_box!=3 and thread_id=threads._id        union select body, date from wpm where type!=3 and thread_id=threads._id        order by date desc limit 1)) as date, threads.message_count, threads.recipient_ids, ( select body from        (select body, date from sms where type!=3 and thread_id=threads._id        union select sub, date*1000 from pdu where msg_box!=3 and thread_id=threads._id        union select body, date from wpm where type!=3 and thread_id=threads._id        order by date desc limit 1)) as snippet, ( select snippet_cs from        (select 0 as snippet_cs, date from sms where type!=3 and thread_id=threads._id        union select sub_cs, date*1000 from pdu where msg_box!=3 and thread_id=threads._id        union select 0 as snippet_cs, date from wpm where type!=3 and thread_id=threads._id order by date desc limit 1)) as snippet_c, threads.read, threads.error, threads.has_attachment, threads.unread_count from threads where message_count > 0 order by date desc    "

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2688
    .local v1, "rawQuery":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method private declared-synchronized getNormalThreadId(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 20
    .param p2, "createThread"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1938
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v9

    .line 1939
    .local v9, "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-string v17, ""

    .line 1941
    .local v17, "recipientIds":Ljava/lang/String;
    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1943
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    .line 1944
    .local v8, "addressId":Ljava/lang/Long;
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v17

    .line 1945
    goto :goto_0

    .line 1947
    .end local v8    # "addressId":Ljava/lang/Long;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/providers/telephony/MmsSmsProvider;->getSortedSet(Ljava/util/Set;)[J

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers([J)Ljava/lang/String;

    move-result-object v17

    .line 1950
    :cond_1
    const-string v2, "TP/MmsSmsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getThreadId: recipientIds (selectionArgs) ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v2, 0x0

    aput-object v17, v18, v2

    .line 1953
    .local v18, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 1956
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v14, 0x0

    .line 1957
    .local v14, "integCursor":Landroid/database/Cursor;
    sget-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v2, :cond_6

    .line 1958
    const-string v2, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v18

    invoke-virtual {v11, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v14

    .line 1962
    :goto_1
    const/4 v10, 0x0

    .line 1966
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v2, "true"

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v14, :cond_9

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_9

    .line 1968
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 1969
    .local v16, "newThreadId":Ljava/lang/Long;
    const/4 v12, 0x0

    .line 1971
    .local v12, "displayRecipientIds":Ljava/lang/String;
    const-string v2, "SELECT _id FROM threads UNION SELECT normal_thread_id as _id FROM im_threads ORDER BY _id DESC LIMIT 1"

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v15

    .line 1973
    .local v15, "newThreadCursor":Landroid/database/Cursor;
    if-eqz v15, :cond_7

    :try_start_2
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_7

    .line 1975
    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v16

    .line 1984
    :goto_2
    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1986
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v12

    .line 1987
    const-string v3, "normal"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThreadByType(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v10

    .line 1996
    .end local v12    # "displayRecipientIds":Ljava/lang/String;
    .end local v15    # "newThreadCursor":Landroid/database/Cursor;
    .end local v16    # "newThreadId":Ljava/lang/Long;
    :cond_2
    if-eqz v14, :cond_3

    .line 1997
    :try_start_4
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 2000
    :cond_3
    if-nez v10, :cond_4

    .line 2001
    sget-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v2, :cond_a

    .line 2002
    const-string v2, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v18

    invoke-virtual {v11, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v10

    :cond_4
    :goto_3
    move-object v2, v10

    .line 2007
    :cond_5
    :goto_4
    monitor-exit p0

    return-object v2

    .line 1960
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_6
    :try_start_5
    const-string v2, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    move-object/from16 v0, v18

    invoke-virtual {v11, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v14

    goto :goto_1

    .line 1977
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "displayRecipientIds":Ljava/lang/String;
    .restart local v15    # "newThreadCursor":Landroid/database/Cursor;
    .restart local v16    # "newThreadId":Ljava/lang/Long;
    :cond_7
    :try_start_6
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1979
    const-string v2, "_id"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 1981
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v16

    goto :goto_2

    .line 1984
    :catchall_0
    move-exception v2

    :try_start_7
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1996
    .end local v12    # "displayRecipientIds":Ljava/lang/String;
    .end local v15    # "newThreadCursor":Landroid/database/Cursor;
    .end local v16    # "newThreadId":Ljava/lang/Long;
    :catchall_1
    move-exception v2

    if-eqz v14, :cond_8

    .line 1997
    :try_start_8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1938
    .end local v9    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v14    # "integCursor":Landroid/database/Cursor;
    .end local v17    # "recipientIds":Ljava/lang/String;
    .end local v18    # "selectionArgs":[Ljava/lang/String;
    :catchall_2
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1988
    .restart local v9    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v14    # "integCursor":Landroid/database/Cursor;
    .restart local v17    # "recipientIds":Ljava/lang/String;
    .restart local v18    # "selectionArgs":[Ljava/lang/String;
    :cond_9
    :try_start_9
    const-string v2, "true"

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v14, :cond_2

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 1989
    const-string v2, "transport_type"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1990
    .local v19, "type":Ljava/lang/String;
    const-string v2, "_id"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1991
    .local v4, "threadId":Ljava/lang/String;
    const-string v2, "im"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1992
    const-string v3, "normal"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThreadByType(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v2

    .line 1996
    if-eqz v14, :cond_5

    .line 1997
    :try_start_a
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 2004
    .end local v4    # "threadId":Ljava/lang/String;
    .end local v19    # "type":Ljava/lang/String;
    :cond_a
    const-string v2, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    move-object/from16 v0, v18

    invoke-virtual {v11, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v10

    goto/16 :goto_3
.end method

.method private getOneBubbleSearch([Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 17
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "searchString"    # Ljava/lang/String;
    .param p3, "returnTF"    # Z

    .prologue
    .line 3469
    if-nez p3, :cond_0

    .line 3470
    const/4 v14, 0x0

    .line 3545
    :goto_0
    return-object v14

    .line 3473
    :cond_0
    const/4 v10, 0x0

    .line 3475
    .local v10, "sub":Ljava/lang/String;
    :try_start_0
    new-instance v11, Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    const-string v15, "iso-8859-1"

    invoke-direct {v11, v14, v15}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v10    # "sub":Ljava/lang/String;
    .local v11, "sub":Ljava/lang/String;
    move-object v10, v11

    .line 3480
    .end local v11    # "sub":Ljava/lang/String;
    .restart local v10    # "sub":Ljava/lang/String;
    :goto_1
    if-nez v10, :cond_1

    .line 3481
    move-object/from16 v10, p2

    .line 3485
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3486
    .local v2, "findString":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 3487
    .local v6, "mSearchString":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3488
    .local v3, "findSubString":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 3490
    .local v7, "mSearchSubString":Ljava/lang/String;
    const-string v9, "SELECT \'sms\' AS transport_type, _id, thread_id, address, body, date, date_sent, read, type, status, locked, error_code, NULL AS sub, NULL AS sub_cs, date, read, NULL AS m_type, NULL AS msg_box, NULL AS d_rpt, NULL AS rr, NULL AS err_type, locked, group_id, group_type, NULL AS href, NULL AS si_id, NULL AS created, NULL AS si_expires, NULL AS action, callback_number, reserved, reserved, pri, pri, teleservice_id, link_url, service_category, NULL AS text_only, svc_cmd, svc_cmd_content, spam_report, sim_slot, sim_imsi, NULL AS session_id, NULL AS transaction_id, NULL AS content_type, NULL AS media_id, NULL AS bytes_transf, NULL AS file_size, NULL AS cancel_reason, NULL AS file_name, NULL AS thumbnail_path, NULL AS file_path, NULL AS message_type, NULL AS display_notification_status, NULL AS displayed_counter, NULL AS reserved, NULL AS date_sent, NULL AS recipients, NULL AS imdn_message_id, NULL AS rcsdb_id, NULL AS user_alias, NULL AS delivered_timestamp, NULL AS remote_uri, NULL AS service_type, NULL AS reject_reason, NULL AS chat_session_id, NULL AS message_type , date * 1 AS normalized_date, 1 AS listorder FROM sms LEFT JOIN (SELECT sms_id, service_category FROM CMAS) as cmas ON sms._id = cmas.sms_id WHERE hidden=0 AND (body LIKE ?) "

    .line 3497
    .local v9, "smsQuery":Ljava/lang/String;
    const-string v8, ""

    .line 3498
    .local v8, "mmsQuery":Ljava/lang/String;
    sget-boolean v14, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v14, :cond_2

    .line 3499
    const-string v8, "SELECT \'mms\' AS transport_type, pdu._id, thread_id, NULL AS address, NULL AS body, date, date_sent, read, NULL AS type, NULL AS status, locked, NULL AS error_code, sub, sub_cs, date, read, m_type, msg_box, d_rpt, rr, err_type, locked, NULL AS group_id, NULL AS group_type, NULL AS href, NULL AS si_id, NULL AS created, NULL AS si_expires, NULL AS action, NULL AS callback_number, reserved, reserved, pri, pri, NULL AS teleservice_id, NULL AS link_url, NULL AS service_category, text_only, NULL AS svc_cmd, NULL AS svc_cmd_content, spam_report, sim_slot, sim_imsi, , date * 1000 AS normalized_date, 1 AS listorder NULL AS session_id, NULL AS transaction_id, NULL AS content_type, NULL AS media_id, NULL AS bytes_transf, NULL AS file_size, NULL AS cancel_reason, NULL AS file_name, NULL AS thumbnail_path, NULL AS file_path, NULL AS message_type, NULL AS display_notification_status, NULL AS displayed_counter, NULL AS reserved, NULL AS date_sent, NULL AS recipients, NULL AS imdn_message_id, NULL AS rcsdb_id, NULL AS user_alias, NULL AS delivered_timestamp, NULL AS remote_uri, NULL AS service_type, NULL AS reject_reason, NULL AS chat_session_id, NULL AS message_type FROM pdu LEFT JOIN pending_msgs ON pdu._id = pending_msgs.msg_id WHERE hidden=0 AND (m_type = 128 OR m_type = 132 OR m_type = 130) AND (select count(*) from part where pdu._id=part.mid and ct=\'text/plain\' AND ((text LIKE ?)))>0 "

    .line 3516
    :goto_2
    const-string v13, "SELECT \'wpm\' AS transport_type, _id, thread_id, NULL AS address, body, date, date_sent, read, type, NULL AS status, locked, NULL AS error_code, NULL AS sub, NULL AS sub_cs, date, read, NULL AS m_type, NULL AS msg_box, NULL AS d_rpt, NULL AS rr, NULL AS err_type, locked, NULL AS group_id, NULL AS group_type, href, si_id, created, si_expires, action, NULL AS callback_number, NULL AS reserved, NULL AS reserved, NULL AS pri, NULL AS pri, NULL AS teleservice_id, NULL AS link_url, NULL AS service_category, NULL AS text_only, NULL AS svc_cmd, NULL AS svc_cmd_content, 0 AS spam_report, sim_slot, sim_imsi, NULL AS session_id, NULL AS transaction_id, NULL AS content_type, NULL AS media_id, NULL AS bytes_transf, NULL AS file_size, NULL AS cancel_reason, NULL AS file_name, NULL AS thumbnail_path, NULL AS file_path, NULL AS message_type, NULL AS display_notification_status, NULL AS displayed_counter, NULL AS reserved, NULL AS date_sent, NULL AS recipients, NULL AS imdn_message_id, NULL AS rcsdb_id, NULL AS user_alias, NULL AS delivered_timestamp, NULL AS remote_uri, NULL AS service_type, NULL AS reject_reason, NULL AS chat_session_id, NULL AS message_type , date * 1 AS normalized_date, 1 AS listorder FROM wpm WHERE (hidden=0 AND body LIKE ?) "

    .line 3523
    .local v13, "wpmQuery":Ljava/lang/String;
    const-string v5, "SELECT \'im\' AS transport_type, _id, thread_id, address, body, date, date_sent, read, type, status, locked, NULL AS error_code, NULL AS sub, NULL AS sub_cs, date, read, NULL AS m_type, NULL AS msg_box, NULL AS d_rpt, NULL AS rr, NULL AS err_type, locked, NULL AS group_id, NULL AS group_type, NULL AS href, NULL AS si_id, NULL AS created, NULL AS si_expires, NULL AS action, NULL AS callback_number, reserved, reserved, NULL AS pri, NULL AS pri, NULL AS teleservice_id, NULL AS link_url, NULL AS service_category, NULL AS text_only, NULL AS svc_cmd, NULL AS svc_cmd_content, 0 AS spam_report, NULL AS sim_slot, sim_imsi, session_id, transaction_id, NULL AS content_type, NULL AS media_id, NULL AS bytes_transf, NULL AS file_size, NULL AS cancel_reason, NULL AS file_name, NULL AS thumbnail_path, NULL AS file_path, message_type, display_notification_status, displayed_counter, reserved, date_sent, recipients, imdn_message_id, rcsdb_id, NULL AS user_alias, delivered_timestamp, remote_uri, service_type, NULL AS reject_reason, NULL AS chat_session_id, NULL AS message_type ,date * 1 AS normalized_date,  1 AS listorder FROM im  WHERE (message_type = 0 OR message_type = 5 OR message_type = 6) AND (hidden=0) AND (body LIKE ?)"

    .line 3532
    .local v5, "imQuery":Ljava/lang/String;
    const-string v4, "SELECT \'ft\' AS transport_type, _id, thread_id, address, NULL AS body, date, date_sent, read, type, status, locked, NULL AS error_code, NULL AS sub, NULL AS sub_cs, date, read, NULL AS m_type, NULL AS msg_box, NULL AS d_rpt, NULL AS rr, NULL AS err_type, locked, NULL AS group_id, NULL AS group_type, NULL AS href, NULL AS si_id, NULL AS created, NULL AS si_expires, NULL AS action, NULL AS callback_number, reserved, reserved, NULL AS pri, NULL AS pri, NULL AS teleservice_id, NULL AS link_url, NULL AS service_category, NULL AS text_only, NULL AS svc_cmd, NULL AS svc_cmd_content, 0 AS spam_report, NULL AS sim_slot, sim_imsi, session_id, transaction_id, content_type, media_id, bytes_transf, file_size, cancel_reason, file_name, thumbnail_path, file_path, NULL AS message_type, NULL AS display_notification_status, displayed_counter, reserved, date_sent, recipients, NULL AS imdn_message_id, rcsdb_id, user_alias, delivered_timestamp, remote_uri, service_type, reject_reason, chat_session_id, message_type , date * 1 AS normalized_date, 1 AS listorder FROM ft  WHERE (hidden=0) AND (file_name LIKE ?)"

    .line 3541
    .local v4, "ftQuery":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " UNION "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " UNION "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " UNION "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " UNION "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ORDER BY normalized_date desc"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3542
    .local v12, "unionQuery":Ljava/lang/String;
    sget-boolean v14, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    if-eqz v14, :cond_3

    .line 3543
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    const/4 v15, 0x5

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    const/16 v16, 0x1

    aput-object v6, v15, v16

    const/16 v16, 0x2

    aput-object v6, v15, v16

    const/16 v16, 0x3

    aput-object v6, v15, v16

    const/16 v16, 0x4

    aput-object v6, v15, v16

    invoke-virtual {v14, v12, v15}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    goto/16 :goto_0

    .line 3476
    .end local v2    # "findString":Ljava/lang/String;
    .end local v3    # "findSubString":Ljava/lang/String;
    .end local v4    # "ftQuery":Ljava/lang/String;
    .end local v5    # "imQuery":Ljava/lang/String;
    .end local v6    # "mSearchString":Ljava/lang/String;
    .end local v7    # "mSearchSubString":Ljava/lang/String;
    .end local v8    # "mmsQuery":Ljava/lang/String;
    .end local v9    # "smsQuery":Ljava/lang/String;
    .end local v12    # "unionQuery":Ljava/lang/String;
    .end local v13    # "wpmQuery":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 3477
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const-string v14, "TP/MmsSmsProvider"

    const-string v15, "ISO_8859_1 must be supported!"

    invoke-static {v14, v15, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 3507
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "findString":Ljava/lang/String;
    .restart local v3    # "findSubString":Ljava/lang/String;
    .restart local v6    # "mSearchString":Ljava/lang/String;
    .restart local v7    # "mSearchSubString":Ljava/lang/String;
    .restart local v8    # "mmsQuery":Ljava/lang/String;
    .restart local v9    # "smsQuery":Ljava/lang/String;
    :cond_2
    const-string v8, "SELECT \'mms\' AS transport_type, pdu._id, thread_id, NULL AS address, NULL AS body, date, date_sent, read, NULL AS type, NULL AS status, locked, NULL AS error_code, sub, sub_cs, date, read, m_type, msg_box, d_rpt, rr, err_type, locked, NULL AS group_id, NULL AS group_type, NULL AS href, NULL AS si_id, NULL AS created, NULL AS si_expires, NULL AS action, NULL AS callback_number, reserved, reserved, pri, pri, NULL AS teleservice_id, NULL AS link_url, NULL AS service_category, text_only, NULL AS svc_cmd, NULL AS svc_cmd_content, spam_report, sim_slot, sim_imsi, NULL AS session_id, NULL AS transaction_id, NULL AS content_type, NULL AS media_id, NULL AS bytes_transf, NULL AS file_size, NULL AS cancel_reason, NULL AS file_name, NULL AS thumbnail_path, NULL AS file_path, NULL AS message_type, NULL AS display_notification_status, NULL AS displayed_counter, NULL AS reserved, NULL AS date_sent, NULL AS recipients, NULL AS imdn_message_id, NULL AS rcsdb_id, NULL AS user_alias, NULL AS delivered_timestamp, NULL AS remote_uri, NULL AS service_type, NULL AS reject_reason, NULL AS chat_session_id, NULL AS message_type , date * 1000 AS normalized_date, 1 AS listorder FROM pdu LEFT JOIN pending_msgs ON pdu._id = pending_msgs.msg_id WHERE hidden=0 AND (m_type = 128 OR m_type = 132 OR m_type = 130) AND (select count(*) from part where pdu._id=part.mid and ct=\'text/plain\' AND ((text LIKE ?) OR (sub LIKE ?)))>0 "

    goto :goto_2

    .line 3545
    .restart local v4    # "ftQuery":Ljava/lang/String;
    .restart local v5    # "imQuery":Ljava/lang/String;
    .restart local v12    # "unionQuery":Ljava/lang/String;
    .restart local v13    # "wpmQuery":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    const/4 v15, 0x6

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    const/16 v16, 0x1

    aput-object v6, v15, v16

    const/16 v16, 0x2

    aput-object v7, v15, v16

    const/16 v16, 0x3

    aput-object v6, v15, v16

    const/16 v16, 0x4

    aput-object v6, v15, v16

    const/16 v16, 0x5

    aput-object v6, v15, v16

    invoke-virtual {v14, v12, v15}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    goto/16 :goto_0
.end method

.method private declared-synchronized getRecipientsIds(Ljava/util/List;)[Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x1

    .line 1919
    monitor-enter p0

    :try_start_0
    const-string v4, "true"

    invoke-direct {p0, p1, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 1920
    .local v1, "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/String;

    .line 1923
    .local v3, "recipientIds":[Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 1925
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1926
    .local v0, "addressId":Ljava/lang/Long;
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1919
    .end local v0    # "addressId":Ljava/lang/Long;
    .end local v1    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "recipientIds":[Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 1929
    .restart local v1    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v3    # "recipientIds":[Ljava/lang/String;
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSortedSet(Ljava/util/Set;)[J

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers([J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1933
    :cond_1
    const/4 v4, 0x1

    invoke-direct {p0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1934
    monitor-exit p0

    return-object v3
.end method

.method private getSimpleConversations([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 2403
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2405
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "TP/MmsSmsProvider"

    const-string v2, "getSimpleConversations entered."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2407
    iget-boolean v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsEnableFingerPrintService:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsSecretMode:Z

    if-nez v1, :cond_0

    .line 2408
    if-nez p2, :cond_1

    .line 2409
    const-string p2, " secret_mode = 0"

    .line 2416
    :cond_0
    :goto_0
    const-string v1, "threads"

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    .line 2411
    :cond_1
    const-string v1, "secret_mode"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2412
    const-string v1, " and secret_mode = 0"

    invoke-virtual {p2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method private getSingleAddressId(Ljava/lang/String;Ljava/lang/String;)J
    .locals 24
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "createThread"    # Ljava/lang/String;

    .prologue
    .line 1518
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    move-result v13

    .line 1519
    .local v13, "isEmail":Z
    const/4 v14, 0x0

    .line 1521
    .local v14, "isPhoneNumber":Z
    sget-boolean v3, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v3, :cond_2

    .line 1522
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->isPhoneNumberEx(Ljava/lang/String;)Z

    move-result v14

    .line 1530
    :goto_0
    if-eqz v13, :cond_3

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    .line 1532
    .local v16, "refinedAddress":Ljava/lang/String;
    :goto_1
    const-string v5, "address=?"

    .line 1533
    .local v5, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1534
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-wide/16 v18, -0x1

    .line 1541
    .local v18, "retVal":J
    if-nez v14, :cond_4

    .line 1542
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v16, v6, v3

    .line 1575
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    :goto_2
    const/4 v11, 0x0

    .line 1578
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1579
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "canonical_addresses"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1583
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "true"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1584
    new-instance v10, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v10, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 1587
    .local v10, "contentValues":Landroid/content/ContentValues;
    if-eqz v14, :cond_0

    .line 1589
    invoke-static/range {v16 .. v16}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1593
    :cond_0
    const-string v3, "address"

    move-object/from16 v0, v16

    invoke-virtual {v10, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1595
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1596
    const-string v3, "canonical_addresses"

    const-string v4, "address"

    invoke-virtual {v2, v3, v4, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1600
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSingleAddressId: insert new canonical_address for xxx, _id="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1624
    if-eqz v11, :cond_1

    .line 1625
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_1
    move-wide/from16 v20, v18

    .line 1628
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v18    # "retVal":J
    .local v20, "retVal":J
    :goto_3
    return-wide v20

    .line 1524
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v16    # "refinedAddress":Ljava/lang/String;
    .end local v20    # "retVal":J
    :cond_2
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v14

    goto :goto_0

    :cond_3
    move-object/from16 v16, p1

    .line 1530
    goto :goto_1

    .line 1547
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    .restart local v16    # "refinedAddress":Ljava/lang/String;
    .restart local v18    # "retVal":J
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseStrictPhoneNumberComparation:Z

    move/from16 v23, v0

    .line 1548
    .local v23, "useStrictPhoneNumberComparation":Z
    invoke-static/range {v16 .. v16}, Lcom/android/providers/telephony/MmsSmsProvider;->containsAlpha(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1549
    const/16 v23, 0x1

    .line 1551
    :cond_5
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 1552
    .local v17, "salesCode":Ljava/lang/String;
    const-string v3, "CHN"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "CHU"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "CHM"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "CTC"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "CHC"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1553
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "PHONE_NUMBERS_EQUAL_N(address, ?, %d)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    sget v9, Lcom/android/providers/telephony/MmsSmsProvider;->MIN_MATCH_CHINA:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1566
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    if-eqz v3, :cond_b

    .line 1567
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCustomCliDigitAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1568
    .local v12, "cutomCliDigitAddress":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v12, v6, v3

    const/4 v3, 0x1

    aput-object v12, v6, v3

    .line 1569
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_2

    .line 1557
    .end local v12    # "cutomCliDigitAddress":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    if-eqz v3, :cond_9

    .line 1558
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR PHONE_NUMBERS_EQUAL(substr(address,max(1, length(address)-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")) , ?, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v23, :cond_8

    const/4 v3, 0x1

    :goto_5
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    .line 1561
    :cond_9
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR PHONE_NUMBERS_EQUAL(address, ?, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v23, :cond_a

    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :cond_a
    const/4 v3, 0x0

    goto :goto_6

    .line 1570
    :cond_b
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v16, v6, v3

    const/4 v3, 0x1

    aput-object v16, v6, v3

    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_2

    .line 1605
    .end local v17    # "salesCode":Ljava/lang/String;
    .end local v23    # "useStrictPhoneNumberComparation":Z
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :cond_c
    :try_start_1
    sget-boolean v3, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v3, :cond_11

    .line 1606
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1607
    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1609
    :cond_d
    const-string v3, "address"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1610
    .local v15, "number":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v22

    .line 1612
    .local v22, "sameNumber":Z
    if-eqz v22, :cond_10

    .line 1613
    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v18

    .line 1624
    .end local v15    # "number":Ljava/lang/String;
    .end local v22    # "sameNumber":Z
    :cond_e
    :goto_7
    if-eqz v11, :cond_f

    .line 1625
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_f
    move-wide/from16 v20, v18

    .line 1628
    .end local v18    # "retVal":J
    .restart local v20    # "retVal":J
    goto/16 :goto_3

    .line 1616
    .end local v20    # "retVal":J
    .restart local v15    # "number":Ljava/lang/String;
    .restart local v18    # "retVal":J
    .restart local v22    # "sameNumber":Z
    :cond_10
    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_d

    goto :goto_7

    .line 1619
    .end local v15    # "number":Ljava/lang/String;
    .end local v22    # "sameNumber":Z
    :cond_11
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1620
    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v18

    goto :goto_7

    .line 1624
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    if-eqz v11, :cond_12

    .line 1625
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_12
    throw v3
.end method

.method private getSingleAddressIdForVZW(Ljava/lang/String;Ljava/lang/String;)J
    .locals 24
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "createThread"    # Ljava/lang/String;

    .prologue
    .line 1383
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    move-result v14

    .line 1384
    .local v14, "isEmail":Z
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v15

    .line 1389
    .local v15, "isPhoneNumber":Z
    if-eqz v14, :cond_5

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    .line 1391
    .local v16, "refinedAddress":Ljava/lang/String;
    :goto_0
    const-string v5, "address=?"

    .line 1392
    .local v5, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1393
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-wide/16 v18, -0x1

    .line 1395
    .local v18, "retVal":J
    move-object/from16 v17, v16

    .line 1402
    .local v17, "refinedAddress4NA":Ljava/lang/String;
    if-nez v15, :cond_6

    .line 1403
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v16, v6, v3

    .line 1436
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    :goto_1
    const/4 v12, 0x0

    .line 1439
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1440
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "canonical_addresses"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1444
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_d

    .line 1445
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1446
    const-string v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1485
    :cond_0
    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "true"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_11

    const-wide/16 v8, -0x1

    cmp-long v3, v18, v8

    if-nez v3, :cond_11

    .line 1486
    :cond_2
    new-instance v11, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v11, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 1489
    .local v11, "contentValues":Landroid/content/ContentValues;
    if-eqz v15, :cond_3

    .line 1491
    invoke-static/range {v16 .. v16}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1495
    :cond_3
    const-string v3, "address"

    move-object/from16 v0, v16

    invoke-virtual {v11, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1498
    const-string v3, "canonical_addresses"

    const-string v4, "address"

    invoke-virtual {v2, v3, v4, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1502
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSingleAddressId: insert new canonical_address for xxx, _id="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1507
    if-eqz v12, :cond_4

    .line 1508
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide/from16 v20, v18

    .line 1511
    .end local v11    # "contentValues":Landroid/content/ContentValues;
    .end local v18    # "retVal":J
    .local v20, "retVal":J
    :goto_3
    return-wide v20

    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v16    # "refinedAddress":Ljava/lang/String;
    .end local v17    # "refinedAddress4NA":Ljava/lang/String;
    .end local v20    # "retVal":J
    :cond_5
    move-object/from16 v16, p1

    .line 1389
    goto/16 :goto_0

    .line 1408
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    .restart local v16    # "refinedAddress":Ljava/lang/String;
    .restart local v17    # "refinedAddress4NA":Ljava/lang/String;
    .restart local v18    # "retVal":J
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseStrictPhoneNumberComparation:Z

    move/from16 v23, v0

    .line 1409
    .local v23, "useStrictPhoneNumberComparation":Z
    invoke-static/range {v16 .. v16}, Lcom/android/providers/telephony/MmsSmsProvider;->containsAlpha(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1410
    const/16 v23, 0x1

    .line 1414
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    if-eqz v3, :cond_a

    .line 1415
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR PHONE_NUMBERS_EQUAL(substr(address,max(1, length(address)-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")) , ?, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v23, :cond_9

    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1422
    :goto_5
    const-string v3, "+011"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1423
    const/4 v3, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1427
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    if-eqz v3, :cond_c

    .line 1428
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCustomCliDigitAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1429
    .local v13, "cutomCliDigitAddress":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v13, v6, v3

    const/4 v3, 0x1

    aput-object v13, v6, v3

    .line 1430
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 1415
    .end local v13    # "cutomCliDigitAddress":Ljava/lang/String;
    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    .line 1418
    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR PHONE_NUMBERS_EQUAL(address, ?, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v23, :cond_b

    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_b
    const/4 v3, 0x0

    goto :goto_6

    .line 1431
    :cond_c
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v17, v6, v3

    const/4 v3, 0x1

    aput-object v17, v6, v3

    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 1448
    .end local v23    # "useStrictPhoneNumberComparation":Z
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :cond_d
    :try_start_1
    const-string v3, "+"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1449
    if-eqz v12, :cond_e

    .line 1450
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1453
    :cond_e
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1455
    if-nez v15, :cond_f

    .line 1456
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v3, 0x0

    aput-object v17, v22, v3

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .local v22, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v6, v22

    .line 1467
    .end local v22    # "selectionArgs":[Ljava/lang/String;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    :goto_7
    const-string v3, "canonical_addresses"

    sget-object v4, Lcom/android/providers/telephony/MmsSmsProvider;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1471
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 1472
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1474
    const-string v3, "address"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1475
    .local v10, "addressResult":Ljava/lang/String;
    const-string v3, "+011"

    invoke-virtual {v10, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1476
    const/4 v3, 0x4

    invoke-virtual {v10, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 1477
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1478
    const-string v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    goto/16 :goto_2

    .line 1459
    .end local v10    # "addressResult":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    if-eqz v3, :cond_10

    .line 1460
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCustomCliDigitAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1461
    .restart local v13    # "cutomCliDigitAddress":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v3, 0x0

    aput-object v13, v22, v3

    const/4 v3, 0x1

    aput-object v13, v22, v3

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .restart local v22    # "selectionArgs":[Ljava/lang/String;
    move-object/from16 v6, v22

    .line 1462
    .end local v22    # "selectionArgs":[Ljava/lang/String;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto :goto_7

    .line 1463
    .end local v13    # "cutomCliDigitAddress":Ljava/lang/String;
    :cond_10
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v3, 0x0

    aput-object v17, v22, v3

    const/4 v3, 0x1

    aput-object v17, v22, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .restart local v22    # "selectionArgs":[Ljava/lang/String;
    move-object/from16 v6, v22

    .end local v22    # "selectionArgs":[Ljava/lang/String;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto :goto_7

    .line 1507
    :cond_11
    if-eqz v12, :cond_12

    .line 1508
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_12
    move-wide/from16 v20, v18

    .line 1511
    .end local v18    # "retVal":J
    .restart local v20    # "retVal":J
    goto/16 :goto_3

    .line 1507
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v20    # "retVal":J
    .restart local v18    # "retVal":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_13

    .line 1508
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_13
    throw v3
.end method

.method private getSortedSet(Ljava/util/Set;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 1661
    .local p1, "numbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v5

    .line 1662
    .local v5, "size":I
    new-array v4, v5, [J

    .line 1663
    .local v4, "result":[J
    const/4 v0, 0x0

    .line 1665
    .local v0, "i":I
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 1666
    .local v3, "number":Ljava/lang/Long;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v0

    move v0, v1

    .line 1667
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 1669
    .end local v3    # "number":Ljava/lang/Long;
    :cond_0
    const/4 v6, 0x1

    if-le v5, v6, :cond_1

    .line 1670
    invoke-static {v4}, Ljava/util/Arrays;->sort([J)V

    .line 1673
    :cond_1
    return-object v4
.end method

.method private getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1694
    .local p1, "numbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1695
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 1697
    .local v1, "i":I
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 1698
    .local v3, "number":Ljava/lang/Long;
    if-eqz v1, :cond_0

    .line 1699
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1701
    :cond_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1702
    add-int/lit8 v1, v1, 0x1

    .line 1703
    goto :goto_0

    .line 1705
    .end local v3    # "number":Ljava/lang/Long;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getSpaceSeparatedNumbers([J)Ljava/lang/String;
    .locals 6
    .param p1, "numbers"    # [J

    .prologue
    .line 1681
    array-length v2, p1

    .line 1682
    .local v2, "size":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1684
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1685
    if-eqz v1, :cond_0

    .line 1686
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1688
    :cond_0
    aget-wide v4, p1, v1

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1684
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1690
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getSpamMessages([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 26
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 3266
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3267
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v22, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v22 .. v22}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3269
    .local v22, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3270
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3274
    const-string v2, "spam_pdu"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3275
    const-string v2, "spam_sms"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3277
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 3278
    .local v21, "smsColumns":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 3279
    .local v17, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 3280
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v16

    .line 3282
    .local v16, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3286
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "msg_box != 3"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 3289
    .local v18, "mmsSelection":Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const-string v7, "(msg_box != 3 AND (m_type = 128 OR m_type = 132 OR m_type = 130))"

    move-object/from16 v0, v18

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 3296
    .local v19, "mmsSubQuery":Ljava/lang/String;
    new-instance v8, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v8, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 3297
    .local v8, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3298
    const-string v2, "group_type"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3301
    const-string v6, "transport_type"

    const/4 v9, 0x0

    const-string v10, "sms"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, v22

    move-object/from16 v7, v16

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 3306
    .local v23, "smsSubQuery":Ljava/lang/String;
    new-instance v25, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v25 .. v25}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3307
    .local v25, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3309
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v23, v2, v5

    const/4 v5, 0x1

    aput-object v19, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 3313
    .local v24, "unionQuery":Ljava/lang/String;
    new-instance v9, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v9}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3314
    .local v9, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3315
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    move-object/from16 v10, v21

    move-object/from16 v14, p3

    invoke-virtual/range {v9 .. v15}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3318
    .local v20, "query":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private declared-synchronized getThreadId(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 31
    .param p1, "type"    # Ljava/lang/String;
    .param p3, "sessionId"    # Ljava/lang/String;
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "createThread"    # Ljava/lang/String;
    .param p6, "forceCreate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 2013
    .local p2, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v21

    .line 2014
    .local v21, "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-string v5, ""

    .line 2015
    .local v5, "recipientIds":Ljava/lang/String;
    const/16 v22, 0x0

    .line 2016
    .local v22, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v24

    .line 2020
    .local v24, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v28, 0x0

    .line 2022
    .local v28, "isSessionIdNull":Z
    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v29

    .line 2024
    .local v29, "newThreadId":Ljava/lang/Long;
    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->size()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_1

    .line 2026
    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Long;

    .line 2027
    .local v20, "addressId":Ljava/lang/Long;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 2028
    goto :goto_0

    .line 2030
    .end local v20    # "addressId":Ljava/lang/Long;
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCmasAlertType(Ljava/util/List;)I

    move-result v10

    .line 2038
    .end local v26    # "i$":Ljava/util/Iterator;
    .local v10, "alerttype":I
    :goto_1
    const-string v4, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThreadId: recipientIds (selectionArgs) ="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2039
    const-string v4, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThreadId sessionId ="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2042
    if-eqz p6, :cond_3

    const-string v4, "true"

    move-object/from16 v0, p6

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2044
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v11

    .line 2046
    .local v11, "displayRecipientIds":Ljava/lang/String;
    const-string v4, "SELECT _id FROM threads UNION SELECT normal_thread_id as _id FROM im_threads ORDER BY _id DESC LIMIT 1"

    const/4 v6, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v22

    .line 2048
    if-eqz v22, :cond_2

    :try_start_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    .line 2050
    const-wide/16 v8, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v29

    .line 2059
    :goto_2
    :try_start_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 2062
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v4, p0

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v4 .. v11}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/lang/Long;

    move-result-object v27

    .line 2064
    .local v27, "insertedThreadId":Ljava/lang/Long;
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/4 v4, 0x0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v30, v4

    .line 2067
    .local v30, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v24

    .line 2068
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads) WHERE _id = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 2070
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v12, -0x1

    invoke-virtual {v4, v6, v8, v9, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v23, v22

    .line 2191
    .end local v11    # "displayRecipientIds":Ljava/lang/String;
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v27    # "insertedThreadId":Ljava/lang/Long;
    .local v23, "cursor":Landroid/database/Cursor;
    :goto_3
    monitor-exit p0

    return-object v23

    .line 2033
    .end local v10    # "alerttype":I
    .end local v23    # "cursor":Landroid/database/Cursor;
    .end local v30    # "selectionArgs":[Ljava/lang/String;
    .restart local v22    # "cursor":Landroid/database/Cursor;
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSortedSet(Ljava/util/Set;)[J

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers([J)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v5

    .line 2035
    const/4 v10, 0x3

    .restart local v10    # "alerttype":I
    goto/16 :goto_1

    .line 2052
    .restart local v11    # "displayRecipientIds":Ljava/lang/String;
    :cond_2
    :try_start_4
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2054
    const-string v4, "_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v29

    .line 2056
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v12, 0x1

    add-long/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v29

    goto :goto_2

    .line 2059
    :catchall_0
    move-exception v4

    :try_start_5
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2013
    .end local v5    # "recipientIds":Ljava/lang/String;
    .end local v10    # "alerttype":I
    .end local v11    # "displayRecipientIds":Ljava/lang/String;
    .end local v21    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v24    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v28    # "isSessionIdNull":Z
    .end local v29    # "newThreadId":Ljava/lang/Long;
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 2076
    .restart local v5    # "recipientIds":Ljava/lang/String;
    .restart local v10    # "alerttype":I
    .restart local v21    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v22    # "cursor":Landroid/database/Cursor;
    .restart local v24    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v28    # "isSessionIdNull":Z
    .restart local v29    # "newThreadId":Ljava/lang/Long;
    :cond_3
    :try_start_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->size()I

    move-result v4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    if-nez p3, :cond_a

    .line 2077
    :cond_4
    const-string v4, "TP/MmsSmsProvider"

    const-string v6, "getThreadId addressIds size is 1 or sessionId is null. xMS or 1-1 chat or group chat with 1 participant"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2080
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/4 v4, 0x0

    aput-object v5, v30, v4

    .line 2081
    .restart local v30    # "selectionArgs":[Ljava/lang/String;
    sget-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v4, :cond_8

    .line 2082
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 2087
    :goto_4
    if-nez p3, :cond_9

    .line 2088
    const/16 v28, 0x1

    .line 2112
    :cond_5
    :goto_5
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_10

    const-string v4, "true"

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2113
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 2114
    const-string v4, "TP/MmsSmsProvider"

    const-string v6, "getThreadId: create new thread_id for recipients xxx"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2117
    const-string v4, "SELECT _id FROM threads UNION SELECT normal_thread_id as _id FROM im_threads ORDER BY _id DESC LIMIT 1"

    const/4 v6, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v22

    .line 2119
    if-eqz v22, :cond_c

    :try_start_7
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_c

    .line 2121
    const-wide/16 v8, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v29

    .line 2130
    :goto_6
    :try_start_8
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 2133
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v11

    .line 2134
    .restart local v11    # "displayRecipientIds":Ljava/lang/String;
    const-string v4, "normal"

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2135
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v6, p0

    move-object v8, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 2143
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v24

    .line 2145
    if-eqz v28, :cond_f

    .line 2147
    sget-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v4, :cond_e

    .line 2148
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 2155
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v12, -0x1

    invoke-virtual {v4, v6, v8, v9, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    .line 2187
    .end local v11    # "displayRecipientIds":Ljava/lang/String;
    :cond_6
    :goto_9
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/4 v6, 0x1

    if-le v4, v6, :cond_7

    .line 2188
    const-string v4, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThreadId: why is cursorCount="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move-object/from16 v23, v22

    .line 2191
    .end local v22    # "cursor":Landroid/database/Cursor;
    .restart local v23    # "cursor":Landroid/database/Cursor;
    goto/16 :goto_3

    .line 2084
    .end local v23    # "cursor":Landroid/database/Cursor;
    .restart local v22    # "cursor":Landroid/database/Cursor;
    :cond_8
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto/16 :goto_4

    .line 2090
    :cond_9
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    .end local v30    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object p3, v30, v4

    .restart local v30    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_5

    .line 2094
    .end local v30    # "selectionArgs":[Ljava/lang/String;
    :cond_a
    const-string v4, "TP/MmsSmsProvider"

    const-string v6, "getThreadId chat id is not null"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2095
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/4 v4, 0x0

    aput-object p3, v30, v4

    .line 2096
    .restart local v30    # "selectionArgs":[Ljava/lang/String;
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads) WHERE session_id = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 2098
    sget-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v4, :cond_5

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_5

    .line 2101
    if-eqz v22, :cond_b

    .line 2102
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 2104
    :cond_b
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    .end local v30    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object v5, v30, v4

    .line 2105
    .restart local v30    # "selectionArgs":[Ljava/lang/String;
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 2106
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    .end local v30    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object p3, v30, v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .restart local v30    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_5

    .line 2123
    :cond_c
    :try_start_9
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2125
    const-string v4, "_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v29

    .line 2127
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v12, 0x1

    add-long/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-result-object v29

    goto/16 :goto_6

    .line 2130
    :catchall_2
    move-exception v4

    :try_start_a
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    throw v4

    .line 2138
    .restart local v11    # "displayRecipientIds":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v4, p0

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v4 .. v11}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/lang/Long;

    goto/16 :goto_7

    .line 2150
    :cond_e
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto/16 :goto_8

    .line 2153
    :cond_f
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads) WHERE session_id = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto/16 :goto_8

    .line 2157
    .end local v11    # "displayRecipientIds":Ljava/lang/String;
    :cond_10
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_6

    const-string v4, "true"

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2158
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2159
    const-string v4, "transport_type"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 2160
    .local v25, "existingType":Ljava/lang/String;
    const-string v4, "_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2161
    .local v7, "threadId":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2162
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v11

    .line 2163
    .restart local v11    # "displayRecipientIds":Ljava/lang/String;
    const-string v4, "normal"

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 2164
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v6, p0

    move-object v8, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 2171
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v24

    .line 2173
    if-eqz v28, :cond_13

    .line 2175
    sget-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v4, :cond_12

    .line 2176
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 2183
    :goto_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v12, -0x1

    invoke-virtual {v4, v6, v8, v9, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    goto/16 :goto_9

    .line 2167
    :cond_11
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v17

    move-object/from16 v12, p0

    move-object v13, v5

    move-object v14, v7

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    move/from16 v18, v10

    move-object/from16 v19, v11

    invoke-direct/range {v12 .. v19}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/lang/Long;

    goto :goto_a

    .line 2178
    :cond_12
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto :goto_b

    .line 2181
    :cond_13
    const-string v4, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads) WHERE session_id = ?"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v22

    goto :goto_b
.end method

.method private declared-synchronized getThreadId(Ljava/util/List;)Landroid/database/Cursor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1863
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    const-string v0, "true"

    invoke-direct {p0, p1, v0}, Lcom/android/providers/telephony/MmsSmsProvider;->getThreadId(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getThreadId(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16
    .param p2, "createThread"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1867
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v3

    .line 1868
    .local v3, "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-string v9, ""

    .line 1875
    .local v9, "recipientIds":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    .line 1877
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1878
    .local v2, "addressId":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    .line 1879
    goto :goto_0

    .line 1881
    .end local v2    # "addressId":Ljava/lang/Long;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getSortedSet(Ljava/util/Set;)[J

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers([J)Ljava/lang/String;

    move-result-object v9

    .line 1884
    :cond_1
    const-string v11, "TP/MmsSmsProvider"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getThreadId: recipientIds (selectionArgs) ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1886
    const/4 v11, 0x1

    new-array v10, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v9, v10, v11

    .line 1887
    .local v10, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1888
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v11, "SELECT _id FROM threads WHERE recipient_ids=?"

    invoke-virtual {v6, v11, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 1890
    .local v5, "cursor":Landroid/database/Cursor;
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "true"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1891
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1892
    const-string v11, "TP/MmsSmsProvider"

    const-string v12, "getThreadId: create new thread_id for recipients xxx"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1895
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCmasAlertType(Ljava/util/List;)I

    move-result v4

    .line 1898
    .local v4, "alerttype":I
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v7

    .line 1899
    .local v7, "displayRecipientIds":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v11, v4, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;IILjava/lang/String;)V

    .line 1903
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1904
    const-string v11, "SELECT _id FROM threads WHERE recipient_ids=?"

    invoke-virtual {v6, v11, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 1909
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, -0x1

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    .line 1911
    .end local v4    # "alerttype":I
    .end local v7    # "displayRecipientIds":Ljava/lang/String;
    :cond_2
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_3

    .line 1912
    const-string v11, "TP/MmsSmsProvider"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getThreadId: why is cursorCount="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1914
    :cond_3
    monitor-exit p0

    return-object v5

    .line 1867
    .end local v3    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v5    # "cursor":Landroid/database/Cursor;
    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "recipientIds":Ljava/lang/String;
    .end local v10    # "selectionArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11
.end method

.method public static getThreadUnReadCount(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "threadId"    # J

    .prologue
    const/4 v9, 0x0

    .line 6552
    const/4 v8, 0x0

    .line 6555
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "threads"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "unread_count"

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 6557
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 6565
    :cond_0
    if-eqz v8, :cond_1

    .line 6566
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6567
    const/4 v8, 0x0

    :cond_1
    move v0, v9

    .line 6571
    :cond_2
    :goto_0
    return v0

    .line 6561
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 6565
    if-eqz v8, :cond_2

    .line 6566
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6567
    const/4 v8, 0x0

    goto :goto_0

    .line 6562
    :catch_0
    move-exception v0

    .line 6565
    if-eqz v8, :cond_4

    .line 6566
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6567
    const/4 v8, 0x0

    :cond_4
    move v0, v9

    .line 6571
    goto :goto_0

    .line 6565
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_5

    .line 6566
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6567
    const/4 v8, 0x0

    :cond_5
    throw v0
.end method

.method private getUndeliveredMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 32
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 4303
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 4305
    .local v24, "mmsProjection":[Ljava/lang/String;
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4306
    .local v2, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v28, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v28 .. v28}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4308
    .local v28, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPendingMsgTables()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4309
    const-string v3, "sms"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4311
    const-string v3, "msg_box = 4"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4313
    .local v8, "finalMmsSelection":Ljava/lang/String;
    const-string v3, "(type = 4 OR type = 5)"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 4320
    .local v15, "finalSmsSelection":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 4321
    .local v27, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 4322
    .local v23, "mmsColumns":[Ljava/lang/String;
    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 4324
    .local v4, "innerMmsProjection":[Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 4327
    .local v11, "innerSmsProjection":[Ljava/lang/String;
    new-instance v5, Ljava/util/HashSet;

    sget-object v3, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v5, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4328
    .local v5, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v3, "pdu._id"

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4329
    const-string v3, "err_type"

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4330
    const-string v3, "transport_type"

    const/4 v6, 0x1

    const-string v7, "mms"

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 4334
    .local v25, "mmsSubQuery":Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x1

    const-string v14, "sms"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v9, v28

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 4338
    .local v29, "smsSubQuery":Ljava/lang/String;
    new-instance v31, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v31 .. v31}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4340
    .local v31, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 4342
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v29, v3, v6

    const/4 v6, 0x1

    aput-object v25, v3, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v3, v6, v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 4345
    .local v30, "unionQuery":Ljava/lang/String;
    new-instance v16, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v16 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 4347
    .local v16, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "("

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 4349
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v22, 0x0

    move-object/from16 v17, v27

    move-object/from16 v21, p4

    invoke-virtual/range {v16 .. v22}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 4352
    .local v26, "outerQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v6, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v3, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method public static getUnreadCount(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 6527
    const/4 v9, 0x0

    .line 6528
    .local v9, "unreadCount":I
    const/4 v8, 0x0

    .line 6531
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND read == 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 6533
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 6541
    :cond_0
    if-eqz v8, :cond_1

    .line 6542
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6543
    const/4 v8, 0x0

    :cond_1
    move v0, v10

    .line 6547
    :goto_0
    return v0

    .line 6537
    :cond_2
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    .line 6541
    if-eqz v8, :cond_3

    .line 6542
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6543
    const/4 v8, 0x0

    :cond_3
    :goto_1
    move v0, v9

    .line 6547
    goto :goto_0

    .line 6538
    :catch_0
    move-exception v0

    .line 6541
    if-eqz v8, :cond_3

    .line 6542
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6543
    const/4 v8, 0x0

    goto :goto_1

    .line 6541
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 6542
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6543
    const/4 v8, 0x0

    :cond_4
    throw v0
.end method

.method private getUnreadMesagesWithFirstText([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 38
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .param p4, "isIntegratedQuery"    # Z

    .prologue
    .line 6363
    const-string v2, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getUnreadMesagesWithFirstText selection="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sortOrder="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isIntegratedQuery="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6364
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->createMmsProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v28

    .line 6366
    .local v28, "mmsProjection":[Ljava/lang/String;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6367
    .local v1, "mmsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v32, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v32 .. v32}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6369
    .local v32, "smsQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6370
    const/4 v2, 0x1

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6371
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/android/providers/telephony/MmsSmsProvider;->joinPduAndPartTables(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6373
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider;->joinSmsAndSerivceCategory()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6375
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 6376
    .local v31, "smsColumns":[Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 6377
    .local v27, "mmsColumns":[Ljava/lang/String;
    const/16 v2, 0x3e8

    move-object/from16 v0, v27

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 6378
    .local v3, "innerMmsProjection":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v26

    .line 6380
    .local v26, "innerSmsProjection":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 6381
    .local v4, "columnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "pdu._id"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6382
    const-string v2, "text"

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6385
    const-string v2, "msg_box != 3"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 6387
    .local v29, "mmsSelection":Ljava/lang/String;
    const-string v2, "transport_type"

    const/4 v5, 0x0

    const-string v6, "mms"

    const-string v7, "(msg_box != 3 AND (m_type = 128 OR m_type = 132 OR m_type = 130))"

    move-object/from16 v0, v29

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 6394
    .local v30, "mmsSubQuery":Ljava/lang/String;
    new-instance v8, Ljava/util/HashSet;

    sget-object v2, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    invoke-direct {v8, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 6395
    .local v8, "smsColumnsPresentInTable":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "group_id"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6396
    const-string v2, "group_type"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6398
    const-string v2, "service_category"

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6400
    const-string v6, "transport_type"

    const/4 v9, 0x0

    const-string v10, "sms"

    const-string v2, "(type != 3)"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, v32

    move-object/from16 v7, v26

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 6405
    .local v33, "smsSubQuery":Ljava/lang/String;
    invoke-direct/range {p0 .. p2}, Lcom/android/providers/telephony/MmsSmsProvider;->buildWPMSubQuery([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 6407
    .local v37, "wpmSubQuery":Ljava/lang/String;
    new-instance v36, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v36 .. v36}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6408
    .local v36, "unionQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6411
    if-eqz p4, :cond_0

    .line 6412
    new-instance v9, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v9}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6413
    .local v9, "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6414
    const-string v2, "im"

    invoke-virtual {v9, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6415
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 6416
    .local v24, "imColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v24

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 6417
    .local v11, "innerimProjection":[Ljava/lang/String;
    const-string v10, "transport_type"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    const/4 v13, 0x0

    const-string v14, "im"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v15, p2

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 6421
    .local v25, "imSubQuery":Ljava/lang/String;
    new-instance v12, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v12}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6422
    .local v12, "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 6423
    const-string v2, "ft"

    invoke-virtual {v12, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6424
    invoke-static/range {p1 .. p1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v22

    .line 6425
    .local v22, "ftColumns":[Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-static {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 6426
    .local v14, "innerftProjection":[Ljava/lang/String;
    const-string v13, "transport_type"

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    const/16 v16, 0x0

    const-string v17, "ft"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v18, p2

    invoke-virtual/range {v12 .. v20}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionSubQuery(Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 6430
    .local v23, "ftSubQuery":Ljava/lang/String;
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v33, v2, v5

    const/4 v5, 0x1

    aput-object v30, v2, v5

    const/4 v5, 0x2

    aput-object v37, v2, v5

    const/4 v5, 0x3

    aput-object v25, v2, v5

    const/4 v5, 0x4

    aput-object v23, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 6437
    .end local v9    # "imQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v11    # "innerimProjection":[Ljava/lang/String;
    .end local v12    # "ftQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v14    # "innerftProjection":[Ljava/lang/String;
    .end local v22    # "ftColumns":[Ljava/lang/String;
    .end local v23    # "ftSubQuery":Ljava/lang/String;
    .end local v24    # "imColumns":[Ljava/lang/String;
    .end local v25    # "imSubQuery":Ljava/lang/String;
    .local v35, "unionQuery":Ljava/lang/String;
    :goto_0
    new-instance v15, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v15}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 6438
    .local v15, "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 6440
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    move-object/from16 v16, v31

    move-object/from16 v20, p3

    invoke-virtual/range {v15 .. v21}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 6443
    .local v34, "totalQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v34

    invoke-virtual {v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 6433
    .end local v15    # "outerQueryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v34    # "totalQuery":Ljava/lang/String;
    .end local v35    # "unionQuery":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v33, v2, v5

    const/4 v5, 0x1

    aput-object v30, v2, v5

    const/4 v5, 0x2

    aput-object v37, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .restart local v35    # "unionQuery":Ljava/lang/String;
    goto :goto_0
.end method

.method private getWapPushMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 2532
    iget-object v1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2533
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "wpm"

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private static handleNullMessageProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    .line 2378
    if-nez p0, :cond_0

    sget-object p0, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_COLUMNS:[Ljava/lang/String;

    .end local p0    # "projection":[Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private static handleNullSortOrder(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 2395
    if-nez p0, :cond_0

    const-string p0, "normalized_date ASC"

    .end local p0    # "sortOrder":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private static handleNullThreadsProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    .line 2387
    if-nez p0, :cond_0

    sget-object p0, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COLUMNS:[Ljava/lang/String;

    .end local p0    # "projection":[Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 4
    .param p0, "affectedRows"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "threadIdString"    # Ljava/lang/String;

    .prologue
    .line 5266
    if-lez p0, :cond_1

    invoke-static {p2}, Lcom/android/providers/telephony/MmsSmsProvider;->haveRead(Landroid/content/ContentValues;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 5267
    .local v0, "needBroadCast":Z
    :goto_0
    const-string v1, "TP/MmsSmsProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "need read changed broadcast:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5268
    if-eqz v0, :cond_0

    .line 5269
    invoke-static {p1, p3}, Lcom/android/providers/telephony/MmsSmsProvider;->sendReadBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 5271
    :cond_0
    return-void

    .line 5266
    .end local v0    # "needBroadCast":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static handleReadChangedBroadcast(ILandroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "affectedRows"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadIdString"    # Ljava/lang/String;

    .prologue
    .line 5274
    if-lez p0, :cond_1

    const/4 v0, 0x1

    .line 5275
    .local v0, "needBroadCast":Z
    :goto_0
    const-string v1, "TP/MmsSmsProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "need read changed broadcast:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5276
    if-eqz v0, :cond_0

    .line 5277
    invoke-static {p1, p2}, Lcom/android/providers/telephony/MmsSmsProvider;->sendReadBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 5279
    :cond_0
    return-void

    .line 5274
    .end local v0    # "needBroadCast":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static haveRead(Landroid/content/ContentValues;)Z
    .locals 1
    .param p0, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 5238
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 5239
    :cond_0
    const/4 v0, 0x0

    .line 5241
    :goto_0
    return v0

    :cond_1
    const-string v0, "read"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private imMarkAsRead(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 24
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 6588
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 6589
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 6590
    .local v8, "affectedRows":I
    const-wide/16 v20, -0x1

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 6591
    .local v16, "threadId":Ljava/lang/Long;
    const/4 v12, -0x1

    .line 6592
    .local v12, "messageBoxType":I
    const/4 v9, 0x0

    .line 6593
    .local v9, "curr_read_status":Ljava/lang/String;
    const/4 v13, 0x0

    .line 6594
    .local v13, "new_read_status":Ljava/lang/String;
    const-string v19, "transaction_Id"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 6595
    .local v17, "transId":Ljava/lang/String;
    const-string v19, "receipient"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 6596
    .local v14, "receipient":Ljava/lang/String;
    const-string v19, "sent_time"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 6599
    .local v15, "sent_time":Ljava/lang/String;
    const-string v4, "select thread_id, type from im where transaction_id = ? UNION select thread_id, type from ft where transaction_id = ?"

    .line 6600
    .local v4, "IMFT_QUERY":Ljava/lang/String;
    const-string v5, "select recipient_ids, read_status from im_threads where normal_thread_id= ?"

    .line 6601
    .local v5, "THREAD_ID_QUERY":Ljava/lang/String;
    const-string v7, "update im SET displayed_counter = displayed_counter +1, status=3 where type=2 and "

    .line 6602
    .local v7, "UPDATE_IM_DISPLAY_COUNT":Ljava/lang/String;
    const-string v6, "update ft SET displayed_counter = displayed_counter +1, status=3 where type=2 and "

    .line 6604
    .local v6, "UPDATE_FT_DISPLAY_COUNT":Ljava/lang/String;
    const-string v19, "TP/MmsSmsProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "imMarkAsRead, transId:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " receipient="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " sent_time="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6607
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v17, v19, v20

    const/16 v20, 0x1

    aput-object v17, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v11, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 6609
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v19

    if-lez v19, :cond_0

    .line 6610
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 6611
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 6612
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    .line 6615
    :cond_0
    if-eqz v10, :cond_1

    .line 6616
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 6619
    :cond_1
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v12, v0, :cond_3

    .line 6621
    const-string v19, "TP/MmsSmsProvider"

    const-string v20, "imMarkAsRead, inbox msg, do nothing."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6622
    const/16 v19, 0x0

    .line 6650
    :goto_0
    return v19

    .line 6615
    :catchall_0
    move-exception v19

    if-eqz v10, :cond_2

    .line 6616
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v19

    .line 6626
    :cond_3
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v11, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 6628
    if-eqz v10, :cond_4

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v19

    if-lez v19, :cond_4

    .line 6629
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 6630
    const-string v19, "read_status"

    move-object/from16 v0, v19

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 6631
    const-string v19, "TP/MmsSmsProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "imMarkAsRead, curr_read_status:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 6634
    :cond_4
    if-eqz v10, :cond_5

    .line 6635
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 6638
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v9, v14, v15}, Lcom/android/providers/telephony/MmsSmsProvider;->parseReadStatus(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;

    move-result-object v18

    .line 6640
    .local v18, "updated":Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;
    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->new_sent_time:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->old_sent_time:J

    move-wide/from16 v22, v0

    cmp-long v19, v20, v22

    if-lez v19, :cond_6

    .line 6642
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "date_sent>"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->old_sent_time:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " and date_sent<="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->new_sent_time:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " and thread_id="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6644
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "date_sent>"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->old_sent_time:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " and date_sent<="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->new_sent_time:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " and thread_id="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6646
    const/4 v8, 0x1

    .line 6648
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-static {v8, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    move/from16 v19, v8

    .line 6650
    goto/16 :goto_0

    .line 6634
    .end local v18    # "updated":Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;
    :catchall_1
    move-exception v19

    if-eqz v10, :cond_7

    .line 6635
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v19
.end method

.method private static initializeColumnSets()V
    .locals 16

    .prologue
    .line 5548
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    array-length v1, v14

    .line 5549
    .local v1, "commonColumnCount":I
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v9, v14

    .line 5550
    .local v9, "mmsOnlyColumnCount":I
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v10, v14

    .line 5551
    .local v10, "smsOnlyColumnCount":I
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 5553
    .local v11, "unionColumns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 5554
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5555
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5557
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5560
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5561
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5563
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_SMS_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5553
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 5565
    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v9, :cond_1

    .line 5566
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5567
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->MMS_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5565
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 5569
    :cond_1
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v10, :cond_2

    .line 5570
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5571
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->SMS_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5569
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 5575
    :cond_2
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v13, v14

    .line 5576
    .local v13, "wpmColumnCount":I
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v13, :cond_3

    .line 5577
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5578
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->WPM_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5576
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 5582
    :cond_3
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v6, v14

    .line 5583
    .local v6, "imColumnCount":I
    const/4 v3, 0x0

    :goto_4
    if-ge v3, v6, :cond_4

    .line 5584
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->IM_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5585
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5583
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 5588
    :cond_4
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->FT_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v2, v14

    .line 5589
    .local v2, "ftColumnCount":I
    const/4 v3, 0x0

    :goto_5
    if-ge v3, v2, :cond_5

    .line 5590
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->FT_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->FT_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5591
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->FT_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5589
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 5594
    :cond_5
    const/4 v3, 0x0

    .line 5595
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5596
    .local v0, "columnName":Ljava/lang/String;
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_COLUMNS:[Ljava/lang/String;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput-object v0, v14, v3

    move v3, v4

    .line 5597
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_6

    .line 5601
    .end local v0    # "columnName":Ljava/lang/String;
    :cond_6
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COMMON_COLUMNS:[Ljava/lang/String;

    array-length v1, v14

    .line 5602
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_ONLY_COLUMNS:[Ljava/lang/String;

    array-length v7, v14

    .line 5603
    .local v7, "imThreadColumnCount":I
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 5604
    .local v12, "unionThreadsColumns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_7
    if-ge v8, v1, :cond_7

    .line 5605
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COMMON_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v8

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5606
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->NORMAL_THREADS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COMMON_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v8

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5607
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->THREADS_COMMON_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v8

    invoke-interface {v12, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5604
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 5610
    :cond_7
    const/4 v8, 0x0

    :goto_8
    if-ge v8, v7, :cond_8

    .line 5611
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_COLUMNS:Ljava/util/Set;

    sget-object v15, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v15, v15, v8

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5612
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->IM_THREADS_ONLY_COLUMNS:[Ljava/lang/String;

    aget-object v14, v14, v8

    invoke-interface {v12, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5610
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 5615
    :cond_8
    const/4 v3, 0x0

    .line 5616
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5617
    .restart local v0    # "columnName":Ljava/lang/String;
    sget-object v14, Lcom/android/providers/telephony/MmsSmsProvider;->UNION_THREADS_COLUMNS:[Ljava/lang/String;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .restart local v4    # "i":I
    aput-object v0, v14, v3

    move v3, v4

    .line 5618
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_9

    .line 5620
    .end local v0    # "columnName":Ljava/lang/String;
    :cond_9
    return-void
.end method

.method private insertInner(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v1, 0x0

    .line 5133
    iget-object v7, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 5135
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 5188
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    const-string v8, "MmsSmsProvider does not support deletes, inserts, or updates for this URI."

    invoke-direct {v7, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 5137
    :sswitch_0
    const-string v7, "pending_msgs"

    invoke-virtual {v0, v7, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 5138
    .local v4, "rowId":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 5185
    .end local v4    # "rowId":J
    :cond_0
    :goto_0
    return-object v1

    .line 5143
    :sswitch_1
    invoke-virtual {p0, p2}, Lcom/android/providers/telephony/MmsSmsProvider;->insertWapPushMessage(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 5148
    :sswitch_2
    const-string v7, "source_directory_path"

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 5150
    .local v6, "sourceDirectoryPath":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 5153
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "mmssms.db"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/data/data/com.android.providers.telephony/databases/mmssms.db"

    invoke-static {v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->backupDatabase(Ljava/lang/String;Ljava/lang/String;)I

    .line 5155
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "app_parts/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/data/data/com.android.providers.telephony/app_parts/"

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/android/providers/telephony/MmsSmsProvider;->directoryCopy(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 5158
    invoke-direct {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->dbReload()V

    goto :goto_0

    .line 5164
    .end local v6    # "sourceDirectoryPath":Ljava/lang/String;
    :sswitch_3
    const-string v7, "spam_filter"

    invoke-virtual {v0, v7, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 5166
    .local v2, "rowID":J
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-lez v7, :cond_1

    .line 5167
    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 5169
    .local v1, "insertedUri":Landroid/net/Uri;
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " succeeded"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5170
    invoke-virtual {p0, p1}, Lcom/android/providers/telephony/MmsSmsProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_0

    .line 5173
    .end local v1    # "insertedUri":Landroid/net/Uri;
    :cond_1
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert: failed! "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5179
    .end local v2    # "rowID":J
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->dataCreateByLDU()V

    goto/16 :goto_0

    .line 5184
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->dataCreateByUnpack()V

    goto/16 :goto_0

    .line 5135
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x20 -> :sswitch_2
        0xc8 -> :sswitch_1
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x320 -> :sswitch_5
    .end sparse-switch
.end method

.method private insertThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/lang/Long;
    .locals 12
    .param p1, "recipientIds"    # Ljava/lang/String;
    .param p2, "threadId"    # Ljava/lang/String;
    .param p3, "sessionId"    # Ljava/lang/String;
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "numberOfRecipients"    # I
    .param p6, "alerttype"    # I
    .param p7, "displayRecipientIds"    # Ljava/lang/String;

    .prologue
    .line 1780
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x7

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 1781
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertThread: recipientIds: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", threadId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", numberOfRecipients: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1785
    .local v2, "date":J
    const-string v7, "session_id"

    invoke-virtual {v6, v7, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1786
    const-string v7, "normal_thread_id"

    invoke-virtual {v6, v7, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1788
    const-string v7, "alias"

    move-object/from16 v0, p4

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1789
    :cond_0
    const-string v7, "date"

    const-wide/16 v8, 0x3e8

    rem-long v8, v2, v8

    sub-long v8, v2, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1790
    const-string v7, "recipient_ids"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1791
    const-string v7, "display_recipient_ids"

    move-object/from16 v0, p7

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1793
    const/4 v7, 0x3

    move/from16 v0, p6

    if-ne v0, v7, :cond_3

    const/4 v7, 0x1

    move/from16 v0, p5

    if-le v0, v7, :cond_3

    .line 1794
    const-string v7, "im_type"

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1799
    :cond_1
    :goto_0
    if-nez p6, :cond_4

    const/4 v7, 0x1

    move/from16 v0, p5

    if-le v0, v7, :cond_4

    .line 1800
    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1811
    :cond_2
    :goto_1
    const-string v7, "message_count"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1813
    iget-object v7, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1814
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v7, "im_threads"

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1815
    .local v5, "insertedThread":Ljava/lang/Long;
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_6

    .line 1816
    invoke-static {p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    .line 1818
    :goto_2
    return-object v7

    .line 1795
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "insertedThread":Ljava/lang/Long;
    :cond_3
    const/4 v7, 0x1

    move/from16 v0, p5

    if-ne v0, v7, :cond_1

    .line 1796
    const-string v7, "im_type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1801
    :cond_4
    const/16 v7, 0x64

    move/from16 v0, p6

    if-lt v0, v7, :cond_5

    const/16 v7, 0x68

    move/from16 v0, p6

    if-gt v0, v7, :cond_5

    .line 1802
    const-string v7, "type"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1803
    const-string v7, "alert_expired"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1805
    :cond_5
    const/16 v7, 0x6e

    move/from16 v0, p6

    if-ne v0, v7, :cond_2

    .line 1806
    const-string v7, "type"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1818
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v5    # "insertedThread":Ljava/lang/Long;
    :cond_6
    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_2
.end method

.method private insertThread(Ljava/lang/String;IILjava/lang/String;)V
    .locals 10
    .param p1, "recipientIds"    # Ljava/lang/String;
    .param p2, "numberOfRecipients"    # I
    .param p3, "alerttype"    # I
    .param p4, "displayRecipientIds"    # Ljava/lang/String;

    .prologue
    .line 1712
    new-instance v3, Landroid/content/ContentValues;

    const/4 v6, 0x6

    invoke-direct {v3, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 1713
    .local v3, "values":Landroid/content/ContentValues;
    const-string v6, "TP/MmsSmsProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "insertThread: recipientIds: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", displayRecipientIds="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", numberOfRecipients: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1717
    .local v0, "date":J
    const-string v6, "date"

    const-wide/16 v8, 0x3e8

    rem-long v8, v0, v8

    sub-long v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1718
    const-string v6, "recipient_ids"

    invoke-virtual {v3, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    const-string v6, "display_recipient_ids"

    invoke-virtual {v3, v6, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1721
    if-nez p3, :cond_1

    const/4 v6, 0x1

    if-le p2, v6, :cond_1

    .line 1722
    const-string v6, "type"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1732
    :cond_0
    :goto_0
    const-string v6, "message_count"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1734
    iget-object v6, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1735
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v6, "threads"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 1736
    .local v4, "threadId":J
    const-string v6, "TP/MmsSmsProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "insertThread: threadId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1741
    return-void

    .line 1723
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "threadId":J
    :cond_1
    const/16 v6, 0x64

    if-lt p3, v6, :cond_2

    const/16 v6, 0x68

    if-gt p3, v6, :cond_2

    .line 1724
    const-string v6, "type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1725
    const-string v6, "alert_expired"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1727
    :cond_2
    const/16 v6, 0x6e

    if-ne p3, v6, :cond_0

    .line 1728
    const-string v6, "type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private insertThread(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 10
    .param p1, "threadId"    # Ljava/lang/String;
    .param p2, "recipientIds"    # Ljava/lang/String;
    .param p3, "numberOfRecipients"    # I
    .param p4, "alerttype"    # I
    .param p5, "displayRecipientIds"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1745
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x6

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 1746
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertThread: recipientIds: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", displayRecipientIds="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", numberOfRecipients: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1750
    .local v0, "date":J
    const-string v5, "_id"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1751
    const-string v5, "date"

    const-wide/16 v6, 0x3e8

    rem-long v6, v0, v6

    sub-long v6, v0, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1752
    const-string v5, "recipient_ids"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1753
    const-string v5, "display_recipient_ids"

    invoke-virtual {v4, v5, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1756
    if-nez p4, :cond_1

    if-le p3, v9, :cond_1

    .line 1757
    const-string v5, "type"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1768
    :cond_0
    :goto_0
    const-string v5, "message_count"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1770
    iget-object v5, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1771
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "threads"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1772
    .local v3, "insertedthreadId":Ljava/lang/Long;
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertThread: threadId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    return-void

    .line 1758
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "insertedthreadId":Ljava/lang/Long;
    :cond_1
    const/16 v5, 0x64

    if-lt p4, v5, :cond_2

    const/16 v5, 0x68

    if-gt p4, v5, :cond_2

    .line 1759
    const-string v5, "type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1760
    const-string v5, "alert_expired"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1762
    :cond_2
    const/16 v5, 0x6e

    if-ne p4, v5, :cond_0

    .line 1763
    const-string v5, "type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private declared-synchronized insertThreadByType(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 23
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "threadId"    # Ljava/lang/String;
    .param p4, "sessionId"    # Ljava/lang/String;
    .param p5, "subject"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 2197
    .local p3, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    const-string v2, "false"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v17

    .line 2198
    .local v17, "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-string v4, ""

    .line 2199
    .local v4, "recipientIds":Ljava/lang/String;
    const/16 v18, 0x0

    .line 2200
    .local v18, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v19

    .line 2204
    .local v19, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v21, 0x0

    .line 2206
    .local v21, "isNormalMode":Z
    const-string v2, "normal"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2207
    const/16 v21, 0x1

    .line 2209
    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 2211
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Long;

    .line 2212
    .local v16, "addressId":Ljava/lang/Long;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 2213
    goto :goto_0

    .line 2215
    .end local v16    # "addressId":Ljava/lang/Long;
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getCmasAlertType(Ljava/util/List;)I

    move-result v6

    .line 2223
    .end local v20    # "i$":Ljava/util/Iterator;
    .local v6, "alerttype":I
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v7

    .line 2225
    .local v7, "displayRecipientIds":Ljava/lang/String;
    const-string v2, "TP/MmsSmsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertThreadByType: recipientIds (selectionArgs) ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2226
    const-string v2, "TP/MmsSmsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getThreadId sessionId ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2228
    if-eqz v21, :cond_4

    .line 2229
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v5

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v7}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 2237
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v19

    .line 2240
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v2, 0x0

    aput-object v4, v22, v2

    .line 2244
    .local v22, "selectionArgs":[Ljava/lang/String;
    sget-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v2, :cond_5

    .line 2245
    const-string v2, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 2250
    :goto_3
    if-eqz v18, :cond_2

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 2251
    const-string v2, "TP/MmsSmsProvider"

    const-string v3, "insertThreadByType: thread duplicating is not done"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2254
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, -0x1

    invoke-virtual {v2, v3, v5, v8, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2255
    monitor-exit p0

    return-object v18

    .line 2219
    .end local v6    # "alerttype":I
    .end local v7    # "displayRecipientIds":Ljava/lang/String;
    .end local v22    # "selectionArgs":[Ljava/lang/String;
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSortedSet(Ljava/util/Set;)[J

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers([J)Ljava/lang/String;

    move-result-object v4

    .line 2221
    const/4 v6, 0x3

    .restart local v6    # "alerttype":I
    goto/16 :goto_1

    .line 2231
    .restart local v7    # "displayRecipientIds":Ljava/lang/String;
    :cond_4
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v13

    move-object/from16 v8, p0

    move-object v9, v4

    move-object/from16 v10, p2

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move v14, v6

    move-object v15, v7

    invoke-direct/range {v8 .. v15}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2197
    .end local v4    # "recipientIds":Ljava/lang/String;
    .end local v6    # "alerttype":I
    .end local v7    # "displayRecipientIds":Ljava/lang/String;
    .end local v17    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v18    # "cursor":Landroid/database/Cursor;
    .end local v19    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v21    # "isNormalMode":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 2247
    .restart local v4    # "recipientIds":Ljava/lang/String;
    .restart local v6    # "alerttype":I
    .restart local v7    # "displayRecipientIds":Ljava/lang/String;
    .restart local v17    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v18    # "cursor":Landroid/database/Cursor;
    .restart local v19    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v21    # "isNormalMode":Z
    .restart local v22    # "selectionArgs":[Ljava/lang/String;
    :cond_5
    :try_start_2
    const-string v2, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v18

    goto :goto_3
.end method

.method public static isPhoneNumberEx(Ljava/lang/String;)Z
    .locals 2
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 6329
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6330
    const/4 v1, 0x0

    .line 6334
    :goto_0
    return v1

    .line 6333
    :cond_0
    sget-object v1, Lcom/android/providers/telephony/MmsSmsProvider;->PHONE_EX:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 6334
    .local v0, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0
.end method

.method private static joinPduAndPartTables(Z)Ljava/lang/String;
    .locals 3
    .param p0, "isOnlyTextPart"    # Z

    .prologue
    .line 6339
    const-string v0, "pdu LEFT JOIN part ON pdu._id = part.mid"

    .line 6342
    .local v0, "table":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 6343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND part.ct=\'text/plain\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6346
    :cond_0
    return-object v0
.end method

.method private static joinPduAndPendingMsgTables()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4279
    const-string v0, "pdu LEFT JOIN pending_msgs ON pdu._id = pending_msgs.msg_id"

    return-object v0
.end method

.method private static joinSmsAndSerivceCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4284
    const-string v0, "sms LEFT JOIN (SELECT sms_id, service_category FROM CMAS) as cmas ON sms._id = cmas.sms_id"

    return-object v0
.end method

.method private makeMessagesTime(I)J
    .locals 8
    .param p1, "i"    # I

    .prologue
    .line 6457
    const-string v3, "26.11.2013 20:52"

    .line 6458
    .local v3, "wantTime":Ljava/lang/String;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "dd.MM.yyyy HH:mm"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 6460
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    const/4 v0, 0x0

    .line 6462
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6468
    :goto_0
    if-nez v0, :cond_0

    .line 6469
    const-wide/16 v4, 0x0

    .line 6471
    :goto_1
    return-wide v4

    .line 6463
    :catch_0
    move-exception v1

    .line 6465
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 6471
    .end local v1    # "e":Ljava/text/ParseException;
    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    mul-int/lit16 v6, p1, 0x7530

    int-to-long v6, v6

    add-long/2addr v4, v6

    goto :goto_1
.end method

.method private makeProjectionWithDateAndThreadId([Ljava/lang/String;I)[Ljava/lang/String;
    .locals 6
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "dateMultiple"    # I

    .prologue
    .line 3196
    array-length v1, p1

    .line 3197
    .local v1, "projectionSize":I
    add-int/lit8 v3, v1, 0x2

    new-array v2, v3, [Ljava/lang/String;

    .line 3199
    .local v2, "result":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string v4, "thread_id AS tid"

    aput-object v4, v2, v3

    .line 3200
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "date * "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS normalized_date"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 3201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 3202
    add-int/lit8 v3, v0, 0x2

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 3201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3204
    :cond_0
    return-object v2
.end method

.method private static makeProjectionWithNormalizedDate([Ljava/lang/String;I)[Ljava/lang/String;
    .locals 5
    .param p0, "projection"    # [Ljava/lang/String;
    .param p1, "dateMultiple"    # I

    .prologue
    const/4 v4, 0x0

    .line 4448
    array-length v0, p0

    .line 4449
    .local v0, "projectionSize":I
    add-int/lit8 v2, v0, 0x1

    new-array v1, v2, [Ljava/lang/String;

    .line 4451
    .local v1, "result":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "date * "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS normalized_date"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 4452
    const/4 v2, 0x1

    invoke-static {p0, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4453
    return-object v1
.end method

.method private markAllImThreadtoDelete()I
    .locals 5

    .prologue
    .line 5529
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 5531
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, " normal_thread_id NOT IN (select thread_id from (SELECT _id, thread_id FROM im where thread_id NOT NULL AND locked=1 UNION SELECT _id, thread_id FROM ft where thread_id NOT NULL AND locked=1) GROUP BY thread_id)"

    .line 5536
    .local v1, "finalSelection":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 5537
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "im_type"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5538
    const-string v3, "im_threads"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private markImThreadtoDelete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 5497
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    .line 5498
    .local v6, "threadId":Ljava/lang/String;
    const-string v8, "TP/MmsSmsProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "markImThreadtoDelete threadId: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5499
    iget-object v8, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 5502
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 5503
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 5504
    .local v4, "lockMsgCnt":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SELECT _id, \"im\" as freemsg_type  FROM im where thread_id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and locked=1 "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "UNION SELECT _id, \"ft\" as freemsg_type FROM ft where thread_id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and locked=1"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5506
    .local v5, "query":Ljava/lang/String;
    invoke-virtual {v2, v5, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 5507
    if-eqz v1, :cond_0

    .line 5508
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 5509
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5511
    :cond_0
    if-lez v4, :cond_2

    .line 5512
    const/4 v0, 0x0

    .line 5525
    :cond_1
    :goto_0
    return v0

    .line 5514
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "normal_thread_id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5516
    .local v3, "finalSelection":Ljava/lang/String;
    const/4 v0, 0x0

    .line 5517
    .local v0, "affectedRows":I
    new-instance v7, Landroid/content/ContentValues;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 5518
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "true"

    const-string v9, "group"

    invoke-virtual {p1, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 5522
    const-string v8, "im_type"

    const/16 v9, 0x9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5523
    const-string v8, "im_threads"

    invoke-virtual {v2, v8, v7, v3, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private notifyChange()V
    .locals 5

    .prologue
    .line 5232
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    .line 5234
    return-void
.end method

.method private parseReadStatus(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;
    .locals 22
    .param p1, "threadId"    # Ljava/lang/Long;
    .param p2, "old_read_status"    # Ljava/lang/String;
    .param p3, "receipient"    # Ljava/lang/String;
    .param p4, "new_timeStamp"    # Ljava/lang/String;

    .prologue
    .line 6653
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 6654
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, ","

    .line 6655
    .local v2, "RECIPIENT_SEPARATOR":Ljava/lang/String;
    const-string v9, ":"

    .line 6656
    .local v9, "SENT_TIME_SEPARATOR":Ljava/lang/String;
    move-object/from16 v16, p2

    .line 6657
    .local v16, "new_read_status":Ljava/lang/String;
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 6658
    .local v17, "oldSentTime":Ljava/lang/Long;
    invoke-static/range {p4 .. p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 6660
    .local v15, "newSentTime":Ljava/lang/Long;
    if-eqz p2, :cond_5

    .line 6661
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 6662
    .local v18, "readStatusSet":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 6663
    .local v10, "builder":Ljava/lang/StringBuilder;
    const/4 v14, 0x0

    .line 6664
    .local v14, "matched":Z
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v13, v3, :cond_1

    .line 6665
    aget-object v20, v18, v13

    .line 6666
    .local v20, "str":Ljava/lang/String;
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "string split str="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6667
    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 6668
    .local v12, "extraStr":[Ljava/lang/String;
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "string split addr="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v12, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sentTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v12, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6669
    const/4 v3, 0x0

    aget-object v3, v12, v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6670
    const/4 v14, 0x1

    .line 6671
    const/4 v3, 0x1

    aget-object v3, v12, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 6672
    const/4 v3, 0x1

    aput-object p4, v12, v3

    .line 6674
    :cond_0
    const/4 v3, 0x0

    aget-object v3, v12, v3

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v4, v12, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6664
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 6676
    .end local v12    # "extraStr":[Ljava/lang/String;
    .end local v20    # "str":Ljava/lang/String;
    :cond_1
    if-nez v14, :cond_2

    .line 6677
    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6679
    :cond_2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 6685
    .end local v10    # "builder":Ljava/lang/StringBuilder;
    .end local v13    # "i":I
    .end local v14    # "matched":Z
    .end local v18    # "readStatusSet":[Ljava/lang/String;
    :goto_1
    if-eqz p2, :cond_3

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 6686
    :cond_3
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 6687
    .local v21, "val":Landroid/content/ContentValues;
    const-string v3, "read_status"

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6688
    const-string v3, "im_threads"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "normal_thread_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v11, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 6689
    .local v19, "rows":I
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseReadStatus, read_status is changed. update imThreads updated rows="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6692
    .end local v19    # "rows":I
    .end local v21    # "val":Landroid/content/ContentValues;
    :cond_4
    new-instance v3, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;-><init>(JJLjava/lang/String;)V

    return-object v3

    .line 6682
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto :goto_1
.end method

.method private declared-synchronized queryIntegratedThreads(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p2, "threadId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x1

    .line 2274
    monitor-enter p0

    :try_start_0
    const-string v7, "false"

    invoke-direct {p0, p1, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getAddressIds(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 2275
    .local v1, "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-string v5, ""

    .line 2276
    .local v5, "recipientIds":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2277
    .local v2, "cursor":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2282
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2283
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    .line 2284
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-string v7, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads) WHERE _id = ?"

    invoke-virtual {v3, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2304
    :goto_0
    monitor-exit p0

    return-object v2

    .line 2286
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v7

    if-ne v7, v8, :cond_1

    .line 2288
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2289
    .local v0, "addressId":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 2290
    goto :goto_1

    .line 2292
    .end local v0    # "addressId":Ljava/lang/Long;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getSortedSet(Ljava/util/Set;)[J

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpaceSeparatedNumbers([J)Ljava/lang/String;

    move-result-object v5

    .line 2295
    :cond_2
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    .line 2298
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v7, :cond_3

    .line 2299
    const-string v7, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type<>2) WHERE recipient_ids = ?"

    invoke-virtual {v3, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 2301
    :cond_3
    const-string v7, "SELECT _id, transport_type, recipient_ids, session_id      FROM (SELECT _id, \'normal\' as transport_type, recipient_ids, null as session_id FROM threads            UNION SELECT normal_thread_id as _id, \'im\' as transport_type, recipient_ids, session_id FROM im_threads where im_type not in(2,3)) WHERE recipient_ids = ?"

    invoke-virtual {v3, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 2274
    .end local v1    # "addressIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "recipientIds":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method private registerFreeMessageModeReceiver()V
    .locals 3

    .prologue
    .line 6696
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 6697
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 6698
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 6699
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mFreeMessageModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 6700
    return-void
.end method

.method private registerSecretModeReceiver()V
    .locals 3

    .prologue
    .line 6288
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 6289
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 6290
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 6291
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 6292
    return-void
.end method

.method private static replaceOrStay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "replacement"    # Ljava/lang/String;

    .prologue
    .line 5118
    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5119
    .end local p0    # "str":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static replaceTypeFieldNameForMms(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 5114
    const-string v0, "msgType"

    const-string v1, "msg_box"

    invoke-static {p0, v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceOrStay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static replaceTypeFieldNameForSms(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 5110
    const-string v0, "msgType"

    const-string v1, "type"

    invoke-static {p0, v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->replaceOrStay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private scheduleSecretModeChanged(Z)V
    .locals 5
    .param p1, "secretMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 6299
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x64

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v4, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 6300
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 6301
    return-void

    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    move v1, v2

    .line 6299
    goto :goto_0
.end method

.method public static sendReadBroadcast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "threadIdString"    # Ljava/lang/String;

    .prologue
    .line 5245
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.provider.Telephony.Threads.action.READ_CHANGED"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5247
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 5249
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 5250
    .local v2, "threadId":J
    const-string v4, "threadId"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 5252
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendReadThreadIDBroadcast Broadcasting intent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  Ex: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "threadId"

    const-wide/16 v8, -0x2

    invoke-virtual {v1, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5254
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5263
    .end local v2    # "threadId":J
    :goto_0
    return-void

    .line 5256
    :catch_0
    move-exception v0

    .line 5257
    .local v0, "exception":Ljava/lang/NumberFormatException;
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "Thread ID must be a Long."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5260
    .end local v0    # "exception":Ljava/lang/NumberFormatException;
    :cond_0
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "Broadcasting READ_CHANGED_ACTION"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5261
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private syncDBData(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 27
    .param p1, "_db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5743
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "syncDBData start"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5745
    const/16 v21, 0x0

    .line 5747
    .local v21, "smsCursor":Landroid/database/Cursor;
    const-wide/16 v22, 0x0

    .line 5748
    .local v22, "threadId":J
    :try_start_0
    const-string v19, ""

    .line 5749
    .local v19, "recipientIds":Ljava/lang/String;
    const/16 v25, 0x0

    .line 5751
    .local v25, "type":I
    const/4 v5, 0x1

    const-string v6, "sms"

    const/4 v4, 0x1

    new-array v7, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "thread_id"

    aput-object v8, v7, v4

    const-string v8, "thread_id NOT IN (SELECT _id FROM threads)  AND thread_id  < 9223372036854775806"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 5754
    :goto_0
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 5756
    const-string v4, "thread_id"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v22

    .line 5759
    const/16 v18, 0x0

    .line 5761
    .local v18, "recipientIdCursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "canonical_addresses"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v4

    const-string v7, "address IN (SELECT address FROM sms WHERE thread_id=?)"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 5765
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    .line 5766
    .local v20, "recipientIdsBuf":Ljava/lang/StringBuffer;
    :goto_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 5767
    const-string v4, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5770
    .end local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v14

    .line 5771
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_2
    throw v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5773
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v18, :cond_0

    .line 5774
    :try_start_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 5791
    .end local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .end local v19    # "recipientIds":Ljava/lang/String;
    .end local v25    # "type":I
    :catch_1
    move-exception v14

    .line 5792
    .restart local v14    # "ex":Ljava/lang/Exception;
    :try_start_4
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v14, v4}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 5793
    const/4 v4, 0x0

    .line 5795
    if-eqz v21, :cond_1

    .line 5796
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 5917
    .end local v14    # "ex":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return v4

    .line 5769
    .restart local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .restart local v19    # "recipientIds":Ljava/lang/String;
    .restart local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    .restart local v25    # "type":I
    :cond_2
    :try_start_5
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v19

    .line 5773
    if-eqz v18, :cond_3

    .line 5774
    :try_start_6
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 5779
    :cond_3
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, " "

    move-object/from16 v0, v19

    invoke-direct {v4, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_4

    .line 5780
    const/16 v25, 0x1

    .line 5783
    :cond_4
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 5784
    .local v24, "threadsInfo":Landroid/content/ContentValues;
    const-string v4, "_id"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5785
    const-string v4, "recipient_ids"

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5786
    const-string v4, "type"

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5788
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SMS syncThread. threadId= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", recipientsIds= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", type= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5789
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->syncThread(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 5795
    .end local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .end local v19    # "recipientIds":Ljava/lang/String;
    .end local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    .end local v24    # "threadsInfo":Landroid/content/ContentValues;
    .end local v25    # "type":I
    :catchall_1
    move-exception v4

    if-eqz v21, :cond_5

    .line 5796
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v4

    .line 5795
    .restart local v19    # "recipientIds":Ljava/lang/String;
    .restart local v25    # "type":I
    :cond_6
    if-eqz v21, :cond_7

    .line 5796
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 5798
    :cond_7
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "syncDBData sms end"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5801
    const/4 v15, 0x0

    .line 5803
    .local v15, "mmsCursor":Landroid/database/Cursor;
    const-wide/16 v22, 0x0

    .line 5804
    const-wide/16 v16, 0x0

    .line 5805
    .local v16, "msgId":J
    :try_start_7
    const-string v19, ""

    .line 5806
    const/16 v25, 0x0

    .line 5809
    const/4 v5, 0x1

    const-string v6, "pdu"

    const/4 v4, 0x2

    new-array v7, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "_id"

    aput-object v8, v7, v4

    const/4 v4, 0x1

    const-string v8, "thread_id"

    aput-object v8, v7, v4

    const-string v8, "thread_id NOT IN (SELECT _id FROM threads)  AND thread_id <> 0 AND thread_id  < 9223372036854775806"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 5813
    :goto_3
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 5815
    const-string v4, "thread_id"

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 5818
    const-string v4, "_id"

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-wide v16

    .line 5821
    const/16 v18, 0x0

    .line 5823
    .restart local v18    # "recipientIdCursor":Landroid/database/Cursor;
    :try_start_8
    const-string v5, "canonical_addresses"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v4

    const-string v7, "address IN (SELECT address FROM addr WHERE msg_id=? AND type= CASE (SELECT msg_box FROM pdu WHERE _id=?) WHEN 1 THEN 137 ELSE 151 END)"

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 5828
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    .line 5829
    .restart local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :goto_4
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 5830
    const-string v4, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_4

    .line 5833
    .end local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :catch_2
    move-exception v14

    .line 5834
    .restart local v14    # "ex":Ljava/lang/Exception;
    :try_start_9
    throw v14
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 5836
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_2
    move-exception v4

    if-eqz v18, :cond_8

    .line 5837
    :try_start_a
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v4
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 5854
    .end local v18    # "recipientIdCursor":Landroid/database/Cursor;
    :catch_3
    move-exception v14

    .line 5855
    .restart local v14    # "ex":Ljava/lang/Exception;
    :try_start_b
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v14, v4}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 5856
    const/4 v4, 0x0

    .line 5858
    if-eqz v15, :cond_1

    .line 5859
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 5832
    .end local v14    # "ex":Ljava/lang/Exception;
    .restart local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .restart local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :cond_9
    :try_start_c
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-result-object v19

    .line 5836
    if-eqz v18, :cond_a

    .line 5837
    :try_start_d
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 5842
    :cond_a
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, " "

    move-object/from16 v0, v19

    invoke-direct {v4, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_b

    .line 5843
    const/16 v25, 0x1

    .line 5846
    :cond_b
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 5847
    .restart local v24    # "threadsInfo":Landroid/content/ContentValues;
    const-string v4, "_id"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5848
    const-string v4, "recipient_ids"

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5849
    const-string v4, "type"

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5851
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MMS syncThread. threadId= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", msgId= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", recipientsIds= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", type= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5852
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->syncThread(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto/16 :goto_3

    .line 5858
    .end local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .end local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    .end local v24    # "threadsInfo":Landroid/content/ContentValues;
    :catchall_3
    move-exception v4

    if-eqz v15, :cond_c

    .line 5859
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v4

    .line 5858
    :cond_d
    if-eqz v15, :cond_e

    .line 5859
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 5861
    :cond_e
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "syncDBData mms end"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5864
    const/16 v26, 0x0

    .line 5866
    .local v26, "wpmCursor":Landroid/database/Cursor;
    const-wide/16 v22, 0x0

    .line 5867
    :try_start_e
    const-string v19, ""

    .line 5868
    const/16 v25, 0x0

    .line 5870
    const/4 v5, 0x1

    const-string v6, "wpm"

    const/4 v4, 0x1

    new-array v7, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "thread_id"

    aput-object v8, v7, v4

    const-string v8, "thread_id NOT IN (SELECT _id FROM threads)  AND thread_id  < 9223372036854775806"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 5873
    :goto_5
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 5875
    const-string v4, "thread_id"

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    move-result-wide v22

    .line 5878
    const/16 v18, 0x0

    .line 5880
    .restart local v18    # "recipientIdCursor":Landroid/database/Cursor;
    :try_start_f
    const-string v5, "canonical_addresses"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v4

    const-string v7, "address=\'Push message\'"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 5883
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    .line 5884
    .restart local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :goto_6
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 5885
    const-string v4, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    goto :goto_6

    .line 5888
    .end local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :catch_4
    move-exception v14

    .line 5889
    .restart local v14    # "ex":Ljava/lang/Exception;
    :try_start_10
    throw v14
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 5891
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_4
    move-exception v4

    if-eqz v18, :cond_f

    .line 5892
    :try_start_11
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v4
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    .line 5909
    .end local v18    # "recipientIdCursor":Landroid/database/Cursor;
    :catch_5
    move-exception v14

    .line 5910
    .restart local v14    # "ex":Ljava/lang/Exception;
    :try_start_12
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v14, v4}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    .line 5911
    const/4 v4, 0x0

    .line 5913
    if-eqz v26, :cond_1

    .line 5914
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 5887
    .end local v14    # "ex":Ljava/lang/Exception;
    .restart local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .restart local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    :cond_10
    :try_start_13
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_4
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    move-result-object v19

    .line 5891
    if-eqz v18, :cond_11

    .line 5892
    :try_start_14
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 5897
    :cond_11
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, " "

    move-object/from16 v0, v19

    invoke-direct {v4, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_12

    .line 5898
    const/16 v25, 0x1

    .line 5901
    :cond_12
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 5902
    .restart local v24    # "threadsInfo":Landroid/content/ContentValues;
    const-string v4, "_id"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5903
    const-string v4, "recipient_ids"

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5904
    const-string v4, "type"

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5906
    const-string v4, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WPM syncThread. threadId= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", recipientsIds= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", type= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5907
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->syncThread(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_5
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    goto/16 :goto_5

    .line 5913
    .end local v18    # "recipientIdCursor":Landroid/database/Cursor;
    .end local v20    # "recipientIdsBuf":Ljava/lang/StringBuffer;
    .end local v24    # "threadsInfo":Landroid/content/ContentValues;
    :catchall_5
    move-exception v4

    if-eqz v26, :cond_13

    .line 5914
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    :cond_13
    throw v4

    .line 5913
    :cond_14
    if-eqz v26, :cond_15

    .line 5914
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 5916
    :cond_15
    const-string v4, "TP/MmsSmsProvider"

    const-string v5, "syncDBData end"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5917
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private syncThread(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)V
    .locals 3
    .param p1, "_db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "_threadsInfo"    # Landroid/content/ContentValues;
    .param p3, "isUpdate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 5922
    const-string v0, "threads"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 5925
    if-eqz p3, :cond_0

    .line 5926
    const-string v0, "_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateThread(Landroid/database/sqlite/SQLiteDatabase;JZ)V

    .line 5928
    :cond_0
    return-void
.end method

.method public static toIsoString([B)Ljava/lang/String;
    .locals 3
    .param p0, "bytes"    # [B

    .prologue
    .line 6448
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "iso-8859-1"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6452
    :goto_0
    return-object v1

    .line 6449
    :catch_0
    move-exception v0

    .line 6451
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "TP/MmsSmsProvider"

    const-string v2, "ISO_8859_1 must be supported!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6452
    const-string v1, ""

    goto :goto_0
.end method

.method private updateConversation(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "threadIdString"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 5454
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateConversation thread_id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5457
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5463
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 5464
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5465
    .local v2, "finalSelection":Ljava/lang/String;
    const-string v3, "pdu"

    invoke-virtual {v0, v3, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    const-string v4, "wpm"

    invoke-virtual {v0, v4, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    const-string v4, "sms"

    invoke-virtual {v0, v4, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "finalSelection":Ljava/lang/String;
    :goto_0
    return v3

    .line 5458
    :catch_0
    move-exception v1

    .line 5459
    .local v1, "exception":Ljava/lang/NumberFormatException;
    const-string v3, "TP/MmsSmsProvider"

    const-string v4, "Thread ID must be a Long."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5460
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private updateIntegratedConversation(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "threadIdString"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 5476
    const-string v3, "TP/MmsSmsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateConversation thread_id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5479
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5485
    iget-object v3, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 5486
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5487
    .local v2, "finalSelection":Ljava/lang/String;
    const-string v3, "pdu"

    invoke-virtual {v0, v3, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    const-string v4, "wpm"

    invoke-virtual {v0, v4, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    const-string v4, "sms"

    invoke-virtual {v0, v4, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    const-string v4, "im"

    invoke-virtual {v0, v4, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    const-string v4, "ft"

    invoke-virtual {v0, v4, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "finalSelection":Ljava/lang/String;
    :goto_0
    return v3

    .line 5480
    :catch_0
    move-exception v1

    .line 5481
    .local v1, "exception":Ljava/lang/NumberFormatException;
    const-string v3, "TP/MmsSmsProvider"

    const-string v4, "Thread ID must be a Long."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5482
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private updateSecretMode(Z)V
    .locals 5
    .param p1, "secretMode"    # Z

    .prologue
    .line 6279
    iget-boolean v0, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsEnableFingerPrintService:Z

    if-nez v0, :cond_0

    .line 6285
    :goto_0
    return-void

    .line 6282
    :cond_0
    const-string v0, "TP/MmsSmsProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSecretMode secretMode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 6283
    iput-boolean p1, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mIsSecretMode:Z

    .line 6284
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    goto :goto_0
.end method


# virtual methods
.method public dataCreateByLDU()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v7, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 6126
    iget-object v4, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 6127
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 6129
    .local v3, "values":Landroid/content/ContentValues;
    const/16 v4, 0x3e

    new-array v0, v4, [[Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v9

    const-string v5, "+44695839****"

    aput-object v5, v4, v8

    const-string v5, "Thanks so much for the flowers! I love them, they\'re so pretty~"

    aput-object v5, v4, v10

    aput-object v4, v0, v9

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "2"

    aput-object v5, v4, v9

    const-string v5, "+44695839****"

    aput-object v5, v4, v8

    const-string v5, "I\'m glad you liked them"

    aput-object v5, v4, v10

    aput-object v4, v0, v8

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "2"

    aput-object v5, v4, v9

    const-string v5, "+44695839****"

    aput-object v5, v4, v8

    const-string v5, "Happy Valentine\'s Day\'s!"

    aput-object v5, v4, v10

    aput-object v4, v0, v10

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "2"

    aput-object v5, v4, v9

    const-string v5, "+44695839****"

    aput-object v5, v4, v8

    const-string v5, "Hope you got me chocolates :D"

    aput-object v5, v4, v10

    aput-object v4, v0, v7

    const/4 v4, 0x4

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+44695839****"

    aput-object v6, v5, v8

    const-string v6, "You\'ll have to wait until tonight to see ;)"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/4 v4, 0x5

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+44695839****"

    aput-object v6, v5, v8

    const-string v6, "Ohh I can\'t wait. See you in a few hours!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/4 v4, 0x6

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1762810****"

    aput-object v6, v5, v8

    const-string v6, "Sorry I can\'t talk, I am in a meeting now. What\'s up?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/4 v4, 0x7

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+1762810****"

    aput-object v6, v5, v8

    const-string v6, "Oh sorry. I wanted to get the phone number of that hair salon you mentioned"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x8

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+1762810****"

    aput-object v6, v5, v8

    const-string v6, "I really need a haircut :P"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x9

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1762810****"

    aput-object v6, v5, v8

    const-string v6, "Ok, let me get back to you."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xa

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1762810****"

    aput-object v6, v5, v8

    const-string v6, "025-603-4485. It\'s near the mall. Ask for Amy ;)"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xb

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+1762810****"

    aput-object v6, v5, v8

    const-string v6, "Hehe thanks! I\'ll let you know how it goes"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xc

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+7764759****"

    aput-object v6, v5, v8

    const-string v6, "How did your exams go?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xd

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+7764759****"

    aput-object v6, v5, v8

    const-string v6, "Pretty good, I think I gave it my best"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xe

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+7764759****"

    aput-object v6, v5, v8

    const-string v6, "And best of all, I\'m officially done!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xf

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+7764759****"

    aput-object v6, v5, v8

    const-string v6, "Oh, that sounds pretty cool. Congratulations!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x10

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+7764759****"

    aput-object v6, v5, v8

    const-string v6, "I still have one more tomorrow"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x11

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+7764759****"

    aput-object v6, v5, v8

    const-string v6, "Thanks! Good luck tomorrow, and let\'s celebrate afterwards"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x12

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+63545528****"

    aput-object v6, v5, v8

    const-string v6, "Hey Grace, when is your flight?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x13

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+63545528****"

    aput-object v6, v5, v8

    const-string v6, "Midnight tonight!! I get there at around 7am."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x14

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+63545528****"

    aput-object v6, v5, v8

    const-string v6, "Want me to get you anything?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x15

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+63545528****"

    aput-object v6, v5, v8

    const-string v6, "Ah thank you, I don\'t need anything."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x16

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+63545528****"

    aput-object v6, v5, v8

    const-string v6, "Have a nice flight, and text me when you get here"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x17

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+63545528****"

    aput-object v6, v5, v8

    const-string v6, "OK, I will. We have to meet up soon, it\'s been forever!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x18

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+55345537****"

    aput-object v6, v5, v8

    const-string v6, "How was the trip to Europe?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x19

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+55345537****"

    aput-object v6, v5, v8

    const-string v6, "It was great! It was beautiful and I ate a lot of good food"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x1a

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+55345537****"

    aput-object v6, v5, v8

    const-string v6, "Oh I\'m jealous. Got any interesting stories to tell?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x1b

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+55345537****"

    aput-object v6, v5, v8

    const-string v6, "I\'ll tell you all about it on Friday. But do you recognize this?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x1c

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+55345537****"

    aput-object v6, v5, v8

    const-string v6, "Guess what I found there ;)"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x1d

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+91887346****"

    aput-object v6, v5, v8

    const-string v6, "Sorry I\'m stuck in traffic, please get started without me."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x1e

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+91887346****"

    aput-object v6, v5, v8

    const-string v6, "Don\'t worry, I\'m the only one here so far"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x1f

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+91887346****"

    aput-object v6, v5, v8

    const-string v6, "I\'m almost there~!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x20

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+91887346****"

    aput-object v6, v5, v8

    const-string v6, "OK. See you soon"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x21

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+34758980****"

    aput-object v6, v5, v8

    const-string v6, "Quick question, which one looks cuter? White one or brown one?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x22

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+34758980****"

    aput-object v6, v5, v8

    const-string v6, "Oh they\'re both cute! It\'s hard"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x23

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+34758980****"

    aput-object v6, v5, v8

    const-string v6, "I know! I can\'t decide which one to get my sister"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x24

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+34758980****"

    aput-object v6, v5, v8

    const-string v6, "You can\'t go wrong with either of them, don\'t worry ;)"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x25

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+34758980****"

    aput-object v6, v5, v8

    const-string v6, "Though I would probably choose the brown one"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x26

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "Wanna play golf on Saturday?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x27

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "Yea, definitely. I\'ve been practicing, and I\'m going to beat you this time"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x28

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "ohh you can try"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x29

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "But look at what I just got"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x2a

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "hahah wow! That looks deadly"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x2b

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "By the way, is Dan coming too?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x2c

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "I don\'t know yet, but I\'ll ask him."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x2d

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+48346783"

    aput-object v6, v5, v8

    const-string v6, "See you on Saturday at 8am sharp!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x2e

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "I\'m holding a birthday party at 10pm at my place this Friday"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x2f

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "You should come!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x30

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "Thanks for inviting me, I\'ll be there!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x31

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "I have a family dinner that day, so I might a bit late."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x32

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "What\'s your address again?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x33

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "OK, great"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x34

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "My address is 450 Main Street"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x35

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "See you on Friday!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x36

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+33947957****"

    aput-object v6, v5, v8

    const-string v6, "OK, see you. Let me know if you need me to bring anything"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x37

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "Hey, did I happen to leave my umbrella at your place?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x38

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "You lost it again?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x39

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "OK, let me check"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x3a

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "Yea... third one I lost this month"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x3b

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "Is it a foldable black one?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x3c

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "Yes!! Thanks, you\'re a lifesaver"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x3d

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+1534301****"

    aput-object v6, v5, v8

    const-string v6, "haha no problem. I\'ll give it to you on Monday"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    .line 6208
    .local v0, "dataLDU":[[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_0

    .line 6209
    const-string v4, "date"

    new-instance v5, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6210
    const-string v4, "type"

    aget-object v5, v0, v2

    aget-object v5, v5, v9

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6211
    const-string v4, "thread_id"

    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    aget-object v6, v0, v2

    aget-object v6, v6, v8

    invoke-static {v5, v6}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6212
    const-string v4, "address"

    aget-object v5, v0, v2

    aget-object v5, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6213
    const-string v4, "body"

    aget-object v5, v0, v2

    aget-object v5, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6214
    const-string v4, "read"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6216
    const-string v4, "sms"

    invoke-virtual {v1, v4, v11, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 6208
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6219
    :cond_0
    invoke-static {v1, v11, v11}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateAllThreads(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6220
    return-void
.end method

.method public dataCreateByUnpack()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v7, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 6224
    iget-object v4, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 6225
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 6227
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "DELETE FROM threads;"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6228
    const-string v4, "DELETE FROM sms;"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6230
    const/16 v4, 0x10

    new-array v0, v4, [[Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v9

    const-string v5, "+14673451"

    aput-object v5, v4, v8

    const-string v5, "Hey, what\'s the plan for today?"

    aput-object v5, v4, v10

    aput-object v4, v0, v9

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "2"

    aput-object v5, v4, v9

    const-string v5, "+14673451"

    aput-object v5, v4, v8

    const-string v5, "Let\'s meet around 3 and chat live."

    aput-object v5, v4, v10

    aput-object v4, v0, v8

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v9

    const-string v5, "+12790548"

    aput-object v5, v4, v8

    const-string v5, "I\'m thinking chicken for dinner, thoughts?"

    aput-object v5, v4, v10

    aput-object v4, v0, v10

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "2"

    aput-object v5, v4, v9

    const-string v5, "+12790548"

    aput-object v5, v4, v8

    const-string v5, "Hmmm, sounds good. I know of a really good place near the Las Vegas Strip."

    aput-object v5, v4, v10

    aput-object v4, v0, v7

    const/4 v4, 0x4

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+16840975"

    aput-object v6, v5, v8

    const-string v6, "I\'m dropping the girls off at the mall."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/4 v4, 0x5

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+16840975"

    aput-object v6, v5, v8

    const-string v6, "Ok. I will pick them up on my way home."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/4 v4, 0x6

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+19704615"

    aput-object v6, v5, v8

    const-string v6, "So.... how was your trip?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/4 v4, 0x7

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+19704615"

    aput-object v6, v5, v8

    const-string v6, "Awesome~!!!!!!!!!!!!!!!! I met bunch of new friends, went to lots of interesting places..."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x8

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+16487543"

    aput-object v6, v5, v8

    const-string v6, "Have you checked how we can register exhibit booth?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0x9

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+16487543"

    aput-object v6, v5, v8

    const-string v6, "Yup! OC\'s webpage provides \'Online Exhibit Registration\' link. Exhibit location is main hall of the venue. Better to register early to reserve the booth we want."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xa

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+13574512"

    aput-object v6, v5, v8

    const-string v6, "See the attached. OC hosts an \"ALL NIGHT PARTY\" for the attendees at Club LAVO on New Year\'s Eve."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xb

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+13574512"

    aput-object v6, v5, v8

    const-string v6, "Hooooooray!!!"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xc

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+17804613"

    aput-object v6, v5, v8

    const-string v6, "I am running little late. Can you print out 10 copies of my presentation deck?"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xd

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+17804613"

    aput-object v6, v5, v8

    const-string v6, "Sure. It looks like our meeting will be delayed about 10 minutes"

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xe

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v5, v9

    const-string v6, "+17804916"

    aput-object v6, v5, v8

    const-string v6, "On Sundays Fountain show times begin at 11 a.m. and run every 15 minutes."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    const/16 v4, 0xf

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "2"

    aput-object v6, v5, v9

    const-string v6, "+17804916"

    aput-object v6, v5, v8

    const-string v6, "Schedule may vary. Shows are subject to cancellation due to high winds."

    aput-object v6, v5, v10

    aput-object v5, v0, v4

    .line 6249
    .local v0, "dataUnpack":[[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_0

    .line 6250
    const-string v4, "date"

    invoke-direct {p0, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->makeMessagesTime(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6251
    const-string v4, "type"

    aget-object v5, v0, v2

    aget-object v5, v5, v9

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6252
    const-string v4, "thread_id"

    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    aget-object v6, v0, v2

    aget-object v6, v6, v8

    invoke-static {v5, v6}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6253
    const-string v4, "address"

    aget-object v5, v0, v2

    aget-object v5, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6254
    const-string v4, "body"

    aget-object v5, v0, v2

    aget-object v5, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6255
    const-string v4, "read"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6257
    const-string v4, "sms"

    invoke-virtual {v1, v4, v11, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 6249
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6260
    :cond_0
    invoke-static {v1, v11, v11}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateAllThreads(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6261
    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 33
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 4656
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 4657
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v15

    .line 4658
    .local v15, "context":Landroid/content/Context;
    const/4 v13, 0x0

    .line 4659
    .local v13, "affectedRows":I
    const/16 v30, 0x0

    .line 4660
    .local v30, "unreadCount":I
    const/4 v14, 0x0

    .line 4662
    .local v14, "affectedThreads":I
    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 4853
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MmsSmsProvider does not support deletes, inserts, or updates for this URI."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 4666
    :sswitch_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v28

    .line 4672
    .local v28, "threadId":J
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v5, v28, v6

    if-eqz v5, :cond_0

    .line 4673
    move-wide/from16 v0, v28

    invoke-static {v15, v4, v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getThreadUnReadCount(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v30

    .line 4676
    :cond_0
    invoke-direct/range {p0 .. p3}, Lcom/android/providers/telephony/MmsSmsProvider;->deleteConversation(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 4678
    const-string v5, "all"

    const-string v6, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 4679
    invoke-direct/range {p0 .. p3}, Lcom/android/providers/telephony/MmsSmsProvider;->markImThreadtoDelete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    .line 4682
    :cond_1
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete URI_CONVERSATIONS_MESSAGES affectedRows="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4683
    move-wide/from16 v0, v28

    invoke-static {v4, v0, v1}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateThread(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 4857
    .end local v28    # "threadId":J
    :goto_0
    if-gtz v13, :cond_2

    if-lez v14, :cond_3

    .line 4858
    :cond_2
    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, -0x1

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    .line 4861
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v25

    .line 4862
    .local v25, "threadIdString":Ljava/lang/String;
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete threadId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4864
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move/from16 v0, v30

    move-object/from16 v1, v25

    invoke-static {v0, v5, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Ljava/lang/String;)V

    .line 4866
    add-int v5, v13, v14

    .end local v25    # "threadIdString":Ljava/lang/String;
    :goto_1
    return v5

    .line 4667
    :catch_0
    move-exception v18

    .line 4668
    .local v18, "e":Ljava/lang/NumberFormatException;
    const-string v5, "TP/MmsSmsProvider"

    const-string v6, "Thread ID must be a long."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4687
    .end local v18    # "e":Ljava/lang/NumberFormatException;
    :sswitch_1
    const/16 v23, 0x0

    .line 4688
    .local v23, "mmsAffectedRows":I
    const/16 v24, 0x0

    .line 4689
    .local v24, "smsAffectedRows":I
    const/16 v32, 0x0

    .line 4691
    .local v32, "wpmAffectedRows":I
    const/16 v21, 0x0

    .line 4692
    .local v21, "imAffectedRows":I
    const/16 v20, 0x0

    .line 4694
    .local v20, "ftAfectedRows":I
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v26

    .line 4696
    .local v26, "startT":J
    const-string v5, "TP/MmsSmsProvider"

    const-string v6, "delete Conversations 1)begin db.delete(sms)"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4704
    const/16 v17, 0x0

    .line 4705
    .local v17, "deleteSmsCount":I
    const/16 v16, 0x0

    .line 4706
    .local v16, "cursor":Landroid/database/Cursor;
    const-string v5, "sms"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 4708
    if-eqz v16, :cond_4

    .line 4709
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 4710
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 4713
    :cond_4
    const/16 v5, 0x2710

    move/from16 v0, v17

    if-lt v0, v5, :cond_6

    .line 4714
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 4716
    :try_start_1
    invoke-static {v4}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->dropSmsTableDeleteTriggers(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4717
    const-string v5, "sms"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v24

    .line 4719
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    check-cast v5, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;

    invoke-virtual {v5, v4}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->rebuildCmasTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4720
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    check-cast v5, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;

    invoke-virtual {v5, v4}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->rebuildWrodsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4722
    invoke-static {v4}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->createSmsTableDeleteTriggers(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4723
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4728
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 4735
    :goto_2
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete Conversations 2)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",end db.delete(sms),affected:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4737
    const-string v5, "TP/MmsSmsProvider"

    const-string v6, "delete Conversations 3)begin MmsProvider.deleteMessages"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4740
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-static {v15, v4, v0, v1, v2}, Lcom/android/providers/telephony/MmsProvider;->deleteAllMessages(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Landroid/net/Uri;)I

    move-result v23

    .line 4742
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete Conversations 4)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",end MmsProvider.deleteMessages,affected:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4744
    const-string v5, "wpm"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v32

    .line 4747
    const-string v5, "all"

    const-string v6, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 4748
    const-string v5, "im"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v21

    .line 4749
    const-string v5, "ft"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    .line 4750
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->markAllImThreadtoDelete()I

    move-result v14

    .line 4753
    :cond_5
    add-int v5, v23, v24

    add-int v5, v5, v32

    add-int v5, v5, v21

    add-int v13, v5, v20

    .line 4759
    const-string v5, "TP/MmsSmsProvider"

    const-string v6, "delete Conversations 5)begin MmsSmsDatabaseHelper.updateAllThreads"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4760
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateAllThreads(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    .line 4761
    move/from16 v30, v13

    .line 4762
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete Conversations 6)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",end MmsSmsDatabaseHelper.updateAllThreads"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4724
    :catch_1
    move-exception v19

    .line 4725
    .local v19, "ex":Ljava/lang/Throwable;
    :try_start_2
    const-string v5, "TP/MmsSmsProvider"

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4728
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .end local v19    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 4731
    :cond_6
    const-string v5, "sms"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_2

    .line 4766
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v17    # "deleteSmsCount":I
    .end local v20    # "ftAfectedRows":I
    .end local v21    # "imAffectedRows":I
    .end local v23    # "mmsAffectedRows":I
    .end local v24    # "smsAffectedRows":I
    .end local v26    # "startT":J
    .end local v32    # "wpmAffectedRows":I
    :sswitch_2
    const-string v5, "threads"

    const-string v6, "_id NOT IN (SELECT DISTINCT thread_id FROM sms where thread_id NOT NULL UNION SELECT DISTINCT thread_id FROM wpm where thread_id NOT NULL UNION SELECT DISTINCT thread_id FROM pdu where thread_id NOT NULL)"

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 4772
    move/from16 v30, v13

    .line 4773
    goto/16 :goto_0

    .line 4777
    :sswitch_3
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 4778
    .local v22, "message_id":I
    move/from16 v0, v22

    invoke-static {v4, v0}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->deleteOneWpm(Landroid/database/sqlite/SQLiteDatabase;I)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result v13

    .line 4779
    move/from16 v30, v13

    goto/16 :goto_0

    .line 4780
    .end local v22    # "message_id":I
    :catch_2
    move-exception v18

    .line 4781
    .local v18, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bad message id: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 4787
    .end local v18    # "e":Ljava/lang/Exception;
    :sswitch_4
    const-string v5, "spam_filter"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 4789
    if-lez v13, :cond_7

    .line 4790
    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/providers/telephony/MmsSmsProvider;->notifyChange(Landroid/net/Uri;)V

    :cond_7
    move v5, v13

    .line 4791
    goto/16 :goto_1

    .line 4794
    :sswitch_5
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 4797
    .local v12, "_id":I
    const-string v5, "spam_filter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 4799
    if-lez v13, :cond_8

    .line 4800
    sget-object v5, Lcom/android/providers/telephony/MmsSmsProvider;->SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/providers/telephony/MmsSmsProvider;->notifyChange(Landroid/net/Uri;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_8
    move v5, v13

    .line 4805
    goto/16 :goto_1

    .line 4801
    .end local v12    # "_id":I
    :catch_3
    move-exception v18

    .line 4802
    .restart local v18    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bad spam filter id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 4829
    .end local v18    # "e":Ljava/lang/Exception;
    :sswitch_6
    const-string v5, "TP/MmsSmsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete  URI_MATCHER.match(uri)= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4830
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v15, v4, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->deleteAllBoxList(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v13

    .line 4831
    move/from16 v30, v13

    .line 4832
    goto/16 :goto_0

    .line 4838
    :sswitch_7
    const-string v5, "ft"

    const-string v6, "session_id"

    const-string v7, "all"

    move-object/from16 v0, p3

    invoke-static {v4, v5, v6, v0, v7}, Lcom/android/providers/telephony/ImProvider;->deleteDirectory(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)I

    .line 4839
    const-string v5, "im"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v21

    .line 4840
    .restart local v21    # "imAffectedRows":I
    const-string v5, "ft"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v4, v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    .line 4841
    .restart local v20    # "ftAfectedRows":I
    add-int v13, v21, v20

    .line 4843
    new-instance v31, Landroid/content/ContentValues;

    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-direct {v0, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 4844
    .local v31, "values":Landroid/content/ContentValues;
    const-string v5, "im_type"

    const/16 v6, 0x9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4845
    const-string v5, "im_threads"

    const/4 v6, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4847
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateAllThreads(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    .line 4848
    move/from16 v30, v13

    .line 4849
    goto/16 :goto_0

    .line 4662
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0xb -> :sswitch_2
        0x28 -> :sswitch_6
        0x29 -> :sswitch_6
        0x2a -> :sswitch_6
        0x2b -> :sswitch_6
        0x2c -> :sswitch_6
        0x2d -> :sswitch_6
        0x2e -> :sswitch_6
        0x2f -> :sswitch_6
        0x30 -> :sswitch_6
        0x31 -> :sswitch_6
        0x32 -> :sswitch_6
        0x33 -> :sswitch_6
        0x34 -> :sswitch_6
        0x35 -> :sswitch_6
        0x36 -> :sswitch_6
        0x37 -> :sswitch_6
        0x38 -> :sswitch_6
        0x39 -> :sswitch_6
        0x3a -> :sswitch_6
        0x3b -> :sswitch_6
        0xc9 -> :sswitch_3
        0x190 -> :sswitch_4
        0x191 -> :sswitch_5
        0x835 -> :sswitch_7
    .end sparse-switch
.end method

.method public fileCopy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 5624
    const/4 v7, 0x0

    .line 5625
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 5626
    .local v9, "fos":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 5627
    .local v1, "in":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 5630
    .local v6, "out":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5631
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 5632
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .local v10, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 5633
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 5635
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_e
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 5640
    if-eqz v6, :cond_0

    .line 5641
    :try_start_3
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 5643
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 5644
    :try_start_4
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 5646
    :cond_1
    :goto_1
    if-eqz v10, :cond_2

    .line 5647
    :try_start_5
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 5649
    :cond_2
    :goto_2
    if-eqz v8, :cond_3

    .line 5650
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 5654
    :cond_3
    :goto_3
    const/4 v2, 0x1

    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :cond_4
    :goto_4
    return v2

    .line 5642
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 5645
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 5648
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 5651
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 5636
    .end local v0    # "e":Ljava/io/IOException;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    .line 5637
    .local v0, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_7
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 5638
    const/4 v2, 0x0

    .line 5640
    if-eqz v6, :cond_5

    .line 5641
    :try_start_8
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 5643
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_6
    if-eqz v1, :cond_6

    .line 5644
    :try_start_9
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 5646
    :cond_6
    :goto_7
    if-eqz v9, :cond_7

    .line 5647
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 5649
    :cond_7
    :goto_8
    if-eqz v7, :cond_4

    .line 5650
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_4

    .line 5651
    :catch_5
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 5642
    .local v0, "e":Ljava/lang/Exception;
    :catch_6
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 5645
    .end local v0    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 5648
    .end local v0    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 5640
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    :goto_9
    if-eqz v6, :cond_8

    .line 5641
    :try_start_c
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 5643
    :cond_8
    :goto_a
    if-eqz v1, :cond_9

    .line 5644
    :try_start_d
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 5646
    :cond_9
    :goto_b
    if-eqz v9, :cond_a

    .line 5647
    :try_start_e
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 5649
    :cond_a
    :goto_c
    if-eqz v7, :cond_b

    .line 5650
    :try_start_f
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_c

    .line 5651
    :cond_b
    :goto_d
    throw v2

    .line 5642
    :catch_9
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 5645
    .end local v0    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 5648
    .end local v0    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 5651
    .end local v0    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 5640
    .end local v0    # "e":Ljava/io/IOException;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v2

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_9

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v2

    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_9

    .line 5636
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_d
    move-exception v0

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v0

    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_5
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 4651
    const-string v0, "vnd.android-dir/mms-sms"

    return-object v0
.end method

.method public getValidRecipients(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, -0x1

    .line 6046
    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 6047
    .local v7, "listRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_8

    .line 6048
    const/4 v2, 0x0

    .line 6049
    .local v2, "arrayAddress":[Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 6050
    .local v8, "recipient":Ljava/lang/String;
    const/4 v2, 0x0

    .line 6051
    if-eqz v8, :cond_3

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-eq v9, v11, :cond_3

    .line 6052
    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 6053
    move-object v1, v2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v0, v1, v5

    .line 6056
    .local v0, "addr":Ljava/lang/String;
    const-string v9, "TP/MmsSmsProvider"

    const-string v10, "query() URI_THREAD_ID addr with(;) :"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6057
    sget-boolean v9, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v9, :cond_2

    .line 6058
    invoke-static {v0}, Lcom/android/providers/telephony/MmsSmsProvider;->isPhoneNumberEx(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 6059
    const-string v9, " "

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 6060
    const-string v9, "-"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 6068
    :cond_1
    :goto_2
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6053
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 6063
    :cond_2
    invoke-static {v0}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 6064
    const-string v9, " "

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 6065
    const-string v9, "-"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 6070
    .end local v0    # "addr":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_3
    if-eqz v8, :cond_6

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-eq v9, v11, :cond_6

    .line 6071
    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 6072
    move-object v1, v2

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_3
    if-ge v5, v6, :cond_0

    aget-object v0, v1, v5

    .line 6075
    .restart local v0    # "addr":Ljava/lang/String;
    const-string v9, "TP/MmsSmsProvider"

    const-string v10, "query() URI_THREAD_ID addr with(,) :"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6076
    sget-boolean v9, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v9, :cond_5

    .line 6077
    invoke-static {v0}, Lcom/android/providers/telephony/MmsSmsProvider;->isPhoneNumberEx(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 6078
    const-string v9, " "

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 6079
    const-string v9, "-"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 6087
    :cond_4
    :goto_4
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6072
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 6082
    :cond_5
    invoke-static {v0}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 6083
    const-string v9, " "

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 6084
    const-string v9, "-"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 6092
    .end local v0    # "addr":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_6
    if-eqz v8, :cond_7

    .line 6093
    sget-boolean v9, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v9, :cond_9

    .line 6094
    invoke-static {v8}, Lcom/android/providers/telephony/MmsSmsProvider;->isPhoneNumberEx(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 6095
    const-string v9, "-"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 6103
    :cond_7
    :goto_5
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 6116
    .end local v2    # "arrayAddress":[Ljava/lang/String;
    .end local v7    # "listRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "recipient":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 6117
    .local v3, "ex":Ljava/lang/Exception;
    const-string v9, "TP/MmsSmsProvider"

    const-string v10, "query() URI_THREAD_ID recipients parse exception occur"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6121
    .end local v3    # "ex":Ljava/lang/Exception;
    :cond_8
    :goto_6
    return-object p1

    .line 6098
    .restart local v2    # "arrayAddress":[Ljava/lang/String;
    .restart local v7    # "listRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v8    # "recipient":Ljava/lang/String;
    :cond_9
    :try_start_1
    invoke-static {v8}, Landroid/provider/Telephony$Mms;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 6099
    const-string v9, "-"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_5

    .line 6110
    .end local v8    # "recipient":Ljava/lang/String;
    :cond_a
    move-object p1, v7

    goto :goto_6
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 5124
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 5126
    .local v0, "token":J
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/android/providers/telephony/MmsSmsProvider;->insertInner(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 5128
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method public insertWapPushMessage(Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 14
    .param p1, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v12, 0x0

    .line 5198
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7, p1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 5200
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "thread_id"

    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 5201
    .local v3, "threadId":Ljava/lang/Long;
    const-string v8, "address"

    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5203
    .local v0, "address":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v8, v8, v12

    if-nez v8, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    .line 5204
    const-string v8, "thread_id"

    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v0}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5208
    :cond_1
    const-string v8, "address"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 5209
    iget-object v8, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 5210
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "wpm"

    const-string v9, "body"

    invoke-virtual {v1, v8, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 5212
    .local v4, "rowID":J
    cmp-long v8, v4, v12

    if-lez v8, :cond_2

    .line 5213
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "content://mms-sms/wap-push-messages/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 5215
    .local v6, "uri":Landroid/net/Uri;
    const-string v8, "TP/MmsSmsProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "insert "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " succeeded"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5216
    invoke-direct {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->notifyChange()V

    .line 5218
    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.android.mms.INSERTED_MESSAGE"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5219
    .local v2, "intent":Landroid/content/Intent;
    const-string v8, "msgUri"

    invoke-virtual {v2, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 5220
    const-string v8, "boxType"

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5221
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 5228
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v6    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v6

    .line 5225
    :cond_2
    const-string v8, "TP/MmsSmsProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "insert: failed! "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5228
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public notifyChange(Landroid/net/Uri;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 5192
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    .line 5193
    return-void
.end method

.method public onCreate()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 751
    const/16 v2, 0xe

    const/16 v5, 0xf

    invoke-virtual {p0, v2, v5}, Lcom/android/providers/telephony/MmsSmsProvider;->setAppOps(II)V

    .line 752
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/providers/telephony/MmsSmsDatabaseHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 753
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x112002e

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseStrictPhoneNumberComparation:Z

    .line 770
    iget v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    if-lez v2, :cond_3

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mUseCutomCliDigit:Z

    .line 771
    const-string v2, "TP/MmsSmsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set contact matching CLI digits:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/providers/telephony/MmsSmsProvider;->mCliDigit:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v5, "CscFeature_Message_EnableMultiDraftBox"

    invoke-virtual {v2, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMultiDraftMsgBox:Z

    .line 774
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v5, "CscFeature_Message_EnableMmsTransactionCustomize4Korea"

    invoke-virtual {v2, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    .line 775
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v5, "CscFeature_Message_EnableMmsSubjectConcept4Korea"

    invoke-virtual {v2, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsSubjectConcept:Z

    .line 776
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v5, "CscFeature_Message_EnableFolderView"

    invoke-virtual {v2, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableChnFolderView:Z

    .line 778
    invoke-direct {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->registerSecretModeReceiver()V

    .line 781
    :try_start_0
    invoke-virtual {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v5, "com.samsung.android.coreapps"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 782
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableFreeMessage:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 787
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/android/providers/telephony/MmsSmsProvider;->registerFreeMessageModeReceiver()V

    .line 789
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v4, "CscFeature_Message_EnableCpm"

    invoke-virtual {v2, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    .line 790
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v4, "CscFeature_IMS_ConfigRcsFeatures"

    invoke-virtual {v2, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 792
    .local v0, "blackBirdCscFeatures":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableCpm:Z

    if-eqz v2, :cond_2

    .line 793
    :cond_1
    sput-boolean v3, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableBlackBirdRcs:Z

    .line 796
    :cond_2
    return v3

    .end local v0    # "blackBirdCscFeatures":Ljava/lang/String;
    :cond_3
    move v2, v4

    .line 770
    goto/16 :goto_0

    .line 784
    :catch_0
    move-exception v1

    .line 785
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sput-boolean v4, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableFreeMessage:Z

    goto :goto_1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 74
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 811
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 812
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v42, 0x0

    .line 814
    .local v42, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider$TimeChecker;->sStart()V

    .line 815
    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v72

    .line 816
    .local v72, "uriMatch":I
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "query,matched:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v72

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ",uri:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "query,matched:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v72

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ", calling pid = "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    sparse-switch v72, :sswitch_data_0

    .line 1345
    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unrecognized URI:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 821
    :sswitch_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getCompleteConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1348
    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :goto_0
    if-eqz v42, :cond_1

    .line 1349
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v42

    invoke-interface {v0, v7, v8}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1351
    :cond_1
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "match "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v72

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/android/providers/telephony/MmsSmsProvider$TimeChecker;->sEnd()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v7, v42

    .line 1352
    :goto_1
    return-object v7

    .line 824
    .restart local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :sswitch_1
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMultiDraftMsgBox:Z

    if-eqz v7, :cond_0

    .line 825
    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getCompleteConversationsIncludingDrafts([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto :goto_0

    .line 829
    :sswitch_2
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMultiDraftMsgBox:Z

    if-nez v7, :cond_2

    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableChnFolderView:Z

    if-eqz v7, :cond_0

    .line 830
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v65

    check-cast v65, Ljava/lang/String;

    .line 832
    .local v65, "szThreadId":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v65 .. v65}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 837
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread_id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v65

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v64

    .line 838
    .local v64, "szSelection":Ljava/lang/String;
    const-string v7, "mms_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    .line 840
    .local v53, "mmsId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v64

    move-object/from16 v3, p5

    move-object/from16 v4, v53

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getCompleteConversationsIncludingDrafts([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 841
    goto/16 :goto_0

    .line 833
    .end local v53    # "mmsId":Ljava/lang/String;
    .end local v64    # "szSelection":Ljava/lang/String;
    :catch_0
    move-exception v45

    .line 834
    .local v45, "exception":Ljava/lang/NumberFormatException;
    const-string v7, "TP/MmsSmsProvider"

    const-string v8, "Thread ID must be a Long."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    const/4 v7, 0x0

    goto :goto_1

    .line 844
    .end local v45    # "exception":Ljava/lang/NumberFormatException;
    .end local v65    # "szThreadId":Ljava/lang/String;
    :sswitch_3
    const-string v7, "simple"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v62

    .line 845
    .local v62, "simple":Ljava/lang/String;
    const-string v7, "mini"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 847
    .local v52, "mini":Ljava/lang/String;
    const-string v7, "integrated"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    .line 849
    .local v47, "integrated":Ljava/lang/String;
    if-eqz v62, :cond_4

    const-string v7, "true"

    move-object/from16 v0, v62

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 850
    const-string v7, "thread_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v70

    .line 852
    .local v70, "threadType":Ljava/lang/String;
    if-eqz v47, :cond_3

    const-string v7, "true"

    move-object/from16 v0, v47

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 853
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedSimpleConversations([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 856
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getSimpleConversations([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 860
    .end local v70    # "threadType":Ljava/lang/String;
    :cond_4
    if-eqz v52, :cond_5

    const-string v7, "true"

    move-object/from16 v0, v52

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 861
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getMiniConversations([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 863
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 867
    goto/16 :goto_0

    .line 869
    .end local v47    # "integrated":Ljava/lang/String;
    .end local v52    # "mini":Ljava/lang/String;
    .end local v62    # "simple":Ljava/lang/String;
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getConversationMessages(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 871
    goto/16 :goto_0

    .line 873
    :sswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v6, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-direct/range {v6 .. v11}, Lcom/android/providers/telephony/MmsSmsProvider;->getConversationById(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    move-result-object v42

    .line 876
    goto/16 :goto_0

    .line 878
    .restart local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v6, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-direct/range {v6 .. v11}, Lcom/android/providers/telephony/MmsSmsProvider;->getConversationById(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    move-result-object v42

    .line 881
    goto/16 :goto_0

    .line 883
    .restart local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :sswitch_7
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getMessagesByPhoneNumber(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 885
    goto/16 :goto_0

    .line 887
    :sswitch_8
    const-string v7, "recipient"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v60

    .line 889
    .local v60, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v7, "createthread"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 891
    .local v41, "createthread":Ljava/lang/String;
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMmsTransactionCustomize4Korea:Z

    if-eqz v7, :cond_6

    .line 893
    move-object/from16 v0, p0

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getValidRecipients(Ljava/util/List;)Ljava/util/List;

    move-result-object v60

    .line 896
    :cond_6
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableFreeMessage:Z

    if-nez v7, :cond_8

    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->sEnableBlackBirdRcs:Z

    if-nez v7, :cond_8

    .line 897
    if-eqz v41, :cond_7

    .line 898
    move-object/from16 v0, p0

    move-object/from16 v1, v60

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getThreadId(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 900
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v60

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getThreadId(Ljava/util/List;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 903
    :cond_8
    if-eqz v41, :cond_9

    .line 904
    move-object/from16 v0, p0

    move-object/from16 v1, v60

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getNormalThreadId(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 906
    :cond_9
    const-string v7, "true"

    move-object/from16 v0, p0

    move-object/from16 v1, v60

    invoke-direct {v0, v1, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getNormalThreadId(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 910
    goto/16 :goto_0

    .line 912
    .end local v41    # "createthread":Ljava/lang/String;
    .end local v60    # "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v10, 0x1

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 913
    .local v46, "extraSelection":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_a

    move-object/from16 v9, v46

    .line 915
    .local v9, "finalSelection":Ljava/lang/String;
    :goto_2
    const-string v7, "canonical_addresses"

    sget-object v8, Lcom/android/providers/telephony/MmsSmsProvider;->CANONICAL_ADDRESSES_COLUMNS_1:[Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v10, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 921
    goto/16 :goto_0

    .line 913
    .end local v9    # "finalSelection":Ljava/lang/String;
    :cond_a
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 924
    .end local v46    # "extraSelection":Ljava/lang/String;
    :sswitch_a
    const-string v11, "canonical_addresses"

    sget-object v12, Lcom/android/providers/telephony/MmsSmsProvider;->CANONICAL_ADDRESSES_COLUMNS_2:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v10, v6

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object/from16 v17, p5

    invoke-virtual/range {v10 .. v17}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 930
    goto/16 :goto_0

    .line 932
    :sswitch_b
    sget-object v7, Lcom/android/providers/telephony/MmsSmsProvider;->SEARCH_STRING:[Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "pattern"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v17, 0x2a

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v8

    .line 937
    if-nez p5, :cond_b

    if-nez p3, :cond_b

    if-nez p4, :cond_b

    if-eqz p2, :cond_c

    .line 941
    :cond_b
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "do not specify sortOrder, selection, selectionArgs, or projectionwith this query"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 946
    :cond_c
    const-string v7, "SELECT snippet(words, \'\', \' \', \'\', 1, 1) as snippet FROM words WHERE index_text MATCH ? ORDER BY snippet LIMIT 50;"

    sget-object v8, Lcom/android/providers/telephony/MmsSmsProvider;->SEARCH_STRING:[Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 947
    goto/16 :goto_0

    .line 953
    :sswitch_c
    :try_start_1
    const-string v7, "row_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v48

    .line 954
    .local v48, "id":J
    const-string v7, "table_to_use"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    goto/16 :goto_0

    .line 956
    :pswitch_0
    const-string v11, "sms"

    const/4 v7, 0x1

    new-array v12, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "thread_id"

    aput-object v8, v12, v7

    const-string v13, "_id=?"

    const/4 v7, 0x1

    new-array v14, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v48 .. v49}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v14, v7

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v10, v6

    invoke-virtual/range {v10 .. v17}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 964
    goto/16 :goto_0

    .line 966
    :pswitch_1
    const-string v54, "SELECT thread_id FROM pdu,part WHERE ((part.mid=pdu._id) AND (part._id=?))"

    .line 969
    .local v54, "mmsQuery":Ljava/lang/String;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v48 .. v49}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v8

    move-object/from16 v0, v54

    invoke-virtual {v6, v0, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v42

    goto/16 :goto_0

    .line 980
    .end local v48    # "id":J
    .end local v54    # "mmsQuery":Ljava/lang/String;
    :sswitch_d
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getFTContents([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 981
    goto/16 :goto_0

    .line 984
    :sswitch_e
    const-string v7, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 985
    .local v11, "type":Ljava/lang/String;
    const-string v7, "session_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 986
    .local v13, "sessionId":Ljava/lang/String;
    const-string v7, "subject"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 987
    .local v14, "subject":Ljava/lang/String;
    const-string v7, "createthread"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 988
    .local v15, "createThread":Ljava/lang/String;
    const-string v7, "force_createthread"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 990
    .local v16, "forceCreateThread":Ljava/lang/String;
    const-string v7, "recipient"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    .local v12, "chatRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v10, p0

    .line 992
    invoke-direct/range {v10 .. v16}, Lcom/android/providers/telephony/MmsSmsProvider;->getThreadId(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 993
    goto/16 :goto_0

    .line 997
    .end local v11    # "type":Ljava/lang/String;
    .end local v12    # "chatRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "sessionId":Ljava/lang/String;
    .end local v14    # "subject":Ljava/lang/String;
    .end local v15    # "createThread":Ljava/lang/String;
    .end local v16    # "forceCreateThread":Ljava/lang/String;
    :sswitch_f
    const-string v7, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 998
    .restart local v11    # "type":Ljava/lang/String;
    const-string v7, "session_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 999
    .restart local v13    # "sessionId":Ljava/lang/String;
    const-string v7, "subject"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1000
    .restart local v14    # "subject":Ljava/lang/String;
    const-string v7, "thread_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1001
    .local v19, "threadId":Ljava/lang/String;
    const-string v7, "recipient"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    .line 1002
    .restart local v12    # "chatRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v7, "only_for_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v56

    .line 1004
    .local v56, "onlyQuery":Ljava/lang/String;
    invoke-static/range {v56 .. v56}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, "true"

    move-object/from16 v0, v56

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 1005
    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->queryIntegratedThreads(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    :cond_d
    move-object/from16 v17, p0

    move-object/from16 v18, v11

    move-object/from16 v20, v12

    move-object/from16 v21, v13

    move-object/from16 v22, v14

    .line 1007
    invoke-direct/range {v17 .. v22}, Lcom/android/providers/telephony/MmsSmsProvider;->insertThreadByType(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 1013
    .end local v11    # "type":Ljava/lang/String;
    .end local v12    # "chatRecipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "sessionId":Ljava/lang/String;
    .end local v14    # "subject":Ljava/lang/String;
    .end local v19    # "threadId":Ljava/lang/String;
    .end local v56    # "onlyQuery":Ljava/lang/String;
    :sswitch_10
    const-string v7, "normal_thread_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getImThreadInfoByThreadId(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1014
    goto/16 :goto_0

    .line 1017
    :sswitch_11
    const-string v7, "session_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getImThreadInfoBySessionId(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1018
    goto/16 :goto_0

    .line 1021
    :sswitch_12
    const-string v7, "transaction_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v71

    .line 1022
    .local v71, "transactionId":Ljava/lang/String;
    const-string v7, "address"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 1023
    .local v38, "addr":Ljava/lang/String;
    const-string v7, "chatType"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 1024
    .local v40, "chatType":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v71

    move-object/from16 v2, v38

    move-object/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getImTransactionIdBySessionId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1025
    goto/16 :goto_0

    .line 1028
    .end local v38    # "addr":Ljava/lang/String;
    .end local v40    # "chatType":Ljava/lang/String;
    .end local v71    # "transactionId":Ljava/lang/String;
    :sswitch_13
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getCheckImThread()Landroid/database/Cursor;

    move-result-object v42

    .line 1029
    goto/16 :goto_0

    .line 1033
    :sswitch_14
    if-nez p5, :cond_e

    if-nez p3, :cond_e

    if-nez p4, :cond_e

    if-eqz p2, :cond_f

    .line 1037
    :cond_e
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "do not specify sortOrder, selection, selectionArgs, or projectionwith this query"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1042
    :cond_f
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "pattern"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v61

    .line 1045
    .local v61, "searchString":Ljava/lang/String;
    :try_start_2
    const-string v7, "SELECT sms._id AS _id,thread_id,address,body,date,date_sent,index_text,words._id FROM sms,words WHERE (index_text MATCH ? AND sms._id=words.source_id AND words.table_to_use=1) UNION SELECT pdu._id,thread_id,addr.address,part.text AS body,pdu.date,pdu.date_sent,index_text,words._id FROM pdu,part,addr,words WHERE ((part.mid=pdu._id) AND (addr.msg_id=pdu._id) AND (addr.type=151) AND (part.ct=\'text/plain\') AND (index_text MATCH ?) AND (part._id = words.source_id) AND (words.table_to_use=2)) GROUP BY thread_id ORDER BY thread_id ASC, date DESC"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v61, v8, v10

    const/4 v10, 0x1

    aput-object v61, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v42

    goto/16 :goto_0

    .line 1046
    :catch_1
    move-exception v44

    .line 1047
    .local v44, "ex":Ljava/lang/Exception;
    const-string v7, "TP/MmsSmsProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "got exception: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1053
    .end local v44    # "ex":Ljava/lang/Exception;
    .end local v61    # "searchString":Ljava/lang/String;
    :sswitch_15
    const-string v7, "protocol"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v59

    .line 1054
    .local v59, "protoName":Ljava/lang/String;
    const-string v7, "message"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    .line 1055
    .local v55, "msgId":Ljava/lang/String;
    invoke-static/range {v59 .. v59}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_11

    const/16 v58, -0x1

    .line 1058
    .local v58, "proto":I
    :goto_3
    const/4 v7, -0x1

    move/from16 v0, v58

    if-eq v0, v7, :cond_13

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "proto_type="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v58

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 1060
    .restart local v46    # "extraSelection":Ljava/lang/String;
    :goto_4
    invoke-static/range {v55 .. v55}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 1061
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND msg_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v55

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 1064
    :cond_10
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_14

    move-object/from16 v9, v46

    .line 1066
    .restart local v9    # "finalSelection":Ljava/lang/String;
    :goto_5
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_15

    const-string v27, "due_time"

    .line 1068
    .local v27, "finalOrder":Ljava/lang/String;
    :goto_6
    const-string v21, "pending_msgs"

    const/16 v22, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v20, v6

    move-object/from16 v23, v9

    move-object/from16 v24, p4

    invoke-virtual/range {v20 .. v27}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1070
    goto/16 :goto_0

    .line 1055
    .end local v9    # "finalSelection":Ljava/lang/String;
    .end local v27    # "finalOrder":Ljava/lang/String;
    .end local v46    # "extraSelection":Ljava/lang/String;
    .end local v58    # "proto":I
    :cond_11
    const-string v7, "sms"

    move-object/from16 v0, v59

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    const/16 v58, 0x0

    goto :goto_3

    :cond_12
    const/16 v58, 0x1

    goto :goto_3

    .line 1058
    .restart local v58    # "proto":I
    :cond_13
    const-string v46, " 0=0 "

    goto :goto_4

    .line 1064
    .restart local v46    # "extraSelection":Ljava/lang/String;
    :cond_14
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v46

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    .restart local v9    # "finalSelection":Ljava/lang/String;
    :cond_15
    move-object/from16 v27, p5

    .line 1066
    goto :goto_6

    .line 1074
    .end local v9    # "finalSelection":Ljava/lang/String;
    .end local v46    # "extraSelection":Ljava/lang/String;
    .end local v55    # "msgId":Ljava/lang/String;
    .end local v58    # "proto":I
    .end local v59    # "protoName":Ljava/lang/String;
    :sswitch_16
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getUndeliveredMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1076
    goto/16 :goto_0

    .line 1081
    :sswitch_17
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedUndeliveredMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1083
    goto/16 :goto_0

    .line 1088
    :sswitch_18
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getDraftThread([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1089
    goto/16 :goto_0

    .line 1095
    :sswitch_19
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-wide v68

    .line 1101
    .local v68, "threadId":J
    const-string v7, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 1102
    .local v51, "mType":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {v68 .. v69}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    move-object/from16 v3, v51

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getFirstLockedMessage([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1105
    goto/16 :goto_0

    .line 1096
    .end local v51    # "mType":Ljava/lang/String;
    .end local v68    # "threadId":J
    :catch_2
    move-exception v43

    .line 1097
    .local v43, "e":Ljava/lang/NumberFormatException;
    const-string v7, "TP/MmsSmsProvider"

    const-string v8, "Thread ID must be a long."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1109
    .end local v43    # "e":Ljava/lang/NumberFormatException;
    :sswitch_1a
    const-string v7, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getFirstLockedMessage([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1110
    goto/16 :goto_0

    .line 1114
    :sswitch_1b
    const-string v7, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v20, p0

    move-object/from16 v21, p2

    move-object/from16 v22, p3

    move-object/from16 v23, p4

    move-object/from16 v24, p5

    invoke-direct/range {v20 .. v25}, Lcom/android/providers/telephony/MmsSmsProvider;->getAllLockedMessage([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1116
    goto/16 :goto_0

    .line 1137
    :sswitch_1c
    const-string v7, "target_directory_path"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 1139
    .local v66, "targetDirectoryPath":Ljava/lang/String;
    if-nez v66, :cond_16

    .line 1140
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1142
    :cond_16
    const/4 v7, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v66

    invoke-direct {v0, v1, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->checkOrCreateDirectory(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_17

    .line 1143
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1145
    :cond_17
    const-string v7, "/data/data/com.android.providers.telephony/databases/mmssms.db"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v66

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "mmssms.db"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->backupDatabase(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    new-instance v67, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v66

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "mmssms.db"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v67

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1149
    .local v67, "targetFile":Ljava/io/File;
    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object/from16 v0, v67

    invoke-virtual {v0, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z

    .line 1150
    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object/from16 v0, v67

    invoke-virtual {v0, v7, v8}, Ljava/io/File;->setWritable(ZZ)Z

    .line 1151
    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object/from16 v0, v67

    invoke-virtual {v0, v7, v8}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 1153
    const-string v7, "/data/data/com.android.providers.telephony/app_parts/"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v66

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "app_parts/"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8, v10}, Lcom/android/providers/telephony/MmsSmsProvider;->directoryCopy(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_18

    .line 1154
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1156
    :cond_18
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1161
    .end local v66    # "targetDirectoryPath":Ljava/lang/String;
    .end local v67    # "targetFile":Ljava/io/File;
    :sswitch_1d
    const-string v7, "pattern"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v61

    .line 1163
    .restart local v61    # "searchString":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getMessagesBySearchString(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1166
    goto/16 :goto_0

    .line 1170
    .end local v61    # "searchString":Ljava/lang/String;
    :sswitch_1e
    const-string v7, "pattern"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v61

    .line 1172
    .restart local v61    # "searchString":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->getMessagesBySearchString(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1173
    goto/16 :goto_0

    .line 1177
    .end local v61    # "searchString":Ljava/lang/String;
    :sswitch_1f
    const-string v7, "pattern"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v61

    .line 1178
    .restart local v61    # "searchString":Ljava/lang/String;
    const/4 v7, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getBubblesBySearch(Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v42

    .line 1179
    goto/16 :goto_0

    .line 1182
    .end local v61    # "searchString":Ljava/lang/String;
    :sswitch_20
    const-string v7, "pattern"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v61

    .line 1183
    .restart local v61    # "searchString":Ljava/lang/String;
    const/4 v7, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v61

    invoke-direct {v0, v1, v2, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getOneBubbleSearch([Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v42

    .line 1185
    goto/16 :goto_0

    .line 1189
    .end local v61    # "searchString":Ljava/lang/String;
    :sswitch_21
    if-nez p4, :cond_19

    .line 1190
    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getBubbleByIntegratedSearchAllData(Z)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 1192
    :cond_19
    const/4 v7, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getBubblesByIntegratedSearch([Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v42

    .line 1194
    goto/16 :goto_0

    .line 1199
    :sswitch_22
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getWapPushMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1200
    goto/16 :goto_0

    .line 1206
    :sswitch_23
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->dbReload()V

    .line 1207
    const/4 v7, 0x0

    goto/16 :goto_1

    :sswitch_24
    move-object/from16 v20, p0

    move-object/from16 v21, p2

    move-object/from16 v22, p3

    move-object/from16 v23, p4

    move-object/from16 v24, p5

    move/from16 v25, v72

    .line 1219
    invoke-direct/range {v20 .. v25}, Lcom/android/providers/telephony/MmsSmsProvider;->getFolderviewMessage([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v42

    .line 1221
    goto/16 :goto_0

    .line 1224
    :sswitch_25
    :try_start_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v73

    .line 1225
    .local v73, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v73

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getWapPushMessages([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v42

    goto/16 :goto_0

    .line 1226
    .end local v73    # "where":Ljava/lang/String;
    :catch_3
    move-exception v43

    .line 1227
    .local v43, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad message id: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1235
    .end local v43    # "e":Ljava/lang/Exception;
    :sswitch_26
    const-string v29, "spam_filter"

    const/16 v33, 0x0

    const/16 v34, 0x0

    move-object/from16 v28, v6

    move-object/from16 v30, p2

    move-object/from16 v31, p3

    move-object/from16 v32, p4

    move-object/from16 v35, p5

    invoke-virtual/range {v28 .. v35}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto/16 :goto_1

    .line 1239
    :sswitch_27
    const-string v7, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v63

    .line 1240
    .local v63, "spamType":Ljava/lang/String;
    if-eqz v63, :cond_1a

    const-string v7, "all"

    move-object/from16 v0, v63

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 1241
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedSpamMessages([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 1243
    :cond_1a
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getSpamMessages([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1245
    goto/16 :goto_0

    .line 1248
    .end local v63    # "spamType":Ljava/lang/String;
    :sswitch_28
    const-string v7, "address"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    .line 1249
    .local v39, "address":Ljava/lang/String;
    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getConversationByAddress(Ljava/lang/String;[Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v42

    .line 1250
    goto/16 :goto_0

    .line 1254
    .end local v39    # "address":Ljava/lang/String;
    :sswitch_29
    const-string v7, "address"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    .line 1255
    .restart local v39    # "address":Ljava/lang/String;
    const/4 v7, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getConversationByAddress(Ljava/lang/String;[Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v42

    .line 1256
    goto/16 :goto_0

    .line 1260
    .end local v39    # "address":Ljava/lang/String;
    :sswitch_2a
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getDraftMessages([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1261
    goto/16 :goto_0

    .line 1264
    :sswitch_2b
    const-string v29, "threads"

    const/16 v33, 0x0

    const/16 v34, 0x0

    move-object/from16 v28, v6

    move-object/from16 v30, p2

    move-object/from16 v31, p3

    move-object/from16 v32, p4

    move-object/from16 v35, p5

    invoke-virtual/range {v28 .. v35}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1265
    goto/16 :goto_0

    .line 1268
    :sswitch_2c
    const-string v7, "integrated"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v57

    .line 1269
    .local v57, "parameter":Ljava/lang/String;
    const/16 v50, 0x0

    .line 1270
    .local v50, "isIntegratedQuery":Z
    const-string v7, "true"

    move-object/from16 v0, v57

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 1271
    const/16 v50, 0x1

    .line 1273
    :cond_1b
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    move/from16 v4, v50

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getUnreadMesagesWithFirstText([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v42

    .line 1274
    goto/16 :goto_0

    .line 1278
    .end local v50    # "isIntegratedQuery":Z
    .end local v57    # "parameter":Ljava/lang/String;
    :sswitch_2d
    const-string v36, "SELECT thread_id, substr(snippet,1,500) FROM (SELECT date * 1000 AS date, text AS snippet, thread_id, part._id AS part_id FROM pdu LEFT OUTER JOIN part ON pdu._id = part.mid AND part.ct=\'text/plain\' WHERE reserved=0) WHERE thread_id = ? ORDER BY date DESC, part_id ASC LIMIT 1"

    .line 1282
    .local v36, "MmsBodyQuery":Ljava/lang/String;
    move-object/from16 v0, v36

    move-object/from16 v1, p4

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1283
    goto/16 :goto_0

    .line 1286
    .end local v36    # "MmsBodyQuery":Ljava/lang/String;
    :sswitch_2e
    const-string v37, "SELECT thread_id FROM pdu ,threads WHERE pdu.date * 1000 = threads.date"

    .line 1287
    .local v37, "MmsIsMMSQuery":Ljava/lang/String;
    move-object/from16 v0, v37

    move-object/from16 v1, p4

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1288
    goto/16 :goto_0

    .line 1292
    .end local v37    # "MmsIsMMSQuery":Ljava/lang/String;
    :sswitch_2f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedConversationMessages(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1294
    goto/16 :goto_0

    .line 1297
    :sswitch_30
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedCompleteConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1298
    goto/16 :goto_0

    .line 1301
    :sswitch_31
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMultiDraftMsgBox:Z

    if-eqz v7, :cond_0

    .line 1302
    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedCompleteConversationsIncludingDrafts([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    goto/16 :goto_0

    .line 1308
    :sswitch_32
    sget-boolean v7, Lcom/android/providers/telephony/MmsSmsProvider;->mEnableMultiDraftMsgBox:Z

    if-eqz v7, :cond_0

    .line 1309
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v65

    check-cast v65, Ljava/lang/String;

    .line 1311
    .restart local v65    # "szThreadId":Ljava/lang/String;
    :try_start_5
    invoke-static/range {v65 .. v65}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1316
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread_id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v65

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-static {v0, v7}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v64

    .line 1317
    .restart local v64    # "szSelection":Ljava/lang/String;
    const-string v7, "mms_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    .line 1319
    .restart local v53    # "mmsId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v64

    move-object/from16 v3, p5

    move-object/from16 v4, v53

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedCompleteConversationsIncludingDrafts([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1321
    goto/16 :goto_0

    .line 1312
    .end local v53    # "mmsId":Ljava/lang/String;
    .end local v64    # "szSelection":Ljava/lang/String;
    :catch_4
    move-exception v45

    .line 1313
    .restart local v45    # "exception":Ljava/lang/NumberFormatException;
    const-string v7, "TP/MmsSmsProvider"

    const-string v8, "Thread ID must be a Long."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1314
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1325
    .end local v45    # "exception":Ljava/lang/NumberFormatException;
    .end local v65    # "szThreadId":Ljava/lang/String;
    :sswitch_33
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedConversationIMnFT(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1327
    goto/16 :goto_0

    .line 1330
    :sswitch_34
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/telephony/MmsSmsProvider;->getImConversations([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1331
    goto/16 :goto_0

    .line 1334
    :sswitch_35
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 1335
    .restart local v19    # "threadId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedDraft(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1336
    goto/16 :goto_0

    .line 1339
    .end local v19    # "threadId":Ljava/lang/String;
    :sswitch_36
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->getIntegratedConversationsLimit([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    .line 1341
    goto/16 :goto_0

    .line 972
    :catch_5
    move-exception v7

    goto/16 :goto_0

    .line 819
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_4
        0x2 -> :sswitch_5
        0x3 -> :sswitch_7
        0x4 -> :sswitch_8
        0x5 -> :sswitch_9
        0x6 -> :sswitch_15
        0x7 -> :sswitch_0
        0x8 -> :sswitch_16
        0x9 -> :sswitch_6
        0xc -> :sswitch_18
        0xd -> :sswitch_a
        0xe -> :sswitch_14
        0xf -> :sswitch_b
        0x10 -> :sswitch_1a
        0x11 -> :sswitch_19
        0x12 -> :sswitch_c
        0x1e -> :sswitch_1b
        0x1f -> :sswitch_1c
        0x24 -> :sswitch_2a
        0x25 -> :sswitch_2b
        0x26 -> :sswitch_1
        0x27 -> :sswitch_2
        0x28 -> :sswitch_24
        0x29 -> :sswitch_24
        0x2a -> :sswitch_24
        0x2b -> :sswitch_24
        0x2c -> :sswitch_24
        0x3b -> :sswitch_24
        0x65 -> :sswitch_1d
        0x66 -> :sswitch_1e
        0x67 -> :sswitch_1f
        0x68 -> :sswitch_20
        0x69 -> :sswitch_21
        0xc8 -> :sswitch_22
        0xc9 -> :sswitch_25
        0x190 -> :sswitch_26
        0x192 -> :sswitch_27
        0x1c2 -> :sswitch_28
        0x1c3 -> :sswitch_29
        0x2bd -> :sswitch_2c
        0x384 -> :sswitch_2d
        0x385 -> :sswitch_2e
        0x3e9 -> :sswitch_23
        0x834 -> :sswitch_d
        0x835 -> :sswitch_e
        0x836 -> :sswitch_f
        0x837 -> :sswitch_10
        0x838 -> :sswitch_11
        0x839 -> :sswitch_34
        0x83a -> :sswitch_30
        0x83b -> :sswitch_31
        0x83c -> :sswitch_32
        0x83d -> :sswitch_2f
        0x83e -> :sswitch_33
        0x83f -> :sswitch_17
        0x841 -> :sswitch_35
        0x842 -> :sswitch_12
        0x844 -> :sswitch_36
        0x845 -> :sswitch_13
    .end sparse-switch

    .line 954
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 28
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 5284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/telephony/MmsSmsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 5285
    .local v8, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v23, "TP/MmsSmsProvider"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "update uri: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5287
    const/4 v6, 0x0

    .line 5288
    .local v6, "affectedRows":I
    sget-object v23, Lcom/android/providers/telephony/MmsSmsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v23

    sparse-switch v23, :sswitch_data_0

    .line 5440
    new-instance v23, Ljava/lang/UnsupportedOperationException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "MmsSmsProvider does not support deletes, inserts, or updates for this URI."

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 5290
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v23

    const/16 v24, 0x1

    invoke-interface/range {v23 .. v24}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 5291
    .local v19, "threadIdString":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->updateConversation(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5294
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-static {v6, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 5444
    .end local v19    # "threadIdString":Ljava/lang/String;
    :cond_0
    :goto_0
    if-lez v6, :cond_1

    .line 5445
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    sget-object v24, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/16 v25, 0x0

    const/16 v26, 0x1

    const/16 v27, -0x1

    invoke-virtual/range {v23 .. v27}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    :cond_1
    move v7, v6

    .line 5448
    .end local v6    # "affectedRows":I
    .local v7, "affectedRows":I
    :goto_1
    return v7

    .line 5298
    .end local v7    # "affectedRows":I
    .restart local v6    # "affectedRows":I
    :sswitch_1
    const-string v23, "pending_msgs"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v24

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5299
    goto :goto_0

    .line 5302
    :sswitch_2
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "_id="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v23

    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 5303
    .local v10, "extraSelection":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_2

    move-object v12, v10

    .line 5306
    .local v12, "finalSelection":Ljava/lang/String;
    :goto_2
    const-string v23, "canonical_addresses"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-virtual {v8, v0, v1, v12, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5307
    goto :goto_0

    .line 5303
    .end local v12    # "finalSelection":Ljava/lang/String;
    :cond_2
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " AND "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_2

    .line 5311
    .end local v10    # "extraSelection":Ljava/lang/String;
    :sswitch_3
    const-string v23, "wpm"

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5313
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-static {v6, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5317
    :sswitch_4
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "_id="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 5318
    .local v22, "where":Ljava/lang/String;
    const-string v23, "wpm"

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v22

    move-object/from16 v3, p4

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5320
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-static {v6, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5326
    .end local v22    # "where":Ljava/lang/String;
    :sswitch_5
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/providers/telephony/MmsSmsProvider;->syncDBData(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v6

    .line 5327
    goto/16 :goto_0

    .line 5335
    :sswitch_6
    const-string v23, "cmas"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v24

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5337
    goto/16 :goto_0

    .line 5341
    :sswitch_7
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "_id="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->concatSelections(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 5342
    .restart local v22    # "where":Ljava/lang/String;
    const-string v23, "spam_filter"

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v22

    move-object/from16 v3, p4

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5344
    if-lez v6, :cond_3

    .line 5345
    sget-object v23, Lcom/android/providers/telephony/MmsSmsProvider;->SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->notifyChange(Landroid/net/Uri;)V

    :cond_3
    move v7, v6

    .line 5346
    .end local v6    # "affectedRows":I
    .restart local v7    # "affectedRows":I
    goto/16 :goto_1

    .line 5350
    .end local v7    # "affectedRows":I
    .end local v22    # "where":Ljava/lang/String;
    .restart local v6    # "affectedRows":I
    :sswitch_8
    const-string v23, "threads"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v24

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5351
    goto/16 :goto_0

    .line 5355
    :sswitch_9
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "_id="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v23

    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 5356
    .local v11, "extraSelection1":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_4

    move-object v13, v11

    .line 5357
    .local v13, "finalSelection1":Ljava/lang/String;
    :goto_3
    const-string v23, "threads"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-virtual {v8, v0, v1, v13, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5358
    goto/16 :goto_0

    .line 5356
    .end local v13    # "finalSelection1":Ljava/lang/String;
    :cond_4
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " AND "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_3

    .line 5362
    .end local v11    # "extraSelection1":Ljava/lang/String;
    :sswitch_a
    const-string v23, "spam_filter"

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5364
    if-lez v6, :cond_0

    .line 5365
    sget-object v23, Lcom/android/providers/telephony/MmsSmsProvider;->SPAM_FILTER_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/providers/telephony/MmsSmsProvider;->notifyChange(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 5369
    :sswitch_b
    invoke-static {v8}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateAllThreads(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    .line 5374
    :sswitch_c
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v17

    .line 5375
    .local v17, "szThreadId":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 5376
    .local v20, "threadId":J
    const-wide/16 v24, 0x0

    cmp-long v23, v20, v24

    if-lez v23, :cond_0

    .line 5377
    move-wide/from16 v0, v20

    invoke-static {v8, v0, v1}, Lcom/android/providers/telephony/MmsSmsDatabaseHelper;->updateThread(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 5379
    .end local v17    # "szThreadId":Ljava/lang/String;
    .end local v20    # "threadId":J
    :catch_0
    move-exception v9

    .line 5380
    .local v9, "e":Ljava/lang/NumberFormatException;
    const-string v23, "TP/MmsSmsProvider"

    const-string v24, "Thread ID must be a Long."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5386
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    :sswitch_d
    const-string v23, "recipient"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v15

    .line 5388
    .local v15, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v23

    if-nez v23, :cond_5

    .line 5389
    const-string v23, "recipient_ids"

    const-string v24, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5390
    const-string v23, "display_recipient_ids"

    const-string v24, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5391
    const-string v23, "recipient"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 5394
    :cond_5
    if-eqz v15, :cond_6

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v23

    if-eqz v23, :cond_6

    .line 5395
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/providers/telephony/MmsSmsProvider;->getRecipientsIds(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v16

    .line 5397
    .local v16, "recipientsIds":[Ljava/lang/String;
    const-string v23, "recipient_ids"

    const/16 v24, 0x0

    aget-object v24, v16, v24

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5398
    const-string v23, "display_recipient_ids"

    const/16 v24, 0x1

    aget-object v24, v16, v24

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5399
    const-string v23, "recipient"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 5402
    .end local v16    # "recipientsIds":[Ljava/lang/String;
    :cond_6
    const-string v23, "im_threads"

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5403
    goto/16 :goto_0

    .line 5406
    .end local v15    # "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_e
    const-string v23, "im_threads"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v24

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5407
    goto/16 :goto_0

    .line 5410
    :sswitch_f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v23

    const/16 v24, 0x1

    invoke-interface/range {v23 .. v24}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 5411
    .local v18, "targetthreadId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/telephony/MmsSmsProvider;->updateIntegratedConversation(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5414
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    invoke-static {v6, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5419
    .end local v18    # "targetthreadId":Ljava/lang/String;
    :sswitch_10
    invoke-direct/range {p0 .. p4}, Lcom/android/providers/telephony/MmsSmsProvider;->imMarkAsRead(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5420
    const-string v23, "TP/MmsSmsProvider"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "update URI_MARK_AS_READ_CONVERSATIONS_IM_N_FT, affectedRows:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5424
    :sswitch_11
    const-string v23, "im_type"

    const/16 v24, 0x2

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5425
    const-string v23, "im_threads"

    const-string v24, "im_type = 3"

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5426
    goto/16 :goto_0

    .line 5429
    :sswitch_12
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v23

    const/16 v24, 0x1

    invoke-interface/range {v23 .. v24}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 5430
    .restart local v18    # "targetthreadId":Ljava/lang/String;
    new-instance v14, Landroid/content/ContentValues;

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 5431
    .local v14, "finalValues":Landroid/content/ContentValues;
    const-string v23, "archived"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 5433
    const-string v23, "archived"

    const-string v24, "archived"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 5435
    :cond_7
    const-string v23, "threads"

    move-object/from16 v0, v23

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v8, v0, v14, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 5436
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/telephony/MmsSmsProvider;->getContext()Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    invoke-static {v6, v0, v1, v2}, Lcom/android/providers/telephony/MmsSmsProvider;->handleReadChangedBroadcast(ILandroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5288
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_12
        0x1 -> :sswitch_0
        0x5 -> :sswitch_2
        0x6 -> :sswitch_1
        0x21 -> :sswitch_6
        0x23 -> :sswitch_9
        0x25 -> :sswitch_8
        0xc8 -> :sswitch_3
        0xc9 -> :sswitch_4
        0x12c -> :sswitch_5
        0x190 -> :sswitch_a
        0x191 -> :sswitch_7
        0x258 -> :sswitch_b
        0x259 -> :sswitch_c
        0x835 -> :sswitch_d
        0x839 -> :sswitch_e
        0x83d -> :sswitch_f
        0x840 -> :sswitch_10
        0x843 -> :sswitch_11
    .end sparse-switch
.end method
