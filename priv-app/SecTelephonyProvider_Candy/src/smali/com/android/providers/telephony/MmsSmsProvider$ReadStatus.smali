.class Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;
.super Ljava/lang/Object;
.source "MmsSmsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/telephony/MmsSmsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReadStatus"
.end annotation


# instance fields
.field new_sent_time:J

.field old_sent_time:J

.field recipient:Ljava/lang/String;


# direct methods
.method public constructor <init>(JJLjava/lang/String;)V
    .locals 1
    .param p1, "old_t"    # J
    .param p3, "new_t"    # J
    .param p5, "addr"    # Ljava/lang/String;

    .prologue
    .line 6580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6581
    iput-wide p1, p0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->old_sent_time:J

    .line 6582
    iput-wide p3, p0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->new_sent_time:J

    .line 6583
    iput-object p5, p0, Lcom/android/providers/telephony/MmsSmsProvider$ReadStatus;->recipient:Ljava/lang/String;

    .line 6584
    return-void
.end method
