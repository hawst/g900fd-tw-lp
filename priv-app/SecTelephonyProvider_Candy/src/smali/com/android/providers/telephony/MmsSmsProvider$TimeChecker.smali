.class public Lcom/android/providers/telephony/MmsSmsProvider$TimeChecker;
.super Ljava/lang/Object;
.source "MmsSmsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/telephony/MmsSmsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeChecker"
.end annotation


# static fields
.field private static sStartT:J


# direct methods
.method public static sEnd()Ljava/lang/String;
    .locals 12

    .prologue
    .line 5950
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sget-wide v6, Lcom/android/providers/telephony/MmsSmsProvider$TimeChecker;->sStartT:J

    sub-long v0, v4, v6

    .line 5951
    .local v0, "elapsed":J
    const-string v2, ""

    .line 5953
    .local v2, "endTime":Ljava/lang/String;
    :try_start_0
    const-string v4, "Elapsed time : %.3f ms"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    long-to-double v8, v0

    const-wide v10, 0x412e848000000000L    # 1000000.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 5957
    :goto_0
    return-object v2

    .line 5954
    :catch_0
    move-exception v3

    .line 5955
    .local v3, "ex":Ljava/lang/IncompatibleClassChangeError;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v4}, Ljava/lang/IncompatibleClassChangeError;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method

.method public static sStart()V
    .locals 2

    .prologue
    .line 5946
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/telephony/MmsSmsProvider$TimeChecker;->sStartT:J

    .line 5947
    return-void
.end method
