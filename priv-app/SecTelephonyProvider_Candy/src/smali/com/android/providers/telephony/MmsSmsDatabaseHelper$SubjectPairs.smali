.class Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;
.super Ljava/lang/Object;
.source "MmsSmsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/telephony/MmsSmsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SubjectPairs"
.end annotation


# instance fields
.field private mMsgId:J

.field private mSub_cs:I

.field private mSubject:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5155
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mMsgId:J

    .line 5156
    const-string v0, ""

    iput-object v0, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mSubject:Ljava/lang/String;

    .line 5157
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mSub_cs:I

    .line 5158
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;I)V
    .locals 1
    .param p1, "msgId"    # J
    .param p3, "subject"    # Ljava/lang/String;
    .param p4, "sub_cs"    # I

    .prologue
    .line 5160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5161
    iput-wide p1, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mMsgId:J

    .line 5162
    iput-object p3, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mSubject:Ljava/lang/String;

    .line 5163
    iput p4, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mSub_cs:I

    .line 5164
    return-void
.end method


# virtual methods
.method public getMsgId()J
    .locals 2

    .prologue
    .line 5168
    iget-wide v0, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mMsgId:J

    return-wide v0
.end method

.method public getSubCs()I
    .locals 1

    .prologue
    .line 5176
    iget v0, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mSub_cs:I

    return v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5172
    iget-object v0, p0, Lcom/android/providers/telephony/MmsSmsDatabaseHelper$SubjectPairs;->mSubject:Ljava/lang/String;

    return-object v0
.end method
