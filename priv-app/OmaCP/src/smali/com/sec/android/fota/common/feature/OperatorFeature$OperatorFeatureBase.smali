.class Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;
.super Ljava/lang/Object;
.source "OperatorFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/feature/OperatorFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OperatorFeatureBase"
.end annotation


# static fields
.field public static final PRODUCTCODE_LTN_MOVISTAR_LIST:Ljava/lang/String; = "TMM/UFN/UFU/COB/CHT/SAM/VMT/TGU/SAL/NBS/PBS/EBE/CRM"

.field public static final SALESCODE_CHINA_LIST:Ljava/lang/String; = "CTC/CHN/CHM/CHU/CHC"

.field private static final SALESCODE_CSC:Ljava/lang/String; = "ro.csc.sales_code"

.field public static final SALESCODE_DCM:Ljava/lang/String; = "DCM"

.field public static final SALESCODE_HKTW_LIST:Ljava/lang/String; = "TGY/BRI"

.field public static final SALESCODE_KOR_ALL_LIST:Ljava/lang/String; = "SKT/SKC/SKO/KT/KTC/KTO/LG/LUC/LUO/KOO"

.field public static final SALESCODE_KOR_ANY_ALL_LIST:Ljava/lang/String; = "ANY/KOO"

.field public static final SALESCODE_KOR_KTT_DEFAULT:Ljava/lang/String; = "KT"

.field public static final SALESCODE_KOR_KTT_UNUSED_LIST:Ljava/lang/String; = "MKT/KTT"

.field public static final SALESCODE_KOR_LGT_ALL_LIST:Ljava/lang/String; = "LG/LUC/LUO"

.field public static final SALESCODE_KOR_LGT_DEFAULT:Ljava/lang/String; = "LG"

.field public static final SALESCODE_KOR_LGT_UNUSED_LIST:Ljava/lang/String; = "MLG/LGT"

.field public static final SALESCODE_KOR_SKT_ALL_LIST:Ljava/lang/String; = "SKT/SKC/SKO"

.field public static final SALESCODE_KOR_SKT_DEFAULT:Ljava/lang/String; = "SKT"

.field public static final SALESCODE_KOR_SKT_UNUSED_LIST:Ljava/lang/String; = "MSK"

.field public static final SALESCODE_LTN_OPEN:Ljava/lang/String; = "TFG"

.field public static final SALESCODE_NA_SPR_DEFAULT:Ljava/lang/String; = "SPR"

.field public static final SALESCODE_NA_SPR_LIST:Ljava/lang/String; = "BST/SPR/VMU/XAS"

.field public static final SALESCODE_NA_TMO:Ljava/lang/String; = "TMB"

.field private static final SALESCODE_RIL:Ljava/lang/String; = "ril.sales_code"

.field public static final SALESCODE_SBM:Ljava/lang/String; = "SBM"

.field private static sInstance:Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;


# instance fields
.field private mSalesCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    .line 160
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->loadSalesCode()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_0
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;

    invoke-direct {v0}, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;-><init>()V

    sput-object v0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;

    .line 155
    :cond_0
    sget-object v0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;

    return-object v0
.end method


# virtual methods
.method public getOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    return-object v0
.end method

.method public loadSalesCode()V
    .locals 5

    .prologue
    .line 172
    const-string v3, "ro.csc.sales_code"

    const-string v4, "none"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    .line 173
    const-string v3, "none"

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 174
    const-string v3, "ril.sales_code"

    const-string v4, "BTU"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    .line 180
    :cond_0
    :try_start_0
    const-string v3, "MSK"

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 181
    const-string v3, "SKT"

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    const-string v3, ""

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    .line 211
    :cond_2
    :goto_1
    return-void

    .line 182
    :cond_3
    :try_start_1
    const-string v3, "MKT/KTT"

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 183
    const-string v3, "KT"

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-static {v0}, Lcom/sec/android/fota/common/Log;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    iget-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    const-string v3, ""

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    goto :goto_1

    .line 184
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_4
    :try_start_3
    const-string v3, "MLG/LGT"

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 185
    const-string v3, "LG"

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 208
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    :cond_5
    throw v3

    .line 188
    :cond_6
    :try_start_4
    const-string v3, "BST/SPR/VMU/XAS"

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 189
    const-string v3, "SPR"

    iput-object v3, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    goto :goto_0

    .line 192
    :cond_7
    const-string v3, "TFG"

    iget-object v4, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    const-string v3, "ril.product_code"

    const-string v4, "none"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 194
    .local v2, "productCode":Ljava/lang/String;
    const-string v3, "none"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 195
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "localCode":Ljava/lang/String;
    const-string v3, "TMM/UFN/UFU/COB/CHT/SAM/VMT/TGU/SAL/NBS/PBS/EBE/CRM"

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 197
    iput-object v1, p0, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->mSalesCode:Ljava/lang/String;

    .line 198
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "Use product code as customerCode for Movistar single binary(TFG)"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->I(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
