.class public Lcom/sec/android/fota/common/cipher/RSA;
.super Ljava/lang/Object;
.source "RSA.java"


# static fields
.field public static final CIPHER_ALGORITHM:Ljava/lang/String; = "RSA/ECB/PKCS1Padding"

.field public static final KEY_ALGORITHM:Ljava/lang/String; = "RSA"

.field protected static final PUBLIC_KEY:Ljava/lang/String; = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4hCCVWWB2/kpBfkG/hhaHT/TeyND99Fj4owwfEk+vNKmP6WAmq5QA32ctQY4MvXDHDRssjQ9Y+Bxg1c4ciaQ9StgpMAnn4epuO+p6q+aHnRNHeMLOkqDHwfMfyzMq57cK/Az/yfVDjNwZrDxZ4lsE+RtTX8QUtvbdwU8O4H+KoT352mlC1kgrNTK8aJEwaEVnpD5LFG9gvWx/insJO7dr7cjk2fyj1d/aYRe2+mjSkaQX9QlYVW+COnAcRnIg9XDIOyW5tGtepFYX6w/621dQT6eoferwegH54glfIf78/GJMUUBwN/FPmOgN3siHZIZH4iv51XfOXyAb0KHlb3UswIDAQAB"

.field private static rsa:Lcom/sec/android/fota/common/cipher/RSA;

.field private static rsaOrig:Lcom/sec/android/fota/common/cipher/RSA;


# instance fields
.field private rsaCipher:Ljavax/crypto/Cipher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4hCCVWWB2/kpBfkG/hhaHT/TeyND99Fj4owwfEk+vNKmP6WAmq5QA32ctQY4MvXDHDRssjQ9Y+Bxg1c4ciaQ9StgpMAnn4epuO+p6q+aHnRNHeMLOkqDHwfMfyzMq57cK/Az/yfVDjNwZrDxZ4lsE+RtTX8QUtvbdwU8O4H+KoT352mlC1kgrNTK8aJEwaEVnpD5LFG9gvWx/insJO7dr7cjk2fyj1d/aYRe2+mjSkaQX9QlYVW+COnAcRnIg9XDIOyW5tGtepFYX6w/621dQT6eoferwegH54glfIf78/GJMUUBwN/FPmOgN3siHZIZH4iv51XfOXyAb0KHlb3UswIDAQAB"

    invoke-static {v0}, Lcom/sec/android/fota/common/cipher/RSA;->resetPublicKey(Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/sec/android/fota/common/cipher/RSA;->rsa:Lcom/sec/android/fota/common/cipher/RSA;

    sput-object v0, Lcom/sec/android/fota/common/cipher/RSA;->rsaOrig:Lcom/sec/android/fota/common/cipher/RSA;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "publicKey"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "GetInstance"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;,
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const-string v4, "RSA"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 70
    .local v1, "keyFactory":Ljava/security/KeyFactory;
    invoke-static {p1}, Lcom/sec/android/fota/common/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v3

    .line 71
    .local v3, "rawPublicKey":[B
    if-nez v3, :cond_0

    .line 72
    new-instance v4, Ljava/security/InvalidKeyException;

    const-string v5, "Key BASE64 decoding failed"

    invoke-direct {v4, v5}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 73
    :cond_0
    new-instance v2, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {v2, v3}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 74
    .local v2, "pubSpec":Ljava/security/spec/X509EncodedKeySpec;
    invoke-virtual {v1, v2}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v0

    .line 75
    .local v0, "encryptionKey":Ljava/security/Key;
    const-string v4, "RSA/ECB/PKCS1Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/fota/common/cipher/RSA;->rsaCipher:Ljavax/crypto/Cipher;

    .line 76
    iget-object v4, p0, Lcom/sec/android/fota/common/cipher/RSA;->rsaCipher:Ljavax/crypto/Cipher;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 77
    return-void
.end method

.method public static encrypt([B)[B
    .locals 1
    .param p0, "input"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/fota/common/cipher/RSA;->rsa:Lcom/sec/android/fota/common/cipher/RSA;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/cipher/RSA;->encrypt_([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static getEncryptedDataSize()I
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/fota/common/cipher/RSA;->rsa:Lcom/sec/android/fota/common/cipher/RSA;

    invoke-virtual {v0}, Lcom/sec/android/fota/common/cipher/RSA;->getEncryptedDataSize_()I

    move-result v0

    return v0
.end method

.method public static resetPublicKey()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/fota/common/cipher/RSA;->rsaOrig:Lcom/sec/android/fota/common/cipher/RSA;

    sput-object v0, Lcom/sec/android/fota/common/cipher/RSA;->rsa:Lcom/sec/android/fota/common/cipher/RSA;

    .line 49
    return-void
.end method

.method public static resetPublicKey(Ljava/lang/String;)V
    .locals 2
    .param p0, "publicKey"    # Ljava/lang/String;

    .prologue
    .line 35
    :try_start_0
    new-instance v1, Lcom/sec/android/fota/common/cipher/RSA;

    invoke-direct {v1, p0}, Lcom/sec/android/fota/common/cipher/RSA;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/sec/android/fota/common/cipher/RSA;->rsa:Lcom/sec/android/fota/common/cipher/RSA;
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3

    .line 45
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/security/InvalidKeyException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 38
    .end local v0    # "e":Ljava/security/InvalidKeyException;
    :catch_1
    move-exception v0

    .line 39
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 40
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v0

    .line 41
    .local v0, "e":Ljava/security/spec/InvalidKeySpecException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 42
    .end local v0    # "e":Ljava/security/spec/InvalidKeySpecException;
    :catch_3
    move-exception v0

    .line 43
    .local v0, "e":Ljavax/crypto/NoSuchPaddingException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized encrypt_([B)[B
    .locals 1
    .param p1, "input"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fota/common/cipher/RSA;->rsaCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEncryptedDataSize_()I
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/fota/common/cipher/RSA;->rsaCipher:Ljavax/crypto/Cipher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v0

    return v0
.end method
