.class public Lcom/sec/android/fota/common/feature/OperatorFeature;
.super Ljava/lang/Object;
.source "OperatorFeature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;
    }
.end annotation


# static fields
.field public static final mOperator:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->getOperator()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    return-void
.end method

.method private static getOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->getInstance()Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/fota/common/feature/OperatorFeature$OperatorFeatureBase;->getOperator()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isChinaOperator()Z
    .locals 2

    .prologue
    .line 18
    const-string v0, "CTC/CHN/CHM/CHU/CHC"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x1

    .line 21
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDCMOperator()Z
    .locals 2

    .prologue
    .line 32
    const-string v0, "DCM"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFotaAgreedInWriting()Z
    .locals 2

    .prologue
    .line 82
    const-string v0, "TMB"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHKTWOperator()Z
    .locals 2

    .prologue
    .line 25
    const-string v0, "TGY/BRI"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x1

    .line 28
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKoreaANYOperator()Z
    .locals 2

    .prologue
    .line 68
    const-string v0, "ANY/KOO"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x1

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKoreaAllOperator()Z
    .locals 2

    .prologue
    .line 46
    const-string v0, "SKT/SKC/SKO/KT/KTC/KTO/LG/LUC/LUO/KOO"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKoreaLGTOperator()Z
    .locals 2

    .prologue
    .line 61
    const-string v0, "LG/LUC/LUO"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKoreaSKTOperator()Z
    .locals 2

    .prologue
    .line 53
    const-string v0, "SKT/SKC/SKO"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "KT"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSBMOperator()Z
    .locals 2

    .prologue
    .line 39
    const-string v0, "SBM"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTMOOperator()Z
    .locals 2

    .prologue
    .line 75
    const-string v0, "TMB"

    sget-object v1, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const/4 v0, 0x1

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static needLocalFotaDisclaimerAgree()Z
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isKoreaAllOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
