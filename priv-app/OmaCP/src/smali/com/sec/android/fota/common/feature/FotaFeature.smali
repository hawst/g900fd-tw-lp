.class public Lcom/sec/android/fota/common/feature/FotaFeature;
.super Ljava/lang/Object;
.source "FotaFeature.java"


# static fields
.field public static final EULA_VER_GET_CELLID:I = 0x1

.field public static final EULA_VER_INIT:I = 0x0

.field private static final TAG_DEFAULT_COUNTRYCODE:I = 0x1

.field private static final TAG_DEFAULT_COUNTRYNAME:I = 0x0

.field private static final TAG_DEFAULT_MCC:I = 0x2

.field public static mAvailableScheduleUpdate:Z = false

.field public static final mBatteryMinLevel:I

.field public static mChinaFeature:Z = false

.field public static final mConfigProxyAddr:Ljava/lang/String;

.field public static mDCMFeature:Z = false

.field public static final mDefaultAutoUpdateSetting:Z

.field public static final mDefaultCacheMargin:I = 0x3c

.field public static final mDefaultCountryCode:Ljava/lang/String;

.field public static final mDefaultCountryName:Ljava/lang/String;

.field public static final mDefaultDataMargin:I = 0xb4

.field public static final mDefaultMCC:Ljava/lang/String;

.field public static final mDefaultMNC:Ljava/lang/String;

.field public static final mDefaultWiFiOnlySetting:Z

.field public static final mDeltaWeight:D = 0.5

.field public static final mDevicePreID:Ljava/lang/String;

.field public static final mDisclaimerPeriod:I

.field public static mDownloadWifiOnly:Z

.field public static final mEULAVersion:F

.field public static final mFotaInWritingAgree:Z

.field public static mHKTWFeature:Z

.field public static mKorFeature:Z

.field public static mKorLGTFeature:Z

.field public static mKorSKTFeature:Z

.field public static mLimitedDeltaSizeViaMobileNetwork:I

.field public static final mLiveDemoDevice:Z

.field public static final mLocalFotaDisclaimerAgree:Z

.field public static mMobileNetworkWarning:Z

.field public static mNewcoForTMB:Z

.field public static mNotifyDeltaSizeBeforeDownload:Z

.field public static final mOperator:Ljava/lang/String;

.field public static final mPreFixUrl:Ljava/lang/String;

.field public static final mPullOnlyViaWiFiOnRoaming:Z

.field public static final mPushType:Ljava/lang/String;

.field public static final mRegistrationWaitingTime:J

.field public static mSBMFeature:Z

.field public static final mShowCountryList:Z

.field public static mShowVersionNameAfterUpdate:Z

.field public static mSupportKies:Z

.field public static mTmoForTMB:Z

.field public static final mUnableNetworkMsgOnRoaming:Z

.field public static final mWiFiOnlyOnRoaming:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDevicePreID()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDevicePreID:Ljava/lang/String;

    .line 29
    sget-object v0, Lcom/sec/android/fota/common/feature/OperatorFeature;->mOperator:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mOperator:Ljava/lang/String;

    .line 32
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isPullonlyViaWiFiOnRoaming()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mPullOnlyViaWiFiOnRoaming:Z

    .line 35
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isWiFionlyOnRoaming()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mWiFiOnlyOnRoaming:Z

    .line 38
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isUnableNetworkMsgOnRoaming()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mUnableNetworkMsgOnRoaming:Z

    .line 41
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isLiveDemoDevice()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mLiveDemoDevice:Z

    .line 44
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultWiFiOnlySetting()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDefaultWiFiOnlySetting:Z

    .line 47
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultAutoUpdateSetting()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDefaultAutoUpdateSetting:Z

    .line 175
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isFotaAgreedInWriting()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mFotaInWritingAgree:Z

    .line 178
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isNeedLocalFotaDisclaimerAgree()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mLocalFotaDisclaimerAgree:Z

    .line 181
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isShowCountryList()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mShowCountryList:Z

    .line 184
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getEULAVersion()F

    move-result v0

    sput v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mEULAVersion:F

    .line 187
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getPushType()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mPushType:Ljava/lang/String;

    .line 190
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDisclaimerPeriod()I

    move-result v0

    sput v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDisclaimerPeriod:I

    .line 193
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getRegistrationWaitingTime()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mRegistrationWaitingTime:J

    .line 196
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultCountryName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDefaultCountryName:Ljava/lang/String;

    .line 199
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultCountryCode()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDefaultCountryCode:Ljava/lang/String;

    .line 202
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultMCC()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDefaultMCC:Ljava/lang/String;

    .line 205
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultMNC()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDefaultMNC:Ljava/lang/String;

    .line 208
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getPreFixUrl()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mPreFixUrl:Ljava/lang/String;

    .line 403
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isSupportKies()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mSupportKies:Z

    .line 406
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isDownloadWifiOnly()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDownloadWifiOnly:Z

    .line 409
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isMobileNetworkWarning()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mMobileNetworkWarning:Z

    .line 412
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isAvailableScheduleUpdate()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mAvailableScheduleUpdate:Z

    .line 415
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isChinaFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mChinaFeature:Z

    .line 418
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isHKTWFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mHKTWFeature:Z

    .line 421
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isDCMFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDCMFeature:Z

    .line 424
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isSBMFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mSBMFeature:Z

    .line 427
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isKorFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mKorFeature:Z

    .line 430
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isKorSKTFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mKorSKTFeature:Z

    .line 433
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isKorLGTFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mKorLGTFeature:Z

    .line 436
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isNewcoForTMB()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mNewcoForTMB:Z

    .line 439
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isTmoForTMB()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mTmoForTMB:Z

    .line 442
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getLimitedDeltaSizeViaMobileNetwork()I

    move-result v0

    sput v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mLimitedDeltaSizeViaMobileNetwork:I

    .line 445
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isNotifyDeltaSizeBeforeDownload()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mNotifyDeltaSizeBeforeDownload:Z

    .line 448
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isShowVersionNameAfterUpdate()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mShowVersionNameAfterUpdate:Z

    .line 451
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getConfigProxyAddr()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mConfigProxyAddr:Ljava/lang/String;

    .line 463
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getLowBatteryLevel()I

    move-result v0

    sput v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mBatteryMinLevel:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getConfigProxyAddr()Ljava/lang/String;
    .locals 3

    .prologue
    .line 656
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_ConfigProxyAddr"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultAutoUpdateSetting()Z
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->getDefaultAutoupdateSettings()Z

    move-result v0

    return v0
.end method

.method private static getDefaultCountry(I)Ljava/lang/String;
    .locals 7
    .param p0, "tag"    # I

    .prologue
    const/4 v6, 0x3

    .line 218
    const-string v1, ""

    .line 219
    .local v1, "output":Ljava/lang/String;
    const/4 v3, 0x0

    .line 221
    .local v3, "tokencount":I
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_SyncML_DefaultLocalConfig"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 223
    .local v0, "feature":Ljava/lang/String;
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v4, ","

    invoke-direct {v2, v0, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v2, "stok":Ljava/util/StringTokenizer;
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    if-eq v4, v6, :cond_0

    .line 225
    const-string v4, ""

    .line 235
    :goto_0
    return-object v4

    .line 227
    :cond_0
    sget-object v4, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v5, "Argument is correct"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/LoggerData;->D(Ljava/lang/String;)V

    .line 229
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_1

    if-ge v3, v6, :cond_1

    .line 230
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 231
    if-ne v3, p0, :cond_2

    :cond_1
    move-object v4, v1

    .line 235
    goto :goto_0

    .line 233
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static getDefaultCountryCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 358
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultCountry(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v2, "get DisclaimerFeature CountryCode"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/LoggerData;->D(Ljava/lang/String;)V

    .line 361
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    .line 363
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getDefaultCountryName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 345
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultCountry(I)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "countryName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 347
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature;->getCountryName()Ljava/lang/String;

    move-result-object v1

    .line 349
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getDefaultMCC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 372
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/sec/android/fota/common/feature/FotaFeature;->getDefaultCountry(I)Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "mcc":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v2, "get DisclaimerFeature MobileCountryCode"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/LoggerData;->D(Ljava/lang/String;)V

    .line 375
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature;->getMCC()Ljava/lang/String;

    move-result-object v1

    .line 377
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getDefaultMNC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature;->getMNC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultWiFiOnlySetting()Z
    .locals 4

    .prologue
    .line 140
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SyncML_DeltaBinaryDownVia"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "feature":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->getDefaultWifionlySettings()Z

    move-result v1

    .line 148
    :goto_0
    return v1

    .line 145
    :cond_0
    const-string v1, "wifi_only"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "wifi_dedicated"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 148
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getDevicePreID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_ConfigDevicePreId"

    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->getPhoneType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDisclaimerPeriod()I
    .locals 3

    .prologue
    .line 321
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_IntervalToNotifyDisclaimer4SwUpdate"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getEULAVersion()F
    .locals 5

    .prologue
    .line 290
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Common_EulaVersion"

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 293
    .local v1, "feature":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 297
    :goto_0
    return v2

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v2, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    .line 297
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getLimitedDeltaSizeViaMobileNetwork()I
    .locals 2

    .prologue
    .line 622
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_EnableLimitationDeltaSizeViaMobileNet"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static getLowBatteryLevel()I
    .locals 1

    .prologue
    .line 670
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isTMOOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671
    const/16 v0, 0x1e

    .line 673
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x14

    goto :goto_0
.end method

.method private static getPreFixUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isChinaFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "chn"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "www"

    goto :goto_0
.end method

.method private static getPushType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 308
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SyncML_PushTypeToTriggerSwUpdate"

    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isChinaFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SPP"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "GCM"

    goto :goto_0
.end method

.method private static getRegistrationWaitingTime()J
    .locals 2

    .prologue
    .line 331
    const-string v0, "SPP"

    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->getPushType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const-wide/32 v0, 0xa4cb80

    .line 336
    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v0, 0x493e0

    goto :goto_0
.end method

.method private static isAvailableScheduleUpdate()Z
    .locals 3

    .prologue
    .line 517
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_ConfigScheduledSwUpdate"

    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isAvailableScheduleUpdate()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isChinaFeature()Z
    .locals 2

    .prologue
    .line 528
    const-string v0, "cn"

    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isChinaOperator()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDCMFeature()Z
    .locals 1

    .prologue
    .line 547
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isDCMOperator()Z

    move-result v0

    return v0
.end method

.method private static isDownloadWifiOnly()Z
    .locals 4

    .prologue
    .line 486
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SyncML_DeltaBinaryDownVia"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 488
    .local v0, "feature":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isDownloadWifiOnly()Z

    move-result v1

    .line 494
    :goto_0
    return v1

    .line 491
    :cond_0
    const-string v1, "wifi_dedicated"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 492
    const/4 v1, 0x1

    goto :goto_0

    .line 494
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isFotaAgreedInWriting()Z
    .locals 3

    .prologue
    .line 245
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_DisableDisclaimer4SwUpdate"

    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isFotaAgreedInWriting()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isHKTWFeature()Z
    .locals 1

    .prologue
    .line 538
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isHKTWOperator()Z

    move-result v0

    return v0
.end method

.method private static isKorFeature()Z
    .locals 1

    .prologue
    .line 565
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isKoreaAllOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isKoreaANYOperator()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isKorLGTFeature()Z
    .locals 1

    .prologue
    .line 583
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isKorFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isKoreaLGTOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isKorSKTFeature()Z
    .locals 1

    .prologue
    .line 574
    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isKorFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isKoreaSKTOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isLiveDemoDevice()Z
    .locals 2

    .prologue
    .line 128
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Common_EnableLiveDemo"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isMobileNetworkWarning()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 504
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_SyncML_DisableWarning4DataCostDuringFota"

    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isMobileNetworkWarning()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private static isNeedLocalFotaDisclaimerAgree()Z
    .locals 4

    .prologue
    .line 258
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SyncML_ConfigDisclaimer4SwUpdate"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "feature":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->needLocalFotaDisclaimerAgree()Z

    move-result v1

    .line 266
    :goto_0
    return v1

    .line 263
    :cond_0
    const-string v1, "ENABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    const/4 v1, 0x1

    goto :goto_0

    .line 266
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isNewcoForTMB()Z
    .locals 5

    .prologue
    .line 596
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_SyncML_FOTA_Operator"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 598
    .local v0, "feature":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_SyncML_ReplaceLabelForFotaUpdateNoti"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 600
    .local v1, "feature2":Ljava/lang/String;
    const-string v2, "MPCS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MPCS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 601
    :cond_0
    const/4 v2, 0x1

    .line 603
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isNotifyDeltaSizeBeforeDownload()Z
    .locals 2

    .prologue
    .line 633
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_EnableNotiDeltaBinarySizeBeforeDownload"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isPullonlyViaWiFiOnRoaming()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_SyncML_SwUpdateActionDuringRoaming"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "feature":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onlywifipull"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isSBMFeature()Z
    .locals 1

    .prologue
    .line 556
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isSBMOperator()Z

    move-result v0

    return v0
.end method

.method private static isShowCountryList()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 277
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_SyncML_DisableDisclaimerSelectCountryList4SwUpdate"

    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isShowCountryList()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private static isShowVersionNameAfterUpdate()Z
    .locals 2

    .prologue
    .line 644
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_EnableShowVersionNameAferSuccessfulUpdate"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isSupportKies()Z
    .locals 3

    .prologue
    .line 473
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_SyncML_ConfigKiesUpdate"

    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isSupportKies()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isTmoForTMB()Z
    .locals 1

    .prologue
    .line 612
    invoke-static {}, Lcom/sec/android/fota/common/feature/OperatorFeature;->isTMOOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/fota/common/feature/FotaFeature;->isNewcoForTMB()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isUnableNetworkMsgOnRoaming()Z
    .locals 4

    .prologue
    .line 110
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SyncML_SwUpdateActionDuringRoaming"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "feature":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isShowRoamingNetworkFailureMsg()Z

    move-result v1

    .line 118
    :goto_0
    return v1

    .line 115
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "roaminginfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    const/4 v1, 0x1

    goto :goto_0

    .line 118
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isWiFionlyOnRoaming()Z
    .locals 4

    .prologue
    .line 91
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SyncML_SwUpdateActionDuringRoaming"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "feature":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-static {}, Lcom/sec/android/fota/common/feature/ProductFeature;->isRoamingWifiOnly()Z

    move-result v1

    .line 99
    :goto_0
    return v1

    .line 96
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onlywifi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    const/4 v1, 0x1

    goto :goto_0

    .line 99
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
