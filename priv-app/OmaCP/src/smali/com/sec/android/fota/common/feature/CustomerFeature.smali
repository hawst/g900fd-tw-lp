.class public Lcom/sec/android/fota/common/feature/CustomerFeature;
.super Ljava/lang/Object;
.source "CustomerFeature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;
    }
.end annotation


# static fields
.field public static final COUNTRYCODE_CHINA:Ljava/lang/String; = "cn"

.field private static final DEFAULT_COUNTRYCODE:Ljava/lang/String; = "gb"

.field private static final DEFAULT_COUNTRYNAME:Ljava/lang/String; = "United Kingdom"

.field private static final DEFAULT_MCC:Ljava/lang/String; = "234"

.field private static final DEFAULT_MNC:Ljava/lang/String; = "00"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    return-void
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getInstance()Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    move-result-object v1

    const-string v2, "CountryISO"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const-string v1, "gb"

    .line 52
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getCountryName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getInstance()Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    move-result-object v1

    const-string v2, "Country"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "countryName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    const-string v0, "United Kingdom"

    .line 43
    .end local v0    # "countryName":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getDefaultLanguage()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 80
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getInstance()Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    move-result-object v2

    const-string v3, "DefLanguage"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "DefLang":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    const-string v2, ""

    .line 88
    :goto_0
    return-object v2

    .line 84
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_1

    .line 85
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "_"

    invoke-direct {v1, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .local v1, "stok":Ljava/util/StringTokenizer;
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 88
    .end local v1    # "stok":Ljava/util/StringTokenizer;
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getMCC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getInstance()Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    move-result-object v1

    const-string v2, "MCCMNC"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "MccMnc":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    const-string v1, "234"

    .line 63
    :goto_0
    return-object v1

    .line 60
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    .line 61
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 63
    :cond_1
    const-string v1, "234"

    goto :goto_0
.end method

.method public static getMNC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 67
    invoke-static {}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getInstance()Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    move-result-object v1

    const-string v2, "MCCMNC"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "MccMnc":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const-string v1, "00"

    .line 74
    :goto_0
    return-object v1

    .line 71
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    .line 72
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 74
    :cond_1
    const-string v1, "00"

    goto :goto_0
.end method
