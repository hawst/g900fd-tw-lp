.class public abstract Lcom/sec/android/fota/common/cipher/AESWithRSA;
.super Ljava/lang/Object;
.source "AESWithRSA.java"


# static fields
.field private static aes:Lcom/sec/android/fota/common/cipher/AES;

.field private static encryptedKey:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    :try_start_0
    invoke-static {}, Lcom/sec/android/fota/common/cipher/AES;->newWithGeneratedKey()Lcom/sec/android/fota/common/cipher/AES;

    move-result-object v1

    sput-object v1, Lcom/sec/android/fota/common/cipher/AESWithRSA;->aes:Lcom/sec/android/fota/common/cipher/AES;

    .line 26
    invoke-static {}, Lcom/sec/android/fota/common/cipher/AESWithRSA;->reload()V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 32
    :goto_0
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 28
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/Logger$Impl;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 29
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 30
    .local v0, "e":Ljavax/crypto/NoSuchPaddingException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/Logger$Impl;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "plainString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/fota/common/cipher/AESWithRSA;->encrypt([B)[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/fota/common/Base64;->encode([B)[B

    move-result-object v1

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public static encrypt([B)[B
    .locals 2
    .param p0, "plainData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 52
    sget-object v1, Lcom/sec/android/fota/common/cipher/AESWithRSA;->aes:Lcom/sec/android/fota/common/cipher/AES;

    invoke-virtual {v1, p0}, Lcom/sec/android/fota/common/cipher/AES;->encrypt([B)[B

    move-result-object v0

    .line 53
    .local v0, "encryptedDataSet":[B
    sget-object v1, Lcom/sec/android/fota/common/cipher/AESWithRSA;->encryptedKey:[B

    invoke-static {v1, v0}, Lcom/sec/android/fota/common/cipher/util/Bytes;->mergeBytes([B[B)[B

    move-result-object v1

    return-object v1
.end method

.method public static reload()V
    .locals 2

    .prologue
    .line 36
    :try_start_0
    sget-object v1, Lcom/sec/android/fota/common/cipher/AESWithRSA;->aes:Lcom/sec/android/fota/common/cipher/AES;

    invoke-virtual {v1}, Lcom/sec/android/fota/common/cipher/AES;->getKey()[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/fota/common/cipher/RSA;->encrypt([B)[B

    move-result-object v1

    sput-object v1, Lcom/sec/android/fota/common/cipher/AESWithRSA;->encryptedKey:[B
    :try_end_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 42
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 39
    .end local v0    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_1
    move-exception v0

    .line 40
    .local v0, "e":Ljavax/crypto/BadPaddingException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
