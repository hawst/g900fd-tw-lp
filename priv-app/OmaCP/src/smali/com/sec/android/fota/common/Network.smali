.class public Lcom/sec/android/fota/common/Network;
.super Ljava/lang/Object;
.source "Network.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isBluetoothNetworkConnected(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 116
    const/4 v0, 0x7

    invoke-static {p0, v0}, Lcom/sec/android/fota/common/Network;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v1, "Bluetooth Network is connected"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/LoggerData;->I(Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x1

    .line 120
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDataConnected(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDataNetworkConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 22
    if-nez p0, :cond_1

    .line 23
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "Context is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->W(Ljava/lang/String;)V

    .line 44
    :cond_0
    :goto_0
    return v2

    .line 27
    :cond_1
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 29
    .local v0, "conMgr":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_2

    .line 30
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "ConnectivityManager is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->W(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 35
    .local v1, "nInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_3

    .line 36
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "NetworkInfo is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->W(Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :cond_3
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 41
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isEthernetNetworkConnected(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    const/16 v0, 0x9

    invoke-static {p0, v0}, Lcom/sec/android/fota/common/Network;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v1, "Ethernet Network is connected"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/LoggerData;->I(Ljava/lang/String;)V

    .line 133
    const/4 v0, 0x1

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMobileNetworkConnected(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-static {p0, v0}, Lcom/sec/android/fota/common/Network;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    sget-object v0, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v1, "Mobile Network is connected"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/LoggerData;->I(Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x1

    .line 106
    :cond_0
    return v0
.end method

.method protected static isNetworkConnected(Landroid/content/Context;I)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    const/4 v2, 0x0

    .line 55
    if-nez p0, :cond_1

    .line 56
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "Context is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->W(Ljava/lang/String;)V

    .line 77
    :cond_0
    :goto_0
    return v2

    .line 60
    :cond_1
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 62
    .local v0, "conMgr":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_2

    .line 63
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "ConnectivityManager is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->W(Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :cond_2
    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 68
    .local v1, "nInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_3

    .line 69
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v4, "NetworkInfo is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/LoggerData;->W(Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_3
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isRoamingNetwork(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->isNetworkRoaming()Z

    move-result v0

    return v0
.end method

.method public static isRoamingNetworkConnected(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    invoke-static {p0}, Lcom/sec/android/fota/common/Network;->isRoamingNetwork(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/fota/common/Network;->isDataConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    sget-object v0, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v1, "Data Netoworks use for Roaming"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/LoggerData;->I(Ljava/lang/String;)V

    .line 166
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWiFiNetworkConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 87
    invoke-static {p0, v0}, Lcom/sec/android/fota/common/Network;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    const-string v2, "WiFi Network is connected"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/LoggerData;->I(Ljava/lang/String;)V

    .line 92
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
