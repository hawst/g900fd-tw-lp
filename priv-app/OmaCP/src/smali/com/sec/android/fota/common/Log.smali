.class public Lcom/sec/android/fota/common/Log;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field public static final ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

.field public static final DATA:Lcom/sec/android/fota/common/log/LoggerData;

.field public static final FILE:Lcom/sec/android/fota/common/log/LoggerFile;

.field public static final TAG_NAME:Ljava/lang/String; = "FOTA"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-array v0, v4, [Ljava/lang/Class;

    const-class v1, Lcom/sec/android/fota/common/Log;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/sec/android/fota/common/log/LogLineInfo;->excludeClass([Ljava/lang/Class;)V

    .line 21
    const-string v0, "FOTA"

    invoke-static {v0}, Lcom/sec/android/fota/common/log/LoggerData;->newInstance(Ljava/lang/String;)Lcom/sec/android/fota/common/log/LoggerData;

    move-result-object v0

    sput-object v0, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    .line 23
    new-instance v0, Lcom/sec/android/fota/common/log/LoggerFile;

    invoke-direct {v0}, Lcom/sec/android/fota/common/log/LoggerFile;-><init>()V

    sput-object v0, Lcom/sec/android/fota/common/Log;->FILE:Lcom/sec/android/fota/common/log/LoggerFile;

    .line 25
    new-instance v0, Lcom/sec/android/fota/common/log/LoggerCollection;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/sec/android/fota/common/log/Logger$Core;

    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    aput-object v2, v1, v3

    sget-object v2, Lcom/sec/android/fota/common/Log;->FILE:Lcom/sec/android/fota/common/log/LoggerFile;

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/sec/android/fota/common/log/LoggerCollection;-><init>([Lcom/sec/android/fota/common/log/Logger$Core;)V

    sput-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static D(Ljava/lang/String;)V
    .locals 1
    .param p0, "param"    # Ljava/lang/String;

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->D(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public static E(Ljava/lang/String;)V
    .locals 1
    .param p0, "param"    # Ljava/lang/String;

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->E(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public static H(Ljava/lang/String;)V
    .locals 1
    .param p0, "param"    # Ljava/lang/String;

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->H(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public static I(Ljava/lang/String;)V
    .locals 1
    .param p0, "param"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->I(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public static V(Ljava/lang/String;)V
    .locals 1
    .param p0, "param"    # Ljava/lang/String;

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->V(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public static W(Ljava/lang/String;)V
    .locals 1
    .param p0, "param"    # Ljava/lang/String;

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->W(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public static printStackTrace(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerCollection;->printStackTrace(Ljava/lang/Throwable;)V

    .line 53
    return-void
.end method

.method public static setContext(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    if-eqz p0, :cond_0

    .line 61
    sget-object v0, Lcom/sec/android/fota/common/Log;->FILE:Lcom/sec/android/fota/common/log/LoggerFile;

    new-instance v1, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;

    new-instance v2, Lcom/sec/android/fota/common/log/LogFileDescriptor;

    const/4 v3, 0x2

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/sec/android/fota/common/log/LogFileDescriptor;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-wide/32 v4, 0x100000

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;-><init>(Lcom/sec/android/fota/common/log/LogDescriptor$Stream;J)V

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/LoggerFile;->setLogDescriptor(Lcom/sec/android/fota/common/log/LogDescriptor;)V

    .line 63
    :cond_0
    return-void
.end method

.method public static setLogDescriptor(Lcom/sec/android/fota/common/log/LogDescriptor;)V
    .locals 1
    .param p0, "logDescriptor"    # Lcom/sec/android/fota/common/log/LogDescriptor;

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/fota/common/Log;->FILE:Lcom/sec/android/fota/common/log/LoggerFile;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerFile;->setLogDescriptor(Lcom/sec/android/fota/common/log/LogDescriptor;)V

    .line 67
    return-void
.end method

.method public static setTagName(Ljava/lang/String;)V
    .locals 1
    .param p0, "tagName"    # Ljava/lang/String;

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v0, p0}, Lcom/sec/android/fota/common/log/LoggerData;->setTagName(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public static shift()V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/fota/common/Log;->FILE:Lcom/sec/android/fota/common/log/LoggerFile;

    invoke-virtual {v0}, Lcom/sec/android/fota/common/log/LoggerFile;->shift()V

    .line 71
    return-void
.end method
