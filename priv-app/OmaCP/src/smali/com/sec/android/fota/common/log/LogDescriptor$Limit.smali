.class public Lcom/sec/android/fota/common/log/LogDescriptor$Limit;
.super Lcom/sec/android/fota/common/log/LogDescriptor;
.source "LogDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/log/LogDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Limit"
.end annotation


# instance fields
.field private limit:J

.field private logSteramDescriptor:Lcom/sec/android/fota/common/log/LogDescriptor$Stream;


# direct methods
.method public constructor <init>(Lcom/sec/android/fota/common/log/LogDescriptor$Stream;J)V
    .locals 0
    .param p1, "logSteramDescriptor"    # Lcom/sec/android/fota/common/log/LogDescriptor$Stream;
    .param p2, "limit"    # J

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/fota/common/log/LogDescriptor;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->logSteramDescriptor:Lcom/sec/android/fota/common/log/LogDescriptor$Stream;

    .line 75
    iput-wide p2, p0, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->limit:J

    .line 76
    return-void
.end method


# virtual methods
.method public onBefore()V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->logSteramDescriptor:Lcom/sec/android/fota/common/log/LogDescriptor$Stream;

    iget-wide v2, p0, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->limit:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/fota/common/log/LogDescriptor$Stream;->isLogExceeds(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->shift()V

    .line 92
    :cond_0
    return-void
.end method

.method public println(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->logSteramDescriptor:Lcom/sec/android/fota/common/log/LogDescriptor$Stream;

    invoke-virtual {v0, p1}, Lcom/sec/android/fota/common/log/LogDescriptor$Stream;->println(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public shift()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/fota/common/log/LogDescriptor$Limit;->logSteramDescriptor:Lcom/sec/android/fota/common/log/LogDescriptor$Stream;

    invoke-virtual {v0}, Lcom/sec/android/fota/common/log/LogDescriptor$Stream;->shift()V

    .line 81
    return-void
.end method
