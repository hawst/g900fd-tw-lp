.class public Lcom/sec/android/fota/common/Device$DeviceID;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceID"
.end annotation


# instance fields
.field private mId:Ljava/lang/String;

.field private mPreId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "cxt"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    .line 124
    invoke-static {p1}, Lcom/sec/android/fota/common/Device;->isWifiOnlyModel(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mLiveDemoDevice:Z

    if-eqz v0, :cond_2

    .line 125
    :cond_0
    const-string v0, "ro.serialno"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    .line 126
    const-string v0, "TWID"

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    .line 149
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getUniqueDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    .line 129
    sget-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDevicePreID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getUniquePhoneType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 136
    const-string v0, "IMEI"

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    .line 143
    :goto_1
    const-string v0, "MEID"

    iget-object v1, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "VZW"

    sget-object v1, Lcom/sec/android/fota/common/feature/FotaFeature;->mOperator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xf

    if-lt v0, v1, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0xe

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    goto :goto_0

    .line 132
    :pswitch_0
    const-string v0, "MEID"

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    goto :goto_1

    .line 140
    :cond_3
    sget-object v0, Lcom/sec/android/fota/common/feature/FotaFeature;->mDevicePreID:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    goto :goto_1

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getPreID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    return-object v0
.end method

.method public isVaild()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const/4 v0, 0x1

    .line 172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 179
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mPreId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/fota/common/Device$DeviceID;->mId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
