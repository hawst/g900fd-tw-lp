.class public abstract Lcom/sec/android/fota/common/log/LogDescriptor$Stream;
.super Lcom/sec/android/fota/common/log/LogDescriptor;
.source "LogDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/log/LogDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stream"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/fota/common/log/LogDescriptor;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getOutputStream()Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public isLogExceeds(J)Z
    .locals 3
    .param p1, "limit"    # J

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/fota/common/log/LogDescriptor$Stream;->size()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public println(Ljava/lang/String;)V
    .locals 5
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 47
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    invoke-virtual {p0}, Lcom/sec/android/fota/common/log/LogDescriptor$Stream;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4

    .line 50
    .local v0, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4

    .line 65
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    :goto_0
    return-void

    .line 56
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v2, v1}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 60
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 61
    .local v1, "e":Ljava/io/FileNotFoundException;
    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v2, v1}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 51
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :catch_2
    move-exception v1

    .line 52
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v2, v1}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 55
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 56
    :catch_3
    move-exception v1

    .line 57
    :try_start_6
    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v2, v1}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 62
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 63
    .local v1, "e":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v2, v1}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 54
    .end local v1    # "e":Ljava/lang/RuntimeException;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v2

    .line 55
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4

    .line 58
    :goto_1
    :try_start_8
    throw v2

    .line 56
    :catch_5
    move-exception v1

    .line 57
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v3, v1}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1
.end method

.method protected abstract size()J
.end method
