.class public Lcom/sec/android/fota/common/AESCrypt;
.super Ljava/lang/Object;
.source "AESCrypt.java"


# static fields
.field private static final CRYPTO_KEY_ALGORITHM:Ljava/lang/String; = "AES"

.field private static final CRYPTO_KEY_SIZE:I = 0x80

.field private static final CRYPTO_RANDOM_ALGORITHM:Ljava/lang/String; = "SHA1PRNG"

.field public static final CRYPTO_SEED_PASSWORD:Ljava/lang/String; = "fotaclient"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decryptor([B)Ljava/lang/String;
    .locals 6
    .param p0, "decryptData"    # [B

    .prologue
    .line 134
    const-string v2, ""

    .line 135
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 137
    .local v1, "result":[B
    const/4 v4, 0x2

    const/4 v5, 0x0

    :try_start_0
    invoke-static {p0, v4, v5}, Lcom/sec/android/fota/common/AESCrypt;->getCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 138
    if-eqz v1, :cond_0

    .line 139
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 144
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static decryptor([BLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "decryptData"    # [B
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 115
    const-string v2, ""

    .line 116
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 118
    .local v1, "result":[B
    const/4 v4, 0x2

    :try_start_0
    invoke-static {p0, v4, p1}, Lcom/sec/android/fota/common/AESCrypt;->getCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 119
    if-eqz v1, :cond_0

    .line 120
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 125
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static decryptorString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "szDecryptText"    # Ljava/lang/String;

    .prologue
    .line 153
    const-string v2, ""

    .line 154
    .local v2, "szResultLog":Ljava/lang/String;
    const/4 v0, 0x0

    .line 156
    .local v0, "decryptionResult":[B
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 157
    invoke-static {p0}, Lcom/sec/android/fota/common/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    .line 159
    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/sec/android/fota/common/AESCrypt;->getCryptionResult([BILjava/lang/String;)[B

    move-result-object v0

    .line 161
    if-eqz v0, :cond_0

    .line 162
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szResultLog":Ljava/lang/String;
    .local v3, "szResultLog":Ljava/lang/String;
    move-object v2, v3

    .line 169
    .end local v3    # "szResultLog":Ljava/lang/String;
    .restart local v2    # "szResultLog":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 166
    :catch_0
    move-exception v1

    .line 167
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encryptor(Ljava/lang/String;)[B
    .locals 5
    .param p0, "szEncryptText"    # Ljava/lang/String;

    .prologue
    .line 50
    const/4 v1, 0x0

    .line 52
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/fota/common/AESCrypt;->getCryptionResult([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 59
    :cond_0
    :goto_0
    return-object v1

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encryptor(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4
    .param p0, "szEncryptText"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 32
    const/4 v1, 0x0

    .line 34
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 35
    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/sec/android/fota/common/AESCrypt;->getCryptionResult([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 41
    :cond_0
    :goto_0
    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encryptorLog(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szEncryptText"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v1, ""

    .line 70
    .local v1, "szResultLog":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/fota/common/AESCrypt;->encryptor(Ljava/lang/String;)[B

    move-result-object v3

    .line 71
    .local v3, "tmp":[B
    if-eqz v3, :cond_0

    .line 72
    invoke-static {v3}, Lcom/sec/android/fota/common/Base64;->encode([B)[B

    move-result-object v3

    .line 73
    if-eqz v3, :cond_0

    .line 74
    new-instance v2, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "szResultLog":Ljava/lang/String;
    .local v2, "szResultLog":Ljava/lang/String;
    move-object v1, v2

    .line 80
    .end local v2    # "szResultLog":Ljava/lang/String;
    .end local v3    # "tmp":[B
    .restart local v1    # "szResultLog":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encryptorString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "szEncryptText"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v2, ""

    .line 90
    .local v2, "szResultLog":Ljava/lang/String;
    const/4 v1, 0x0

    .line 92
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 93
    const-string v4, "UTF-8"

    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/sec/android/fota/common/AESCrypt;->getCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 95
    if-eqz v1, :cond_0

    .line 96
    invoke-static {v1}, Lcom/sec/android/fota/common/Base64;->encode([B)[B

    move-result-object v1

    .line 97
    if-eqz v1, :cond_0

    .line 98
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szResultLog":Ljava/lang/String;
    .local v3, "szResultLog":Ljava/lang/String;
    move-object v2, v3

    .line 105
    .end local v3    # "szResultLog":Ljava/lang/String;
    .restart local v2    # "szResultLog":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static getCryptionResult([BILjava/lang/String;)[B
    .locals 10
    .param p0, "cryptionData"    # [B
    .param p1, "nCryptionMode"    # I
    .param p2, "szSeedPassword"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "GetInstance"
        }
    .end annotation

    .prologue
    .line 181
    const/4 v3, 0x0

    .line 183
    .local v3, "mCipher":Ljavax/crypto/Cipher;
    const/4 v0, 0x0

    .line 184
    .local v0, "cryptResult":[B
    const/4 v1, 0x0

    .line 186
    .local v1, "cryptionKey":[B
    const/4 v6, 0x0

    .line 187
    .local v6, "mSecureRandom":Ljava/security/SecureRandom;
    const/4 v4, 0x0

    .line 188
    .local v4, "mKeyGenerator":Ljavax/crypto/KeyGenerator;
    const/4 v5, 0x0

    .line 191
    .local v5, "mSecretKey":Ljavax/crypto/SecretKey;
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 192
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 193
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v8, 0x172c

    const/16 v9, 0x10

    invoke-static {v8, v9}, Lcom/sec/android/fota/common/AESCrypt;->mealyMachine(II)Ljava/lang/String;

    move-result-object v8

    const-string v9, "UTF-8"

    invoke-virtual {v8, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v8

    const-string v9, "AES"

    invoke-direct {v7, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v3, p1, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 195
    invoke-virtual {v3, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 213
    :goto_0
    return-object v0

    .line 197
    :cond_0
    const-string v7, "SHA1PRNG"

    invoke-static {v7}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v6

    .line 198
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v4

    .line 199
    const-string v7, "fotaclient"

    const-string v8, "UTF-8"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 200
    const/16 v7, 0x80

    invoke-virtual {v4, v7, v6}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 201
    invoke-virtual {v4}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v5

    .line 202
    invoke-interface {v5}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    .line 204
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 205
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    const-string v8, "AES"

    invoke-direct {v7, v1, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v3, p1, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 206
    invoke-virtual {v3, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v2

    .line 210
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static mealyMachine(II)Ljava/lang/String;
    .locals 11
    .param p0, "v"    # I
    .param p1, "sz"    # I

    .prologue
    .line 217
    new-array v8, p1, [B

    .line 218
    .local v8, "str":[B
    const/16 v9, 0x10

    new-array v4, v9, [[I

    const/4 v9, 0x0

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_0

    aput-object v10, v4, v9

    const/4 v9, 0x1

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_1

    aput-object v10, v4, v9

    const/4 v9, 0x2

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_2

    aput-object v10, v4, v9

    const/4 v9, 0x3

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_3

    aput-object v10, v4, v9

    const/4 v9, 0x4

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_4

    aput-object v10, v4, v9

    const/4 v9, 0x5

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_5

    aput-object v10, v4, v9

    const/4 v9, 0x6

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_6

    aput-object v10, v4, v9

    const/4 v9, 0x7

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_7

    aput-object v10, v4, v9

    const/16 v9, 0x8

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_8

    aput-object v10, v4, v9

    const/16 v9, 0x9

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_9

    aput-object v10, v4, v9

    const/16 v9, 0xa

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_a

    aput-object v10, v4, v9

    const/16 v9, 0xb

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_b

    aput-object v10, v4, v9

    const/16 v9, 0xc

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_c

    aput-object v10, v4, v9

    const/16 v9, 0xd

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_d

    aput-object v10, v4, v9

    const/16 v9, 0xe

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_e

    aput-object v10, v4, v9

    const/16 v9, 0xf

    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_f

    aput-object v10, v4, v9

    .line 253
    .local v4, "next":[[I
    const/16 v9, 0x10

    new-array v5, v9, [[C

    const/4 v9, 0x0

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_10

    aput-object v10, v5, v9

    const/4 v9, 0x1

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_11

    aput-object v10, v5, v9

    const/4 v9, 0x2

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_12

    aput-object v10, v5, v9

    const/4 v9, 0x3

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_13

    aput-object v10, v5, v9

    const/4 v9, 0x4

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_14

    aput-object v10, v5, v9

    const/4 v9, 0x5

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_15

    aput-object v10, v5, v9

    const/4 v9, 0x6

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_16

    aput-object v10, v5, v9

    const/4 v9, 0x7

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_17

    aput-object v10, v5, v9

    const/16 v9, 0x8

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_18

    aput-object v10, v5, v9

    const/16 v9, 0x9

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_19

    aput-object v10, v5, v9

    const/16 v9, 0xa

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_1a

    aput-object v10, v5, v9

    const/16 v9, 0xb

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_1b

    aput-object v10, v5, v9

    const/16 v9, 0xc

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_1c

    aput-object v10, v5, v9

    const/16 v9, 0xd

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_1d

    aput-object v10, v5, v9

    const/16 v9, 0xe

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_1e

    aput-object v10, v5, v9

    const/16 v9, 0xf

    const/4 v10, 0x2

    new-array v10, v10, [C

    fill-array-data v10, :array_1f

    aput-object v10, v5, v9

    .line 289
    .local v5, "out_char":[[C
    const/4 v7, 0x0

    .local v7, "state":I
    const/4 v2, 0x0

    .local v2, "len":I
    move v3, v2

    .line 291
    .end local v2    # "len":I
    .local v3, "len":I
    :goto_0
    if-ge v3, p1, :cond_0

    .line 292
    and-int/lit8 v1, p0, 0x1

    .line 293
    .local v1, "input":I
    shr-int/lit8 p0, p0, 0x1

    .line 294
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "len":I
    .restart local v2    # "len":I
    aget-object v9, v5, v7

    aget-char v9, v9, v1

    int-to-byte v9, v9

    aput-byte v9, v8, v3

    .line 295
    aget-object v9, v4, v7

    aget v7, v9, v1

    move v3, v2

    .line 296
    .end local v2    # "len":I
    .restart local v3    # "len":I
    goto :goto_0

    .line 298
    .end local v1    # "input":I
    :cond_0
    const-string v6, ""

    .line 300
    .local v6, "result":Ljava/lang/String;
    :try_start_0
    new-instance v6, Ljava/lang/String;

    .end local v6    # "result":Ljava/lang/String;
    const-string v9, "UTF-8"

    invoke-direct {v6, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    .restart local v6    # "result":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 301
    .end local v6    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v6, ""

    .restart local v6    # "result":Ljava/lang/String;
    goto :goto_1

    .line 218
    :array_0
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x4
    .end array-data

    :array_2
    .array-data 4
        0x8
        0xf
    .end array-data

    :array_3
    .array-data 4
        0xb
        0x2
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3
    .end array-data

    :array_5
    .array-data 4
        0x9
        0x0
    .end array-data

    :array_6
    .array-data 4
        0xf
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_b
    .array-data 4
        0x1
        0x6
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x3
        0xd
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x2
        0xd
    .end array-data

    .line 253
    :array_10
    .array-data 2
        0x73s
        0x33s
    .end array-data

    :array_11
    .array-data 2
        0x76s
        0x6es
    .end array-data

    :array_12
    .array-data 2
        0x31s
        0x39s
    .end array-data

    :array_13
    .array-data 2
        0x6ds
        0x30s
    .end array-data

    :array_14
    .array-data 2
        0x65s
        0x63s
    .end array-data

    :array_15
    .array-data 2
        0x33s
        0x42s
    .end array-data

    :array_16
    .array-data 2
        0x37s
        0x4es
    .end array-data

    :array_17
    .array-data 2
        0x6bs
        0x32s
    .end array-data

    :array_18
    .array-data 2
        0x32s
        0x43s
    .end array-data

    :array_19
    .array-data 2
        0x61s
        0x43s
    .end array-data

    :array_1a
    .array-data 2
        0x4as
        0x32s
    .end array-data

    :array_1b
    .array-data 2
        0x79s
        0x6cs
    .end array-data

    :array_1c
    .array-data 2
        0x38s
        0x64s
    .end array-data

    :array_1d
    .array-data 2
        0x31s
        0x30s
    .end array-data

    :array_1e
    .array-data 2
        0x41s
        0x5es
    .end array-data

    :array_1f
    .array-data 2
        0x37s
        0x30s
    .end array-data
.end method
