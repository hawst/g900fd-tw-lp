.class public Lcom/sec/android/fota/common/log/LoggerData$SecOff;
.super Lcom/sec/android/fota/common/log/LoggerData;
.source "LoggerData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/log/LoggerData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecOff"
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/sec/android/fota/common/log/LoggerData$SecOff;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/fota/common/log/LogLineInfo;->excludeClass([Ljava/lang/Class;)V

    .line 83
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/sec/android/fota/common/log/LoggerData;-><init>(Ljava/lang/String;)V

    .line 87
    return-void
.end method


# virtual methods
.method public println(ILjava/lang/String;)V
    .locals 0
    .param p1, "priority"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 91
    packed-switch p1, :pswitch_data_0

    .line 101
    invoke-super {p0, p1, p2}, Lcom/sec/android/fota/common/log/LoggerData;->println(ILjava/lang/String;)V

    .line 104
    :pswitch_0
    return-void

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
