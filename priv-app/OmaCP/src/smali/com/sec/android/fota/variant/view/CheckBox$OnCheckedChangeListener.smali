.class public abstract Lcom/sec/android/fota/variant/view/CheckBox$OnCheckedChangeListener;
.super Ljava/lang/Object;
.source "CheckBox.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/variant/view/CheckBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnCheckedChangeListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/fota/variant/view/CheckBox;

    .end local p1    # "buttonView":Landroid/widget/CompoundButton;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/fota/variant/view/CheckBox$OnCheckedChangeListener;->onCheckedChanged(Lcom/sec/android/fota/variant/view/CheckBox;Z)V

    .line 18
    return-void
.end method

.method public abstract onCheckedChanged(Lcom/sec/android/fota/variant/view/CheckBox;Z)V
.end method
