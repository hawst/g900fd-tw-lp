.class Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;
.super Ljava/lang/Object;
.source "CustomerFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/feature/CustomerFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CustomerFeatureBase"
.end annotation


# static fields
.field private static final CUSTOMER_XML:Ljava/lang/String; = "/system/csc/customer.xml"

.field protected static sInstance:Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;


# instance fields
.field protected mCustomerList:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->mCustomerList:Ljava/util/Hashtable;

    .line 108
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->loadCustomerFile()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v1, v0}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    invoke-direct {v0}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;-><init>()V

    sput-object v0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    .line 103
    :cond_0
    sget-object v0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->sInstance:Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;

    return-object v0
.end method

.method private loadCustomerFile()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 124
    const/4 v6, 0x0

    .line 125
    .local v6, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v5, 0x0

    .line 126
    .local v5, "fi":Ljava/io/InputStream;
    const/4 v3, -0x1

    .line 127
    .local v3, "eventType":I
    const/4 v0, 0x0

    .line 128
    .local v0, "TagName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 130
    .local v1, "TagValue":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->mCustomerList:Ljava/util/Hashtable;

    invoke-virtual {v7}, Ljava/util/Hashtable;->clear()V

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 133
    if-nez v5, :cond_1

    .line 170
    if-eqz v5, :cond_0

    .line 171
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 172
    const/4 v5, 0x0

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v2

    .line 175
    .local v2, "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 136
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 137
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 138
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 139
    const/4 v7, 0x0

    invoke-interface {v6, v5, v7}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 140
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 142
    :goto_1
    if-eq v3, v9, :cond_5

    .line 143
    const/4 v7, 0x2

    if-ne v3, v7, :cond_3

    .line 144
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 159
    :cond_2
    :goto_2
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_1

    .line 145
    :cond_3
    const/4 v7, 0x4

    if-ne v3, v7, :cond_2

    .line 146
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    .line 147
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 148
    iget-object v7, p0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->mCustomerList:Ljava/util/Hashtable;

    invoke-virtual {v7, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v7

    if-eqz v7, :cond_4

    .line 150
    :try_start_3
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    goto :goto_1

    .line 151
    :catch_1
    move-exception v2

    .line 152
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_4
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 162
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catch_2
    move-exception v2

    .line 163
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_5
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 170
    if-eqz v5, :cond_0

    .line 171
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 172
    const/4 v5, 0x0

    goto :goto_0

    .line 156
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_4
    :try_start_7
    iget-object v7, p0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->mCustomerList:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 164
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catch_3
    move-exception v2

    .line 165
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_8
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 170
    if-eqz v5, :cond_0

    .line 171
    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 172
    const/4 v5, 0x0

    goto :goto_0

    .line 170
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_5
    if-eqz v5, :cond_0

    .line 171
    :try_start_a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 172
    const/4 v5, 0x0

    goto :goto_0

    .line 174
    :catch_4
    move-exception v2

    .line 175
    .local v2, "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 174
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_5
    move-exception v2

    .line 175
    .local v2, "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 174
    .local v2, "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v2

    .line 175
    .local v2, "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 166
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v2

    .line 167
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_b
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 170
    if-eqz v5, :cond_0

    .line 171
    :try_start_c
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 172
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 174
    :catch_8
    move-exception v2

    .line 175
    sget-object v7, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v7, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 169
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 170
    if-eqz v5, :cond_6

    .line 171
    :try_start_d
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 172
    const/4 v5, 0x0

    .line 176
    :cond_6
    :goto_3
    throw v7

    .line 174
    :catch_9
    move-exception v2

    .line 175
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/fota/common/Log;->DATA:Lcom/sec/android/fota/common/log/LoggerData;

    invoke-virtual {v8, v2}, Lcom/sec/android/fota/common/log/LoggerData;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_3
.end method


# virtual methods
.method protected getInputStream()Ljava/io/InputStream;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 182
    new-instance v0, Ljava/io/File;

    const-string v2, "/system/csc/customer.xml"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .local v0, "customerXmlFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 184
    :cond_0
    const/4 v1, 0x0

    .line 187
    :goto_0
    return-object v1

    .line 186
    :cond_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 187
    .local v1, "fi":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->mCustomerList:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/fota/common/feature/CustomerFeature$CustomerFeatureBase;->mCustomerList:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 118
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
