.class public Lcom/sec/android/fota/common/log/LoggerCollection;
.super Lcom/sec/android/fota/common/log/Logger$Impl;
.source "LoggerCollection.java"

# interfaces
.implements Lcom/sec/android/fota/common/log/Logger$Core;
.implements Lcom/sec/android/fota/common/log/Logger;


# instance fields
.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/fota/common/log/Logger$Core;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/sec/android/fota/common/log/LoggerCollection;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/fota/common/log/LogLineInfo;->excludeClass([Ljava/lang/Class;)V

    .line 11
    return-void
.end method

.method public varargs constructor <init>([Lcom/sec/android/fota/common/log/Logger$Core;)V
    .locals 5
    .param p1, "array"    # [Lcom/sec/android/fota/common/log/Logger$Core;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/fota/common/log/Logger$Impl;-><init>()V

    .line 16
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/fota/common/log/LoggerCollection;->list:Ljava/util/List;

    .line 17
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/fota/common/log/Logger$Core;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 18
    .local v3, "logger":Lcom/sec/android/fota/common/log/Logger$Core;
    iget-object v4, p0, Lcom/sec/android/fota/common/log/LoggerCollection;->list:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 19
    .end local v3    # "logger":Lcom/sec/android/fota/common/log/Logger$Core;
    :cond_0
    return-void
.end method


# virtual methods
.method public println(ILjava/lang/String;)V
    .locals 3
    .param p1, "priority"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 23
    iget-object v2, p0, Lcom/sec/android/fota/common/log/LoggerCollection;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/fota/common/log/Logger$Core;

    .line 24
    .local v1, "logger":Lcom/sec/android/fota/common/log/Logger$Core;
    invoke-interface {v1, p1, p2}, Lcom/sec/android/fota/common/log/Logger$Core;->println(ILjava/lang/String;)V

    goto :goto_0

    .line 25
    .end local v1    # "logger":Lcom/sec/android/fota/common/log/Logger$Core;
    :cond_0
    return-void
.end method
