.class public Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;
.super Ljava/lang/Object;
.source "FotaTelephonyManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDataPhoneId()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getDefaultPhoneId(I)I

    move-result v0

    return v0
.end method

.method public static getDataState()I
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getDataState(I)I

    move-result v0

    return v0
.end method

.method public static getDataSubId()J
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getDefaultSubId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getNetworkOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getNetworkOperator(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNetworkType()I
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getNetworkType(I)I

    move-result v0

    return v0
.end method

.method public static getPhoneId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 20
    invoke-static {p0, p1}, Lcom/samsung/android/telephony/MultiSimManager;->getPhoneId(J)I

    move-result v0

    return v0
.end method

.method public static getSimCardIcon(Landroid/content/Context;I)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "slotId"    # I

    .prologue
    .line 158
    invoke-static {p0, p1}, Lcom/samsung/android/telephony/MultiSimManager;->getSimIcon(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static getSimOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getSimOperator(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSimOperator(I)Ljava/lang/String;
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 167
    invoke-static {p0}, Lcom/samsung/android/telephony/MultiSimManager;->getSimOperator(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSimSlotCount()I
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v0

    return v0
.end method

.method public static getSimState()I
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getSimState(I)I

    move-result v0

    return v0
.end method

.method public static getSubId(I)J
    .locals 2
    .param p0, "slotId"    # I

    .prologue
    .line 29
    invoke-static {p0}, Lcom/samsung/android/telephony/MultiSimManager;->getSubId(I)[J

    move-result-object v0

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public static getSubscriberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSubscriberId(I)Ljava/lang/String;
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 176
    invoke-static {p0}, Lcom/samsung/android/telephony/MultiSimManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUniqueDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getDeviceId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUniquePhoneType()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getCurrentPhoneType(I)I

    move-result v0

    return v0
.end method

.method public static isNetworkRoaming()Z
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->isNetworkRoaming(I)Z

    move-result v0

    return v0
.end method
