.class public abstract Lcom/sec/android/fota/common/log/Logger$Impl;
.super Ljava/lang/Object;
.source "Logger.java"

# interfaces
.implements Lcom/sec/android/fota/common/log/Logger$Core;
.implements Lcom/sec/android/fota/common/log/Logger;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/fota/common/log/Logger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Impl"
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/sec/android/fota/common/log/Logger$Impl;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/fota/common/log/LogLineInfo;->excludeClass([Ljava/lang/Class;)V

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public D(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 95
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/fota/common/log/Logger$Impl;->println(ILjava/lang/String;)V

    .line 96
    return-void
.end method

.method public E(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 110
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/fota/common/log/Logger$Impl;->println(ILjava/lang/String;)V

    .line 111
    return-void
.end method

.method public H(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/fota/common/log/Logger$Impl;->println(ILjava/lang/String;)V

    .line 86
    return-void
.end method

.method public I(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 100
    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/fota/common/log/Logger$Impl;->println(ILjava/lang/String;)V

    .line 101
    return-void
.end method

.method public V(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 90
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/fota/common/log/Logger$Impl;->println(ILjava/lang/String;)V

    .line 91
    return-void
.end method

.method public W(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 105
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/fota/common/log/Logger$Impl;->println(ILjava/lang/String;)V

    .line 106
    return-void
.end method

.method public printStackTrace(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 115
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 116
    .local v0, "string":Ljava/io/StringWriter;
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 118
    .local v1, "writer":Ljava/io/PrintWriter;
    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 119
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 123
    return-void

    .line 121
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    throw v2
.end method
