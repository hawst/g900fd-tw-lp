.class public Lcom/sec/android/omacp/carrier/CarrierInfo;
.super Ljava/lang/Object;
.source "CarrierInfo.java"


# instance fields
.field private index:I

.field private slotId:I

.field private subId:J

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v2, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->index:I

    .line 18
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->subId:J

    .line 19
    iput v2, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->slotId:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->type:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "subId"    # J
    .param p4, "type"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->index:I

    .line 25
    iput-wide p2, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->subId:J

    .line 26
    invoke-static {p2, p3}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getPhoneId(J)I

    move-result v0

    iput v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->slotId:I

    .line 27
    iput-object p4, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->type:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->index:I

    return v0
.end method

.method public getSlotId()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->slotId:I

    return v0
.end method

.method public getSubId()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->subId:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->type:Ljava/lang/String;

    return-object v0
.end method

.method public isExist()Z
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->index:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->index:I

    .line 40
    return-void
.end method

.method public setPhoneId(J)V
    .locals 1
    .param p1, "subId"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->subId:J

    .line 48
    invoke-static {p1, p2}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getPhoneId(J)I

    move-result v0

    iput v0, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->slotId:I

    .line 49
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/omacp/carrier/CarrierInfo;->type:Ljava/lang/String;

    .line 61
    return-void
.end method
