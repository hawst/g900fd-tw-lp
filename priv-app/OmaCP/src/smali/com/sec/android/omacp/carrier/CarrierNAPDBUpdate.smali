.class public Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;
.super Lcom/sec/android/omacp/carrier/CarrierDBUpdate;
.source "CarrierNAPDBUpdate.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected calcOrginCarrierType(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->getOppositeType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected calcTargetCarrierType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->getOppositeType()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/wsomacp/eng/core/XCPUtil;->xcpRemoveStringAndComma(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getCarrierType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    const-string v0, "default"

    return-object v0
.end method

.method protected getContentValues(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "subId"    # J
    .param p3, "dataPXLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p4, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p5, "type"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Lcom/sec/android/omacp/carrier/CarrierContentValues;

    invoke-direct {v0}, Lcom/sec/android/omacp/carrier/CarrierContentValues;-><init>()V

    .line 71
    .local v0, "values":Lcom/sec/android/omacp/carrier/CarrierContentValues;
    invoke-virtual {v0, p3}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putLOGICALValues(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;)V

    .line 72
    invoke-virtual {v0, p4}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putNAPDEFValues(Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->isExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putNumericValues(J)V

    .line 75
    invoke-virtual {v0, p5}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putTypeValues(Ljava/lang/String;)V

    .line 77
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    return-object v1
.end method

.method protected getCurrentCarrierInfo(JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;
    .locals 5
    .param p1, "subId"    # J
    .param p3, "selection"    # Ljava/lang/String;

    .prologue
    .line 33
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Find Prefer APN Carrier"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->context:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferCarrierInfo(Landroid/content/Context;J)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    .line 35
    .local v0, "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->containOppositeCarrierType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Prefer APN is not available to Update. Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 39
    const/4 v0, 0x0

    .line 43
    :cond_0
    if-nez v0, :cond_1

    .line 44
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Can not found Perfer APN Carrier Info!!"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 45
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->context:Landroid/content/Context;

    invoke-static {v1, p1, p2, p3}, Lcom/sec/android/omacp/carrier/Carrier;->getCarrierInfo(Landroid/content/Context;JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    .line 47
    :cond_1
    return-object v0
.end method

.method protected getOppositeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const-string v0, "mms"

    return-object v0
.end method

.method protected updatePreferApn()V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getSubId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getIndex()I

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/sec/android/omacp/carrier/Carrier;->updatePreferApnDB(Landroid/content/Context;JI)I

    .line 87
    return-void
.end method
