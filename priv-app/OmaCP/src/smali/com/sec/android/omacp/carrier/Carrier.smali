.class public Lcom/sec/android/omacp/carrier/Carrier;
.super Ljava/lang/Object;
.source "Carrier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/omacp/carrier/Carrier$CarrierKey;
    }
.end annotation


# static fields
.field public static final AND:Ljava/lang/String; = " and "

.field public static final APN_URI:Landroid/net/Uri;

.field public static final PHONE_SLOT2:I = 0x1

.field public static final PREFERAPN_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "content://telephony/carriers/preferapn/subId/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/omacp/carrier/Carrier;->PREFERAPN_URI:Landroid/net/Uri;

    .line 22
    const-string v0, "content://telephony/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static createApnEqualSelection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "apn"    # Ljava/lang/String;

    .prologue
    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "apn=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createCurrentSelection(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # J

    .prologue
    const/4 v2, 0x0

    .line 303
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 304
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 306
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const/4 v0, 0x1

    sget-object v1, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getPhoneId(J)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6}, Lcom/sec/android/omacp/carrier/Carrier;->getCurrentColumnForPhoneSlot2(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 311
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 314
    :goto_0
    return-object v0

    .line 311
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 314
    :cond_1
    const-string v0, "current=1"

    goto :goto_0

    .line 311
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static createIndexNotEqualSelection(I)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # I

    .prologue
    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createNameEqualSelection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createNumericEqualSelection(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "numeric="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v1, p0, p1}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getSimOperator(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createTypeLikeSelection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createTypeNotLikeSelection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type not like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCarrierInfo(Landroid/content/Context;JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # J
    .param p3, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 219
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    move-object v3, p3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 221
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 222
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Cursor is null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 239
    :goto_0
    return-object v2

    .line 227
    :cond_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 229
    .local v9, "index":I
    const-string v0, "type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 231
    .local v10, "type":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-direct {v6, v9, p1, p2, v10}, Lcom/sec/android/omacp/carrier/CarrierInfo;-><init>(IJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    .local v6, "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v2, v6

    goto :goto_0

    .end local v6    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    .end local v9    # "index":I
    .end local v10    # "type":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 234
    :catch_0
    move-exception v8

    .line 235
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-static {v8}, Lcom/sec/android/fota/common/Log;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getCarrierInfoList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/omacp/carrier/CarrierInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 192
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 194
    .local v6, "CarrierInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/omacp/carrier/CarrierInfo;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 196
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 197
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Cursor is null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 215
    :goto_0
    return-object v2

    .line 202
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 204
    .local v10, "index":I
    const-string v0, "sub_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 205
    .local v12, "subId":J
    const-string v0, "type"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 207
    .local v11, "type":Ljava/lang/String;
    new-instance v7, Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-direct {v7, v10, v12, v13, v11}, Lcom/sec/android/omacp/carrier/CarrierInfo;-><init>(IJLjava/lang/String;)V

    .line 208
    .local v7, "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 210
    .end local v7    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    .end local v10    # "index":I
    .end local v11    # "type":Ljava/lang/String;
    .end local v12    # "subId":J
    :catch_0
    move-exception v9

    .line 211
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-static {v9}, Lcom/sec/android/fota/common/Log;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :goto_2
    move-object v2, v6

    .line 215
    goto :goto_0

    .line 213
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getCurrentColumnForPhoneSlot2(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 6
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 288
    const/4 v4, 0x3

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "current1"

    aput-object v5, v0, v4

    const/4 v4, 0x1

    const-string v5, "current2"

    aput-object v5, v0, v4

    const/4 v4, 0x2

    const-string v5, "current_1"

    aput-object v5, v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 291
    .local v1, "columnName":Ljava/lang/String;
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_0

    .line 293
    .end local v1    # "columnName":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 288
    .restart local v1    # "columnName":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 293
    .end local v1    # "columnName":Ljava/lang/String;
    :cond_1
    const-string v1, "invalidColumnName"

    goto :goto_1
.end method

.method public static getDataPreferApnURI()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 157
    sget-object v0, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v0}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getDataSubId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferApnURI(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getPreferApnURI(J)Landroid/net/Uri;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 153
    sget-object v0, Lcom/sec/android/omacp/carrier/Carrier;->PREFERAPN_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getPreferCarrierInfo(Landroid/content/Context;J)Lcom/sec/android/omacp/carrier/CarrierInfo;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # J

    .prologue
    const/4 v2, 0x0

    .line 128
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferApnURI(J)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "name ASC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 130
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 131
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Cursor is null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 149
    :goto_0
    return-object v2

    .line 136
    :cond_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 138
    .local v9, "index":I
    const-string v0, "type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 140
    .local v10, "type":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-direct {v6, v9, p1, p2, v10}, Lcom/sec/android/omacp/carrier/CarrierInfo;-><init>(IJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    .local v6, "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v2, v6

    goto :goto_0

    .end local v6    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    .end local v9    # "index":I
    .end local v10    # "type":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 143
    :catch_0
    move-exception v8

    .line 144
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-static {v8}, Lcom/sec/android/fota/common/Log;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static insertCarrierDB(Landroid/content/Context;Landroid/content/ContentValues;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 262
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, p1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 263
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 264
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 266
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 270
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 274
    :goto_0
    return v0

    .line 270
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 273
    :cond_1
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Fail to insert carrier to DB"

    invoke-virtual {v0, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 274
    const/4 v0, -0x1

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static updateCarrierDB(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 251
    sget-object v1, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 252
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, p2, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static updateDataPreferApnDB(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "index"    # I

    .prologue
    .line 183
    sget-object v0, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v0}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getDataSubId()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p1}, Lcom/sec/android/omacp/carrier/Carrier;->updatePreferApnDB(Landroid/content/Context;JI)I

    move-result v0

    return v0
.end method

.method public static updatePreferApnDB(Landroid/content/Context;JI)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # J
    .param p3, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 169
    invoke-static {p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferApnURI(J)Landroid/net/Uri;

    move-result-object v0

    .line 170
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 171
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "apn_id"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 172
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method


# virtual methods
.method public getCarrierKey(Landroid/content/Context;J)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "subId"    # J

    .prologue
    const/4 v2, 0x0

    .line 100
    const-string v6, ""

    .line 101
    .local v6, "key":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferApnURI(J)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "name ASC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 103
    .local v7, "preferCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 104
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Preferred Cursor count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 105
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 107
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Preferred Cursor key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 111
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 114
    :cond_0
    return-object v6

    .line 109
    :cond_1
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Preferred APN does not set up"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDataPreferedCarrierKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 65
    const-string v6, ""

    .line 66
    .local v6, "key":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v1}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getDataSubId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferApnURI(J)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "name ASC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 69
    .local v8, "preferCursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 70
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preferred Cursor count is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 71
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 73
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Preferred Cursor key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 93
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_0
    return-object v6

    .line 75
    :cond_1
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Preferred APN does not set up"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/omacp/carrier/Carrier;->APN_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v4}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getDataSubId()J

    move-result-wide v4

    invoke-static {p1, v4, v5}, Lcom/sec/android/omacp/carrier/Carrier;->createCurrentSelection(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "and type like \'%default%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 82
    .local v7, "phoneDBCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    .line 83
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneDB Cursor count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 84
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 86
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneDB Cursor key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 88
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 90
    :cond_3
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "PhoneDB does not set up"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto :goto_0
.end method
