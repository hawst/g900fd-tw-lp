.class public Lcom/sec/android/omacp/log/FileLog;
.super Ljava/lang/Object;
.source "FileLog.java"


# static fields
.field public static final DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

.field public static final LOGFILE_NAME:Ljava/lang/String; = "omacp%d.log"

.field public static final LOGFILE_PATH:Ljava/lang/String; = "log"

.field public static final TAG_NAME:Ljava/lang/String; = "OMACP"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    invoke-static {}, Lcom/wsomacp/eng/core/XCPTrace;->xcpGetPrivateLogStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    sget-object v0, Lcom/sec/android/fota/common/Log;->ALL:Lcom/sec/android/fota/common/log/LoggerCollection;

    sput-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    .line 32
    :goto_0
    return-void

    .line 22
    :cond_0
    new-instance v0, Lcom/sec/android/omacp/log/FileLog$1;

    invoke-direct {v0}, Lcom/sec/android/omacp/log/FileLog$1;-><init>()V

    sput-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/fota/common/log/LogLineInfo;->excludeClass([Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
