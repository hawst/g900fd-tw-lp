.class public Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;
.super Lcom/sec/android/omacp/carrier/CarrierDBUpdate;
.source "CarrierMMSCDBUpdate.java"


# instance fields
.field protected mmsProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;-><init>(Landroid/content/Context;)V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->mmsProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .line 19
    return-void
.end method


# virtual methods
.method protected calcOrginCarrierType(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/wsomacp/eng/core/XCPUtil;->xcpRemoveStringAndComma(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected calcTargetCarrierType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getCarrierType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string v0, "mms"

    return-object v0
.end method

.method protected getContentValues(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "subId"    # J
    .param p3, "dataPXLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p4, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p5, "type"    # Ljava/lang/String;

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/omacp/carrier/CarrierContentValues;

    invoke-direct {v0}, Lcom/sec/android/omacp/carrier/CarrierContentValues;-><init>()V

    .line 65
    .local v0, "values":Lcom/sec/android/omacp/carrier/CarrierContentValues;
    invoke-virtual {v0, p4}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putNAPDEFValues(Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V

    .line 66
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->mmsProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putMMSCValues(Lcom/wsomacp/eng/core/XCPMMSProfileInfo;)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->isExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putNumericValues(J)V

    .line 69
    invoke-virtual {v0, p5}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->putTypeValues(Ljava/lang/String;)V

    .line 71
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    return-object v1
.end method

.method protected getCurrentCarrierInfo(JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;
    .locals 3
    .param p1, "subId"    # J
    .param p3, "selection"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Find Carrier"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->context:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/omacp/carrier/Carrier;->getCarrierInfo(Landroid/content/Context;JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    return-object v0
.end method

.method protected getOppositeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "default"

    return-object v0
.end method

.method public setMMSProfile(Lcom/wsomacp/eng/core/XCPMMSProfileInfo;)V
    .locals 0
    .param p1, "profile"    # Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->mmsProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .line 33
    return-void
.end method

.method protected updatePreferApn()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method
