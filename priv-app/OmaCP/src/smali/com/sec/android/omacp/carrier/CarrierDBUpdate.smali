.class public abstract Lcom/sec/android/omacp/carrier/CarrierDBUpdate;
.super Ljava/lang/Object;
.source "CarrierDBUpdate.java"


# static fields
.field public static final CARRIER_TYPE_DEFAULT:Ljava/lang/String; = "default"

.field public static final CARRIER_TYPE_MMS:Ljava/lang/String; = "mms"


# instance fields
.field protected context:Landroid/content/Context;

.field protected targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    .line 26
    iput-object p1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private createSelectionByApn(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;
    .locals 5
    .param p1, "subId"    # J
    .param p3, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->createNumericEqualSelection(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createApnEqualSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createTypeLikeSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 110
    return-object v0
.end method

.method private createSelectionByCurrentExceptTarget(IJLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;
    .locals 4
    .param p1, "id"    # I
    .param p2, "subId"    # J
    .param p4, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/omacp/carrier/Carrier;->createIndexNotEqualSelection(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2, p3}, Lcom/sec/android/omacp/carrier/Carrier;->createNumericEqualSelection(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createTypeLikeSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-static {v2, p2, p3}, Lcom/sec/android/omacp/carrier/Carrier;->createCurrentSelection(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 136
    return-object v0
.end method

.method private createSelectionByNameNumeric(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;
    .locals 5
    .param p1, "subId"    # J
    .param p3, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p3, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createNameEqualSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->createNumericEqualSelection(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createTypeLikeSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 102
    return-object v0
.end method

.method private createSelectionMultipleApnByCurrent(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;
    .locals 5
    .param p1, "subId"    # J
    .param p3, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->createNumericEqualSelection(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getOppositeType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createTypeNotLikeSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createTypeLikeSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->createCurrentSelection(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 119
    return-object v0
.end method

.method private createSelectionSingleApnByCurrent(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;
    .locals 5
    .param p1, "subId"    # J
    .param p3, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->createNumericEqualSelection(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/omacp/carrier/Carrier;->createTypeLikeSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->createCurrentSelection(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 127
    return-object v0
.end method

.method private getTargetCarrierInfo(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Lcom/sec/android/omacp/carrier/CarrierInfo;
    .locals 5
    .param p1, "subId"    # J
    .param p3, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_SAME_APN:Z

    if-eqz v2, :cond_0

    .line 56
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "Find Carrier which has target Name and Numeric"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 58
    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->createSelectionByNameNumeric(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p1, p2, v3}, Lcom/sec/android/omacp/carrier/Carrier;->getCarrierInfo(Landroid/content/Context;JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 92
    .end local v0    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    .local v1, "carrierInfo":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 64
    .end local v1    # "carrierInfo":Ljava/lang/Object;
    .restart local v0    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    :cond_0
    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_OVERWRITE_BY_APN:Z

    if-eqz v2, :cond_2

    .line 65
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "Find Carrier which has target Apn"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 66
    iget-object v2, p3, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 67
    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->createSelectionByApn(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p1, p2, v3}, Lcom/sec/android/omacp/carrier/Carrier;->getCarrierInfo(Landroid/content/Context;JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    .line 69
    :cond_1
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 70
    .restart local v1    # "carrierInfo":Ljava/lang/Object;
    goto :goto_0

    .line 73
    .end local v1    # "carrierInfo":Ljava/lang/Object;
    :cond_2
    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MULTI_APN:Z

    if-eqz v2, :cond_4

    .line 74
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "Find Carrier which support Mutiple Apn"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->createSelectionMultipleApnByCurrent(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCurrentCarrierInfo(JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    .line 78
    :cond_3
    if-eqz v0, :cond_6

    move-object v1, v0

    .line 79
    .restart local v1    # "carrierInfo":Ljava/lang/Object;
    goto :goto_0

    .line 81
    .end local v1    # "carrierInfo":Ljava/lang/Object;
    :cond_4
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "Find Carrier which support Single Apn"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->createSelectionSingleApnByCurrent(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCurrentCarrierInfo(JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v0

    .line 85
    :cond_5
    if-eqz v0, :cond_6

    move-object v1, v0

    .line 86
    .restart local v1    # "carrierInfo":Ljava/lang/Object;
    goto :goto_0

    .line 89
    .end local v1    # "carrierInfo":Ljava/lang/Object;
    :cond_6
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "Need to create New Carrier"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/sec/android/omacp/carrier/CarrierInfo;

    .end local v0    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    invoke-direct {v0}, Lcom/sec/android/omacp/carrier/CarrierInfo;-><init>()V

    .line 91
    .restart local v0    # "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/omacp/carrier/CarrierInfo;->setType(Ljava/lang/String;)V

    move-object v1, v0

    .line 92
    .restart local v1    # "carrierInfo":Ljava/lang/Object;
    goto :goto_0
.end method

.method private installTargetCarrier(JLandroid/content/ContentValues;)V
    .locals 5
    .param p1, "subId"    # J
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 214
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->isExist()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v2}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p3}, Lcom/sec/android/omacp/carrier/Carrier;->updateCarrierDB(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)I

    .line 216
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update Carrier: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v3}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 225
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->updatePreferApn()V

    .line 226
    return-void

    .line 218
    :cond_0
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-static {v1, p3}, Lcom/sec/android/omacp/carrier/Carrier;->insertCarrierDB(Landroid/content/Context;Landroid/content/ContentValues;)I

    move-result v0

    .line 219
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1, v0}, Lcom/sec/android/omacp/carrier/CarrierInfo;->setIndex(I)V

    .line 220
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/omacp/carrier/CarrierInfo;->setPhoneId(J)V

    .line 221
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Create new Carrier: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v3}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCarrierType(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)V
    .locals 7
    .param p1, "subId"    # J
    .param p3, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 143
    sget-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MULTI_APN:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v4}, Lcom/sec/android/omacp/carrier/CarrierInfo;->isExist()Z

    move-result v4

    if-nez v4, :cond_0

    .line 145
    iget-object v4, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-static {v4, p1, p2}, Lcom/sec/android/omacp/carrier/Carrier;->getPreferCarrierInfo(Landroid/content/Context;J)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v3

    .line 146
    .local v3, "preferCarrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    if-eqz v3, :cond_1

    .line 147
    invoke-virtual {v3}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->containMyCarrierType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->containOppositeCarrierType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 149
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "Update Prefer Carrier Info type"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, v3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->updateOriginCarrierType(Lcom/sec/android/omacp/carrier/CarrierInfo;)V

    .line 151
    invoke-direct {p0, v3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->updateTargetCarrierType(Lcom/sec/android/omacp/carrier/CarrierInfo;)V

    .line 173
    .end local v3    # "preferCarrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    :cond_0
    :goto_0
    return-void

    .line 156
    .restart local v3    # "preferCarrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-direct {p0, v5, p1, p2, p3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->createSelectionByCurrentExceptTarget(IJLcom/wsomacp/eng/parser/XCPCtxNapDef;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/omacp/carrier/Carrier;->getCarrierInfoList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 158
    .local v2, "orginCarrierInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/omacp/carrier/CarrierInfo;>;"
    if-nez v2, :cond_2

    .line 159
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "Can\'t find Carrier Info"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_2
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Find CarrierInfoList Count:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/omacp/carrier/CarrierInfo;

    .line 165
    .local v0, "carrierInfo":Lcom/sec/android/omacp/carrier/CarrierInfo;
    invoke-virtual {v0}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->containOppositeCarrierType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 166
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "Update First Current Carrier Info type"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 167
    invoke-direct {p0, v0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->updateOriginCarrierType(Lcom/sec/android/omacp/carrier/CarrierInfo;)V

    .line 168
    invoke-direct {p0, v0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->updateTargetCarrierType(Lcom/sec/android/omacp/carrier/CarrierInfo;)V

    goto :goto_0
.end method

.method private updateOriginCarrierType(Lcom/sec/android/omacp/carrier/CarrierInfo;)V
    .locals 5
    .param p1, "carrierInfo"    # Lcom/sec/android/omacp/carrier/CarrierInfo;

    .prologue
    .line 184
    invoke-virtual {p1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getIndex()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->calcOrginCarrierType(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "originType":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update Origin Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 187
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 188
    .local v1, "value":Landroid/content/ContentValues;
    const-string v2, "type"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->context:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getIndex()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/android/omacp/carrier/Carrier;->updateCarrierDB(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)I

    .line 191
    return-void
.end method

.method private updateTargetCarrierType(Lcom/sec/android/omacp/carrier/CarrierInfo;)V
    .locals 4
    .param p1, "carrierInfo"    # Lcom/sec/android/omacp/carrier/CarrierInfo;

    .prologue
    .line 194
    invoke-virtual {p1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->calcTargetCarrierType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "targetType":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Target Type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1, v0}, Lcom/sec/android/omacp/carrier/CarrierInfo;->setType(Ljava/lang/String;)V

    .line 198
    return-void
.end method


# virtual methods
.method protected abstract calcOrginCarrierType(ILjava/lang/String;)Ljava/lang/String;
.end method

.method protected abstract calcTargetCarrierType(Ljava/lang/String;)Ljava/lang/String;
.end method

.method protected containMyCarrierType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getCarrierType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected containOppositeCarrierType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getOppositeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected abstract getCarrierType()Ljava/lang/String;
.end method

.method protected abstract getContentValues(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Ljava/lang/String;)Landroid/content/ContentValues;
.end method

.method protected abstract getCurrentCarrierInfo(JLjava/lang/String;)Lcom/sec/android/omacp/carrier/CarrierInfo;
.end method

.method protected abstract getOppositeType()Ljava/lang/String;
.end method

.method public setConnection(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V
    .locals 7
    .param p1, "subId"    # J
    .param p3, "dataPXLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p4, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getTargetCarrierInfo(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)Lcom/sec/android/omacp/carrier/CarrierInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    .line 39
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->updateCarrierType(JLcom/wsomacp/eng/parser/XCPCtxNapDef;)V

    .line 42
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->targetCarrierInfo:Lcom/sec/android/omacp/carrier/CarrierInfo;

    invoke-virtual {v1}, Lcom/sec/android/omacp/carrier/CarrierInfo;->getType()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->getContentValues(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 46
    .local v0, "values":Landroid/content/ContentValues;
    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/omacp/carrier/CarrierDBUpdate;->installTargetCarrier(JLandroid/content/ContentValues;)V

    .line 48
    return-void
.end method

.method protected abstract updatePreferApn()V
.end method
