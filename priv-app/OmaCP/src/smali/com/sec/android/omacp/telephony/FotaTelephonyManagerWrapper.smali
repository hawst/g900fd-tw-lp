.class public Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;
.super Ljava/lang/Object;
.source "FotaTelephonyManagerWrapper.java"


# static fields
.field public static final PHONE_KEY:Ljava/lang/String; = "phone"

.field public static final SUBSCRIPTION_KEY:Ljava/lang/String; = "subscription"

.field public static instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-direct {v0}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;-><init>()V

    sput-object v0, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPhoneSlotId(Landroid/content/Intent;)I
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 107
    const-string v1, "phone"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 108
    .local v0, "phoneId":I
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PHONE_KEY: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 109
    return v0
.end method

.method public static getSubscriptionId(Landroid/content/Intent;)J
    .locals 6
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    const-string v2, "subscription"

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 94
    .local v0, "subId":J
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SUBSCRIPTION_KEY: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 95
    return-wide v0
.end method


# virtual methods
.method public getDataSubId()J
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getDataSubId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getPhoneId(J)I
    .locals 1
    .param p1, "subId"    # J

    .prologue
    .line 31
    invoke-static {p1, p2}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getPhoneId(J)I

    move-result v0

    return v0
.end method

.method public getSimIcon(Landroid/content/Context;I)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "slotId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 146
    invoke-static {p1, p2}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSimCardIcon(Landroid/content/Context;I)I

    move-result v0

    .line 147
    .local v0, "simIconIdx":I
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SIM SlotId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " /Icon: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 148
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 149
    const v1, 0x7f020005

    .line 151
    :goto_0
    return v1

    :cond_0
    const v1, 0x7f020004

    goto :goto_0
.end method

.method public getSimIcon(Landroid/content/Context;J)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "subId"    # J

    .prologue
    .line 72
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getPhoneId(J)I

    move-result v1

    .line 73
    .local v1, "slotId":I
    invoke-static {p1, v1}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSimCardIcon(Landroid/content/Context;I)I

    move-result v0

    .line 74
    .local v0, "simIconIdx":I
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM SlotId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /Icon: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 75
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 76
    const v2, 0x7f020005

    .line 78
    :goto_0
    return v2

    :cond_0
    const v2, 0x7f020004

    goto :goto_0
.end method

.method public getSimOperator(I)Ljava/lang/String;
    .locals 1
    .param p1, "slotId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123
    invoke-static {p1}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSimOperator(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSimOperator(J)Ljava/lang/String;
    .locals 1
    .param p1, "subId"    # J

    .prologue
    .line 51
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getPhoneId(J)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSimOperator(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubscriberId(I)Ljava/lang/String;
    .locals 1
    .param p1, "slotId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 134
    invoke-static {p1}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubscriberId(J)Ljava/lang/String;
    .locals 1
    .param p1, "subId"    # J

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getPhoneId(J)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isMultiSim()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 41
    invoke-static {}, Lcom/sec/android/fota/variant/telephony/FotaTelephonyManager;->getSimSlotCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
