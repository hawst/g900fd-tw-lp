.class public Lcom/sec/android/omacp/carrier/CarrierContentValues;
.super Ljava/lang/Object;
.source "CarrierContentValues.java"


# static fields
.field public static final DEFAULT_PROXY_ADDR:Ljava/lang/String; = "0.0.0.0"

.field public static final DEFAULT_PROXY_PORT:Ljava/lang/String; = "80"

.field public static final NET_PDP_AUTH_CHAP:I = 0x2

.field public static final NET_PDP_AUTH_NONE:I = 0x0

.field public static final NET_PDP_AUTH_NOT_SET:I = -0x1

.field public static final NET_PDP_AUTH_PAP:I = 0x1


# instance fields
.field private contentValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    .line 30
    return-void
.end method

.method private getAuthValue(Ljava/lang/String;)I
    .locals 3
    .param p1, "authType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 102
    const-string v1, "PAP"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x9a

    if-ne v1, v2, :cond_2

    .line 103
    :cond_0
    const/4 v0, 0x1

    .line 107
    :cond_1
    :goto_0
    return v0

    .line 104
    :cond_2
    const-string v1, "CHAP"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x9b

    if-ne v1, v2, :cond_1

    .line 105
    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private getProxyPortValue(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;)Ljava/lang/String;
    .locals 1
    .param p1, "dataPXLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .prologue
    .line 136
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    .line 141
    :goto_0
    return-object v0

    .line 138
    :cond_0
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    goto :goto_0

    .line 141
    :cond_1
    const-string v0, "80"

    goto :goto_0
.end method


# virtual methods
.method public getContentValues()Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    return-object v0
.end method

.method public putLOGICALValues(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;)V
    .locals 3
    .param p1, "dataPXLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-nez v0, :cond_1

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "proxy"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "port"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "proxy"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "port"

    invoke-direct {p0, p1}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->getProxyPortValue(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public putMMSCValues(Lcom/wsomacp/eng/core/XCPMMSProfileInfo;)V
    .locals 3
    .param p1, "mmsProfile"    # Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .prologue
    .line 154
    if-nez p1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsc"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsproxy"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsport"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsc"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :goto_1
    iget-object v0, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyAddr:Ljava/lang/String;

    const-string v1, "0.0.0.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsproxy"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsport"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsc"

    iget-object v2, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szAddr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 172
    :cond_3
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsproxy"

    iget-object v2, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyAddr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyPort:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 174
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsport"

    iget-object v2, p1, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyPort:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "mmsport"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public putNAPDEFValues(Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V
    .locals 3
    .param p1, "dataNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .prologue
    .line 64
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "name"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "apn"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "user"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :goto_0
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthSecret:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "password"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :goto_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 88
    :cond_4
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "authtype"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    :goto_2
    return-void

    .line 76
    :cond_5
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "user"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_6
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "password"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthSecret:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 90
    :cond_7
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "authtype"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/omacp/carrier/CarrierContentValues;->getAuthValue(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2
.end method

.method public putNumericValues(J)V
    .locals 5
    .param p1, "subId"    # J

    .prologue
    const/4 v4, 0x3

    .line 48
    sget-object v1, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getSimOperator(J)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "numeric":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 51
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v2, "sub_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 52
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v2, "mcc"

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v2, "mnc"

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v2, "numeric"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    return-void
.end method

.method public putTypeValues(Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/omacp/carrier/CarrierContentValues;->contentValues:Landroid/content/ContentValues;

    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method
