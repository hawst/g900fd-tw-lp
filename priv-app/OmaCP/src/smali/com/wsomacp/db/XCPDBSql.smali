.class public interface abstract Lcom/wsomacp/db/XCPDBSql;
.super Ljava/lang/Object;
.source "XCPDBSql.java"


# static fields
.field public static final XCP_CONTENT_URI:Landroid/net/Uri;

.field public static final XCP_DATABASE_CREATE:Ljava/lang/String; = "create table messagelist (_id integer primary key autoincrement, date text, message text not null, installed integer, type integer, simid integer, subid long )"

.field public static final XCP_DATABASE_TABLE:Ljava/lang/String; = "messagelist"

.field public static final XCP_DB_VERSION:I = 0x4

.field public static final XCP_SQL_DB_ALL:Ljava/lang/String; = "*"

.field public static final XCP_SQL_DB_KEY_DATE:Ljava/lang/String; = "date"

.field public static final XCP_SQL_DB_KEY_ID:Ljava/lang/String; = "_id"

.field public static final XCP_SQL_DB_KEY_INSTALLED:Ljava/lang/String; = "installed"

.field public static final XCP_SQL_DB_KEY_MESSAGE:Ljava/lang/String; = "message"

.field public static final XCP_SQL_DB_KEY_SIMID:Ljava/lang/String; = "simid"

.field public static final XCP_SQL_DB_KEY_SUBID:Ljava/lang/String; = "subid"

.field public static final XCP_SQL_DB_KEY_TYPE:Ljava/lang/String; = "type"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "content://com.wsomacp.messagelist"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/db/XCPDBSql;->XCP_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method
