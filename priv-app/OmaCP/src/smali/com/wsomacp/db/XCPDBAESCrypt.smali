.class public Lcom/wsomacp/db/XCPDBAESCrypt;
.super Ljava/lang/Object;
.source "XCPDBAESCrypt.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field private static final XCP_CRYPTO_KEY_ALGORITHM:Ljava/lang/String; = "AES"

.field public static final XCP_CRYPTO_KEY_SIZE:I = 0x80


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcpDecryptor([B)Ljava/lang/String;
    .locals 6
    .param p0, "decryptData"    # [B

    .prologue
    .line 32
    const-string v2, ""

    .line 33
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 36
    .local v1, "result":[B
    const/4 v4, 0x2

    :try_start_0
    invoke-static {p0, v4}, Lcom/wsomacp/db/XCPDBAESCrypt;->xcpGetCryptionResult([BI)[B

    move-result-object v1

    .line 37
    if-eqz v1, :cond_0

    .line 38
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 44
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 40
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xcpEncryptor(Ljava/lang/String;)[B
    .locals 4
    .param p0, "szEncryptText"    # Ljava/lang/String;

    .prologue
    .line 16
    const/4 v1, 0x0

    .line 20
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/wsomacp/db/XCPDBAESCrypt;->xcpGetCryptionResult([BI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 27
    :goto_0
    return-object v1

    .line 22
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xcpGetCryptionResult([BI)[B
    .locals 6
    .param p0, "cryptionData"    # [B
    .param p1, "nCryptionMode"    # I

    .prologue
    .line 49
    const/4 v2, 0x0

    .line 50
    .local v2, "m_Cipher":Ljavax/crypto/Cipher;
    const/4 v0, 0x0

    .line 54
    .local v0, "cryptResult":[B
    :try_start_0
    const-string v3, "AES"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 55
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v4, 0x172c

    const/16 v5, 0x10

    invoke-static {v4, v5}, Lcom/wsomacp/db/XCPDBAESCrypt;->xcpMealyMachine(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const-string v5, "AES"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, p1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 56
    invoke-virtual {v2, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 58
    :catch_0
    move-exception v1

    .line 60
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xcpMealyMachine(II)Ljava/lang/String;
    .locals 14
    .param p0, "v"    # I
    .param p1, "sz"    # I

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    .line 68
    new-array v6, p1, [B

    .line 69
    .local v6, "str":[B
    const/16 v7, 0x10

    new-array v4, v7, [[I

    new-array v7, v9, [I

    fill-array-data v7, :array_0

    aput-object v7, v4, v10

    new-array v7, v9, [I

    fill-array-data v7, :array_1

    aput-object v7, v4, v11

    new-array v7, v9, [I

    fill-array-data v7, :array_2

    aput-object v7, v4, v9

    new-array v7, v9, [I

    fill-array-data v7, :array_3

    aput-object v7, v4, v12

    new-array v7, v9, [I

    fill-array-data v7, :array_4

    aput-object v7, v4, v13

    const/4 v7, 0x5

    new-array v8, v9, [I

    fill-array-data v8, :array_5

    aput-object v8, v4, v7

    const/4 v7, 0x6

    new-array v8, v9, [I

    fill-array-data v8, :array_6

    aput-object v8, v4, v7

    const/4 v7, 0x7

    new-array v8, v9, [I

    fill-array-data v8, :array_7

    aput-object v8, v4, v7

    const/16 v7, 0x8

    new-array v8, v9, [I

    fill-array-data v8, :array_8

    aput-object v8, v4, v7

    const/16 v7, 0x9

    new-array v8, v9, [I

    fill-array-data v8, :array_9

    aput-object v8, v4, v7

    const/16 v7, 0xa

    new-array v8, v9, [I

    fill-array-data v8, :array_a

    aput-object v8, v4, v7

    const/16 v7, 0xb

    new-array v8, v9, [I

    fill-array-data v8, :array_b

    aput-object v8, v4, v7

    const/16 v7, 0xc

    new-array v8, v9, [I

    fill-array-data v8, :array_c

    aput-object v8, v4, v7

    const/16 v7, 0xd

    new-array v8, v9, [I

    fill-array-data v8, :array_d

    aput-object v8, v4, v7

    const/16 v7, 0xe

    new-array v8, v9, [I

    fill-array-data v8, :array_e

    aput-object v8, v4, v7

    const/16 v7, 0xf

    new-array v8, v9, [I

    fill-array-data v8, :array_f

    aput-object v8, v4, v7

    .line 87
    .local v4, "next":[[I
    const/16 v7, 0x10

    new-array v5, v7, [[C

    new-array v7, v9, [C

    fill-array-data v7, :array_10

    aput-object v7, v5, v10

    new-array v7, v9, [C

    fill-array-data v7, :array_11

    aput-object v7, v5, v11

    new-array v7, v9, [C

    fill-array-data v7, :array_12

    aput-object v7, v5, v9

    new-array v7, v9, [C

    fill-array-data v7, :array_13

    aput-object v7, v5, v12

    new-array v7, v9, [C

    fill-array-data v7, :array_14

    aput-object v7, v5, v13

    const/4 v7, 0x5

    new-array v8, v9, [C

    fill-array-data v8, :array_15

    aput-object v8, v5, v7

    const/4 v7, 0x6

    new-array v8, v9, [C

    fill-array-data v8, :array_16

    aput-object v8, v5, v7

    const/4 v7, 0x7

    new-array v8, v9, [C

    fill-array-data v8, :array_17

    aput-object v8, v5, v7

    const/16 v7, 0x8

    new-array v8, v9, [C

    fill-array-data v8, :array_18

    aput-object v8, v5, v7

    const/16 v7, 0x9

    new-array v8, v9, [C

    fill-array-data v8, :array_19

    aput-object v8, v5, v7

    const/16 v7, 0xa

    new-array v8, v9, [C

    fill-array-data v8, :array_1a

    aput-object v8, v5, v7

    const/16 v7, 0xb

    new-array v8, v9, [C

    fill-array-data v8, :array_1b

    aput-object v8, v5, v7

    const/16 v7, 0xc

    new-array v8, v9, [C

    fill-array-data v8, :array_1c

    aput-object v8, v5, v7

    const/16 v7, 0xd

    new-array v8, v9, [C

    fill-array-data v8, :array_1d

    aput-object v8, v5, v7

    const/16 v7, 0xe

    new-array v8, v9, [C

    fill-array-data v8, :array_1e

    aput-object v8, v5, v7

    const/16 v7, 0xf

    new-array v8, v9, [C

    fill-array-data v8, :array_1f

    aput-object v8, v5, v7

    .line 106
    .local v5, "out_char":[[C
    const/4 v3, 0x0

    .line 107
    .local v3, "nState":I
    const/4 v1, 0x0

    .local v1, "nLen":I
    move v2, v1

    .line 109
    .end local v1    # "nLen":I
    .local v2, "nLen":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 111
    and-int/lit8 v0, p0, 0x1

    .line 112
    .local v0, "nInput":I
    shr-int/lit8 p0, p0, 0x1

    .line 113
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "nLen":I
    .restart local v1    # "nLen":I
    aget-object v7, v5, v3

    aget-char v7, v7, v0

    int-to-byte v7, v7

    aput-byte v7, v6, v2

    .line 114
    aget-object v7, v4, v3

    aget v3, v7, v0

    move v2, v1

    .line 115
    .end local v1    # "nLen":I
    .restart local v2    # "nLen":I
    goto :goto_0

    .line 117
    .end local v0    # "nInput":I
    :cond_0
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>([B)V

    return-object v7

    .line 69
    nop

    :array_0
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x4
    .end array-data

    :array_2
    .array-data 4
        0x8
        0xf
    .end array-data

    :array_3
    .array-data 4
        0xb
        0x2
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3
    .end array-data

    :array_5
    .array-data 4
        0x9
        0x0
    .end array-data

    :array_6
    .array-data 4
        0xf
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_b
    .array-data 4
        0x1
        0x6
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x3
        0xd
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x2
        0xd
    .end array-data

    .line 87
    :array_10
    .array-data 2
        0x73s
        0x33s
    .end array-data

    :array_11
    .array-data 2
        0x76s
        0x6es
    .end array-data

    :array_12
    .array-data 2
        0x31s
        0x39s
    .end array-data

    :array_13
    .array-data 2
        0x6ds
        0x30s
    .end array-data

    :array_14
    .array-data 2
        0x65s
        0x63s
    .end array-data

    :array_15
    .array-data 2
        0x33s
        0x42s
    .end array-data

    :array_16
    .array-data 2
        0x37s
        0x4es
    .end array-data

    :array_17
    .array-data 2
        0x6bs
        0x32s
    .end array-data

    :array_18
    .array-data 2
        0x32s
        0x43s
    .end array-data

    :array_19
    .array-data 2
        0x61s
        0x43s
    .end array-data

    :array_1a
    .array-data 2
        0x4as
        0x32s
    .end array-data

    :array_1b
    .array-data 2
        0x79s
        0x6cs
    .end array-data

    :array_1c
    .array-data 2
        0x38s
        0x64s
    .end array-data

    :array_1d
    .array-data 2
        0x31s
        0x30s
    .end array-data

    :array_1e
    .array-data 2
        0x41s
        0x5es
    .end array-data

    :array_1f
    .array-data 2
        0x37s
        0x30s
    .end array-data
.end method
