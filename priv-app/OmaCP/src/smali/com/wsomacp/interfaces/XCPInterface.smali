.class public interface abstract Lcom/wsomacp/interfaces/XCPInterface;
.super Ljava/lang/Object;
.source "XCPInterface.java"


# static fields
.field public static final XCP_ACTION_BROADCAST_BOOKMARK:Ljava/lang/String; = "android.intent.action.OMACP_BROWSER_SET_BOOKMARK"

.field public static final XCP_ACTION_BROADCAST_BROWSER:Ljava/lang/String; = "android.intent.action.OMADM_BROWSER_SET_HOMEPAGE"

.field public static final XCP_ACTION_BROADCAST_DM:Ljava/lang/String; = "android.intent.action.OMACP_DM_SET"

.field public static final XCP_ACTION_BROADCAST_DM_BOOTSTRAP:Ljava/lang/String; = "android.provider.Telephony.WAP_PUSH_DM_RECEIVED"

.field public static final XCP_ACTION_BROADCAST_DM_NGSM:Ljava/lang/String; = "android.intent.action.SET_DM"

.field public static final XCP_ACTION_BROADCAST_DS:Ljava/lang/String; = "android.intent.action.SET_DS"

.field public static final XCP_ACTION_BROADCAST_DS_MYPHONEBOOK:Ljava/lang/String; = "android.intent.action.SET_DS_MYPHONEBOOK"

.field public static final XCP_ACTION_BROADCAST_EMAIL:Ljava/lang/String; = "android.intent.action.SET_RECV_HOST"

.field public static final XCP_ACTION_BROADCAST_SET_CP:Ljava/lang/String; = "android.intent.action.SET_CP"

.field public static final XCP_ACTION_BROADCAST_XCAP:Ljava/lang/String; = "com.sec.android.widgetapp.community.cpmp.CMT_CPMPService.mBroadcastReceiver"

.field public static final XCP_ADAPTER_DELETE:I = 0x2

.field public static final XCP_ADAPTER_LIST:I = 0x1

.field public static final XCP_APPID_BROWSER:I = 0xe

.field public static final XCP_APPID_DM:I = 0x436

.field public static final XCP_APPID_DS:I = 0x435

.field public static final XCP_APPID_EMAIL:I = 0x37

.field public static final XCP_APPID_MMS:I = 0xf

.field public static final XCP_APPID_NAP:I = 0x1

.field public static final XCP_APPID_PROXY:I = 0x0

.field public static final XCP_APPID_STREAM:I = 0x20

.field public static final XCP_APPID_XCAP:I = 0x438

.field public static final XCP_AUTH_TYPE_BASIC:Ljava/lang/String; = "BASIC"

.field public static final XCP_AUTH_TYPE_DIGEST:Ljava/lang/String; = "DIGEST"

.field public static final XCP_AUTH_TYPE_DIGIPASS:Ljava/lang/String; = "DIGIPASS"

.field public static final XCP_AUTH_TYPE_HMAC:Ljava/lang/String; = "HMAC"

.field public static final XCP_AUTH_TYPE_NONE:Ljava/lang/String; = "NONE"

.field public static final XCP_AUTH_TYPE_SAFEWORD:Ljava/lang/String; = "SAFEWORD"

.field public static final XCP_AUTH_TYPE_SECUREID:Ljava/lang/String; = "SECUREID"

.field public static final XCP_AUTH_TYPE_X509:Ljava/lang/String; = "X509"

.field public static final XCP_BG_INTSALL_NONE:I = 0x0

.field public static final XCP_BG_INTSALL_VENDORCONFIG:I = 0x1

.field public static final XCP_BG_INTSALL_WIFISETTING:I = 0x2

.field public static final XCP_CRED_TYPE_BASIC:I = 0x0

.field public static final XCP_CRED_TYPE_DIGIPASS:I = 0x7

.field public static final XCP_CRED_TYPE_HMAC:I = 0x2

.field public static final XCP_CRED_TYPE_MD5:I = 0x1

.field public static final XCP_CRED_TYPE_MD5_NOT_BASE64:I = 0x3

.field public static final XCP_CRED_TYPE_NONE:I = -0x1

.field public static final XCP_CRED_TYPE_SAFEWORD:I = 0x6

.field public static final XCP_CRED_TYPE_SECUREID:I = 0x5

.field public static final XCP_CRED_TYPE_SHA1:I = 0x8

.field public static final XCP_CRED_TYPE_STRING_BASIC:Ljava/lang/String; = "syncml:auth-basic"

.field public static final XCP_CRED_TYPE_STRING_DIGIPASS:Ljava/lang/String; = "syncml:auth-digipass"

.field public static final XCP_CRED_TYPE_STRING_HMAC:Ljava/lang/String; = "syncml:auth-MAC"

.field public static final XCP_CRED_TYPE_STRING_MD5:Ljava/lang/String; = "syncml:auth-md5"

.field public static final XCP_CRED_TYPE_STRING_SAFEWORD:Ljava/lang/String; = "syncml:auth-safeword"

.field public static final XCP_CRED_TYPE_STRING_SECUREID:Ljava/lang/String; = "syncml:auth-securid"

.field public static final XCP_CRED_TYPE_STRING_X509:Ljava/lang/String; = "syncml:auth-X509"

.field public static final XCP_CRED_TYPE_X509:I = 0x4

.field public static final XCP_DBG:Ljava/lang/String; = "WSOMACP"

.field public static final XCP_DEFAULT_PROXY_ADDR:Ljava/lang/String; = "0.0.0.0"

.field public static final XCP_DEFAULT_PROXY_PORT:Ljava/lang/String; = "80"

.field public static final XCP_DELETE_MESSAGE_CONFIRM:I = 0x18

.field public static final XCP_DELETE_MULTIMESSAGE_CONFIRM:I = 0x19

.field public static final XCP_DIALOG_INSTALL_FAILED:I = 0x3

.field public static final XCP_DIALOG_INSTALL_NEW_ACCOUNT:I = 0x7

.field public static final XCP_DIALOG_INSTALL_NOT_SUPPORT:I = 0x8

.field public static final XCP_DIALOG_INSTALL_SUCCESS:I = 0x2

.field public static final XCP_DIALOG_PARSING_FAIL:I = 0x6

.field public static final XCP_DIALOG_PINCODE_AGAIN:I = 0x4

.field public static final XCP_DIALOG_PINCODE_FAIL:I = 0x5

.field public static final XCP_DIALOG_PINCODE_INPUT:I = 0x1

.field public static final XCP_DUAL_SIM_ID_1:I = 0x0

.field public static final XCP_EMAIL_SSL_PORT_IMAP4:I = 0x3e1

.field public static final XCP_EMAIL_SSL_PORT_POP3:I = 0x3e3

.field public static final XCP_EMAIL_SSL_PORT_SMTP:I = 0x1d1

.field public static final XCP_EMAIL_TLS_PORT_SMTP:I = 0x24b

.field public static final XCP_EMULATOR_TEST:Z = false

.field public static final XCP_ERROR_FAIL:I = 0x1

.field public static final XCP_ERROR_FILE_ALREADY_EXIST:I = 0x2

.field public static final XCP_ERROR_OK:I = 0x0

.field public static final XCP_EXCEPTION:Ljava/lang/String; = "WSOMACP"

.field public static final XCP_INDICATOR_NONE:I = 0x0

.field public static final XCP_INDICATOR_OFF:I = 0x1

.field public static final XCP_INDICATOR_ON:I = 0x2

.field public static final XCP_INSTALL_FAIL_MESSAGE:I = 0x14

.field public static final XCP_INSTALL_NEW_ACCOUNT:I = 0x16

.field public static final XCP_INSTALL_NOT_SUPPORT_MESSAGE:I = 0x17

.field public static final XCP_INSTALL_PARSER_FAIL_MESSAGE:I = 0x1a

.field public static final XCP_INSTALL_RESULT_MESSAGE:I = 0x15

.field public static final XCP_INSTALL_SUCCESS_MESSAGE:I = 0x10

.field public static final XCP_MIME_TYPE_VCALENDAR:Ljava/lang/String; = "text/x-vcalendar"

.field public static final XCP_MIME_TYPE_VCARD:Ljava/lang/String; = "text/x-vcard"

.field public static final XCP_OPERATOR_ATT:Ljava/lang/String; = "ATT"

.field public static final XCP_PINCODE_INPUNT_RETRY:I = 0x13

.field public static final XCP_PINCODE_INPUT_AGAIN:I = 0x12

.field public static final XCP_PINCODE_INPUT_MESSAGE:I = 0x11

.field public static final XCP_RESULT_ACCOUNT_NOT_SUPPORT:I = 0x7

.field public static final XCP_RESULT_APP_ACCOUNT_INSTALL_SUCCESS:I = 0x6

.field public static final XCP_RESULT_EMAIL_ACCOUNT_INSTALL:I = 0x5

.field public static final XCP_RESULT_INSTALL_FAIL:I = 0x9

.field public static final XCP_RESULT_NAP_ACCOUNT_INSTALL_MAX:I = 0x2

.field public static final XCP_RESULT_NAP_ACCOUNT_INSTALL_SUCCESS:I = 0x3

.field public static final XCP_RESULT_NAP_ACCOUNT_RENAME:I = 0x1

.field public static final XCP_RESULT_NONE:I = 0x0

.field public static final XCP_RESULT_NOT_SPECIFIED_TYPE:I = 0x5

.field public static final XCP_RESULT_PIN_CHECK_RETRY:I = 0x8

.field public static final XCP_RESULT_SYNCMLDS_ACCOUNT_INSTALL_MAX:I = 0x4

.field public static final XCP_SEC_RETURN_VALUE_NOT_SUPPORT:I = 0x0

.field public static final XCP_SEC_RETURN_VALUE_SUPPORT:I = 0x1

.field public static final XCP_SEC_RETURN_VALUE_SUPPORT_MISMATCH_SECURITY:I = 0x2

.field public static final XCP_SEC_TYPE_ALL:I = 0x1111

.field public static final XCP_SEC_TYPE_NETWPIN:I = 0x10

.field public static final XCP_SEC_TYPE_NONE:I = 0x1

.field public static final XCP_SEC_TYPE_USERNETWPIN:I = 0x1000

.field public static final XCP_SEC_TYPE_USERWPIN:I = 0x100

.field public static final XCP_STR_BROADCAST_LOCALE_CHANGED:Ljava/lang/String; = "android.intent.action.LOCALE_CHANGED"

.field public static final XCP_STR_BROADCAST_SET_LTE_MCCMNC:Ljava/lang/String; = "android.intent.action.SET_LTE_MCCMNC"

.field public static final XCP_STR_BROADCAST_SET_WIFI:Ljava/lang/String; = "android.intent.action.SET_WIFI"

.field public static final XCP_STR_BROADCAST_SMS_CP_RECEIVE:Ljava/lang/String; = "android.provider.Telephony.WAP_PUSH_CP_RECEIVED"

.field public static final XCP_STR_BROADCAST_SMS_RECEIVE:Ljava/lang/String; = "android.provider.Telephony.WAP_PUSH_RECEIVED"

.field public static final XCP_STR_BROWSER:Ljava/lang/String; = "Browser"

.field public static final XCP_STR_EMAIL:Ljava/lang/String; = "Email"

.field public static final XCP_STR_EMAIL_STARTTLS:Ljava/lang/String; = "STARTTLS"

.field public static final XCP_STR_MMS:Ljava/lang/String; = "MMS"

.field public static final XCP_STR_NAP:Ljava/lang/String; = "APN"

.field public static final XCP_STR_STREAMING:Ljava/lang/String; = "STREAMING"

.field public static final XCP_STR_SYNCML_DM:Ljava/lang/String; = "SyncML DM"

.field public static final XCP_STR_SYNCML_DS:Ljava/lang/String; = "Synchronise"

.field public static final XCP_STR_XCAP:Ljava/lang/String; = "XCAP"

.field public static final XCP_UI_MSG_DISCARD:I = -0x1

.field public static final XCP_UI_MSG_INTSALL_BY_BG:I = 0x1

.field public static final XCP_UI_MSG_SAVE:I = 0x0

.field public static final XCP_WAP_PUSH_SEC_TYPE_NETWPIN:I = 0x80

.field public static final XCP_WAP_PUSH_SEC_TYPE_USERNETWPIN:I = 0x82

.field public static final XCP_WAP_PUSH_SEC_TYPE_USERPINMAC:I = 0x83

.field public static final XCP_WAP_PUSH_SEC_TYPE_USERWPIN:I = 0x81

.field public static final XCP_WSP_CONTENT_TYPE_CP_BOOTSTRAP:I = 0x0

.field public static final XCP_WSP_CONTENT_TYPE_DM_BOOTSTRAP:I = 0x1

.field public static final XCP_WSP_CONTENT_TYPE_NO_PROCESSING:I = -0x1

.field public static final _SML_CP_LOG_ON_:Z = true
