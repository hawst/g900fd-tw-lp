.class public Lcom/wsomacp/eng/core/XCPAuthMD5;
.super Ljava/lang/Object;
.source "XCPAuthMD5.java"


# instance fields
.field m_MD5Sum:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final xcpComputeMD5Credentials(Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 9
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "nonce"    # [B

    .prologue
    .line 15
    move-object v6, p1

    .line 16
    .local v6, "szUserAndPassword":Ljava/lang/String;
    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 17
    invoke-virtual {v6, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 22
    :try_start_0
    const-string v7, "MD5"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    iput-object v7, p0, Lcom/wsomacp/eng/core/XCPAuthMD5;->m_MD5Sum:Ljava/lang/Object;

    .line 23
    iget-object v7, p0, Lcom/wsomacp/eng/core/XCPAuthMD5;->m_MD5Sum:Ljava/lang/Object;

    check-cast v7, Ljava/security/MessageDigest;

    invoke-virtual {v7}, Ljava/security/MessageDigest;->reset()V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :goto_0
    iget-object v7, p0, Lcom/wsomacp/eng/core/XCPAuthMD5;->m_MD5Sum:Ljava/lang/Object;

    check-cast v7, Ljava/security/MessageDigest;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 34
    .local v1, "digest":[B
    invoke-static {v1}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    move-result-object v3

    .line 36
    .local v3, "endoder":[B
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/lang/String;-><init>([B)V

    .line 37
    .local v5, "szDigestStr":Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p3}, Ljava/lang/String;-><init>([B)V

    .line 38
    .local v4, "non":Ljava/lang/String;
    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 39
    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 41
    iget-object v7, p0, Lcom/wsomacp/eng/core/XCPAuthMD5;->m_MD5Sum:Ljava/lang/Object;

    check-cast v7, Ljava/security/MessageDigest;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 44
    invoke-static {v1}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    move-result-object v0

    .line 45
    .local v0, "credentials":[B
    return-object v0

    .line 25
    .end local v0    # "credentials":[B
    .end local v1    # "digest":[B
    .end local v3    # "endoder":[B
    .end local v4    # "non":Ljava/lang/String;
    .end local v5    # "szDigestStr":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 27
    .local v2, "e1":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method
