.class public Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;
.super Ljava/lang/Object;
.source "XCPNVMSyncDMInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field XCP_DM_MAX_PROFILE_NAME:I

.field XCP_MAX_ADDRESS_LENGTH:I

.field XCP_MAX_ID_LENGTH:I

.field XCP_MAX_PASSWORD_LENGTH:I

.field XCP_MAX_PATH_LENGTH:I

.field XCP_MAX_PROTOCOL_LENGTH:I

.field XCP_MAX_URL_LENGTH:I

.field XCP_NETINFO_NAME_LEN_MAX:I

.field public m_CConBackup:Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;

.field public m_CConRef:Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;

.field public m_bChangedProtocol:Z

.field public m_nClientNonceFormat:I

.field public m_nMagicNumber:I

.field public m_nNetworkConnIndex:I

.field public m_nObexType:I

.field public m_nServerNonceFormat:I

.field public m_nServerPort:I

.field public m_nServerPort_Org:I

.field public m_szAppID:Ljava/lang/String;

.field public m_szAuthLevel:Ljava/lang/String;

.field public m_szAuthType:Ljava/lang/String;

.field public m_szClientNonce:Ljava/lang/String;

.field public m_szNetworkConnName:Ljava/lang/String;

.field public m_szPassword:Ljava/lang/String;

.field public m_szPath:Ljava/lang/String;

.field public m_szPath_Org:Ljava/lang/String;

.field public m_szPrefConRef:Ljava/lang/String;

.field public m_szProfileName:Ljava/lang/String;

.field public m_szProtocol:Ljava/lang/String;

.field public m_szProtocol_Org:Ljava/lang/String;

.field public m_szServerAuthLevel:Ljava/lang/String;

.field public m_szServerAuthType:Ljava/lang/String;

.field public m_szServerID:Ljava/lang/String;

.field public m_szServerIP:Ljava/lang/String;

.field public m_szServerIP_Org:Ljava/lang/String;

.field public m_szServerNonce:Ljava/lang/String;

.field public m_szServerPwd:Ljava/lang/String;

.field public m_szServerUrl:Ljava/lang/String;

.field public m_szServerUrl_Org:Ljava/lang/String;

.field public m_szUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x40

    const/4 v2, 0x0

    const/16 v1, 0x80

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/16 v0, 0x10

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_MAX_PROTOCOL_LENGTH:I

    .line 9
    const/16 v0, 0x100

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_MAX_URL_LENGTH:I

    .line 10
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_MAX_ADDRESS_LENGTH:I

    .line 11
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_MAX_PATH_LENGTH:I

    .line 12
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_MAX_ID_LENGTH:I

    .line 13
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_MAX_PASSWORD_LENGTH:I

    .line 14
    iput v3, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_DM_MAX_PROFILE_NAME:I

    .line 15
    iput v3, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->XCP_NETINFO_NAME_LEN_MAX:I

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProtocol:Ljava/lang/String;

    .line 58
    const/16 v0, 0x50

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerPort:I

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProtocol_Org:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl_Org:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP_Org:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath_Org:Ljava/lang/String;

    .line 67
    const/4 v0, 0x2

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nObexType:I

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szAuthType:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerAuthType:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szAppID:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szAuthLevel:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerAuthLevel:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPrefConRef:Ljava/lang/String;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szUserName:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPassword:Ljava/lang/String;

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerID:Ljava/lang/String;

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerPwd:Ljava/lang/String;

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szClientNonce:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerNonce:Ljava/lang/String;

    .line 82
    iput v2, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerNonceFormat:I

    .line 83
    iput v2, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nClientNonceFormat:I

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProfileName:Ljava/lang/String;

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szNetworkConnName:Ljava/lang/String;

    .line 87
    const/4 v0, 0x1

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nNetworkConnIndex:I

    .line 89
    new-instance v0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_CConRef:Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;

    .line 90
    new-instance v0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_CConBackup:Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;

    .line 91
    return-void
.end method
