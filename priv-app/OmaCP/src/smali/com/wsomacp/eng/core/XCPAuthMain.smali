.class public Lcom/wsomacp/eng/core/XCPAuthMain;
.super Ljava/lang/Object;
.source "XCPAuthMain.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field private static final XCP_SHA_KEY_PAD_LEN:I = 0x40

.field private static final XCP_SHA_KEY_PAD_LEN_:I = 0x40

.field public static g_CMD5:Lcom/wsomacp/eng/core/XCPAuthMD5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcpAuthAAuthType2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 70
    packed-switch p0, :pswitch_data_0

    .line 94
    :pswitch_0
    const-string v0, "NONE"

    :goto_0
    return-object v0

    .line 73
    :pswitch_1
    const-string v0, "BASIC"

    goto :goto_0

    .line 76
    :pswitch_2
    const-string v0, "DIGEST"

    goto :goto_0

    .line 79
    :pswitch_3
    const-string v0, "HMAC"

    goto :goto_0

    .line 82
    :pswitch_4
    const-string v0, "X509"

    goto :goto_0

    .line 85
    :pswitch_5
    const-string v0, "SECUREID"

    goto :goto_0

    .line 88
    :pswitch_6
    const-string v0, "SAFEWORD"

    goto :goto_0

    .line 91
    :pswitch_7
    const-string v0, "DIGIPASS"

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static xcpAuthAAuthtring2Type(Ljava/lang/String;)I
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 100
    const-string v0, "BASIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0

    .line 102
    :cond_0
    const-string v0, "DIGEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 103
    const/4 v0, 0x1

    goto :goto_0

    .line 104
    :cond_1
    const-string v0, "HMAC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 105
    const/4 v0, 0x2

    goto :goto_0

    .line 106
    :cond_2
    const-string v0, "X509"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 107
    const/4 v0, 0x4

    goto :goto_0

    .line 108
    :cond_3
    const-string v0, "SECUREID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 109
    const/4 v0, 0x5

    goto :goto_0

    .line 110
    :cond_4
    const-string v0, "DIGIPASS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 111
    const/4 v0, 0x7

    goto :goto_0

    .line 113
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xcpAuthCredString2Type(Ljava/lang/String;)I
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 50
    const-string v0, "syncml:auth-basic"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 65
    :goto_0
    return v0

    .line 52
    :cond_0
    const-string v0, "syncml:auth-md5"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 53
    const/4 v0, 0x1

    goto :goto_0

    .line 54
    :cond_1
    const-string v0, "syncml:auth-MAC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 55
    const/4 v0, 0x2

    goto :goto_0

    .line 56
    :cond_2
    const-string v0, "syncml:auth-X509"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 57
    const/4 v0, 0x4

    goto :goto_0

    .line 58
    :cond_3
    const-string v0, "syncml:auth-securid"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 59
    const/4 v0, 0x5

    goto :goto_0

    .line 60
    :cond_4
    const-string v0, "syncml:auth-safeword"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 61
    const/4 v0, 0x6

    goto :goto_0

    .line 62
    :cond_5
    const-string v0, "syncml:auth-digipass"

    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 63
    const/4 v0, 0x7

    goto :goto_0

    .line 65
    :cond_6
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xcpAuthCredType2String(I)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 19
    packed-switch p0, :pswitch_data_0

    .line 43
    :pswitch_0
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Not Support Auth Type"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 22
    :pswitch_1
    const-string v0, "syncml:auth-basic"

    goto :goto_0

    .line 25
    :pswitch_2
    const-string v0, "syncml:auth-md5"

    goto :goto_0

    .line 28
    :pswitch_3
    const-string v0, "syncml:auth-MAC"

    goto :goto_0

    .line 31
    :pswitch_4
    const-string v0, "syncml:auth-X509"

    goto :goto_0

    .line 34
    :pswitch_5
    const-string v0, "syncml:auth-securid"

    goto :goto_0

    .line 37
    :pswitch_6
    const-string v0, "syncml:auth-safeword"

    goto :goto_0

    .line 40
    :pswitch_7
    const-string v0, "syncml:auth-digipass"

    goto :goto_0

    .line 19
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static xcpAuthFactoryDigest([B[B)Ljava/lang/String;
    .locals 4
    .param p0, "buf"    # [B
    .param p1, "digest"    # [B

    .prologue
    .line 244
    const/4 v1, 0x0

    .line 248
    .local v1, "md5":Ljava/security/MessageDigest;
    :try_start_0
    const-string v3, "MD5"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 256
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    .line 257
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    .line 258
    :goto_0
    return-object v2

    .line 250
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 253
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xcpAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;
    .locals 18
    .param p0, "authType"    # I
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "passWord"    # Ljava/lang/String;
    .param p3, "nonce"    # [B
    .param p4, "nonceLength"    # I
    .param p5, "packetBody"    # [B
    .param p6, "bodyLength"    # I

    .prologue
    .line 118
    const/4 v14, 0x0

    .line 119
    .local v14, "szRet":Ljava/lang/String;
    const/4 v12, 0x0

    .line 121
    .local v12, "szCreddata":Ljava/lang/String;
    const/16 v15, 0x10

    new-array v3, v15, [B

    .line 123
    .local v3, "digest":[B
    packed-switch p0, :pswitch_data_0

    .line 151
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "Not Support Auth Type"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 152
    const/4 v15, 0x0

    .line 239
    :goto_0
    return-object v15

    .line 126
    :pswitch_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 128
    :cond_0
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "userName or passWord is NULL"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 129
    const/4 v15, 0x0

    goto :goto_0

    .line 134
    :pswitch_1
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    if-eqz p3, :cond_1

    if-gtz p4, :cond_3

    .line 136
    :cond_1
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "userName or passWord or nonce or nonceLength is NULL"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 137
    const/4 v15, 0x0

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    if-eqz p3, :cond_2

    if-lez p4, :cond_2

    if-eqz p5, :cond_2

    if-gtz p6, :cond_3

    .line 145
    :cond_2
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "userName or passWord or nonce or nonceLength or packetBody or bodyLength is NULL"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 146
    const/4 v15, 0x0

    goto :goto_0

    .line 155
    :cond_3
    packed-switch p0, :pswitch_data_1

    :goto_1
    move-object v15, v14

    .line 239
    goto :goto_0

    .line 158
    :pswitch_3
    move-object/from16 v12, p1

    .line 159
    const-string v15, ":"

    invoke-virtual {v12, v15}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 160
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 161
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    invoke-static {v15}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    move-result-object v3

    .line 162
    const/4 v14, 0x0

    .line 163
    new-instance v14, Ljava/lang/String;

    .end local v14    # "szRet":Ljava/lang/String;
    invoke-direct {v14, v3}, Ljava/lang/String;-><init>([B)V

    .line 165
    .restart local v14    # "szRet":Ljava/lang/String;
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "SML_CRED_TYPE_BASIC.. userName"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "passWord"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "creddata"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "ret"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 166
    const/4 v12, 0x0

    .line 167
    goto :goto_1

    .line 170
    :pswitch_4
    new-instance v8, Lcom/wsomacp/eng/core/XCPAuthMD5;

    invoke-direct {v8}, Lcom/wsomacp/eng/core/XCPAuthMD5;-><init>()V

    .line 171
    .local v8, "md5":Lcom/wsomacp/eng/core/XCPAuthMD5;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v8, v0, v1, v2}, Lcom/wsomacp/eng/core/XCPAuthMD5;->xcpComputeMD5Credentials(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v9

    .line 172
    .local v9, "md5digest":[B
    const-string v14, ""

    .line 173
    new-instance v14, Ljava/lang/String;

    .end local v14    # "szRet":Ljava/lang/String;
    invoke-direct {v14, v9}, Ljava/lang/String;-><init>([B)V

    .line 175
    .restart local v14    # "szRet":Ljava/lang/String;
    new-instance v11, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-direct {v11, v0}, Ljava/lang/String;-><init>([B)V

    .line 176
    .local v11, "non":Ljava/lang/String;
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "SML_CRED_TYPE_MD5 smlAuthMakeDigest userName= "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " passWord= "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " nonce= "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " ret= "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 178
    const/4 v12, 0x0

    .line 179
    goto/16 :goto_1

    .line 183
    .end local v8    # "md5":Lcom/wsomacp/eng/core/XCPAuthMD5;
    .end local v9    # "md5digest":[B
    .end local v11    # "non":Ljava/lang/String;
    :pswitch_5
    const/4 v10, 0x0

    .line 188
    .local v10, "msgDigest":Ljava/security/MessageDigest;
    :try_start_0
    const-string v15, "MD5"

    invoke-static {v15}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 196
    move-object/from16 v12, p1

    .line 197
    const-string v15, ":"

    invoke-virtual {v12, v15}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 198
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 199
    invoke-virtual {v10}, Ljava/security/MessageDigest;->reset()V

    .line 202
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 205
    invoke-static {v3}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    move-result-object v5

    .line 207
    .local v5, "encoder":[B
    new-instance v12, Ljava/lang/String;

    .end local v12    # "szCreddata":Ljava/lang/String;
    invoke-direct {v12, v5}, Ljava/lang/String;-><init>([B)V

    .line 209
    .restart local v12    # "szCreddata":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 211
    invoke-static {v3}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    move-result-object v6

    .line 212
    .local v6, "encoder2":[B
    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v6}, Ljava/lang/String;-><init>([B)V

    .line 216
    .local v13, "szDatabody":Ljava/lang/String;
    const-string v15, ":"

    invoke-virtual {v12, v15}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 217
    new-instance v15, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-direct {v15, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v15}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 218
    const-string v15, ":"

    invoke-virtual {v12, v15}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 219
    invoke-virtual {v12, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 220
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 221
    const/4 v15, 0x2

    move/from16 v0, p0

    if-ne v0, v15, :cond_4

    .line 224
    invoke-static {v3}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    move-result-object v7

    .line 225
    .local v7, "encoder3":[B
    const/4 v14, 0x0

    .line 226
    new-instance v14, Ljava/lang/String;

    .end local v14    # "szRet":Ljava/lang/String;
    invoke-direct {v14, v7}, Ljava/lang/String;-><init>([B)V

    .line 230
    .restart local v14    # "szRet":Ljava/lang/String;
    goto/16 :goto_1

    .line 190
    .end local v5    # "encoder":[B
    .end local v6    # "encoder2":[B
    .end local v7    # "encoder3":[B
    .end local v13    # "szDatabody":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 192
    .local v4, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 193
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 234
    .end local v4    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v5    # "encoder":[B
    .restart local v6    # "encoder2":[B
    .restart local v13    # "szDatabody":Ljava/lang/String;
    :cond_4
    new-instance v14, Ljava/lang/String;

    .end local v14    # "szRet":Ljava/lang/String;
    invoke-direct {v14, v3}, Ljava/lang/String;-><init>([B)V

    .restart local v14    # "szRet":Ljava/lang/String;
    goto/16 :goto_1

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 155
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;
    .locals 14
    .param p0, "nAuthType"    # I
    .param p1, "pszSecretKey"    # [B
    .param p2, "nSecertLen"    # I
    .param p3, "pszPacketBody"    # [B
    .param p4, "nBodyLen"    # I

    .prologue
    .line 263
    move/from16 v5, p2

    .line 264
    .local v5, "nSecretLen":I
    const/16 v12, 0x40

    new-array v9, v12, [B

    .line 265
    .local v9, "szK_IPad":[B
    const/16 v12, 0x40

    new-array v10, v12, [B

    .line 266
    .local v10, "szK_OPad":[B
    const/4 v11, 0x0

    .line 267
    .local v11, "szTemp":[B
    const/4 v1, 0x0

    .line 268
    .local v1, "digest":[B
    const-string v7, ""

    .line 270
    .local v7, "szDigestStr":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 278
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "Not Support Auth Type."

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    move-object v8, v7

    .line 327
    .end local v7    # "szDigestStr":Ljava/lang/String;
    .local v8, "szDigestStr":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 273
    .end local v8    # "szDigestStr":Ljava/lang/String;
    .restart local v7    # "szDigestStr":Ljava/lang/String;
    :pswitch_0
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move-object v8, v7

    .line 274
    .end local v7    # "szDigestStr":Ljava/lang/String;
    .restart local v8    # "szDigestStr":Ljava/lang/String;
    goto :goto_0

    .line 282
    .end local v8    # "szDigestStr":Ljava/lang/String;
    .restart local v7    # "szDigestStr":Ljava/lang/String;
    :cond_1
    const/16 v12, 0x40

    if-gt v5, v12, :cond_2

    .line 288
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_3

    .line 294
    .end local v3    # "i":I
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    const/16 v12, 0x40

    if-lt v3, v12, :cond_4

    .line 299
    const/4 v4, 0x0

    .local v4, "nCount":I
    :goto_3
    const/16 v12, 0x40

    if-lt v4, v12, :cond_5

    .line 305
    const/4 v6, 0x0

    .line 308
    .local v6, "sha":Ljava/security/MessageDigest;
    :try_start_0
    const-string v12, "SHA-1"

    invoke-static {v12}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 316
    invoke-virtual {v6, v9}, Ljava/security/MessageDigest;->update([B)V

    .line 317
    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 318
    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v11

    .line 320
    invoke-virtual {v6, v10}, Ljava/security/MessageDigest;->update([B)V

    .line 321
    invoke-virtual {v6, v11}, Ljava/security/MessageDigest;->update([B)V

    .line 322
    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 324
    invoke-static {v1}, Lcom/wsomacp/eng/core/XCPUtil;->xcpBytesToHexString([B)Ljava/lang/String;

    move-result-object v7

    .line 325
    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .line 327
    .end local v7    # "szDigestStr":Ljava/lang/String;
    .restart local v8    # "szDigestStr":Ljava/lang/String;
    goto :goto_0

    .line 290
    .end local v4    # "nCount":I
    .end local v6    # "sha":Ljava/security/MessageDigest;
    .end local v8    # "szDigestStr":Ljava/lang/String;
    .restart local v7    # "szDigestStr":Ljava/lang/String;
    :cond_3
    aget-byte v12, p1, v3

    aput-byte v12, v9, v3

    .line 288
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 296
    :cond_4
    aget-byte v12, v9, v3

    aput-byte v12, v10, v3

    .line 294
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 301
    .restart local v4    # "nCount":I
    :cond_5
    aget-byte v12, v9, v4

    xor-int/lit8 v12, v12, 0x36

    int-to-byte v12, v12

    aput-byte v12, v9, v4

    .line 302
    aget-byte v12, v10, v4

    xor-int/lit8 v12, v12, 0x5c

    int-to-byte v12, v12

    aput-byte v12, v10, v4

    .line 299
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 310
    .restart local v6    # "sha":Ljava/security/MessageDigest;
    :catch_0
    move-exception v2

    .line 312
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    move-object v8, v7

    .line 313
    .end local v7    # "szDigestStr":Ljava/lang/String;
    .restart local v8    # "szDigestStr":Ljava/lang/String;
    goto :goto_0

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method
