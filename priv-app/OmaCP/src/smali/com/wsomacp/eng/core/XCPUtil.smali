.class public Lcom/wsomacp/eng/core/XCPUtil;
.super Ljava/lang/Object;
.source "XCPUtil.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static XCP_DATE_FORMAT_YYYYMMDD:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "yyyy.MM.dd"

    sput-object v0, Lcom/wsomacp/eng/core/XCPUtil;->XCP_DATE_FORMAT_YYYYMMDD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcpBytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 75
    if-nez p0, :cond_0

    .line 76
    const/4 v3, 0x0

    .line 93
    :goto_0
    return-object v3

    .line 78
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 80
    .local v2, "ret":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p0

    if-lt v1, v3, :cond_1

    .line 93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 84
    :cond_1
    aget-byte v3, p0, v1

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v0, v3, 0xf

    .line 86
    .local v0, "b":I
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    aget-byte v3, p0, v1

    and-int/lit8 v0, v3, 0xf

    .line 90
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "orgDate"    # Ljava/lang/String;

    .prologue
    .line 98
    const-string v2, "MM/dd/yyyy"

    .line 99
    .local v2, "DATE_FORMAT_MDY":Ljava/lang/String;
    const-string v3, "yyyy/MM/dd"

    .line 100
    .local v3, "DATE_FORMAT_YMD":Ljava/lang/String;
    const-string v1, "dd/MM/yyyy"

    .line 102
    .local v1, "DATE_FORMAT_DMY":Ljava/lang/String;
    const-string v0, "dd.MM.yyyy"

    .line 103
    .local v0, "DATE_FORMAT_DE":Ljava/lang/String;
    const/4 v5, 0x0

    .line 104
    .local v5, "date":Ljava/util/Date;
    const/4 v10, 0x0

    .line 105
    .local v10, "strDateFormat":Ljava/lang/String;
    const/4 v9, 0x0

    .line 106
    .local v9, "strDate":Ljava/lang/String;
    const/4 v11, 0x0

    .line 108
    .local v11, "strLang":Ljava/lang/String;
    invoke-static {}, Lcom/wsomacp/XCPApplication;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "date_format"

    invoke-static {v12, v13}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "dateFormat":Ljava/lang/String;
    :try_start_0
    new-instance v12, Ljava/text/SimpleDateFormat;

    sget-object v13, Lcom/wsomacp/eng/core/XCPUtil;->XCP_DATE_FORMAT_YYYYMMDD:Ljava/lang/String;

    invoke-direct {v12, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 122
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 123
    .local v4, "cal":Ljava/util/Calendar;
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSystemApiGetLanguageSetting()Ljava/lang/String;

    move-result-object v11

    .line 124
    const-string v12, "de"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 125
    move-object v10, v0

    .line 146
    :goto_1
    if-nez v5, :cond_0

    .line 147
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    .line 149
    :cond_0
    new-instance v8, Ljava/text/SimpleDateFormat;

    invoke-direct {v8, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 150
    .local v8, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    .line 151
    return-object v9

    .line 116
    .end local v4    # "cal":Ljava/util/Calendar;
    .end local v8    # "sdf":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v7

    .line 118
    .local v7, "ex":Ljava/lang/Exception;
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 128
    .end local v7    # "ex":Ljava/lang/Exception;
    .restart local v4    # "cal":Ljava/util/Calendar;
    :cond_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 130
    const-string v12, "yyyy-MM-dd"

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 131
    move-object v10, v3

    goto :goto_1

    .line 132
    :cond_2
    const-string v12, "dd-MM-yyyy"

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 133
    move-object v10, v1

    goto :goto_1

    .line 134
    :cond_3
    const-string v12, "MM-dd-yyyy"

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 135
    move-object v10, v2

    goto :goto_1

    .line 137
    :cond_4
    move-object v10, v2

    .line 138
    goto :goto_1

    .line 141
    :cond_5
    move-object v10, v2

    goto :goto_1
.end method

.method static xcpHexCharToInt(C)I
    .locals 3
    .param p0, "c"    # C

    .prologue
    .line 61
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 62
    add-int/lit8 v0, p0, -0x30

    .line 68
    :goto_0
    return v0

    .line 64
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 65
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 67
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 68
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 70
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid hex char \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static xcpHexStringToBytes(Ljava/lang/String;)[B
    .locals 7
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 28
    const/4 v2, 0x0

    .line 29
    .local v2, "ret":[B
    const/4 v3, 0x0

    .line 30
    .local v3, "sz":I
    const/4 v1, 0x0

    .line 34
    .local v1, "i":I
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    const/4 v4, 0x0

    .line 56
    :goto_0
    return-object v4

    .line 37
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 39
    div-int/lit8 v4, v3, 0x2

    new-array v2, v4, [B

    .line 41
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v3, :cond_1

    :goto_2
    move-object v4, v2

    .line 56
    goto :goto_0

    .line 44
    :cond_1
    const/16 v4, 0xf82

    if-le v1, v4, :cond_2

    .line 45
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", i :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 47
    :cond_2
    div-int/lit8 v4, v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/wsomacp/eng/core/XCPUtil;->xcpHexCharToInt(C)I

    move-result v5

    shl-int/lit8 v5, v5, 0x4

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/wsomacp/eng/core/XCPUtil;->xcpHexCharToInt(C)I

    move-result v6

    or-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v2, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    .line 52
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 53
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", i :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static xcpRemoveStringAndComma(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "orgStr"    # Ljava/lang/String;
    .param p1, "replaceStr"    # Ljava/lang/String;

    .prologue
    .line 233
    const-string v0, ""

    .line 234
    .local v0, "destStr":Ljava/lang/String;
    const-string v2, ""

    .line 236
    .local v2, "tmpStr":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v1, v0

    .line 248
    .end local v0    # "destStr":Ljava/lang/String;
    .local v1, "destStr":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 239
    .end local v1    # "destStr":Ljava/lang/String;
    .restart local v0    # "destStr":Ljava/lang/String;
    :cond_1
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    const-string v3, ""

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 242
    const-string v3, ","

    invoke-virtual {v3, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 243
    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 245
    const-string v3, ""

    invoke-virtual {v0, p1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 247
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "orgStr="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", destStr="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    move-object v1, v0

    .line 248
    .end local v0    # "destStr":Ljava/lang/String;
    .restart local v1    # "destStr":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xcpURLParser(Ljava/lang/String;)Lcom/wsomacp/eng/core/XCPDBURLParserType;
    .locals 15
    .param p0, "pURL"    # Ljava/lang/String;

    .prologue
    .line 156
    const-string v0, "http://"

    .line 157
    .local v0, "HTTP":Ljava/lang/String;
    const-string v1, "https://"

    .line 162
    .local v1, "HTTPS":Ljava/lang/String;
    const/4 v3, 0x0

    .line 167
    .local v3, "nIndex":I
    new-instance v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;

    invoke-direct {v2}, Lcom/wsomacp/eng/core/XCPDBURLParserType;-><init>()V

    .line 169
    .local v2, "URLParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 171
    const-string v10, "https"

    .line 172
    .local v10, "pProtocol":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 186
    .local v6, "pCurrentPointer":Ljava/lang/String;
    :goto_0
    move-object v12, v6

    .line 187
    .local v12, "path":Ljava/lang/String;
    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 189
    const/4 v13, -0x1

    if-eq v3, v13, :cond_2

    .line 191
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v12, v3, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 198
    .local v9, "pPath":Ljava/lang/String;
    :goto_1
    const-string v13, "/"

    invoke-virtual {v6, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 199
    .local v7, "pNextPointer":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v11, v7, v13

    .line 200
    .local v11, "pTempAddress":Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v6, v7, v13

    .line 203
    const-string v13, ":"

    invoke-virtual {v6, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 205
    .local v8, "pNextPointer2":[Ljava/lang/String;
    array-length v13, v8

    const/4 v14, 0x2

    if-lt v13, v14, :cond_3

    .line 207
    const/4 v13, 0x1

    aget-object v13, v8, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 208
    .local v4, "nPort":I
    const/4 v13, 0x0

    aget-object v5, v8, v13

    .line 222
    .local v5, "pAddress":Ljava/lang/String;
    :goto_2
    iput-object p0, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    .line 223
    iput-object v5, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szAddress:Ljava/lang/String;

    .line 224
    iput-object v9, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szPath:Ljava/lang/String;

    .line 225
    iput-object v10, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szProtocol:Ljava/lang/String;

    .line 226
    iput v4, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_nPort:I

    .line 228
    .end local v4    # "nPort":I
    .end local v5    # "pAddress":Ljava/lang/String;
    .end local v6    # "pCurrentPointer":Ljava/lang/String;
    .end local v7    # "pNextPointer":[Ljava/lang/String;
    .end local v8    # "pNextPointer2":[Ljava/lang/String;
    .end local v9    # "pPath":Ljava/lang/String;
    .end local v10    # "pProtocol":Ljava/lang/String;
    .end local v11    # "pTempAddress":Ljava/lang/String;
    .end local v12    # "path":Ljava/lang/String;
    :goto_3
    return-object v2

    .line 174
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 176
    const-string v10, "http"

    .line 177
    .restart local v10    # "pProtocol":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 178
    .restart local v6    # "pCurrentPointer":Ljava/lang/String;
    goto :goto_0

    .line 181
    .end local v6    # "pCurrentPointer":Ljava/lang/String;
    .end local v10    # "pProtocol":Ljava/lang/String;
    :cond_1
    move-object p0, v0

    .line 182
    iput-object p0, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    goto :goto_3

    .line 195
    .restart local v6    # "pCurrentPointer":Ljava/lang/String;
    .restart local v10    # "pProtocol":Ljava/lang/String;
    .restart local v12    # "path":Ljava/lang/String;
    :cond_2
    const-string v9, ""

    .restart local v9    # "pPath":Ljava/lang/String;
    goto :goto_1

    .line 212
    .restart local v7    # "pNextPointer":[Ljava/lang/String;
    .restart local v8    # "pNextPointer2":[Ljava/lang/String;
    .restart local v11    # "pTempAddress":Ljava/lang/String;
    :cond_3
    move-object v5, v11

    .line 214
    .restart local v5    # "pAddress":Ljava/lang/String;
    const-string v13, "https"

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 215
    const/16 v4, 0x1bb

    .restart local v4    # "nPort":I
    goto :goto_2

    .line 216
    .end local v4    # "nPort":I
    :cond_4
    const-string v13, "http"

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 217
    const/16 v4, 0x50

    .restart local v4    # "nPort":I
    goto :goto_2

    .line 219
    .end local v4    # "nPort":I
    :cond_5
    const/16 v4, 0x50

    .restart local v4    # "nPort":I
    goto :goto_2
.end method
