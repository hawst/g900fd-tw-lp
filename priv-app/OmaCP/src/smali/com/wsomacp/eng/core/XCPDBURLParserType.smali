.class public Lcom/wsomacp/eng/core/XCPDBURLParserType;
.super Ljava/lang/Object;
.source "XCPDBURLParserType.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final serialVersionUID:J = 0x1L


# instance fields
.field public m_nPort:I

.field public m_szAddress:Ljava/lang/String;

.field public m_szPath:Ljava/lang/String;

.field public m_szProtocol:Ljava/lang/String;

.field public m_szURL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szAddress:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szPath:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_nPort:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szProtocol:Ljava/lang/String;

    .line 21
    return-void
.end method
