.class public interface abstract Lcom/wsomacp/eng/core/XCPDecoder;
.super Ljava/lang/Object;
.source "XCPDecoder.java"


# static fields
.field public static final XCP_BEARER_AMPS_CDPD:I = 0xa

.field public static final XCP_BEARER_ANSI_136_CSD:I = 0x6

.field public static final XCP_BEARER_ANSI_136_GPRS:I = 0x7

.field public static final XCP_BEARER_ANSI_136_GUTS:I = 0x2

.field public static final XCP_BEARER_CDMA2000_1X_MOBILEIP:I = 0x18

.field public static final XCP_BEARER_CDMA2000_1X_SIMPLEIP:I = 0x17

.field public static final XCP_BEARER_END:I = 0x19

.field public static final XCP_BEARER_FLEX_REFLEX:I = 0x10

.field public static final XCP_BEARER_GSM_CSD:I = 0x8

.field public static final XCP_BEARER_GSM_GPRS:I = 0x9

.field public static final XCP_BEARER_GSM_SMS:I = 0x1

.field public static final XCP_BEARER_GSM_USSD:I = 0x0

.field public static final XCP_BEARER_IDEN_CSD:I = 0xe

.field public static final XCP_BEARER_IDEN_PACKET:I = 0xf

.field public static final XCP_BEARER_IDEN_SMS:I = 0xd

.field public static final XCP_BEARER_IS_95_CDMA_CSD:I = 0x4

.field public static final XCP_BEARER_IS_95_CDMA_PACKET:I = 0x5

.field public static final XCP_BEARER_IS_95_CDMA_SMS:I = 0x3

.field public static final XCP_BEARER_MOBITEX_MPAK:I = 0x16

.field public static final XCP_BEARER_PDC_CSD:I = 0xb

.field public static final XCP_BEARER_PDC_PACKET:I = 0xc

.field public static final XCP_BEARER_PHS_CSD:I = 0x12

.field public static final XCP_BEARER_PHS_SMS:I = 0x11

.field public static final XCP_BEARER_TETRA_ANSI136GOAST:I = 0x15

.field public static final XCP_BEARER_TETRA_PACKET:I = 0x14

.field public static final XCP_BEARER_TETRA_SDS:I = 0x13

.field public static final XCP_CONTEXT_APPLICATION:I = 0x0

.field public static final XCP_CONTEXT_NAP:I = 0x2

.field public static final XCP_CONTEXT_NONE:I = -0x1

.field public static final XCP_CONTEXT_PROXY:I = 0x1

.field public static final XCP_CONTEXT_VENDORCONFIG:I = 0x3

.field public static final XCP_DEFAULT_BEARER_COUNT:I = 0x1a

.field public static final XCP_INSTALL_NAP_PROFILE_OVERWRITE:I = 0x2

.field public static final XCP_INSTALL_NAP_PROFILE_RENAME:I = 0x1

.field public static final XCP_INSTALL_NONE:I = 0x0

.field public static final XCP_INSTALL_SYNCMLDS_PROFILE_OVERWRITE:I = 0x4

.field public static final XCP_NETPM_PDP_APP_PROTO_TYPE_HTTP:I = 0x1

.field public static final XCP_NETPM_PDP_TYPE_IP:I = 0x1

.field public static final XCP_NET_ADDR_IPV4:I = 0x1

.field public static final XCP_NET_ADDR_IPV6:I = 0x2

.field public static final XCP_NET_ERR_UNKNOWN:I = 0x0

.field public static final XCP_NET_PDP_AUTH_CHAP:I = 0x2

.field public static final XCP_NET_PDP_AUTH_NONE:I = 0x0

.field public static final XCP_NET_PDP_AUTH_PAP:I = 0x1

.field public static final XCP_NET_PROFILE_TYPE_PDP:I = 0x1

.field public static final XCP_PIN_CHECK_MAX_COUNT:I = 0x3

.field public static final XCP_STR_APPID_BROWSER:Ljava/lang/String; = "w2"

.field public static final XCP_STR_APPID_EMAIL_IN_IMAP4:Ljava/lang/String; = "143"

.field public static final XCP_STR_APPID_EMAIL_IN_POP3:Ljava/lang/String; = "110"

.field public static final XCP_STR_APPID_EMAIL_OUT_SMTP:Ljava/lang/String; = "25"

.field public static final XCP_STR_APPID_MMS:Ljava/lang/String; = "w4"

.field public static final XCP_STR_APPID_STREAMING:Ljava/lang/String; = "554"

.field public static final XCP_STR_APPID_SYNCML_DM:Ljava/lang/String; = "w7"

.field public static final XCP_STR_APPID_SYNCML_DS:Ljava/lang/String; = "w5"

.field public static final XCP_STR_APPID_XCAP:Ljava/lang/String; = "ap9227"

.field public static final XCP_TP_NETWORK_TYPE_HTTP:Ljava/lang/String; = "http"

.field public static final XCP_TP_NETWORK_TYPE_HTTPS:Ljava/lang/String; = "https"
