.class public interface abstract Lcom/wsomacp/eng/core/XCPDef;
.super Ljava/lang/Object;
.source "XCPDef.java"


# static fields
.field public static final USER_SYNCMLDM_UI_ACTIVATE_ID:Ljava/lang/String; = "User.SyncMLDM.UI.Activate"

.field public static final USER_SYNCML_DM_MENU_ID:Ljava/lang/String; = "User.SyncMLDM.Menu"

.field public static final XCP_UI_EMAIL_ACCOUNT_INSTALL:I = 0x9

.field public static final XCP_UI_INSTALL_FAIL:I = 0x4

.field public static final XCP_UI_INSTALL_NEW_ACCOUNT:I = 0x1

.field public static final XCP_UI_INSTALL_NOT_SUPPORT:I = 0x5

.field public static final XCP_UI_INSTALL_RETRY:I = 0x3

.field public static final XCP_UI_INSTALL_SUCCESS:I = 0x2

.field public static final XCP_UI_NET_ACCOUNT_MAX:I = 0x7

.field public static final XCP_UI_NET_ACCOUNT_RENAME:I = 0x6

.field public static final XCP_UI_SYNCMLDS_ACCOUNT_MAX:I = 0x8
