.class public Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;
.super Ljava/lang/Object;
.source "XCPSMLDSGlobalSyncInfo.java"


# instance fields
.field public m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

.field public m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

.field public m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

.field public m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

.field public m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

.field public m_nHttpServerPort:I

.field public m_nProtocol:I

.field public m_szAuthLevel:Ljava/lang/String;

.field public m_szAuthType:Ljava/lang/String;

.field public m_szCPSyncMLDSProfileName:Ljava/lang/String;

.field public m_szHttpServerHostName:Ljava/lang/String;

.field public m_szHttpServerPath:Ljava/lang/String;

.field public m_szHttpServerUrl:Ljava/lang/String;

.field public m_szHttpUserID:Ljava/lang/String;

.field public m_szHttpUserPS:Ljava/lang/String;

.field public m_szNextNonce:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerHostName:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerPath:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szNextNonce:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpUserID:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpUserPS:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthLevel:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthType:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szCPSyncMLDSProfileName:Ljava/lang/String;

    .line 33
    new-instance v0, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    .line 34
    new-instance v0, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    .line 35
    new-instance v0, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    .line 36
    new-instance v0, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    .line 37
    new-instance v0, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    .line 38
    return-void
.end method
