.class public Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;
.super Ljava/lang/Object;
.source "XCPNVMSyncDMInfoConRef.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field XCP_MAX_URL_LENGTH:I

.field public m_CAdvSetting:Lcom/wsomacp/eng/core/XCPConRefAdvSetting;

.field public m_CNAP:Lcom/wsomacp/eng/core/XCPConRefNAP;

.field public m_CPX:Lcom/wsomacp/eng/core/XCPConRefPX;

.field public m_bActive:Z

.field public m_bProxyUse:Z

.field public m_nService:I

.field public m_szHomeUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/16 v0, 0x100

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;->XCP_MAX_URL_LENGTH:I

    .line 20
    const-string v0, " "

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;->m_szHomeUrl:Ljava/lang/String;

    .line 21
    new-instance v0, Lcom/wsomacp/eng/core/XCPConRefNAP;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPConRefNAP;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;->m_CNAP:Lcom/wsomacp/eng/core/XCPConRefNAP;

    .line 22
    new-instance v0, Lcom/wsomacp/eng/core/XCPConRefPX;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPConRefPX;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;->m_CPX:Lcom/wsomacp/eng/core/XCPConRefPX;

    .line 23
    new-instance v0, Lcom/wsomacp/eng/core/XCPConRefAdvSetting;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPConRefAdvSetting;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfoConRef;->m_CAdvSetting:Lcom/wsomacp/eng/core/XCPConRefAdvSetting;

    .line 24
    return-void
.end method
