.class public Lcom/wsomacp/eng/core/XCPAdapter;
.super Ljava/lang/Object;
.source "XCPAdapter.java"

# interfaces
.implements Lcom/wsomacp/eng/core/XCPDecoder;
.implements Lcom/wsomacp/eng/core/XCPDef;
.implements Lcom/wsomacp/interfaces/XCPInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcpSetDefaultBearerValue()[Lcom/wsomacp/eng/core/XCPBearer;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x19

    .line 28
    const/16 v2, 0x1a

    new-array v0, v2, [Lcom/wsomacp/eng/core/XCPBearer;

    .line 29
    .local v0, "gBearerValue":[Lcom/wsomacp/eng/core/XCPBearer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 35
    aget-object v2, v0, v4

    if-nez v2, :cond_0

    .line 36
    new-instance v2, Lcom/wsomacp/eng/core/XCPBearer;

    invoke-direct {v2}, Lcom/wsomacp/eng/core/XCPBearer;-><init>()V

    aput-object v2, v0, v4

    .line 37
    :cond_0
    aget-object v2, v0, v4

    iput v5, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_nToken:I

    .line 39
    aget-object v2, v0, v5

    const-string v3, "GSM-USSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 40
    const/4 v2, 0x1

    aget-object v2, v0, v2

    const-string v3, "GSM-SMS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 41
    const/4 v2, 0x2

    aget-object v2, v0, v2

    const-string v3, "ANSI-136-GUTS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 42
    const/4 v2, 0x3

    aget-object v2, v0, v2

    const-string v3, "IS-95-CDMA-SMS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 43
    const/4 v2, 0x4

    aget-object v2, v0, v2

    const-string v3, "IS-95-CDMA-CSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 44
    const/4 v2, 0x5

    aget-object v2, v0, v2

    const-string v3, "IS-95-CDMA-PACKET"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 45
    const/4 v2, 0x6

    aget-object v2, v0, v2

    const-string v3, "ANSI-136-CSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 46
    const/4 v2, 0x7

    aget-object v2, v0, v2

    const-string v3, "ANSI-136-GPRS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 47
    const/16 v2, 0x8

    aget-object v2, v0, v2

    const-string v3, "GSM-CSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 48
    const/16 v2, 0x9

    aget-object v2, v0, v2

    const-string v3, "GSM-GPRS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 49
    const/16 v2, 0xa

    aget-object v2, v0, v2

    const-string v3, "AMPS-CDPD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 50
    const/16 v2, 0xb

    aget-object v2, v0, v2

    const-string v3, "PDC-CSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 51
    const/16 v2, 0xc

    aget-object v2, v0, v2

    const-string v3, "PDC-PACKET"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 52
    const/16 v2, 0xd

    aget-object v2, v0, v2

    const-string v3, "IDEN-SMS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 53
    const/16 v2, 0xe

    aget-object v2, v0, v2

    const-string v3, "IDEN-CSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 54
    const/16 v2, 0xf

    aget-object v2, v0, v2

    const-string v3, "IDEN-PACKET"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 55
    const/16 v2, 0x10

    aget-object v2, v0, v2

    const-string v3, "FLEX/REFLEX"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 56
    const/16 v2, 0x11

    aget-object v2, v0, v2

    const-string v3, "PHS-SMS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 57
    const/16 v2, 0x12

    aget-object v2, v0, v2

    const-string v3, "PHS-CSD"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 58
    const/16 v2, 0x13

    aget-object v2, v0, v2

    const-string v3, "TETRA-SDS"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 59
    const/16 v2, 0x14

    aget-object v2, v0, v2

    const-string v3, "TETRA-PACKET"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 60
    const/16 v2, 0x15

    aget-object v2, v0, v2

    const-string v3, "ANSI-136-GHOST"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 61
    const/16 v2, 0x16

    aget-object v2, v0, v2

    const-string v3, "MOBITEX-MPAK"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 62
    const/16 v2, 0x17

    aget-object v2, v0, v2

    const-string v3, "CDMA2000-1X-SIMPLE-IP"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 63
    const/16 v2, 0x18

    aget-object v2, v0, v2

    const-string v3, "CDMA2000-1X-MOBILE-IP"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 64
    aget-object v2, v0, v4

    const-string v3, "Unkown Bearer"

    iput-object v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 66
    return-object v0

    .line 31
    :cond_1
    aget-object v2, v0, v1

    if-nez v2, :cond_2

    .line 32
    new-instance v2, Lcom/wsomacp/eng/core/XCPBearer;

    invoke-direct {v2}, Lcom/wsomacp/eng/core/XCPBearer;-><init>()V

    aput-object v2, v0, v1

    .line 33
    :cond_2
    aget-object v2, v0, v1

    add-int/lit16 v3, v1, 0xa2

    iput v3, v2, Lcom/wsomacp/eng/core/XCPBearer;->m_nToken:I

    .line 29
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method private xcpUtil4BitChange(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "szSrc"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    new-array v3, v4, [C

    .line 11
    .local v3, "szTemp":[C
    const/4 v2, 0x0

    .line 12
    .local v2, "nTemp":I
    const/4 v1, 0x0

    .local v1, "nSize":I
    const/4 v0, 0x0

    .line 14
    .local v0, "nCount":I
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 15
    array-length v1, v3

    .line 16
    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    .line 23
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([C)V

    return-object v4

    .line 18
    :cond_0
    const/4 v2, 0x0

    .line 19
    aget-char v4, v3, v0

    and-int/lit16 v4, v4, 0xf0

    shr-int/lit8 v2, v4, 0x4

    .line 20
    aget-char v4, v3, v0

    and-int/lit8 v4, v4, 0xf

    shl-int/lit8 v4, v4, 0x4

    or-int/2addr v2, v4

    .line 21
    int-to-char v4, v2

    aput-char v4, v3, v0

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
