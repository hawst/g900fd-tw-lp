.class public Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;
.super Ljava/lang/Object;
.source "XCPNVMSyncNetConProfile_Backup.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field XCP_DEFAULT_BUFFER_SIZE_2:I

.field XCP_MAX_ADDRESS_LENGTH:I

.field XCP_MAX_ID_LENGTH:I

.field XCP_MAX_PASSWORD_LENGTH:I

.field public m_bActive:Z

.field public m_nProtoAppType:I

.field public m_szAPN:Ljava/lang/String;

.field public m_szAddress:Ljava/lang/String;

.field public m_szId:Ljava/lang/String;

.field public m_szPassword:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/16 v0, 0x40

    iput v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->XCP_DEFAULT_BUFFER_SIZE_2:I

    .line 9
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->XCP_MAX_ID_LENGTH:I

    .line 10
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->XCP_MAX_PASSWORD_LENGTH:I

    .line 11
    iput v1, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->XCP_MAX_ADDRESS_LENGTH:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->m_szAPN:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->m_szId:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->m_szPassword:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/eng/core/XCPNVMSyncNetConProfile_Backup;->m_szAddress:Ljava/lang/String;

    .line 26
    return-void
.end method
