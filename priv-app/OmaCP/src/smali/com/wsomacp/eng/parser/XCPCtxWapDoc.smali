.class public Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
.super Ljava/lang/Object;
.source "XCPCtxWapDoc.java"


# instance fields
.field public m_CAccess:Lcom/wsomacp/eng/parser/XCPCtxAccess;

.field public m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

.field public m_CBootstrap:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

.field public m_CClientIdent:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

.field public m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

.field public m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

.field public m_CVendrCfg:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

.field public m_CWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 17
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 18
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CBootstrap:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 19
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CClientIdent:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    .line 20
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CVendrCfg:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 21
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxApplication;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 22
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxAccess;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CAccess:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .line 23
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxWifi;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxWifi;-><init>()V

    iput-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 24
    return-void
.end method
