.class public Lcom/wsomacp/eng/parser/XCPCtxValidity;
.super Ljava/lang/Object;
.source "XCPCtxValidity.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxValidity;

.field public m_szCountry:Ljava/lang/String;

.field public m_szNetwork:Ljava/lang/String;

.field public m_szSid:Ljava/lang/String;

.field public m_szSoc:Ljava/lang/String;

.field public m_szValiduntil:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_pxValidityAddList(Lcom/wsomacp/eng/parser/XCPCtxValidity;Lcom/wsomacp/eng/parser/XCPCtxValidity;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxValidity;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxValidity;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxValidity;
    .param p2, "validitString"    # [Ljava/lang/String;

    .prologue
    .line 14
    const/4 v0, 0x0

    .line 15
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxValidity;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxValidity;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxValidity;-><init>()V

    .line 16
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxValidity;
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_szCountry:Ljava/lang/String;

    .line 17
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_szNetwork:Ljava/lang/String;

    .line 18
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_szSid:Ljava/lang/String;

    .line 19
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_szSoc:Ljava/lang/String;

    .line 20
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_szValiduntil:Ljava/lang/String;

    .line 21
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxValidity;

    .line 23
    if-nez p0, :cond_0

    .line 25
    move-object p0, v0

    .line 26
    move-object p1, p0

    .line 40
    :goto_0
    return-object p0

    .line 30
    :cond_0
    if-nez p1, :cond_1

    .line 32
    move-object p1, p0

    .line 33
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxValidity;

    if-eqz v1, :cond_1

    .line 34
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxValidity;

    goto :goto_1

    .line 37
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxValidity;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxValidity;

    .line 38
    move-object p1, v0

    goto :goto_0
.end method
