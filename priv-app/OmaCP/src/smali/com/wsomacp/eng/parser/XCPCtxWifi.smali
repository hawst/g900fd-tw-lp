.class public Lcom/wsomacp/eng/parser/XCPCtxWifi;
.super Ljava/lang/Object;
.source "XCPCtxWifi.java"


# static fields
.field public static PXWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

.field public m_szWifi:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxWifi;->PXWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetPxWifiCount()I
.end method

.method static native wsomacp_GetPxWifiData(I)[Ljava/lang/String;
.end method

.method public static xcp_Wifi()V
    .locals 6

    .prologue
    .line 18
    const/4 v4, 0x0

    .line 19
    .local v4, "wifiIndex":I
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxWifi;->wsomacp_GetPxWifiCount()I

    move-result v1

    .line 20
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "head":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    const/4 v3, 0x0

    .line 22
    .local v3, "tail":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    invoke-static {v1}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetWifiCount(I)V

    .line 24
    if-lez v1, :cond_0

    .line 26
    const/4 v4, 0x1

    :goto_0
    add-int/lit8 v5, v1, 0x1

    if-ge v4, v5, :cond_0

    .line 28
    invoke-static {v4}, Lcom/wsomacp/eng/parser/XCPCtxWifi;->wsomacp_GetPxWifiData(I)[Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "WifiStr":[Ljava/lang/String;
    add-int/lit8 v5, v4, -0x1

    invoke-static {v2, v3, v0, v5}, Lcom/wsomacp/eng/parser/XCPCtxWifi;->xcp_WifiAddList(Lcom/wsomacp/eng/parser/XCPCtxWifi;Lcom/wsomacp/eng/parser/XCPCtxWifi;[Ljava/lang/String;I)Lcom/wsomacp/eng/parser/XCPCtxWifi;

    move-result-object v2

    .line 30
    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxWifi;->PXWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 26
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 33
    .end local v0    # "WifiStr":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static xcp_WifiAddList(Lcom/wsomacp/eng/parser/XCPCtxWifi;Lcom/wsomacp/eng/parser/XCPCtxWifi;[Ljava/lang/String;I)Lcom/wsomacp/eng/parser/XCPCtxWifi;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxWifi;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxWifi;
    .param p2, "data"    # [Ljava/lang/String;
    .param p3, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 37
    const/4 v0, 0x0

    .line 38
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxWifi;-><init>()V

    .line 40
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    if-nez p2, :cond_0

    .line 42
    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_szWifi:[Ljava/lang/String;

    .line 49
    :goto_0
    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 51
    if-nez p0, :cond_1

    .line 53
    move-object p0, v0

    .line 54
    move-object p1, p0

    .line 69
    :goto_1
    return-object p0

    .line 46
    :cond_0
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_szWifi:[Ljava/lang/String;

    goto :goto_0

    .line 58
    :cond_1
    if-nez p1, :cond_2

    .line 60
    move-object p1, p0

    .line 62
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    if-eqz v1, :cond_2

    .line 63
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    goto :goto_2

    .line 66
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 67
    move-object p1, v0

    goto :goto_1
.end method
