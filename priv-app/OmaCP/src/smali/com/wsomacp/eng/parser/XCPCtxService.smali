.class public Lcom/wsomacp/eng/parser/XCPCtxService;
.super Ljava/lang/Object;
.source "XCPCtxService.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

.field public m_szService:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_ServiceAddList(Lcom/wsomacp/eng/parser/XCPCtxService;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxService;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxService;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxService;
    .param p2, "service"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxService;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxService;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxService;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxService;-><init>()V

    .line 13
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxService;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    .line 14
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    .line 15
    if-nez p0, :cond_0

    .line 17
    move-object p0, v0

    .line 18
    move-object p1, p0

    .line 32
    :goto_0
    return-object p0

    .line 22
    :cond_0
    if-nez p1, :cond_1

    .line 24
    move-object p1, p0

    .line 25
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v1, :cond_1

    .line 26
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    goto :goto_1

    .line 29
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    .line 30
    move-object p1, v0

    goto :goto_0
.end method

.method public static xcp_ServiceListDelete(Lcom/wsomacp/eng/parser/XCPCtxService;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxService;

    .prologue
    .line 37
    move-object v0, p0

    .line 38
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxService;
    :goto_0
    if-eqz v0, :cond_0

    .line 40
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    goto :goto_0

    .line 43
    :cond_0
    const/4 p0, 0x0

    .line 44
    return-void
.end method
