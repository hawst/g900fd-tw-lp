.class public Lcom/wsomacp/eng/parser/XCPCtxAccess;
.super Ljava/lang/Object;
.source "XCPCtxAccess.java"


# static fields
.field public static g_CPXAccess_h:Lcom/wsomacp/eng/parser/XCPCtxAccess;

.field static m_CPXAccess_t:Lcom/wsomacp/eng/parser/XCPCtxAccess;


# instance fields
.field public m_CAppIdList:Lcom/wsomacp/eng/parser/XCPCtxAppId;

.field public m_CDomainList:Lcom/wsomacp/eng/parser/XCPCtxDomain;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAccess;

.field public m_CPortNbrList:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

.field public m_CRuleList:Lcom/wsomacp/eng/parser/XCPCtxRule;

.field public m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

.field public m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->g_CPXAccess_h:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .line 41
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CPXAccess_t:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetPxAccessDomainCount(I)I
.end method

.method static native wsomacp_GetPxAccessDomainData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAccessToNapIdCount(I)I
.end method

.method static native wsomacp_GetPxAccessToNapIdData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAccessToProxyCount(I)I
.end method

.method static native wsomacp_GetPxAccessToProxyData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAppIdCount(I)I
.end method

.method static native wsomacp_GetPxAppIdData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxCtxAccessCount()I
.end method

.method static native wsomacp_GetPxPortNbrCount(I)I
.end method

.method static native wsomacp_GetPxPortNbrData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxRuleCount(I)I
.end method

.method static native wsomacp_GetPxRuleData(II)Ljava/lang/String;
.end method

.method private static xcpCtxAccessAddList(Lcom/wsomacp/eng/parser/XCPCtxAccess;Lcom/wsomacp/eng/parser/XCPCtxAccess;Lcom/wsomacp/eng/parser/XCPCtxRule;Lcom/wsomacp/eng/parser/XCPCtxAppId;Lcom/wsomacp/eng/parser/XCPCtxPortNbr;Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToProxy;)Lcom/wsomacp/eng/parser/XCPCtxAccess;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxAccess;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxAccess;
    .param p2, "ctxrule_h"    # Lcom/wsomacp/eng/parser/XCPCtxRule;
    .param p3, "ctxappid_h"    # Lcom/wsomacp/eng/parser/XCPCtxAppId;
    .param p4, "ctxportnbr_h"    # Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    .param p5, "ctxdomain_h"    # Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .param p6, "ctxtonapid_h"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .param p7, "ctxtoproxy_h"    # Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxAccess;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .end local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxAccess;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxAccess;-><init>()V

    .line 155
    .restart local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxAccess;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CRuleList:Lcom/wsomacp/eng/parser/XCPCtxRule;

    .line 156
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CAppIdList:Lcom/wsomacp/eng/parser/XCPCtxAppId;

    .line 157
    iput-object p4, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CPortNbrList:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    .line 158
    iput-object p5, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CDomainList:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .line 159
    iput-object p6, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .line 160
    iput-object p7, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    .line 162
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .line 164
    if-nez p0, :cond_0

    .line 166
    move-object p0, v0

    .line 167
    move-object p1, p0

    .line 181
    :goto_0
    return-object p0

    .line 171
    :cond_0
    if-nez p1, :cond_1

    .line 173
    move-object p1, p0

    .line 174
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    if-eqz v1, :cond_1

    .line 175
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    goto :goto_1

    .line 178
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .line 179
    move-object p1, v0

    goto :goto_0
.end method

.method public static xcp_CtxAccess()V
    .locals 32

    .prologue
    .line 45
    const/16 v22, 0x0

    .line 46
    .local v22, "i":I
    const/16 v26, 0x0

    .line 48
    .local v26, "nPxindex":I
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxCtxAccessCount()I

    move-result v23

    .line 50
    .local v23, "nCount":I
    const/4 v4, 0x0

    .line 51
    .local v4, "CtxRuleList_h":Lcom/wsomacp/eng/parser/XCPCtxRule;
    const/4 v14, 0x0

    .line 53
    .local v14, "CtxRuleList_t":Lcom/wsomacp/eng/parser/XCPCtxRule;
    const/4 v5, 0x0

    .line 54
    .local v5, "CtxAppIdList_h":Lcom/wsomacp/eng/parser/XCPCtxAppId;
    const/4 v11, 0x0

    .line 56
    .local v11, "CtxAppIdList_t":Lcom/wsomacp/eng/parser/XCPCtxAppId;
    const/4 v6, 0x0

    .line 57
    .local v6, "CtxPortNbrList_h":Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    const/4 v13, 0x0

    .line 59
    .local v13, "CtxPortNbrList_t":Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    const/4 v7, 0x0

    .line 60
    .local v7, "CtxDomainList_h":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    const/4 v12, 0x0

    .line 62
    .local v12, "CtxDomainList_t":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    const/4 v8, 0x0

    .line 63
    .local v8, "CtxToNapIdList_h":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    const/4 v15, 0x0

    .line 65
    .local v15, "CtxToNapIdList_t":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    const/4 v9, 0x0

    .line 66
    .local v9, "CtxToProxyList_h":Lcom/wsomacp/eng/parser/XCPCtxToProxy;
    const/16 v16, 0x0

    .line 68
    .local v16, "CtxToProxyList_t":Lcom/wsomacp/eng/parser/XCPCtxToProxy;
    if-lez v23, :cond_6

    .line 70
    const/16 v26, 0x0

    .line 71
    const/16 v26, 0x1

    :goto_0
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, v26

    if-ge v0, v2, :cond_6

    .line 73
    invoke-static/range {v26 .. v26}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxRuleCount(I)I

    move-result v28

    .line 74
    .local v28, "nPxrulecount":I
    if-lez v28, :cond_0

    .line 76
    const/16 v22, 0x0

    .line 77
    const/16 v22, 0x1

    :goto_1
    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v28

    int-to-float v3, v0

    const/high16 v31, 0x3f800000    # 1.0f

    add-float v3, v3, v31

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 79
    move/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxRuleData(II)Ljava/lang/String;

    move-result-object v19

    .line 80
    .local v19, "RuleString":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-static {v4, v14, v0}, Lcom/wsomacp/eng/parser/XCPCtxRule;->xcp_pxCtxRuleAddList(Lcom/wsomacp/eng/parser/XCPCtxRule;Lcom/wsomacp/eng/parser/XCPCtxRule;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxRule;

    move-result-object v4

    .line 77
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 84
    .end local v19    # "RuleString":Ljava/lang/String;
    :cond_0
    invoke-static/range {v26 .. v26}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAppIdCount(I)I

    move-result v24

    .line 85
    .local v24, "nPxappidcount":I
    if-lez v24, :cond_1

    .line 87
    const/16 v22, 0x0

    .line 88
    const/16 v22, 0x1

    :goto_2
    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v24

    int-to-float v3, v0

    const/high16 v31, 0x3f800000    # 1.0f

    add-float v3, v3, v31

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 90
    move/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAppIdData(II)Ljava/lang/String;

    move-result-object v10

    .line 91
    .local v10, "AppIdString":Ljava/lang/String;
    invoke-static {v5, v11, v10}, Lcom/wsomacp/eng/parser/XCPCtxAppId;->xcp_pxCtxAppidAddList(Lcom/wsomacp/eng/parser/XCPCtxAppId;Lcom/wsomacp/eng/parser/XCPCtxAppId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxAppId;

    move-result-object v5

    .line 88
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 95
    .end local v10    # "AppIdString":Ljava/lang/String;
    :cond_1
    invoke-static/range {v26 .. v26}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxPortNbrCount(I)I

    move-result v27

    .line 96
    .local v27, "nPxportcount":I
    if-lez v27, :cond_2

    .line 98
    const/16 v22, 0x0

    .line 99
    const/16 v22, 0x1

    :goto_3
    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v27

    int-to-float v3, v0

    const/high16 v31, 0x3f800000    # 1.0f

    add-float v3, v3, v31

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 101
    move/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxPortNbrData(II)Ljava/lang/String;

    move-result-object v18

    .line 102
    .local v18, "PortNbrString":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-static {v6, v13, v0}, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;->xcp_ProvPortNbrAddList(Lcom/wsomacp/eng/parser/XCPCtxPortNbr;Lcom/wsomacp/eng/parser/XCPCtxPortNbr;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    move-result-object v6

    .line 99
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 106
    .end local v18    # "PortNbrString":Ljava/lang/String;
    :cond_2
    invoke-static/range {v26 .. v26}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAccessDomainCount(I)I

    move-result v25

    .line 107
    .local v25, "nPxdomaincount":I
    if-lez v25, :cond_3

    .line 109
    const/16 v22, 0x0

    .line 110
    const/16 v22, 0x1

    :goto_4
    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v25

    int-to-float v3, v0

    const/high16 v31, 0x3f800000    # 1.0f

    add-float v3, v3, v31

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 112
    move/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAccessDomainData(II)Ljava/lang/String;

    move-result-object v17

    .line 113
    .local v17, "DomainString":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v7, v12, v0}, Lcom/wsomacp/eng/parser/XCPCtxDomain;->xcp_pxDomainAddList(Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxDomain;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxDomain;

    move-result-object v7

    .line 110
    add-int/lit8 v22, v22, 0x1

    goto :goto_4

    .line 117
    .end local v17    # "DomainString":Ljava/lang/String;
    :cond_3
    invoke-static/range {v26 .. v26}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAccessToNapIdCount(I)I

    move-result v29

    .line 118
    .local v29, "nPxtonapidcount":I
    if-lez v29, :cond_4

    .line 120
    const/16 v22, 0x0

    .line 121
    const/16 v22, 0x1

    :goto_5
    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v29

    int-to-float v3, v0

    const/high16 v31, 0x3f800000    # 1.0f

    add-float v3, v3, v31

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 123
    move/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAccessToNapIdData(II)Ljava/lang/String;

    move-result-object v20

    .line 124
    .local v20, "ToNapIdString":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-static {v8, v15, v0}, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->xcp_PXNapIdAddList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    move-result-object v8

    .line 121
    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    .line 128
    .end local v20    # "ToNapIdString":Ljava/lang/String;
    :cond_4
    invoke-static/range {v26 .. v26}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAccessToProxyCount(I)I

    move-result v30

    .line 129
    .local v30, "nPxtoproxycount":I
    if-lez v30, :cond_5

    .line 131
    const/16 v22, 0x0

    .line 132
    const/16 v22, 0x1

    :goto_6
    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v30

    int-to-float v3, v0

    const/high16 v31, 0x3f800000    # 1.0f

    add-float v3, v3, v31

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    .line 134
    move/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->wsomacp_GetPxAccessToProxyData(II)Ljava/lang/String;

    move-result-object v21

    .line 135
    .local v21, "ToProxyString":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v9, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->xcp_pxtoproxyAddList(Lcom/wsomacp/eng/parser/XCPCtxToProxy;Lcom/wsomacp/eng/parser/XCPCtxToProxy;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    move-result-object v9

    .line 132
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 139
    .end local v21    # "ToProxyString":Ljava/lang/String;
    :cond_5
    sget-object v2, Lcom/wsomacp/eng/parser/XCPCtxAccess;->g_CPXAccess_h:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxAccess;->m_CPXAccess_t:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    invoke-static/range {v2 .. v9}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->xcpCtxAccessAddList(Lcom/wsomacp/eng/parser/XCPCtxAccess;Lcom/wsomacp/eng/parser/XCPCtxAccess;Lcom/wsomacp/eng/parser/XCPCtxRule;Lcom/wsomacp/eng/parser/XCPCtxAppId;Lcom/wsomacp/eng/parser/XCPCtxPortNbr;Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToProxy;)Lcom/wsomacp/eng/parser/XCPCtxAccess;

    move-result-object v2

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxAccess;->g_CPXAccess_h:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .line 141
    const/4 v4, 0x0

    .line 142
    const/4 v5, 0x0

    .line 143
    const/4 v6, 0x0

    .line 144
    const/4 v7, 0x0

    .line 145
    const/4 v8, 0x0

    .line 146
    const/4 v9, 0x0

    .line 71
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_0

    .line 149
    .end local v24    # "nPxappidcount":I
    .end local v25    # "nPxdomaincount":I
    .end local v27    # "nPxportcount":I
    .end local v28    # "nPxrulecount":I
    .end local v29    # "nPxtonapidcount":I
    .end local v30    # "nPxtoproxycount":I
    :cond_6
    return-void
.end method
