.class public Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
.super Ljava/lang/Object;
.source "XCPCtxVendorConfig.java"


# static fields
.field public static PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

.field public m_szAttrName:Ljava/lang/String;

.field public m_szAttrValue:Ljava/lang/String;

.field public m_szName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetPxVenderConfigCount()I
.end method

.method static native wsomacp_GetPxVenderConfigData(I)[Ljava/lang/String;
.end method

.method public static xcp_VendorConfig()V
    .locals 6

    .prologue
    .line 20
    const/4 v4, 0x0

    .line 21
    .local v4, "vendorindex":I
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->wsomacp_GetPxVenderConfigCount()I

    move-result v1

    .line 22
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "head":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    const/4 v3, 0x0

    .line 24
    .local v3, "tail":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    invoke-static {v1}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetVendorConfigCount(I)V

    .line 26
    if-lez v1, :cond_0

    .line 28
    const/4 v4, 0x1

    :goto_0
    add-int/lit8 v5, v1, 0x1

    if-ge v4, v5, :cond_0

    .line 30
    invoke-static {v4}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->wsomacp_GetPxVenderConfigData(I)[Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "VendorConfigStr":[Ljava/lang/String;
    invoke-static {v2, v3, v0}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->xcp_VendorConfigAddList(Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    move-result-object v2

    .line 32
    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 28
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 35
    .end local v0    # "VendorConfigStr":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static xcp_VendorConfigAddList(Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    .param p2, "data"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 39
    const/4 v0, 0x0

    .line 40
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;-><init>()V

    .line 42
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    if-nez p2, :cond_0

    .line 44
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrValue:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szName:Ljava/lang/String;

    .line 53
    :goto_0
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 55
    if-nez p0, :cond_1

    .line 57
    move-object p0, v0

    .line 58
    move-object p1, p0

    .line 73
    :goto_1
    return-object p0

    .line 48
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szName:Ljava/lang/String;

    .line 49
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrName:Ljava/lang/String;

    .line 50
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrValue:Ljava/lang/String;

    goto :goto_0

    .line 62
    :cond_1
    if-nez p1, :cond_2

    .line 64
    move-object p1, p0

    .line 66
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    if-eqz v1, :cond_2

    .line 67
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    goto :goto_2

    .line 70
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 71
    move-object p1, v0

    goto :goto_1
.end method
