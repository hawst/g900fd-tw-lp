.class public Lcom/wsomacp/eng/parser/XCPCtxNetwork;
.super Ljava/lang/Object;
.source "XCPCtxNetwork.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

.field public m_szNetwork:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_pxNetworkAddList(Lcom/wsomacp/eng/parser/XCPCtxNetwork;Lcom/wsomacp/eng/parser/XCPCtxNetwork;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    .param p2, "networkString"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "Network":Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    .end local v0    # "Network":Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxNetwork;-><init>()V

    .line 13
    .restart local v0    # "Network":Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxNetwork;->m_szNetwork:Ljava/lang/String;

    .line 15
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNetwork;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    .line 17
    if-nez p0, :cond_0

    .line 19
    move-object p0, v0

    .line 20
    move-object p1, p0

    .line 35
    :goto_0
    return-object p0

    .line 24
    :cond_0
    if-nez p1, :cond_1

    .line 26
    move-object p1, p0

    .line 27
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNetwork;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    if-eqz v1, :cond_1

    .line 28
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxNetwork;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    goto :goto_1

    .line 31
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNetwork;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    .line 32
    move-object p1, v0

    goto :goto_0
.end method
