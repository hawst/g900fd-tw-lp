.class public Lcom/wsomacp/eng/parser/XCPCtxApplication;
.super Ljava/lang/Object;
.source "XCPCtxApplication.java"


# static fields
.field public static g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

.field static m_CPXApp_t:Lcom/wsomacp/eng/parser/XCPCtxApplication;


# instance fields
.field public m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

.field public m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

.field public m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

.field public m_CRoaming:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

.field public m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

.field public m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

.field public m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

.field public m_szAaccept:Ljava/lang/String;

.field public m_szAppId:Ljava/lang/String;

.field public m_szAprotocol:Ljava/lang/String;

.field public m_szFrom:Ljava/lang/String;

.field public m_szInternet:Ljava/lang/String;

.field public m_szName:Ljava/lang/String;

.field public m_szProviderId:Ljava/lang/String;

.field public m_szXCAP_AUID_CDMS:Ljava/lang/String;

.field public m_szXCAP_AUID_DCS:Ljava/lang/String;

.field public m_szXCAP_AUID_PMS:Ljava/lang/String;

.field public m_szXCAP_IMEI_HEADER:Ljava/lang/String;

.field public m_szXCAP_TID_HEADER:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 72
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CPXApp_t:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetApplicationData(I)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAppAddrCount(I)I
.end method

.method static native wsomacp_GetPxAppAddrData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAppAuthCount(I)I
.end method

.method static native wsomacp_GetPxAppAuthData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAppPortCount(II)I
.end method

.method static native wsomacp_GetPxAppPortData(III)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxApplicationCount()I
.end method

.method static native wsomacp_GetPxNapIdCount(I)I
.end method

.method static native wsomacp_GetPxNapIdData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxResourceCount(I)I
.end method

.method static native wsomacp_GetPxResourceData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxRoamingData()[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxServiceCount(III)I
.end method

.method static native wsomacp_GetPxServiceData(IIII)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxToAddrCount(I)I
.end method

.method static native wsomacp_GetPxToAddrData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxToProxyCount(I)I
.end method

.method static native wsomacp_GetPxToProxyData(II)Ljava/lang/String;
.end method

.method public static xcp_Application()V
    .locals 48

    .prologue
    .line 141
    const/16 v30, 0x0

    .local v30, "i":I
    const/16 v31, 0x0

    .local v31, "j":I
    const/16 v32, 0x0

    .line 142
    .local v32, "k":I
    const/16 v38, 0x0

    .line 144
    .local v38, "nPxindex":I
    const/4 v7, 0x0

    .local v7, "ToProxyList_h":Lcom/wsomacp/eng/parser/XCPCtxToProxy;
    const/16 v29, 0x0

    .line 145
    .local v29, "ToProxyList_t":Lcom/wsomacp/eng/parser/XCPCtxToProxy;
    const/4 v8, 0x0

    .local v8, "ToNapIdList_h":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    const/16 v28, 0x0

    .line 146
    .local v28, "ToNapIdList_t":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    const/4 v9, 0x0

    .local v9, "ToAddrList_h":Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    const/16 v27, 0x0

    .line 147
    .local v27, "ToAddrList_t":Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    const/4 v10, 0x0

    .local v10, "AppAddr_h":Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    const/4 v15, 0x0

    .line 148
    .local v15, "AppAddr_t":Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    const/4 v11, 0x0

    .local v11, "AppAuth_h":Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    const/16 v16, 0x0

    .line 149
    .local v16, "AppAuth_t":Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    const/4 v12, 0x0

    .local v12, "Resource_h":Lcom/wsomacp/eng/parser/XCPCtxResource;
    const/16 v21, 0x0

    .line 150
    .local v21, "Resource_t":Lcom/wsomacp/eng/parser/XCPCtxResource;
    const/4 v13, 0x0

    .local v13, "Roaming_h":Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    const/16 v22, 0x0

    .line 152
    .local v22, "Roaming_t":Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxApplicationCount()I

    move-result v35

    .line 153
    .local v35, "nCount":I
    invoke-static/range {v35 .. v35}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetApplicationCount(I)V

    .line 155
    if-lez v35, :cond_8

    .line 157
    const/16 v38, 0x0

    .line 158
    const/16 v38, 0x1

    :goto_0
    add-int/lit8 v4, v35, 0x1

    move/from16 v0, v38

    if-ge v0, v4, :cond_8

    .line 160
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxToProxyCount(I)I

    move-result v37

    .line 161
    .local v37, "nPxcount":I
    if-lez v37, :cond_0

    .line 163
    const/16 v30, 0x0

    .line 164
    const/16 v30, 0x1

    :goto_1
    move/from16 v0, v30

    int-to-float v4, v0

    move/from16 v0, v37

    int-to-float v5, v0

    const/high16 v47, 0x3f800000    # 1.0f

    add-float v5, v5, v47

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 166
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxToProxyData(II)Ljava/lang/String;

    move-result-object v46

    .line 167
    .local v46, "toproxyString":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v46

    invoke-static {v7, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->xcp_pxtoproxyAddList(Lcom/wsomacp/eng/parser/XCPCtxToProxy;Lcom/wsomacp/eng/parser/XCPCtxToProxy;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    move-result-object v7

    .line 164
    add-int/lit8 v30, v30, 0x1

    goto :goto_1

    .line 171
    .end local v46    # "toproxyString":Ljava/lang/String;
    :cond_0
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxNapIdCount(I)I

    move-result v36

    .line 172
    .local v36, "nNapidcount":I
    if-lez v36, :cond_1

    .line 174
    const/16 v30, 0x0

    .line 175
    const/16 v30, 0x1

    :goto_2
    move/from16 v0, v30

    int-to-float v4, v0

    move/from16 v0, v36

    int-to-float v5, v0

    const/high16 v47, 0x3f800000    # 1.0f

    add-float v5, v5, v47

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    .line 177
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxNapIdData(II)Ljava/lang/String;

    move-result-object v41

    .line 178
    .local v41, "napIdString":Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v41

    invoke-static {v8, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->xcp_PXNapIdAddList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    move-result-object v8

    .line 175
    add-int/lit8 v30, v30, 0x1

    goto :goto_2

    .line 182
    .end local v41    # "napIdString":Ljava/lang/String;
    :cond_1
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxToAddrCount(I)I

    move-result v40

    .line 183
    .local v40, "nToaddrcount":I
    if-lez v40, :cond_2

    .line 185
    const/16 v30, 0x0

    .line 186
    const/16 v30, 0x1

    :goto_3
    move/from16 v0, v30

    int-to-float v4, v0

    move/from16 v0, v40

    int-to-float v5, v0

    const/high16 v47, 0x3f800000    # 1.0f

    add-float v5, v5, v47

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    .line 188
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxToAddrData(II)Ljava/lang/String;

    move-result-object v45

    .line 189
    .local v45, "toAddrString":Ljava/lang/String;
    move-object/from16 v0, v27

    move-object/from16 v1, v45

    invoke-static {v9, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->xcp_PXToAddrAddList(Lcom/wsomacp/eng/parser/XCPCtxToAddr;Lcom/wsomacp/eng/parser/XCPCtxToAddr;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    move-result-object v9

    .line 186
    add-int/lit8 v30, v30, 0x1

    goto :goto_3

    .line 193
    .end local v45    # "toAddrString":Ljava/lang/String;
    :cond_2
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxAppAddrCount(I)I

    move-result v33

    .line 194
    .local v33, "nAppAddrrcount":I
    if-lez v33, :cond_5

    .line 196
    const/16 v30, 0x0

    .line 197
    const/16 v30, 0x1

    :goto_4
    move/from16 v0, v30

    int-to-float v4, v0

    move/from16 v0, v33

    int-to-float v5, v0

    const/high16 v47, 0x3f800000    # 1.0f

    add-float v5, v5, v47

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    .line 199
    const/16 v18, 0x0

    .local v18, "Port_h":Lcom/wsomacp/eng/parser/XCPCtxPort;
    const/16 v19, 0x0

    .line 200
    .local v19, "Port_t":Lcom/wsomacp/eng/parser/XCPCtxPort;
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxAppPortCount(II)I

    move-result v43

    .line 201
    .local v43, "portcount":I
    if-lez v43, :cond_4

    .line 203
    const/16 v31, 0x0

    .line 204
    const/16 v31, 0x1

    :goto_5
    add-int/lit8 v4, v43, 0x1

    move/from16 v0, v31

    if-ge v0, v4, :cond_4

    .line 206
    const/16 v25, 0x0

    .local v25, "Service_h":Lcom/wsomacp/eng/parser/XCPCtxService;
    const/16 v26, 0x0

    .line 207
    .local v26, "Service_t":Lcom/wsomacp/eng/parser/XCPCtxService;
    move/from16 v0, v38

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxServiceCount(III)I

    move-result v44

    .line 208
    .local v44, "servicecount":I
    if-lez v44, :cond_3

    .line 210
    const/16 v32, 0x0

    .line 211
    const/16 v32, 0x1

    :goto_6
    add-int/lit8 v4, v44, 0x1

    move/from16 v0, v32

    if-ge v0, v4, :cond_3

    .line 213
    move/from16 v0, v38

    move/from16 v1, v30

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-static {v0, v1, v2, v3}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxServiceData(IIII)Ljava/lang/String;

    move-result-object v24

    .line 214
    .local v24, "ServiceString":Ljava/lang/String;
    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxService;->xcp_ServiceAddList(Lcom/wsomacp/eng/parser/XCPCtxService;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxService;

    move-result-object v25

    .line 211
    add-int/lit8 v32, v32, 0x1

    goto :goto_6

    .line 218
    .end local v24    # "ServiceString":Ljava/lang/String;
    :cond_3
    move/from16 v0, v38

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxAppPortData(III)Ljava/lang/String;

    move-result-object v42

    .line 219
    .local v42, "pStr":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v25

    move-object/from16 v3, v42

    invoke-static {v0, v1, v2, v3}, Lcom/wsomacp/eng/parser/XCPCtxPort;->xcp_PXPortAddList(Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPort;

    move-result-object v18

    .line 204
    add-int/lit8 v31, v31, 0x1

    goto :goto_5

    .line 222
    .end local v25    # "Service_h":Lcom/wsomacp/eng/parser/XCPCtxService;
    .end local v26    # "Service_t":Lcom/wsomacp/eng/parser/XCPCtxService;
    .end local v42    # "pStr":Ljava/lang/String;
    .end local v44    # "servicecount":I
    :cond_4
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxAppAddrData(II)[Ljava/lang/String;

    move-result-object v14

    .line 223
    .local v14, "AppAddrString":[Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-static {v10, v15, v14, v0}, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->xcp_PXAppAddrAddList(Lcom/wsomacp/eng/parser/XCPCtxAppAddr;Lcom/wsomacp/eng/parser/XCPCtxAppAddr;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxPort;)Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    move-result-object v10

    .line 197
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_4

    .line 227
    .end local v14    # "AppAddrString":[Ljava/lang/String;
    .end local v18    # "Port_h":Lcom/wsomacp/eng/parser/XCPCtxPort;
    .end local v19    # "Port_t":Lcom/wsomacp/eng/parser/XCPCtxPort;
    .end local v43    # "portcount":I
    :cond_5
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxAppAuthCount(I)I

    move-result v34

    .line 228
    .local v34, "nAuthcount":I
    if-lez v34, :cond_6

    .line 230
    const/16 v30, 0x0

    .line 231
    const/16 v30, 0x1

    :goto_7
    move/from16 v0, v30

    int-to-float v4, v0

    move/from16 v0, v34

    int-to-float v5, v0

    const/high16 v47, 0x3f800000    # 1.0f

    add-float v5, v5, v47

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    .line 233
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxAppAuthData(II)[Ljava/lang/String;

    move-result-object v17

    .line 234
    .local v17, "AuthString":[Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v11, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->xcp_PXAppAuthAddList(Lcom/wsomacp/eng/parser/XCPCtxAppAuth;Lcom/wsomacp/eng/parser/XCPCtxAppAuth;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    move-result-object v11

    .line 231
    add-int/lit8 v30, v30, 0x1

    goto :goto_7

    .line 237
    .end local v17    # "AuthString":[Ljava/lang/String;
    :cond_6
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxResourceCount(I)I

    move-result v39

    .line 238
    .local v39, "nResourcecount":I
    if-lez v39, :cond_7

    .line 240
    const/16 v30, 0x0

    .line 241
    const/16 v30, 0x1

    :goto_8
    move/from16 v0, v30

    int-to-float v4, v0

    move/from16 v0, v39

    int-to-float v5, v0

    const/high16 v47, 0x3f800000    # 1.0f

    add-float v5, v5, v47

    cmpg-float v4, v4, v5

    if-gez v4, :cond_7

    .line 243
    move/from16 v0, v38

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxResourceData(II)[Ljava/lang/String;

    move-result-object v20

    .line 244
    .local v20, "ResourceString":[Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v12, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxResource;->xcp_PXAppResourceAddList(Lcom/wsomacp/eng/parser/XCPCtxResource;Lcom/wsomacp/eng/parser/XCPCtxResource;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxResource;

    move-result-object v12

    .line 241
    add-int/lit8 v30, v30, 0x1

    goto :goto_8

    .line 248
    .end local v20    # "ResourceString":[Ljava/lang/String;
    :cond_7
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetPxRoamingData()[Ljava/lang/String;

    move-result-object v23

    .line 249
    .local v23, "Roamingdata":[Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v13, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->xcp_PXRoamingAddList(Lcom/wsomacp/eng/parser/XCPCtxRoaming;Lcom/wsomacp/eng/parser/XCPCtxRoaming;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    move-result-object v13

    .line 251
    invoke-static/range {v38 .. v38}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->wsomacp_GetApplicationData(I)[Ljava/lang/String;

    move-result-object v6

    .line 252
    .local v6, "AppString":[Ljava/lang/String;
    sget-object v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    sget-object v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CPXApp_t:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    invoke-static/range {v4 .. v13}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->xcp_PXAppAppAddList(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxApplication;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxToProxy;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToAddr;Lcom/wsomacp/eng/parser/XCPCtxAppAddr;Lcom/wsomacp/eng/parser/XCPCtxAppAuth;Lcom/wsomacp/eng/parser/XCPCtxResource;Lcom/wsomacp/eng/parser/XCPCtxRoaming;)Lcom/wsomacp/eng/parser/XCPCtxApplication;

    move-result-object v4

    sput-object v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 253
    const/4 v6, 0x0

    .line 254
    const/4 v7, 0x0

    .line 255
    const/4 v8, 0x0

    .line 256
    const/4 v9, 0x0

    .line 257
    const/4 v10, 0x0

    .line 258
    const/4 v11, 0x0

    .line 259
    const/4 v12, 0x0

    .line 260
    const/4 v13, 0x0

    .line 158
    add-int/lit8 v38, v38, 0x1

    goto/16 :goto_0

    .line 263
    .end local v6    # "AppString":[Ljava/lang/String;
    .end local v23    # "Roamingdata":[Ljava/lang/String;
    .end local v33    # "nAppAddrrcount":I
    .end local v34    # "nAuthcount":I
    .end local v36    # "nNapidcount":I
    .end local v37    # "nPxcount":I
    .end local v39    # "nResourcecount":I
    .end local v40    # "nToaddrcount":I
    :cond_8
    return-void
.end method

.method private static xcp_PXAppAppAddList(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxApplication;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxToProxy;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToAddr;Lcom/wsomacp/eng/parser/XCPCtxAppAddr;Lcom/wsomacp/eng/parser/XCPCtxAppAuth;Lcom/wsomacp/eng/parser/XCPCtxResource;Lcom/wsomacp/eng/parser/XCPCtxRoaming;)Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "appString"    # [Ljava/lang/String;
    .param p3, "toProxyList_h"    # Lcom/wsomacp/eng/parser/XCPCtxToProxy;
    .param p4, "toNapIdList_h"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .param p5, "toAddrList_h"    # Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    .param p6, "appAddr_h"    # Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    .param p7, "appAuth_h"    # Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    .param p8, "resource_h"    # Lcom/wsomacp/eng/parser/XCPCtxResource;
    .param p9, "Roaming_h"    # Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    .prologue
    const/4 v2, 0x0

    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxApplication;-><init>()V

    .line 89
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    if-nez p2, :cond_0

    .line 91
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_TID_HEADER:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_IMEI_HEADER:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_PMS:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_DCS:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_CDMS:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szInternet:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAprotocol:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAaccept:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    .line 108
    :goto_0
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    .line 109
    iput-object p4, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .line 110
    iput-object p5, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    .line 111
    iput-object p6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    .line 112
    iput-object p7, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .line 113
    iput-object p8, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 114
    iput-object p9, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CRoaming:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    .line 116
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 118
    if-nez p0, :cond_1

    .line 120
    move-object p0, v0

    .line 121
    move-object p1, p0

    .line 136
    :goto_1
    return-object p0

    .line 95
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    .line 96
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    .line 97
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    .line 98
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    .line 99
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAaccept:Ljava/lang/String;

    .line 100
    const/4 v1, 0x5

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAprotocol:Ljava/lang/String;

    .line 101
    const/4 v1, 0x6

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szInternet:Ljava/lang/String;

    .line 102
    const/4 v1, 0x7

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_DCS:Ljava/lang/String;

    .line 103
    const/16 v1, 0x8

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_PMS:Ljava/lang/String;

    .line 104
    const/16 v1, 0x9

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_CDMS:Ljava/lang/String;

    .line 105
    const/16 v1, 0xa

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_IMEI_HEADER:Ljava/lang/String;

    .line 106
    const/16 v1, 0xb

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_TID_HEADER:Ljava/lang/String;

    goto :goto_0

    .line 125
    :cond_1
    if-nez p1, :cond_2

    .line 127
    move-object p1, p0

    .line 129
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v1, :cond_2

    .line 130
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    goto :goto_2

    .line 133
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 134
    move-object p1, v0

    goto :goto_1
.end method
