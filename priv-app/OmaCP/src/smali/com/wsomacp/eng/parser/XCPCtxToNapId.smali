.class public Lcom/wsomacp/eng/parser/XCPCtxToNapId;
.super Ljava/lang/Object;
.source "XCPCtxToNapId.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

.field public m_szToNapId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static XCP_PXNapIdListDelete(Lcom/wsomacp/eng/parser/XCPCtxToNapId;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .prologue
    .line 38
    move-object v0, p0

    .line 39
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    :goto_0
    if-eqz v0, :cond_0

    .line 41
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    goto :goto_0

    .line 44
    :cond_0
    const/4 p0, 0x0

    .line 45
    return-void
.end method

.method public static xcp_PXNapIdAddList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .param p2, "NapId"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxToNapId;-><init>()V

    .line 13
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    .line 14
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .line 16
    if-nez p0, :cond_0

    .line 18
    move-object p0, v0

    .line 19
    move-object p1, p0

    .line 33
    :goto_0
    return-object p0

    .line 23
    :cond_0
    if-nez p1, :cond_1

    .line 25
    move-object p1, p0

    .line 26
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v1, :cond_1

    .line 27
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    goto :goto_1

    .line 30
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .line 31
    move-object p1, v0

    goto :goto_0
.end method
