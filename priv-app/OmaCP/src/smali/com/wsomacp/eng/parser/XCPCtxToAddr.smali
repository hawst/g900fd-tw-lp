.class public Lcom/wsomacp/eng/parser/XCPCtxToAddr;
.super Ljava/lang/Object;
.source "XCPCtxToAddr.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

.field public m_szToAddr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PXToAddrAddList(Lcom/wsomacp/eng/parser/XCPCtxToAddr;Lcom/wsomacp/eng/parser/XCPCtxToAddr;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    .param p2, "toAddrString"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "toAddr":Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    .end local v0    # "toAddr":Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxToAddr;-><init>()V

    .line 13
    .restart local v0    # "toAddr":Lcom/wsomacp/eng/parser/XCPCtxToAddr;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    .line 14
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    .line 15
    if-nez p0, :cond_0

    .line 17
    move-object p0, v0

    .line 18
    move-object p1, p0

    .line 33
    :goto_0
    return-object p0

    .line 22
    :cond_0
    if-nez p1, :cond_1

    .line 24
    move-object p1, p0

    .line 25
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v1, :cond_1

    .line 26
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    goto :goto_1

    .line 29
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    .line 30
    move-object p1, v0

    goto :goto_0
.end method
