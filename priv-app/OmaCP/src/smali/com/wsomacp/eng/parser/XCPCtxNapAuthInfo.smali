.class public Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
.super Ljava/lang/Object;
.source "XCPCtxNapAuthInfo.java"


# instance fields
.field public m_CAuthEntity:Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

.field public m_szAuthName:Ljava/lang/String;

.field public m_szAuthSecret:Ljava/lang/String;

.field public m_szAuthType:Ljava/lang/String;

.field public m_szSpi:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_pxAuthInfoList(Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;)Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    .param p2, "AuthInfoString"    # [Ljava/lang/String;
    .param p3, "authEntity_h"    # Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;

    .prologue
    const/4 v2, 0x0

    .line 14
    const/4 v0, 0x0

    .line 15
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;-><init>()V

    .line 17
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    if-nez p2, :cond_0

    .line 19
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szSpi:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthSecret:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    .line 28
    :goto_0
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_CAuthEntity:Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;

    .line 29
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    .line 31
    if-nez p0, :cond_1

    .line 33
    move-object p0, v0

    .line 34
    move-object p1, p0

    .line 47
    :goto_1
    return-object p0

    .line 23
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    .line 24
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthName:Ljava/lang/String;

    .line 25
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthSecret:Ljava/lang/String;

    .line 26
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szSpi:Ljava/lang/String;

    goto :goto_0

    .line 38
    :cond_1
    if-nez p1, :cond_2

    .line 40
    move-object p1, p0

    .line 41
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    if-eqz v1, :cond_2

    .line 42
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    goto :goto_2

    .line 44
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    .line 45
    move-object p1, v0

    goto :goto_1
.end method
