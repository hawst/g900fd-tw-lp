.class public Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
.super Ljava/lang/Object;
.source "XCPCtxAppAddr.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

.field public m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

.field public m_szAddr:Ljava/lang/String;

.field public m_szAddrType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PXAppAddrAddList(Lcom/wsomacp/eng/parser/XCPCtxAppAddr;Lcom/wsomacp/eng/parser/XCPCtxAppAddr;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxPort;)Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    .param p2, "appAddrString"    # [Ljava/lang/String;
    .param p3, "ptPort"    # Lcom/wsomacp/eng/parser/XCPCtxPort;

    .prologue
    .line 12
    const/4 v0, 0x0

    .line 13
    .local v0, "appAddr":Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    .end local v0    # "appAddr":Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;-><init>()V

    .line 15
    .restart local v0    # "appAddr":Lcom/wsomacp/eng/parser/XCPCtxAppAddr;
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    .line 16
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddrType:Ljava/lang/String;

    .line 17
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    .line 18
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    .line 19
    if-nez p0, :cond_0

    .line 21
    move-object p0, v0

    .line 22
    move-object p1, p0

    .line 37
    :goto_0
    return-object p0

    .line 26
    :cond_0
    if-nez p1, :cond_1

    .line 28
    move-object p1, p0

    .line 29
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v1, :cond_1

    .line 30
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    goto :goto_1

    .line 33
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    .line 34
    move-object p1, v0

    goto :goto_0
.end method
