.class public Lcom/wsomacp/eng/parser/XCPCtxPort;
.super Ljava/lang/Object;
.source "XCPCtxPort.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

.field public m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

.field public m_szPortNbr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PXPortAddList(Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPort;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxPort;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxPort;
    .param p2, "service"    # Lcom/wsomacp/eng/parser/XCPCtxService;
    .param p3, "portNbr"    # Ljava/lang/String;

    .prologue
    .line 11
    const/4 v0, 0x0

    .line 12
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxPort;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxPort;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxPort;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxPort;-><init>()V

    .line 14
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxPort;
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    .line 15
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    .line 16
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    .line 17
    if-nez p0, :cond_0

    .line 19
    move-object p0, v0

    .line 20
    move-object p1, p0

    .line 34
    :goto_0
    return-object p0

    .line 24
    :cond_0
    if-nez p1, :cond_1

    .line 26
    move-object p1, p0

    .line 27
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v1, :cond_1

    .line 28
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    goto :goto_1

    .line 31
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    .line 32
    move-object p1, v0

    goto :goto_0
.end method

.method public static xcp_PXPortListDelete(Lcom/wsomacp/eng/parser/XCPCtxPort;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxPort;

    .prologue
    .line 39
    move-object v0, p0

    .line 40
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxPort;
    :goto_0
    if-eqz v0, :cond_0

    .line 42
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    goto :goto_0

    .line 45
    :cond_0
    const/4 p0, 0x0

    .line 46
    return-void
.end method
