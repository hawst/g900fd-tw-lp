.class public Lcom/wsomacp/eng/parser/XCPCtxRoaming;
.super Ljava/lang/Object;
.source "XCPCtxRoaming.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

.field public m_szSYNC_ALLOWED:Ljava/lang/String;

.field public m_szSYNC_DEFAULT:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PXRoamingAddList(Lcom/wsomacp/eng/parser/XCPCtxRoaming;Lcom/wsomacp/eng/parser/XCPCtxRoaming;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    .param p2, "roamingString"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 11
    const/4 v0, 0x0

    .line 12
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxRoaming;-><init>()V

    .line 14
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxRoaming;
    if-nez p2, :cond_0

    .line 16
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_szSYNC_DEFAULT:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_szSYNC_ALLOWED:Ljava/lang/String;

    .line 24
    :goto_0
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    .line 26
    if-nez p0, :cond_1

    .line 28
    move-object p0, v0

    .line 29
    move-object p1, p0

    .line 44
    :goto_1
    return-object p0

    .line 20
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_szSYNC_ALLOWED:Ljava/lang/String;

    .line 21
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_szSYNC_DEFAULT:Ljava/lang/String;

    goto :goto_0

    .line 33
    :cond_1
    if-nez p1, :cond_2

    .line 35
    move-object p1, p0

    .line 36
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    if-eqz v1, :cond_2

    .line 37
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    goto :goto_2

    .line 40
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    .line 41
    move-object p1, v0

    goto :goto_1
.end method
