.class public Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
.super Ljava/lang/Object;
.source "XCPCtxPortNbr.java"


# instance fields
.field m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

.field m_szPortNbr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_ProvPortNbrAddList(Lcom/wsomacp/eng/parser/XCPCtxPortNbr;Lcom/wsomacp/eng/parser/XCPCtxPortNbr;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    .param p2, "portNbr"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "PortNbr":Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    .end local v0    # "PortNbr":Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;-><init>()V

    .line 12
    .restart local v0    # "PortNbr":Lcom/wsomacp/eng/parser/XCPCtxPortNbr;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;->m_szPortNbr:Ljava/lang/String;

    .line 13
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    .line 15
    if-nez p0, :cond_0

    .line 17
    move-object p0, v0

    .line 18
    move-object p1, p0

    .line 31
    :goto_0
    return-object p0

    .line 22
    :cond_0
    if-nez p1, :cond_1

    .line 24
    move-object p1, p0

    .line 25
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    if-eqz v1, :cond_1

    .line 26
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    goto :goto_1

    .line 28
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPortNbr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPortNbr;

    .line 29
    move-object p1, v0

    goto :goto_0
.end method
