.class public Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
.super Ljava/lang/Object;
.source "XCPCtxBootstrap.java"


# static fields
.field public static g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

.field static m_CPXBoot_t:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;


# instance fields
.field public m_CNetworkList:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

.field public m_CProxyIdList:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

.field public m_szContextAllow:Ljava/lang/String;

.field public m_szCountry:Ljava/lang/String;

.field public m_szName:Ljava/lang/String;

.field public m_szProvUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 27
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CPXBoot_t:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetBootstrapData(I)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxBootstrapCount()I
.end method

.method static native wsomacp_GetPxNetworkCount(I)I
.end method

.method static native wsomacp_GetPxNetworkData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxProxyIdCount(I)I
.end method

.method static native wsomacp_GetPxProxyIdData(II)Ljava/lang/String;
.end method

.method private static xcpBootstrapAddList(Lcom/wsomacp/eng/parser/XCPCtxBootstrap;Lcom/wsomacp/eng/parser/XCPCtxBootstrap;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxProxyId;Lcom/wsomacp/eng/parser/XCPCtxNetwork;)Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
    .param p2, "bootstrapdata"    # [Ljava/lang/String;
    .param p3, "proxyIdList_h"    # Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    .param p4, "network_h"    # Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    .prologue
    const/4 v2, 0x0

    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .end local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;-><init>()V

    .line 77
    .restart local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxBootstrap;
    if-nez p2, :cond_0

    .line 79
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szContextAllow:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szProvUrl:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szCountry:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szName:Ljava/lang/String;

    .line 89
    :goto_0
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CProxyIdList:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    .line 90
    iput-object p4, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CNetworkList:Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    .line 91
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 93
    if-nez p0, :cond_1

    .line 95
    move-object p0, v0

    .line 96
    move-object p1, p0

    .line 111
    :goto_1
    return-object p0

    .line 83
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szName:Ljava/lang/String;

    .line 84
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szCountry:Ljava/lang/String;

    .line 85
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szProvUrl:Ljava/lang/String;

    .line 86
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szContextAllow:Ljava/lang/String;

    goto :goto_0

    .line 100
    :cond_1
    if-nez p1, :cond_2

    .line 102
    move-object p1, p0

    .line 103
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    if-eqz v1, :cond_2

    .line 104
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    goto :goto_2

    .line 107
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 108
    move-object p1, v0

    goto :goto_1
.end method

.method public static xcp_Bootstrap()V
    .locals 15

    .prologue
    const/high16 v14, 0x3f800000    # 1.0f

    .line 31
    const/4 v7, 0x0

    .line 32
    .local v7, "i":I
    const/4 v8, 0x0

    .line 33
    .local v8, "nBootstrapindex":I
    const/4 v5, 0x0

    .local v5, "ProxyIdList_h":Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    const/4 v6, 0x0

    .line 34
    .local v6, "ProxyIdList_t":Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    const/4 v3, 0x0

    .local v3, "Network_h":Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    const/4 v4, 0x0

    .line 36
    .local v4, "Network_t":Lcom/wsomacp/eng/parser/XCPCtxNetwork;
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->wsomacp_GetPxBootstrapCount()I

    move-result v9

    .line 37
    .local v9, "nCount":I
    if-lez v9, :cond_2

    .line 39
    const/4 v8, 0x0

    .line 40
    const/4 v8, 0x1

    :goto_0
    add-int/lit8 v12, v9, 0x1

    if-ge v8, v12, :cond_2

    .line 42
    invoke-static {v8}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->wsomacp_GetPxProxyIdCount(I)I

    move-result v11

    .line 43
    .local v11, "nPxidcount":I
    if-lez v11, :cond_0

    .line 45
    const/4 v7, 0x0

    .line 46
    const/4 v7, 0x1

    :goto_1
    int-to-float v12, v7

    int-to-float v13, v11

    add-float/2addr v13, v14

    cmpg-float v12, v12, v13

    if-gez v12, :cond_0

    .line 48
    invoke-static {v8, v7}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->wsomacp_GetPxProxyIdData(II)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "NdtworkString":Ljava/lang/String;
    invoke-static {v5, v6, v1}, Lcom/wsomacp/eng/parser/XCPCtxProxyId;->xcp_PxIdAddList(Lcom/wsomacp/eng/parser/XCPCtxProxyId;Lcom/wsomacp/eng/parser/XCPCtxProxyId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    move-result-object v5

    .line 46
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 53
    .end local v1    # "NdtworkString":Ljava/lang/String;
    :cond_0
    invoke-static {v8}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->wsomacp_GetPxNetworkCount(I)I

    move-result v10

    .line 54
    .local v10, "nNetworkcount":I
    if-lez v10, :cond_1

    .line 56
    const/4 v7, 0x0

    .line 57
    const/4 v7, 0x1

    :goto_2
    int-to-float v12, v7

    int-to-float v13, v10

    add-float/2addr v13, v14

    cmpg-float v12, v12, v13

    if-gez v12, :cond_1

    .line 59
    invoke-static {v8, v7}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->wsomacp_GetPxNetworkData(II)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "NetworkString":Ljava/lang/String;
    invoke-static {v3, v4, v2}, Lcom/wsomacp/eng/parser/XCPCtxNetwork;->xcp_pxNetworkAddList(Lcom/wsomacp/eng/parser/XCPCtxNetwork;Lcom/wsomacp/eng/parser/XCPCtxNetwork;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxNetwork;

    move-result-object v3

    .line 57
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 64
    .end local v2    # "NetworkString":Ljava/lang/String;
    :cond_1
    invoke-static {v8}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->wsomacp_GetBootstrapData(I)[Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "Bootstrapdata":[Ljava/lang/String;
    sget-object v12, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    sget-object v13, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_CPXBoot_t:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    invoke-static {v12, v13, v0, v5, v3}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->xcpBootstrapAddList(Lcom/wsomacp/eng/parser/XCPCtxBootstrap;Lcom/wsomacp/eng/parser/XCPCtxBootstrap;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxProxyId;Lcom/wsomacp/eng/parser/XCPCtxNetwork;)Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    move-result-object v12

    sput-object v12, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 66
    const/4 v0, 0x0

    .line 67
    const/4 v5, 0x0

    .line 68
    const/4 v3, 0x0

    .line 40
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "Bootstrapdata":[Ljava/lang/String;
    .end local v10    # "nNetworkcount":I
    .end local v11    # "nPxidcount":I
    :cond_2
    return-void
.end method
