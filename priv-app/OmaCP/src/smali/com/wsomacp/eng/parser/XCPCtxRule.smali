.class public Lcom/wsomacp/eng/parser/XCPCtxRule;
.super Ljava/lang/Object;
.source "XCPCtxRule.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRule;

.field public m_szRule:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_pxCtxRuleAddList(Lcom/wsomacp/eng/parser/XCPCtxRule;Lcom/wsomacp/eng/parser/XCPCtxRule;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxRule;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxRule;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxRule;
    .param p2, "pRuleString"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "CtxRule":Lcom/wsomacp/eng/parser/XCPCtxRule;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxRule;

    .end local v0    # "CtxRule":Lcom/wsomacp/eng/parser/XCPCtxRule;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxRule;-><init>()V

    .line 13
    .restart local v0    # "CtxRule":Lcom/wsomacp/eng/parser/XCPCtxRule;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxRule;->m_szRule:Ljava/lang/String;

    .line 15
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxRule;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRule;

    .line 16
    if-nez p0, :cond_0

    .line 18
    move-object p0, v0

    .line 19
    move-object p1, p0

    .line 34
    :goto_0
    return-object p0

    .line 23
    :cond_0
    if-nez p1, :cond_1

    .line 25
    move-object p1, p0

    .line 26
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxRule;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRule;

    if-eqz v1, :cond_1

    .line 27
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxRule;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRule;

    goto :goto_1

    .line 30
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxRule;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxRule;

    .line 31
    move-object p1, v0

    goto :goto_0
.end method
