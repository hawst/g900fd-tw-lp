.class public Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
.super Ljava/lang/Object;
.source "XCPCtxPxAuthInfo.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

.field public m_szPxAuthId:Ljava/lang/String;

.field public m_szPxAuthPw:Ljava/lang/String;

.field public m_szPxAuthType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PxAuthInfoAddList(Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    .param p2, "data"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 12
    const/4 v0, 0x0

    .line 13
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;-><init>()V

    .line 15
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    if-nez p2, :cond_0

    .line 17
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_szPxAuthPw:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_szPxAuthId:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_szPxAuthType:Ljava/lang/String;

    .line 25
    :goto_0
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    .line 27
    if-nez p0, :cond_1

    .line 29
    move-object p0, v0

    .line 30
    move-object p1, p0

    .line 44
    :goto_1
    return-object p0

    .line 21
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_szPxAuthType:Ljava/lang/String;

    .line 22
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_szPxAuthId:Ljava/lang/String;

    .line 23
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_szPxAuthPw:Ljava/lang/String;

    goto :goto_0

    .line 34
    :cond_1
    if-nez p1, :cond_2

    .line 36
    move-object p1, p0

    .line 37
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    if-eqz v1, :cond_2

    .line 38
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    goto :goto_2

    .line 41
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    .line 42
    move-object p1, v0

    goto :goto_1
.end method

.method public static xcp_PxAuthInfoListDelete(Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    .prologue
    .line 49
    move-object v0, p0

    .line 50
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    :goto_0
    if-eqz v0, :cond_0

    .line 52
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    goto :goto_0

    .line 55
    :cond_0
    const/4 p0, 0x0

    .line 56
    return-void
.end method
