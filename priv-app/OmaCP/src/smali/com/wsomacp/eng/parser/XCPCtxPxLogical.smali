.class public Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
.super Ljava/lang/Object;
.source "XCPCtxPxLogical.java"


# static fields
.field public static PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

.field static PX_t:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;


# instance fields
.field public m_CDomainList:Lcom/wsomacp/eng/parser/XCPCtxDomain;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

.field public m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

.field public m_CPxAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

.field public m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

.field public m_szBasAuthId:Ljava/lang/String;

.field public m_szBasAuthPw:Ljava/lang/String;

.field public m_szMaster:Ljava/lang/String;

.field public m_szName:Ljava/lang/String;

.field public m_szPpgAuthType:Ljava/lang/String;

.field public m_szProxyId:Ljava/lang/String;

.field public m_szProxyProviderId:Ljava/lang/String;

.field public m_szProxyPw:Ljava/lang/String;

.field public m_szPullEnabled:Ljava/lang/String;

.field public m_szPushEnabled:Ljava/lang/String;

.field public m_szStartPage:Ljava/lang/String;

.field public m_szTrust:Ljava/lang/String;

.field public m_szWspVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 67
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_t:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetProxyData(I)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxAuthInfoCount(I)I
.end method

.method static native wsomacp_GetPxAuthInfoData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxDomainCount(I)I
.end method

.method static native wsomacp_GetPxDomainData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxLogicalCount()I
.end method

.method static native wsomacp_GetPxPhsicalDomainData(III)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxPhsicalNapIdCount(II)I
.end method

.method static native wsomacp_GetPxPhysicalCount(I)I
.end method

.method static native wsomacp_GetPxPhysicalData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxPhysicalDomainCount(II)I
.end method

.method static native wsomacp_GetPxPhysicalNapIdData(III)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxPhysicalPortCount(II)I
.end method

.method static native wsomacp_GetPxPhysicalPortData(III)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxPhysicalPortServiceCount(III)I
.end method

.method static native wsomacp_GetPxPhysicalPortServiceData(IIII)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxPortCount(I)I
.end method

.method static native wsomacp_GetPxPortData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxPortServiceCount(II)I
.end method

.method static native wsomacp_GetPxPortServiceData(III)Ljava/lang/String;
.end method

.method public static xcpPXAddList(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;)Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p2, "data"    # [Ljava/lang/String;
    .param p3, "domain"    # Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .param p4, "pxauthinfo"    # Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    .param p5, "port"    # Lcom/wsomacp/eng/parser/XCPCtxPort;
    .param p6, "physical"    # Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    .prologue
    const/4 v2, 0x0

    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .end local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;-><init>()V

    .line 74
    .restart local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    if-nez p2, :cond_0

    .line 76
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szPullEnabled:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szPushEnabled:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szWspVersion:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szBasAuthPw:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szBasAuthId:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szStartPage:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szMaster:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szTrust:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyProviderId:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szPpgAuthType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyPw:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    .line 94
    :goto_0
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CDomainList:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .line 95
    iput-object p4, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    .line 96
    iput-object p5, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    .line 97
    iput-object p6, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    .line 98
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 99
    if-nez p0, :cond_1

    .line 101
    move-object p0, v0

    .line 102
    move-object p1, p0

    .line 117
    :goto_1
    return-object p0

    .line 80
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    .line 81
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyPw:Ljava/lang/String;

    .line 82
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szPpgAuthType:Ljava/lang/String;

    .line 83
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyProviderId:Ljava/lang/String;

    .line 84
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szName:Ljava/lang/String;

    .line 85
    const/4 v1, 0x5

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szTrust:Ljava/lang/String;

    .line 86
    const/4 v1, 0x6

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szMaster:Ljava/lang/String;

    .line 87
    const/4 v1, 0x7

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szStartPage:Ljava/lang/String;

    .line 88
    const/16 v1, 0x8

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szBasAuthId:Ljava/lang/String;

    .line 89
    const/16 v1, 0x9

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szBasAuthPw:Ljava/lang/String;

    .line 90
    const/16 v1, 0xa

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szWspVersion:Ljava/lang/String;

    .line 91
    const/16 v1, 0xb

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szPushEnabled:Ljava/lang/String;

    .line 92
    const/16 v1, 0xc

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szPullEnabled:Ljava/lang/String;

    goto :goto_0

    .line 106
    :cond_1
    if-nez p1, :cond_2

    .line 108
    move-object p1, p0

    .line 109
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v1, :cond_2

    .line 110
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    goto :goto_2

    .line 113
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 114
    move-object p1, v0

    goto :goto_1
.end method

.method public static xcpPXAddListDelete(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .prologue
    .line 122
    move-object v0, p0

    .line 123
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    :goto_0
    if-eqz v0, :cond_0

    .line 125
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    goto :goto_0

    .line 128
    :cond_0
    const/4 p0, 0x0

    .line 129
    return-void
.end method

.method public static xcp_PxLogic()V
    .locals 47

    .prologue
    .line 133
    const/16 v25, 0x0

    .local v25, "i":I
    const/16 v26, 0x0

    .local v26, "j":I
    const/16 v27, 0x0

    .line 134
    .local v27, "k":I
    const/16 v42, 0x0

    .line 135
    .local v42, "pxindex":I
    const/4 v13, 0x0

    .local v13, "d_h":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    const/16 v21, 0x0

    .line 136
    .local v21, "d_t":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    const/4 v14, 0x0

    .local v14, "Auth_h":Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    const/16 v18, 0x0

    .line 137
    .local v18, "Auth_t":Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;
    const/4 v4, 0x0

    .local v4, "phy_h":Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    const/4 v5, 0x0

    .line 138
    .local v5, "phy_t":Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    const/4 v15, 0x0

    .local v15, "p_h":Lcom/wsomacp/eng/parser/XCPCtxPort;
    const/16 v34, 0x0

    .line 140
    .local v34, "p_t":Lcom/wsomacp/eng/parser/XCPCtxPort;
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxLogicalCount()I

    move-result v20

    .line 141
    .local v20, "count":I
    if-lez v20, :cond_9

    .line 143
    const/16 v42, 0x0

    .line 144
    const/16 v42, 0x1

    :goto_0
    add-int/lit8 v10, v20, 0x1

    move/from16 v0, v42

    if-ge v0, v10, :cond_9

    .line 146
    invoke-static/range {v42 .. v42}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxDomainCount(I)I

    move-result v23

    .line 147
    .local v23, "domaincount":I
    if-lez v23, :cond_0

    .line 149
    const/16 v25, 0x0

    .line 150
    const/16 v25, 0x1

    :goto_1
    move/from16 v0, v25

    int-to-float v10, v0

    move/from16 v0, v23

    int-to-float v11, v0

    const/high16 v16, 0x3f800000    # 1.0f

    add-float v11, v11, v16

    cmpg-float v10, v10, v11

    if-gez v10, :cond_0

    .line 152
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxDomainData(II)Ljava/lang/String;

    move-result-object v24

    .line 153
    .local v24, "domainstring":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v13, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxDomain;->xcp_pxDomainAddList(Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxDomain;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxDomain;

    move-result-object v13

    .line 150
    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    .line 157
    .end local v24    # "domainstring":Ljava/lang/String;
    :cond_0
    invoke-static/range {v42 .. v42}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxAuthInfoCount(I)I

    move-result v19

    .line 158
    .local v19, "Authincount":I
    if-lez v19, :cond_1

    .line 160
    const/16 v17, 0x0

    .line 161
    .local v17, "AuthInfoStr":[Ljava/lang/String;
    const/16 v25, 0x0

    .line 162
    const/16 v25, 0x1

    :goto_2
    add-int/lit8 v10, v19, 0x1

    move/from16 v0, v25

    if-ge v0, v10, :cond_1

    .line 164
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxAuthInfoData(II)[Ljava/lang/String;

    move-result-object v17

    .line 165
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;->xcp_PxAuthInfoAddList(Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;

    move-result-object v14

    .line 162
    add-int/lit8 v25, v25, 0x1

    goto :goto_2

    .line 169
    .end local v17    # "AuthInfoStr":[Ljava/lang/String;
    :cond_1
    invoke-static/range {v42 .. v42}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPortCount(I)I

    move-result v41

    .line 170
    .local v41, "portcount":I
    if-lez v41, :cond_3

    .line 172
    const/16 v25, 0x0

    .line 173
    const/16 v25, 0x1

    :goto_3
    add-int/lit8 v10, v41, 0x1

    move/from16 v0, v25

    if-ge v0, v10, :cond_3

    .line 175
    const/16 v43, 0x0

    .local v43, "s_h":Lcom/wsomacp/eng/parser/XCPCtxService;
    const/16 v44, 0x0

    .line 176
    .local v44, "s_t":Lcom/wsomacp/eng/parser/XCPCtxService;
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPortServiceCount(II)I

    move-result v45

    .line 177
    .local v45, "servicecount":I
    if-lez v45, :cond_2

    .line 179
    const/16 v26, 0x0

    .line 180
    const/16 v26, 0x1

    :goto_4
    add-int/lit8 v10, v45, 0x1

    move/from16 v0, v26

    if-ge v0, v10, :cond_2

    .line 182
    move/from16 v0, v42

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPortServiceData(III)Ljava/lang/String;

    move-result-object v46

    .line 183
    .local v46, "servicestr":Ljava/lang/String;
    move-object/from16 v0, v43

    move-object/from16 v1, v44

    move-object/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxService;->xcp_ServiceAddList(Lcom/wsomacp/eng/parser/XCPCtxService;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxService;

    move-result-object v43

    .line 180
    add-int/lit8 v26, v26, 0x1

    goto :goto_4

    .line 186
    .end local v46    # "servicestr":Ljava/lang/String;
    :cond_2
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPortData(II)Ljava/lang/String;

    move-result-object v40

    .line 187
    .local v40, "port":Ljava/lang/String;
    move-object/from16 v0, v34

    move-object/from16 v1, v43

    move-object/from16 v2, v40

    invoke-static {v15, v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPort;->xcp_PXPortAddList(Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPort;

    move-result-object v15

    .line 173
    add-int/lit8 v25, v25, 0x1

    goto :goto_3

    .line 191
    .end local v40    # "port":Ljava/lang/String;
    .end local v43    # "s_h":Lcom/wsomacp/eng/parser/XCPCtxService;
    .end local v44    # "s_t":Lcom/wsomacp/eng/parser/XCPCtxService;
    .end local v45    # "servicecount":I
    :cond_3
    invoke-static/range {v42 .. v42}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalCount(I)I

    move-result v36

    .line 192
    .local v36, "physicalcount":I
    if-lez v36, :cond_8

    .line 195
    const/16 v25, 0x0

    .line 196
    const/16 v25, 0x1

    :goto_5
    add-int/lit8 v10, v36, 0x1

    move/from16 v0, v25

    if-ge v0, v10, :cond_8

    .line 198
    const/4 v8, 0x0

    .local v8, "nap_h":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    const/16 v28, 0x0

    .line 199
    .local v28, "nap_t":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    const/4 v9, 0x0

    .local v9, "p_h1":Lcom/wsomacp/eng/parser/XCPCtxPort;
    const/16 v35, 0x0

    .line 200
    .local v35, "p_t1":Lcom/wsomacp/eng/parser/XCPCtxPort;
    const/4 v7, 0x0

    .local v7, "d_h1":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    const/16 v22, 0x0

    .line 201
    .local v22, "d_t1":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalDomainCount(II)I

    move-result v37

    .line 202
    .local v37, "physicaldomaincount":I
    if-lez v37, :cond_4

    .line 204
    const/16 v26, 0x0

    .line 205
    const/16 v26, 0x1

    :goto_6
    add-int/lit8 v10, v37, 0x1

    move/from16 v0, v26

    if-ge v0, v10, :cond_4

    .line 207
    move/from16 v0, v42

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhsicalDomainData(III)Ljava/lang/String;

    move-result-object v30

    .line 208
    .local v30, "pStr":Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-static {v7, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxDomain;->xcp_pxDomainAddList(Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxDomain;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxDomain;

    move-result-object v7

    .line 205
    add-int/lit8 v26, v26, 0x1

    goto :goto_6

    .line 212
    .end local v30    # "pStr":Ljava/lang/String;
    :cond_4
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhsicalNapIdCount(II)I

    move-result v38

    .line 213
    .local v38, "physicalnapidcount":I
    if-lez v38, :cond_5

    .line 215
    const/16 v26, 0x0

    .line 216
    const/16 v26, 0x1

    :goto_7
    add-int/lit8 v10, v38, 0x1

    move/from16 v0, v26

    if-ge v0, v10, :cond_5

    .line 218
    move/from16 v0, v42

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalNapIdData(III)Ljava/lang/String;

    move-result-object v29

    .line 219
    .local v29, "napidStr":Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-static {v8, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->xcp_PXNapIdAddList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    move-result-object v8

    .line 216
    add-int/lit8 v26, v26, 0x1

    goto :goto_7

    .line 223
    .end local v29    # "napidStr":Ljava/lang/String;
    :cond_5
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalPortCount(II)I

    move-result v39

    .line 224
    .local v39, "physicalportcount":I
    if-lez v39, :cond_7

    .line 226
    const/16 v26, 0x0

    .line 227
    const/16 v26, 0x1

    :goto_8
    add-int/lit8 v10, v39, 0x1

    move/from16 v0, v26

    if-ge v0, v10, :cond_7

    .line 229
    const/16 v43, 0x0

    .restart local v43    # "s_h":Lcom/wsomacp/eng/parser/XCPCtxService;
    const/16 v44, 0x0

    .line 230
    .restart local v44    # "s_t":Lcom/wsomacp/eng/parser/XCPCtxService;
    move/from16 v0, v42

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalPortServiceCount(III)I

    move-result v32

    .line 231
    .local v32, "p_p_s_count":I
    if-lez v32, :cond_6

    .line 233
    const/16 v27, 0x0

    .line 234
    const/16 v27, 0x1

    :goto_9
    add-int/lit8 v10, v32, 0x1

    move/from16 v0, v27

    if-ge v0, v10, :cond_6

    .line 236
    move/from16 v0, v42

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalPortServiceData(IIII)Ljava/lang/String;

    move-result-object v31

    .line 237
    .local v31, "p_Str":Ljava/lang/String;
    move-object/from16 v0, v43

    move-object/from16 v1, v44

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxService;->xcp_ServiceAddList(Lcom/wsomacp/eng/parser/XCPCtxService;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxService;

    move-result-object v43

    .line 234
    add-int/lit8 v27, v27, 0x1

    goto :goto_9

    .line 241
    .end local v31    # "p_Str":Ljava/lang/String;
    :cond_6
    move/from16 v0, v42

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalPortData(III)Ljava/lang/String;

    move-result-object v33

    .line 242
    .local v33, "p_port":Ljava/lang/String;
    move-object/from16 v0, v35

    move-object/from16 v1, v43

    move-object/from16 v2, v33

    invoke-static {v9, v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxPort;->xcp_PXPortAddList(Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxService;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxPort;

    move-result-object v9

    .line 227
    add-int/lit8 v26, v26, 0x1

    goto :goto_8

    .line 245
    .end local v32    # "p_p_s_count":I
    .end local v33    # "p_port":Ljava/lang/String;
    .end local v43    # "s_h":Lcom/wsomacp/eng/parser/XCPCtxService;
    .end local v44    # "s_t":Lcom/wsomacp/eng/parser/XCPCtxService;
    :cond_7
    move/from16 v0, v42

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetPxPhysicalData(II)[Ljava/lang/String;

    move-result-object v6

    .line 246
    .local v6, "data":[Ljava/lang/String;
    invoke-static/range {v4 .. v9}, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->xcp_PxPhysicalAddList(Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxPort;)Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    move-result-object v4

    .line 196
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_5

    .line 250
    .end local v6    # "data":[Ljava/lang/String;
    .end local v7    # "d_h1":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .end local v8    # "nap_h":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .end local v9    # "p_h1":Lcom/wsomacp/eng/parser/XCPCtxPort;
    .end local v22    # "d_t1":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .end local v28    # "nap_t":Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .end local v35    # "p_t1":Lcom/wsomacp/eng/parser/XCPCtxPort;
    .end local v37    # "physicaldomaincount":I
    .end local v38    # "physicalnapidcount":I
    .end local v39    # "physicalportcount":I
    :cond_8
    invoke-static/range {v42 .. v42}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->wsomacp_GetProxyData(I)[Ljava/lang/String;

    move-result-object v12

    .line 251
    .local v12, "pxdata":[Ljava/lang/String;
    sget-object v10, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    sget-object v11, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_t:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    move-object/from16 v16, v4

    invoke-static/range {v10 .. v16}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->xcpPXAddList(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxPxAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxPort;Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;)Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    move-result-object v10

    sput-object v10, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 253
    const/4 v12, 0x0

    .line 254
    const/4 v13, 0x0

    .line 255
    const/4 v14, 0x0

    .line 256
    const/4 v15, 0x0

    .line 257
    const/4 v4, 0x0

    .line 144
    add-int/lit8 v42, v42, 0x1

    goto/16 :goto_0

    .line 260
    .end local v12    # "pxdata":[Ljava/lang/String;
    .end local v19    # "Authincount":I
    .end local v23    # "domaincount":I
    .end local v36    # "physicalcount":I
    .end local v41    # "portcount":I
    :cond_9
    return-void
.end method
