.class public Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;
.super Ljava/lang/Object;
.source "XCPCtxClientIdentity.java"


# static fields
.field public static g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;


# instance fields
.field public m_szClientId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetPxClientIdentityData()Ljava/lang/String;
.end method

.method public static xcp_ClientIdentity()V
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->wsomacp_GetPxClientIdentityData()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "Id":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 18
    new-instance v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    invoke-direct {v1}, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;-><init>()V

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    .line 19
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    iput-object v0, v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->m_szClientId:Ljava/lang/String;

    .line 25
    :goto_0
    return-void

    .line 23
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    goto :goto_0
.end method
