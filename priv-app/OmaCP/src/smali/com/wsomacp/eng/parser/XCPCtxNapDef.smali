.class public Lcom/wsomacp/eng/parser/XCPCtxNapDef;
.super Ljava/lang/Object;
.source "XCPCtxNapDef.java"


# static fields
.field public static g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

.field static m_CPXDef_t:Lcom/wsomacp/eng/parser/XCPCtxNapDef;


# instance fields
.field public m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

.field public m_CDnsAddrList:Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;

.field public m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

.field public m_CValidity:Lcom/wsomacp/eng/parser/XCPCtxValidity;

.field public m_szCallType:Ljava/lang/String;

.field public m_szDeliverErrSdu:Ljava/lang/String;

.field public m_szDeliverOrder:Ljava/lang/String;

.field public m_szDnLinkSpeed:Ljava/lang/String;

.field public m_szFirstRetryTimeOut:Ljava/lang/String;

.field public m_szGuaranteedBitrateDnLink:Ljava/lang/String;

.field public m_szGuaranteedBitrateUpLink:Ljava/lang/String;

.field public m_szInit:Ljava/lang/String;

.field public m_szInternet:Ljava/lang/String;

.field public m_szLinger:Ljava/lang/String;

.field public m_szLinkSpeed:Ljava/lang/String;

.field public m_szLocalAddr:Ljava/lang/String;

.field public m_szLocalAddrType:Ljava/lang/String;

.field public m_szMaxBitrateDnLink:Ljava/lang/String;

.field public m_szMaxBitrateUpLink:Ljava/lang/String;

.field public m_szMaxNumRetry:Ljava/lang/String;

.field public m_szMaxSduSize:Ljava/lang/String;

.field public m_szName:Ljava/lang/String;

.field public m_szNapAddr:Ljava/lang/String;

.field public m_szNapAddrType:Ljava/lang/String;

.field public m_szNapId:Ljava/lang/String;

.field public m_szReregThreshold:Ljava/lang/String;

.field public m_szResidualBer:Ljava/lang/String;

.field public m_szSduErrorRatio:Ljava/lang/String;

.field public m_szTbit:Ljava/lang/String;

.field public m_szTrafficClass:Ljava/lang/String;

.field public m_szTrafficHandlPrio:Ljava/lang/String;

.field public m_szTransferDelay:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 67
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CPXDef_t:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetPxAuthInfopAuthEntityData(III)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxBearerCount(I)I
.end method

.method static native wsomacp_GetPxBearerData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxDnsAddrCount(I)I
.end method

.method static native wsomacp_GetPxDnsAddrData(II)Ljava/lang/String;
.end method

.method static native wsomacp_GetPxNapAuthEntityCount(II)I
.end method

.method static native wsomacp_GetPxNapAuthInfoCount(I)I
.end method

.method static native wsomacp_GetPxNapAuthInfoData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxNapDefCount()I
.end method

.method static native wsomacp_GetPxNapDefData(I)[Ljava/lang/String;
.end method

.method static native wsomacp_GetPxNapValidityCount(I)I
.end method

.method static native wsomacp_GetPxValidityData(II)[Ljava/lang/String;
.end method

.method static native wsomacp_SetPxNapDefIndex(I)V
.end method

.method private static xcpNapDefAddList(Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/parser/XCPCtxNapDef;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;Lcom/wsomacp/eng/parser/XCPCtxBearer;Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxValidity;)Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p2, "napDefdata"    # [Ljava/lang/String;
    .param p3, "dnsAddrList_h"    # Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;
    .param p4, "bearerList_h"    # Lcom/wsomacp/eng/parser/XCPCtxBearer;
    .param p5, "napAuthInfo_h"    # Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    .param p6, "validity_h"    # Lcom/wsomacp/eng/parser/XCPCtxValidity;

    .prologue
    const/4 v2, 0x0

    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .end local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;-><init>()V

    .line 73
    .restart local v0    # "pxTmp":Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    if-nez p2, :cond_0

    .line 75
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szInit:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTbit:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szReregThreshold:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szFirstRetryTimeOut:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxNumRetry:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szGuaranteedBitrateDnLink:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szGuaranteedBitrateUpLink:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTransferDelay:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTrafficHandlPrio:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szSduErrorRatio:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szResidualBer:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxBitrateDnLink:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxBitrateUpLink:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxSduSize:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTrafficClass:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szDeliverOrder:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szDeliverErrSdu:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinger:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinkSpeed:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLocalAddrType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLocalAddr:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szCallType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddrType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szInternet:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    .line 108
    :goto_0
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CDnsAddrList:Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;

    .line 109
    iput-object p4, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    .line 110
    iput-object p5, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    .line 111
    iput-object p6, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CValidity:Lcom/wsomacp/eng/parser/XCPCtxValidity;

    .line 113
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 115
    if-nez p0, :cond_1

    .line 117
    move-object p0, v0

    .line 118
    move-object p1, p0

    .line 133
    :goto_1
    return-object p0

    .line 79
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    .line 80
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    .line 81
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szInternet:Ljava/lang/String;

    .line 82
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    .line 83
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddrType:Ljava/lang/String;

    .line 84
    const/4 v1, 0x5

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szCallType:Ljava/lang/String;

    .line 85
    const/4 v1, 0x6

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLocalAddr:Ljava/lang/String;

    .line 86
    const/4 v1, 0x7

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLocalAddrType:Ljava/lang/String;

    .line 87
    const/16 v1, 0x8

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinkSpeed:Ljava/lang/String;

    .line 88
    const/16 v1, 0x9

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szDnLinkSpeed:Ljava/lang/String;

    .line 89
    const/16 v1, 0xa

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinger:Ljava/lang/String;

    .line 90
    const/16 v1, 0xb

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szDeliverErrSdu:Ljava/lang/String;

    .line 91
    const/16 v1, 0xc

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szDeliverOrder:Ljava/lang/String;

    .line 92
    const/16 v1, 0xd

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTrafficClass:Ljava/lang/String;

    .line 93
    const/16 v1, 0xe

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxSduSize:Ljava/lang/String;

    .line 94
    const/16 v1, 0xf

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxBitrateUpLink:Ljava/lang/String;

    .line 95
    const/16 v1, 0x10

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxBitrateDnLink:Ljava/lang/String;

    .line 96
    const/16 v1, 0x11

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szResidualBer:Ljava/lang/String;

    .line 97
    const/16 v1, 0x12

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szSduErrorRatio:Ljava/lang/String;

    .line 98
    const/16 v1, 0x13

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTrafficHandlPrio:Ljava/lang/String;

    .line 99
    const/16 v1, 0x14

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTransferDelay:Ljava/lang/String;

    .line 100
    const/16 v1, 0x15

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szGuaranteedBitrateUpLink:Ljava/lang/String;

    .line 101
    const/16 v1, 0x16

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szGuaranteedBitrateDnLink:Ljava/lang/String;

    .line 102
    const/16 v1, 0x17

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szMaxNumRetry:Ljava/lang/String;

    .line 103
    const/16 v1, 0x18

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szFirstRetryTimeOut:Ljava/lang/String;

    .line 104
    const/16 v1, 0x19

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szReregThreshold:Ljava/lang/String;

    .line 105
    const/16 v1, 0x1a

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szTbit:Ljava/lang/String;

    .line 106
    const/16 v1, 0x1b

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szInit:Ljava/lang/String;

    goto/16 :goto_0

    .line 122
    :cond_1
    if-nez p1, :cond_2

    .line 124
    move-object p1, p0

    .line 125
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v1, :cond_2

    .line 126
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    goto :goto_2

    .line 129
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 130
    move-object p1, v0

    goto/16 :goto_1
.end method

.method public static xcp_NapDef()V
    .locals 31

    .prologue
    .line 138
    const/16 v20, 0x0

    .local v20, "i":I
    const/16 v21, 0x0

    .line 139
    .local v21, "j":I
    const/16 v27, 0x0

    .line 140
    .local v27, "nNapindex":I
    const/4 v6, 0x0

    .local v6, "DnsAddrList_h":Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;
    const/4 v15, 0x0

    .line 141
    .local v15, "DnsAddrList_t":Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;
    const/4 v7, 0x0

    .local v7, "BearerList_h":Lcom/wsomacp/eng/parser/XCPCtxBearer;
    const/4 v13, 0x0

    .line 142
    .local v13, "BearerList_t":Lcom/wsomacp/eng/parser/XCPCtxBearer;
    const/4 v8, 0x0

    .local v8, "NapAuthInfo_h":Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    const/16 v16, 0x0

    .line 143
    .local v16, "NapAuthInfo_t":Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;
    const/4 v9, 0x0

    .local v9, "Validity_h":Lcom/wsomacp/eng/parser/XCPCtxValidity;
    const/16 v18, 0x0

    .line 145
    .local v18, "Validity_t":Lcom/wsomacp/eng/parser/XCPCtxValidity;
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxNapDefCount()I

    move-result v25

    .line 146
    .local v25, "nCount":I
    if-lez v25, :cond_5

    .line 148
    const/16 v27, 0x0

    .line 149
    const/16 v27, 0x1

    :goto_0
    add-int/lit8 v3, v25, 0x1

    move/from16 v0, v27

    if-ge v0, v3, :cond_5

    .line 151
    invoke-static/range {v27 .. v27}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxDnsAddrCount(I)I

    move-result v26

    .line 152
    .local v26, "nDnscount":I
    if-lez v26, :cond_0

    .line 154
    const/16 v20, 0x0

    .line 155
    const/16 v20, 0x1

    :goto_1
    move/from16 v0, v20

    int-to-float v3, v0

    move/from16 v0, v26

    int-to-float v4, v0

    const/high16 v30, 0x3f800000    # 1.0f

    add-float v4, v4, v30

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 157
    move/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxDnsAddrData(II)Ljava/lang/String;

    move-result-object v19

    .line 158
    .local v19, "dnsString":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-static {v6, v15, v0}, Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;->xcp_pxDnsAddrAddList(Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;

    move-result-object v6

    .line 155
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 162
    .end local v19    # "dnsString":Ljava/lang/String;
    :cond_0
    invoke-static/range {v27 .. v27}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxBearerCount(I)I

    move-result v24

    .line 163
    .local v24, "nBearercount":I
    if-lez v24, :cond_1

    .line 165
    const/16 v20, 0x0

    .line 166
    const/16 v20, 0x1

    :goto_2
    move/from16 v0, v20

    int-to-float v3, v0

    move/from16 v0, v24

    int-to-float v4, v0

    const/high16 v30, 0x3f800000    # 1.0f

    add-float v4, v4, v30

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 168
    move/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxBearerData(II)Ljava/lang/String;

    move-result-object v14

    .line 169
    .local v14, "BearerString":Ljava/lang/String;
    invoke-static {v7, v13, v14}, Lcom/wsomacp/eng/parser/XCPCtxBearer;->xcp_pxBearerAddList(Lcom/wsomacp/eng/parser/XCPCtxBearer;Lcom/wsomacp/eng/parser/XCPCtxBearer;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxBearer;

    move-result-object v7

    .line 166
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 173
    .end local v14    # "BearerString":Ljava/lang/String;
    :cond_1
    invoke-static/range {v27 .. v27}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxNapAuthInfoCount(I)I

    move-result v22

    .line 174
    .local v22, "nAuthcount":I
    if-lez v22, :cond_3

    .line 176
    const/16 v20, 0x0

    .line 177
    const/16 v20, 0x1

    :goto_3
    move/from16 v0, v20

    int-to-float v3, v0

    move/from16 v0, v22

    int-to-float v4, v0

    const/high16 v30, 0x3f800000    # 1.0f

    add-float v4, v4, v30

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 179
    const/4 v10, 0x0

    .local v10, "AuthEntity_h":Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;
    const/4 v11, 0x0

    .line 180
    .local v11, "AuthEntity_t":Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;
    move/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxNapAuthEntityCount(II)I

    move-result v23

    .line 181
    .local v23, "nAuthentitycount":I
    if-lez v23, :cond_2

    .line 183
    const/16 v21, 0x0

    .line 184
    const/16 v21, 0x1

    :goto_4
    add-int/lit8 v3, v23, 0x1

    move/from16 v0, v21

    if-ge v0, v3, :cond_2

    .line 186
    move/from16 v0, v27

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxAuthInfopAuthEntityData(III)Ljava/lang/String;

    move-result-object v29

    .line 187
    .local v29, "pStr":Ljava/lang/String;
    move-object/from16 v0, v29

    invoke-static {v10, v11, v0}, Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;->xcp_pxAuthEntityAddList(Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;

    move-result-object v10

    .line 184
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 190
    .end local v29    # "pStr":Ljava/lang/String;
    :cond_2
    move/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxNapAuthInfoData(II)[Ljava/lang/String;

    move-result-object v12

    .line 191
    .local v12, "AuthInfoString":[Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v8, v0, v12, v10}, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->xcp_pxAuthInfoList(Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;)Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    move-result-object v8

    .line 177
    add-int/lit8 v20, v20, 0x1

    goto :goto_3

    .line 195
    .end local v10    # "AuthEntity_h":Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;
    .end local v11    # "AuthEntity_t":Lcom/wsomacp/eng/parser/XCPCtxAuthEntity;
    .end local v12    # "AuthInfoString":[Ljava/lang/String;
    .end local v23    # "nAuthentitycount":I
    :cond_3
    invoke-static/range {v27 .. v27}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxNapValidityCount(I)I

    move-result v28

    .line 196
    .local v28, "nValiditycount":I
    if-lez v28, :cond_4

    .line 198
    const/16 v20, 0x0

    .line 199
    const/16 v20, 0x1

    :goto_5
    move/from16 v0, v20

    int-to-float v3, v0

    move/from16 v0, v28

    int-to-float v4, v0

    const/high16 v30, 0x3f800000    # 1.0f

    add-float v4, v4, v30

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 201
    move/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxValidityData(II)[Ljava/lang/String;

    move-result-object v17

    .line 202
    .local v17, "ValiditString":[Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v9, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxValidity;->xcp_pxValidityAddList(Lcom/wsomacp/eng/parser/XCPCtxValidity;Lcom/wsomacp/eng/parser/XCPCtxValidity;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxValidity;

    move-result-object v9

    .line 199
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 205
    .end local v17    # "ValiditString":[Ljava/lang/String;
    :cond_4
    invoke-static/range {v27 .. v27}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->wsomacp_GetPxNapDefData(I)[Ljava/lang/String;

    move-result-object v5

    .line 206
    .local v5, "NapDefdata":[Ljava/lang/String;
    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    sget-object v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CPXDef_t:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-static/range {v3 .. v9}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->xcpNapDefAddList(Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/parser/XCPCtxNapDef;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxDnsAddr;Lcom/wsomacp/eng/parser/XCPCtxBearer;Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;Lcom/wsomacp/eng/parser/XCPCtxValidity;)Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    move-result-object v3

    sput-object v3, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 207
    const/4 v5, 0x0

    .line 208
    const/4 v6, 0x0

    .line 209
    const/4 v7, 0x0

    .line 210
    const/4 v8, 0x0

    .line 211
    const/4 v9, 0x0

    .line 149
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_0

    .line 214
    .end local v5    # "NapDefdata":[Ljava/lang/String;
    .end local v22    # "nAuthcount":I
    .end local v24    # "nBearercount":I
    .end local v26    # "nDnscount":I
    .end local v28    # "nValiditycount":I
    :cond_5
    return-void
.end method
