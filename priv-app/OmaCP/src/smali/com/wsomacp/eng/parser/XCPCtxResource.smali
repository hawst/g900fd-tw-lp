.class public Lcom/wsomacp/eng/parser/XCPCtxResource;
.super Ljava/lang/Object;
.source "XCPCtxResource.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

.field public m_szAaccept:Ljava/lang/String;

.field public m_szAauthData:Ljava/lang/String;

.field public m_szAauthName:Ljava/lang/String;

.field public m_szAauthSecret:Ljava/lang/String;

.field public m_szAauthType:Ljava/lang/String;

.field public m_szName:Ljava/lang/String;

.field public m_szStartPage:Ljava/lang/String;

.field public m_szUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PXAppResourceAddList(Lcom/wsomacp/eng/parser/XCPCtxResource;Lcom/wsomacp/eng/parser/XCPCtxResource;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxResource;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxResource;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxResource;
    .param p2, "resourceString"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 17
    const/4 v0, 0x0

    .line 18
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxResource;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxResource;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxResource;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxResource;-><init>()V

    .line 20
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxResource;
    if-nez p2, :cond_0

    .line 22
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szStartPage:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthData:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthSecret:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    .line 35
    :goto_0
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 37
    if-nez p0, :cond_1

    .line 39
    move-object p0, v0

    .line 40
    move-object p1, p0

    .line 54
    :goto_1
    return-object p0

    .line 26
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    .line 27
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    .line 28
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    .line 29
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthType:Ljava/lang/String;

    .line 30
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthName:Ljava/lang/String;

    .line 31
    const/4 v1, 0x5

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthSecret:Ljava/lang/String;

    .line 32
    const/4 v1, 0x6

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthData:Ljava/lang/String;

    .line 33
    const/4 v1, 0x7

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szStartPage:Ljava/lang/String;

    goto :goto_0

    .line 44
    :cond_1
    if-nez p1, :cond_2

    .line 46
    move-object p1, p0

    .line 47
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v1, :cond_2

    .line 48
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    goto :goto_2

    .line 51
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 52
    move-object p1, v0

    goto :goto_1
.end method
