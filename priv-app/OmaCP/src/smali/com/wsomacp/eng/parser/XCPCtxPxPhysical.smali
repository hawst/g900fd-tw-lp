.class public Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
.super Ljava/lang/Object;
.source "XCPCtxPxPhysical.java"


# instance fields
.field public m_CDomainList:Lcom/wsomacp/eng/parser/XCPCtxDomain;

.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

.field public m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

.field public m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

.field public m_szPhysicalProxyId:Ljava/lang/String;

.field public m_szPullEnabled:Ljava/lang/String;

.field public m_szPushEnabled:Ljava/lang/String;

.field public m_szPxAddr:Ljava/lang/String;

.field public m_szPxAddrFqdn:Ljava/lang/String;

.field public m_szPxAddrType:Ljava/lang/String;

.field public m_szWspVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PxPhysicalAddList(Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;[Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxToNapId;Lcom/wsomacp/eng/parser/XCPCtxPort;)Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    .param p2, "data"    # [Ljava/lang/String;
    .param p3, "DomainList"    # Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .param p4, "NapIDList"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;
    .param p5, "port"    # Lcom/wsomacp/eng/parser/XCPCtxPort;

    .prologue
    const/4 v2, 0x0

    .line 20
    const/4 v0, 0x0

    .line 21
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;-><init>()V

    .line 23
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    if-nez p2, :cond_0

    .line 25
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPullEnabled:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPushEnabled:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szWspVersion:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrFqdn:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPhysicalProxyId:Ljava/lang/String;

    .line 38
    :goto_0
    iput-object p3, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CDomainList:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .line 39
    iput-object p4, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .line 40
    iput-object p5, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    .line 41
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    .line 43
    if-nez p0, :cond_1

    .line 45
    move-object p0, v0

    .line 46
    move-object p1, p0

    .line 60
    :goto_1
    return-object p0

    .line 30
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPhysicalProxyId:Ljava/lang/String;

    .line 31
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    .line 32
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    .line 33
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrFqdn:Ljava/lang/String;

    .line 34
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szWspVersion:Ljava/lang/String;

    .line 35
    const/4 v1, 0x5

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPushEnabled:Ljava/lang/String;

    .line 36
    const/4 v1, 0x6

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPullEnabled:Ljava/lang/String;

    goto :goto_0

    .line 50
    :cond_1
    if-nez p1, :cond_2

    .line 52
    move-object p1, p0

    .line 53
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v1, :cond_2

    .line 54
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    goto :goto_2

    .line 57
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    .line 58
    move-object p1, v0

    goto :goto_1
.end method

.method public static xcp_PxPhysicalListDelete(Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    .prologue
    .line 65
    move-object v0, p0

    .line 66
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;
    :goto_0
    if-eqz v0, :cond_0

    .line 68
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    goto :goto_0

    .line 70
    :cond_0
    const/4 p0, 0x0

    .line 71
    return-void
.end method
