.class public Lcom/wsomacp/eng/parser/XCPCtxParser;
.super Ljava/lang/Object;
.source "XCPCtxParser.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

.field public static g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

.field public static g_CCPCtxNapDefMulti:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

.field public static g_CCPCtxPXLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

.field public static g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

.field public static g_CCPCtxVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

.field public static g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

.field public static g_CCPCtxWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

.field public static g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 34
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 35
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 36
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 37
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 38
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDefMulti:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 39
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 40
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 42
    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native wsomacp_GetParserBody()[B
.end method

.method static native wsomacp_GetParserBodyLen()I
.end method

.method static native wsomacp_GetParserContentType()I
.end method

.method static native wsomacp_GetParserHeaderLen()I
.end method

.method static native wsomacp_GetParserMAC()[B
.end method

.method static native wsomacp_GetParserMACLen()I
.end method

.method static native wsomacp_GetParserSEC()I
.end method

.method static native wsomacp_parser([BJI)Z
.end method

.method static native wsomacp_parser_del()V
.end method

.method public static xcpCtxParserNativeGetSMLWapPush()Lcom/wsomacp/eng/core/XCPSMLWapPush;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPSMLWapPush;-><init>()V

    .line 55
    .local v0, "CPengSMLWapPush":Lcom/wsomacp/eng/core/XCPSMLWapPush;
    new-instance v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-direct {v1}, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;-><init>()V

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    .line 56
    iget-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserContentType()I

    move-result v2

    iput v2, v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nContentType:I

    .line 57
    iget-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserHeaderLen()I

    move-result v2

    iput v2, v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nHeaderLen:I

    .line 58
    iget-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserBodyLen()I

    move-result v2

    iput v2, v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    .line 59
    iget-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserMACLen()I

    move-result v2

    iput v2, v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nMACLen:I

    .line 60
    iget-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserSEC()I

    move-result v2

    iput v2, v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    .line 61
    iget-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserMAC()[B

    move-result-object v2

    iput-object v2, v1, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_byMAC:[B

    .line 62
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_GetParserBody()[B

    move-result-object v1

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    .line 64
    return-object v0
.end method

.method public static xcpCtxParserNativeParser([BJI)Z
    .locals 1
    .param p0, "buf"    # [B
    .param p1, "len"    # J
    .param p3, "nContentType"    # I

    .prologue
    .line 47
    invoke-static {p0, p1, p2, p3}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser([BJI)Z

    move-result v0

    .line 49
    .local v0, "bParser":Z
    return v0
.end method

.method public static xcpCtxParserSearchPXPort(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "PXId"    # Ljava/lang/String;

    .prologue
    .line 308
    const-string v1, ""

    .line 309
    .local v1, "szRetData":Ljava/lang/String;
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 311
    .local v0, "provCtxPxLogical":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    :goto_0
    if-eqz v0, :cond_0

    .line 313
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 315
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v2, :cond_1

    .line 317
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    .line 332
    :cond_0
    :goto_1
    return-object v1

    .line 321
    :cond_1
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v2, :cond_0

    .line 322
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    goto :goto_1

    .line 328
    :cond_2
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    goto :goto_0
.end method

.method public static xcpStartMsgParse([BI)Z
    .locals 9
    .param p0, "RecvMsg"    # [B
    .param p1, "nContentType"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 73
    if-ne p1, v5, :cond_3

    .line 75
    :try_start_0
    array-length v6, p0

    int-to-long v6, v6

    invoke-static {p0, v6, v7, p1}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeParser([BJI)Z

    move-result v0

    .line 76
    .local v0, "bParser":Z
    if-nez v0, :cond_0

    .line 78
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "Parser Error"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 196
    .end local v0    # "bParser":Z
    :goto_0
    return v4

    .line 83
    .restart local v0    # "bParser":Z
    :cond_0
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeGetSMLWapPush()Lcom/wsomacp/eng/core/XCPSMLWapPush;

    move-result-object v6

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    .line 84
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    if-nez v6, :cond_1

    .line 86
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 87
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "header parser Error"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 190
    .end local v0    # "bParser":Z
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "bParser":Z
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    :cond_2
    :goto_1
    move v4, v5

    .line 196
    goto :goto_0

    .line 95
    .end local v0    # "bParser":Z
    :cond_3
    array-length v6, p0

    int-to-long v6, v6

    invoke-static {p0, v6, v7, p1}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeParser([BJI)Z

    move-result v0

    .line 96
    .restart local v0    # "bParser":Z
    if-nez v0, :cond_4

    .line 98
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "Parser Error"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_4
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->xcp_PxLogic()V

    .line 104
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->xcp_NapDef()V

    .line 105
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->xcp_Bootstrap()V

    .line 106
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->xcp_ClientIdentity()V

    .line 107
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxApplication;->xcp_Application()V

    .line 108
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxAccess;->xcp_CtxAccess()V

    .line 111
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeGetSMLWapPush()Lcom/wsomacp/eng/core/XCPSMLWapPush;

    move-result-object v6

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    .line 112
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    if-nez v6, :cond_5

    .line 114
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 115
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "header parser Error"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_5
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 121
    new-instance v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    invoke-direct {v6}, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;-><init>()V

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 122
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 123
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 124
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CBootstrap:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 125
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CClientIdent:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    .line 126
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 127
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxAccess;->g_CPXAccess_h:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CAccess:Lcom/wsomacp/eng/parser/XCPCtxAccess;

    .line 129
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 130
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 131
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDefMulti:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    sput-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 133
    new-instance v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;

    invoke-direct {v3}, Lcom/wsomacp/eng/parser/XCPCtxApplication;-><init>()V

    .line 134
    .local v3, "mApp":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 136
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetApplicationCount()I

    move-result v6

    if-ge v2, v6, :cond_7

    .line 138
    iget-object v6, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v7, "w4"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_6

    .line 140
    iget-object v6, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v6, :cond_6

    .line 142
    iget-object v6, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 143
    iget-object v6, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    invoke-static {v6}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetMMSproxyId(Ljava/lang/String;)V

    .line 146
    :cond_6
    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 149
    :cond_7
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "wssCPengConAccount.getApplicationCount = ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetApplicationCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 150
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetApplicationCount()I

    move-result v6

    if-ne v6, v5, :cond_2

    .line 152
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CBootstrap:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    if-eqz v6, :cond_b

    .line 154
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CBootstrap:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 156
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CBootstrap:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->m_szName:Ljava/lang/String;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 158
    :cond_8
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 160
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v6, :cond_a

    .line 162
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 163
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 165
    :cond_9
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    const-string v7, "Unknown"

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 169
    :cond_a
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    const-string v7, "Unknown"

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 173
    :cond_b
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 175
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v6, :cond_d

    .line 177
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 178
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 180
    :cond_c
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    const-string v7, "Unknown"

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 184
    :cond_d
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    const-string v7, "Unknown"

    iput-object v7, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public static xcpStartParseforBG([BILandroid/content/Context;)Z
    .locals 10
    .param p0, "RecvMsg"    # [B
    .param p1, "nContentType"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 205
    if-eq p1, v6, :cond_b

    .line 207
    :try_start_0
    array-length v7, p0

    int-to-long v8, v7

    invoke-static {p0, v8, v9, p1}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeParser([BJI)Z

    move-result v0

    .line 208
    .local v0, "bParser":Z
    if-nez v0, :cond_0

    .line 210
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v7, "Parser Error"

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 303
    .end local v0    # "bParser":Z
    :goto_0
    return v5

    .line 215
    .restart local v0    # "bParser":Z
    :cond_0
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->xcp_VendorConfig()V

    .line 216
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxWifi;->xcp_Wifi()V

    .line 219
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetVendorConfigCount()I

    move-result v7

    if-lt v7, v6, :cond_3

    .line 221
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeGetSMLWapPush()Lcom/wsomacp/eng/core/XCPSMLWapPush;

    move-result-object v7

    sput-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    .line 222
    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    if-nez v7, :cond_1

    .line 224
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 225
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v7, "header parser Error"

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297
    .end local v0    # "bParser":Z
    :catch_0
    move-exception v1

    .line 299
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "bParser":Z
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 231
    new-instance v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    invoke-direct {v7}, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;-><init>()V

    sput-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 232
    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v8, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    iput-object v8, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CVendrCfg:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 233
    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CVendrCfg:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    sput-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 235
    new-instance v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    invoke-direct {v3}, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;-><init>()V

    .line 236
    .local v3, "mVendor":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 238
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetVendorConfigCount()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 240
    if-nez v3, :cond_4

    .line 242
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v8, "mVendor is null"

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 257
    :cond_2
    const/4 v7, 0x1

    invoke-static {v7}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetBGInstall(I)V

    .line 260
    .end local v2    # "i":I
    .end local v3    # "mVendor":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    :cond_3
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetWifiCount()I

    move-result v7

    if-lt v7, v6, :cond_b

    .line 262
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserNativeGetSMLWapPush()Lcom/wsomacp/eng/core/XCPSMLWapPush;

    move-result-object v7

    sput-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    .line 263
    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    if-nez v7, :cond_7

    .line 265
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 266
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v7, "header parser Error"

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    .restart local v2    # "i":I
    .restart local v3    # "mVendor":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    :cond_4
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "LTEPRODUCT"

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_5

    .line 248
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "MCCMNC"

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_5

    .line 250
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_szAttrValue:Ljava/lang/String;

    invoke-static {v7}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetVendorConfigValue(Ljava/lang/String;)V

    .line 253
    :cond_5
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    if-eqz v7, :cond_6

    .line 254
    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 238
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 270
    .end local v2    # "i":I
    .end local v3    # "mVendor":Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    :cond_7
    invoke-static {}, Lcom/wsomacp/eng/parser/XCPCtxParser;->wsomacp_parser_del()V

    .line 272
    new-instance v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    invoke-direct {v7}, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;-><init>()V

    sput-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 273
    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v8, Lcom/wsomacp/eng/parser/XCPCtxWifi;->PXWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    iput-object v8, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 274
    sget-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    sput-object v7, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 276
    new-instance v4, Lcom/wsomacp/eng/parser/XCPCtxWifi;

    invoke-direct {v4}, Lcom/wsomacp/eng/parser/XCPCtxWifi;-><init>()V

    .line 277
    .local v4, "mWifi":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    sget-object v4, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWifi:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    .line 279
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetWifiCount()I

    move-result v7

    if-ge v2, v7, :cond_a

    .line 281
    if-nez v4, :cond_9

    .line 283
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v8, "mWifi is null"

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 279
    :cond_8
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 287
    :cond_9
    iget-object v7, v4, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_szWifi:[Ljava/lang/String;

    invoke-static {v7, p2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallWifiSetting([Ljava/lang/String;Landroid/content/Context;)V

    .line 289
    iget-object v7, v4, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    if-eqz v7, :cond_8

    .line 290
    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxWifi;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxWifi;

    goto :goto_3

    .line 293
    :cond_a
    const/4 v7, 0x2

    invoke-static {v7}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetBGInstall(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v0    # "bParser":Z
    .end local v2    # "i":I
    .end local v4    # "mWifi":Lcom/wsomacp/eng/parser/XCPCtxWifi;
    :cond_b
    move v5, v6

    .line 303
    goto/16 :goto_0
.end method
