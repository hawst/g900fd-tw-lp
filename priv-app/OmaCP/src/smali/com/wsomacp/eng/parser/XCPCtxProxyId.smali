.class public Lcom/wsomacp/eng/parser/XCPCtxProxyId;
.super Ljava/lang/Object;
.source "XCPCtxProxyId.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

.field public m_szProxyId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PxIdAddList(Lcom/wsomacp/eng/parser/XCPCtxProxyId;Lcom/wsomacp/eng/parser/XCPCtxProxyId;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    .param p2, "pxidString"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "ProxyId":Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    .end local v0    # "ProxyId":Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxProxyId;-><init>()V

    .line 13
    .restart local v0    # "ProxyId":Lcom/wsomacp/eng/parser/XCPCtxProxyId;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxProxyId;->m_szProxyId:Ljava/lang/String;

    .line 15
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxProxyId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    .line 16
    if-nez p0, :cond_0

    .line 18
    move-object p0, v0

    .line 19
    move-object p1, p0

    .line 34
    :goto_0
    return-object p0

    .line 23
    :cond_0
    if-nez p1, :cond_1

    .line 25
    move-object p1, p0

    .line 26
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxProxyId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    if-eqz v1, :cond_1

    .line 27
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxProxyId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    goto :goto_1

    .line 30
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxProxyId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxProxyId;

    .line 31
    move-object p1, v0

    goto :goto_0
.end method
