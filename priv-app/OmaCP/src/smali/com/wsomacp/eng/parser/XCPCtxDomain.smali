.class public Lcom/wsomacp/eng/parser/XCPCtxDomain;
.super Ljava/lang/Object;
.source "XCPCtxDomain.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxDomain;

.field public m_szDomain:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_pxDomainAddList(Lcom/wsomacp/eng/parser/XCPCtxDomain;Lcom/wsomacp/eng/parser/XCPCtxDomain;Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .locals 2
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxDomain;
    .param p2, "Domain"    # Ljava/lang/String;

    .prologue
    .line 10
    const/4 v0, 0x0

    .line 11
    .local v0, "domain":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .end local v0    # "domain":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxDomain;-><init>()V

    .line 13
    .restart local v0    # "domain":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    iput-object p2, v0, Lcom/wsomacp/eng/parser/XCPCtxDomain;->m_szDomain:Ljava/lang/String;

    .line 14
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxDomain;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .line 15
    if-nez p0, :cond_0

    .line 17
    move-object p0, v0

    .line 18
    move-object p1, p0

    .line 33
    :goto_0
    return-object p0

    .line 22
    :cond_0
    if-nez p1, :cond_1

    .line 24
    move-object p1, p0

    .line 25
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxDomain;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    if-eqz v1, :cond_1

    .line 26
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxDomain;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    goto :goto_1

    .line 29
    :cond_1
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxDomain;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .line 30
    move-object p1, v0

    goto :goto_0
.end method

.method public static xcp_pxDomainListDelete(Lcom/wsomacp/eng/parser/XCPCtxDomain;)V
    .locals 1
    .param p0, "header"    # Lcom/wsomacp/eng/parser/XCPCtxDomain;

    .prologue
    .line 38
    move-object v0, p0

    .line 39
    .local v0, "curr":Lcom/wsomacp/eng/parser/XCPCtxDomain;
    :goto_0
    if-eqz v0, :cond_0

    .line 41
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxDomain;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxDomain;

    goto :goto_0

    .line 44
    :cond_0
    const/4 p0, 0x0

    .line 45
    return-void
.end method
