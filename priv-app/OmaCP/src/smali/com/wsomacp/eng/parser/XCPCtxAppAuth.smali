.class public Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
.super Ljava/lang/Object;
.source "XCPCtxAppAuth.java"


# instance fields
.field public m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

.field public m_szAauthData:Ljava/lang/String;

.field public m_szAauthLevel:Ljava/lang/String;

.field public m_szAauthName:Ljava/lang/String;

.field public m_szAauthSecret:Ljava/lang/String;

.field public m_szAauthType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xcp_PXAppAuthAddList(Lcom/wsomacp/eng/parser/XCPCtxAppAuth;Lcom/wsomacp/eng/parser/XCPCtxAppAuth;[Ljava/lang/String;)Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    .locals 3
    .param p0, "head"    # Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    .param p1, "tail"    # Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    .param p2, "authString"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 14
    const/4 v0, 0x0

    .line 15
    .local v0, "tmp":Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .end local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;-><init>()V

    .line 17
    .restart local v0    # "tmp":Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    if-nez p2, :cond_0

    .line 19
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    .line 29
    :goto_0
    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .line 31
    if-nez p0, :cond_1

    .line 33
    move-object p0, v0

    .line 34
    move-object p1, p0

    .line 48
    :goto_1
    return-object p0

    .line 23
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    .line 24
    const/4 v1, 0x1

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    .line 25
    const/4 v1, 0x2

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    .line 26
    const/4 v1, 0x3

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    .line 27
    const/4 v1, 0x4

    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    goto :goto_0

    .line 38
    :cond_1
    if-nez p1, :cond_2

    .line 40
    move-object p1, p0

    .line 41
    :goto_2
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v1, :cond_2

    .line 42
    iget-object p1, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    goto :goto_2

    .line 45
    :cond_2
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .line 46
    move-object p1, v0

    goto :goto_1
.end method
