.class public Lcom/wsomacp/ui/XCPUIDetailFragment;
.super Landroid/app/Fragment;
.source "XCPUIDetailFragment.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

.field public static g_bInstalling:Z

.field public static g_bMultiMessage:Z

.field public static g_byRecvMsg:[B

.field public static g_nCPId:I

.field private static m_Context:Landroid/content/Context;

.field protected static m_bNextAccount:Z

.field protected static m_bTabletNextAccount:Z

.field private static m_szAppName:Ljava/lang/String;

.field private static m_tAppName:Landroid/widget/TextView;


# instance fields
.field private actionbar:Landroid/app/ActionBar;

.field private m_bDelete:Landroid/widget/Button;

.field private m_bInstall:Landroid/widget/Button;

.field private m_bNapSetting:Z

.field private m_bParserResult:Z

.field private m_layout:Landroid/widget/LinearLayout;

.field public m_nWheInstall:I

.field private m_szAppAPN:Ljava/lang/String;

.field private m_szAppPX:Ljava/lang/String;

.field public m_szRcvDate:Ljava/lang/String;

.field public m_szSMSDbId:Ljava/lang/String;

.field private m_tAppTitle:Landroid/widget/TextView;

.field private m_tDetail:[Landroid/widget/TextView;

.field private m_tNode:[Landroid/widget/TextView;

.field private m_view:Landroid/view/View;

.field public nSubid:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    .line 43
    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    .line 48
    sput-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_bMultiMessage:Z

    .line 52
    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    .line 56
    const-string v0, ""

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 59
    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    .line 68
    sput-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_bInstalling:Z

    .line 69
    sput v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    .line 71
    sput-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNextAccount:Z

    .line 72
    sput-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bTabletNextAccount:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 49
    iput-boolean v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNapSetting:Z

    .line 51
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppTitle:Landroid/widget/TextView;

    .line 53
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    .line 54
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    .line 55
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_layout:Landroid/widget/LinearLayout;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szSMSDbId:Ljava/lang/String;

    .line 61
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bInstall:Landroid/widget/Button;

    .line 62
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bDelete:Landroid/widget/Button;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    .line 65
    iput v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_nWheInstall:I

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->nSubid:J

    .line 73
    iput-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    .line 74
    iput-boolean v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bParserResult:Z

    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    return-object v0
.end method

.method private xcpAppClearMsgData()V
    .locals 3

    .prologue
    .line 547
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 548
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 550
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 551
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 553
    :cond_0
    return-void
.end method

.method private xcpAppDispAPPMsgData()V
    .locals 4

    .prologue
    const v3, 0x7f05002e

    .line 604
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 605
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    invoke-static {v1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetCurrentContexts(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    .line 606
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v0

    .line 607
    .local v0, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v1, :cond_a

    .line 609
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 611
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 613
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 614
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "w7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 615
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayDMMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    .line 638
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "next pointer app"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 650
    :goto_1
    return-void

    .line 616
    :cond_1
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "w2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 617
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayBrowserMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 618
    :cond_2
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "w4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 619
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayMMSMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 620
    :cond_3
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "w5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 621
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayDSMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 622
    :cond_4
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "554"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 623
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayStreamingMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 624
    :cond_5
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "25"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 625
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplaySMTPMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 626
    :cond_6
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "110"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 627
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayPOP3Msg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 628
    :cond_7
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "143"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 629
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayIMAP4Msg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto :goto_0

    .line 630
    :cond_8
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "ap9227"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 631
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayXcapMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    goto/16 :goto_0

    .line 635
    :cond_9
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "AppID is null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 640
    :cond_a
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v1, :cond_b

    .line 642
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 643
    invoke-direct {p0, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpDisplayAPNMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    .line 644
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "next pointer nap"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 648
    :cond_b
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private xcpAppSearchAPNAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "APNId"    # Ljava/lang/String;

    .prologue
    .line 1688
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1689
    const-string v1, ""

    .line 1690
    .local v1, "szRetData":Ljava/lang/String;
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 1692
    .local v0, "provCtxNapDef":Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    :goto_0
    if-eqz v0, :cond_0

    .line 1694
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 1696
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1697
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    .line 1705
    :cond_0
    return-object v1

    .line 1702
    :cond_1
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    goto :goto_0
.end method

.method private xcpAppSearchPXAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "PXId"    # Ljava/lang/String;

    .prologue
    .line 1710
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1711
    const-string v1, ""

    .line 1712
    .local v1, "szRetData":Ljava/lang/String;
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 1714
    .local v0, "provCtxPxLogical":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    :goto_0
    if-eqz v0, :cond_0

    .line 1716
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 1718
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v2, :cond_0

    .line 1719
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v1, v2, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    .line 1728
    :cond_0
    return-object v1

    .line 1724
    :cond_1
    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    goto :goto_0
.end method

.method private xcpDisplayAPNMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 6
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 654
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 657
    :try_start_0
    const-string v4, "APN"

    sput-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 659
    const-string v1, ""

    .line 660
    .local v1, "szNapAPN":Ljava/lang/String;
    const-string v2, ""

    .line 661
    .local v2, "szNapProxyAddr":Ljava/lang/String;
    const-string v3, ""

    .line 663
    .local v3, "szNapProxyPort":Ljava/lang/String;
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v5, 0x7f050002

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 665
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050002

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 666
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f05002f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 667
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f05002d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 668
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f05000a

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 670
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v4, :cond_0

    .line 672
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v1, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    .line 673
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 675
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 676
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 684
    :cond_0
    :goto_0
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v4, :cond_6

    .line 686
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v4, :cond_3

    .line 688
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v2, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    .line 690
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v4, :cond_1

    .line 691
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v3, v4, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    .line 698
    :cond_1
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 700
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 701
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 708
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 710
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 711
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 726
    :goto_3
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 728
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 740
    .end local v1    # "szNapAPN":Ljava/lang/String;
    .end local v2    # "szNapProxyAddr":Ljava/lang/String;
    .end local v3    # "szNapProxyPort":Ljava/lang/String;
    :goto_4
    return-void

    .line 680
    .restart local v1    # "szNapAPN":Ljava/lang/String;
    .restart local v2    # "szNapProxyAddr":Ljava/lang/String;
    .restart local v3    # "szNapProxyPort":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 736
    .end local v1    # "szNapAPN":Ljava/lang/String;
    .end local v2    # "szNapProxyAddr":Ljava/lang/String;
    .end local v3    # "szNapProxyPort":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 738
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_4

    .line 693
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "szNapAPN":Ljava/lang/String;
    .restart local v2    # "szNapProxyAddr":Ljava/lang/String;
    .restart local v3    # "szNapProxyPort":Ljava/lang/String;
    :cond_3
    :try_start_1
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v4, :cond_1

    .line 695
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v3, v4, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    goto :goto_1

    .line 705
    :cond_4
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 715
    :cond_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 720
    :cond_6
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 721
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 722
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 723
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 732
    :cond_7
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 733
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method private xcpDisplayBrowserMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 11
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 944
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v7, ""

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 947
    :try_start_0
    const-string v6, "Browser"

    sput-object v6, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 950
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v6, :cond_0

    .line 952
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v7, "application is null return"

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1079
    :goto_0
    return-void

    .line 956
    :cond_0
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 957
    sget-object v6, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v7, 0x7f050005

    invoke-virtual {p0, v7}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 961
    :goto_1
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const v7, 0x7f05003c

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 963
    iget-boolean v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNapSetting:Z

    if-nez v6, :cond_7

    .line 965
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const v7, 0x7f050002

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 966
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const v7, 0x7f05002f

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 967
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    const v7, 0x7f05002d

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 968
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    const v7, 0x7f050004

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 969
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    const v7, 0x7f05000a

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 976
    :goto_2
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 978
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v6, :cond_1

    .line 979
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 981
    :cond_1
    iget-boolean v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNapSetting:Z

    if-nez v6, :cond_4

    .line 983
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v6, :cond_2

    .line 984
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 986
    :cond_2
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v6, :cond_8

    .line 988
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 998
    :cond_3
    :goto_3
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 1000
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchAPNAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1001
    .local v1, "apnname":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1003
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1004
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1017
    .end local v1    # "apnname":Ljava/lang/String;
    :goto_4
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 1019
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchPXAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1020
    .local v3, "proxyname":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1022
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1023
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1030
    :goto_5
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v6}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserSearchPXPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1031
    .local v4, "proxyport":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1033
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1034
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1050
    .end local v3    # "proxyname":Ljava/lang/String;
    .end local v4    # "proxyport":Ljava/lang/String;
    :goto_6
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 1052
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1061
    :cond_4
    :goto_7
    new-instance v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;

    invoke-direct {v0}, Lcom/wsomacp/eng/parser/XCPCtxApplication;-><init>()V

    .line 1062
    .local v0, "CPCtxApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iput-object v6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 1064
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 1065
    .local v5, "sb":Ljava/lang/StringBuffer;
    :goto_8
    iget-object v6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v6, :cond_f

    .line 1067
    iget-object v6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1068
    iget-object v6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v6, :cond_5

    .line 1069
    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1071
    :cond_5
    iget-object v6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iput-object v6, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    .line 1075
    .end local v0    # "CPCtxApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v2

    .line 1077
    .local v2, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 959
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_6
    :try_start_1
    sget-object v6, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v7, "%s [%s]"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v10, v10, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const v10, 0x7f050005

    invoke-virtual {p0, v10}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 973
    :cond_7
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const v7, 0x7f05000a

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 992
    :cond_8
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v6, :cond_3

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v6, :cond_3

    .line 994
    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    goto/16 :goto_3

    .line 1008
    .restart local v1    # "apnname":Ljava/lang/String;
    :cond_9
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1013
    .end local v1    # "apnname":Ljava/lang/String;
    :cond_a
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1014
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 1027
    .restart local v3    # "proxyname":Ljava/lang/String;
    :cond_b
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1038
    .restart local v4    # "proxyport":Ljava/lang/String;
    :cond_c
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1044
    .end local v3    # "proxyname":Ljava/lang/String;
    .end local v4    # "proxyport":Ljava/lang/String;
    :cond_d
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1045
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1046
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1047
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_6

    .line 1056
    :cond_e
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1057
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_7

    .line 1073
    .restart local v0    # "CPCtxApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .restart local v5    # "sb":Ljava/lang/StringBuffer;
    :cond_f
    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayDMMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 9
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 1170
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1173
    :try_start_0
    const-string v4, "SyncML DM"

    sput-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 1176
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v4, :cond_0

    .line 1178
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "application is null return"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1289
    :goto_0
    return-void

    .line 1182
    :cond_0
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1183
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v5, 0x7f05003a

    invoke-virtual {p0, v5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1187
    :goto_1
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050035

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1188
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050002

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1189
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f05002f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1190
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f05002d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1191
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, 0x7f05000a

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1193
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1195
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v4, :cond_4

    .line 1197
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1209
    :goto_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v4, :cond_1

    .line 1210
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 1212
    :cond_1
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v4, :cond_6

    .line 1214
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 1224
    :cond_2
    :goto_3
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1226
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchAPNAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1227
    .local v0, "apnname":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1229
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1230
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1243
    .end local v0    # "apnname":Ljava/lang/String;
    :goto_4
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1245
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchPXAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1246
    .local v2, "proxyname":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1248
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1249
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1256
    :goto_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v4}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserSearchPXPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1257
    .local v3, "proxyport":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1259
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1260
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1275
    .end local v2    # "proxyname":Ljava/lang/String;
    .end local v3    # "proxyport":Ljava/lang/String;
    :goto_6
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 1277
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1285
    :catch_0
    move-exception v1

    .line 1287
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1185
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v5, "%s [%s]"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const v8, 0x7f05003a

    invoke-virtual {p0, v8}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1199
    :cond_4
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v4, :cond_5

    .line 1201
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1205
    :cond_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1206
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 1218
    :cond_6
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v4, :cond_2

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v4, :cond_2

    .line 1220
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    goto/16 :goto_3

    .line 1234
    .restart local v0    # "apnname":Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1239
    .end local v0    # "apnname":Ljava/lang/String;
    :cond_8
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1240
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 1253
    .restart local v2    # "proxyname":Ljava/lang/String;
    :cond_9
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1264
    .restart local v3    # "proxyport":Ljava/lang/String;
    :cond_a
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1269
    .end local v2    # "proxyname":Ljava/lang/String;
    .end local v3    # "proxyport":Ljava/lang/String;
    :cond_b
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1270
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1271
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1272
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_6

    .line 1281
    :cond_c
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1282
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayDSMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 9
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 744
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 747
    :try_start_0
    const-string v4, "Synchronise"

    sput-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 750
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v4, :cond_0

    .line 752
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "application is null return"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 860
    :goto_0
    return-void

    .line 756
    :cond_0
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 757
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v5, 0x7f05003b

    invoke-virtual {p0, v5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 761
    :goto_1
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050035

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 762
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050002

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 763
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f05002f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 764
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f05002d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 765
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, 0x7f05000a

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 767
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 769
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v4, :cond_5

    .line 771
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 779
    :goto_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v4, :cond_1

    .line 780
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 782
    :cond_1
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v4, :cond_2

    .line 783
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 785
    :cond_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v4, :cond_6

    .line 787
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 798
    :cond_3
    :goto_3
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 800
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchAPNAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 801
    .local v0, "apnname":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 803
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 804
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 817
    .end local v0    # "apnname":Ljava/lang/String;
    :goto_4
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 820
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchPXAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 821
    .local v2, "proxyname":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 823
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 824
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 829
    :goto_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v4}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserSearchPXPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 830
    .local v3, "proxyport":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 832
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 833
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 846
    .end local v2    # "proxyname":Ljava/lang/String;
    .end local v3    # "proxyport":Ljava/lang/String;
    :goto_6
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 848
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 856
    :catch_0
    move-exception v1

    .line 858
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 759
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v5, "%s [%s]"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const v8, 0x7f05003b

    invoke-virtual {p0, v8}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 775
    :cond_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 776
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 791
    :cond_6
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v4, :cond_3

    .line 793
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v4, :cond_3

    .line 794
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    goto/16 :goto_3

    .line 808
    .restart local v0    # "apnname":Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 813
    .end local v0    # "apnname":Ljava/lang/String;
    :cond_8
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 814
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 827
    .restart local v2    # "proxyname":Ljava/lang/String;
    :cond_9
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 836
    .restart local v3    # "proxyport":Ljava/lang/String;
    :cond_a
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 840
    .end local v2    # "proxyname":Ljava/lang/String;
    .end local v3    # "proxyport":Ljava/lang/String;
    :cond_b
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 841
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 842
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 843
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_6

    .line 852
    :cond_c
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 853
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayIMAP4Msg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 7
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 1475
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1478
    :try_start_0
    const-string v2, "Email"

    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 1479
    const/4 v1, 0x0

    .line 1482
    .local v1, "nPort":I
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v2, :cond_0

    .line 1484
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "application is null return"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1562
    .end local v1    # "nPort":I
    :goto_0
    return-void

    .line 1488
    .restart local v1    # "nPort":I
    :cond_0
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1489
    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v3, 0x7f05000f

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1493
    :goto_1
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const v3, 0x7f050018

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1494
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, 0x7f050035

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1495
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, 0x7f050036

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1496
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f05003d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1497
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050038

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1498
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, 0x7f05000a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1500
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1502
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const v3, 0x7f050010

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1504
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1506
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514
    :goto_2
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1516
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1517
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1525
    :goto_3
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1527
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1536
    :goto_4
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const-string v3, "STARTTLS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1538
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1548
    :goto_5
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1550
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1558
    .end local v1    # "nPort":I
    :catch_0
    move-exception v0

    .line 1560
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1491
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "nPort":I
    :cond_1
    :try_start_1
    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v3, "%s [%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const v6, 0x7f05000f

    invoke-virtual {p0, v6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1510
    :cond_2
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1511
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 1521
    :cond_3
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1522
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 1531
    :cond_4
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1532
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 1542
    :cond_5
    const/16 v2, 0x3e1

    if-ne v1, v2, :cond_6

    .line 1543
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 1545
    :cond_6
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050042

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 1554
    :cond_7
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1555
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayMMSMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 8
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 864
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 867
    :try_start_0
    const-string v3, "MMS"

    sput-object v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 870
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v3, :cond_0

    .line 872
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "application is null return"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 940
    :goto_0
    return-void

    .line 876
    :cond_0
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 877
    sget-object v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v4, 0x7f05001e

    invoke-virtual {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 881
    :goto_1
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const v4, 0x7f05001f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 882
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const v4, 0x7f050021

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 883
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const v4, 0x7f050020

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 884
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const v4, 0x7f05000a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 886
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 888
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v3, :cond_1

    .line 889
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 891
    :cond_1
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v3, :cond_2

    .line 892
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 894
    :cond_2
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 896
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchPXAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 897
    .local v1, "proxyaddr":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 899
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const v4, 0x7f050024

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 900
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 907
    :goto_2
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v3}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserSearchPXPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 908
    .local v2, "proxyport":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 910
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const v4, 0x7f050024

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 911
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 926
    .end local v1    # "proxyaddr":Ljava/lang/String;
    .end local v2    # "proxyport":Ljava/lang/String;
    :goto_3
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 928
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 936
    :catch_0
    move-exception v0

    .line 938
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 879
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    sget-object v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v4, "%s [%s]"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const v7, 0x7f05001e

    invoke-virtual {p0, v7}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 904
    .restart local v1    # "proxyaddr":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 915
    .restart local v2    # "proxyport":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 920
    .end local v1    # "proxyaddr":Ljava/lang/String;
    .end local v2    # "proxyport":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const v4, 0x7f050024

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 921
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 922
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const v4, 0x7f050024

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 923
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 932
    :cond_7
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const v4, 0x7f050024

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 933
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayPOP3Msg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 7
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 1293
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1296
    :try_start_0
    const-string v2, "Email"

    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 1297
    const/4 v1, 0x0

    .line 1300
    .local v1, "nPort":I
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v2, :cond_0

    .line 1302
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "application is null return"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1381
    .end local v1    # "nPort":I
    :goto_0
    return-void

    .line 1306
    .restart local v1    # "nPort":I
    :cond_0
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1307
    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v3, 0x7f05000f

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311
    :goto_1
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const v3, 0x7f050018

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1312
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, 0x7f050035

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1313
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, 0x7f050036

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1314
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f05003d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1315
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050038

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1316
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, 0x7f05000a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1318
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1320
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const v3, 0x7f05002c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1322
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1324
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1332
    :goto_2
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1334
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1335
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1343
    :goto_3
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1345
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354
    :goto_4
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const-string v3, "STARTTLS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1356
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1366
    :goto_5
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1368
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1377
    .end local v1    # "nPort":I
    :catch_0
    move-exception v0

    .line 1379
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1309
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "nPort":I
    :cond_1
    :try_start_1
    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v3, "%s [%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const v6, 0x7f05000f

    invoke-virtual {p0, v6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1328
    :cond_2
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1329
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 1339
    :cond_3
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1340
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 1349
    :cond_4
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1350
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 1360
    :cond_5
    const/16 v2, 0x3e3

    if-ne v1, v2, :cond_6

    .line 1361
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 1363
    :cond_6
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050042

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 1372
    :cond_7
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1373
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplaySMTPMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 7
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 1385
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1388
    :try_start_0
    const-string v2, "Email"

    sput-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 1389
    const/4 v1, 0x0

    .line 1392
    .local v1, "nPort":I
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v2, :cond_0

    .line 1394
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "application is null return"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1471
    .end local v1    # "nPort":I
    :goto_0
    return-void

    .line 1398
    .restart local v1    # "nPort":I
    :cond_0
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1399
    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v3, 0x7f05000f

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1403
    :goto_1
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const v3, 0x7f050026

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1404
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, 0x7f050035

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1405
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, 0x7f050036

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1406
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f050038

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1407
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050034

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1408
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, 0x7f05000a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1410
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1412
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const v3, 0x7f050037

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1414
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1416
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1424
    :goto_2
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1426
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1427
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1436
    :goto_3
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const-string v3, "STARTTLS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1438
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1448
    :goto_4
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v2, :cond_7

    .line 1450
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050045

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1457
    :goto_5
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1459
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1467
    .end local v1    # "nPort":I
    :catch_0
    move-exception v0

    .line 1469
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1401
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "nPort":I
    :cond_1
    :try_start_1
    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v3, "%s [%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const v6, 0x7f05000f

    invoke-virtual {p0, v6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1420
    :cond_2
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1421
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 1431
    :cond_3
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1432
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 1442
    :cond_4
    const/16 v2, 0x1d1

    if-eq v1, v2, :cond_5

    const/16 v2, 0x24b

    if-ne v1, v2, :cond_6

    .line 1443
    :cond_5
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    .line 1445
    :cond_6
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const v3, 0x7f050042

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    .line 1454
    :cond_7
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const v3, 0x7f050041

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 1463
    :cond_8
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, 0x7f050024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1464
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayStreamingMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 9
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 1566
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1569
    :try_start_0
    const-string v4, "STREAMING"

    sput-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 1572
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v4, :cond_0

    .line 1574
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "application is null return"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1684
    :goto_0
    return-void

    .line 1578
    :cond_0
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1579
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v5, 0x7f050039

    invoke-virtual {p0, v5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1583
    :goto_1
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050001

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1584
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050002

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1585
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f05002f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1586
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f05002d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1587
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, 0x7f05000a

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1589
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1591
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1593
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1601
    :goto_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v4, :cond_1

    .line 1602
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 1604
    :cond_1
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v4, :cond_2

    .line 1605
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 1607
    :cond_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v4, :cond_6

    .line 1609
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 1620
    :cond_3
    :goto_3
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1622
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchAPNAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1623
    .local v0, "apnname":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1625
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1626
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1639
    .end local v0    # "apnname":Ljava/lang/String;
    :goto_4
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1641
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppSearchPXAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1642
    .local v2, "proxyname":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1644
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1645
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1652
    :goto_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    invoke-static {v4}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpCtxParserSearchPXPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1653
    .local v3, "proxyport":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1655
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1656
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1670
    .end local v2    # "proxyname":Ljava/lang/String;
    .end local v3    # "proxyport":Ljava/lang/String;
    :goto_6
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 1672
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1680
    :catch_0
    move-exception v1

    .line 1682
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1581
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v5, "%s [%s]"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const v8, 0x7f050039

    invoke-virtual {p0, v8}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1597
    :cond_5
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1598
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 1613
    :cond_6
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v4, :cond_3

    .line 1615
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v4, :cond_3

    .line 1616
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    iput-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    goto/16 :goto_3

    .line 1630
    .restart local v0    # "apnname":Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1635
    .end local v0    # "apnname":Ljava/lang/String;
    :cond_8
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1636
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 1649
    .restart local v2    # "proxyname":Ljava/lang/String;
    :cond_9
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1659
    .restart local v3    # "proxyport":Ljava/lang/String;
    :cond_a
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1664
    .end local v2    # "proxyname":Ljava/lang/String;
    .end local v3    # "proxyport":Ljava/lang/String;
    :cond_b
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1665
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1666
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1667
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_6

    .line 1676
    :cond_c
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, 0x7f050024

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1677
    iget-object v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private xcpDisplayXcapMsg(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 6
    .param p1, "contextWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 1083
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1086
    :try_start_0
    const-string v1, "XCAP"

    sput-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 1089
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v1, :cond_1

    .line 1091
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "application is null return"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1166
    :cond_0
    :goto_0
    return-void

    .line 1095
    :cond_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1096
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const v2, 0x7f05003f

    invoke-virtual {p0, v2}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1100
    :goto_1
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const v2, 0x7f050003

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1102
    iget-boolean v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNapSetting:Z

    if-nez v1, :cond_4

    .line 1104
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, 0x7f050022

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1105
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const v2, 0x7f050001

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1106
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const v2, 0x7f050031

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1107
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    const v2, 0x7f05000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1114
    :goto_2
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1115
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1117
    :cond_2
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1119
    const-string v1, "ap9227"

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1121
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1123
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1131
    :goto_3
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1133
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1141
    :goto_4
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1143
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1151
    :goto_5
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CRoaming:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    if-eqz v1, :cond_8

    .line 1153
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const-string v2, "yes"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1162
    :catch_0
    move-exception v0

    .line 1164
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1098
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    const-string v2, "%s [%s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const v5, 0x7f05003f

    invoke-virtual {p0, v5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1111
    :cond_4
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, 0x7f05000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 1127
    :cond_5
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const v2, 0x7f050024

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1128
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 1137
    :cond_6
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, 0x7f050024

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1138
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 1147
    :cond_7
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const v2, 0x7f050024

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1148
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_5

    .line 1157
    :cond_8
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const v2, 0x7f050024

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1158
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static xcpInstallConAccountSetting(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subid"    # J

    .prologue
    .line 460
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 463
    :try_start_0
    new-instance v1, Lcom/wsomacp/agent/XCPConAccount;

    sget-object v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/wsomacp/agent/XCPConAccount;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 465
    .local v1, "pConAccount":Lcom/wsomacp/agent/XCPConAccount;
    sget-object v2, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDefMulti:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {v1, v2, v3, p1, p2}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetConnecting(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    .end local v1    # "pConAccount":Lcom/wsomacp/agent/XCPConAccount;
    :goto_0
    return-void

    .line 467
    :catch_0
    move-exception v0

    .line 469
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static xcpNewInstance(ILandroid/content/Context;)Lcom/wsomacp/ui/XCPUIDetailFragment;
    .locals 5
    .param p0, "index"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 81
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetNapSetting(Z)V

    .line 83
    new-instance v1, Lcom/wsomacp/ui/XCPUIDetailFragment;

    invoke-direct {v1}, Lcom/wsomacp/ui/XCPUIDetailFragment;-><init>()V

    .line 84
    .local v1, "f":Lcom/wsomacp/ui/XCPUIDetailFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 85
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "index"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "index : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 87
    sput p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    .line 88
    sput-object p1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    .line 90
    invoke-virtual {v1, v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->setArguments(Landroid/os/Bundle;)V

    .line 91
    return-object v1
.end method

.method private xcpSelectAppID()V
    .locals 2

    .prologue
    .line 557
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 558
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v0, :cond_a

    .line 560
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 562
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 564
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "w7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 565
    const-string v0, "SyncML DM"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 588
    :cond_0
    :goto_0
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 589
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "next pointer"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 600
    :goto_1
    return-void

    .line 566
    :cond_1
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "w2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 567
    const-string v0, "Browser"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto :goto_0

    .line 568
    :cond_2
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "w4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 569
    const-string v0, "MMS"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto :goto_0

    .line 570
    :cond_3
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "w5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 571
    const-string v0, "Synchronise"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto :goto_0

    .line 572
    :cond_4
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "554"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 573
    const-string v0, "STREAMING"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto :goto_0

    .line 574
    :cond_5
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "25"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 575
    const-string v0, "Email"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto :goto_0

    .line 576
    :cond_6
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "110"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 577
    const-string v0, "Email"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto :goto_0

    .line 578
    :cond_7
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "143"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 579
    const-string v0, "Email"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto/16 :goto_0

    .line 580
    :cond_8
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v1, "ap9227"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 581
    const-string v0, "XCAP"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    goto/16 :goto_0

    .line 585
    :cond_9
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "AppID is null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 591
    :cond_a
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v0, :cond_b

    .line 593
    const-string v0, "APN"

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 594
    sget-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    sput-object v0, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    goto/16 :goto_1

    .line 598
    :cond_b
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "Not Support"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 103
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 104
    if-nez p2, :cond_0

    .line 105
    const/4 v1, 0x0

    .line 195
    :goto_0
    return-object v1

    .line 107
    :cond_0
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 109
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "m_Context is null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 110
    const/4 v1, 0x0

    goto :goto_0

    .line 113
    :cond_1
    sget-boolean v1, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-nez v1, :cond_2

    .line 115
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->actionbar:Landroid/app/ActionBar;

    .line 116
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->actionbar:Landroid/app/ActionBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 118
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 119
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 120
    invoke-virtual {p0, v6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->setHasOptionsMenu(Z)V

    .line 123
    :cond_2
    sput-boolean v5, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNextAccount:Z

    .line 124
    iput-object p2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    .line 125
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 126
    sget v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_6

    .line 128
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpInit()V

    .line 129
    sget v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    invoke-virtual {p0, v1}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpGetMsg(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bParserResult:Z

    .line 131
    iget-boolean v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bParserResult:Z

    if-nez v1, :cond_4

    .line 133
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 134
    .local v0, "vi":Landroid/view/LayoutInflater;
    if-nez v0, :cond_3

    .line 136
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "vi is null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    goto/16 :goto_0

    .line 140
    :cond_3
    const v1, 0x7f030004

    invoke-virtual {v0, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    .line 141
    sget-object v1, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v2, 0x1a

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 195
    :goto_1
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    goto/16 :goto_0

    .line 145
    .end local v0    # "vi":Landroid/view/LayoutInflater;
    :cond_4
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 146
    .restart local v0    # "vi":Landroid/view/LayoutInflater;
    if-nez v0, :cond_5

    .line 148
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "vi is null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    goto/16 :goto_0

    .line 152
    :cond_5
    const v1, 0x7f030001

    invoke-virtual {v0, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    .line 154
    const-string v1, ""

    sput-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppTitle:Landroid/widget/TextView;

    .line 156
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sput-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tAppName:Landroid/widget/TextView;

    .line 158
    const/4 v1, 0x6

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    .line 159
    const/4 v1, 0x6

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    .line 161
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f080004

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v5

    .line 162
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f080006

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v6

    .line 163
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f080008

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v7

    .line 164
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f08000a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v8

    .line 165
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f08000c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v9

    .line 166
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tNode:[Landroid/widget/TextView;

    const/4 v3, 0x5

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v4, 0x7f08000e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    .line 168
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f080005

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v5

    .line 169
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f080007

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v6

    .line 170
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f080009

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v7

    .line 171
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f08000b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v8

    .line 172
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v3, 0x7f08000d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v9

    .line 173
    iget-object v2, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_tDetail:[Landroid/widget/TextView;

    const/4 v3, 0x5

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v4, 0x7f08000f

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    .line 175
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v2, 0x7f080010

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_layout:Landroid/widget/LinearLayout;

    .line 176
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v2, 0x7f080012

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bInstall:Landroid/widget/Button;

    .line 177
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    const v2, 0x7f080011

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bDelete:Landroid/widget/Button;

    .line 179
    const-string v1, ""

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 180
    const-string v1, ""

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    goto/16 :goto_1

    .line 185
    .end local v0    # "vi":Landroid/view/LayoutInflater;
    :cond_6
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 186
    .restart local v0    # "vi":Landroid/view/LayoutInflater;
    if-nez v0, :cond_7

    .line 188
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "vi is null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 189
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    goto/16 :goto_0

    .line 192
    :cond_7
    const v1, 0x7f030004

    invoke-virtual {v0, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_view:Landroid/view/View;

    goto/16 :goto_1
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 352
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 354
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 357
    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_bInstalling:Z

    .line 358
    const/4 v1, 0x0

    sput-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_bMultiMessage:Z

    .line 360
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 361
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 362
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 364
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v1, :cond_0

    .line 365
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 367
    :cond_0
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v1, :cond_1

    .line 368
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 370
    :cond_1
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v1, :cond_2

    .line 371
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 373
    :cond_2
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    if-eqz v1, :cond_3

    .line 374
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 376
    :cond_3
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    if-eqz v1, :cond_4

    .line 377
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    .line 379
    :cond_4
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    if-eqz v1, :cond_5

    .line 380
    const/4 v1, 0x0

    sput-object v1, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :cond_5
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Install complete. Finish... "

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 389
    :goto_0
    return-void

    .line 382
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Finish..."

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 337
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 346
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 340
    :pswitch_0
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailActivity;->xcpUIDetailActivityRemove()V

    goto :goto_0

    .line 337
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 201
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 202
    const/4 v0, 0x0

    .line 203
    .local v0, "cursor":Landroid/database/Cursor;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 205
    sget-object v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 207
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "m_Context is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_2

    .line 216
    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-nez v3, :cond_2

    .line 218
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "CP Message Empty !!! "

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 233
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 234
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v2

    .line 238
    .local v2, "ex":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_2
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 233
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 234
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 242
    :cond_3
    :goto_1
    sget v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    if-eqz v3, :cond_0

    .line 244
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_layout:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    .line 248
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_layout:Landroid/widget/LinearLayout;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 249
    iget v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_nWheInstall:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    .line 251
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_layout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 253
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bInstall:Landroid/widget/Button;

    if-eqz v3, :cond_4

    .line 255
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bInstall:Landroid/widget/Button;

    new-instance v4, Lcom/wsomacp/ui/XCPUIDetailFragment$1;

    invoke-direct {v4, p0}, Lcom/wsomacp/ui/XCPUIDetailFragment$1;-><init>(Lcom/wsomacp/ui/XCPUIDetailFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    :cond_4
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bDelete:Landroid/widget/Button;

    if-eqz v3, :cond_5

    .line 287
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bDelete:Landroid/widget/Button;

    new-instance v4, Lcom/wsomacp/ui/XCPUIDetailFragment$2;

    invoke-direct {v4, p0}, Lcom/wsomacp/ui/XCPUIDetailFragment$2;-><init>(Lcom/wsomacp/ui/XCPUIDetailFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    :cond_5
    const-string v3, ""

    iput-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppAPN:Ljava/lang/String;

    .line 312
    const-string v3, ""

    iput-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppPX:Ljava/lang/String;

    .line 314
    sget-boolean v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bNextAccount:Z

    if-nez v3, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppClearMsgData()V

    .line 317
    invoke-direct {p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpAppDispAPPMsgData()V

    goto/16 :goto_0

    .line 236
    :catch_1
    move-exception v2

    .line 238
    .restart local v2    # "ex":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_1

    .line 223
    .end local v2    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v1

    .line 225
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 232
    :try_start_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 233
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 234
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 236
    :catch_3
    move-exception v2

    .line 238
    .restart local v2    # "ex":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 230
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 232
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 233
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 234
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 239
    :cond_6
    :goto_2
    throw v3

    .line 236
    :catch_4
    move-exception v2

    .line 238
    .restart local v2    # "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public xcpCancleToast()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 393
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 395
    sget-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

    if-eqz v0, :cond_0

    const-string v0, "Synchronise"

    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    const v1, 0x7f05000e

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 399
    :goto_0
    return-void

    .line 398
    :cond_0
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;

    const v1, 0x7f05000b

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public xcpGetMsg(I)Z
    .locals 12
    .param p1, "nId"    # I

    .prologue
    const/4 v11, 0x0

    .line 403
    const/4 v0, 0x0

    .line 404
    .local v0, "bRet":Z
    const-string v6, ""

    .line 405
    .local v6, "szDate":Ljava/lang/String;
    const-string v7, ""

    .line 406
    .local v7, "szMessage":Ljava/lang/String;
    const/4 v3, 0x0

    .line 408
    .local v3, "nInstalId":I
    const-wide/16 v4, 0x0

    .line 410
    .local v4, "subid":J
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "nId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 411
    int-to-long v8, p1

    invoke-static {v8, v9}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryEntry(J)Landroid/database/Cursor;

    move-result-object v2

    .line 412
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_4

    .line 414
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-nez v8, :cond_1

    .line 416
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "CP Message Empty !!! "

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 427
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 428
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 429
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 431
    :cond_0
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "nInstalId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 433
    invoke-static {v7}, Lcom/wsomacp/eng/core/XCPUtil;->xcpHexStringToBytes(Ljava/lang/String;)[B

    move-result-object v8

    sput-object v8, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    .line 434
    sget-object v8, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    if-nez v8, :cond_2

    .line 436
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "RecvMsg is null"

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move v1, v0

    .line 455
    .end local v0    # "bRet":Z
    .local v1, "bRet":I
    :goto_1
    return v1

    .line 420
    .end local v1    # "bRet":I
    .restart local v0    # "bRet":Z
    :cond_1
    const/4 v8, 0x1

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 421
    const/4 v8, 0x2

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 422
    const/4 v8, 0x3

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 425
    const/4 v8, 0x6

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    goto :goto_0

    .line 440
    :cond_2
    invoke-static {v6}, Lcom/wsomacp/eng/core/XCPUtil;->xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szRcvDate:Ljava/lang/String;

    .line 441
    sget-boolean v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v8, :cond_3

    .line 442
    iput v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_nWheInstall:I

    .line 446
    :goto_2
    iput-wide v4, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->nSubid:J

    .line 448
    sget-object v8, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    invoke-static {v8, v11}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpStartMsgParse([BI)Z

    move-result v0

    :goto_3
    move v1, v0

    .line 455
    .restart local v1    # "bRet":I
    goto :goto_1

    .line 444
    .end local v1    # "bRet":I
    :cond_3
    iput v11, p0, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_nWheInstall:I

    goto :goto_2

    .line 452
    :cond_4
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "onquerycomplete..cursor is null"

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public xcpGetShownIndex()I
    .locals 2

    .prologue
    .line 96
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 97
    sget v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    return v0
.end method

.method public declared-synchronized xcpInit()V
    .locals 2

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 325
    const-string v0, "omacp"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 327
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Lcom/wsomacp/agent/XCPAgentAdapter;

    invoke-direct {v0}, Lcom/wsomacp/agent/XCPAgentAdapter;-><init>()V

    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    .line 330
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    new-instance v1, Lcom/wsomacp/agent/XCPAppInstall;

    invoke-direct {v1}, Lcom/wsomacp/agent/XCPAppInstall;-><init>()V

    iput-object v1, v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_0
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public xcpVerifyAuthentication([BLandroid/content/Context;IZIJ)I
    .locals 16
    .param p1, "message"    # [B
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "rType"    # I
    .param p4, "bCheck"    # Z
    .param p5, "contentType"    # I
    .param p6, "subid"    # J

    .prologue
    .line 476
    const/4 v12, 0x0

    .line 477
    .local v12, "nRtn":I
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 479
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpInit()V

    .line 480
    sput-object p1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    .line 482
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    move/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v4, v0, v1}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpStartParseforBG([BILandroid/content/Context;)Z

    .line 484
    const/16 v4, 0x1111

    move/from16 v0, p3

    if-eq v0, v4, :cond_0

    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetBGInstall()I

    move-result v4

    if-eqz v4, :cond_1

    .line 486
    :cond_0
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "XCP_SEC_TYPE_ALL return!"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move v13, v12

    .line 542
    .end local v12    # "nRtn":I
    .local v13, "nRtn":I
    :goto_0
    return v13

    .line 490
    .end local v13    # "nRtn":I
    .restart local v12    # "nRtn":I
    :cond_1
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_byRecvMsg:[B

    move/from16 v0, p5

    invoke-static {v4, v0}, Lcom/wsomacp/eng/parser/XCPCtxParser;->xcpStartMsgParse([BI)Z

    .line 492
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    sget-object v5, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v6, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    move-object/from16 v4, p2

    move/from16 v7, p3

    move/from16 v8, p4

    move/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-static/range {v4 .. v11}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpVerifyAuthentication(Landroid/content/Context;Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Lcom/wsomacp/eng/core/XCPSMLWapPush;IZIJ)I

    move-result v14

    .line 493
    .local v14, "type":I
    packed-switch v14, :pswitch_data_0

    .line 541
    :cond_2
    :goto_1
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nRtn: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move v13, v12

    .line 542
    .end local v12    # "nRtn":I
    .restart local v13    # "nRtn":I
    goto :goto_0

    .line 497
    .end local v13    # "nRtn":I
    .restart local v12    # "nRtn":I
    :pswitch_0
    const/4 v12, -0x1

    .line 498
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    invoke-virtual {v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentDelete()V

    .line 499
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    invoke-virtual {v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContexts()V

    goto :goto_1

    .line 503
    :pswitch_1
    sget-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

    if-eqz v4, :cond_3

    .line 505
    const/4 v4, 0x1

    move/from16 v0, p5

    if-eq v0, v4, :cond_2

    .line 513
    :cond_3
    sget-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_NETWPIN_CHECK:Z

    if-eqz v4, :cond_4

    .line 515
    :goto_2
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    invoke-virtual {v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCheckNextAccount()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 517
    const/4 v4, 0x0

    sput-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    .line 518
    invoke-direct/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpSelectAppID()V

    .line 519
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    const-string v5, "STREAMING"

    if-ne v4, v5, :cond_5

    .line 521
    const/4 v12, -0x1

    .line 537
    :cond_4
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    invoke-virtual {v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContexts()V

    goto :goto_1

    .line 524
    :cond_5
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;

    const-string v5, "SyncML DM"

    if-ne v4, v5, :cond_6

    .line 526
    sget-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP:Z

    if-eqz v4, :cond_6

    .line 528
    const/4 v12, 0x1

    .line 529
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentAccount(Landroid/content/Context;)I

    .line 530
    move-object/from16 v0, p2

    move-wide/from16 v1, p6

    invoke-static {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpInstallConAccountSetting(Landroid/content/Context;J)V

    .line 533
    :cond_6
    sget-object v4, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    invoke-virtual {v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentDelete()V

    goto :goto_2

    .line 493
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
