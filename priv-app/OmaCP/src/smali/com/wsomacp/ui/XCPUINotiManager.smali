.class public Lcom/wsomacp/ui/XCPUINotiManager;
.super Ljava/lang/Object;
.source "XCPUINotiManager.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field private static NotiTime:J = 0x0L

.field private static final XCP_NOT_USE_TICKER:I = 0x0

.field private static final XCP_STATUS_NOTIFICATION_CP:I = 0x1

.field private static bIsNotiOn:Ljava/lang/Boolean;

.field private static m_Context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    .line 21
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUINotiManager;->bIsNotiOn:Ljava/lang/Boolean;

    .line 22
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/wsomacp/ui/XCPUINotiManager;->NotiTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setIndicator(I)V
    .locals 7
    .param p0, "nIndicator"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 31
    sget-object v6, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    if-nez v6, :cond_0

    .line 33
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "mContext is null, return"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 37
    :cond_0
    const-string v6, "notification"

    invoke-static {v6}, Lcom/wsomacp/XCPApplication;->xcpGetSystemServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 38
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-nez v0, :cond_1

    .line 40
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "notificationManager is null, return"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_1
    packed-switch p0, :pswitch_data_0

    .line 63
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "_wssAppDMUiSetIndicatorState : nIndicatorState is NONE"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :pswitch_0
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 48
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sput-object v4, Lcom/wsomacp/ui/XCPUINotiManager;->bIsNotiOn:Ljava/lang/Boolean;

    goto :goto_0

    .line 52
    :pswitch_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sput-object v6, Lcom/wsomacp/ui/XCPUINotiManager;->bIsNotiOn:Ljava/lang/Boolean;

    .line 53
    const v2, 0x7f050007

    .line 54
    .local v2, "nTitleId":I
    const v3, 0x7f050008

    .line 55
    .local v3, "nTextId":I
    const v1, 0x7f020006

    .local v1, "nMoodId":I
    move v6, p0

    .line 57
    invoke-static/range {v0 .. v6}, Lcom/wsomacp/ui/XCPUINotiManager;->xcpSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xcpInitialize(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    sput-object p0, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    .line 27
    return-void
.end method

.method public static xcpRefreshNoti()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 126
    sget-object v7, Lcom/wsomacp/ui/XCPUINotiManager;->bIsNotiOn:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 128
    const/4 v5, 0x0

    .line 129
    .local v5, "text":Ljava/lang/CharSequence;
    const/4 v6, 0x0

    .line 131
    .local v6, "titletext":Ljava/lang/CharSequence;
    sget-object v7, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    if-nez v7, :cond_1

    .line 133
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v8, "mContext is null, return"

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    sget-object v7, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    const v8, 0x7f050007

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    .line 138
    sget-object v7, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    const v8, 0x7f050008

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .line 139
    const v2, 0x7f020006

    .line 141
    .local v2, "nMoodId":I
    const-string v7, "notification"

    invoke-static {v7}, Lcom/wsomacp/XCPApplication;->xcpGetSystemServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 142
    .local v4, "notificationManager":Landroid/app/NotificationManager;
    if-nez v4, :cond_2

    .line 144
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v8, "notificationManager is null, return"

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_2
    new-instance v1, Landroid/content/Intent;

    sget-object v7, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    const-class v8, Lcom/wsomacp/WSSClientProvUiList;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 149
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v7, 0x34000000

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 150
    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    sget-object v7, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    invoke-static {v7, v9, v1, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 153
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    new-instance v7, Landroid/app/Notification$Builder;

    sget-object v8, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    sget-wide v8, Lcom/wsomacp/ui/XCPUINotiManager;->NotiTime:J

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 154
    .local v3, "notification":Landroid/app/Notification;
    const/16 v7, 0x30

    iput v7, v3, Landroid/app/Notification;->flags:I

    .line 155
    const/4 v7, 0x1

    invoke-virtual {v4, v7, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public static xcpSetNotification(Landroid/app/NotificationManager;IIIIII)V
    .locals 12
    .param p0, "notificationManager"    # Landroid/app/NotificationManager;
    .param p1, "nMoodId"    # I
    .param p2, "nTitleId"    # I
    .param p3, "nTextId"    # I
    .param p4, "nTickerId"    # I
    .param p5, "nId"    # I
    .param p6, "nIndicatorState"    # I

    .prologue
    .line 71
    const/4 v7, 0x0

    .line 72
    .local v7, "text":Ljava/lang/CharSequence;
    const/4 v8, 0x0

    .line 73
    .local v8, "titletext":Ljava/lang/CharSequence;
    const/4 v6, 0x0

    .line 75
    .local v6, "szTickerText":Ljava/lang/String;
    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    if-nez v9, :cond_0

    .line 77
    sget-object v9, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v10, "mContext is null, return"

    invoke-virtual {v9, v10}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 81
    :cond_0
    if-nez p0, :cond_1

    .line 83
    sget-object v9, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v10, "notificationManager is null, return"

    invoke-virtual {v9, v10}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :cond_1
    if-lez p3, :cond_2

    .line 88
    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    invoke-virtual {v9, p3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .line 90
    :cond_2
    if-lez p2, :cond_3

    .line 91
    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    invoke-virtual {v9, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    .line 93
    :cond_3
    if-lez p4, :cond_4

    .line 94
    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    move/from16 v0, p4

    invoke-virtual {v9, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .end local v6    # "szTickerText":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 96
    .restart local v6    # "szTickerText":Ljava/lang/String;
    :cond_4
    sget-object v9, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "NotificationID : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / Notification IndicatorState : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p6

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 98
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sput-wide v10, Lcom/wsomacp/ui/XCPUINotiManager;->NotiTime:J

    .line 100
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    invoke-direct {v2, v9}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 101
    .local v2, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-virtual {v2, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    sget-wide v10, Lcom/wsomacp/ui/XCPUINotiManager;->NotiTime:J

    invoke-virtual {v9, v10, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 104
    const/4 v9, 0x2

    move/from16 v0, p6

    if-ne v0, v9, :cond_5

    .line 106
    new-instance v4, Landroid/content/Intent;

    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    const-class v10, Lcom/wsomacp/WSSClientProvUiList;

    invoke-direct {v4, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v4, "intent":Landroid/content/Intent;
    const/high16 v9, 0x34000000

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 108
    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    sget-object v9, Lcom/wsomacp/ui/XCPUINotiManager;->m_Context:Landroid/content/Context;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v4, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 111
    .local v3, "contentIntent":Landroid/app/PendingIntent;
    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    .line 112
    .local v5, "notification":Landroid/app/Notification;
    const/16 v9, 0x30

    iput v9, v5, Landroid/app/Notification;->flags:I

    .line 113
    const/4 v9, 0x3

    iput v9, v5, Landroid/app/Notification;->defaults:I

    .line 120
    .end local v3    # "contentIntent":Landroid/app/PendingIntent;
    .end local v4    # "intent":Landroid/content/Intent;
    :goto_1
    move/from16 v0, p5

    invoke-virtual {p0, v0, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 121
    invoke-static {}, Lcom/wsomacp/XCPApplication;->xcpWakeLcdScreenOn()V

    goto/16 :goto_0

    .line 117
    .end local v5    # "notification":Landroid/app/Notification;
    :cond_5
    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    .restart local v5    # "notification":Landroid/app/Notification;
    goto :goto_1
.end method
