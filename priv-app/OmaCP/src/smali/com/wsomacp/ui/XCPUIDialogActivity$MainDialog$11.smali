.class Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$11;
.super Ljava/lang/Object;
.source "XCPUIDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;


# direct methods
.method constructor <init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$11;->this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v2, 0x0

    .line 380
    sget-boolean v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v0, :cond_0

    .line 381
    sput-boolean v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bTabletNextAccount:Z

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$11;->this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 385
    sget v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/wsomacp/ui/XCPUIDialogActivity;->xcpUIDialogActivityDeleteMsg(II)Z

    .line 387
    sget-boolean v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-nez v0, :cond_1

    .line 388
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailActivity;->xcpUIDetailActivityRemove()V

    .line 391
    :goto_0
    return-void

    .line 390
    :cond_1
    sput v2, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    goto :goto_0
.end method
