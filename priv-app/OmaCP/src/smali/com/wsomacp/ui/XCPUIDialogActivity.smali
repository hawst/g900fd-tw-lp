.class public Lcom/wsomacp/ui/XCPUIDialogActivity;
.super Landroid/app/Activity;
.source "XCPUIDialogActivity.java"

# interfaces
.implements Lcom/wsomacp/eng/core/XCPDecoder;
.implements Lcom/wsomacp/interfaces/XCPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    }
.end annotation


# static fields
.field private static dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

.field private static m_Context:Landroid/content/Context;

.field private static m_nBeforeDialogId:I

.field private static m_nSubid:J

.field private static m_szResponseText:Ljava/lang/String;


# instance fields
.field private m_nDialogId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;

    .line 38
    const-string v0, ""

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    .line 39
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nSubid:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 229
    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 32
    sput-object p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200()J
    .locals 2

    .prologue
    .line 32
    sget-wide v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nSubid:J

    return-wide v0
.end method

.method static synthetic access$300()Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    .locals 0
    .param p0, "x0"    # Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .prologue
    .line 32
    sput-object p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    return-object p0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nBeforeDialogId:I

    return v0
.end method

.method public static getDialogId(I)I
    .locals 4
    .param p0, "nId"    # I

    .prologue
    const/4 v0, 0x0

    .line 103
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 104
    packed-switch p0, :pswitch_data_0

    .line 140
    :goto_0
    :pswitch_0
    return v0

    .line 110
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 113
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 116
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    .line 119
    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    .line 122
    :pswitch_5
    const/4 v0, 0x2

    goto :goto_0

    .line 125
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 128
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_0

    .line 131
    :pswitch_8
    const/16 v0, 0x18

    goto :goto_0

    .line 134
    :pswitch_9
    const/16 v0, 0x19

    goto :goto_0

    .line 137
    :pswitch_a
    const/4 v0, 0x6

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static xcpUIDialogActivityDeleteMsg(II)Z
    .locals 3
    .param p0, "nId"    # I
    .param p1, "install"    # I

    .prologue
    .line 580
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 582
    sget-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    if-eqz v0, :cond_0

    .line 583
    int-to-long v0, p0

    invoke-static {v0, v1}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlRemoveEntry(J)V

    .line 587
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 585
    :cond_0
    int-to-long v0, p0

    invoke-static {v0, v1, p1}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlUpdateEntry(JI)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    sput-object p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;

    .line 48
    if-eqz p1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->finish()V

    .line 50
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 98
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 56
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/wsomacp/ui/XCPUIDialogActivity;->setIntent(Landroid/content/Intent;)V

    .line 58
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 91
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 64
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 66
    iget v2, p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nDialogId:I

    if-lez v2, :cond_0

    .line 68
    iget v2, p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nDialogId:I

    sput v2, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nBeforeDialogId:I

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "sid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 74
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nDialogId:I

    .line 76
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 77
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 79
    const-string v2, "subid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    sput-wide v2, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nSubid:J

    .line 80
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "subid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nSubid:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 83
    :cond_1
    iget v2, p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nDialogId:I

    invoke-virtual {p0, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity;->xcpUIPopUp(I)Z

    .line 85
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_2
    return-void
.end method

.method public xcpUIDialogActivityDeleteMsg(IZ)Z
    .locals 3
    .param p1, "nId"    # I
    .param p2, "bForceDel"    # Z

    .prologue
    .line 592
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 594
    if-nez p2, :cond_0

    sget-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    if-eqz v0, :cond_1

    .line 596
    :cond_0
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlRemoveEntry(J)V

    .line 599
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public xcpUIPopUp(I)Z
    .locals 5
    .param p1, "nId"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 146
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 148
    iget v0, p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nDialogId:I

    if-eq v0, p1, :cond_0

    .line 149
    iput p1, p0, Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nDialogId:I

    .line 151
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 226
    :goto_0
    return v3

    .line 154
    :pswitch_0
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "INSTALL_SUCCESS_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_1
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "PINCODE_INPUT_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 159
    invoke-static {v3}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 160
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_PINCODE_INPUT"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :pswitch_2
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "PINCODE_INPUT_AGAIN"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 165
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 166
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_PINCODE_AGAIN"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :pswitch_3
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "PINCODE_INPUNT_RETRY"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 172
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_PINCODE_FAIL"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :pswitch_4
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "INSTALL_FAIL_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 177
    sget-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    if-eqz v0, :cond_1

    .line 178
    sget v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    invoke-virtual {p0, v0, v4}, Lcom/wsomacp/ui/XCPUIDialogActivity;->xcpUIDialogActivityDeleteMsg(IZ)Z

    .line 179
    :cond_1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 180
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_INSTALL_FAILED"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :pswitch_5
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "INSTALL_RESULT_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 185
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 186
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_INSTALL_SUCCESS"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 190
    :pswitch_6
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "INSTALL_NEW_ACCOUNT"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 191
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 192
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_INSTALL_NEW_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    :pswitch_7
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "INSTALL_NOT_SUPPORT_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 197
    sget-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    if-eqz v0, :cond_2

    .line 198
    sget v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    invoke-virtual {p0, v0, v4}, Lcom/wsomacp/ui/XCPUIDialogActivity;->xcpUIDialogActivityDeleteMsg(IZ)Z

    .line 199
    :cond_2
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 200
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_INSTALL_NOT_SUPPORT"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 204
    :pswitch_8
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "DELETE_MESSAGE_CONFIRM"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 205
    const/16 v0, 0x18

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 206
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DELETE_MESSAGE_CONFIRM"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    :pswitch_9
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "DELETE_MULTIMESSAGE_CONFIRM"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 211
    const/16 v0, 0x19

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 212
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DELETE_MULTIMESSAGE_CONFIRM"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 216
    :pswitch_a
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "INSTALL_PARSER_FAIL_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 217
    sget v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    invoke-virtual {p0, v0, v3}, Lcom/wsomacp/ui/XCPUIDialogActivity;->xcpUIDialogActivityDeleteMsg(IZ)Z

    .line 218
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    sput-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 219
    sget-object v0, Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_PARSING_FAIL"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
