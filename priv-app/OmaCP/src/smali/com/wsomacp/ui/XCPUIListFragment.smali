.class public Lcom/wsomacp/ui/XCPUIListFragment;
.super Landroid/app/ListFragment;
.source "XCPUIListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field static final XCP_Menu_ID:I = 0x1

.field private static m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

.field public static m_ListView:Landroid/widget/ListView;

.field public static m_bDualPane:Z


# instance fields
.field private m_CpMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/wsomacp/agent/XCPItem;",
            ">;"
        }
    .end annotation
.end field

.field m_nCurCheckPosition:I

.field m_nShownCheckPosition:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nCurCheckPosition:I

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nShownCheckPosition:I

    return-void
.end method

.method public static xcpAddMessage()V
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 453
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 454
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 456
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v10

    if-nez v10, :cond_1

    .line 458
    sget-object v10, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "CP Message Empty !!! "

    invoke-virtual {v10, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 459
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 465
    .local v1, "id":I
    const-string v10, "%s  "

    new-array v11, v12, [Ljava/lang/Object;

    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/wsomacp/eng/core/XCPUtil;->xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v13

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 466
    .local v2, "date":Ljava/lang/String;
    const/4 v10, 0x2

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 467
    .local v3, "message":Ljava/lang/String;
    const/4 v10, 0x3

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 468
    .local v4, "installed":I
    const/4 v10, 0x4

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 470
    .local v5, "type":I
    const-wide/16 v6, 0x0

    .line 472
    .local v6, "subId":J
    sget-boolean v10, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    if-eqz v10, :cond_2

    .line 476
    :try_start_0
    sget-object v10, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "onActivityCreated ( subId - TRY ) "

    invoke-virtual {v10, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 477
    const/4 v10, 0x6

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    int-to-long v6, v10

    .line 486
    :cond_2
    :goto_1
    new-instance v0, Lcom/wsomacp/agent/XCPItem;

    invoke-direct/range {v0 .. v7}, Lcom/wsomacp/agent/XCPItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIJ)V

    .line 487
    .local v0, "item":Lcom/wsomacp/agent/XCPItem;
    sget-object v10, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    if-eqz v10, :cond_3

    .line 489
    sget-object v10, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v10, v0, v13}, Lcom/wsomacp/ui/XCPUIItemAdapter;->insert(Ljava/lang/Object;I)V

    .line 490
    sget v10, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    add-int/lit8 v10, v10, 0x1

    sput v10, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    .line 491
    sget v10, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    sput v10, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    .line 492
    sget-object v10, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v10}, Lcom/wsomacp/ui/XCPUIItemAdapter;->notifyDataSetChanged()V

    .line 495
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 496
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 497
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 479
    .end local v0    # "item":Lcom/wsomacp/agent/XCPItem;
    :catch_0
    move-exception v9

    .line 481
    .local v9, "e":Ljava/lang/IllegalStateException;
    sget-object v10, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "onActivityCreated ( subId - Exception : SEt subId - 0) "

    invoke-virtual {v10, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 482
    const-wide/16 v6, 0x0

    goto :goto_1
.end method

.method public static xcpResetPosition()V
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    if-eqz v0, :cond_0

    .line 505
    const/4 v0, 0x0

    sput v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    .line 506
    sget v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    sput v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    .line 508
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super/range {p0 .. p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 53
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, ""

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 55
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CpMessageList:Ljava/util/ArrayList;

    .line 56
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v10

    .line 57
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_3

    .line 59
    invoke-interface {v10}, Landroid/database/Cursor;->moveToLast()Z

    move-result v14

    if-nez v14, :cond_1

    .line 61
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, "CP Message Empty !!! "

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 62
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Activity;->finish()V

    .line 66
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, "Actvity finish"

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    const/4 v14, 0x0

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 75
    .local v3, "id":I
    const-string v14, "%s  "

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/wsomacp/eng/core/XCPUtil;->xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "date":Ljava/lang/String;
    const/4 v14, 0x2

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 77
    .local v5, "message":Ljava/lang/String;
    const/4 v14, 0x3

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 78
    .local v6, "installed":I
    const/4 v14, 0x4

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 80
    .local v7, "type":I
    const-wide/16 v8, 0x0

    .line 82
    .local v8, "subId":J
    sget-boolean v14, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    if-eqz v14, :cond_2

    .line 86
    :try_start_0
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, "onActivityCreated ( subId - TRY ) "

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 87
    const/4 v14, 0x6

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v14

    int-to-long v8, v14

    .line 96
    :cond_2
    :goto_1
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onResume ( subId ) "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 97
    new-instance v2, Lcom/wsomacp/agent/XCPItem;

    invoke-direct/range {v2 .. v9}, Lcom/wsomacp/agent/XCPItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIJ)V

    .line 98
    .local v2, "item":Lcom/wsomacp/agent/XCPItem;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-interface {v10}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v14

    if-nez v14, :cond_1

    .line 101
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 102
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 103
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 106
    .end local v2    # "item":Lcom/wsomacp/agent/XCPItem;
    .end local v3    # "id":I
    .end local v4    # "date":Ljava/lang/String;
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "installed":I
    .end local v7    # "type":I
    .end local v8    # "subId":J
    :cond_3
    new-instance v14, Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-static {}, Lcom/wsomacp/WSSClientProvUiList;->getContext()Landroid/content/Context;

    move-result-object v15

    const v16, 0x7f030005

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CpMessageList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-direct/range {v14 .. v18}, Lcom/wsomacp/ui/XCPUIItemAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    sput-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    .line 107
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/wsomacp/ui/XCPUIListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v14

    sput-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    .line 110
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 111
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/ListView;->setClickable(Z)V

    .line 113
    sget-boolean v14, Lcom/wsomacp/feature/XCPFeature;->XCP_DEVICETYPE_FEATURE_TABLET:Z

    if-eqz v14, :cond_4

    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    const v15, 0x7f080017

    invoke-virtual {v14, v15}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 116
    .local v11, "detailsFrame":Landroid/view/View;
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_6

    const/4 v14, 0x1

    :goto_2
    sput-boolean v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    .line 119
    .end local v11    # "detailsFrame":Landroid/view/View;
    :cond_4
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v14}, Lcom/wsomacp/ui/XCPUIItemAdapter;->XCPUIGetItems()Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 123
    :try_start_1
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v14}, Lcom/wsomacp/ui/XCPUIItemAdapter;->XCPUIGetItems()Ljava/util/ArrayList;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/wsomacp/agent/XCPItem;

    invoke-virtual {v14}, Lcom/wsomacp/agent/XCPItem;->xcpGetCpID()I

    move-result v13

    .line 124
    .local v13, "nIndex":I
    if-eqz p1, :cond_5

    .line 127
    const-string v14, "curChoice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nCurCheckPosition:I

    .line 128
    const-string v14, "shownChoice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nShownCheckPosition:I

    .line 130
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mCurCheckPosition : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nCurCheckPosition:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 131
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mShownCheckPosition : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nShownCheckPosition:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 134
    :cond_5
    sget-boolean v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v14, :cond_7

    .line 136
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, "mDualPane"

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 137
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 138
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/wsomacp/ui/XCPUIListFragment;->xcpShowDetails(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 148
    .end local v13    # "nIndex":I
    :catch_0
    move-exception v12

    .line 150
    .local v12, "e":Ljava/lang/Exception;
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v3    # "id":I
    .restart local v4    # "date":Ljava/lang/String;
    .restart local v5    # "message":Ljava/lang/String;
    .restart local v6    # "installed":I
    .restart local v7    # "type":I
    .restart local v8    # "subId":J
    :catch_1
    move-exception v12

    .line 91
    .local v12, "e":Ljava/lang/IllegalStateException;
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, "onActivityCreated ( subId - Exception : SEt subId - 0) "

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 92
    const-wide/16 v8, 0x0

    goto/16 :goto_1

    .line 116
    .end local v3    # "id":I
    .end local v4    # "date":Ljava/lang/String;
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "installed":I
    .end local v7    # "type":I
    .end local v8    # "subId":J
    .end local v12    # "e":Ljava/lang/IllegalStateException;
    .restart local v11    # "detailsFrame":Landroid/view/View;
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 142
    .end local v11    # "detailsFrame":Landroid/view/View;
    .restart local v13    # "nIndex":I
    :cond_7
    :try_start_2
    sget-object v14, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v15, "SinglePanel"

    invoke-virtual {v14, v15}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 143
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailActivity;->xcpUIDetailActivityRemove()V

    .line 144
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 145
    sget-object v14, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 327
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 328
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 447
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 448
    const/4 v0, 0x0

    sput v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    .line 449
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 25
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    sget-object v20, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "position : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 188
    sget-object v16, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    .line 189
    .local v16, "list":Landroid/widget/ListView;
    if-nez v16, :cond_0

    .line 191
    sget-object v20, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v21, "list is null"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 322
    :goto_0
    return-void

    .line 194
    :cond_0
    sget-object v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    move-object/from16 v0, v20

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/wsomacp/agent/XCPItem;

    .line 195
    .local v10, "cpItem":Lcom/wsomacp/agent/XCPItem;
    sput p3, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    .line 197
    sput p3, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    .line 198
    invoke-virtual {v10}, Lcom/wsomacp/agent/XCPItem;->xcpGetCpID()I

    move-result v20

    sput v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nCPId:I

    .line 199
    invoke-virtual {v10}, Lcom/wsomacp/agent/XCPItem;->xcpGetType()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 201
    sget-object v20, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v21, "DM Bootstrap Message"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 203
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ListView;->getChildCount()I

    move-result v6

    .line 204
    .local v6, "childCount":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    if-ge v14, v6, :cond_2

    .line 206
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 207
    .local v7, "childView":Landroid/view/View;
    const v20, 0x7f08001a

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 208
    .local v5, "TitleText":Landroid/widget/TextView;
    const v20, 0x7f08001b

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 209
    .local v3, "DateText":Landroid/widget/TextView;
    const v20, 0x7f08001c

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 211
    .local v4, "SubText":Landroid/widget/TextView;
    sget-boolean v20, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v20, :cond_1

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f040001

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 214
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f040002

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 215
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f040002

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 204
    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 220
    .end local v3    # "DateText":Landroid/widget/TextView;
    .end local v4    # "SubText":Landroid/widget/TextView;
    .end local v5    # "TitleText":Landroid/widget/TextView;
    .end local v7    # "childView":Landroid/view/View;
    :cond_2
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v20

    sub-int v20, p3, v20

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 221
    .restart local v7    # "childView":Landroid/view/View;
    const v20, 0x7f08001a

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 222
    .restart local v5    # "TitleText":Landroid/widget/TextView;
    const v20, 0x7f08001b

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 223
    .restart local v3    # "DateText":Landroid/widget/TextView;
    const v20, 0x7f08001c

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 224
    .restart local v4    # "SubText":Landroid/widget/TextView;
    sget-boolean v20, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v20, :cond_3

    .line 226
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [[I

    move-object/from16 v18, v0

    const/16 v20, 0x0

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const v23, 0x10100a7

    aput v23, v21, v22

    aput-object v21, v18, v20

    const/16 v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const v23, -0x10100a7

    aput v23, v21, v22

    aput-object v21, v18, v20

    .line 230
    .local v18, "states":[[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v8, v0, [I

    const/16 v20, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f040001

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    aput v21, v8, v20

    const/16 v20, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f040004

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    aput v21, v8, v20

    .line 232
    .local v8, "color":[I
    new-instance v9, Landroid/content/res/ColorStateList;

    move-object/from16 v0, v18

    invoke-direct {v9, v0, v8}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 234
    .local v9, "colors":Landroid/content/res/ColorStateList;
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 235
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 236
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 239
    .end local v8    # "color":[I
    .end local v9    # "colors":Landroid/content/res/ColorStateList;
    .end local v18    # "states":[[I
    :cond_3
    sget-boolean v20, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v20, :cond_4

    .line 241
    sget v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nCPId:I

    sget-object v21, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-static/range {v20 .. v21}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpNewInstance(ILandroid/content/Context;)Lcom/wsomacp/ui/XCPUIDetailFragment;

    move-result-object v11

    .line 242
    .local v11, "df":Lcom/wsomacp/ui/XCPUIDetailFragment;
    sget-object v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    check-cast v20, Landroid/app/Activity;

    invoke-virtual/range {v20 .. v20}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v13

    .line 243
    .local v13, "ft":Landroid/app/FragmentTransaction;
    const v20, 0x7f080017

    move/from16 v0, v20

    invoke-virtual {v13, v0, v11}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 244
    const/16 v20, 0x1003

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 245
    invoke-virtual {v13}, Landroid/app/FragmentTransaction;->commit()I

    .line 249
    .end local v11    # "df":Lcom/wsomacp/ui/XCPUIDetailFragment;
    .end local v13    # "ft":Landroid/app/FragmentTransaction;
    :cond_4
    sget-object v20, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v21, "%s/items/%d"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    sget-object v24, Lcom/wsomacp/ui/XCPUIItemAdapter;->XCP_CONTENT_URI:Landroid/net/Uri;

    aput-object v24, v22, v23

    const/16 v23, 0x1

    sget v24, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nCPId:I

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v20 .. v22}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 250
    .local v19, "struri":Ljava/lang/String;
    sget-object v20, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onClick DM Bootstrap Message === "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 251
    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 252
    .local v17, "sendDBUri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v20, "android.provider.Telephony.WAP_PUSH_DM_RECEIVED"

    move-object/from16 v0, v20

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 253
    .local v2, "DMintent":Landroid/content/Intent;
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 254
    .local v12, "extra":Landroid/os/Bundle;
    const-string v20, "dm_message"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 255
    invoke-virtual {v2, v12}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 256
    const-string v20, "wsomacp_enable"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 257
    const/16 v20, 0x20

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 258
    sget-object v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 262
    .end local v2    # "DMintent":Landroid/content/Intent;
    .end local v3    # "DateText":Landroid/widget/TextView;
    .end local v4    # "SubText":Landroid/widget/TextView;
    .end local v5    # "TitleText":Landroid/widget/TextView;
    .end local v6    # "childCount":I
    .end local v7    # "childView":Landroid/view/View;
    .end local v12    # "extra":Landroid/os/Bundle;
    .end local v14    # "i":I
    .end local v17    # "sendDBUri":Landroid/net/Uri;
    .end local v19    # "struri":Ljava/lang/String;
    :cond_5
    sget-object v20, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v21, "CP Message"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 264
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ListView;->getChildCount()I

    move-result v6

    .line 265
    .restart local v6    # "childCount":I
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_2
    if-ge v14, v6, :cond_7

    .line 267
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 268
    .restart local v7    # "childView":Landroid/view/View;
    const v20, 0x7f08001a

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 269
    .restart local v5    # "TitleText":Landroid/widget/TextView;
    const v20, 0x7f08001b

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 270
    .restart local v3    # "DateText":Landroid/widget/TextView;
    const v20, 0x7f08001c

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 272
    .restart local v4    # "SubText":Landroid/widget/TextView;
    sget-boolean v20, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v20, :cond_6

    .line 274
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f040001

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f040002

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f040002

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 265
    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 281
    .end local v3    # "DateText":Landroid/widget/TextView;
    .end local v4    # "SubText":Landroid/widget/TextView;
    .end local v5    # "TitleText":Landroid/widget/TextView;
    .end local v7    # "childView":Landroid/view/View;
    :cond_7
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v20

    sub-int v20, p3, v20

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 282
    .restart local v7    # "childView":Landroid/view/View;
    if-nez v7, :cond_8

    .line 284
    sget-object v20, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v21, "childView is null"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 287
    :cond_8
    const v20, 0x7f08001a

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 288
    .restart local v5    # "TitleText":Landroid/widget/TextView;
    const v20, 0x7f08001b

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 289
    .restart local v3    # "DateText":Landroid/widget/TextView;
    const v20, 0x7f08001c

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 291
    .restart local v4    # "SubText":Landroid/widget/TextView;
    sget-boolean v20, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v20, :cond_9

    .line 293
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [[I

    move-object/from16 v18, v0

    const/16 v20, 0x0

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const v23, 0x10100a7

    aput v23, v21, v22

    aput-object v21, v18, v20

    const/16 v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const v23, -0x10100a7

    aput v23, v21, v22

    aput-object v21, v18, v20

    .line 297
    .restart local v18    # "states":[[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v8, v0, [I

    const/16 v20, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f040001

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    aput v21, v8, v20

    const/16 v20, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f040004

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    aput v21, v8, v20

    .line 299
    .restart local v8    # "color":[I
    new-instance v9, Landroid/content/res/ColorStateList;

    move-object/from16 v0, v18

    invoke-direct {v9, v0, v8}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 301
    .restart local v9    # "colors":Landroid/content/res/ColorStateList;
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 302
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 303
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 306
    .end local v8    # "color":[I
    .end local v9    # "colors":Landroid/content/res/ColorStateList;
    .end local v18    # "states":[[I
    :cond_9
    sget-boolean v20, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v20, :cond_a

    .line 308
    sget v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nCPId:I

    sget-object v21, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-static/range {v20 .. v21}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpNewInstance(ILandroid/content/Context;)Lcom/wsomacp/ui/XCPUIDetailFragment;

    move-result-object v11

    .line 309
    .restart local v11    # "df":Lcom/wsomacp/ui/XCPUIDetailFragment;
    sget-object v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    check-cast v20, Landroid/app/Activity;

    invoke-virtual/range {v20 .. v20}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v13

    .line 310
    .restart local v13    # "ft":Landroid/app/FragmentTransaction;
    const v20, 0x7f080017

    move/from16 v0, v20

    invoke-virtual {v13, v0, v11}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 311
    const/16 v20, 0x1003

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 312
    invoke-virtual {v13}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 316
    .end local v11    # "df":Lcom/wsomacp/ui/XCPUIDetailFragment;
    .end local v13    # "ft":Landroid/app/FragmentTransaction;
    :cond_a
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 317
    .local v15, "intent":Landroid/content/Intent;
    sget-object v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const-class v21, Lcom/wsomacp/ui/XCPUIDetailActivity;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 318
    const-string v20, "index"

    sget v21, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nCPId:I

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 319
    sget-object v20, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 338
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 340
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/wsomacp/ui/XCPUIListFragment;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 341
    return-void
.end method

.method public onResume()V
    .locals 18

    .prologue
    .line 345
    invoke-super/range {p0 .. p0}, Landroid/app/ListFragment;->onResume()V

    .line 346
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, ""

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 348
    const/4 v8, 0x1

    invoke-static {v8}, Lcom/wsomacp/ui/XCPUINotiManager;->setIndicator(I)V

    .line 350
    sget-boolean v8, Lcom/wsomacp/ui/XCPUIDetailFragment;->m_bTabletNextAccount:Z

    if-nez v8, :cond_0

    .line 352
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CpMessageList:Ljava/util/ArrayList;

    .line 353
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v10

    .line 354
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_3

    .line 356
    invoke-interface {v10}, Landroid/database/Cursor;->moveToLast()Z

    move-result v8

    if-nez v8, :cond_1

    .line 358
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "CP Message Empty !!! "

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 359
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 360
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 362
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    .line 363
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "Actvity finish"

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 433
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 371
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_1
    const/4 v8, 0x0

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 372
    .local v3, "id":I
    const-string v8, "%s  "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/wsomacp/eng/core/XCPUtil;->xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v9, v15

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 373
    .local v4, "date":Ljava/lang/String;
    const/4 v8, 0x2

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 374
    .local v5, "message":Ljava/lang/String;
    const/4 v8, 0x3

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 375
    .local v6, "installed":I
    const/4 v8, 0x4

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 377
    .local v7, "type":I
    const/4 v14, 0x0

    .line 379
    .local v14, "subId":I
    sget-boolean v8, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    if-eqz v8, :cond_2

    .line 383
    :try_start_0
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "onResume ( subId - TRY ) "

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 384
    const/4 v8, 0x6

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v14

    .line 393
    :cond_2
    :goto_1
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onResume ( subId ) "

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 394
    new-instance v2, Lcom/wsomacp/agent/XCPItem;

    int-to-long v8, v14

    invoke-direct/range {v2 .. v9}, Lcom/wsomacp/agent/XCPItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIJ)V

    .line 395
    .local v2, "item":Lcom/wsomacp/agent/XCPItem;
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "installed : "

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 396
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    invoke-interface {v10}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v8

    if-nez v8, :cond_1

    .line 399
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 400
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 401
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 404
    .end local v2    # "item":Lcom/wsomacp/agent/XCPItem;
    .end local v3    # "id":I
    .end local v4    # "date":Ljava/lang/String;
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "installed":I
    .end local v7    # "type":I
    .end local v14    # "subId":I
    :cond_3
    new-instance v8, Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-static {}, Lcom/wsomacp/WSSClientProvUiList;->getContext()Landroid/content/Context;

    move-result-object v9

    const v15, 0x7f030005

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wsomacp/ui/XCPUIListFragment;->m_CpMessageList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v8, v9, v15, v0, v1}, Lcom/wsomacp/ui/XCPUIItemAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    sput-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    .line 405
    sget-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/wsomacp/ui/XCPUIListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 407
    sget-boolean v8, Lcom/wsomacp/feature/XCPFeature;->XCP_DEVICETYPE_FEATURE_TABLET:Z

    if-eqz v8, :cond_4

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f080017

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 410
    .local v11, "detailsFrame":Landroid/view/View;
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_6

    const/4 v8, 0x1

    :goto_2
    sput-boolean v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    .line 413
    .end local v11    # "detailsFrame":Landroid/view/View;
    :cond_4
    sget-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v8}, Lcom/wsomacp/ui/XCPUIItemAdapter;->getCount()I

    move-result v8

    if-lez v8, :cond_0

    .line 415
    sget v8, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    sget-object v9, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v9}, Lcom/wsomacp/ui/XCPUIItemAdapter;->getCount()I

    move-result v9

    if-ne v8, v9, :cond_5

    .line 416
    const/4 v8, 0x0

    sput v8, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    .line 418
    :cond_5
    sget-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v8}, Lcom/wsomacp/ui/XCPUIItemAdapter;->XCPUIGetItems()Ljava/util/ArrayList;

    move-result-object v8

    sget v9, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/wsomacp/agent/XCPItem;

    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->xcpGetCpID()I

    move-result v13

    .line 419
    .local v13, "nIndex":I
    sget-boolean v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v8, :cond_7

    .line 421
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "mDualPane"

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 422
    sget-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 423
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/wsomacp/ui/XCPUIListFragment;->xcpShowDetails(I)V

    goto/16 :goto_0

    .line 386
    .end local v13    # "nIndex":I
    .restart local v3    # "id":I
    .restart local v4    # "date":Ljava/lang/String;
    .restart local v5    # "message":Ljava/lang/String;
    .restart local v6    # "installed":I
    .restart local v7    # "type":I
    .restart local v14    # "subId":I
    :catch_0
    move-exception v12

    .line 388
    .local v12, "e":Ljava/lang/IllegalStateException;
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "onResume ( subId - Exception : SEt subId - 0) "

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 389
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 410
    .end local v3    # "id":I
    .end local v4    # "date":Ljava/lang/String;
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "installed":I
    .end local v7    # "type":I
    .end local v12    # "e":Ljava/lang/IllegalStateException;
    .end local v14    # "subId":I
    .restart local v11    # "detailsFrame":Landroid/view/View;
    :cond_6
    const/4 v8, 0x0

    goto :goto_2

    .line 427
    .end local v11    # "detailsFrame":Landroid/view/View;
    .restart local v13    # "nIndex":I
    :cond_7
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, "SinglePanel"

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 428
    sget-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 429
    sget-object v8, Lcom/wsomacp/ui/XCPUIListFragment;->m_ListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 438
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 439
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 440
    const-string v0, "curChoice"

    iget v1, p0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nCurCheckPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 441
    const-string v0, "shownChoice"

    iget v1, p0, Lcom/wsomacp/ui/XCPUIListFragment;->m_nShownCheckPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 442
    return-void
.end method

.method public xcpLoadAdapter(Lcom/wsomacp/ui/XCPUIItemAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/wsomacp/ui/XCPUIItemAdapter;

    .prologue
    .line 332
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 333
    sput-object p1, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    .line 334
    return-void
.end method

.method xcpShowDetails(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const v5, 0x7f080017

    .line 157
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 158
    sget-boolean v3, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v3, :cond_1

    .line 160
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/wsomacp/ui/XCPUIDetailFragment;

    .line 162
    .local v0, "details":Lcom/wsomacp/ui/XCPUIDetailFragment;
    sget-object v3, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v3}, Lcom/wsomacp/ui/XCPUIItemAdapter;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 164
    sget-object v3, Lcom/wsomacp/ui/XCPUIListFragment;->m_CAdapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v3}, Lcom/wsomacp/ui/XCPUIItemAdapter;->XCPUIGetItems()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/wsomacp/agent/XCPItem;

    invoke-virtual {v3}, Lcom/wsomacp/agent/XCPItem;->xcpGetType()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 166
    invoke-static {}, Lcom/wsomacp/WSSClientProvUiList;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpNewInstance(ILandroid/content/Context;)Lcom/wsomacp/ui/XCPUIDetailFragment;

    move-result-object v0

    .line 167
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 168
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v1, v5, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 169
    const/16 v3, 0x1003

    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 170
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 181
    .end local v0    # "details":Lcom/wsomacp/ui/XCPUIDetailFragment;
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 177
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/wsomacp/ui/XCPUIDetailActivity;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 178
    const-string v3, "index"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v2}, Lcom/wsomacp/ui/XCPUIListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
