.class Lcom/wsomacp/ui/XCPUIListDelActivity$2;
.super Ljava/lang/Object;
.source "XCPUIListDelActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wsomacp/ui/XCPUIListDelActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;


# direct methods
.method constructor <init>(Lcom/wsomacp/ui/XCPUIListDelActivity;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 113
    if-nez p3, :cond_3

    .line 115
    sget-object v7, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    invoke-virtual {v7}, Lcom/sec/android/fota/variant/view/CheckBox;->isChecked()Z

    move-result v7

    if-nez v7, :cond_0

    .line 117
    .local v1, "bDeleteLogAllCheck":Z
    :goto_0
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->mDone:Landroid/view/MenuItem;
    invoke-static {v7}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$000(Lcom/wsomacp/ui/XCPUIListDelActivity;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 118
    sget-object v7, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    invoke-virtual {v7, v1}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    .line 120
    const/4 v4, 0x0

    .local v4, "idx":I
    :goto_1
    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$100()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_1

    .line 122
    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$200()Lcom/wsomacp/ui/XCPUIItemAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/wsomacp/ui/XCPUIItemAdapter;->XCPUIGetItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/wsomacp/agent/XCPItem;

    invoke-virtual {v7, v1}, Lcom/wsomacp/agent/XCPItem;->xcpSetmChecked(Z)V

    .line 123
    sget-object v7, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_ListView:Landroid/widget/ListView;

    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$200()Lcom/wsomacp/ui/XCPUIItemAdapter;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 120
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v1    # "bDeleteLogAllCheck":Z
    .end local v4    # "idx":I
    :cond_0
    move v1, v8

    .line 115
    goto :goto_0

    .line 125
    .restart local v1    # "bDeleteLogAllCheck":Z
    .restart local v4    # "idx":I
    :cond_1
    if-eqz v1, :cond_2

    .line 126
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/wsomacp/ui/XCPUIListDelActivity;->setCountItems(I)V

    .line 168
    .end local v1    # "bDeleteLogAllCheck":Z
    .end local v4    # "idx":I
    :goto_2
    return-void

    .line 128
    .restart local v1    # "bDeleteLogAllCheck":Z
    .restart local v4    # "idx":I
    :cond_2
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    invoke-virtual {v7, v8}, Lcom/wsomacp/ui/XCPUIListDelActivity;->setCountItems(I)V

    goto :goto_2

    .line 132
    .end local v1    # "bDeleteLogAllCheck":Z
    .end local v4    # "idx":I
    :cond_3
    sget-object v7, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    add-int/lit8 v9, p3, -0x1

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wsomacp/agent/XCPItem;

    .line 133
    .local v2, "cpItem":Lcom/wsomacp/agent/XCPItem;
    const v7, 0x7f08001d

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/fota/variant/view/CheckBox;

    .line 135
    .local v6, "titlecheckbox":Lcom/sec/android/fota/variant/view/CheckBox;
    const/4 v5, 0x0

    .line 137
    .local v5, "nChecked":I
    invoke-virtual {v6}, Lcom/sec/android/fota/variant/view/CheckBox;->isChecked()Z

    move-result v7

    if-nez v7, :cond_5

    move v0, v1

    .line 139
    .local v0, "bChecked":Z
    :goto_3
    invoke-virtual {v6, v0}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    .line 141
    if-eqz v0, :cond_6

    .line 142
    invoke-virtual {v2, v1}, Lcom/wsomacp/agent/XCPItem;->xcpSetmChecked(Z)V

    .line 146
    :goto_4
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_5
    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$100()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_7

    .line 148
    sget-object v7, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/wsomacp/agent/XCPItem;

    invoke-virtual {v7}, Lcom/wsomacp/agent/XCPItem;->xcpIsmChecked()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 150
    add-int/lit8 v5, v5, 0x1

    .line 152
    :cond_4
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    invoke-virtual {v7, v5}, Lcom/wsomacp/ui/XCPUIListDelActivity;->setCountItems(I)V

    .line 146
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .end local v0    # "bChecked":Z
    .end local v3    # "i":I
    :cond_5
    move v0, v8

    .line 137
    goto :goto_3

    .line 144
    .restart local v0    # "bChecked":Z
    :cond_6
    invoke-virtual {v2, v8}, Lcom/wsomacp/agent/XCPItem;->xcpSetmChecked(Z)V

    goto :goto_4

    .line 154
    .restart local v3    # "i":I
    :cond_7
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->mDone:Landroid/view/MenuItem;
    invoke-static {v7}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$000(Lcom/wsomacp/ui/XCPUIListDelActivity;)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 156
    if-lez v5, :cond_9

    .line 157
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->mDone:Landroid/view/MenuItem;
    invoke-static {v7}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$000(Lcom/wsomacp/ui/XCPUIListDelActivity;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 162
    :cond_8
    :goto_6
    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$100()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v5, v7, :cond_a

    .line 163
    sget-object v7, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    invoke-virtual {v7, v1}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 159
    :cond_9
    iget-object v7, p0, Lcom/wsomacp/ui/XCPUIListDelActivity$2;->this$0:Lcom/wsomacp/ui/XCPUIListDelActivity;

    # getter for: Lcom/wsomacp/ui/XCPUIListDelActivity;->mDone:Landroid/view/MenuItem;
    invoke-static {v7}, Lcom/wsomacp/ui/XCPUIListDelActivity;->access$000(Lcom/wsomacp/ui/XCPUIListDelActivity;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_6

    .line 165
    :cond_a
    sget-object v7, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    goto/16 :goto_2
.end method
