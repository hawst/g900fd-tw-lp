.class Lcom/wsomacp/ui/XCPUIDetailFragment$1;
.super Ljava/lang/Object;
.source "XCPUIDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wsomacp/ui/XCPUIDetailFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wsomacp/ui/XCPUIDetailFragment;


# direct methods
.method constructor <init>(Lcom/wsomacp/ui/XCPUIDetailFragment;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/wsomacp/ui/XCPUIDetailFragment$1;->this$0:Lcom/wsomacp/ui/XCPUIDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 262
    :try_start_0
    sget-boolean v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_bMultiMessage:Z

    if-nez v1, :cond_1

    .line 264
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "bMultiMessage false"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 265
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    sget v2, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_nCPId:I

    sget-object v3, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    sget-object v4, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    # getter for: Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailFragment;->access$000()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/wsomacp/ui/XCPUIDetailFragment$1;->this$0:Lcom/wsomacp/ui/XCPUIDetailFragment;

    iget-wide v6, v6, Lcom/wsomacp/ui/XCPUIDetailFragment;->nSubid:J

    invoke-virtual/range {v1 .. v7}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstall(ILcom/wsomacp/eng/parser/XCPCtxWapDoc;Lcom/wsomacp/eng/core/XCPSMLWapPush;Landroid/content/Context;J)I

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "bMultiMessage true"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 270
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    # getter for: Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailFragment;->access$000()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallProgress(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    new-instance v0, Lcom/wsomacp/agent/XCPConAccount;

    # getter for: Lcom/wsomacp/ui/XCPUIDetailFragment;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailFragment;->access$000()Landroid/content/Context;

    move-result-object v1

    # getter for: Lcom/wsomacp/ui/XCPUIDetailFragment;->m_szAppName:Ljava/lang/String;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailFragment;->access$100()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/wsomacp/agent/XCPConAccount;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 273
    .local v0, "ConAccount":Lcom/wsomacp/agent/XCPConAccount;
    sget-object v1, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    sget-object v2, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDefMulti:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIDetailFragment$1;->this$0:Lcom/wsomacp/ui/XCPUIDetailFragment;

    iget-wide v4, v3, Lcom/wsomacp/ui/XCPUIDetailFragment;->nSubid:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetConnecting(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 277
    .end local v0    # "ConAccount":Lcom/wsomacp/agent/XCPConAccount;
    :catch_0
    move-exception v8

    .line 279
    .local v8, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method
