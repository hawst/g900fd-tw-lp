.class public Lcom/wsomacp/ui/XCPUIListDelActivity;
.super Landroid/app/Activity;
.source "XCPUIListDelActivity.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static g_ListView:Landroid/widget/ListView;

.field public static g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

.field private static m_Activity:Landroid/app/Activity;

.field private static m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

.field private static m_Context:Landroid/content/Context;

.field private static m_CpMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/wsomacp/agent/XCPItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private actionbar:Landroid/app/ActionBar;

.field private mDone:Landroid/view/MenuItem;

.field private mSelectCount:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Activity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/wsomacp/ui/XCPUIListDelActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/wsomacp/ui/XCPUIListDelActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->mDone:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200()Lcom/wsomacp/ui/XCPUIItemAdapter;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    return-object v0
.end method

.method public static xcpAddMessageInDel()V
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 304
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 305
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 307
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v10

    if-nez v10, :cond_1

    .line 309
    sget-object v10, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "CP Message Empty !!! "

    invoke-virtual {v10, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 310
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 316
    .local v1, "id":I
    const-string v10, "%s  "

    new-array v11, v12, [Ljava/lang/Object;

    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/wsomacp/eng/core/XCPUtil;->xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v13

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 317
    .local v2, "date":Ljava/lang/String;
    const/4 v10, 0x2

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 318
    .local v3, "message":Ljava/lang/String;
    const/4 v10, 0x3

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 319
    .local v4, "installed":I
    const/4 v10, 0x4

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 321
    .local v5, "type":I
    const-wide/16 v6, 0x0

    .line 323
    .local v6, "subId":J
    sget-boolean v10, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    if-eqz v10, :cond_2

    .line 327
    :try_start_0
    sget-object v10, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "onActivityCreated ( subId - TRY ) "

    invoke-virtual {v10, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 328
    const/4 v10, 0x6

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    int-to-long v6, v10

    .line 337
    :cond_2
    :goto_1
    new-instance v0, Lcom/wsomacp/agent/XCPItem;

    invoke-direct/range {v0 .. v7}, Lcom/wsomacp/agent/XCPItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIJ)V

    .line 338
    .local v0, "item":Lcom/wsomacp/agent/XCPItem;
    sget-object v10, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    if-eqz v10, :cond_3

    .line 340
    sget-object v10, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v10, v0, v13}, Lcom/wsomacp/ui/XCPUIItemAdapter;->insert(Ljava/lang/Object;I)V

    .line 341
    sget-object v10, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v10}, Lcom/wsomacp/ui/XCPUIItemAdapter;->notifyDataSetChanged()V

    .line 345
    :cond_3
    sget-object v10, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    if-eqz v10, :cond_4

    .line 346
    sget-object v10, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    invoke-virtual {v10, v13}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    .line 348
    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 349
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 350
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 330
    .end local v0    # "item":Lcom/wsomacp/agent/XCPItem;
    :catch_0
    move-exception v9

    .line 332
    .local v9, "e":Ljava/lang/IllegalStateException;
    sget-object v10, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "onActivityCreated ( subId - Exception : SEt subId - 0) "

    invoke-virtual {v10, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 333
    const-wide/16 v6, 0x0

    goto :goto_1
.end method

.method public static xcpDeleteCPItem()V
    .locals 4

    .prologue
    .line 271
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListFragment;->xcpResetPosition()V

    .line 273
    const/4 v1, 0x0

    .local v1, "key":I
    :goto_0
    sget-object v2, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 275
    sget-object v2, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wsomacp/agent/XCPItem;

    .line 276
    .local v0, "cpItem":Lcom/wsomacp/agent/XCPItem;
    invoke-virtual {v0}, Lcom/wsomacp/agent/XCPItem;->xcpIsmChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278
    invoke-virtual {v0}, Lcom/wsomacp/agent/XCPItem;->xcpGetCpID()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlRemoveEntry(J)V

    .line 273
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 281
    .end local v0    # "cpItem":Lcom/wsomacp/agent/XCPItem;
    :cond_1
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    .line 283
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->xcpUIDelteActivityRemove()V

    .line 284
    return-void
.end method

.method static declared-synchronized xcpItemInit()V
    .locals 14

    .prologue
    .line 228
    const-class v10, Lcom/wsomacp/ui/XCPUIListDelActivity;

    monitor-enter v10

    :try_start_0
    sget-object v9, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, ""

    invoke-virtual {v9, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 229
    sget-object v9, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    if-nez v9, :cond_1

    .line 231
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    sput-object v9, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    .line 232
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 233
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 235
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v9

    if-nez v9, :cond_2

    .line 237
    sget-object v9, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v11, "CP Message Empty !!! "

    invoke-virtual {v9, v11}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 238
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 239
    sget-object v9, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Activity:Landroid/app/Activity;

    if-eqz v9, :cond_0

    .line 241
    sget-object v9, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Activity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->finish()V

    .line 260
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 261
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 262
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    :cond_1
    monitor-exit v10

    return-void

    .line 248
    :cond_2
    const/4 v9, 0x0

    :try_start_1
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 250
    .local v1, "id":I
    const-string v9, "%s  "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/wsomacp/eng/core/XCPUtil;->xcpGetDateByLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 251
    .local v2, "date":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "message":Ljava/lang/String;
    const/4 v9, 0x3

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 253
    .local v4, "installed":I
    const/4 v9, 0x4

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 254
    .local v5, "type":I
    const/4 v9, 0x6

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    int-to-long v6, v9

    .line 256
    .local v6, "subId":J
    new-instance v0, Lcom/wsomacp/agent/XCPItem;

    invoke-direct/range {v0 .. v7}, Lcom/wsomacp/agent/XCPItem;-><init>(ILjava/lang/String;Ljava/lang/String;IIJ)V

    .line 257
    .local v0, "item":Lcom/wsomacp/agent/XCPItem;
    sget-object v9, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    invoke-interface {v8}, Landroid/database/Cursor;->moveToPrevious()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    if-nez v9, :cond_2

    goto :goto_0

    .line 228
    .end local v0    # "item":Lcom/wsomacp/agent/XCPItem;
    .end local v1    # "id":I
    .end local v2    # "date":Ljava/lang/String;
    .end local v3    # "message":Ljava/lang/String;
    .end local v4    # "installed":I
    .end local v5    # "type":I
    .end local v6    # "subId":J
    :catchall_0
    move-exception v9

    monitor-exit v10

    throw v9
.end method

.method public static xcpUIDelteActivityRemove()V
    .locals 3

    .prologue
    .line 290
    :try_start_0
    sget-object v1, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Activity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 292
    sget-object v1, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 293
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Actvity finish"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 296
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 298
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 53
    sput-object p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Context:Landroid/content/Context;

    .line 54
    sput-object v5, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    .line 55
    sput-object p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Activity:Landroid/app/Activity;

    .line 57
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iput-object v3, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->actionbar:Landroid/app/ActionBar;

    .line 58
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->actionbar:Landroid/app/ActionBar;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 60
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/high16 v4, 0x7f030000

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 61
    .local v2, "mCustomView":Landroid/view/View;
    iget-object v3, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 63
    const/high16 v3, 0x7f080000

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/wsomacp/ui/XCPUIListDelActivity$1;

    invoke-direct {v4, p0}, Lcom/wsomacp/ui/XCPUIListDelActivity$1;-><init>(Lcom/wsomacp/ui/XCPUIListDelActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    if-nez p1, :cond_0

    .line 73
    const v3, 0x7f030003

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIListDelActivity;->setContentView(I)V

    .line 74
    const v3, 0x7f080016

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIListDelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    sput-object v3, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_ListView:Landroid/widget/ListView;

    .line 75
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 76
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030002

    const v3, 0x7f080013

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIListDelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "header":Landroid/view/View;
    sget-object v3, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_ListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 78
    sget-object v3, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_ListView:Landroid/widget/ListView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 79
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/wsomacp/ui/XCPUIListDelActivity;->setCountItems(I)V

    .line 85
    .end local v0    # "header":Landroid/view/View;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->finish()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f08001f

    .line 184
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 186
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 188
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->mDone:Landroid/view/MenuItem;

    .line 189
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 191
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 91
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 92
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->finish()V

    .line 93
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 197
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    .line 199
    .local v3, "nSelectItem":I
    packed-switch v3, :pswitch_data_0

    .line 223
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    return v4

    .line 203
    :pswitch_0
    const/4 v0, 0x0

    .line 204
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "key":I
    :goto_1
    sget-object v4, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 206
    sget-object v4, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/wsomacp/agent/XCPItem;

    .line 207
    .local v1, "cpItem":Lcom/wsomacp/agent/XCPItem;
    invoke-virtual {v1}, Lcom/wsomacp/agent/XCPItem;->xcpIsmChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 209
    add-int/lit8 v0, v0, 0x1

    .line 204
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 213
    .end local v1    # "cpItem":Lcom/wsomacp/agent/XCPItem;
    :cond_1
    const/4 v4, 0x2

    if-lt v0, v4, :cond_2

    .line 214
    sget-object v4, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v5, 0x19

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 216
    :cond_2
    sget-object v4, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v5, 0x18

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x7f08001f
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 98
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 99
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUINotiManager;->setIndicator(I)V

    .line 102
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->xcpItemInit()V

    .line 104
    new-instance v0, Lcom/wsomacp/ui/XCPUIItemAdapter;

    sget-object v1, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Context:Landroid/content/Context;

    const v2, 0x7f030006

    sget-object v3, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_CpMessageList:Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/wsomacp/ui/XCPUIItemAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    sput-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    .line 105
    sget-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_ListView:Landroid/widget/ListView;

    sget-object v1, Lcom/wsomacp/ui/XCPUIListDelActivity;->m_Adapter:Lcom/wsomacp/ui/XCPUIItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    const v0, 0x7f080014

    invoke-virtual {p0, v0}, Lcom/wsomacp/ui/XCPUIListDelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/fota/variant/view/CheckBox;

    sput-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_selectAll_check:Lcom/sec/android/fota/variant/view/CheckBox;

    .line 109
    sget-object v0, Lcom/wsomacp/ui/XCPUIListDelActivity;->g_ListView:Landroid/widget/ListView;

    new-instance v1, Lcom/wsomacp/ui/XCPUIListDelActivity$2;

    invoke-direct {v1, p0}, Lcom/wsomacp/ui/XCPUIListDelActivity$2;-><init>(Lcom/wsomacp/ui/XCPUIListDelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 170
    return-void
.end method

.method public setCountItems(I)V
    .locals 5
    .param p1, "i"    # I

    .prologue
    .line 174
    const v1, 0x7f080001

    invoke-virtual {p0, v1}, Lcom/wsomacp/ui/XCPUIListDelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    .local v0, "mCount":Landroid/widget/TextView;
    const v1, 0x7f050032

    invoke-virtual {p0, v1}, Lcom/wsomacp/ui/XCPUIListDelActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->mSelectCount:Ljava/lang/String;

    .line 177
    if-eqz v0, :cond_0

    .line 178
    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIListDelActivity;->mSelectCount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    :cond_0
    return-void
.end method
