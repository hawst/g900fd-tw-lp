.class public Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
.super Landroid/app/DialogFragment;
.source "XCPUIDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wsomacp/ui/XCPUIDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MainDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 233
    new-instance v1, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-direct {v1}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;-><init>()V

    .line 234
    .local v1, "dialogFragment":Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 235
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 236
    invoke-virtual {v1, v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->setArguments(Landroid/os/Bundle;)V

    .line 237
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .prologue
    const v12, 0x7f05001a

    const v11, 0x7f050030

    const/4 v10, 0x1

    const/4 v9, 0x0

    const v8, 0x7f050025

    .line 243
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 244
    .local v2, "id":I
    sparse-switch v2, :sswitch_data_0

    .line 547
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 247
    :sswitch_0
    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 248
    .local v1, "factory":Landroid/view/LayoutInflater;
    const v6, 0x7f030007

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 250
    .local v4, "textEntryView":Landroid/view/View;
    const v6, 0x7f08001e

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 251
    .local v3, "text":Landroid/widget/EditText;
    new-instance v6, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$1;

    invoke-direct {v6, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$1;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 267
    sget-boolean v6, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v6, :cond_0

    .line 269
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f050029

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$3;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$3;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$2;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$2;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 301
    :cond_0
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f050029

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$5;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$5;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$4;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$4;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 333
    .local v0, "InputDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 334
    .local v5, "window":Landroid/view/Window;
    const/16 v6, 0x25

    invoke-virtual {v5, v6}, Landroid/view/Window;->setSoftInputMode(I)V

    goto/16 :goto_0

    .line 338
    .end local v0    # "InputDialog":Landroid/app/AlertDialog;
    .end local v1    # "factory":Landroid/view/LayoutInflater;
    .end local v3    # "text":Landroid/widget/EditText;
    .end local v4    # "textEntryView":Landroid/view/View;
    .end local v5    # "window":Landroid/view/Window;
    :sswitch_1
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f05002a

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050011

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$7;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$7;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$6;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$6;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 357
    :sswitch_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f05002b

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050012

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$9;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$9;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$8;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$8;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 376
    :sswitch_3
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050017

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$11;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$11;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$10;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$10;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 404
    :sswitch_4
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050014

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$13;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$13;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$12;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$12;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 427
    :sswitch_5
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050016

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$15;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$15;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$14;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$14;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 450
    :sswitch_6
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f050023

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050015

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050023

    new-instance v8, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$17;

    invoke-direct {v8, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$17;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$16;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$16;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 470
    :sswitch_7
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f050006

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f05001d

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$19;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$19;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$18;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$18;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 493
    :sswitch_8
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v12}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f05000c

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$22;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$22;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050040

    new-instance v8, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$21;

    invoke-direct {v8, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$21;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$20;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$20;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 520
    :sswitch_9
    new-instance v6, Landroid/app/AlertDialog$Builder;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v12}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f05000d

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$25;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$25;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f050040

    new-instance v8, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$24;

    invoke-direct {v8, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$24;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$23;

    invoke-direct {v7, p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$23;-><init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 244
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_7
        0x7 -> :sswitch_6
        0x8 -> :sswitch_5
        0x18 -> :sswitch_8
        0x19 -> :sswitch_9
    .end sparse-switch
.end method

.method protected onPrepareDialog(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 552
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v7, ""

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 554
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 555
    .local v3, "id":I
    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nBeforeDialogId:I
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$400()I

    move-result v6

    if-lez v6, :cond_0

    .line 557
    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nBeforeDialogId:I
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$400()I

    move-result v6

    invoke-static {v6}, Lcom/wsomacp/ui/XCPUIDialogActivity;->getDialogId(I)I

    move-result v4

    .line 560
    .local v4, "nId":I
    if-eq v4, v3, :cond_0

    .line 562
    :try_start_0
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v5

    .line 563
    .local v5, "prev":Landroid/app/Fragment;
    if-eqz v5, :cond_0

    .line 565
    move-object v0, v5

    check-cast v0, Landroid/app/DialogFragment;

    move-object v1, v0

    .line 566
    .local v1, "df":Landroid/app/DialogFragment;
    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    .end local v1    # "df":Landroid/app/DialogFragment;
    .end local v4    # "nId":I
    .end local v5    # "prev":Landroid/app/Fragment;
    :cond_0
    :goto_0
    return-void

    .line 570
    .restart local v4    # "nId":I
    :catch_0
    move-exception v2

    .line 572
    .local v2, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method
