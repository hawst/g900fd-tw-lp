.class public Lcom/wsomacp/ui/XCPUIDetailActivity;
.super Landroid/app/Activity;
.source "XCPUIDetailActivity.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static g_Context:Landroid/content/Context;

.field private static m_Activity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailActivity;->g_Context:Landroid/content/Context;

    .line 15
    sput-object v0, Lcom/wsomacp/ui/XCPUIDetailActivity;->m_Activity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static xcpUIDetailActivityRemove()V
    .locals 3

    .prologue
    .line 54
    :try_start_0
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailActivity;->m_Activity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lcom/wsomacp/ui/XCPUIDetailActivity;->m_Activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 57
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "Actvity finish"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 60
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 62
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    sput-object p0, Lcom/wsomacp/ui/XCPUIDetailActivity;->g_Context:Landroid/content/Context;

    .line 22
    sput-object p0, Lcom/wsomacp/ui/XCPUIDetailActivity;->m_Activity:Landroid/app/Activity;

    .line 24
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 26
    if-nez p1, :cond_1

    .line 28
    new-instance v0, Lcom/wsomacp/ui/XCPUIDetailFragment;

    invoke-direct {v0}, Lcom/wsomacp/ui/XCPUIDetailFragment;-><init>()V

    .line 29
    .local v0, "details":Lcom/wsomacp/ui/XCPUIDetailFragment;
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 30
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 31
    .local v1, "extras":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 33
    .local v4, "nIndex":I
    if-eqz v1, :cond_0

    .line 35
    const-string v5, "index"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 37
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v0, v0, v5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 38
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "nIndex : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 39
    sget-object v5, Lcom/wsomacp/ui/XCPUIDetailActivity;->g_Context:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpNewInstance(ILandroid/content/Context;)Lcom/wsomacp/ui/XCPUIDetailFragment;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/wsomacp/ui/XCPUIDetailActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 41
    .local v2, "ft":Landroid/app/FragmentTransaction;
    const v5, 0x1020002

    invoke-virtual {v2, v5, v0}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 42
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 48
    .end local v0    # "details":Lcom/wsomacp/ui/XCPUIDetailFragment;
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "nIndex":I
    :goto_0
    return-void

    .line 46
    :cond_1
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDetailActivity;->xcpUIDetailActivityRemove()V

    goto :goto_0
.end method
