.class public Lcom/wsomacp/ui/XCPUIItemAdapter;
.super Landroid/widget/ArrayAdapter;
.source "XCPUIItemAdapter.java"

# interfaces
.implements Lcom/wsomacp/db/XCPDBSql;
.implements Lcom/wsomacp/interfaces/XCPInterface;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/wsomacp/agent/XCPItem;",
        ">;",
        "Lcom/wsomacp/interfaces/XCPInterface;",
        "Lcom/wsomacp/db/XCPDBSql;"
    }
.end annotation


# static fields
.field public static g_nCPId:I

.field public static g_nSelectCheck:Z

.field static m_Context:Landroid/content/Context;

.field static m_CpItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/wsomacp/agent/XCPItem;",
            ">;"
        }
    .end annotation
.end field

.field static m_nClickedPos:I

.field static m_nSaveClickedPos:I


# instance fields
.field private m_nReferenceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    sput v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    .line 29
    sput v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nSaveClickedPos:I

    .line 31
    sput v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nCPId:I

    .line 32
    sput-boolean v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->g_nSelectCheck:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p4, "CallType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/wsomacp/agent/XCPItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p3, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/wsomacp/agent/XCPItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 38
    sput-object p3, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    .line 39
    sput-object p1, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    .line 40
    iput p4, p0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nReferenceType:I

    .line 41
    return-void
.end method


# virtual methods
.method public XCPUIGetItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/wsomacp/agent/XCPItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    sget-object v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 26
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 46
    move-object/from16 v21, p2

    .line 48
    .local v21, "view":Landroid/view/View;
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    if-nez v22, :cond_0

    .line 50
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v23, "m_Context is null"

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 51
    const/16 v22, 0x0

    .line 190
    :goto_0
    return-object v22

    .line 54
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nReferenceType:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_8

    .line 56
    if-nez v21, :cond_2

    .line 58
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const-string v23, "layout_inflater"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/LayoutInflater;

    .line 59
    .local v20, "vi":Landroid/view/LayoutInflater;
    if-nez v20, :cond_1

    .line 61
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v23, "vi is null"

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    move-object/from16 v22, v21

    .line 62
    goto :goto_0

    .line 65
    :cond_1
    const v22, 0x7f030005

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, p3

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    .line 68
    .end local v20    # "vi":Landroid/view/LayoutInflater;
    :cond_2
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/wsomacp/agent/XCPItem;

    .line 69
    .local v8, "cpItem":Lcom/wsomacp/agent/XCPItem;
    if-eqz v8, :cond_4

    .line 71
    const v22, 0x7f08001a

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 72
    .local v10, "cpTitleTextID":Landroid/widget/TextView;
    const v22, 0x7f08001b

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 73
    .local v7, "cpDateTextID":Landroid/widget/TextView;
    const v22, 0x7f08001c

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 75
    .local v9, "cpSubTextID":Landroid/widget/TextView;
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const v23, 0x7f050007

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 76
    .local v18, "titleText":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->xcpGetInstalled()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    .line 78
    const-string v22, " "

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 79
    const-string v22, "("

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 80
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const v23, 0x7f050013

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 81
    const-string v22, ")"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 85
    :cond_3
    sget-boolean v22, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    if-eqz v22, :cond_6

    .line 87
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v23, "getView ( Sim icon display setting start - Dual Mode) "

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 88
    const v22, 0x7f080019

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 90
    .local v13, "simTypeImage":Landroid/widget/ImageView;
    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->getSubId()J

    move-result-wide v16

    .line 92
    .local v16, "subId":J
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "getView ( cpItem.getSubId() ) =  "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 94
    if-eqz v13, :cond_5

    .line 96
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    :try_start_0
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "getView ( if :subId == "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 101
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    sget-object v23, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getSimIcon(Landroid/content/Context;J)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    .end local v13    # "simTypeImage":Landroid/widget/ImageView;
    .end local v16    # "subId":J
    :goto_1
    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->xcpGetDate()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lcom/wsomacp/agent/XCPItem;->xcpGetPreMessageInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    sget-boolean v22, Lcom/wsomacp/ui/XCPUIListFragment;->m_bDualPane:Z

    if-eqz v22, :cond_4

    .line 126
    sget v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_nClickedPos:I

    move/from16 v0, p1

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 128
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v14, v0, [[I

    const/16 v22, 0x0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const v25, 0x10100a7

    aput v25, v23, v24

    aput-object v23, v14, v22

    const/16 v22, 0x1

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const v25, -0x10100a7

    aput v25, v23, v24

    aput-object v23, v14, v22

    .line 132
    .local v14, "states":[[I
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v5, v0, [I

    const/16 v22, 0x0

    sget-object v23, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f040001

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    aput v23, v5, v22

    const/16 v22, 0x1

    sget-object v23, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f040004

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    aput v23, v5, v22

    .line 134
    .local v5, "color":[I
    new-instance v6, Landroid/content/res/ColorStateList;

    invoke-direct {v6, v14, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 136
    .local v6, "colors":Landroid/content/res/ColorStateList;
    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 137
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 138
    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .end local v5    # "color":[I
    .end local v6    # "colors":Landroid/content/res/ColorStateList;
    .end local v7    # "cpDateTextID":Landroid/widget/TextView;
    .end local v9    # "cpSubTextID":Landroid/widget/TextView;
    .end local v10    # "cpTitleTextID":Landroid/widget/TextView;
    .end local v14    # "states":[[I
    .end local v18    # "titleText":Ljava/lang/String;
    :cond_4
    :goto_2
    move-object/from16 v22, v21

    .line 190
    goto/16 :goto_0

    .line 103
    .restart local v7    # "cpDateTextID":Landroid/widget/TextView;
    .restart local v9    # "cpSubTextID":Landroid/widget/TextView;
    .restart local v10    # "cpTitleTextID":Landroid/widget/TextView;
    .restart local v13    # "simTypeImage":Landroid/widget/ImageView;
    .restart local v16    # "subId":J
    .restart local v18    # "titleText":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 105
    .local v12, "e":Ljava/lang/Exception;
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 110
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_5
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v23, "getView ( else :simTypeImage == null ) "

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 115
    .end local v13    # "simTypeImage":Landroid/widget/ImageView;
    .end local v16    # "subId":J
    :cond_6
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v23, "getView ( Sim icon display setting pass - single Mode) "

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 142
    :cond_7
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f040001

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 143
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f040002

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 144
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f040002

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 152
    .end local v7    # "cpDateTextID":Landroid/widget/TextView;
    .end local v8    # "cpItem":Lcom/wsomacp/agent/XCPItem;
    .end local v9    # "cpSubTextID":Landroid/widget/TextView;
    .end local v10    # "cpTitleTextID":Landroid/widget/TextView;
    .end local v18    # "titleText":Ljava/lang/String;
    :cond_8
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const-string v23, "layout_inflater"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/LayoutInflater;

    .line 153
    .restart local v20    # "vi":Landroid/view/LayoutInflater;
    if-nez v20, :cond_9

    .line 155
    sget-object v22, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v23, "vi is null"

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    move-object/from16 v22, v21

    .line 156
    goto/16 :goto_0

    .line 158
    :cond_9
    const v22, 0x7f030006

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, p3

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    .line 160
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_CpItems:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/wsomacp/agent/XCPItem;

    .line 161
    .restart local v8    # "cpItem":Lcom/wsomacp/agent/XCPItem;
    const v22, 0x7f08001a

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 162
    .local v4, "checkedTextView":Landroid/widget/TextView;
    const v22, 0x7f08001d

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Lcom/sec/android/fota/variant/view/CheckBox;

    .line 163
    .local v19, "titlecheckbox":Lcom/sec/android/fota/variant/view/CheckBox;
    const v22, 0x7f08001b

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 164
    .local v11, "dateTextView":Landroid/widget/TextView;
    const v22, 0x7f08001c

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 166
    .local v15, "subjectTextView":Landroid/widget/TextView;
    if-eqz v8, :cond_4

    .line 169
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const v23, 0x7f050007

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 170
    .restart local v18    # "titleText":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->xcpGetInstalled()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_a

    .line 172
    const-string v22, " "

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 173
    const-string v22, "("

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 174
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    const v23, 0x7f050013

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 175
    const-string v22, ")"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 178
    :cond_a
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->xcpGetDate()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    sget-object v22, Lcom/wsomacp/ui/XCPUIItemAdapter;->m_Context:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lcom/wsomacp/agent/XCPItem;->xcpGetPreMessageInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    invoke-virtual {v8}, Lcom/wsomacp/agent/XCPItem;->xcpIsmChecked()Z

    move-result v22

    if-eqz v22, :cond_b

    .line 185
    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    goto/16 :goto_2

    .line 187
    :cond_b
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/variant/view/CheckBox;->setChecked(Z)V

    goto/16 :goto_2
.end method
