.class Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$3;
.super Ljava/lang/Object;
.source "XCPUIDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;


# direct methods
.method constructor <init>(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$3;->this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v1, 0x4

    .line 273
    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_szResponseText:Ljava/lang/String;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_szResponseText:Ljava/lang/String;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_1

    .line 275
    sget-object v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_CPAdapter:Lcom/wsomacp/agent/XCPAgentAdapter;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_szResponseText:Ljava/lang/String;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$100()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPengSMLWapPush:Lcom/wsomacp/eng/core/XCPSMLWapPush;

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$000()Landroid/content/Context;

    move-result-object v3

    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->m_nSubid:J
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$200()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallVerifyPinCode(Ljava/lang/String;Lcom/wsomacp/eng/core/XCPSMLWapPush;Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wsomacp/ui/XCPUIDetailFragment;->g_bMultiMessage:Z

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$3;->this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 285
    :goto_0
    return-void

    .line 282
    :cond_1
    invoke-static {v1}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->newInstance(I)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    # setter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    invoke-static {v0}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$302(Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;)Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    .line 283
    # getter for: Lcom/wsomacp/ui/XCPUIDialogActivity;->dialogFragment:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;
    invoke-static {}, Lcom/wsomacp/ui/XCPUIDialogActivity;->access$300()Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog$3;->this$0:Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;

    invoke-virtual {v1}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XCP_DIALOG_PINCODE_AGAIN"

    invoke-virtual {v0, v1, v2}, Lcom/wsomacp/ui/XCPUIDialogActivity$MainDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
