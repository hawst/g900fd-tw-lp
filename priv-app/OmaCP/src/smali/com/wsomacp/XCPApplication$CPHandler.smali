.class Lcom/wsomacp/XCPApplication$CPHandler;
.super Landroid/os/Handler;
.source "XCPApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wsomacp/XCPApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CPHandler"
.end annotation


# instance fields
.field private final mApplication:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/wsomacp/XCPApplication;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/wsomacp/XCPApplication;)V
    .locals 1
    .param p1, "application"    # Lcom/wsomacp/XCPApplication;

    .prologue
    .line 298
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 299
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/wsomacp/XCPApplication$CPHandler;->mApplication:Ljava/lang/ref/WeakReference;

    .line 300
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 305
    iget-object v1, p0, Lcom/wsomacp/XCPApplication$CPHandler;->mApplication:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wsomacp/XCPApplication;

    .line 306
    .local v0, "application":Lcom/wsomacp/XCPApplication;
    # invokes: Lcom/wsomacp/XCPApplication;->handleMessage(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/wsomacp/XCPApplication;->access$000(Lcom/wsomacp/XCPApplication;Landroid/os/Message;)V

    .line 307
    return-void
.end method
