.class public Lcom/wsomacp/agent/XCPAppInstall;
.super Lcom/wsomacp/agent/XCPAgentAdapter;
.source "XCPAppInstall.java"

# interfaces
.implements Lcom/wsomacp/db/XCPDBSql;


# instance fields
.field final XCP_APP_ID_EMAIL_IMAP4:Ljava/lang/String;

.field final XCP_APP_ID_EMAIL_POP3:Ljava/lang/String;

.field final XCP_APP_ID_EMAIL_SMTP:Ljava/lang/String;

.field m_nNetRet:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;-><init>()V

    .line 21
    const-string v0, "110"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPAppInstall;->XCP_APP_ID_EMAIL_POP3:Ljava/lang/String;

    .line 22
    const-string v0, "143"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPAppInstall;->XCP_APP_ID_EMAIL_IMAP4:Ljava/lang/String;

    .line 23
    const-string v0, "25"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPAppInstall;->XCP_APP_ID_EMAIL_SMTP:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 29
    return-void
.end method


# virtual methods
.method public xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I
    .locals 17
    .param p1, "nAppID"    # I
    .param p2, "ptNetProfile"    # Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    .param p3, "mContext"    # Landroid/content/Context;
    .param p4, "Obj"    # Ljava/lang/Object;

    .prologue
    .line 33
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 34
    .local v5, "intent":Landroid/content/Intent;
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 35
    .local v6, "intentBookmark":Landroid/content/Intent;
    sparse-switch p1, :sswitch_data_0

    .line 301
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "not specified type for :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 302
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 303
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 321
    :goto_0
    return v12

    .line 39
    :sswitch_0
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_PROXY, WSSCP_APPID_NAP"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 40
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 41
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto :goto_0

    .line 44
    :sswitch_1
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_BROWSER"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move-object/from16 v7, p4

    .line 45
    check-cast v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;

    .line 47
    .local v7, "ptBrowserProfile":Lcom/wsomacp/eng/core/XCPBrowserInfo;
    const/4 v2, 0x0

    .local v2, "cou_intent":I
    :goto_1
    iget v12, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    if-ge v2, v12, :cond_0

    .line 49
    const-string v12, "android.intent.action.OMACP_BROWSER_SET_BOOKMARK"

    invoke-virtual {v6, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v12, "title"

    iget-object v13, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark_name:[Ljava/lang/String;

    aget-object v13, v13, v2

    invoke-virtual {v6, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v12, "url"

    iget-object v13, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark:[Ljava/lang/String;

    aget-object v13, v13, v2

    invoke-virtual {v6, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "title : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark_name:[Ljava/lang/String;

    aget-object v14, v14, v2

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 54
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "url : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark:[Ljava/lang/String;

    aget-object v14, v14, v2

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 58
    :try_start_0
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Broadcast Send - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 59
    const/16 v12, 0x20

    invoke-virtual {v6, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 60
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 61
    const/4 v12, 0x3

    invoke-static {v12}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpClearInstallStatus(I)V

    .line 62
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 63
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "nNetRet:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 65
    :catch_0
    move-exception v3

    .line 67
    .local v3, "ex":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2

    .line 70
    .end local v3    # "ex":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v6, 0x0

    .line 71
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "bookmark count number "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 74
    iget-object v12, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 76
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "HomeURL is NULL!!"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 77
    const/4 v12, 0x3

    invoke-static {v12}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpClearInstallStatus(I)V

    .line 78
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 79
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "nNetRet:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 80
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 84
    :cond_1
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "HomeURL Send!!!"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 85
    const-string v12, "android.intent.action.OMADM_BROWSER_SET_HOMEPAGE"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v12, "homepage"

    iget-object v13, v7, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    .end local v2    # "cou_intent":I
    .end local v7    # "ptBrowserProfile":Lcom/wsomacp/eng/core/XCPBrowserInfo;
    :cond_2
    :goto_3
    :try_start_1
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Broadcast Send - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 309
    const/16 v12, 0x20

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 310
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 311
    const/4 v5, 0x0

    .line 312
    const/4 v12, 0x3

    invoke-static {v12}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpClearInstallStatus(I)V

    .line 313
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 314
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "nNetRet:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 321
    :goto_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 91
    :sswitch_2
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_MMS"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 92
    if-eqz p2, :cond_3

    .line 94
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_szProfileName:Ljava/lang/String;

    const-string v13, ""

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v14, v14, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v14, v14, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyIP:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v15, v15, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v15, v15, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v12 .. v16}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetMMSProfileInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_3
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 97
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 100
    :sswitch_3
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_STREAM"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 102
    const/4 v12, 0x7

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 103
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 106
    :sswitch_4
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_EMAIL"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move-object/from16 v9, p4

    .line 107
    check-cast v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;

    .line 109
    .local v9, "ptEmailProfile":Lcom/wsomacp/eng/core/XCPEmailAccountInfo;
    const-string v12, "android.intent.action.SET_RECV_HOST"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v12, "110"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 113
    const-string v12, "service"

    const-string v13, "pop3"

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v12, "send_from"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string v12, "provider_id"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const-string v12, "receive_host"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const-string v12, "receive_port"

    iget v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 118
    const-string v12, "receive_security"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string v12, "user_id"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szUser_name:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    const-string v12, "user_passwd"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "POP3 intent sent. send_from = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 122
    iget-object v12, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    iget v12, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    if-nez v12, :cond_2

    .line 124
    :cond_4
    const/16 v12, 0x9

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 125
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 128
    :cond_5
    const-string v12, "143"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 130
    const-string v12, "service"

    const-string v13, "imap4"

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    const-string v12, "send_from"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    const-string v12, "provider_id"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    const-string v12, "receive_host"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v12, "receive_port"

    iget v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 135
    const-string v12, "receive_security"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string v12, "user_id"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szUser_name:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string v12, "user_passwd"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "IMAP4 intent sent. send_from = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 139
    iget-object v12, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_6

    iget v12, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    if-nez v12, :cond_2

    .line 141
    :cond_6
    const/16 v12, 0x9

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 142
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 145
    :cond_7
    const-string v12, "25"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 147
    const-string v12, "send_from"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v12, "provider_id"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const-string v12, "send_host"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_host:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    const-string v12, "send_port"

    iget v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSend_port:I

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 151
    const-string v12, "send_auth"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_auth:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v12, "send_security"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_security:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v12, "user_id"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_user:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v12, "user_passwd"

    iget-object v13, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_password:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SMTP intent sent. send_from = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 156
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "user_name:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_user:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " password:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_password:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 158
    iget-object v12, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_host:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_8

    iget v12, v9, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSend_port:I

    if-nez v12, :cond_2

    .line 160
    :cond_8
    const/16 v12, 0x9

    move-object/from16 v0, p0

    iput v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    .line 161
    move-object/from16 v0, p0

    iget v12, v0, Lcom/wsomacp/agent/XCPAppInstall;->m_nNetRet:I

    goto/16 :goto_0

    .line 167
    .end local v9    # "ptEmailProfile":Lcom/wsomacp/eng/core/XCPEmailAccountInfo;
    :sswitch_5
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_DS"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move-object/from16 v8, p4

    .line 168
    check-cast v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;

    .line 169
    .local v8, "ptDSProfile":Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;
    sget-boolean v12, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

    if-eqz v12, :cond_a

    .line 170
    const-string v12, "android.intent.action.SET_DS_MYPHONEBOOK"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    :goto_5
    if-eqz v8, :cond_2

    .line 176
    iget-object v12, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szCPSyncMLDSProfileName:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_9

    .line 178
    const-string v12, "PROFILENAME"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szCPSyncMLDSProfileName:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    :cond_9
    const-string v12, "ADDR"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string v12, "AAUTHLEVEL"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthLevel:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const-string v12, "AAUTHTYPE"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    const-string v12, "AAUTHNAME"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpUserID:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    const-string v12, "AAUTHSECRET"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpUserPS:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    iget-object v12, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    if-eqz v12, :cond_b

    .line 189
    const-string v12, "ProfileURI"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const-string v12, "PROFILEAACCEPT"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    :goto_6
    iget-object v12, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    if-eqz v12, :cond_c

    .line 200
    const-string v12, "ContactsURI"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const-string v12, "ContactsAACCEPT"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    :goto_7
    iget-object v12, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    if-eqz v12, :cond_d

    .line 211
    const-string v12, "CalendarURI"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string v12, "CalendarAACCEPT"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    :goto_8
    iget-object v12, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    if-eqz v12, :cond_e

    .line 222
    const-string v12, "NoteURI"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string v12, "NoteAACCEPT"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    :goto_9
    iget-object v12, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    if-eqz v12, :cond_f

    .line 233
    const-string v12, "TaskURI"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v12, "TaskAACCEPT"

    iget-object v13, v8, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 172
    :cond_a
    const-string v12, "android.intent.action.SET_DS"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 194
    :cond_b
    const-string v12, "ProfileURI"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v12, "PROFILEAACCEPT"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_6

    .line 205
    :cond_c
    const-string v12, "ContactsURI"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v12, "ContactsAACCEPT"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_7

    .line 216
    :cond_d
    const-string v12, "CalendarURI"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    const-string v12, "CalendarAACCEPT"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_8

    .line 227
    :cond_e
    const-string v12, "NoteURI"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const-string v12, "NoteAACCEPT"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_9

    .line 238
    :cond_f
    const-string v12, "TaskURI"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const-string v12, "TaskAACCEPT"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 245
    .end local v8    # "ptDSProfile":Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;
    :sswitch_6
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "WSSCP_APPID_DM"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move-object/from16 v10, p4

    .line 246
    check-cast v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;

    .line 248
    .local v10, "ptSyncMLDMProfile":Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;
    sget-boolean v12, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_USA_ATT:Z

    if-eqz v12, :cond_10

    .line 249
    const-string v12, "android.intent.action.SET_DM"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    :goto_a
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 254
    .local v4, "extras":Landroid/os/Bundle;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 255
    .local v1, "conValue":Landroid/content/ContentValues;
    const-string v12, "ProfileName"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProfileName:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v12, "ServerPort"

    iget v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerPort:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 257
    const-string v12, "ServerUrl"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v12, "ServerIP"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v12, "Path"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v12, "Protocol"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProtocol:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v12, "ServerAuthType"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerAuthType:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v12, "ServerId"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerID:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v12, "ServerPwd"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerPwd:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v12, "ServerNonce"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerNonce:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v12, "ClientAuthType"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v12, "UserId"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v12, "UserPw"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v12, "ClientNonce"

    iget-object v13, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szClientNonce:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v12, "DM_Settings"

    invoke-virtual {v4, v12, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 270
    invoke-virtual {v5, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 272
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "ptSyncMLDMProfile.AuthType : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v10, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 251
    .end local v1    # "conValue":Landroid/content/ContentValues;
    .end local v4    # "extras":Landroid/os/Bundle;
    :cond_10
    const-string v12, "android.intent.action.OMACP_DM_SET"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_a

    .line 276
    .end local v10    # "ptSyncMLDMProfile":Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;
    :sswitch_7
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "Here make XCAP Intent..."

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move-object/from16 v11, p4

    .line 277
    check-cast v11, Lcom/wsomacp/eng/core/XCPXcapInfo;

    .line 279
    .local v11, "ptXcapProfile":Lcom/wsomacp/eng/core/XCPXcapInfo;
    const-string v12, "com.sec.android.widgetapp.community.cpmp.CMT_CPMPService.mBroadcastReceiver"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const-string v12, "application/vnd.tmo.tmoclient"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    const-string v12, "CMD"

    const/16 v13, 0x20

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 283
    const-string v12, "name"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_NAME:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const-string v12, "addr"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_ADDR:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    const-string v12, "xcap_auid_dcs"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_AUID_DCS:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    const-string v12, "xcap_auid_pms"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_AUID_PMS:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v12, "xcap_auid_cdms"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_AUID_CDMS:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string v12, "xcap_imei_header"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_IMEI_HEADER:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    const-string v12, "xcap_tid_header"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_TID_HEADER:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    const-string v12, "sync_allowed"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_ALLOWED:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    const-string v12, "sync_default"

    iget-object v13, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_DEFAULT:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "xcap_header : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v11, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_IMEI_HEADER:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 297
    sget-object v12, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v13, "XCAP Intent send!!!!!!!!!!!!"

    invoke-virtual {v12, v13}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 316
    .end local v11    # "ptXcapProfile":Lcom/wsomacp/eng/core/XCPXcapInfo;
    :catch_1
    move-exception v3

    .line 318
    .restart local v3    # "ex":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_4

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0xe -> :sswitch_1
        0xf -> :sswitch_2
        0x20 -> :sswitch_3
        0x37 -> :sswitch_4
        0x435 -> :sswitch_5
        0x436 -> :sswitch_6
        0x438 -> :sswitch_7
    .end sparse-switch
.end method
