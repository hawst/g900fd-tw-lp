.class public Lcom/wsomacp/agent/XCPAgentAdapter;
.super Ljava/lang/Object;
.source "XCPAgentAdapter.java"

# interfaces
.implements Lcom/wsomacp/eng/core/XCPDecoder;
.implements Lcom/wsomacp/eng/core/XCPDef;
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static g_nCPMsgID:I

.field private static m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

.field private static m_nCPCtxType:I

.field static m_nCPInstallStatus:I


# instance fields
.field public m_CBearerValue:[Lcom/wsomacp/eng/core/XCPBearer;

.field public m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

.field private m_nCPPinCheckCount:I

.field private m_szCPInstallProfileRename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    sput v0, Lcom/wsomacp/agent/XCPAgentAdapter;->g_nCPMsgID:I

    .line 53
    sput v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    .line 57
    sput v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPCtxType:I

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_szCPInstallProfileRename:Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    .line 63
    invoke-static {}, Lcom/wsomacp/eng/core/XCPAdapter;->xcpSetDefaultBearerValue()[Lcom/wsomacp/eng/core/XCPBearer;

    move-result-object v0

    iput-object v0, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CBearerValue:[Lcom/wsomacp/eng/core/XCPBearer;

    .line 64
    iput v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    .line 65
    return-void
.end method

.method public static xcpClearInstallStatus(I)V
    .locals 3
    .param p0, "nStatus"    # I

    .prologue
    .line 112
    sget v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    xor-int/lit8 v1, p0, -0x1

    and-int/2addr v0, v1

    sput v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    .line 113
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "g_eCPInstallStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public static xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    return-object v0
.end method

.method private xcpInstallAccountBrowser(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 8
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 2080
    const/16 v0, 0x9

    .line 2082
    .local v0, "nRet":I
    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v5, :cond_1

    .line 2084
    :cond_0
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "m_CApplication is null"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 2127
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 2088
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 2089
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    new-instance v4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    invoke-direct {v4}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 2090
    .local v4, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    new-instance v3, Lcom/wsomacp/eng/core/XCPBrowserInfo;

    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPBrowserInfo;-><init>()V

    .line 2092
    .local v3, "ptBrowserProfile":Lcom/wsomacp/eng/core/XCPBrowserInfo;
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v5, v6, v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v4

    .line 2093
    if-eqz v4, :cond_2

    .line 2094
    iget-object v5, v4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v5, v5, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v5, v5, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    iput-object v5, v3, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    .line 2096
    :cond_2
    invoke-direct {p0, v2, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountBrowser(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPBrowserInfo;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2098
    const/4 v4, 0x0

    move v1, v0

    .line 2099
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2103
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_3
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v5, :cond_5

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v5, :cond_5

    .line 2105
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2107
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2108
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v6, "CSD"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xaa

    if-ne v5, v6, :cond_5

    .line 2110
    :cond_4
    const/4 v0, 0x7

    .line 2111
    const/4 v4, 0x0

    move v1, v0

    .line 2112
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2117
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_5
    iget-object v5, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v6, 0xe

    invoke-virtual {v5, v6, v4, p2, v3}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 2119
    const/4 v5, 0x3

    if-eq v0, v5, :cond_6

    .line 2121
    const/4 v4, 0x0

    move v1, v0

    .line 2122
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0

    .line 2125
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_6
    const/4 v0, 0x6

    .line 2126
    const/4 v4, 0x0

    move v1, v0

    .line 2127
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0
.end method

.method private xcpInstallAccountNAP(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 6
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x9

    .line 1071
    const/4 v1, 0x0

    .line 1072
    .local v1, "ptNapDef":Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    const/4 v2, 0x0

    .line 1074
    .local v2, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/16 v0, 0x9

    .line 1076
    .local v0, "nRet":I
    if-nez p1, :cond_1

    .line 1114
    :cond_0
    :goto_0
    return v3

    .line 1079
    :cond_1
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v4, :cond_0

    .line 1082
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 1083
    new-instance v2, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v2    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v2}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 1085
    .restart local v2    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1087
    :cond_2
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "m_szNapId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " m_szName:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " m_szNapAddr:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1088
    const/4 v2, 0x0

    move v3, v0

    .line 1089
    goto :goto_0

    .line 1092
    :cond_3
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-direct {p0, v3, v1, v2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountNAP(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v2

    .line 1093
    if-nez v2, :cond_4

    move v3, v0

    .line 1094
    goto :goto_0

    .line 1097
    :cond_4
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v3, :cond_6

    .line 1099
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1101
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1102
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v4, "CSD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0xaa

    if-ne v3, v4, :cond_6

    .line 1104
    :cond_5
    const/4 v0, 0x7

    .line 1105
    const/4 v2, 0x0

    move v3, v0

    .line 1106
    goto/16 :goto_0

    .line 1112
    :cond_6
    iget-object v3, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/4 v4, 0x1

    const-string v5, ""

    invoke-virtual {v3, v4, v2, p2, v5}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    move v3, v0

    .line 1114
    goto/16 :goto_0
.end method

.method private xcpInstallAccountXcap(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 7
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1983
    const/4 v2, 0x0

    .line 1984
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v3, 0x0

    .line 1985
    .local v3, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/4 v4, 0x0

    .line 1986
    .local v4, "ptXcapProfile":Lcom/wsomacp/eng/core/XCPXcapInfo;
    const/16 v0, 0x9

    .line 1988
    .local v0, "nRet":I
    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v5, :cond_1

    .line 1990
    :cond_0
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "ptCPContexts is null"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 2016
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 1994
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 1995
    new-instance v3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 1996
    .restart local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    new-instance v4, Lcom/wsomacp/eng/core/XCPXcapInfo;

    .end local v4    # "ptXcapProfile":Lcom/wsomacp/eng/core/XCPXcapInfo;
    invoke-direct {v4}, Lcom/wsomacp/eng/core/XCPXcapInfo;-><init>()V

    .line 1998
    .restart local v4    # "ptXcapProfile":Lcom/wsomacp/eng/core/XCPXcapInfo;
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v5, v6, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v3

    .line 2000
    invoke-virtual {p0, v2, v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountXcap(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPXcapInfo;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2002
    const/4 v3, 0x0

    .line 2003
    const/4 v4, 0x0

    move v1, v0

    .line 2004
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2007
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    iget-object v5, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v6, 0x438

    invoke-virtual {v5, v6, v3, p2, v4}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 2008
    const/4 v5, 0x3

    if-eq v0, v5, :cond_3

    .line 2010
    const/4 v3, 0x0

    move v1, v0

    .line 2011
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2014
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_3
    const/4 v0, 0x6

    .line 2015
    const/4 v3, 0x0

    move v1, v0

    .line 2016
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0
.end method

.method public static xcpInstallVerifyPinCode(Landroid/content/Context;Lcom/wsomacp/eng/core/XCPSMLWapPush;J)Z
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ptWapPush"    # Lcom/wsomacp/eng/core/XCPSMLWapPush;
    .param p2, "subId"    # J

    .prologue
    .line 812
    const-string v10, ""

    .line 813
    .local v10, "szSecKey":Ljava/lang/String;
    const-string v8, ""

    .line 814
    .local v8, "szDigest":Ljava/lang/String;
    const-string v9, ""

    .line 815
    .local v9, "szMAC":Ljava/lang/String;
    const/4 v7, 0x0

    .line 817
    .local v7, "nSecKeyLen":I
    if-nez p1, :cond_0

    .line 819
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v14, "ptWapPush null"

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 820
    const/4 v13, 0x0

    .line 876
    :goto_0
    return v13

    .line 823
    :cond_0
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v13, v13, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    const/16 v14, 0x80

    if-eq v13, v14, :cond_1

    .line 825
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v14, "not NETWPIN"

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 826
    const/4 v13, 0x1

    goto :goto_0

    .line 829
    :cond_1
    new-instance v9, Ljava/lang/String;

    .end local v9    # "szMAC":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget-object v13, v13, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_byMAC:[B

    invoke-direct {v9, v13}, Ljava/lang/String;-><init>([B)V

    .line 830
    .restart local v9    # "szMAC":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 832
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v14, "szMAC is empty"

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 833
    const/4 v13, 0x0

    goto :goto_0

    .line 836
    :cond_2
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2}, Lcom/wsomacp/agent/XCPSystem;->xcpSystemApiGetNetPinFromSubscriberId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v10

    .line 837
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 839
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v14, "szSecKey is empty"

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 840
    const/4 v13, 0x0

    goto :goto_0

    .line 843
    :cond_3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v7

    .line 844
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "szSecKey is: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 845
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Len: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 847
    new-array v5, v7, [B

    .line 848
    .local v5, "bkey":[B
    const/4 v6, 0x0

    .local v6, "n":I
    :goto_1
    if-ge v6, v7, :cond_4

    .line 850
    invoke-virtual {v10, v6}, Ljava/lang/String;->charAt(I)C

    move-result v13

    int-to-byte v13, v13

    and-int/lit16 v13, v13, 0xff

    int-to-byte v13, v13

    aput-byte v13, v5, v6

    .line 848
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 853
    :cond_4
    const/16 v13, 0x8

    array-length v14, v5

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v13, v5, v14, v15, v0}, Lcom/wsomacp/eng/core/XCPAuthMain;->xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;

    move-result-object v8

    .line 854
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 856
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    .line 857
    .local v11, "tt":[B
    invoke-static {v11}, Lcom/wsomacp/eng/core/XCPUtil;->xcpBytesToHexString([B)Ljava/lang/String;

    move-result-object v3

    .line 858
    .local v3, "aaa":Ljava/lang/String;
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Phone Make MAC Value : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 860
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    .line 861
    .local v12, "tt2":[B
    invoke-static {v12}, Lcom/wsomacp/eng/core/XCPUtil;->xcpBytesToHexString([B)Ljava/lang/String;

    move-result-object v4

    .line 862
    .local v4, "aaa2":Ljava/lang/String;
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "CP MAC Value : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 864
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-eqz v13, :cond_6

    .line 866
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v14, "MAC is different"

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 867
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 872
    .end local v3    # "aaa":Ljava/lang/String;
    .end local v4    # "aaa2":Ljava/lang/String;
    .end local v11    # "tt":[B
    .end local v12    # "tt2":[B
    :cond_5
    sget-object v13, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v14, "szDigest is empty"

    invoke-virtual {v13, v14}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 873
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 876
    .restart local v3    # "aaa":Ljava/lang/String;
    .restart local v4    # "aaa2":Ljava/lang/String;
    .restart local v11    # "tt":[B
    .restart local v12    # "tt2":[B
    :cond_6
    const/4 v13, 0x1

    goto/16 :goto_0
.end method

.method public static xcpInstallWifiSetting([Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p0, "szValue"    # [Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2533
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2534
    .local v0, "intentWIFI":Landroid/content/Intent;
    const-string v1, "android.intent.action.SET_WIFI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2536
    if-eqz p0, :cond_0

    array-length v1, p0

    const/16 v2, 0xc

    if-ge v1, v2, :cond_1

    .line 2538
    :cond_0
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "szValue is Empty"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 2556
    :goto_0
    return-void

    .line 2542
    :cond_1
    const-string v1, "NetworkSSID"

    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2543
    const-string v1, "KeyMgmt"

    const/4 v2, 0x1

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2544
    const-string v1, "EAPMethod"

    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2545
    const-string v1, "Phase2"

    const/4 v2, 0x3

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2546
    const-string v1, "Identity"

    const/4 v2, 0x4

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2547
    const-string v1, "AnonIdentity"

    const/4 v2, 0x5

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2548
    const-string v1, "Password"

    const/4 v2, 0x6

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2549
    const-string v1, "CACert"

    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2550
    const-string v1, "USERCert"

    const/16 v2, 0x8

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2551
    const-string v1, "Phase1"

    const/16 v2, 0x9

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2552
    const-string v1, "AuthAlgo"

    const/16 v2, 0xa

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2553
    const-string v1, "Priority"

    const/16 v2, 0xb

    aget-object v2, p0, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2554
    const-string v1, "WIFICount"

    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetWifiCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2555
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private xcpMakeAccountBrowser(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPBrowserInfo;)Z
    .locals 4
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptBrowserProfile"    # Lcom/wsomacp/eng/core/XCPBrowserInfo;

    .prologue
    .line 2021
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2023
    :cond_0
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "ptApplication or ptBrowserProfile is null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 2024
    const/4 v0, 0x0

    .line 2075
    :goto_0
    return v0

    .line 2027
    :cond_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v0, :cond_6

    .line 2034
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2041
    :goto_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v0, :cond_3

    .line 2043
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szStartPage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2045
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    .line 2046
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeURL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2048
    :cond_2
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2049
    iget-object v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark_name:[Ljava/lang/String;

    iget v1, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 2053
    :goto_2
    iget-object v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark:[Ljava/lang/String;

    iget v1, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 2054
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bookmark:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark:[Ljava/lang/String;

    iget v3, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2056
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-ne v0, v1, :cond_5

    .line 2075
    :cond_3
    :goto_3
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2051
    :cond_4
    iget-object v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szBookmark_name:[Ljava/lang/String;

    iget v1, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    aput-object v2, v0, v1

    goto :goto_2

    .line 2060
    :cond_5
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 2061
    iget v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_nCount_Bookmark:I

    goto/16 :goto_1

    .line 2066
    :cond_6
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v0, :cond_3

    .line 2068
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2070
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    .line 2071
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeURL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPBrowserInfo;->m_szHomeURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private xcpMakeAccountEmail(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z
    .locals 3
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptEmailProfile"    # Lcom/wsomacp/eng/core/XCPEmailAccountInfo;

    .prologue
    const/4 v0, 0x0

    .line 1177
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1200
    :cond_0
    :goto_0
    return v0

    .line 1181
    :cond_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "25"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 1183
    invoke-direct {p0, p1, p2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountEmailSTMP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1200
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1186
    :cond_3
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "143"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 1188
    invoke-direct {p0, p1, p2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountEmailIMAP4(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1191
    :cond_4
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v2, "110"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 1193
    invoke-direct {p0, p1, p2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountEmailPOP3(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method private xcpMakeAccountEmailIMAP4(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z
    .locals 4
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptEmailProfile"    # Lcom/wsomacp/eng/core/XCPEmailAccountInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1292
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    .line 1376
    :cond_1
    :goto_0
    return v0

    .line 1295
    :cond_2
    const/4 v2, 0x2

    iput v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceiving_server_type:I

    .line 1297
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nRetrieval_mode:I

    .line 1298
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nUse_security:I

    .line 1299
    iput v0, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nKeep_on_server:I

    .line 1300
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nFlag1:I

    .line 1302
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v1, :cond_7

    .line 1304
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1306
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceiving_server_addr:Ljava/lang/String;

    .line 1307
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receiving_server_addr:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceiving_server_addr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1310
    :cond_3
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1312
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nPort_num:I

    .line 1313
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "port_num:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nPort_num:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1316
    :cond_4
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1318
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    .line 1319
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive_host:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1322
    :cond_5
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1324
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    .line 1325
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive_port:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1328
    :cond_6
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v1, :cond_f

    .line 1330
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const-string v2, "STARTTLS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1331
    const-string v1, "tls"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    .line 1340
    :goto_1
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive_security:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1343
    :cond_7
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1344
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pAauthLevel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1346
    :cond_8
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1348
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szUser_name:Ljava/lang/String;

    .line 1349
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "user_name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szUser_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1352
    :cond_9
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1354
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szPassword:Ljava/lang/String;

    .line 1355
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "password:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1358
    :cond_a
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1360
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    .line 1361
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send_from:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1364
    :cond_b
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1366
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    .line 1367
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "provider_id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1370
    :cond_c
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1372
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    .line 1373
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "service:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1332
    :cond_d
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const/16 v2, 0x3e1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1333
    const-string v1, "ssl"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    goto/16 :goto_1

    .line 1335
    :cond_e
    const-string v1, "off"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    goto/16 :goto_1

    .line 1338
    :cond_f
    const-string v1, "off"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method private xcpMakeAccountEmailPOP3(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z
    .locals 4
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptEmailProfile"    # Lcom/wsomacp/eng/core/XCPEmailAccountInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1205
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    .line 1287
    :cond_1
    :goto_0
    return v0

    .line 1208
    :cond_2
    iput v0, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceiving_server_type:I

    .line 1210
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nRetrieval_mode:I

    .line 1212
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nUse_security:I

    .line 1213
    iput v0, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nKeep_on_server:I

    .line 1214
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nFlag1:I

    .line 1216
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v1, :cond_7

    .line 1218
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1220
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceiving_server_addr:Ljava/lang/String;

    .line 1221
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receiving_server_addr:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceiving_server_addr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1224
    :cond_3
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1225
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nPort_num:I

    .line 1227
    :cond_4
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1229
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    .line 1230
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive_host:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_host:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1233
    :cond_5
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1235
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    .line 1236
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive_port:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nReceive_port:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1239
    :cond_6
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v1, :cond_f

    .line 1241
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const-string v2, "STARTTLS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1242
    const-string v1, "tls"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    .line 1251
    :goto_1
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive_security:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1254
    :cond_7
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1255
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pAauthLevel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1257
    :cond_8
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1259
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szUser_name:Ljava/lang/String;

    .line 1260
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "user_name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szUser_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1263
    :cond_9
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1265
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szPassword:Ljava/lang/String;

    .line 1266
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "password:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1269
    :cond_a
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1271
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    .line 1272
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send_from:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1275
    :cond_b
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1277
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    .line 1278
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "provider_id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1281
    :cond_c
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1283
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    .line 1284
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "service:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1243
    :cond_d
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const/16 v2, 0x3e3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1244
    const-string v1, "ssl"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    goto/16 :goto_1

    .line 1246
    :cond_e
    const-string v1, "off"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    goto/16 :goto_1

    .line 1249
    :cond_f
    const-string v1, "off"

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szReceive_security:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method private xcpMakeAccountEmailSTMP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z
    .locals 5
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptEmailProfile"    # Lcom/wsomacp/eng/core/XCPEmailAccountInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1381
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v1

    .line 1468
    :goto_0
    return v0

    .line 1384
    :cond_1
    const/4 v2, 0x3

    iput v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSending_server_type:I

    .line 1385
    const/16 v2, 0x19

    iput v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSending_port_num:I

    .line 1386
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSending_security:I

    .line 1387
    iput v0, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nFlag2:I

    .line 1388
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nPop_before_smtp:I

    .line 1390
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1392
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szEmail_addr:Ljava/lang/String;

    .line 1393
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "email_addr:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szEmail_addr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1396
    :cond_2
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1398
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    .line 1399
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send_from:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_from:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1402
    :cond_3
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1404
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    .line 1405
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "provider_id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szProvider_id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1408
    :cond_4
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1410
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    .line 1411
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "service:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szService:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1414
    :cond_5
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1416
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_host:Ljava/lang/String;

    .line 1417
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send_host:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_host:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1420
    :cond_6
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1422
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSend_port:I

    .line 1423
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send_port:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSend_port:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1426
    :cond_7
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v2, :cond_a

    .line 1427
    const-string v2, "yes"

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_auth:Ljava/lang/String;

    .line 1431
    :goto_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v2, :cond_d

    .line 1433
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const-string v3, "STARTTLS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1434
    const-string v2, "tls"

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_security:Ljava/lang/String;

    .line 1443
    :goto_2
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send_security:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_security:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1445
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v2, :cond_e

    .line 1447
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "ptApplication.ptAppAuth"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1449
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1451
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_user:Ljava/lang/String;

    .line 1452
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sending_user:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_user:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1455
    :cond_8
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1457
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_password:Ljava/lang/String;

    .line 1458
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sending_password:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSending_password:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1460
    :cond_9
    iput v0, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSending_auth:I

    goto/16 :goto_0

    .line 1429
    :cond_a
    const-string v2, "no"

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_auth:Ljava/lang/String;

    goto/16 :goto_1

    .line 1435
    :cond_b
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    const/16 v3, 0x1d1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1436
    const-string v2, "ssl"

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_security:Ljava/lang/String;

    goto/16 :goto_2

    .line 1438
    :cond_c
    const-string v2, "off"

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_security:Ljava/lang/String;

    goto/16 :goto_2

    .line 1441
    :cond_d
    const-string v2, "off"

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_szSend_security:Ljava/lang/String;

    goto/16 :goto_2

    .line 1464
    :cond_e
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "ptApplication.ptAppAuth is null"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 1465
    iput v1, p2, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;->m_nSending_auth:I

    goto/16 :goto_0
.end method

.method private xcpMakeAccountNAP(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    .locals 5
    .param p1, "pszNAPID"    # Ljava/lang/String;
    .param p2, "ptNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p3, "ptNetProfile"    # Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2388
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    :cond_0
    move-object p3, v0

    .line 2481
    .end local p3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    :goto_0
    return-object p3

    .line 2398
    .restart local p3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    :cond_1
    iget-object p2, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 2391
    :cond_2
    if-eqz p2, :cond_3

    .line 2393
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2395
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 2401
    :cond_3
    if-nez p2, :cond_4

    move-object p3, v0

    .line 2402
    goto :goto_0

    .line 2404
    :cond_4
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2405
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptNapDef.pNapId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2407
    :cond_5
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetInstallStatus()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_10

    .line 2409
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetInstallProfileRename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_szProfileName:Ljava/lang/String;

    .line 2410
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rename ProfileName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_szProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2421
    :cond_6
    :goto_1
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2423
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szApn:Ljava/lang/String;

    .line 2424
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "APN:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szApn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2427
    :cond_7
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddrType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2428
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptNapDef.pNapAddrType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddrType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2430
    :cond_8
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinger:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2432
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinger:Ljava/lang/String;

    const-string v1, "Empty"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_9

    .line 2433
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szLinger:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_nLingerTime:I

    .line 2436
    :cond_9
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    if-eqz v0, :cond_d

    .line 2438
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2440
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptNapDef.ptNapAuthInfo.pAuthType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2442
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    const-string v1, "PAP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x9a

    if-ne v0, v1, :cond_11

    .line 2443
    :cond_a
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    iput v3, v0, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_nAuthType:I

    .line 2450
    :cond_b
    :goto_2
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2452
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthName:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_szUserName:Ljava/lang/String;

    .line 2453
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UserName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2456
    :cond_c
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthSecret:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2458
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthSecret:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_szPassword:Ljava/lang/String;

    .line 2459
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Password:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2463
    :cond_d
    iput v3, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_nProfileType:I

    .line 2464
    iput v3, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_nProfileAttr:I

    .line 2465
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iput v3, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_nProtocolType:I

    .line 2466
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iput v3, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_nAppProtoType:I

    .line 2468
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2469
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_e

    .line 2470
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    const-string v1, "http://"

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    .line 2472
    :cond_e
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2474
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    const-string v1, "0.0.0.0"

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyIP:Ljava/lang/String;

    .line 2475
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    const-string v1, "80"

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    .line 2476
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyIP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyAddr:Ljava/lang/String;

    .line 2479
    :cond_f
    sput-object p2, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxNapDefMulti:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    goto/16 :goto_0

    .line 2414
    :cond_10
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2416
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    iput-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_szProfileName:Ljava/lang/String;

    .line 2417
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ProfileName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_szProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2444
    :cond_11
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    const-string v1, "CHAP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNapAuthInfo:Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxNapAuthInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x9b

    if-ne v0, v1, :cond_13

    .line 2445
    :cond_12
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    const/4 v1, 0x2

    iput v1, v0, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_nAuthType:I

    goto/16 :goto_2

    .line 2447
    :cond_13
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_CAuthInfo:Lcom/wsomacp/eng/core/XCPPMauthInfo;

    iput v4, v0, Lcom/wsomacp/eng/core/XCPPMauthInfo;->m_nAuthType:I

    goto/16 :goto_2
.end method

.method private xcpMakeAccountStreaming(Lcom/wsomacp/eng/parser/XCPCtxApplication;)Z
    .locals 1
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .prologue
    .line 2208
    if-nez p1, :cond_0

    .line 2209
    const/4 v0, 0x0

    .line 2211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private xcpMakeAccountSyncMLDM(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;)Z
    .locals 6
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptSyncMLDMProfile"    # Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;

    .prologue
    const/4 v2, 0x0

    .line 1473
    const/4 v1, 0x0

    .line 1476
    .local v1, "ptAppAuth":Lcom/wsomacp/eng/parser/XCPCtxAppAuth;
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1582
    :cond_0
    :goto_0
    return v2

    .line 1479
    :cond_1
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1481
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    iput-object v3, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProfileName:Ljava/lang/String;

    .line 1482
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "szProfileName:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProfileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1485
    :cond_2
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v3, :cond_8

    .line 1487
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1489
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    iput-object v3, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    .line 1490
    iput v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerPort:I

    .line 1491
    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/wsomacp/eng/core/XCPUtil;->xcpURLParser(Ljava/lang/String;)Lcom/wsomacp/eng/core/XCPDBURLParserType;

    move-result-object v0

    .line 1493
    .local v0, "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    .line 1494
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szAddress:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP:Ljava/lang/String;

    .line 1495
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szPath:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath:Ljava/lang/String;

    .line 1496
    iget v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_nPort:I

    iput v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerPort:I

    .line 1497
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szProtocol:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProtocol:Ljava/lang/String;

    .line 1499
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szHttpServerUrl:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " szHttpServerHostName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " szHttpServerPath:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1520
    .end local v0    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    :cond_3
    :goto_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .line 1521
    :goto_2
    if-eqz v1, :cond_d

    .line 1523
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1525
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    const-string v3, "APPSRV"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_9

    .line 1527
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1529
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ptAppAuth.pAauthType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1530
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerAuthType:Ljava/lang/String;

    .line 1533
    :cond_4
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1535
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerID:Ljava/lang/String;

    .line 1536
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szServerID:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1539
    :cond_5
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1541
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerPwd:Ljava/lang/String;

    .line 1542
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szServerPS:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerPwd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1545
    :cond_6
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1547
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerNonce:Ljava/lang/String;

    .line 1548
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szServerNonce:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerNonce:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1579
    :cond_7
    :goto_3
    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    goto/16 :goto_2

    .line 1502
    :cond_8
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v3, :cond_3

    .line 1504
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1506
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v3, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    .line 1507
    iput v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerPort:I

    .line 1508
    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/wsomacp/eng/core/XCPUtil;->xcpURLParser(Ljava/lang/String;)Lcom/wsomacp/eng/core/XCPDBURLParserType;

    move-result-object v0

    .line 1510
    .restart local v0    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    .line 1511
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szAddress:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP:Ljava/lang/String;

    .line 1512
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szPath:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath:Ljava/lang/String;

    .line 1513
    iget v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_nPort:I

    iput v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_nServerPort:I

    .line 1514
    iget-object v2, v0, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szProtocol:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szProtocol:Ljava/lang/String;

    .line 1516
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szHttpServerUrl:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " szHttpServerHostName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szServerIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " szHttpServerPath:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1554
    .end local v0    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    :cond_9
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1556
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ptAppAuth.pAauthType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1557
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szAuthType:Ljava/lang/String;

    .line 1560
    :cond_a
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1562
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szUserName:Ljava/lang/String;

    .line 1563
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UserID:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1566
    :cond_b
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 1568
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPassword:Ljava/lang/String;

    .line 1569
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UserPS:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1572
    :cond_c
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1574
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    iput-object v2, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szClientNonce:Ljava/lang/String;

    .line 1575
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szClientNonce:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;->m_szClientNonce:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1582
    :cond_d
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private xcpMakeAccountSyncMLDS(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;)Z
    .locals 13
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptSyncMLDSProfile"    # Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1649
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v9, ""

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1651
    const/4 v3, 0x0

    .line 1654
    .local v3, "ptResource":Lcom/wsomacp/eng/parser/XCPCtxResource;
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v6, v7

    .line 1896
    :cond_1
    return v6

    .line 1657
    :cond_2
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1659
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    iput-object v8, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szCPSyncMLDSProfileName:Ljava/lang/String;

    .line 1660
    sget-object v8, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "g_wsscpSyncMLDSProfileName:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szCPSyncMLDSProfileName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1663
    :cond_3
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v8, :cond_c

    .line 1665
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1667
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    iput-object v8, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    .line 1668
    iput v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nHttpServerPort:I

    .line 1669
    iput v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nProtocol:I

    .line 1670
    const-string v5, ""

    .line 1671
    .local v5, "szProtocol":Ljava/lang/String;
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    invoke-static {v7}, Lcom/wsomacp/eng/core/XCPUtil;->xcpURLParser(Ljava/lang/String;)Lcom/wsomacp/eng/core/XCPDBURLParserType;

    move-result-object v2

    .line 1673
    .local v2, "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    iget-object v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    .line 1674
    iget-object v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szAddress:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerHostName:Ljava/lang/String;

    .line 1675
    iget-object v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szPath:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerPath:Ljava/lang/String;

    .line 1676
    iget v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_nPort:I

    iput v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nHttpServerPort:I

    .line 1677
    iget-object v5, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szProtocol:Ljava/lang/String;

    .line 1678
    const-string v7, "https"

    invoke-virtual {v5, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_b

    .line 1679
    iput v12, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nProtocol:I

    .line 1683
    :goto_0
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szHttpServerUrl:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " szHttpServerHostName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerHostName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " szHttpServerPath:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1711
    .end local v2    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    .end local v5    # "szProtocol":Ljava/lang/String;
    :cond_4
    :goto_1
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v7, :cond_9

    .line 1713
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1715
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthLevel:Ljava/lang/String;

    .line 1716
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "pAauthLevel:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthLevel:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1719
    :cond_5
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 1722
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpUserID:Ljava/lang/String;

    .line 1723
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "pAauthName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v9, v9, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1726
    :cond_6
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1729
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpUserPS:Ljava/lang/String;

    .line 1730
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "pAauthSecret:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v9, v9, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1733
    :cond_7
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 1735
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szNextNonce:Ljava/lang/String;

    .line 1736
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szNextNonce:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1739
    :cond_8
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 1741
    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthType:Ljava/lang/String;

    .line 1742
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szNextNonce:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szAuthType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1746
    :cond_9
    new-instance v3, Lcom/wsomacp/eng/parser/XCPCtxResource;

    .end local v3    # "ptResource":Lcom/wsomacp/eng/parser/XCPCtxResource;
    invoke-direct {v3}, Lcom/wsomacp/eng/parser/XCPCtxResource;-><init>()V

    .line 1747
    .restart local v3    # "ptResource":Lcom/wsomacp/eng/parser/XCPCtxResource;
    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 1749
    :goto_2
    if-eqz v3, :cond_1

    .line 1752
    sget-boolean v7, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

    if-eqz v7, :cond_15

    .line 1754
    const-string v0, "./Address"

    .line 1755
    .local v0, "AddressDB":Ljava/lang/String;
    const-string v1, "./Event"

    .line 1757
    .local v1, "EventDB":Ljava/lang/String;
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1759
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v8, "Accept is null"

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 1831
    :cond_a
    :goto_3
    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 1832
    goto :goto_2

    .line 1681
    .end local v0    # "AddressDB":Ljava/lang/String;
    .end local v1    # "EventDB":Ljava/lang/String;
    .restart local v2    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    .restart local v5    # "szProtocol":Ljava/lang/String;
    :cond_b
    iput v11, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nProtocol:I

    goto/16 :goto_0

    .line 1686
    .end local v2    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    .end local v5    # "szProtocol":Ljava/lang/String;
    :cond_c
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v8, :cond_4

    .line 1688
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1690
    iget-object v8, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    iget-object v8, v8, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    iput-object v8, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    .line 1691
    iput v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nHttpServerPort:I

    .line 1692
    iput v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nProtocol:I

    .line 1693
    const-string v5, ""

    .line 1694
    .restart local v5    # "szProtocol":Ljava/lang/String;
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    invoke-static {v7}, Lcom/wsomacp/eng/core/XCPUtil;->xcpURLParser(Ljava/lang/String;)Lcom/wsomacp/eng/core/XCPDBURLParserType;

    move-result-object v2

    .line 1696
    .restart local v2    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    iget-object v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szURL:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    .line 1697
    iget-object v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szAddress:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerHostName:Ljava/lang/String;

    .line 1698
    iget-object v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szPath:Ljava/lang/String;

    iput-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerPath:Ljava/lang/String;

    .line 1699
    iget v7, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_nPort:I

    iput v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nHttpServerPort:I

    .line 1700
    iget-object v5, v2, Lcom/wsomacp/eng/core/XCPDBURLParserType;->m_szProtocol:Ljava/lang/String;

    .line 1702
    const-string v7, "https"

    invoke-virtual {v5, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_d

    .line 1703
    iput v12, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nProtocol:I

    .line 1707
    :goto_4
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szHttpServerUrl:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " szHttpServerHostName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerHostName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " szHttpServerPath:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_szHttpServerPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1705
    :cond_d
    iput v11, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_nProtocol:I

    goto :goto_4

    .line 1763
    .end local v2    # "getParser":Lcom/wsomacp/eng/core/XCPDBURLParserType;
    .end local v5    # "szProtocol":Ljava/lang/String;
    .restart local v0    # "AddressDB":Ljava/lang/String;
    .restart local v1    # "EventDB":Ljava/lang/String;
    :cond_e
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    const-string v8, "text/x-vcard"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 1765
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_f

    .line 1767
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1768
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1769
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1770
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_3

    .line 1775
    :cond_f
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    const-string v8, "./Address"

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1776
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    const-string v8, "text/x-vcard"

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1777
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1778
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_3

    .line 1781
    :cond_10
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    const-string v8, "text/x-vcalendar"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1795
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_14

    .line 1797
    iget-object v4, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    .line 1798
    .local v4, "resourcename":Ljava/lang/String;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 1800
    const-string v7, "CALENDAR"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "CALENDAR DB"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "EVENT"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "KALENDER"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 1802
    :cond_11
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 1804
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1805
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1806
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1807
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_3

    .line 1810
    :cond_12
    const-string v7, "TASKS"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_13

    const-string v7, "TODO DB"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_13

    const-string v7, "TASK"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_13

    const-string v7, "AUFGABEN"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1812
    :cond_13
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 1814
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1815
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1816
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1817
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_3

    .line 1824
    .end local v4    # "resourcename":Ljava/lang/String;
    :cond_14
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    const-string v8, "./Event"

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1825
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    const-string v8, "text/x-vcalendar"

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1826
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1827
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_3

    .line 1835
    .end local v0    # "AddressDB":Ljava/lang/String;
    .end local v1    # "EventDB":Ljava/lang/String;
    :cond_15
    iget-object v4, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    .line 1836
    .restart local v4    # "resourcename":Ljava/lang/String;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 1838
    const-string v7, "PROFILE"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_16

    const-string v7, "PROFILES"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 1840
    :cond_16
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_17

    .line 1842
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1843
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1844
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1845
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    .line 1893
    :cond_17
    :goto_5
    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 1894
    goto/16 :goto_2

    .line 1848
    :cond_18
    const-string v7, "CONTACTS"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_19

    const-string v7, "ADDRESS"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_19

    const-string v7, "CONTACT DB"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_19

    const-string v7, "KONTAKTE"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 1850
    :cond_19
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_17

    .line 1852
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1853
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1854
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1855
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CPhoneBookInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto :goto_5

    .line 1858
    :cond_1a
    const-string v7, "CALENDAR"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1b

    const-string v7, "CALENDAR DB"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1b

    const-string v7, "EVENT"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1b

    const-string v7, "KALENDER"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1c

    .line 1860
    :cond_1b
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_17

    .line 1862
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1863
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1864
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1865
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CCalendarInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_5

    .line 1868
    :cond_1c
    const-string v7, "TASKS"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1d

    const-string v7, "TODO DB"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1d

    const-string v7, "TASK"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1d

    const-string v7, "AUFGABEN"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1e

    .line 1870
    :cond_1d
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_17

    .line 1872
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1873
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1874
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1875
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CTaskInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_5

    .line 1878
    :cond_1e
    const-string v7, "NOTES"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1f

    const-string v7, "NOTE"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1f

    const-string v7, "NOTIZEN"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_20

    .line 1880
    :cond_1f
    iget-object v7, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_17

    .line 1882
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    .line 1883
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v8, v3, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    iput-object v8, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    .line 1884
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DBName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szDBName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Accept:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iget-object v9, v9, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_szAAccept:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1885
    iget-object v7, p2, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;->m_CtNoteInfo:Lcom/wsomacp/eng/core/XCPSyncTypeInfo;

    iput-boolean v6, v7, Lcom/wsomacp/eng/core/XCPSyncTypeInfo;->m_bSync:Z

    goto/16 :goto_5

    .line 1890
    :cond_20
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v8, "There is No type matching with Resource"

    invoke-virtual {v7, v8}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 1891
    sget-object v7, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v7, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public static xcpSetContextType(I)V
    .locals 0
    .param p0, "nContextType"    # I

    .prologue
    .line 69
    sput p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPCtxType:I

    .line 70
    return-void
.end method

.method public static xcpSetCurrentContexts(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V
    .locals 1
    .param p0, "CPCtxWapDoc"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .prologue
    .line 79
    if-nez p0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 82
    :cond_0
    sget-object v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    if-eqz v0, :cond_1

    .line 83
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v0, :cond_2

    .line 86
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetContextType(I)V

    .line 96
    :goto_1
    sput-object p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    goto :goto_0

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v0, :cond_3

    .line 88
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetContextType(I)V

    goto :goto_1

    .line 89
    :cond_3
    iget-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v0, :cond_4

    .line 90
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetContextType(I)V

    goto :goto_1

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CVendrCfg:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    if-eqz v0, :cond_5

    .line 92
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetContextType(I)V

    goto :goto_1

    .line 94
    :cond_5
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetContextType(I)V

    goto :goto_1
.end method

.method public static xcpVerifyAuthentication(Landroid/content/Context;Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Lcom/wsomacp/eng/core/XCPSMLWapPush;IZIJ)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "ptWapPush"    # Lcom/wsomacp/eng/core/XCPSMLWapPush;
    .param p3, "rType"    # I
    .param p4, "bCheck"    # Z
    .param p5, "contentType"    # I
    .param p6, "subId"    # J

    .prologue
    .line 2561
    const/4 v0, 0x0

    .line 2562
    .local v0, "nRet":I
    const/4 v2, 0x0

    .line 2564
    .local v2, "nType":I
    if-nez p1, :cond_0

    const/4 v3, 0x1

    if-ne p5, v3, :cond_1

    :cond_0
    if-nez p2, :cond_2

    .line 2566
    :cond_1
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "ptCPContexts null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 2613
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 2570
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    invoke-static {p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetCurrentContexts(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    .line 2571
    iget-object v3, p2, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v3, v3, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    packed-switch v3, :pswitch_data_0

    .line 2591
    const/4 v2, 0x1

    .line 2594
    :goto_1
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nSEC: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v5, v5, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2596
    and-int v3, v2, p3

    if-eqz v3, :cond_4

    .line 2598
    const/4 v0, 0x1

    .line 2601
    if-eqz p4, :cond_3

    .line 2603
    invoke-static {p0, p2, p6, p7}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallVerifyPinCode(Landroid/content/Context;Lcom/wsomacp/eng/core/XCPSMLWapPush;J)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2604
    const/4 v0, 0x2

    .line 2612
    :cond_3
    :goto_2
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nRet: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    move v1, v0

    .line 2613
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2574
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :pswitch_0
    const/16 v2, 0x10

    .line 2575
    goto :goto_1

    .line 2578
    :pswitch_1
    const/16 v2, 0x100

    .line 2579
    goto :goto_1

    .line 2582
    :pswitch_2
    const/16 v2, 0x1000

    .line 2583
    goto :goto_1

    .line 2609
    :cond_4
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "Not Support Security Type"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 2610
    const/4 v0, 0x0

    goto :goto_2

    .line 2571
    nop

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private xcpVerifyPinCode(Landroid/content/Context;Ljava/lang/String;Lcom/wsomacp/eng/core/XCPSMLWapPush;J)Z
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pszPinCode"    # Ljava/lang/String;
    .param p3, "ptWapPush"    # Lcom/wsomacp/eng/core/XCPSMLWapPush;
    .param p4, "subId"    # J

    .prologue
    .line 2618
    const-string v14, ""

    .line 2619
    .local v14, "szSecKey":Ljava/lang/String;
    const/4 v10, 0x0

    .line 2620
    .local v10, "nSecKeyLen":I
    const-string v12, ""

    .line 2621
    .local v12, "szDigest":Ljava/lang/String;
    const-string v13, ""

    .line 2622
    .local v13, "szIMSI":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2623
    .local v8, "nIMSILen":I
    const/4 v9, 0x0

    .line 2625
    .local v9, "nPinCodeLen":I
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "m_nSEC:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 2627
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v15, v15, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    packed-switch v15, :pswitch_data_0

    .line 2755
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "Not Support Sec Type"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 2759
    :goto_0
    new-instance v11, Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget-object v15, v15, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_byMAC:[B

    invoke-direct {v11, v15}, Ljava/lang/String;-><init>([B)V

    .line 2761
    .local v11, "strMAC":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 2763
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "strMAC is Empty"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 2764
    const/4 v15, 0x0

    .line 2775
    .end local v11    # "strMAC":Ljava/lang/String;
    :goto_1
    return v15

    .line 2631
    :pswitch_0
    sget-boolean v15, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MAC_CHECK:Z

    if-nez v15, :cond_0

    .line 2632
    const/4 v15, 0x1

    goto :goto_1

    .line 2634
    :cond_0
    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/wsomacp/agent/XCPSystem;->xcpSystemApiGetNetPinFromSubscriberId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    .line 2635
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 2637
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "pszSecKey is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto :goto_0

    .line 2640
    :cond_1
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v10

    .line 2641
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "pszSecKey is : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2642
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Len "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2644
    new-array v3, v10, [B

    .line 2645
    .local v3, "bkey":[B
    const/4 v7, 0x0

    .local v7, "n":I
    :goto_2
    if-ge v7, v10, :cond_2

    .line 2647
    invoke-virtual {v14, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    int-to-byte v15, v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v3, v7

    .line 2645
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 2649
    :cond_2
    const/16 v15, 0x8

    array-length v0, v3

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    move/from16 v18, v0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v15, v3, v0, v1, v2}, Lcom/wsomacp/eng/core/XCPAuthMain;->xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;

    move-result-object v12

    .line 2650
    goto/16 :goto_0

    .line 2655
    .end local v3    # "bkey":[B
    .end local v7    # "n":I
    :pswitch_1
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2657
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "pszPinCode is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2661
    :cond_3
    move-object/from16 v14, p2

    .line 2662
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2664
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "pszSecKey is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2667
    :cond_4
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v10

    .line 2669
    new-array v3, v10, [B

    .line 2670
    .restart local v3    # "bkey":[B
    const/4 v7, 0x0

    .restart local v7    # "n":I
    :goto_3
    if-ge v7, v10, :cond_5

    .line 2672
    invoke-virtual {v14, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    int-to-byte v15, v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v3, v7

    .line 2670
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 2675
    :cond_5
    const/16 v15, 0x8

    array-length v0, v3

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    move/from16 v18, v0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v15, v3, v0, v1, v2}, Lcom/wsomacp/eng/core/XCPAuthMain;->xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;

    move-result-object v12

    .line 2676
    goto/16 :goto_0

    .line 2681
    .end local v3    # "bkey":[B
    .end local v7    # "n":I
    :pswitch_2
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 2683
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "pszPinCode is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2687
    :cond_6
    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/wsomacp/agent/XCPSystem;->xcpSystemApiGetNetPinFromSubscriberId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v13

    .line 2688
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2690
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "SubscriberId is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2694
    :cond_7
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v8

    .line 2695
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v9

    .line 2696
    add-int v10, v8, v9

    .line 2697
    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2698
    move-object v14, v13

    .line 2700
    new-array v3, v10, [B

    .line 2701
    .restart local v3    # "bkey":[B
    const/4 v7, 0x0

    .restart local v7    # "n":I
    :goto_4
    if-ge v7, v10, :cond_8

    .line 2703
    invoke-virtual {v14, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    int-to-byte v15, v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v3, v7

    .line 2701
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 2706
    :cond_8
    const/16 v15, 0x8

    array-length v0, v3

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    move/from16 v18, v0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v15, v3, v0, v1, v2}, Lcom/wsomacp/eng/core/XCPAuthMain;->xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;

    move-result-object v12

    .line 2707
    goto/16 :goto_0

    .line 2712
    .end local v3    # "bkey":[B
    .end local v7    # "n":I
    :pswitch_3
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 2714
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "pszPinCode is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2718
    :cond_9
    move-object/from16 v14, p2

    .line 2719
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 2721
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "pszSecKey is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2724
    :cond_a
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v10

    .line 2726
    new-array v3, v10, [B

    .line 2727
    .restart local v3    # "bkey":[B
    const/4 v7, 0x0

    .restart local v7    # "n":I
    :goto_5
    if-ge v7, v10, :cond_b

    .line 2729
    invoke-virtual {v14, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    int-to-byte v15, v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v3, v7

    .line 2727
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 2731
    :cond_b
    const/16 v15, 0x8

    array-length v0, v3

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    move/from16 v18, v0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v15, v3, v0, v1, v2}, Lcom/wsomacp/eng/core/XCPAuthMain;->xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;

    move-result-object v12

    .line 2733
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2734
    .local v6, "conPin":Ljava/lang/String;
    const/4 v15, 0x0

    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 2736
    .local v5, "cPin":Ljava/lang/String;
    move-object v14, v5

    .line 2737
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 2739
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "pszSecKey is null"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2742
    :cond_c
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v10

    .line 2744
    new-array v4, v10, [B

    .line 2745
    .local v4, "bkey1":[B
    const/4 v7, 0x0

    :goto_6
    if-ge v7, v10, :cond_d

    .line 2747
    invoke-virtual {v14, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    int-to-byte v15, v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v4, v7

    .line 2745
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 2750
    :cond_d
    const/16 v15, 0x8

    array-length v0, v4

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nBodyLen:I

    move/from16 v18, v0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v15, v4, v0, v1, v2}, Lcom/wsomacp/eng/core/XCPAuthMain;->xcpAuthMakeDigestSHA1(I[BI[BI)Ljava/lang/String;

    move-result-object v12

    .line 2751
    goto/16 :goto_0

    .line 2767
    .end local v3    # "bkey":[B
    .end local v4    # "bkey1":[B
    .end local v5    # "cPin":Ljava/lang/String;
    .end local v6    # "conPin":Ljava/lang/String;
    .end local v7    # "n":I
    .restart local v11    # "strMAC":Ljava/lang/String;
    :cond_e
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_f

    .line 2769
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "not equal!!"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 2770
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 2774
    :cond_f
    sget-object v15, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v16, "equal!!"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 2775
    const/4 v15, 0x1

    goto/16 :goto_1

    .line 2627
    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public XCPInstallNewAccount()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 628
    const/4 v2, 0x0

    .line 629
    .local v2, "szEncoded":Ljava/lang/String;
    const/4 v1, 0x0

    .line 630
    .local v1, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    const/4 v0, 0x0

    .line 632
    .local v0, "nCPContextType":I
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v1

    .line 633
    if-nez v1, :cond_0

    .line 692
    :goto_0
    return v3

    .line 636
    :cond_0
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetContextType()I

    move-result v0

    .line 637
    packed-switch v0, :pswitch_data_0

    .line 681
    :cond_1
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 683
    sget-object v3, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v4, 0x16

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 684
    const/4 v3, 0x1

    goto :goto_0

    .line 640
    :pswitch_0
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v4, :cond_1

    .line 642
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 644
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "app name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 646
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    goto :goto_1

    .line 650
    :cond_2
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v2, v4, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    goto :goto_1

    .line 657
    :pswitch_1
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v4, :cond_1

    .line 659
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 661
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " px name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 663
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v2, v4, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szName:Ljava/lang/String;

    goto :goto_1

    .line 669
    :pswitch_2
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v4, :cond_1

    .line 671
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 673
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nap name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 675
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v2, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szName:Ljava/lang/String;

    goto/16 :goto_1

    .line 688
    :cond_3
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "pEncoded is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 689
    sget-object v4, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 637
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public xcpCtxDelectContextAppAddr(Lcom/wsomacp/eng/parser/XCPCtxAppAddr;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    .prologue
    const/4 v0, 0x0

    .line 230
    if-nez p1, :cond_0

    .line 244
    :goto_0
    return-void

    .line 233
    :cond_0
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddr:Ljava/lang/String;

    .line 234
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_szAddrType:Ljava/lang/String;

    .line 236
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextPort(Lcom/wsomacp/eng/parser/XCPCtxPort;)V

    .line 239
    :cond_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v0, :cond_2

    .line 240
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextAppAddr(Lcom/wsomacp/eng/parser/XCPCtxAppAddr;)V

    .line 242
    :cond_2
    const/4 p1, 0x0

    .line 244
    goto :goto_0
.end method

.method public xcpCtxDelectContextAppAuth(Lcom/wsomacp/eng/parser/XCPCtxAppAuth;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .prologue
    const/4 v0, 0x0

    .line 282
    if-nez p1, :cond_0

    .line 296
    :goto_0
    return-void

    .line 285
    :cond_0
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthData:Ljava/lang/String;

    .line 286
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthLevel:Ljava/lang/String;

    .line 287
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthName:Ljava/lang/String;

    .line 288
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthSecret:Ljava/lang/String;

    .line 289
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_szAauthType:Ljava/lang/String;

    .line 291
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxAppAuth;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextAppAuth(Lcom/wsomacp/eng/parser/XCPCtxAppAuth;)V

    .line 294
    :cond_1
    const/4 p1, 0x0

    .line 296
    goto :goto_0
.end method

.method public xcpCtxDelectContextPort(Lcom/wsomacp/eng/parser/XCPCtxPort;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxPort;

    .prologue
    .line 249
    if-nez p1, :cond_0

    .line 262
    :goto_0
    return-void

    .line 252
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    .line 254
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CServiceList:Lcom/wsomacp/eng/parser/XCPCtxService;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextServiceList(Lcom/wsomacp/eng/parser/XCPCtxService;)V

    .line 257
    :cond_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPort;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextPort(Lcom/wsomacp/eng/parser/XCPCtxPort;)V

    .line 260
    :cond_2
    const/4 p1, 0x0

    .line 262
    goto :goto_0
.end method

.method public xcpCtxDelectContextResource(Lcom/wsomacp/eng/parser/XCPCtxResource;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxResource;

    .prologue
    const/4 v0, 0x0

    .line 301
    if-nez p1, :cond_0

    .line 319
    :goto_0
    return-void

    .line 304
    :cond_0
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAaccept:Ljava/lang/String;

    .line 305
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthData:Ljava/lang/String;

    .line 306
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthName:Ljava/lang/String;

    .line 307
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthSecret:Ljava/lang/String;

    .line 308
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szAauthType:Ljava/lang/String;

    .line 310
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szName:Ljava/lang/String;

    .line 311
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szStartPage:Ljava/lang/String;

    .line 312
    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    .line 314
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxResource;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextResource(Lcom/wsomacp/eng/parser/XCPCtxResource;)V

    .line 317
    :cond_1
    const/4 p1, 0x0

    .line 319
    goto :goto_0
.end method

.method public xcpCtxDelectContextServiceList(Lcom/wsomacp/eng/parser/XCPCtxService;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxService;

    .prologue
    .line 267
    if-nez p1, :cond_0

    .line 277
    :goto_0
    return-void

    .line 270
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_szService:Ljava/lang/String;

    .line 272
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxService;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxService;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextServiceList(Lcom/wsomacp/eng/parser/XCPCtxService;)V

    .line 275
    :cond_1
    const/4 p1, 0x0

    .line 277
    goto :goto_0
.end method

.method public xcpCtxDelectContextToAddrList(Lcom/wsomacp/eng/parser/XCPCtxToAddr;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    .prologue
    .line 185
    if-nez p1, :cond_0

    .line 195
    :goto_0
    return-void

    .line 188
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    .line 190
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextToAddrList(Lcom/wsomacp/eng/parser/XCPCtxToAddr;)V

    .line 193
    :cond_1
    const/4 p1, 0x0

    .line 195
    goto :goto_0
.end method

.method public xcpCtxDelectContextToNapIdList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .prologue
    .line 200
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    .line 205
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextToNapIdList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;)V

    .line 208
    :cond_1
    const/4 p1, 0x0

    .line 210
    goto :goto_0
.end method

.method public xcpCtxDelectContextToProxyList(Lcom/wsomacp/eng/parser/XCPCtxToProxy;)V
    .locals 1
    .param p1, "ptCtx"    # Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    .prologue
    .line 215
    if-nez p1, :cond_0

    .line 225
    :goto_0
    return-void

    .line 218
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    .line 220
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextToProxyList(Lcom/wsomacp/eng/parser/XCPCtxToProxy;)V

    .line 223
    :cond_1
    const/4 p1, 0x0

    .line 225
    goto :goto_0
.end method

.method public xcpDeleteContextApplication(Lcom/wsomacp/eng/parser/XCPCtxApplication;)V
    .locals 2
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .prologue
    const/4 v1, 0x0

    .line 137
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAaccept:Ljava/lang/String;

    .line 138
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    .line 139
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAprotocol:Ljava/lang/String;

    .line 140
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    .line 141
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szProviderId:Ljava/lang/String;

    .line 142
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szFrom:Ljava/lang/String;

    .line 144
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v0, :cond_0

    .line 146
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    .line 147
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextToAddrList(Lcom/wsomacp/eng/parser/XCPCtxToAddr;)V

    .line 150
    :cond_0
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v0, :cond_1

    .line 152
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    .line 153
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextToNapIdList(Lcom/wsomacp/eng/parser/XCPCtxToNapId;)V

    .line 156
    :cond_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v0, :cond_2

    .line 158
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    .line 159
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextToProxyList(Lcom/wsomacp/eng/parser/XCPCtxToProxy;)V

    .line 162
    :cond_2
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    if-eqz v0, :cond_3

    .line 164
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    .line 165
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAddr:Lcom/wsomacp/eng/parser/XCPCtxAppAddr;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextAppAddr(Lcom/wsomacp/eng/parser/XCPCtxAppAddr;)V

    .line 168
    :cond_3
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    if-eqz v0, :cond_4

    .line 170
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    .line 171
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CAppAuth:Lcom/wsomacp/eng/parser/XCPCtxAppAuth;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextAppAuth(Lcom/wsomacp/eng/parser/XCPCtxAppAuth;)V

    .line 174
    :cond_4
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v0, :cond_5

    .line 176
    iput-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    .line 177
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpCtxDelectContextResource(Lcom/wsomacp/eng/parser/XCPCtxResource;)V

    .line 180
    :cond_5
    const/4 p1, 0x0

    .line 181
    return-void
.end method

.method public xcpDeleteContextApplicationEmail()V
    .locals 5

    .prologue
    .line 324
    const/4 v0, 0x0

    .line 325
    .local v0, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    const/4 v1, 0x0

    .local v1, "ptCurApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v2, 0x0

    .line 327
    .local v2, "ptPrevApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v0

    .line 328
    if-nez v0, :cond_1

    .line 360
    :cond_0
    return-void

    .line 331
    :cond_1
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 332
    move-object v2, v1

    .line 334
    :goto_0
    if-eqz v1, :cond_0

    .line 336
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "25"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "143"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "110"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 338
    :cond_2
    if-ne v2, v1, :cond_3

    .line 340
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iput-object v3, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 342
    invoke-virtual {p0, v1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContextApplication(Lcom/wsomacp/eng/parser/XCPCtxApplication;)V

    .line 343
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 344
    move-object v2, v1

    goto :goto_0

    .line 348
    :cond_3
    iget-object v3, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iput-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 350
    invoke-virtual {p0, v1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContextApplication(Lcom/wsomacp/eng/parser/XCPCtxApplication;)V

    .line 351
    iget-object v1, v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    goto :goto_0

    .line 356
    :cond_4
    move-object v2, v1

    .line 357
    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    goto :goto_0
.end method

.method public xcpDeleteContextCurrentApplication()V
    .locals 3

    .prologue
    .line 364
    const/4 v1, 0x0

    .line 365
    .local v1, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    const/4 v0, 0x0

    .line 367
    .local v0, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v1

    .line 368
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v2, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v0, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 372
    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iput-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 374
    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContextApplication(Lcom/wsomacp/eng/parser/XCPCtxApplication;)V

    goto :goto_0
.end method

.method public xcpDeleteContextCurrentNapDef()V
    .locals 3

    .prologue
    .line 422
    const/4 v0, 0x0

    .line 423
    .local v0, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    const/4 v1, 0x0

    .line 425
    .local v1, "ptCurNapDef":Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-nez v2, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 430
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 432
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapId:Ljava/lang/String;

    .line 477
    const/4 v1, 0x0

    .line 478
    goto :goto_0
.end method

.method public xcpDeleteContextCurrentPxLogical()V
    .locals 3

    .prologue
    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    const/4 v1, 0x0

    .line 382
    .local v1, "ptCurPxLogical":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v0

    .line 383
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-nez v2, :cond_1

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v1, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 387
    iget-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iput-object v2, v0, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 389
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    .line 417
    const/4 v1, 0x0

    .line 418
    goto :goto_0
.end method

.method public xcpDeleteContexts()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 482
    const/4 v1, 0x0

    .line 484
    .local v1, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v1

    .line 485
    if-nez v1, :cond_0

    .line 505
    :goto_0
    return-void

    .line 488
    :cond_0
    const/4 v1, 0x0

    .line 489
    sput-object v2, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CCPCtxWapDoc:Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    .line 493
    const/4 v2, 0x0

    :try_start_0
    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->PX_h:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 494
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->g_CPXDef_h:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    .line 495
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxBootstrap;->g_CPXBoot_h:Lcom/wsomacp/eng/parser/XCPCtxBootstrap;

    .line 496
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;->g_CPXClientId:Lcom/wsomacp/eng/parser/XCPCtxClientIdentity;

    .line 497
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;->PXVendorConfig:Lcom/wsomacp/eng/parser/XCPCtxVendorConfig;

    .line 498
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxApplication;->g_CPXApp_h:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 499
    const/4 v2, 0x0

    sput-object v2, Lcom/wsomacp/eng/parser/XCPCtxAccess;->g_CPXAccess_h:Lcom/wsomacp/eng/parser/XCPCtxAccess;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 501
    :catch_0
    move-exception v0

    .line 503
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xcpDisplayDataGetAPN(Lcom/wsomacp/eng/parser/XCPCtxNapDef;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "ptNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p2, "pszNAPID"    # Ljava/lang/String;
    .param p3, "pszAPN"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 522
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 538
    :cond_0
    :goto_0
    return v0

    .line 525
    :cond_1
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pszNAPID:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 527
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 529
    iget-object p3, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    .line 530
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pszAPN:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 538
    const/4 v0, 0x1

    goto :goto_0

    .line 534
    :cond_2
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "ptNapDef.pNapAddr null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xcpDisplayDataGetBear(Lcom/wsomacp/eng/parser/XCPCtxNapDef;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "ptNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p2, "pszNAPID"    # Ljava/lang/String;
    .param p3, "pszBear"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 543
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 566
    :cond_0
    :goto_0
    return v0

    .line 546
    :cond_1
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pszNAPID:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 548
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v1, :cond_4

    .line 550
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 552
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/wsomacp/eng/core/XCPStr;->xcpStrIsAscii(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 553
    iget-object p3, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_szNapAddr:Ljava/lang/String;

    .line 557
    :goto_1
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pszBear:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 566
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 555
    :cond_3
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0, p3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetBearerValue(ILjava/lang/String;)V

    goto :goto_1

    .line 562
    :cond_4
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "ptNapDef.ptBearerList null"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xcpGetBearerValue(ILjava/lang/String;)V
    .locals 4
    .param p1, "nToken"    # I
    .param p2, "pValue"    # Ljava/lang/String;

    .prologue
    .line 509
    const/4 v0, 0x0

    .line 511
    .local v0, "nIdx":I
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x19

    if-ge v0, v1, :cond_0

    .line 513
    iget-object v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CBearerValue:[Lcom/wsomacp/eng/core/XCPBearer;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/wsomacp/eng/core/XCPBearer;->m_nToken:I

    if-ne v1, p1, :cond_1

    .line 516
    :cond_0
    iget-object v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CBearerValue:[Lcom/wsomacp/eng/core/XCPBearer;

    aget-object v1, v1, v0

    iget-object p2, v1, Lcom/wsomacp/eng/core/XCPBearer;->m_szValue:Ljava/lang/String;

    .line 517
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bearer: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 518
    return-void

    .line 511
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public xcpGetContextType()I
    .locals 1

    .prologue
    .line 74
    sget v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPCtxType:I

    return v0
.end method

.method public xcpGetInstallProfileRename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_szCPInstallProfileRename:Ljava/lang/String;

    return-object v0
.end method

.method public xcpGetInstallStatus()I
    .locals 3

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "g_eCPInstallStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 119
    sget v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    return v0
.end method

.method public xcpInstall(ILcom/wsomacp/eng/parser/XCPCtxWapDoc;Lcom/wsomacp/eng/core/XCPSMLWapPush;Landroid/content/Context;J)I
    .locals 9
    .param p1, "nID"    # I
    .param p2, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p3, "ptWapPush"    # Lcom/wsomacp/eng/core/XCPSMLWapPush;
    .param p4, "mContext"    # Landroid/content/Context;
    .param p5, "subid"    # J

    .prologue
    .line 697
    const/4 v7, 0x1

    .line 698
    .local v7, "nError":I
    sput p1, Lcom/wsomacp/agent/XCPAgentAdapter;->g_nCPMsgID:I

    .line 700
    if-nez p2, :cond_0

    .line 702
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "ptCPContexts null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v8, v7

    .line 762
    .end local v7    # "nError":I
    .local v8, "nError":I
    :goto_0
    return v8

    .line 706
    .end local v8    # "nError":I
    .restart local v7    # "nError":I
    :cond_0
    if-nez p3, :cond_1

    .line 708
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "ptWapPush null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v8, v7

    .line 709
    .end local v7    # "nError":I
    .restart local v8    # "nError":I
    goto :goto_0

    .line 712
    .end local v8    # "nError":I
    .restart local v7    # "nError":I
    :cond_1
    invoke-static {p2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetCurrentContexts(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;)V

    .line 713
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptWapPush Security Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v2, v2, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 715
    iget-object v0, p3, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    sparse-switch v0, :sswitch_data_0

    .line 741
    const-string v1, ""

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallVerifyPinCode(Ljava/lang/String;Lcom/wsomacp/eng/core/XCPSMLWapPush;Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 743
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "cpInstall fail"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 744
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContexts()V

    .line 745
    const/4 v7, 0x1

    :goto_1
    move v8, v7

    .line 762
    .end local v7    # "nError":I
    .restart local v8    # "nError":I
    goto :goto_0

    .line 718
    .end local v8    # "nError":I
    .restart local v7    # "nError":I
    :sswitch_0
    invoke-virtual {p0, p4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallProgress(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    invoke-static {p4, p5, p6}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpInstallConAccountSetting(Landroid/content/Context;J)V

    .line 721
    const/4 v7, 0x0

    goto :goto_1

    .line 725
    :cond_2
    const/4 v7, 0x1

    .line 727
    goto :goto_1

    .line 733
    :sswitch_1
    sget-object v0, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 734
    .local v6, "message":Landroid/os/Message;
    sget-object v0, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 749
    .end local v6    # "message":Landroid/os/Message;
    :cond_3
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->XCPInstallNewAccount()Z

    move-result v0

    if-nez v0, :cond_4

    .line 751
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "wsscpInstallNewAccount FALSE"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 752
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContexts()V

    .line 753
    const/4 v7, 0x1

    goto :goto_1

    .line 757
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 715
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x81 -> :sswitch_1
        0x82 -> :sswitch_1
        0x83 -> :sswitch_1
    .end sparse-switch
.end method

.method public xcpInstallAccountEmail(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 8
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1119
    const/4 v2, 0x0

    .line 1120
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v4, 0x0

    .line 1121
    .local v4, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/4 v3, 0x0

    .line 1122
    .local v3, "ptEmailProfile":Lcom/wsomacp/eng/core/XCPEmailAccountInfo;
    const/4 v0, 0x0

    .line 1124
    .local v0, "nRet":I
    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v5, :cond_1

    .line 1126
    :cond_0
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "ptCPContexts is null"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 1172
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 1130
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 1131
    new-instance v4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v4    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v4}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 1132
    .restart local v4    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    new-instance v3, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;

    .end local v3    # "ptEmailProfile":Lcom/wsomacp/eng/core/XCPEmailAccountInfo;
    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPEmailAccountInfo;-><init>()V

    .line 1134
    .restart local v3    # "ptEmailProfile":Lcom/wsomacp/eng/core/XCPEmailAccountInfo;
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v5, v6, v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v4

    .line 1136
    invoke-direct {p0, v2, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountEmail(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPEmailAccountInfo;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1138
    const/4 v4, 0x0

    .line 1139
    const/4 v3, 0x0

    move v1, v0

    .line 1140
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 1144
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v5, :cond_4

    .line 1146
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1148
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1149
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v6, "CSD"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xaa

    if-ne v5, v6, :cond_4

    .line 1151
    :cond_3
    const/4 v0, 0x7

    .line 1153
    const/4 v4, 0x0

    .line 1154
    const/4 v3, 0x0

    move v1, v0

    .line 1155
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 1161
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_4
    iget-object v5, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v6, 0x37

    invoke-virtual {v5, v6, v4, p2, v3}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 1162
    const/4 v5, 0x3

    if-eq v0, v5, :cond_5

    .line 1164
    const/4 v4, 0x0

    .line 1165
    const/4 v3, 0x0

    move v1, v0

    .line 1166
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0

    .line 1169
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_5
    const/4 v0, 0x6

    .line 1170
    const/4 v4, 0x0

    .line 1171
    const/4 v3, 0x0

    move v1, v0

    .line 1172
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0
.end method

.method public xcpInstallAccountMMS(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 7
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 2157
    const/4 v2, 0x0

    .line 2158
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v3, 0x0

    .line 2159
    .local v3, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/16 v0, 0x9

    .line 2161
    .local v0, "nRet":I
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v4, :cond_1

    .line 2163
    :cond_0
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "ptCPContexts is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 2203
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 2167
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 2168
    new-instance v3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 2169
    .restart local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v4, v5, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v3

    .line 2171
    invoke-virtual {p0, v2, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountMMS(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2173
    const/4 v3, 0x0

    move v1, v0

    .line 2174
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2178
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v4, :cond_4

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v4, :cond_4

    .line 2180
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2182
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2183
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v5, "CSD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0xaa

    if-ne v4, v5, :cond_4

    .line 2185
    :cond_3
    const/4 v0, 0x7

    .line 2186
    const/4 v3, 0x0

    move v1, v0

    .line 2187
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2193
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_4
    iget-object v4, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v5, 0xf

    const-string v6, ""

    invoke-virtual {v4, v5, v3, p2, v6}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 2195
    const/4 v4, 0x3

    if-eq v0, v4, :cond_5

    .line 2197
    const/4 v3, 0x0

    move v1, v0

    .line 2198
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2201
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_5
    const/4 v0, 0x6

    .line 2202
    const/4 v3, 0x0

    move v1, v0

    .line 2203
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0
.end method

.method public xcpInstallAccountProxy(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 8
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/16 v3, 0x9

    .line 2486
    const/4 v2, 0x0

    .line 2487
    .local v2, "ptPxLogical":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    const/4 v1, 0x0

    .line 2488
    .local v1, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/16 v0, 0x9

    .line 2490
    .local v0, "nRet":I
    if-nez p1, :cond_1

    .line 2528
    :cond_0
    :goto_0
    return v3

    .line 2493
    :cond_1
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v4, :cond_0

    .line 2496
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 2498
    new-instance v1, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v1    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v1}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 2499
    .restart local v1    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    iget-object v4, v2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2501
    const/4 v1, 0x0

    .line 2502
    goto :goto_0

    .line 2505
    :cond_2
    iget-object v4, v2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v4, v2, v5, v1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountProxy(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v1

    .line 2506
    if-eqz v1, :cond_0

    .line 2512
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v4, :cond_4

    .line 2514
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2516
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2517
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v5, "CSD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0xaa

    if-ne v4, v5, :cond_4

    .line 2519
    :cond_3
    const/4 v0, 0x7

    .line 2520
    const/4 v1, 0x0

    .line 2521
    goto :goto_0

    .line 2527
    :cond_4
    iget-object v3, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const-string v4, ""

    invoke-virtual {v3, v7, v1, p2, v4}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    move v3, v0

    .line 2528
    goto :goto_0
.end method

.method public xcpInstallAccountStreaming(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 7
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 2216
    const/4 v2, 0x0

    .line 2217
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v3, 0x0

    .line 2218
    .local v3, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/16 v0, 0x9

    .line 2220
    .local v0, "nRet":I
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v4, :cond_1

    .line 2222
    :cond_0
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "ptCPContexts is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 2261
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 2226
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 2227
    new-instance v3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 2228
    .restart local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v4, v5, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v3

    .line 2229
    invoke-direct {p0, v2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountStreaming(Lcom/wsomacp/eng/parser/XCPCtxApplication;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2231
    const/4 v3, 0x0

    move v1, v0

    .line 2232
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2236
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v4, :cond_4

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v4, :cond_4

    .line 2238
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2240
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v6, v6, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2241
    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v5, "CSD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v4, v4, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0xaa

    if-ne v4, v5, :cond_4

    .line 2243
    :cond_3
    const/4 v0, 0x7

    .line 2244
    const/4 v3, 0x0

    move v1, v0

    .line 2245
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2252
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_4
    iget-object v4, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v5, 0x20

    const-string v6, ""

    invoke-virtual {v4, v5, v3, p2, v6}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 2253
    const/4 v4, 0x3

    if-eq v0, v4, :cond_5

    .line 2255
    const/4 v3, 0x0

    move v1, v0

    .line 2256
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 2259
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_5
    const/4 v0, 0x6

    .line 2260
    const/4 v3, 0x0

    move v1, v0

    .line 2261
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0
.end method

.method public xcpInstallAccountSyncMLDM(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 8
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1587
    const/4 v2, 0x0

    .line 1588
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v3, 0x0

    .line 1589
    .local v3, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/4 v4, 0x0

    .line 1590
    .local v4, "ptSyncMLDMProfile":Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;
    const/16 v0, 0x9

    .line 1592
    .local v0, "nRet":I
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1594
    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v5, :cond_1

    .line 1596
    :cond_0
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "ptCPContexts is null"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 1644
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 1600
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 1601
    new-instance v3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 1602
    .restart local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    new-instance v4, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;

    .end local v4    # "ptSyncMLDMProfile":Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;
    invoke-direct {v4}, Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;-><init>()V

    .line 1604
    .restart local v4    # "ptSyncMLDMProfile":Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v5, v6, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v3

    .line 1606
    invoke-direct {p0, v2, v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountSyncMLDM(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPNVMSyncDMInfo;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1608
    const/4 v3, 0x0

    .line 1609
    const/4 v4, 0x0

    move v1, v0

    .line 1610
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 1614
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v5, :cond_4

    .line 1616
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1618
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1619
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v6, "CSD"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xaa

    if-ne v5, v6, :cond_4

    .line 1621
    :cond_3
    const/4 v0, 0x7

    .line 1622
    const/4 v3, 0x0

    .line 1623
    const/4 v4, 0x0

    move v1, v0

    .line 1624
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 1630
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_4
    if-eqz v3, :cond_5

    .line 1631
    const-string v5, "DM Profile"

    iput-object v5, v3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_szProfileName:Ljava/lang/String;

    .line 1633
    :cond_5
    iget-object v5, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v6, 0x436

    invoke-virtual {v5, v6, v3, p2, v4}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 1634
    const/4 v5, 0x3

    if-eq v0, v5, :cond_6

    .line 1636
    const/4 v3, 0x0

    .line 1637
    const/4 v4, 0x0

    move v1, v0

    .line 1638
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0

    .line 1641
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_6
    const/4 v0, 0x6

    .line 1642
    const/4 v3, 0x0

    .line 1643
    const/4 v4, 0x0

    move v1, v0

    .line 1644
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0
.end method

.method public xcpInstallAccountSyncMLDS(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I
    .locals 8
    .param p1, "ptCPContexts"    # Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1902
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 1903
    const/4 v2, 0x0

    .line 1904
    .local v2, "ptApplication":Lcom/wsomacp/eng/parser/XCPCtxApplication;
    const/4 v3, 0x0

    .line 1905
    .local v3, "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    const/4 v4, 0x0

    .line 1906
    .local v4, "ptSyncMLDSProfile":Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;
    const/16 v0, 0x9

    .line 1908
    .local v0, "nRet":I
    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-nez v5, :cond_1

    .line 1910
    :cond_0
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v6, "ptCPContexts is null"

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v1, v0

    .line 1957
    .end local v0    # "nRet":I
    .local v1, "nRet":I
    :goto_0
    return v1

    .line 1914
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_1
    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    .line 1915
    new-instance v3, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .end local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    invoke-direct {v3}, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;-><init>()V

    .line 1916
    .restart local v3    # "ptNetProfile":Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    new-instance v4, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;

    .end local v4    # "ptSyncMLDSProfile":Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;
    invoke-direct {v4}, Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;-><init>()V

    .line 1918
    .restart local v4    # "ptSyncMLDSProfile":Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    iget-object v6, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    invoke-virtual {p0, v2, v5, v6, v3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object v3

    .line 1920
    invoke-direct {p0, v2, v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountSyncMLDS(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPSMLDSGlobalSyncInfo;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1922
    const/4 v3, 0x0

    .line 1923
    const/4 v4, 0x0

    move v1, v0

    .line 1924
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 1928
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_2
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    if-eqz v5, :cond_4

    .line 1930
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1932
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ptNapDef.ptBearerList.pBearer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v7, v7, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1933
    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const-string v6, "CSD"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxNapDef;->m_CBearerList:Lcom/wsomacp/eng/parser/XCPCtxBearer;

    iget-object v5, v5, Lcom/wsomacp/eng/parser/XCPCtxBearer;->m_szBearer:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xaa

    if-ne v5, v6, :cond_4

    .line 1935
    :cond_3
    const/4 v0, 0x7

    .line 1936
    const/4 v3, 0x0

    .line 1937
    const/4 v4, 0x0

    move v1, v0

    .line 1938
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto :goto_0

    .line 1943
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_4
    iget-object v5, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_CInstaller:Lcom/wsomacp/agent/XCPAppInstall;

    const/16 v6, 0x435

    invoke-virtual {v5, v6, v3, p2, v4}, Lcom/wsomacp/agent/XCPAppInstall;->xcpInstallNetProfile(ILcom/wsomacp/eng/core/XCPNetPMProfileInfo;Landroid/content/Context;Ljava/lang/Object;)I

    move-result v0

    .line 1944
    const/4 v5, 0x3

    if-eq v0, v5, :cond_5

    .line 1946
    const/4 v3, 0x0

    .line 1947
    const/4 v4, 0x0

    move v1, v0

    .line 1948
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0

    .line 1951
    .end local v1    # "nRet":I
    .restart local v0    # "nRet":I
    :cond_5
    const/4 v5, 0x4

    invoke-static {v5}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpClearInstallStatus(I)V

    .line 1953
    const/4 v0, 0x6

    .line 1954
    const/4 v3, 0x0

    .line 1955
    const/4 v4, 0x0

    move v1, v0

    .line 1957
    .end local v0    # "nRet":I
    .restart local v1    # "nRet":I
    goto/16 :goto_0
.end method

.method public xcpInstallCheckNextAccount()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 942
    const/4 v0, 0x0

    .line 943
    .local v0, "nCPContextType":I
    const/4 v1, 0x0

    .line 945
    .local v1, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v1

    .line 946
    if-nez v1, :cond_1

    .line 968
    :cond_0
    :goto_0
    return v2

    .line 949
    :cond_1
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetContextType()I

    move-result v0

    .line 950
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 953
    :pswitch_0
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v4, :cond_0

    move v2, v3

    .line 954
    goto :goto_0

    .line 958
    :pswitch_1
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v4, :cond_0

    move v2, v3

    .line 959
    goto :goto_0

    .line 963
    :pswitch_2
    iget-object v4, v1, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v4, :cond_0

    move v2, v3

    .line 964
    goto :goto_0

    .line 950
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public xcpInstallCurrentAccount(Landroid/content/Context;)I
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 994
    const/4 v2, 0x0

    .line 995
    .local v2, "ptCPContexts":Lcom/wsomacp/eng/parser/XCPCtxWapDoc;
    const/4 v0, 0x0

    .line 996
    .local v0, "nCPContextType":I
    const/4 v1, 0x0

    .line 998
    .local v1, "nRet":I
    invoke-static {}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetCurrentContexts()Lcom/wsomacp/eng/parser/XCPCtxWapDoc;

    move-result-object v2

    .line 999
    if-nez v2, :cond_0

    .line 1001
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "ptCPContexts is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 1002
    const/16 v3, 0x9

    .line 1066
    :goto_0
    return v3

    .line 1005
    :cond_0
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetContextType()I

    move-result v0

    .line 1007
    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    move v3, v1

    .line 1066
    goto :goto_0

    .line 1010
    :pswitch_0
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    if-eqz v3, :cond_1

    .line 1012
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 1014
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "w2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 1016
    invoke-direct {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountBrowser(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 1018
    :cond_2
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "w4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 1020
    invoke-virtual {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountMMS(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 1022
    :cond_3
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "554"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 1024
    invoke-virtual {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountStreaming(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 1026
    :cond_4
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "w5"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    .line 1028
    invoke-virtual {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountSyncMLDS(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 1030
    :cond_5
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "w7"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    .line 1032
    invoke-virtual {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountSyncMLDM(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 1034
    :cond_6
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "ap9227"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_7

    .line 1036
    invoke-direct {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountXcap(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 1038
    :cond_7
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "25"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "143"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CApplication:Lcom/wsomacp/eng/parser/XCPCtxApplication;

    iget-object v3, v3, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szAppId:Ljava/lang/String;

    const-string v4, "110"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_9

    .line 1041
    :cond_8
    invoke-virtual {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountEmail(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto/16 :goto_1

    .line 1045
    :cond_9
    const/4 v1, 0x7

    goto/16 :goto_1

    .line 1050
    :cond_a
    const/16 v1, 0x9

    goto/16 :goto_1

    .line 1056
    :pswitch_1
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CPxLogical:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    if-eqz v3, :cond_1

    .line 1057
    invoke-virtual {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountProxy(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto/16 :goto_1

    .line 1061
    :pswitch_2
    iget-object v3, v2, Lcom/wsomacp/eng/parser/XCPCtxWapDoc;->m_CNapDef:Lcom/wsomacp/eng/parser/XCPCtxNapDef;

    if-eqz v3, :cond_1

    .line 1062
    invoke-direct {p0, v2, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallAccountNAP(Lcom/wsomacp/eng/parser/XCPCtxWapDoc;Landroid/content/Context;)I

    move-result v1

    goto/16 :goto_1

    .line 1007
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public xcpInstallCurrentDelete()V
    .locals 1

    .prologue
    .line 973
    const/4 v0, 0x0

    .line 975
    .local v0, "nCPContextType":I
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetContextType()I

    move-result v0

    .line 976
    packed-switch v0, :pswitch_data_0

    .line 990
    :goto_0
    return-void

    .line 979
    :pswitch_0
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContextCurrentApplication()V

    goto :goto_0

    .line 983
    :pswitch_1
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContextCurrentPxLogical()V

    goto :goto_0

    .line 987
    :pswitch_2
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContextCurrentNapDef()V

    goto :goto_0

    .line 976
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public xcpInstallProgress(Landroid/content/Context;)Z
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 913
    const/4 v1, 0x0

    .line 914
    .local v1, "nRet":I
    const/4 v0, 0x0

    .line 916
    .local v0, "bRet":Z
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 917
    invoke-virtual {p0, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentAccount(Landroid/content/Context;)I

    move-result v1

    .line 918
    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 920
    :cond_0
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentDelete()V

    .line 921
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCheckNextAccount()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 923
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->XCPInstallNewAccount()Z

    .line 930
    :goto_0
    const/4 v0, 0x1

    .line 937
    :goto_1
    return v0

    .line 927
    :cond_1
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContexts()V

    .line 928
    invoke-virtual {p0, v1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallResult(I)V

    goto :goto_0

    .line 934
    :cond_2
    invoke-virtual {p0, v1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallResult(I)V

    .line 935
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public xcpInstallRename(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5
    .param p1, "pszName"    # Ljava/lang/String;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 781
    const/4 v0, 0x0

    .line 783
    .local v0, "nRet":I
    invoke-virtual {p0, v4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetInstallStatus(I)V

    .line 784
    invoke-virtual {p0, p1}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpSetInstallProfileRename(Ljava/lang/String;)V

    .line 785
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pszName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetInstallProfileRename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 787
    invoke-virtual {p0, p2}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentAccount(Landroid/content/Context;)I

    move-result v0

    .line 789
    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 791
    :cond_0
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCurrentDelete()V

    .line 792
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallCheckNextAccount()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 794
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->XCPInstallNewAccount()Z

    .line 807
    :goto_0
    return v4

    .line 798
    :cond_1
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpDeleteContexts()V

    .line 799
    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallResult(I)V

    goto :goto_0

    .line 804
    :cond_2
    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallResult(I)V

    goto :goto_0
.end method

.method public xcpInstallResult(I)V
    .locals 6
    .param p1, "nRet"    # I

    .prologue
    const/16 v5, 0x14

    const/4 v4, 0x3

    .line 571
    const-string v0, ""

    .line 573
    .local v0, "szName":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 623
    :goto_0
    :pswitch_0
    return-void

    .line 576
    :pswitch_1
    invoke-virtual {p0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpGetInstallProfileRename()Ljava/lang/String;

    .line 577
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/wsomacp/eng/core/XCPAuthBase64;->encode([B)[B

    goto :goto_0

    .line 590
    :pswitch_2
    iput v4, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    .line 591
    sget-object v1, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 596
    :pswitch_3
    iput v4, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    .line 597
    sget-object v1, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 601
    :pswitch_4
    iget v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    .line 602
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Install Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 604
    iget v1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    if-nez v1, :cond_0

    .line 606
    sget-object v1, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 607
    iput v4, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    goto :goto_0

    .line 611
    :cond_0
    sget-object v1, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 616
    :pswitch_5
    sget-object v1, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 617
    iput v4, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPPinCheckCount:I

    goto :goto_0

    .line 573
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public xcpInstallVerifyPinCode(Ljava/lang/String;Lcom/wsomacp/eng/core/XCPSMLWapPush;Landroid/content/Context;J)Z
    .locals 8
    .param p1, "pszPinCode"    # Ljava/lang/String;
    .param p2, "ptWapPush"    # Lcom/wsomacp/eng/core/XCPSMLWapPush;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "subid"    # J

    .prologue
    const/4 v6, 0x0

    .line 881
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 883
    if-nez p2, :cond_0

    .line 885
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "ptWapPush null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move v0, v6

    .line 907
    :goto_0
    return v0

    :cond_0
    move-object v0, p0

    move-object v1, p3

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p4

    .line 889
    invoke-direct/range {v0 .. v5}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpVerifyPinCode(Landroid/content/Context;Ljava/lang/String;Lcom/wsomacp/eng/core/XCPSMLWapPush;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 891
    invoke-virtual {p0, p3}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallProgress(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 893
    invoke-static {p3, p4, p5}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpInstallConAccountSetting(Landroid/content/Context;J)V

    .line 894
    const/4 v0, 0x1

    goto :goto_0

    .line 899
    :cond_1
    iget-object v0, p2, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    iget v0, v0, Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;->m_nSEC:I

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    .line 900
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallResult(I)V

    :goto_1
    move v0, v6

    .line 904
    goto :goto_0

    .line 902
    :cond_2
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpInstallResult(I)V

    goto :goto_1

    :cond_3
    move v0, v6

    .line 907
    goto :goto_0
.end method

.method public xcpMakeAccountMMS(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Z
    .locals 3
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptNetProfile"    # Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .prologue
    .line 2132
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2133
    :cond_0
    const/4 v0, 0x0

    .line 2152
    :goto_0
    return v0

    .line 2135
    :cond_1
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    if-eqz v0, :cond_3

    .line 2137
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2139
    iget-object v0, p2, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CResource:Lcom/wsomacp/eng/parser/XCPCtxResource;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxResource;->m_szUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    .line 2140
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptNetProfile.ProfileInfo.Pdp.HomeURL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2152
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2143
    :cond_3
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    if-eqz v0, :cond_2

    .line 2145
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2147
    iget-object v0, p2, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    .line 2148
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptNetProfile.ProfileInfo.Pdp.HomeURL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xcpMakeAccountProxy(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    .locals 4
    .param p1, "pszProxyID"    # Ljava/lang/String;
    .param p2, "ptPxLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p3, "ptNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p4, "ptNetProfile"    # Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 2299
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_2

    .line 2383
    :cond_0
    :goto_0
    return-object v0

    .line 2309
    :cond_1
    iget-object p2, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CNext:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    .line 2302
    :cond_2
    if-eqz p2, :cond_3

    .line 2304
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2306
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 2312
    :cond_3
    if-eqz p2, :cond_0

    .line 2315
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2317
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    invoke-direct {p0, v1, p3, p4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountNAP(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object p4

    .line 2318
    if-eqz p4, :cond_0

    .line 2326
    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szStartPage:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2328
    iget-object v1, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v1, v1, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iput-object v0, v1, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    .line 2329
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szStartPage:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szHomeURL:Ljava/lang/String;

    .line 2332
    :cond_4
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    if-eqz v0, :cond_7

    .line 2334
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2336
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    const-string v1, "IPV4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x85

    if-ne v0, v1, :cond_8

    .line 2337
    :cond_5
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iput v3, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_nAddrType:I

    .line 2344
    :cond_6
    :goto_1
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2347
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    const-string v1, "0.0.0.0"

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyIP:Ljava/lang/String;

    .line 2356
    :goto_2
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v0, :cond_c

    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2359
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    .line 2372
    :goto_3
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyIP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyAddr:Ljava/lang/String;

    .line 2380
    :cond_7
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szProxyAddr:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v2, v2, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 2381
    sput-object p2, Lcom/wsomacp/eng/parser/XCPCtxParser;->g_CCPCtxPXLogicalMulti:Lcom/wsomacp/eng/parser/XCPCtxPxLogical;

    move-object v0, p4

    .line 2383
    goto/16 :goto_0

    .line 2338
    :cond_8
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    const-string v1, "IPV6"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddrType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x86

    if-ne v0, v1, :cond_a

    .line 2339
    :cond_9
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    const/4 v1, 0x2

    iput v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_nAddrType:I

    goto/16 :goto_1

    .line 2341
    :cond_a
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iput v3, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_nAddrType:I

    goto/16 :goto_1

    .line 2352
    :cond_b
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPxPhysical:Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPxPhysical;->m_szPxAddr:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyIP:Ljava/lang/String;

    goto/16 :goto_2

    .line 2361
    :cond_c
    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    if-eqz v0, :cond_d

    iget-object v0, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2363
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    iget-object v1, p2, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_CPort:Lcom/wsomacp/eng/parser/XCPCtxPort;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxPort;->m_szPortNbr:Ljava/lang/String;

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    goto/16 :goto_3

    .line 2368
    :cond_d
    iget-object v0, p4, Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;->m_CProfileInfo:Lcom/wsomacp/eng/core/XCPPMProfileInfo;

    iget-object v0, v0, Lcom/wsomacp/eng/core/XCPPMProfileInfo;->m_CPdp:Lcom/wsomacp/eng/core/XCPPMPdp;

    const-string v1, "80"

    iput-object v1, v0, Lcom/wsomacp/eng/core/XCPPMPdp;->m_szProxyPort:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method public xcpMakeAccountXcap(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/core/XCPXcapInfo;)Z
    .locals 3
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptXcapProfile"    # Lcom/wsomacp/eng/core/XCPXcapInfo;

    .prologue
    .line 1962
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1963
    :cond_0
    const/4 v0, 0x0

    .line 1978
    :goto_0
    return v0

    .line 1965
    :cond_1
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ptApplication.pName"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 1966
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szName:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_NAME:Ljava/lang/String;

    .line 1967
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToAddrList:Lcom/wsomacp/eng/parser/XCPCtxToAddr;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxToAddr;->m_szToAddr:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_ADDR:Ljava/lang/String;

    .line 1969
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_CDMS:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_AUID_CDMS:Ljava/lang/String;

    .line 1970
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_DCS:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_AUID_DCS:Ljava/lang/String;

    .line 1971
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_AUID_PMS:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_AUID_PMS:Ljava/lang/String;

    .line 1972
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_IMEI_HEADER:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_IMEI_HEADER:Ljava/lang/String;

    .line 1973
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_szXCAP_TID_HEADER:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_XCAP_TID_HEADER:Ljava/lang/String;

    .line 1975
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CRoaming:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_szSYNC_ALLOWED:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_ALLOWED:Ljava/lang/String;

    .line 1976
    iget-object v0, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CRoaming:Lcom/wsomacp/eng/parser/XCPCtxRoaming;

    iget-object v0, v0, Lcom/wsomacp/eng/parser/XCPCtxRoaming;->m_szSYNC_DEFAULT:Ljava/lang/String;

    iput-object v0, p2, Lcom/wsomacp/eng/core/XCPXcapInfo;->XCP_DEFAULT:Ljava/lang/String;

    .line 1978
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public xcpMakeApplicationNAP(Lcom/wsomacp/eng/parser/XCPCtxApplication;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;
    .locals 2
    .param p1, "ptApplication"    # Lcom/wsomacp/eng/parser/XCPCtxApplication;
    .param p2, "ptPxLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p3, "ptNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p4, "ptNetProfile"    # Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    .prologue
    const/4 v0, 0x0

    .line 2266
    if-eqz p1, :cond_0

    if-nez p4, :cond_1

    .line 2294
    :cond_0
    :goto_0
    return-object v0

    .line 2269
    :cond_1
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    if-eqz v1, :cond_3

    .line 2271
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2273
    if-eqz p2, :cond_0

    .line 2276
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToProxyList:Lcom/wsomacp/eng/parser/XCPCtxToProxy;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToProxy;->m_szToProxy:Ljava/lang/String;

    invoke-virtual {p0, v1, p2, p3, p4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountProxy(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object p4

    .line 2277
    if-eqz p4, :cond_0

    :cond_2
    move-object v0, p4

    .line 2294
    goto :goto_0

    .line 2281
    :cond_3
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    if-eqz v1, :cond_2

    .line 2283
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2285
    if-eqz p3, :cond_0

    .line 2288
    iget-object v1, p1, Lcom/wsomacp/eng/parser/XCPCtxApplication;->m_CToNapIdList:Lcom/wsomacp/eng/parser/XCPCtxToNapId;

    iget-object v1, v1, Lcom/wsomacp/eng/parser/XCPCtxToNapId;->m_szToNapId:Ljava/lang/String;

    invoke-direct {p0, v1, p3, p4}, Lcom/wsomacp/agent/XCPAgentAdapter;->xcpMakeAccountNAP(Ljava/lang/String;Lcom/wsomacp/eng/parser/XCPCtxNapDef;Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;)Lcom/wsomacp/eng/core/XCPNetPMProfileInfo;

    move-result-object p4

    .line 2289
    if-nez p4, :cond_2

    goto :goto_0
.end method

.method public xcpPushHdlrFreeWapPushMsg(Lcom/wsomacp/eng/core/XCPSMLWapPush;)V
    .locals 2
    .param p1, "ptMsg"    # Lcom/wsomacp/eng/core/XCPSMLWapPush;

    .prologue
    const/4 v0, 0x0

    .line 767
    if-nez p1, :cond_0

    .line 769
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "ptMsg is null"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 776
    :goto_0
    return-void

    .line 773
    :cond_0
    iput-object v0, p1, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_CWapPushInfo:Lcom/wsomacp/eng/core/XCPSMLWapPushInfo;

    .line 774
    iput-object v0, p1, Lcom/wsomacp/eng/core/XCPSMLWapPush;->m_byBody:[B

    .line 775
    const/4 p1, 0x0

    .line 776
    goto :goto_0
.end method

.method public xcpSetInstallProfileRename(Ljava/lang/String;)V
    .locals 1
    .param p1, "pszName"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iput-object p1, p0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_szCPInstallProfileRename:Ljava/lang/String;

    .line 128
    :cond_0
    return-void
.end method

.method public xcpSetInstallStatus(I)V
    .locals 3
    .param p1, "nStatus"    # I

    .prologue
    .line 106
    sget v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    or-int/2addr v0, p1

    sput v0, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    .line 107
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "g_eCPInstallStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wsomacp/agent/XCPAgentAdapter;->m_nCPInstallStatus:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 108
    return-void
.end method
