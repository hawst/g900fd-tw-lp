.class public Lcom/wsomacp/agent/XCPItem;
.super Ljava/lang/Object;
.source "XCPItem.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# instance fields
.field private XCP_APPID_IMAP4:Ljava/lang/String;

.field private XCP_APPID_NAP:Ljava/lang/String;

.field private XCP_APPID_POP3:Ljava/lang/String;

.field private XCP_APPID_SMTP:Ljava/lang/String;

.field private XCP_APPID_STREAMING:Ljava/lang/String;

.field private XCP_APPID_W2:Ljava/lang/String;

.field private XCP_APPID_W4:Ljava/lang/String;

.field private XCP_APPID_W5:Ljava/lang/String;

.field private XCP_APPID_W7:Ljava/lang/String;

.field private XCP_APPID_XCAP:Ljava/lang/String;

.field private m_bChecked:Z

.field private m_nCPID:I

.field private m_nInstalled:I

.field private m_nSubId:J

.field private m_nType:I

.field private m_szDate:Ljava/lang/String;

.field private m_szMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;IIJ)V
    .locals 2
    .param p1, "nCPId"    # I
    .param p2, "szDate"    # Ljava/lang/String;
    .param p3, "szMessage"    # Ljava/lang/String;
    .param p4, "nInstalled"    # I
    .param p5, "nType"    # I
    .param p6, "subid"    # J

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, "060377320001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W2:Ljava/lang/String;

    .line 12
    const-string v0, "060377340001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W4:Ljava/lang/String;

    .line 13
    const-string v0, "060377350001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W5:Ljava/lang/String;

    .line 14
    const-string v0, "060377370001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W7:Ljava/lang/String;

    .line 15
    const-string v0, "060332350001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_SMTP:Ljava/lang/String;

    .line 16
    const-string v0, "06033131300001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_POP3:Ljava/lang/String;

    .line 17
    const-string v0, "06033134330001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_IMAP4:Ljava/lang/String;

    .line 18
    const-string v0, "06033535340001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_STREAMING:Ljava/lang/String;

    .line 19
    const-string v0, "c65501"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_NAP:Ljava/lang/String;

    .line 20
    const-string v0, "06036170393232370001"

    iput-object v0, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_XCAP:Ljava/lang/String;

    .line 32
    iput p1, p0, Lcom/wsomacp/agent/XCPItem;->m_nCPID:I

    .line 33
    iput-object p2, p0, Lcom/wsomacp/agent/XCPItem;->m_szDate:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    .line 35
    iput p4, p0, Lcom/wsomacp/agent/XCPItem;->m_nInstalled:I

    .line 36
    iput p5, p0, Lcom/wsomacp/agent/XCPItem;->m_nType:I

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/wsomacp/agent/XCPItem;->m_bChecked:Z

    .line 38
    iput-wide p6, p0, Lcom/wsomacp/agent/XCPItem;->m_nSubId:J

    .line 39
    return-void
.end method


# virtual methods
.method public getSubId()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/wsomacp/agent/XCPItem;->m_nSubId:J

    return-wide v0
.end method

.method public xcpGetCpID()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/wsomacp/agent/XCPItem;->m_nCPID:I

    return v0
.end method

.method public xcpGetDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/wsomacp/agent/XCPItem;->m_szDate:Ljava/lang/String;

    return-object v0
.end method

.method public xcpGetInstalled()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/wsomacp/agent/XCPItem;->m_nInstalled:I

    return v0
.end method

.method public xcpGetMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    return-object v0
.end method

.method public xcpGetPreMessageInfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    const v1, 0x7f05002e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "szAppInfo":Ljava/lang/String;
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    const v1, 0x7f050005

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_XCAP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const v1, 0x7f05003f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W4:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    const v1, 0x7f05001e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W5:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 99
    const v1, 0x7f05003b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_3
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_W7:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 103
    const v1, 0x7f05003a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_4
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_SMTP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_POP3:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_IMAP4:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 107
    :cond_5
    const v1, 0x7f05000f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 109
    :cond_6
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_STREAMING:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 111
    const v1, 0x7f050039

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 113
    :cond_7
    iget-object v1, p0, Lcom/wsomacp/agent/XCPItem;->m_szMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/wsomacp/agent/XCPItem;->XCP_APPID_NAP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 115
    const-string v1, "APN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 119
    :cond_8
    iget v1, p0, Lcom/wsomacp/agent/XCPItem;->m_nType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 120
    const-string v1, "SyncML DM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 122
    :cond_9
    const-string v1, "Not Supported"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public xcpGetType()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/wsomacp/agent/XCPItem;->m_nType:I

    return v0
.end method

.method public xcpIsmChecked()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/wsomacp/agent/XCPItem;->m_bChecked:Z

    return v0
.end method

.method public xcpSetCpID(I)V
    .locals 0
    .param p1, "nCPId"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/wsomacp/agent/XCPItem;->m_nCPID:I

    .line 49
    return-void
.end method

.method public xcpSetmChecked(Z)V
    .locals 0
    .param p1, "bChecked"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/wsomacp/agent/XCPItem;->m_bChecked:Z

    .line 135
    return-void
.end method
