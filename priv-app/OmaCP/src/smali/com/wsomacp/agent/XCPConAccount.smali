.class public Lcom/wsomacp/agent/XCPConAccount;
.super Ljava/lang/Object;
.source "XCPConAccount.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field public static final XCP_NET_PDP_AUTH_CHAP:I = 0x2

.field public static final XCP_NET_PDP_AUTH_NONE:I = 0x0

.field public static final XCP_NET_PDP_AUTH_PAP:I = 0x1

.field private static g_BGInstallApp:I

.field private static m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

.field private static m_bNapSetting:Z

.field private static m_nApplicationCount:I

.field private static m_nVendorConfigCount:I

.field private static m_nWifiCount:I

.field private static m_szMMSproxyId:Ljava/lang/String;

.field private static m_szVendorConfigValue:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private m_szAppName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wsomacp/agent/XCPConAccount;->m_bNapSetting:Z

    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wsomacp/agent/XCPConAccount;->context:Landroid/content/Context;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    .line 45
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;-><init>()V

    sput-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .line 47
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pszName"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wsomacp/agent/XCPConAccount;->context:Landroid/content/Context;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    .line 51
    iput-object p1, p0, Lcom/wsomacp/agent/XCPConAccount;->context:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public static xcpGetApplicationCount()I
    .locals 1

    .prologue
    .line 153
    sget v0, Lcom/wsomacp/agent/XCPConAccount;->m_nApplicationCount:I

    return v0
.end method

.method public static xcpGetBGInstall()I
    .locals 1

    .prologue
    .line 198
    sget v0, Lcom/wsomacp/agent/XCPConAccount;->g_BGInstallApp:I

    return v0
.end method

.method public static xcpGetMMSProfileInfo()Lcom/wsomacp/eng/core/XCPMMSProfileInfo;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    return-object v0
.end method

.method public static xcpGetMMSproxyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_szMMSproxyId:Ljava/lang/String;

    return-object v0
.end method

.method private static xcpGetNapSetting()Z
    .locals 1

    .prologue
    .line 113
    sget-boolean v0, Lcom/wsomacp/agent/XCPConAccount;->m_bNapSetting:Z

    return v0
.end method

.method public static xcpGetVendorConfigCount()I
    .locals 1

    .prologue
    .line 173
    sget v0, Lcom/wsomacp/agent/XCPConAccount;->m_nVendorConfigCount:I

    return v0
.end method

.method public static xcpGetVendorConfigValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_szVendorConfigValue:Ljava/lang/String;

    return-object v0
.end method

.method public static xcpGetWifiCount()I
    .locals 1

    .prologue
    .line 203
    sget v0, Lcom/wsomacp/agent/XCPConAccount;->m_nWifiCount:I

    return v0
.end method

.method public static xcpSetApplicationCount(I)V
    .locals 0
    .param p0, "nCount"    # I

    .prologue
    .line 158
    sput p0, Lcom/wsomacp/agent/XCPConAccount;->m_nApplicationCount:I

    .line 159
    return-void
.end method

.method public static xcpSetBGInstall(I)V
    .locals 0
    .param p0, "bIsInstall"    # I

    .prologue
    .line 193
    sput p0, Lcom/wsomacp/agent/XCPConAccount;->g_BGInstallApp:I

    .line 194
    return-void
.end method

.method public static xcpSetMMSProfileInfo(Lcom/wsomacp/eng/core/XCPMMSProfileInfo;)V
    .locals 1
    .param p0, "mmsinfo"    # Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .prologue
    .line 140
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;-><init>()V

    sput-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .line 143
    :cond_0
    sput-object p0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .line 144
    return-void
.end method

.method public static xcpSetMMSProfileInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "szToProxy"    # Ljava/lang/String;
    .param p2, "szProxyAddr"    # Ljava/lang/String;
    .param p3, "szProxyPort"    # Ljava/lang/String;
    .param p4, "szAddr"    # Ljava/lang/String;

    .prologue
    .line 123
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    invoke-direct {v0}, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;-><init>()V

    sput-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    .line 126
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    iput-object p0, v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szName:Ljava/lang/String;

    .line 128
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 129
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    iput-object p1, v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szToProxy:Ljava/lang/String;

    .line 130
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 131
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    iput-object p2, v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyAddr:Ljava/lang/String;

    .line 132
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 133
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    iput-object p3, v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szProxyPort:Ljava/lang/String;

    .line 134
    :cond_4
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 135
    sget-object v0, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    iput-object p4, v0, Lcom/wsomacp/eng/core/XCPMMSProfileInfo;->m_szAddr:Ljava/lang/String;

    .line 136
    :cond_5
    return-void
.end method

.method public static xcpSetMMSproxyId(Ljava/lang/String;)V
    .locals 0
    .param p0, "ProxyId"    # Ljava/lang/String;

    .prologue
    .line 163
    sput-object p0, Lcom/wsomacp/agent/XCPConAccount;->m_szMMSproxyId:Ljava/lang/String;

    .line 164
    return-void
.end method

.method public static xcpSetNapSetting(Z)V
    .locals 0
    .param p0, "bSet"    # Z

    .prologue
    .line 118
    sput-boolean p0, Lcom/wsomacp/agent/XCPConAccount;->m_bNapSetting:Z

    .line 119
    return-void
.end method

.method public static xcpSetVendorConfigCount(I)V
    .locals 0
    .param p0, "nCount"    # I

    .prologue
    .line 178
    sput p0, Lcom/wsomacp/agent/XCPConAccount;->m_nVendorConfigCount:I

    .line 179
    return-void
.end method

.method public static xcpSetVendorConfigValue(Ljava/lang/String;)V
    .locals 0
    .param p0, "Value"    # Ljava/lang/String;

    .prologue
    .line 183
    sput-object p0, Lcom/wsomacp/agent/XCPConAccount;->m_szVendorConfigValue:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public static xcpSetWifiCount(I)V
    .locals 0
    .param p0, "nCount"    # I

    .prologue
    .line 208
    sput p0, Lcom/wsomacp/agent/XCPConAccount;->m_nWifiCount:I

    .line 209
    return-void
.end method


# virtual methods
.method public xcpSetConnecting(Lcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;J)V
    .locals 7
    .param p1, "pPXLogical"    # Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    .param p2, "pNapDef"    # Lcom/wsomacp/eng/parser/XCPCtxNapDef;
    .param p3, "subId"    # J

    .prologue
    const/4 v6, 0x1

    .line 67
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AppName is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->D(Ljava/lang/String;)V

    .line 69
    if-nez p2, :cond_1

    .line 71
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "No APN Data"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    move-object v2, p1

    .line 77
    .local v2, "tmPXLogical":Lcom/wsomacp/eng/parser/XCPCtxPxLogical;
    if-eqz p1, :cond_2

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 79
    const-string v3, "MMS"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p1, Lcom/wsomacp/eng/parser/XCPCtxPxLogical;->m_szProxyId:Ljava/lang/String;

    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetMMSproxyId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 81
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "MMS Proxydata. return..."

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 82
    const/4 v2, 0x0

    .line 87
    :cond_2
    const-string v3, "SyncML DM"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 89
    invoke-static {v6}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetNapSetting(Z)V

    .line 90
    new-instance v1, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;

    iget-object v3, p0, Lcom/wsomacp/agent/XCPConAccount;->context:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;-><init>(Landroid/content/Context;)V

    .line 91
    .local v1, "napUpdate":Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;
    invoke-virtual {v1, p3, p4, v2, p2}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->setConnection(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V

    .line 103
    .end local v1    # "napUpdate":Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;
    :cond_3
    :goto_1
    const-string v3, "MMS"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105
    new-instance v0, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;

    iget-object v3, p0, Lcom/wsomacp/agent/XCPConAccount;->context:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;-><init>(Landroid/content/Context;)V

    .line 106
    .local v0, "mmsUpdate":Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;
    sget-object v3, Lcom/wsomacp/agent/XCPConAccount;->m_CMMSProfile:Lcom/wsomacp/eng/core/XCPMMSProfileInfo;

    invoke-virtual {v0, v3}, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->setMMSProfile(Lcom/wsomacp/eng/core/XCPMMSProfileInfo;)V

    .line 107
    invoke-virtual {v0, p3, p4, v2, p2}, Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;->setConnection(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V

    goto :goto_0

    .line 93
    .end local v0    # "mmsUpdate":Lcom/sec/android/omacp/carrier/CarrierMMSCDBUpdate;
    :cond_4
    const-string v3, "Synchronise"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "Browser"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "Email"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "APN"

    iget-object v4, p0, Lcom/wsomacp/agent/XCPConAccount;->m_szAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 95
    :cond_5
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetNapSetting()Z

    move-result v3

    if-nez v3, :cond_3

    .line 97
    invoke-static {v6}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetNapSetting(Z)V

    .line 98
    new-instance v1, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;

    iget-object v3, p0, Lcom/wsomacp/agent/XCPConAccount;->context:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;-><init>(Landroid/content/Context;)V

    .line 99
    .restart local v1    # "napUpdate":Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;
    invoke-virtual {v1, p3, p4, v2, p2}, Lcom/sec/android/omacp/carrier/CarrierNAPDBUpdate;->setConnection(JLcom/wsomacp/eng/parser/XCPCtxPxLogical;Lcom/wsomacp/eng/parser/XCPCtxNapDef;)V

    goto :goto_1
.end method
