.class public Lcom/wsomacp/agent/XCPSystem;
.super Ljava/lang/Object;
.source "XCPSystem.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# static fields
.field private static final XCP_CSCFEATURE_DMBOOTSTRAPHANDLEDCP:Ljava/lang/String; = "CscFeature_Message_DMBootstrapHandledCp"

.field private static final XCP_CSCFEATURE_INSTALLDMINBGCP:Ljava/lang/String; = "CscFeature_Message_InstallDMInBGCp"

.field private static final XCP_CSC_SALES_CODE:Ljava/lang/String; = "ro.csc.sales_code"

.field private static final XCP_CSC_SALES_CODE2:Ljava/lang/String; = "ril.sales_code"

.field private static final XCP_DEVICE_TYPE_DIVISION:Ljava/lang/String; = "ro.build.characteristics"

.field public static final XCP_LANGUAGE_DE:Ljava/lang/String; = "de"

.field public static final XCP_LANGUAGE_EN_US:Ljava/lang/String; = "en-us"

.field private static final XCP_SECURITY_TYPE_NETWPIN:Ljava/lang/String; = "Netwpin"

.field private static final XCP_SECURITY_TYPE_NONE:Ljava/lang/String; = "None"

.field private static final XCP_SECURITY_TYPE_USERNETWPIN:Ljava/lang/String; = "Usernetwpin"

.field private static final XCP_SECURITY_TYPE_USERPIN:Ljava/lang/String; = "Userpin"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static xcpCharToHex(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 38
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 39
    add-int/lit8 v0, p0, -0x30

    .line 45
    :goto_0
    return v0

    .line 40
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 41
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 42
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 43
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 45
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static xcpGetBooleanFromFeature(Ljava/lang/String;)Z
    .locals 4
    .param p0, "featureName"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 145
    .local v0, "bfeatureValue":Z
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "feature:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 146
    return v0
.end method

.method public static xcpGetBuildSDKVersion()I
    .locals 4

    .prologue
    .line 137
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 138
    .local v0, "nSdkVersion":I
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "szVersion is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 139
    return v0
.end method

.method private static xcpGetStringFromFeature(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "featureName"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "szfeatureValue":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "feature:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 153
    return-object v0
.end method

.method public static xcpGetTargetSalesCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 158
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "szSalesCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    :cond_0
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SalesCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 163
    return-object v0
.end method

.method public static xcpSetATTOperator()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 253
    const-string v0, "ATT"

    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpGetTargetSalesCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_USA_ATT:Z

    .line 256
    const/16 v0, 0x10

    sput v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SECURITY:I

    .line 257
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MAC_CHECK:Z

    .line 258
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_NETWPIN_CHECK:Z

    .line 259
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MULTI_APN:Z

    .line 260
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

    .line 261
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP:Z

    .line 262
    sput-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_USA_ATT:Z

    goto :goto_0
.end method

.method private static xcpSetApnFeature()V
    .locals 1

    .prologue
    .line 209
    const-string v0, "CscFeature_SyncML_DisableMultipleApnCp"

    invoke-static {v0}, Lcom/wsomacp/agent/XCPSystem;->xcpGetBooleanFromFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MULTI_APN:Z

    .line 212
    :cond_0
    const-string v0, "CscFeature_SyncML_OverwriteApn4SameProfileName"

    invoke-static {v0}, Lcom/wsomacp/agent/XCPSystem;->xcpGetBooleanFromFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_OVERWRITE_BY_APN:Z

    .line 214
    :cond_1
    return-void
.end method

.method private static xcpSetDMBootstrapFeature()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 231
    const-string v0, "CscFeature_Message_DMBootstrapHandledCp"

    invoke-static {v0}, Lcom/wsomacp/agent/XCPSystem;->xcpGetBooleanFromFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    sput-boolean v1, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

    .line 234
    :cond_0
    const-string v0, "CscFeature_Message_InstallDMInBGCp"

    invoke-static {v0}, Lcom/wsomacp/agent/XCPSystem;->xcpGetBooleanFromFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    sput-boolean v1, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP:Z

    .line 236
    :cond_1
    return-void
.end method

.method public static xcpSetDeviceType()V
    .locals 3

    .prologue
    .line 240
    const-string v1, "ro.build.characteristics"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "szDeviceType":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 243
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v2, "empty szDeviceType"

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    const/4 v1, 0x1

    sput-boolean v1, Lcom/wsomacp/feature/XCPFeature;->XCP_DEVICETYPE_FEATURE_TABLET:Z

    goto :goto_0
.end method

.method private static xcpSetMessageControlFeature()V
    .locals 1

    .prologue
    .line 219
    const-string v0, "CscFeature_SyncML_DeleteCpAfterTryingToInstall"

    invoke-static {v0}, Lcom/wsomacp/agent/XCPSystem;->xcpGetBooleanFromFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    .line 221
    :cond_0
    return-void
.end method

.method private static xcpSetMyPhonebookFeature()V
    .locals 1

    .prologue
    .line 225
    const-string v0, "CscFeature_Common_MyPhonebookBrandName"

    invoke-static {v0}, Lcom/wsomacp/agent/XCPSystem;->xcpGetStringFromFeature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

    .line 227
    :cond_0
    return-void
.end method

.method private static xcpSetSecurityFeature()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 168
    const-string v2, ""

    .line 169
    .local v2, "sztmpSecuty":Ljava/lang/String;
    const/16 v1, 0x1111

    .line 170
    .local v1, "nSecurity":I
    const/4 v0, 0x0

    .line 173
    .local v0, "nMacCheck":I
    const-string v3, "CscFeature_SyncML_DiscardCpSecurityType4"

    invoke-static {v3}, Lcom/wsomacp/agent/XCPSystem;->xcpGetStringFromFeature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 176
    const-string v3, "None"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 177
    add-int/lit8 v1, v1, -0x1

    .line 178
    :cond_0
    const-string v3, "Netwpin"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 179
    add-int/lit8 v1, v1, -0x10

    .line 180
    :cond_1
    const-string v3, "Userpin"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 181
    add-int/lit16 v1, v1, -0x100

    .line 182
    :cond_2
    const-string v3, "Usernetwpin"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 183
    add-int/lit16 v1, v1, -0x1000

    .line 188
    :cond_3
    sput v1, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SECURITY:I

    .line 193
    const-string v3, "CscFeature_SyncML_EnableCpValidiationSecurityType4"

    invoke-static {v3}, Lcom/wsomacp/agent/XCPSystem;->xcpGetStringFromFeature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 194
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 196
    const-string v3, "Netwpin"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 197
    add-int/lit8 v0, v0, 0x10

    .line 200
    :cond_4
    if-eqz v0, :cond_5

    .line 201
    sput-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MAC_CHECK:Z

    .line 203
    :cond_5
    const-string v3, "CscFeature_SyncML_DiscardInvalidNetwpinCp"

    invoke-static {v3}, Lcom/wsomacp/agent/XCPSystem;->xcpGetBooleanFromFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 204
    sput-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_NETWPIN_CHECK:Z

    .line 205
    :cond_6
    return-void
.end method

.method public static xcpSetSupportSameAPN()V
    .locals 4

    .prologue
    .line 272
    const-string v0, ""

    .line 274
    .local v0, "tmpAPNActionFeature":Ljava/lang/String;
    const-string v1, "CscFeature_SyncML_ActionOmaCpApn"

    invoke-static {v1}, Lcom/wsomacp/agent/XCPSystem;->xcpGetStringFromFeature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 275
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TAG_CSCFEATURE_SYNCML_ACTIONOMACPAPN : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 277
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    const/4 v1, 0x1

    sput-boolean v1, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_SAME_APN:Z

    .line 281
    :cond_0
    return-void
.end method

.method public static xcpSystemApiGetLanguageSetting()Ljava/lang/String;
    .locals 3

    .prologue
    .line 97
    const/4 v1, 0x0

    .line 99
    .local v1, "szLang":Ljava/lang/String;
    invoke-static {}, Lcom/wsomacp/XCPApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 100
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const-string v1, "en-us"

    .line 103
    :cond_0
    return-object v1
.end method

.method public static xcpSystemApiGetNetPinFromSubscriberId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # J

    .prologue
    .line 50
    const-string v7, ""

    .line 51
    .local v7, "szIMSI":Ljava/lang/String;
    const-string v10, ""

    .line 52
    .local v10, "szPadIMSI":Ljava/lang/String;
    const/4 v3, 0x0

    .line 53
    .local v3, "nPadIMSILen":I
    const/4 v2, 0x0

    .line 55
    .local v2, "i":I
    const/4 v6, 0x0

    .line 56
    .local v6, "pNetPin":[C
    const-string v8, ""

    .line 58
    .local v8, "szNetPin":Ljava/lang/String;
    sget-object v11, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v11, p1, p2}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getSubscriberId(J)Ljava/lang/String;

    move-result-object v7

    .line 59
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 61
    sget-object v11, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v12, "SubscriberId is null"

    invoke-virtual {v11, v12}, Lcom/sec/android/fota/common/log/Logger$Impl;->W(Ljava/lang/String;)V

    move-object v9, v8

    .line 92
    .end local v8    # "szNetPin":Ljava/lang/String;
    .local v9, "szNetPin":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 65
    .end local v9    # "szNetPin":Ljava/lang/String;
    .restart local v8    # "szNetPin":Ljava/lang/String;
    :cond_0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    .line 66
    rem-int/lit8 v11, v3, 0x2

    if-eqz v11, :cond_1

    .line 68
    add-int/lit8 v3, v3, 0x1

    .line 69
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "9"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 77
    :goto_1
    div-int/lit8 v11, v3, 0x2

    new-array v6, v11, [C

    .line 78
    const/4 v2, 0x0

    :goto_2
    div-int/lit8 v11, v3, 0x2

    if-ge v2, v11, :cond_2

    .line 80
    mul-int/lit8 v11, v2, 0x2

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 81
    .local v4, "nTemp1":C
    mul-int/lit8 v11, v2, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 83
    .local v5, "nTemp2":C
    invoke-static {v4}, Lcom/wsomacp/agent/XCPSystem;->xcpCharToHex(I)I

    move-result v11

    shl-int/lit8 v11, v11, 0x4

    int-to-char v0, v11

    .line 84
    .local v0, "a":C
    invoke-static {v5}, Lcom/wsomacp/agent/XCPSystem;->xcpCharToHex(I)I

    move-result v11

    int-to-char v1, v11

    .line 85
    .local v1, "b":C
    or-int v11, v0, v1

    int-to-char v11, v11

    aput-char v11, v6, v2

    .line 87
    const/4 v4, 0x0

    .line 88
    const/4 v5, 0x0

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 73
    .end local v0    # "a":C
    .end local v1    # "b":C
    .end local v4    # "nTemp1":C
    .end local v5    # "nTemp2":C
    :cond_1
    add-int/lit8 v3, v3, 0x2

    .line 74
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "1"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "F"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    .line 91
    :cond_2
    invoke-static {v6}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    .line 92
    .end local v8    # "szNetPin":Ljava/lang/String;
    .restart local v9    # "szNetPin":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xcpSystemSetCPFeature()V
    .locals 3

    .prologue
    .line 108
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetSecurityFeature()V

    .line 109
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetApnFeature()V

    .line 110
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetMessageControlFeature()V

    .line 111
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetMyPhonebookFeature()V

    .line 112
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetDMBootstrapFeature()V

    .line 113
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetDeviceType()V

    .line 115
    sget-object v0, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->instance:Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;

    invoke-virtual {v0}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->isMultiSim()Z

    move-result v0

    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    .line 117
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetATTOperator()V

    .line 118
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSetSupportSameAPN()V

    .line 120
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_MAC_CHECK: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MAC_CHECK:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 121
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_SECURITY: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SECURITY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 122
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_NETWPIN_CHECK: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_NETWPIN_CHECK:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 123
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_MULTI_APN: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MULTI_APN:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 124
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_OVERWRITE_BY_APN: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_OVERWRITE_BY_APN:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 125
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_DELETE_CP_MESSAGE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_SUPPORT_MYPHONEBOOK: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 128
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 129
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_DEVICETYPE_FEATURE_TABLET : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_DEVICETYPE_FEATURE_TABLET:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 130
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_SUPPORT_DUAL_SIM : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_SUPPORT_USA_ATT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_USA_ATT:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 132
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCP_SUPPORT_SAME_APN : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_SAME_APN:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 133
    return-void
.end method
