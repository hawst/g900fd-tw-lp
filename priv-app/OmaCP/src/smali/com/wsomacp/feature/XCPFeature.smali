.class public Lcom/wsomacp/feature/XCPFeature;
.super Ljava/lang/Object;
.source "XCPFeature.java"


# static fields
.field public static XCP_DEVICETYPE_FEATURE_TABLET:Z

.field public static XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP:Z

.field public static XCP_FEATURE_DELETE_CP_MESSAGE:Z

.field public static XCP_FEATURE_DM_BOOTSTRAP_V_1_2:Z

.field public static XCP_FEATURE_MAC_CHECK:Z

.field public static XCP_FEATURE_MULTI_APN:Z

.field public static XCP_FEATURE_NETWPIN_CHECK:Z

.field public static XCP_FEATURE_OVERWRITE_BY_APN:Z

.field public static XCP_FEATURE_SECURITY:I

.field public static XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

.field public static XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

.field public static XCP_SUPPORT_DUAL_SIM:Z

.field public static XCP_SUPPORT_SAME_APN:Z

.field public static XCP_SUPPORT_USA_ATT:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5
    sput v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SECURITY:I

    .line 6
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MAC_CHECK:Z

    .line 8
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_NETWPIN_CHECK:Z

    .line 9
    sput-boolean v1, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_MULTI_APN:Z

    .line 10
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_OVERWRITE_BY_APN:Z

    .line 11
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DELETE_CP_MESSAGE:Z

    .line 12
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_MYPHONEBOOK:Z

    .line 14
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

    .line 16
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_BG_INTSATLL_DM_BOOTSTRAP:Z

    .line 17
    sput-boolean v1, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_DM_BOOTSTRAP_V_1_2:Z

    .line 20
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_DEVICETYPE_FEATURE_TABLET:Z

    .line 23
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_DUAL_SIM:Z

    .line 26
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_USA_ATT:Z

    .line 28
    sput-boolean v0, Lcom/wsomacp/feature/XCPFeature;->XCP_SUPPORT_SAME_APN:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
