.class public final Lcom/wsomacp/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wsomacp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ACTION_DONE:I = 0x7f050000

.field public static final ADDRESS:I = 0x7f050001

.field public static final APN:I = 0x7f050002

.field public static final APPID:I = 0x7f050003

.field public static final BOOKMARK:I = 0x7f050004

.field public static final BROWSER:I = 0x7f050005

.field public static final CHECK_CPDATA:I = 0x7f050006

.field public static final CONFIGURATION_MESSAGE:I = 0x7f050007

.field public static final CONFIGURATION_MESSAGE_LIST:I = 0x7f050008

.field public static final CP_MESSAGE:I = 0x7f050009

.field public static final DATE:I = 0x7f05000a

.field public static final DEFAULTCANCEL:I = 0x7f05000b

.field public static final DELETE_MESSAGE_CONFIRM:I = 0x7f05000c

.field public static final DELETE_MULTIMESSAGE_CONFIRM:I = 0x7f05000d

.field public static final DSCANCEL:I = 0x7f05000e

.field public static final EMAIL:I = 0x7f05000f

.field public static final IMAP4:I = 0x7f050010

.field public static final INPUT_4DIGIT:I = 0x7f050011

.field public static final INPUT_PINCODE:I = 0x7f050012

.field public static final INSTALLED:I = 0x7f050013

.field public static final INSTALL_FAIL:I = 0x7f050014

.field public static final INSTALL_NEW_ACCOUNT:I = 0x7f050015

.field public static final INSTALL_NOT_SUPPORT:I = 0x7f050016

.field public static final INSTALL_SUCCESS:I = 0x7f050017

.field public static final IN_SERVER:I = 0x7f050018

.field public static final MENU_OPTION_CANCEL:I = 0x7f050019

.field public static final MENU_OPTION_DELETE:I = 0x7f05001a

.field public static final MENU_OPTION_INSTALL:I = 0x7f05001b

.field public static final MENU_OPTION_MORE_OPTIONS:I = 0x7f05001c

.field public static final MESSAGE_ERROR:I = 0x7f05001d

.field public static final MMS:I = 0x7f05001e

.field public static final MMSCENTER:I = 0x7f05001f

.field public static final MMS_PORT:I = 0x7f050020

.field public static final MMS_PROXY:I = 0x7f050021

.field public static final NAME:I = 0x7f050022

.field public static final NEXT:I = 0x7f050023

.field public static final NODATA:I = 0x7f050024

.field public static final OK:I = 0x7f050025

.field public static final OUT_SERVER:I = 0x7f050026

.field public static final PERMISSION_LIST_DESC:I = 0x7f050027

.field public static final PERMISSION_LIST_LAB:I = 0x7f050028

.field public static final PINCODE:I = 0x7f050029

.field public static final PINCODE_AGAIN:I = 0x7f05002a

.field public static final PINCODE_FAIL:I = 0x7f05002b

.field public static final POP3:I = 0x7f05002c

.field public static final PORT:I = 0x7f05002d

.field public static final PROFILE_NAME:I = 0x7f05002e

.field public static final PROXY:I = 0x7f05002f

.field public static final RESULT:I = 0x7f050030

.field public static final ROAMING:I = 0x7f050031

.field public static final SELECTED:I = 0x7f050032

.field public static final SELECT_ALL:I = 0x7f050033

.field public static final SEND_AUTH:I = 0x7f050034

.field public static final SERVER_ADDR:I = 0x7f050035

.field public static final SERVER_PORT:I = 0x7f050036

.field public static final SMTP:I = 0x7f050037

.field public static final SSL:I = 0x7f050038

.field public static final STREAMING:I = 0x7f050039

.field public static final SYNCML_DM:I = 0x7f05003a

.field public static final SYNCML_DS:I = 0x7f05003b

.field public static final URL:I = 0x7f05003c

.field public static final USER_ID:I = 0x7f05003d

.field public static final WSS_CP:I = 0x7f05003e

.field public static final XCAP:I = 0x7f05003f

.field public static final cancel_action:I = 0x7f050040

.field public static final no:I = 0x7f050041

.field public static final off:I = 0x7f050042

.field public static final on:I = 0x7f050043

.field public static final stms_version:I = 0x7f050044

.field public static final yes:I = 0x7f050045


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
