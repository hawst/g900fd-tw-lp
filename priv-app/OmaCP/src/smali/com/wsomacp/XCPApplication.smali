.class public Lcom/wsomacp/XCPApplication;
.super Landroid/app/Application;
.source "XCPApplication.java"

# interfaces
.implements Lcom/wsomacp/interfaces/XCPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/wsomacp/XCPApplication$CPHandler;
    }
.end annotation


# static fields
.field public static g_hView:Landroid/os/Handler;

.field private static m_Context:Landroid/content/Context;

.field private static m_szReleaseVer:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, ""

    sput-object v0, Lcom/wsomacp/XCPApplication;->m_szReleaseVer:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 293
    return-void
.end method

.method static synthetic access$000(Lcom/wsomacp/XCPApplication;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/wsomacp/XCPApplication;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/wsomacp/XCPApplication;->handleMessage(Landroid/os/Message;)V

    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method private handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 243
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 290
    :goto_0
    sget-object v0, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 291
    return-void

    .line 246
    :pswitch_0
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 250
    :pswitch_1
    const/16 v0, 0x11

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(ILjava/lang/Object;)V

    goto :goto_0

    .line 254
    :pswitch_2
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 258
    :pswitch_3
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 262
    :pswitch_4
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 266
    :pswitch_5
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 270
    :pswitch_6
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 274
    :pswitch_7
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 278
    :pswitch_8
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 282
    :pswitch_9
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 286
    :pswitch_a
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/wsomacp/XCPApplication;->xcpServiceSend(I)V

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static xcpGetReleaseVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/wsomacp/XCPApplication;->m_szReleaseVer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "Not Defined"

    .line 81
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/wsomacp/XCPApplication;->m_szReleaseVer:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xcpGetSettingScreenOffTimeOut()I
    .locals 5

    .prologue
    .line 169
    const/4 v1, 0x0

    .line 173
    .local v1, "nScreenOffTimeOut":I
    :try_start_0
    sget-object v2, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_off_timeout"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 174
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get Screen off timeout : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_0
    return v1

    .line 176
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xcpGetSystemServiceManager(Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p0, "szServiceName"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v2, 0x0

    .line 87
    .local v2, "manager":Ljava/lang/Object;
    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 118
    .end local v2    # "manager":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v2

    .line 92
    .restart local v2    # "manager":Ljava/lang/Object;
    :cond_1
    :try_start_0
    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 93
    if-nez v2, :cond_0

    .line 95
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/16 v3, 0xa

    if-ge v1, v3, :cond_0

    .line 99
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 106
    :goto_2
    :try_start_2
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is null, retry..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 107
    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 108
    if-nez v2, :cond_0

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 101
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 113
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xcpGetTopPackageName()Ljava/lang/String;
    .locals 8

    .prologue
    .line 212
    const/4 v4, 0x0

    .line 216
    .local v4, "sbPackageName":Ljava/lang/StringBuffer;
    :try_start_0
    const-string v6, "activity"

    invoke-static {v6}, Lcom/wsomacp/XCPApplication;->xcpGetSystemServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 217
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-eqz v0, :cond_0

    .line 219
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 220
    .local v3, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v3, :cond_0

    .line 222
    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 223
    .local v1, "componentName":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 225
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v4    # "sbPackageName":Ljava/lang/StringBuffer;
    .local v5, "sbPackageName":Ljava/lang/StringBuffer;
    move-object v4, v5

    .line 235
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v5    # "sbPackageName":Ljava/lang/StringBuffer;
    .restart local v4    # "sbPackageName":Ljava/lang/StringBuffer;
    :cond_0
    :goto_0
    if-nez v4, :cond_1

    .line 236
    new-instance v4, Ljava/lang/StringBuffer;

    .end local v4    # "sbPackageName":Ljava/lang/StringBuffer;
    const-string v6, "NoPackage"

    invoke-direct {v4, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 238
    .restart local v4    # "sbPackageName":Ljava/lang/StringBuffer;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 230
    :catch_0
    move-exception v2

    .line 232
    .local v2, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xcpServiceSend(I)V
    .locals 5
    .param p1, "nId"    # I

    .prologue
    .line 123
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 124
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    const-class v4, Lcom/wsomacp/ui/XCPUIDialogActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 126
    invoke-virtual {p0, v0}, Lcom/wsomacp/XCPApplication;->startActivity(Landroid/content/Intent;)V

    .line 127
    return-void
.end method

.method private xcpServiceSend(ILjava/lang/Object;)V
    .locals 5
    .param p1, "nId"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 131
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 132
    sget-object v1, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 133
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    const-class v4, Lcom/wsomacp/ui/XCPUIDialogActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "subid"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 135
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0, v0}, Lcom/wsomacp/XCPApplication;->startActivity(Landroid/content/Intent;)V

    .line 137
    return-void
.end method

.method private static xcpSetReleaseVer(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 64
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v2, Lcom/wsomacp/XCPApplication;->m_szReleaseVer:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 70
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static xcpWakeLcdScreenOn()V
    .locals 6

    .prologue
    .line 141
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 143
    const/4 v1, 0x0

    .line 144
    .local v1, "powerManager":Landroid/os/PowerManager;
    const/4 v0, 0x0

    .line 145
    .local v0, "isScreenOn":Z
    const/4 v2, 0x0

    .line 147
    .local v2, "wakeLock":Landroid/os/PowerManager$WakeLock;
    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 149
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "CONTEXT is null"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 153
    :cond_0
    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "powerManager":Landroid/os/PowerManager;
    check-cast v1, Landroid/os/PowerManager;

    .line 154
    .restart local v1    # "powerManager":Landroid/os/PowerManager;
    invoke-virtual {v1}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    .line 155
    if-eqz v0, :cond_1

    .line 157
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "lcd state is on"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :cond_1
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v4, "Lcd On"

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 163
    const v3, 0x3000000a

    const-string v4, "com.wsomacp"

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    .line 164
    invoke-static {}, Lcom/wsomacp/XCPApplication;->xcpGetSettingScreenOffTimeOut()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    goto :goto_0
.end method

.method public static xcpisCheckTopCPScreen()Z
    .locals 6

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 190
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {}, Lcom/wsomacp/XCPApplication;->xcpGetTopPackageName()Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "szTopPackage":Ljava/lang/String;
    sget-object v3, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 193
    const/4 v0, 0x1

    .line 205
    .end local v2    # "szTopPackage":Ljava/lang/String;
    :goto_0
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "xcpCheckTopCPScreen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 207
    return v0

    .line 197
    .restart local v2    # "szTopPackage":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    .end local v2    # "szTopPackage":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 36
    sput-object p1, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    .line 37
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 48
    const-string v0, "OMACP"

    invoke-static {v0}, Lcom/sec/android/fota/common/Log;->setTagName(Ljava/lang/String;)V

    .line 49
    sget-object v0, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    const-string v1, "log"

    const-string v2, "omacp%d.log"

    invoke-static {v0, v1, v2}, Lcom/sec/android/fota/common/Log;->setContext(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v1, "CPApplication Start !"

    invoke-virtual {v0, v1}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/wsomacp/XCPApplication;->xcpSetReleaseVer(Landroid/content/Context;)V

    .line 53
    sget-object v0, Lcom/wsomacp/XCPApplication;->m_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/wsomacp/ui/XCPUINotiManager;->xcpInitialize(Landroid/content/Context;)V

    .line 54
    invoke-static {}, Lcom/wsomacp/agent/XCPSystem;->xcpSystemSetCPFeature()V

    .line 56
    new-instance v0, Lcom/wsomacp/XCPApplication$CPHandler;

    invoke-direct {v0, p0}, Lcom/wsomacp/XCPApplication$CPHandler;-><init>(Lcom/wsomacp/XCPApplication;)V

    sput-object v0, Lcom/wsomacp/XCPApplication;->g_hView:Landroid/os/Handler;

    .line 57
    return-void
.end method
