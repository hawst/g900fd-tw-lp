.class public Lcom/wsomacp/XCPBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XCPBroadcastReceiver.java"

# interfaces
.implements Lcom/wsomacp/db/XCPDBSql;
.implements Lcom/wsomacp/interfaces/XCPInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private xcpCheckContentType(Ljava/lang/String;)I
    .locals 7
    .param p1, "totalheader"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0x8

    .line 291
    const-string v1, ""

    .line 292
    .local v1, "szContentType":Ljava/lang/String;
    const/4 v0, -0x1

    .line 293
    .local v0, "nRet":I
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cp totalheader : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v6, :cond_1

    .line 295
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 299
    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cp message content type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 301
    const-string v2, "B6"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 302
    const/4 v0, 0x0

    .line 308
    :goto_1
    return v0

    .line 296
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v5, :cond_0

    .line 297
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 303
    :cond_2
    const-string v2, "C2"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 304
    const/4 v0, 0x1

    goto :goto_1

    .line 306
    :cond_3
    sget-object v2, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v3, "Cannot handle data. content type is not support !!"

    invoke-virtual {v2, v3}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 48
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    if-nez p1, :cond_1

    .line 41
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "context is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    if-nez p2, :cond_2

    .line 47
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "intent is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_2
    const-string v4, "android.provider.Telephony.WAP_PUSH_CP_RECEIVED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 52
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "android.provider.Telephony.WAP_PUSH_CP_RECEIVED"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_3
    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 56
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/wsomacp/ui/XCPUINotiManager;->setIndicator(I)V

    goto :goto_0

    .line 58
    :cond_4
    const-string v4, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 60
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 62
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v19

    .line 63
    .local v19, "extras":Landroid/os/Bundle;
    if-nez v19, :cond_5

    .line 65
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "bundle is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_5
    const-string v4, "header"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v30

    .line 70
    .local v30, "pheader":[B
    const-string v4, "data"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v29

    .line 71
    .local v29, "pPdus":[B
    if-eqz v30, :cond_6

    if-nez v29, :cond_7

    .line 73
    :cond_6
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "any data is null"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_7
    const-string v4, "transactionId"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v28

    .line 78
    .local v28, "nTransactionId":I
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v38

    .line 80
    .local v38, "szCptransactionId":Ljava/lang/String;
    const-string v4, "pduType"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v26

    .line 81
    .local v26, "nPushType":I
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v37

    .line 83
    .local v37, "szCppushType":Ljava/lang/String;
    const/4 v7, -0x1

    .line 85
    .local v7, "nContentType":I
    invoke-static/range {p2 .. p2}, Lcom/sec/android/omacp/telephony/FotaTelephonyManagerWrapper;->getSubscriptionId(Landroid/content/Intent;)J

    move-result-wide v8

    .line 87
    .local v8, "subId":J
    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_8

    .line 88
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    .line 90
    :cond_8
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_9

    .line 91
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    .line 93
    :cond_9
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "szCptransactionId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 94
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "szCppushType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v37

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 96
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v23, v0

    .line 97
    .local v23, "nHeaderLen":I
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v35

    .line 98
    .local v35, "szCpHeaderLen":Ljava/lang/String;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_a

    .line 99
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 101
    :cond_a
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "header length : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 103
    invoke-static/range {v30 .. v30}, Lcom/wsomacp/eng/core/XCPUtil;->xcpBytesToHexString([B)Ljava/lang/String;

    move-result-object v39

    .line 104
    .local v39, "szHeader":Ljava/lang/String;
    const-string v4, "%s%s%s%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v38, v5, v6

    const/4 v6, 0x1

    aput-object v37, v5, v6

    const/4 v6, 0x2

    aput-object v35, v5, v6

    const/4 v6, 0x3

    aput-object v39, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v40

    .line 106
    .local v40, "szTotalHeader":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/wsomacp/XCPBroadcastReceiver;->xcpCheckContentType(Ljava/lang/String;)I

    move-result v7

    .line 107
    sget-boolean v4, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SUPPORT_OMADM_BOOTSTRAP:Z

    if-eqz v4, :cond_b

    .line 109
    const/4 v4, -0x1

    if-ne v7, v4, :cond_d

    .line 111
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "WSSOMA CP doesn\'t handle this data"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 117
    :cond_b
    const/4 v4, 0x1

    if-eq v7, v4, :cond_c

    const/4 v4, -0x1

    if-ne v7, v4, :cond_d

    .line 119
    :cond_c
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "WSSOMA CP doesn\'t handle DM Bootstrap data"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 124
    :cond_d
    invoke-static/range {v29 .. v29}, Lcom/wsomacp/eng/core/XCPUtil;->xcpBytesToHexString([B)Ljava/lang/String;

    move-result-object v36

    .line 126
    .local v36, "szCpMessage":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "=== CP Message is received === "

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 127
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cp message header : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 128
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cp message body : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 130
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 131
    .local v41, "szTotalpdus":Ljava/lang/String;
    invoke-static/range {v41 .. v41}, Lcom/wsomacp/eng/core/XCPUtil;->xcpHexStringToBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 132
    .local v3, "byMessage":[B
    if-eqz v3, :cond_0

    .line 135
    new-instance v2, Lcom/wsomacp/ui/XCPUIDetailFragment;

    invoke-direct {v2}, Lcom/wsomacp/ui/XCPUIDetailFragment;-><init>()V

    .line 137
    .local v2, "uiConf":Lcom/wsomacp/ui/XCPUIDetailFragment;
    sget v5, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_SECURITY:I

    sget-boolean v6, Lcom/wsomacp/feature/XCPFeature;->XCP_FEATURE_NETWPIN_CHECK:Z

    move-object/from16 v4, p1

    invoke-virtual/range {v2 .. v9}, Lcom/wsomacp/ui/XCPUIDetailFragment;->xcpVerifyAuthentication([BLandroid/content/Context;IZIJ)I

    move-result v27

    .line 138
    .local v27, "nRtn":I
    if-nez v27, :cond_12

    .line 140
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetBGInstall()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    .line 142
    new-instance v21, Landroid/content/Intent;

    invoke-direct/range {v21 .. v21}, Landroid/content/Intent;-><init>()V

    .line 143
    .local v21, "intentMCCMNC":Landroid/content/Intent;
    const-string v4, "android.intent.action.SET_LTE_MCCMNC"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetVendorConfigValue()Ljava/lang/String;

    move-result-object v43

    .line 146
    .local v43, "szValue":Ljava/lang/String;
    const-string v4, ";"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v44

    .line 147
    .local v44, "sz_Value":[Ljava/lang/String;
    const-string v4, "MCCMNC"

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Broadcast Send - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 149
    const/16 v4, 0x20

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 150
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 151
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetBGInstall(I)V

    goto/16 :goto_0

    .line 154
    .end local v21    # "intentMCCMNC":Landroid/content/Intent;
    .end local v43    # "szValue":Ljava/lang/String;
    .end local v44    # "sz_Value":[Ljava/lang/String;
    :cond_e
    invoke-static {}, Lcom/wsomacp/agent/XCPConAccount;->xcpGetBGInstall()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_f

    .line 156
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/wsomacp/agent/XCPConAccount;->xcpSetBGInstall(I)V

    goto/16 :goto_0

    .line 160
    :cond_f
    const-wide/16 v24, 0x0

    .line 164
    .local v24, "lRowId":J
    :try_start_0
    new-instance v32, Ljava/text/SimpleDateFormat;

    sget-object v4, Lcom/wsomacp/eng/core/XCPUtil;->XCP_DATE_FORMAT_YYYYMMDD:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v32

    invoke-direct {v0, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 165
    .local v32, "sdfNow":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v46

    move-wide/from16 v0, v46

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 167
    .local v15, "date":Ljava/lang/String;
    move-object/from16 v0, v41

    invoke-static {v15, v0, v7, v8, v9}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlInsertEntry(Ljava/lang/String;Ljava/lang/String;IJ)J

    move-result-wide v24

    .line 170
    invoke-static {}, Lcom/wsomacp/XCPApplication;->xcpisCheckTopCPScreen()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 172
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListFragment;->xcpAddMessage()V

    .line 173
    invoke-static {}, Lcom/wsomacp/ui/XCPUIListDelActivity;->xcpAddMessageInDel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_10
    const/4 v4, 0x1

    if-ne v7, v4, :cond_11

    .line 185
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s/items/%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v45, 0x0

    sget-object v46, Lcom/wsomacp/XCPBroadcastReceiver;->XCP_CONTENT_URI:Landroid/net/Uri;

    aput-object v46, v6, v45

    const/16 v45, 0x1

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v46

    aput-object v46, v6, v45

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    .line 186
    .local v42, "szUri":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onClick DM Bootstrap Message === "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v42

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->H(Ljava/lang/String;)V

    .line 187
    invoke-static/range {v42 .. v42}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v33

    .line 188
    .local v33, "sendDBUri":Landroid/net/Uri;
    new-instance v10, Landroid/content/Intent;

    const-string v4, "android.provider.Telephony.WAP_PUSH_DM_RECEIVED"

    invoke-direct {v10, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 189
    .local v10, "DMintent":Landroid/content/Intent;
    new-instance v18, Landroid/os/Bundle;

    invoke-direct/range {v18 .. v18}, Landroid/os/Bundle;-><init>()V

    .line 190
    .local v18, "extra":Landroid/os/Bundle;
    const-string v4, "dm_message"

    move-object/from16 v0, v18

    move-object/from16 v1, v33

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 191
    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 192
    const-string v4, "wsomacp_enable"

    const/4 v5, 0x1

    invoke-virtual {v10, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 193
    const/16 v4, 0x20

    invoke-virtual {v10, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 194
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 176
    .end local v10    # "DMintent":Landroid/content/Intent;
    .end local v15    # "date":Ljava/lang/String;
    .end local v18    # "extra":Landroid/os/Bundle;
    .end local v32    # "sdfNow":Ljava/text/SimpleDateFormat;
    .end local v33    # "sendDBUri":Landroid/net/Uri;
    .end local v42    # "szUri":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 178
    .local v16, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 198
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v15    # "date":Ljava/lang/String;
    .restart local v32    # "sdfNow":Ljava/text/SimpleDateFormat;
    :cond_11
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/wsomacp/ui/XCPUINotiManager;->setIndicator(I)V

    goto/16 :goto_0

    .line 201
    .end local v15    # "date":Ljava/lang/String;
    .end local v24    # "lRowId":J
    .end local v32    # "sdfNow":Ljava/text/SimpleDateFormat;
    :cond_12
    const/4 v4, -0x1

    move/from16 v0, v27

    if-ne v0, v4, :cond_13

    .line 203
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "Not saved CP msg"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 205
    :cond_13
    const/4 v4, 0x1

    move/from16 v0, v27

    if-ne v0, v4, :cond_0

    .line 207
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "Bg CP msg"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    .end local v2    # "uiConf":Lcom/wsomacp/ui/XCPUIDetailFragment;
    .end local v3    # "byMessage":[B
    .end local v7    # "nContentType":I
    .end local v8    # "subId":J
    .end local v19    # "extras":Landroid/os/Bundle;
    .end local v23    # "nHeaderLen":I
    .end local v26    # "nPushType":I
    .end local v27    # "nRtn":I
    .end local v28    # "nTransactionId":I
    .end local v29    # "pPdus":[B
    .end local v30    # "pheader":[B
    .end local v35    # "szCpHeaderLen":Ljava/lang/String;
    .end local v36    # "szCpMessage":Ljava/lang/String;
    .end local v37    # "szCppushType":Ljava/lang/String;
    .end local v38    # "szCptransactionId":Ljava/lang/String;
    .end local v39    # "szHeader":Ljava/lang/String;
    .end local v40    # "szTotalHeader":Ljava/lang/String;
    .end local v41    # "szTotalpdus":Ljava/lang/String;
    :cond_14
    const-string v4, "android.intent.action.SET_CP"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 212
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "android.intent.action.SET_CP"

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 213
    const-string v11, ""

    .line 214
    .local v11, "activity":Ljava/lang/String;
    const/4 v13, 0x0

    .line 215
    .local v13, "className":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v14, 0x0

    .line 219
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlQuery;->xcpDBSqlQueryCursor()Landroid/database/Cursor;

    move-result-object v14

    .line 220
    if-eqz v14, :cond_15

    .line 222
    invoke-interface {v14}, Landroid/database/Cursor;->moveToLast()Z

    move-result v4

    if-nez v4, :cond_15

    .line 224
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    const-string v5, "CP Message Empty !!! "

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 239
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 240
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 242
    :catch_1
    move-exception v17

    .line 244
    .local v17, "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    .end local v17    # "ex":Ljava/lang/Exception;
    :cond_15
    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 239
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    if-eqz v4, :cond_16

    .line 240
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 248
    :cond_16
    :goto_1
    const-string v4, "activity"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager;

    .line 249
    .local v12, "activityManager":Landroid/app/ActivityManager;
    if-eqz v12, :cond_18

    .line 251
    const/16 v4, 0x64

    invoke-virtual {v12, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v20

    .line 252
    .local v20, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v20, :cond_18

    .line 254
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_17
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_18

    .line 256
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 257
    .local v31, "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v31

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.wsomacp"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 259
    move-object/from16 v0, v31

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    .line 266
    .end local v20    # "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v22    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v31    # "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_18
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "activity : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->I(Ljava/lang/String;)V

    .line 267
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1d

    .line 269
    const-string v4, "com.wsomacp.ui.XCPUIDetailActivity"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 270
    const-class v13, Lcom/wsomacp/ui/XCPUIDetailActivity;

    .line 283
    :goto_2
    new-instance v34, Landroid/content/Intent;

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 284
    .local v34, "startActivity":Landroid/content/Intent;
    const/high16 v4, 0x34000000

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 242
    .end local v12    # "activityManager":Landroid/app/ActivityManager;
    .end local v34    # "startActivity":Landroid/content/Intent;
    :catch_2
    move-exception v17

    .line 244
    .restart local v17    # "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 229
    .end local v17    # "ex":Ljava/lang/Exception;
    :catch_3
    move-exception v16

    .line 231
    .restart local v16    # "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 238
    :try_start_5
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 239
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 240
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 242
    :catch_4
    move-exception v17

    .line 244
    .restart local v17    # "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 236
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v17    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 238
    :try_start_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 239
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    if-eqz v5, :cond_19

    .line 240
    invoke-static {}, Lcom/wsomacp/db/XCPDBSqlProvider;->xcpDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 245
    :cond_19
    :goto_3
    throw v4

    .line 242
    :catch_5
    move-exception v17

    .line 244
    .restart local v17    # "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/omacp/log/FileLog;->DEBUG:Lcom/sec/android/fota/common/log/Logger$Impl;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/fota/common/log/Logger$Impl;->E(Ljava/lang/String;)V

    goto :goto_3

    .line 271
    .end local v17    # "ex":Ljava/lang/Exception;
    .restart local v12    # "activityManager":Landroid/app/ActivityManager;
    :cond_1a
    const-string v4, "com.wsomacp.ui.XCPUIDialogActivity"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 272
    const-class v13, Lcom/wsomacp/ui/XCPUIDialogActivity;

    goto :goto_2

    .line 273
    :cond_1b
    const-string v4, "com.wsomacp.ui.XCPUIListDelActivity"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 274
    const-class v13, Lcom/wsomacp/ui/XCPUIListDelActivity;

    goto :goto_2

    .line 276
    :cond_1c
    const-class v13, Lcom/wsomacp/WSSClientProvUiList;

    goto/16 :goto_2

    .line 280
    :cond_1d
    const-class v13, Lcom/wsomacp/WSSClientProvUiList;

    goto/16 :goto_2
.end method
