.class public Lldpsdksub/e/a;
.super Ljava/lang/Object;


# static fields
.field private static e:Ljava/lang/String;


# instance fields
.field protected a:Lldpsdksub/g/f;

.field protected b:Lldpsdksub/c/d;

.field protected c:Lldpsdksub/g/g;

.field protected d:Z

.field private f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

.field private g:Lldpsdksub/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/e/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/e/a;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lldpsdksub/c/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    iput-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iput-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    iput-object v0, p0, Lldpsdksub/e/a;->g:Lldpsdksub/c/a;

    iput-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lldpsdksub/e/a;->d:Z

    iput-object p1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    new-instance v0, Lldpsdksub/g/f;

    invoke-direct {v0}, Lldpsdksub/g/f;-><init>()V

    iput-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    new-instance v0, Lldpsdksub/g/g;

    invoke-direct {v0}, Lldpsdksub/g/g;-><init>()V

    iput-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    return-void
.end method

.method private b(Landroid/graphics/Bitmap$Config;)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InitDrawerCanvas() m_bLDBLyrics:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lldpsdksub/e/a;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :try_start_0
    iget-boolean v1, p0, Lldpsdksub/e/a;->d:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v1}, Lldpsdksub/g/f;->c()I

    move-result v1

    iget-object v2, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v3, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    iget-object v4, v2, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v4, :cond_2

    iget-object v2, v2, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v2, v3}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetLDPWordData(Lldpsdksub/g/f;)V

    :cond_2
    :goto_1
    iget-object v2, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v2, v1, p1}, Lldpsdksub/c/d;->a(ILandroid/graphics/Bitmap$Config;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lldpsdksub/e/a;->e()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lldpsdksub/e/a;->c(I)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v1}, Lldpsdksub/g/g;->c()I

    move-result v1

    iget-object v2, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v3, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    iget-object v4, v2, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v4, :cond_2

    iget-object v2, v2, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v2, v3}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetLDBWordData(Lldpsdksub/g/g;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v2, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitDrawerCanvas() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitDrawerCanvas() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method public static c()V
    .locals 0

    return-void
.end method

.method private j()Z
    .locals 5

    const/4 v3, 0x3

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-boolean v0, v0, Lldpsdksub/c/d;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v2, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    invoke-interface {v2}, Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;->GetScrollViewWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lldpsdksub/c/d;->a(I)V

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v2, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    invoke-interface {v2}, Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;->GetScrollViewHegiht()I

    move-result v2

    invoke-virtual {v0, v2}, Lldpsdksub/c/d;->b(I)V

    :cond_0
    iget-object v0, p0, Lldpsdksub/e/a;->g:Lldpsdksub/c/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-boolean v0, v0, Lldpsdksub/c/d;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v2, p0, Lldpsdksub/e/a;->g:Lldpsdksub/c/a;

    invoke-interface {v2}, Lldpsdksub/c/a;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lldpsdksub/c/d;->a(I)V

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v2, p0, Lldpsdksub/e/a;->g:Lldpsdksub/c/a;

    invoke-interface {v2}, Lldpsdksub/c/a;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Lldpsdksub/c/d;->b(I)V

    :cond_1
    iget-boolean v0, p0, Lldpsdksub/e/a;->d:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-nez v0, :cond_3

    :cond_2
    :goto_0
    return v1

    :cond_3
    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0}, Lldpsdksub/g/f;->c()I

    move-result v0

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    iget-object v2, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v2, v2, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    iget-object v3, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v3, v3, Lldpsdksub/c/d;->a:I

    iget-object v4, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v4}, Lldpsdksub/c/d;->c()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lldpsdksub/g/f;->a(Landroid/graphics/Paint;II)Z

    move-result v0

    iget-object v2, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v2}, Lldpsdksub/g/f;->f()V

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v0}, Lldpsdksub/g/g;->c()I

    move-result v0

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    iget-object v2, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-object v2, v2, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    iget-object v3, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v3, v3, Lldpsdksub/c/d;->a:I

    iget-object v4, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v4}, Lldpsdksub/c/d;->c()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lldpsdksub/g/g;->a(Landroid/graphics/Paint;II)Z

    move-result v0

    iget-object v2, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v2}, Lldpsdksub/g/g;->f()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v2, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitLyricWidth() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitLyricWidth() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected final a(II)I
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0, p1, p2}, Lldpsdksub/g/f;->b(II)I

    move-result v0

    return v0
.end method

.method protected final a(I)Lldpsdksub/g/b;
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0, p1}, Lldpsdksub/g/f;->b(I)Lldpsdksub/g/b;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0}, Lldpsdksub/c/d;->a()V

    :cond_0
    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0}, Lldpsdksub/g/f;->a()V

    :cond_1
    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v0}, Lldpsdksub/g/g;->a()V

    :cond_2
    iput-object v1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iput-object v1, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    iput-object v1, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    return-void
.end method

.method protected final a(III)V
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0, p1, p2, p3}, Lldpsdksub/c/d;->a(III)V

    return-void
.end method

.method public final a(Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;)V
    .locals 0

    iput-object p1, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    return-void
.end method

.method public final a(Lldpsdksub/c/a;)V
    .locals 1

    iput-object p1, p0, Lldpsdksub/e/a;->g:Lldpsdksub/c/a;

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0, p1}, Lldpsdksub/c/d;->a(Lldpsdksub/c/a;)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lldpsdksub/e/a;->d:Z

    return-void
.end method

.method public final a(IIILandroid/graphics/Typeface;)Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lldpsdksub/c/d;->a(IIILandroid/graphics/Typeface;)Z

    move-result v0

    return v0
.end method

.method protected final a(IIILldpsdksub/g/f;)Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lldpsdksub/c/d;->a(IIILldpsdksub/g/f;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final a(IIILldpsdksub/g/g;)Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lldpsdksub/c/d;->a(IIILldpsdksub/g/g;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap$Config;)Z
    .locals 1

    invoke-direct {p0, p1}, Lldpsdksub/e/a;->b(Landroid/graphics/Bitmap$Config;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/View;IIIIZ)Z
    .locals 2

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    check-cast p1, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iput-object p1, v0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iput p2, v0, Lldpsdksub/c/d;->a:I

    iput p3, v0, Lldpsdksub/c/d;->b:I

    iput-boolean p6, v0, Lldpsdksub/c/d;->c:Z

    new-instance v1, Lldpsdksub/a/a;

    invoke-direct {v1, p5}, Lldpsdksub/a/a;-><init>(I)V

    iput-object v1, v0, Lldpsdksub/c/d;->d:Lldpsdksub/a/a;

    iput p4, v0, Lldpsdksub/c/d;->f:I

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lldpsdksub/g/b;)Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0, p1}, Lldpsdksub/g/f;->a(Lldpsdksub/g/b;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppendWordLine() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/e/a;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppendWordLine() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lldpsdksub/g/c;)Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v0, p1}, Lldpsdksub/g/g;->a(Lldpsdksub/g/c;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0}, Lldpsdksub/g/f;->b()V

    :cond_0
    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v0}, Lldpsdksub/g/g;->b()V

    :cond_1
    invoke-virtual {p0}, Lldpsdksub/e/a;->f()V

    return-void
.end method

.method public b(I)V
    .locals 0

    return-void
.end method

.method protected final c(I)V
    .locals 4

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    iget-object v1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->h:I

    iget-object v2, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v2}, Lldpsdksub/g/f;->c()I

    move-result v2

    iget-object v3, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v3, v3, Lldpsdksub/c/d;->f:I

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;->SetLineChange(IIII)V

    :cond_0
    return-void
.end method

.method protected final d(I)V
    .locals 4

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    iget-object v1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->h:I

    iget-object v2, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v2}, Lldpsdksub/g/g;->c()I

    move-result v2

    iget-object v3, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v3, v3, Lldpsdksub/c/d;->f:I

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;->SetLineChange(IIII)V

    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    invoke-direct {p0}, Lldpsdksub/e/a;->j()Z

    move-result v0

    return v0
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected final f()V
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    invoke-virtual {v0}, Lldpsdksub/c/d;->b()V

    :cond_0
    return-void
.end method

.method protected final g()V
    .locals 5

    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->f:Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;

    iget-object v1, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->h:I

    iget-object v2, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v2}, Lldpsdksub/g/f;->c()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget v4, v4, Lldpsdksub/c/d;->f:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;->InitLineScroll(IIII)V

    :cond_0
    return-void
.end method

.method protected final h()Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/e/a;->b:Lldpsdksub/c/d;

    iget-boolean v0, v0, Lldpsdksub/c/d;->c:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lldpsdksub/e/a;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lldpsdksub/e/a;->a:Lldpsdksub/g/f;

    invoke-virtual {v0}, Lldpsdksub/g/f;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    if-nez v0, :cond_2

    const-string v0, ""

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lldpsdksub/e/a;->c:Lldpsdksub/g/g;

    invoke-virtual {v0}, Lldpsdksub/g/g;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
