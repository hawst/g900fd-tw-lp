.class public Lldpsdksub/e/b;
.super Lldpsdksub/e/a;


# static fields
.field private static e:Ljava/lang/String;


# instance fields
.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/e/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/e/b;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lldpsdksub/c/d;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lldpsdksub/e/a;-><init>(Lldpsdksub/c/d;)V

    iput v1, p0, Lldpsdksub/e/b;->f:I

    iput v1, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->h:I

    iput v0, p0, Lldpsdksub/e/b;->i:I

    iput v0, p0, Lldpsdksub/e/b;->j:I

    iput v0, p0, Lldpsdksub/e/b;->k:I

    return-void
.end method

.method private e(I)I
    .locals 5

    const/4 v3, -0x1

    const/16 v0, -0xb

    iget-object v1, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    if-nez v1, :cond_0

    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {v1}, Lldpsdksub/g/f;->c()I

    move-result v1

    iput v1, p0, Lldpsdksub/e/b;->h:I

    iget v1, p0, Lldpsdksub/e/b;->h:I

    if-gtz v1, :cond_1

    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {v1}, Lldpsdksub/g/f;->d()I

    move-result v1

    iget-object v2, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {v2}, Lldpsdksub/g/f;->e()I

    move-result v2

    if-eq v1, v3, :cond_2

    if-ne v2, v3, :cond_3

    :cond_2
    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    goto :goto_0

    :cond_3
    if-le v1, p1, :cond_4

    const/16 v0, -0xc

    goto :goto_0

    :cond_4
    if-gt v2, p1, :cond_5

    const/16 v0, -0xf

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    iget v4, p0, Lldpsdksub/e/b;->j:I

    invoke-virtual {v3, p1, v4}, Lldpsdksub/g/f;->a(II)Lldpsdksub/g/b;

    move-result-object v3

    if-eqz v3, :cond_7

    :try_start_0
    invoke-virtual {v3}, Lldpsdksub/g/b;->d()I

    move-result v3

    iput v3, p0, Lldpsdksub/e/b;->j:I

    iget v3, p0, Lldpsdksub/e/b;->i:I

    iget v4, p0, Lldpsdksub/e/b;->j:I

    if-ne v3, v4, :cond_6

    const/16 v0, -0xe

    goto :goto_0

    :cond_6
    iget v0, p0, Lldpsdksub/e/b;->j:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    :cond_7
    iget-object v3, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {v3, p1}, Lldpsdksub/g/f;->a(I)Lldpsdksub/g/b;

    move-result-object v3

    if-eqz v3, :cond_8

    :try_start_1
    invoke-virtual {v3}, Lldpsdksub/g/b;->d()I

    move-result v3

    iput v3, p0, Lldpsdksub/e/b;->k:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v0, -0xd

    goto :goto_0

    :catch_1
    move-exception v3

    :cond_8
    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLine() -------------------- m_nEmptyLineIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLine() -------------------- m_nNowLineIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lldpsdksub/e/b;->i:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLine() -------------------- m_nLastLineIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lldpsdksub/e/b;->j:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLine() -------------------- nFirstLineStartTime:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "GetCurrentLine() -------------------- nLastLineEndTime:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetCurrentLine() -------------------- nCurrentTime:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private f(I)I
    .locals 3

    const/4 v1, 0x0

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    iget v2, p0, Lldpsdksub/e/b;->h:I

    if-gt v2, v0, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    iget-object v2, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v2, v2, Lldpsdksub/c/d;->e:I

    mul-int/2addr v2, v0

    if-le v2, p1, :cond_2

    iget-object v1, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->e:I

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v1, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private g(I)I
    .locals 2

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lldpsdksub/e/b;->h:I

    if-gt v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v0, v0, Lldpsdksub/c/d;->e:I

    :goto_1
    return v0

    :cond_1
    iget-object v1, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->e:I

    mul-int/2addr v1, v0

    if-le v1, p1, :cond_2

    iget-object v1, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->e:I

    mul-int/2addr v0, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private h(I)I
    .locals 5

    const/4 v3, -0x1

    const/16 v0, -0xb

    iget-object v1, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {v1}, Lldpsdksub/g/g;->c()I

    move-result v1

    iput v1, p0, Lldpsdksub/e/b;->h:I

    iget v1, p0, Lldpsdksub/e/b;->h:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {v1}, Lldpsdksub/g/g;->d()I

    move-result v1

    iget-object v2, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {v2}, Lldpsdksub/g/g;->e()I

    move-result v2

    if-eq v1, v3, :cond_2

    if-ne v2, v3, :cond_3

    :cond_2
    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    goto :goto_0

    :cond_3
    if-le v1, p1, :cond_4

    const/16 v0, -0xc

    goto :goto_0

    :cond_4
    if-gt v2, p1, :cond_5

    const/16 v0, -0xf

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    iget v4, p0, Lldpsdksub/e/b;->j:I

    invoke-virtual {v3, p1, v4}, Lldpsdksub/g/g;->a(II)Lldpsdksub/g/c;

    move-result-object v3

    if-eqz v3, :cond_6

    :try_start_0
    iget v3, v3, Lldpsdksub/g/c;->b:I

    iput v3, p0, Lldpsdksub/e/b;->j:I

    iget v0, p0, Lldpsdksub/e/b;->j:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_6
    const/16 v0, -0xe

    goto :goto_0

    :catch_0
    move-exception v3

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLineLDB() GetCurrentLineEmptyTime() -------------------- nCurrentTime:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v3, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {v3, p1}, Lldpsdksub/g/g;->a(I)Lldpsdksub/g/c;

    move-result-object v3

    if-eqz v3, :cond_7

    :try_start_1
    iget v3, v3, Lldpsdksub/g/c;->b:I

    iput v3, p0, Lldpsdksub/e/b;->k:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v0, -0xd

    goto :goto_0

    :catch_1
    move-exception v3

    :cond_7
    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLineLDB() -------------------- m_nEmptyLineIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLineLDB() -------------------- m_nNowLineIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lldpsdksub/e/b;->i:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLineLDB() -------------------- m_nLastLineIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lldpsdksub/e/b;->j:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetCurrentLineLDB() -------------------- nFirstLineStartTime:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "GetCurrentLineLDB() -------------------- nLastLineStartTime:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v1, Lldpsdksub/e/b;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetCurrentLineLDB() -------------------- nCurrentTime:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private i(I)I
    .locals 3

    const/4 v1, 0x0

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    iget v2, p0, Lldpsdksub/e/b;->h:I

    if-gt v2, v0, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    iget-object v2, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v2, v2, Lldpsdksub/c/d;->e:I

    mul-int/2addr v2, v0

    if-le v2, p1, :cond_2

    iget-object v1, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->e:I

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v1, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private j(I)I
    .locals 2

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lldpsdksub/e/b;->h:I

    if-gt v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v0, v0, Lldpsdksub/c/d;->e:I

    :goto_1
    return v0

    :cond_1
    iget-object v1, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->e:I

    mul-int/2addr v1, v0

    if-le v1, p1, :cond_2

    iget-object v1, p0, Lldpsdksub/e/b;->b:Lldpsdksub/c/d;

    iget v1, v1, Lldpsdksub/c/d;->e:I

    mul-int/2addr v0, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final b(I)V
    .locals 5

    const/16 v2, -0xb

    const/4 v4, 0x7

    const/4 v1, -0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lldpsdksub/e/b;->d:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lldpsdksub/e/b;->h(I)I

    move-result v0

    if-eq v0, v2, :cond_0

    packed-switch v0, :pswitch_data_0

    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v3, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    if-ltz v0, :cond_0

    iget v1, p0, Lldpsdksub/e/b;->i:I

    if-eq v1, v0, :cond_0

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->i(I)I

    move-result v1

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->j(I)I

    move-result v2

    iget-object v3, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {p0, v1, v2, v0, v3}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/g;)Z

    iput v0, p0, Lldpsdksub/e/b;->i:I

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->d(I)V

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iput v3, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v3}, Lldpsdksub/e/b;->i(I)I

    move-result v0

    invoke-direct {p0, v3}, Lldpsdksub/e/b;->j(I)I

    move-result v1

    iget-object v2, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {p0, v0, v1, v3, v2}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/g;)Z

    iget v0, p0, Lldpsdksub/e/b;->f:I

    if-le v4, v0, :cond_0

    invoke-virtual {p0, v3}, Lldpsdksub/e/b;->d(I)V

    invoke-virtual {p0}, Lldpsdksub/e/b;->g()V

    iget v0, p0, Lldpsdksub/e/b;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lldpsdksub/e/b;->f:I

    goto :goto_0

    :pswitch_2
    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v3, p0, Lldpsdksub/e/b;->g:I

    iget v0, p0, Lldpsdksub/e/b;->k:I

    if-ltz v0, :cond_0

    iget v0, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->i(I)I

    move-result v0

    iget v1, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v1}, Lldpsdksub/e/b;->j(I)I

    move-result v1

    iget v2, p0, Lldpsdksub/e/b;->k:I

    iget-object v3, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {p0, v0, v1, v2, v3}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/g;)Z

    iget v0, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->d(I)V

    goto :goto_0

    :pswitch_3
    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    iget v0, p0, Lldpsdksub/e/b;->g:I

    if-le v4, v0, :cond_0

    iget v0, p0, Lldpsdksub/e/b;->h:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->i(I)I

    move-result v0

    iget v1, p0, Lldpsdksub/e/b;->h:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lldpsdksub/e/b;->j(I)I

    move-result v1

    iget v2, p0, Lldpsdksub/e/b;->h:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lldpsdksub/e/b;->c:Lldpsdksub/g/g;

    invoke-virtual {p0, v0, v1, v2, v3}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/g;)Z

    iget v0, p0, Lldpsdksub/e/b;->h:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->d(I)V

    iget v0, p0, Lldpsdksub/e/b;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lldpsdksub/e/b;->g:I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lldpsdksub/e/b;->e(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    sget-object v0, Lldpsdksub/e/b;->e:Ljava/lang/String;

    goto :goto_0

    :cond_2
    packed-switch v0, :pswitch_data_1

    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v3, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    if-ltz v0, :cond_0

    iget v1, p0, Lldpsdksub/e/b;->i:I

    if-eq v1, v0, :cond_0

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->f(I)I

    move-result v1

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->g(I)I

    move-result v2

    iget-object v3, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {p0, v1, v2, v0, v3}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/f;)Z

    iput v0, p0, Lldpsdksub/e/b;->i:I

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->c(I)V

    goto/16 :goto_0

    :pswitch_4
    iput v3, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v3}, Lldpsdksub/e/b;->f(I)I

    move-result v0

    invoke-direct {p0, v3}, Lldpsdksub/e/b;->g(I)I

    move-result v1

    iget-object v2, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {p0, v0, v1, v3, v2}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/f;)Z

    iget v0, p0, Lldpsdksub/e/b;->f:I

    if-le v4, v0, :cond_0

    invoke-virtual {p0, v3}, Lldpsdksub/e/b;->c(I)V

    invoke-virtual {p0}, Lldpsdksub/e/b;->g()V

    iget v0, p0, Lldpsdksub/e/b;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lldpsdksub/e/b;->f:I

    goto/16 :goto_0

    :pswitch_5
    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v3, p0, Lldpsdksub/e/b;->g:I

    iget v0, p0, Lldpsdksub/e/b;->k:I

    if-ltz v0, :cond_0

    iget v0, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->f(I)I

    move-result v0

    iget v1, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v1}, Lldpsdksub/e/b;->g(I)I

    move-result v1

    iget v2, p0, Lldpsdksub/e/b;->k:I

    iget-object v3, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {p0, v0, v1, v2, v3}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/f;)Z

    iget v0, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->c(I)V

    iget v0, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->a(I)Lldpsdksub/g/b;

    move-result-object v0

    iget v1, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {p0, p1, v1}, Lldpsdksub/e/b;->a(II)I

    move-result v1

    iget v2, p0, Lldpsdksub/e/b;->k:I

    iget v3, p0, Lldpsdksub/e/b;->k:I

    invoke-direct {p0, v3}, Lldpsdksub/e/b;->f(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Lldpsdksub/g/b;->g()I

    move-result v3

    invoke-virtual {v0}, Lldpsdksub/g/b;->f()I

    invoke-virtual {p0, v1, v2, v3}, Lldpsdksub/e/b;->a(III)V

    goto/16 :goto_0

    :pswitch_6
    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    iget v0, p0, Lldpsdksub/e/b;->h:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lldpsdksub/e/b;->g:I

    if-le v4, v1, :cond_3

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->f(I)I

    move-result v1

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->g(I)I

    move-result v2

    iget-object v3, p0, Lldpsdksub/e/b;->a:Lldpsdksub/g/f;

    invoke-virtual {p0, v1, v2, v0, v3}, Lldpsdksub/e/b;->a(IIILldpsdksub/g/f;)Z

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->c(I)V

    iget v1, p0, Lldpsdksub/e/b;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lldpsdksub/e/b;->g:I

    :cond_3
    invoke-virtual {p0}, Lldpsdksub/e/b;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->a(I)Lldpsdksub/g/b;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lldpsdksub/e/b;->a(II)I

    move-result v2

    invoke-direct {p0, v0}, Lldpsdksub/e/b;->f(I)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v1}, Lldpsdksub/g/b;->g()I

    move-result v3

    invoke-virtual {v1}, Lldpsdksub/g/b;->f()I

    invoke-virtual {p0, v2, v0, v3}, Lldpsdksub/e/b;->a(III)V

    goto/16 :goto_0

    :pswitch_7
    iput v3, p0, Lldpsdksub/e/b;->f:I

    iput v3, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->k:I

    iget v0, p0, Lldpsdksub/e/b;->j:I

    if-ltz v0, :cond_0

    iget v0, p0, Lldpsdksub/e/b;->j:I

    invoke-virtual {p0, v0}, Lldpsdksub/e/b;->a(I)Lldpsdksub/g/b;

    move-result-object v0

    iget v1, p0, Lldpsdksub/e/b;->j:I

    invoke-virtual {p0, p1, v1}, Lldpsdksub/e/b;->a(II)I

    move-result v1

    iget v2, p0, Lldpsdksub/e/b;->j:I

    iget v3, p0, Lldpsdksub/e/b;->j:I

    invoke-direct {p0, v3}, Lldpsdksub/e/b;->f(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Lldpsdksub/g/b;->g()I

    move-result v3

    invoke-virtual {v0}, Lldpsdksub/g/b;->f()I

    invoke-virtual {p0, v1, v2, v3}, Lldpsdksub/e/b;->a(III)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xf
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0xf
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v1, p0, Lldpsdksub/e/b;->f:I

    iput v1, p0, Lldpsdksub/e/b;->g:I

    iput v1, p0, Lldpsdksub/e/b;->h:I

    iput v0, p0, Lldpsdksub/e/b;->i:I

    iput v0, p0, Lldpsdksub/e/b;->j:I

    iput v0, p0, Lldpsdksub/e/b;->k:I

    invoke-virtual {p0}, Lldpsdksub/e/b;->g()V

    invoke-virtual {p0}, Lldpsdksub/e/b;->f()V

    sget-object v0, Lldpsdksub/e/b;->e:Ljava/lang/String;

    return-void
.end method
