.class public Lldpsdksub/b/c;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Lldpsdksub/b/a;

.field private c:[S

.field private d:[S

.field private e:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iput-object v0, p0, Lldpsdksub/b/c;->c:[S

    iput-object v0, p0, Lldpsdksub/b/c;->d:[S

    iput-object v0, p0, Lldpsdksub/b/c;->e:[S

    new-instance v0, Lldpsdksub/b/a;

    invoke-direct {v0}, Lldpsdksub/b/a;-><init>()V

    iput-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    const/4 v0, 0x6

    new-array v0, v0, [S

    iput-object v0, p0, Lldpsdksub/b/c;->c:[S

    const/4 v0, 0x3

    new-array v0, v0, [S

    iput-object v0, p0, Lldpsdksub/b/c;->d:[S

    iget-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget v0, v0, Lldpsdksub/b/a;->a:I

    new-array v0, v0, [S

    iput-object v0, p0, Lldpsdksub/b/c;->e:[S

    return-void
.end method

.method private static a(BB)I
    .locals 2

    and-int/lit8 v0, p0, 0x7f

    mul-int/lit16 v0, v0, 0x80

    and-int/lit8 v1, p1, 0x7f

    add-int/2addr v0, v1

    return v0
.end method

.method private static a(BBBBI)I
    .locals 6

    const/4 v5, 0x4

    const/4 v0, 0x0

    new-array v3, v5, [B

    and-int/lit8 v1, p2, 0x7f

    int-to-byte v1, v1

    aput-byte v1, v3, v0

    const/4 v1, 0x1

    and-int/lit8 v2, p3, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    const/4 v1, 0x2

    and-int/lit8 v2, p1, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    const/4 v1, 0x3

    and-int/lit8 v2, p0, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    const/16 v1, 0x15

    move v2, v0

    :goto_0
    if-lt v0, v5, :cond_0

    sub-int v0, v2, p4

    return v0

    :cond_0
    aget-byte v4, v3, v0

    shl-int/2addr v4, v1

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, -0x7

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a([BII[S)[B
    .locals 10

    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    if-eqz p4, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    array-length v0, p4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    :cond_0
    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    move v0, v7

    move v1, v7

    :goto_1
    if-lt v1, v6, :cond_3

    move v3, v7

    move v0, v7

    move v1, v7

    :goto_2
    const/16 v4, 0x8

    if-lt v3, v4, :cond_4

    mul-int v3, v1, v0

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    :try_start_0
    rem-int/lit8 v3, v0, 0xa

    move v1, v7

    :goto_3
    iget-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget v0, v0, Lldpsdksub/b/a;->a:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-lt v1, v0, :cond_7

    :try_start_1
    new-array v1, p3, [B
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    iget-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v3, p0, Lldpsdksub/b/c;->e:[S

    const/4 v4, 0x0

    aget-short v3, v3, v4

    iget-object v4, p0, Lldpsdksub/b/c;->e:[S

    const/4 v5, 0x1

    aget-short v4, v4, v5

    invoke-virtual {v0, v3, v4}, Lldpsdksub/b/a;->a(II)B

    move-result v3

    iget-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v4, p0, Lldpsdksub/b/c;->e:[S

    const/4 v5, 0x3

    aget-short v4, v4, v5

    iget-object v5, p0, Lldpsdksub/b/c;->e:[S

    const/4 v6, 0x2

    aget-short v5, v5, v6

    invoke-virtual {v0, v4, v5}, Lldpsdksub/b/a;->a(II)B

    move-result v4

    mul-int v0, v3, v4

    rem-int/lit8 v0, v0, 0x20
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    if-nez v0, :cond_2

    mul-int v0, v3, v4

    :try_start_3
    rem-int/lit8 v0, v0, 0x1f

    :cond_2
    move v3, p2

    move v4, v7

    :goto_4
    add-int v5, p2, p3

    if-lt v3, v5, :cond_9

    const/4 v0, 0x0

    aput-byte v0, v1, v4

    move v0, v7

    move v3, v7

    :goto_5
    aget-byte v4, v1, v0
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7

    if-nez v4, :cond_a

    move-object v0, v1

    goto :goto_0

    :cond_3
    :try_start_4
    aget-short v3, p4, v0

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v3, v3

    shl-int/lit8 v3, v3, 0x4

    int-to-byte v3, v3

    iget-object v4, p0, Lldpsdksub/b/c;->d:[S

    add-int/lit8 v5, v0, 0x1

    aget-short v5, p4, v5

    add-int/lit8 v5, v5, -0x70

    add-int/2addr v3, v5

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    aput-short v3, v4, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lldpsdksub/b/c;->d:[S

    const/4 v5, 0x1

    aget-short v4, v4, v5

    iget-object v5, p0, Lldpsdksub/b/c;->d:[S

    const/4 v6, 0x2

    aget-short v5, v5, v6

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    iget-object v5, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v5, v5, Lldpsdksub/b/a;->e:[I

    aget v5, v5, v3

    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    :cond_5
    iget-object v4, p0, Lldpsdksub/b/c;->d:[S

    const/4 v5, 0x1

    aget-short v4, v4, v5

    iget-object v5, p0, Lldpsdksub/b/c;->d:[S

    const/4 v6, 0x2

    aget-short v5, v5, v6

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    iget-object v5, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v5, v5, Lldpsdksub/b/a;->f:[I

    aget v5, v5, v3

    and-int/2addr v4, v5

    if-eqz v4, :cond_6

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lldpsdksub/b/c;->d:[S

    const/4 v4, 0x1

    aget-short v0, v0, v4

    iget-object v4, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v4, v4, Lldpsdksub/b/a;->b:[B

    aget-byte v4, v4, v1

    add-int/2addr v0, v4

    rem-int/lit8 v0, v0, 0xa

    add-int/2addr v0, v3

    iget-object v4, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v4, v4, Lldpsdksub/b/a;->d:[B

    aget-byte v4, v4, v1

    iget-object v5, p0, Lldpsdksub/b/c;->d:[S

    const/4 v6, 0x2

    aget-short v5, v5, v6

    add-int/2addr v4, v5

    rem-int/lit8 v4, v4, 0xa

    iget-object v5, p0, Lldpsdksub/b/c;->e:[S

    iget-object v6, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget v6, v6, Lldpsdksub/b/a;->a:I

    if-ge v0, v6, :cond_8

    :goto_6
    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    aput-short v0, v5, v4

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    :cond_8
    iget-object v6, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget v6, v6, Lldpsdksub/b/a;->a:I
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    sub-int/2addr v0, v6

    goto :goto_6

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 00 NullPointerException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 00 Exception:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 11 NullPointerException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_3
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 11 Exception:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_4
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 22 NullPointerException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_5
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 22 Exception:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v5, v4, 0x1

    :try_start_5
    aget-byte v6, p1, v3

    sub-int/2addr v6, v0

    shl-int/lit8 v6, v6, 0x4

    add-int/lit8 v8, v3, 0x1

    aget-byte v8, p1, v8

    sub-int/2addr v8, v0

    add-int/2addr v6, v8

    int-to-byte v6, v6

    aput-byte v6, v1, v4

    add-int/lit8 v3, v3, 0x2

    move v4, v5

    goto/16 :goto_4

    :cond_a
    iget-object v4, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget v4, v4, Lldpsdksub/b/a;->a:I

    if-ne v3, v4, :cond_e

    move v6, v7

    :goto_7
    iget-object v3, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v3, v3, Lldpsdksub/b/a;->c:[B

    aget-byte v3, v3, v6

    move v5, v3

    move v3, v0

    :goto_8
    iget-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget v0, v0, Lldpsdksub/b/a;->a:I

    if-lt v5, v0, :cond_c

    :cond_b
    add-int/lit8 v0, v6, 0x1

    move v9, v3

    move v3, v0

    move v0, v9

    goto/16 :goto_5

    :cond_c
    aget-byte v0, v1, v3

    if-eqz v0, :cond_b

    iget-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iget-object v4, p0, Lldpsdksub/b/c;->e:[S

    aget-short v4, v4, v6

    invoke-virtual {v0, v4, v5}, Lldpsdksub/b/a;->a(II)B

    move-result v0

    aget-byte v4, v1, v3

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    shl-int v8, v4, v0

    and-int/lit16 v8, v8, 0xff

    int-to-short v8, v8

    rsub-int/lit8 v0, v0, 0x8

    ushr-int v0, v4, v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    if-gez v8, :cond_d

    and-int/lit16 v0, v0, 0x80

    int-to-short v0, v0

    :cond_d
    add-int/lit8 v4, v3, 0x1

    add-int/2addr v0, v8

    int-to-byte v0, v0

    aput-byte v0, v1, v3
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    add-int/lit8 v0, v5, 0x1

    move v3, v4

    move v5, v0

    goto :goto_8

    :catch_6
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 33 NullPointerException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :catch_7
    move-exception v0

    sget-object v1, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DecodeDyn() 33 Exception:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_0

    :cond_e
    move v6, v3

    goto :goto_7
.end method

.method private b(Lldpsdksub/e/a;[BII)I
    .locals 23

    new-instance v9, Lldpsdksub/b/e;

    invoke-direct {v9}, Lldpsdksub/b/e;-><init>()V

    new-instance v5, Lldpsdksub/b/d;

    invoke-direct {v5}, Lldpsdksub/b/d;-><init>()V

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    array-length v4, v0

    move/from16 v0, p3

    if-ne v4, v0, :cond_0

    if-ltz p4, :cond_0

    move/from16 v0, p4

    move/from16 v1, p3

    if-lt v0, v1, :cond_1

    :cond_0
    const/16 v4, 0xa

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    :try_start_0
    iget v4, v5, Lldpsdksub/b/d;->b:I

    add-int v4, v4, p4

    add-int/lit8 v4, v4, 0x0

    aget-byte v4, p2, v4

    add-int/lit8 v4, v4, -0x80

    shl-int/lit8 v4, v4, 0x18

    const/high16 v7, -0x1000000

    and-int/2addr v4, v7

    int-to-long v10, v4

    iget v4, v5, Lldpsdksub/b/d;->b:I

    add-int v4, v4, p4

    add-int/lit8 v4, v4, 0x1

    aget-byte v4, p2, v4

    add-int/lit8 v4, v4, -0x80

    shl-int/lit8 v4, v4, 0x10

    const/high16 v7, 0xff0000

    and-int/2addr v4, v7

    int-to-long v12, v4

    add-long/2addr v10, v12

    iget v4, v5, Lldpsdksub/b/d;->b:I

    add-int v4, v4, p4

    add-int/lit8 v4, v4, 0x2

    aget-byte v4, p2, v4

    add-int/lit8 v4, v4, -0x80

    shl-int/lit8 v4, v4, 0x8

    const v7, 0xff00

    and-int/2addr v4, v7

    int-to-long v12, v4

    add-long/2addr v10, v12

    iget v4, v5, Lldpsdksub/b/d;->b:I

    add-int v4, v4, p4

    add-int/lit8 v4, v4, 0x3

    aget-byte v4, p2, v4

    add-int/lit8 v4, v4, -0x80

    shl-int/lit8 v4, v4, 0x0

    and-int/lit16 v4, v4, 0xff

    int-to-long v12, v4

    add-long/2addr v10, v12

    const-wide/32 v12, 0x1000001

    cmp-long v4, v10, v12

    if-eqz v4, :cond_2

    const/16 v4, 0x14

    goto :goto_0

    :cond_2
    iget v4, v5, Lldpsdksub/b/d;->c:I

    add-int v4, v4, p4

    add-int/lit8 v4, v4, 0x0

    aget-byte v4, p2, v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    iget v7, v5, Lldpsdksub/b/d;->c:I

    add-int v7, v7, p4

    add-int/lit8 v7, v7, 0x1

    aget-byte v7, p2, v7

    and-int/lit16 v7, v7, 0xff

    int-to-short v7, v7

    iget v8, v5, Lldpsdksub/b/d;->c:I

    add-int v8, v8, p4

    add-int/lit8 v8, v8, 0x2

    aget-byte v8, p2, v8

    and-int/lit16 v8, v8, 0xff

    int-to-short v8, v8

    const/4 v10, 0x4

    new-array v10, v10, [I

    const/4 v11, 0x0

    and-int/lit8 v4, v4, 0x3

    aput v4, v10, v11

    const/4 v4, 0x1

    and-int/lit8 v11, v7, -0x4

    aput v11, v10, v4

    const/4 v4, 0x2

    and-int/lit8 v7, v7, 0x1f

    shl-int/lit8 v7, v7, 0xf

    aput v7, v10, v4

    const/4 v4, 0x3

    and-int/lit8 v7, v8, 0xf

    shl-int/lit8 v7, v7, 0xc

    aput v7, v10, v4

    const/4 v4, 0x0

    aget v4, v10, v4

    const/4 v7, 0x1

    aget v7, v10, v7

    add-int/2addr v4, v7

    const/4 v7, 0x2

    aget v7, v10, v7

    add-int/2addr v4, v7

    const/4 v7, 0x3

    aget v7, v10, v7

    add-int/2addr v4, v7

    const/high16 v7, 0x7f0000

    add-int v10, v4, v7

    iget v4, v5, Lldpsdksub/b/d;->d:I

    add-int v4, v4, p4

    add-int/lit8 v4, v4, 0x0

    aget-byte v4, p2, v4

    iget v7, v5, Lldpsdksub/b/d;->d:I

    add-int v7, v7, p4

    add-int/lit8 v7, v7, 0x1

    aget-byte v7, p2, v7

    invoke-static {v4, v7}, Lldpsdksub/b/c;->a(BB)I

    move-result v11

    if-gtz v11, :cond_3

    const/16 v4, 0x15

    goto/16 :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_1
    const/4 v7, 0x6

    if-lt v4, v7, :cond_4

    move-object/from16 v0, p2

    array-length v4, v0

    iget v7, v5, Lldpsdksub/b/d;->a:I

    sub-int/2addr v4, v7

    sub-int v4, v4, p4

    add-int/lit8 v4, v4, 0xa

    iget v7, v5, Lldpsdksub/b/d;->g:I

    sub-int/2addr v4, v7

    iget v7, v5, Lldpsdksub/b/d;->f:I

    add-int v7, v7, p4

    iget v5, v5, Lldpsdksub/b/d;->a:I

    add-int/2addr v5, v7

    add-int/lit8 v4, v4, -0x50

    add-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    iget-object v7, v0, Lldpsdksub/b/c;->c:[S

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v5, v4, v7}, Lldpsdksub/b/c;->a([BII[S)[B

    move-result-object v12

    if-nez v12, :cond_5

    const/16 v4, 0x16

    goto/16 :goto_0

    :cond_4
    iget v7, v5, Lldpsdksub/b/d;->e:I

    add-int v7, v7, p4

    add-int/2addr v7, v4

    aget-byte v7, p2, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lldpsdksub/b/c;->c:[S

    and-int/lit16 v7, v7, 0xff

    int-to-short v7, v7

    aput-short v7, v8, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    :goto_2
    add-int/lit8 v4, v5, 0x0

    aget-byte v4, v12, v4

    const/4 v7, 0x4

    if-eq v4, v7, :cond_6

    add-int/lit8 v4, v5, 0x0

    aget-byte v4, v12, v4

    const/4 v7, 0x3

    if-ne v4, v7, :cond_7

    :cond_6
    iget v4, v9, Lldpsdksub/b/e;->d:I

    iget v4, v9, Lldpsdksub/b/e;->h:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    aget-byte v4, v12, v4

    iget v7, v9, Lldpsdksub/b/e;->h:I

    add-int/2addr v7, v5

    add-int/lit8 v7, v7, 0x1

    aget-byte v7, v12, v7

    invoke-static {v4, v7}, Lldpsdksub/b/c;->a(BB)I

    move-result v4

    iget v7, v9, Lldpsdksub/b/e;->i:I

    add-int/2addr v5, v7

    add-int/2addr v5, v4

    goto :goto_2

    :cond_7
    const/4 v4, 0x0

    move/from16 v22, v4

    move-object v4, v6

    move v6, v5

    move/from16 v5, v22

    :goto_3
    if-lt v5, v11, :cond_8

    :goto_4
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lldpsdksub/e/a;->a(Z)V

    invoke-static {}, Lldpsdksub/e/a;->c()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_5
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x17

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v7, v6, 0x0

    :try_start_1
    aget-byte v7, v12, v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_9

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_4

    :cond_9
    iget v7, v9, Lldpsdksub/b/e;->a:I

    add-int/2addr v7, v6

    aget-byte v7, v12, v7

    and-int/lit8 v7, v7, 0x7f

    const/4 v8, 0x1

    if-eq v7, v8, :cond_a

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_4

    :cond_a
    iget v7, v9, Lldpsdksub/b/e;->b:I

    add-int/2addr v7, v6

    aget-byte v7, v12, v7

    and-int/lit8 v13, v7, 0x7f

    if-gtz v13, :cond_b

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_4

    :cond_b
    iget v7, v9, Lldpsdksub/b/e;->c:I

    add-int/2addr v7, v6

    aget-byte v7, v12, v7

    and-int/lit8 v7, v7, 0x7f

    const/4 v8, 0x2

    if-eq v7, v8, :cond_c

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_4

    :cond_c
    new-instance v14, Lldpsdksub/g/b;

    invoke-direct {v14}, Lldpsdksub/g/b;-><init>()V

    const/4 v7, 0x0

    move/from16 v22, v7

    move v7, v6

    move/from16 v6, v22

    :goto_6
    if-lt v6, v13, :cond_e

    iget v6, v9, Lldpsdksub/b/e;->d:I

    add-int/2addr v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lldpsdksub/e/a;->a(Lldpsdksub/g/b;)Z

    move-result v7

    if-nez v7, :cond_d

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_e
    new-instance v15, Lldpsdksub/g/a;

    invoke-direct {v15}, Lldpsdksub/g/a;-><init>()V

    iget v8, v9, Lldpsdksub/b/e;->d:I

    iget v8, v9, Lldpsdksub/b/e;->e:I

    add-int/2addr v8, v7

    add-int/lit8 v8, v8, 0x0

    aget-byte v8, v12, v8

    iget v0, v9, Lldpsdksub/b/e;->e:I

    move/from16 v16, v0

    add-int v16, v16, v7

    add-int/lit8 v16, v16, 0x1

    aget-byte v16, v12, v16

    iget v0, v9, Lldpsdksub/b/e;->e:I

    move/from16 v17, v0

    add-int v17, v17, v7

    add-int/lit8 v17, v17, 0x2

    aget-byte v17, v12, v17

    iget v0, v9, Lldpsdksub/b/e;->e:I

    move/from16 v18, v0

    add-int v18, v18, v7

    add-int/lit8 v18, v18, 0x3

    aget-byte v18, v12, v18

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v8, v0, v1, v2, v10}, Lldpsdksub/b/c;->a(BBBBI)I

    move-result v16

    iget v8, v9, Lldpsdksub/b/e;->f:I

    iget v8, v9, Lldpsdksub/b/e;->g:I

    add-int/2addr v8, v7

    add-int/lit8 v8, v8, 0x0

    aget-byte v8, v12, v8

    iget v0, v9, Lldpsdksub/b/e;->g:I

    move/from16 v17, v0

    add-int v17, v17, v7

    add-int/lit8 v17, v17, 0x1

    aget-byte v17, v12, v17

    iget v0, v9, Lldpsdksub/b/e;->g:I

    move/from16 v18, v0

    add-int v18, v18, v7

    add-int/lit8 v18, v18, 0x2

    aget-byte v18, v12, v18

    iget v0, v9, Lldpsdksub/b/e;->g:I

    move/from16 v19, v0

    add-int v19, v19, v7

    add-int/lit8 v19, v19, 0x3

    aget-byte v19, v12, v19

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2, v10}, Lldpsdksub/b/c;->a(BBBBI)I

    move-result v17

    iget v8, v9, Lldpsdksub/b/e;->h:I

    add-int/2addr v8, v7

    aget-byte v8, v12, v8

    and-int/lit8 v8, v8, 0x7f

    mul-int/lit16 v8, v8, 0x80

    iget v0, v9, Lldpsdksub/b/e;->h:I

    move/from16 v18, v0

    add-int v18, v18, v7

    add-int/lit8 v18, v18, 0x1

    aget-byte v18, v12, v18

    and-int/lit8 v18, v18, 0x7f

    add-int v18, v18, v8

    iget v8, v9, Lldpsdksub/b/e;->i:I

    add-int/2addr v8, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lldpsdksub/b/c;->c:[S

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v8, v1, v2}, Lldpsdksub/b/c;->a([BII[S)[B

    move-result-object v8

    if-nez v8, :cond_f

    const/16 v4, 0x17

    goto/16 :goto_0

    :cond_f
    new-instance v19, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>([B)V

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    const/16 v20, 0x0

    aget-byte v8, v8, v20

    const/16 v20, 0x20

    move/from16 v0, v20

    if-ne v8, v0, :cond_10

    const-string v8, " "

    :goto_7
    move/from16 v0, v16

    iput v0, v15, Lldpsdksub/g/a;->a:I

    move/from16 v0, v17

    iput v0, v15, Lldpsdksub/g/a;->b:I

    iput-object v8, v15, Lldpsdksub/g/a;->c:Ljava/lang/String;

    iget v8, v9, Lldpsdksub/b/e;->i:I

    iget v0, v9, Lldpsdksub/b/e;->d:I

    move/from16 v16, v0

    sub-int v8, v8, v16

    add-int/2addr v7, v8

    add-int v7, v7, v18

    invoke-virtual {v14, v15}, Lldpsdksub/g/b;->a(Lldpsdksub/g/a;)Z

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_6

    :cond_10
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    goto :goto_7

    :catch_0
    move-exception v4

    :try_start_2
    sget-object v5, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GetLDPtoUNIBuffer() NullPointerException:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto/16 :goto_5

    :catch_1
    move-exception v4

    sget-object v5, Lldpsdksub/b/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GetLDPtoUNIBuffer() Exception:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    goto/16 :goto_5

    :catchall_0
    move-exception v4

    throw v4

    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lldpsdksub/e/a;[BII)I
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lldpsdksub/b/c;->b(Lldpsdksub/e/a;[BII)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/b/c;->b:Lldpsdksub/b/a;

    iput-object v0, p0, Lldpsdksub/b/c;->c:[S

    iput-object v0, p0, Lldpsdksub/b/c;->d:[S

    iput-object v0, p0, Lldpsdksub/b/c;->e:[S

    return-void
.end method
