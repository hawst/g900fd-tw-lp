.class public Lldpsdksub/b/b;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/b/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/b/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lldpsdksub/e/a;[BII)I
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lldpsdksub/b/b;->b(Lldpsdksub/e/a;[BII)I

    move-result v0

    return v0
.end method

.method private static a(Lldpsdksub/e/a;Ljava/util/ArrayList;[I)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    move v2, v3

    :goto_0
    if-gt v5, v4, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    if-eqz v2, :cond_0

    move v1, v3

    :cond_0
    :goto_1
    return v1

    :cond_1
    new-instance v6, Lldpsdksub/g/c;

    invoke-direct {v6}, Lldpsdksub/g/c;-><init>()V

    aget v7, p2, v4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Lldpsdksub/g/c;->a(ILjava/lang/String;)V

    invoke-virtual {p0, v6}, Lldpsdksub/e/a;->a(Lldpsdksub/g/c;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method private static b(Lldpsdksub/e/a;[BII)I
    .locals 16

    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, p2

    if-ne v2, v0, :cond_0

    if-ltz p3, :cond_0

    move/from16 v0, p3

    move/from16 v1, p2

    if-lt v0, v1, :cond_1

    :cond_0
    const/16 v2, 0xa

    :goto_0
    return v2

    :cond_1
    add-int/lit8 v2, p3, 0x10

    add-int/lit8 v2, v2, 0x10

    :try_start_0
    aget-byte v3, p1, v2

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, p1, v4

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    const/4 v3, 0x1

    if-eq v4, v3, :cond_3

    :cond_2
    const/16 v2, 0x14

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x2

    aget-byte v3, p1, v2

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    add-int/lit8 v5, v2, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    int-to-short v5, v5

    const/4 v6, 0x4

    new-array v6, v6, [I

    const/4 v7, 0x0

    and-int/lit8 v3, v3, 0x1f

    aput v3, v6, v7

    const/4 v3, 0x1

    and-int/lit8 v7, v4, -0x20

    aput v7, v6, v3

    const/4 v3, 0x2

    and-int/lit8 v4, v4, 0x7

    shl-int/lit8 v4, v4, 0x10

    aput v4, v6, v3

    const/4 v3, 0x3

    and-int/lit8 v4, v5, 0x1f

    shl-int/lit8 v4, v4, 0xb

    aput v4, v6, v3

    const/4 v3, 0x0

    aget v3, v6, v3

    const/4 v4, 0x1

    aget v4, v6, v4

    add-int/2addr v3, v4

    const/4 v4, 0x2

    aget v4, v6, v4

    add-int/2addr v3, v4

    const/4 v4, 0x3

    aget v4, v6, v4

    add-int/2addr v3, v4

    const/high16 v4, 0x80000

    add-int v8, v3, v4

    add-int/lit8 v3, v2, 0x3

    aget-byte v3, p1, v3

    mul-int/lit16 v3, v3, 0x100

    add-int/lit8 v2, v2, 0x4

    aget-byte v2, p1, v2

    add-int v9, v3, v2

    add-int/lit8 v3, p3, 0x50

    const/4 v5, 0x0

    new-array v10, v9, [I

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v7, v2

    move v2, v5

    move v5, v3

    :goto_1
    if-gt v9, v7, :cond_4

    move-object/from16 v0, p0

    invoke-static {v0, v11, v10}, Lldpsdksub/b/b;->a(Lldpsdksub/e/a;Ljava/util/ArrayList;[I)Z

    move-result v2

    if-nez v2, :cond_c

    const/16 v2, 0x17

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    aget-byte v4, p1, v5

    const/16 v6, 0x8

    if-ne v4, v6, :cond_5

    const/4 v4, 0x0

    :goto_2
    if-gez v4, :cond_7

    const/16 v2, 0x17

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x7

    if-ne v4, v6, :cond_6

    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    const/4 v4, -0x1

    goto :goto_2

    :cond_7
    const/4 v6, 0x1

    if-ne v4, v6, :cond_8

    const/4 v3, 0x1

    move v6, v3

    :goto_3
    add-int/lit8 v3, v5, 0x1

    add-int/lit8 v4, v3, 0x3

    aget-byte v4, p1, v4

    and-int/lit8 v4, v4, 0x7f

    shl-int/lit8 v4, v4, 0x7

    add-int/lit8 v5, v3, 0x1

    aget-byte v5, p1, v5

    and-int/lit8 v5, v5, 0x7f

    shl-int/lit8 v5, v5, 0xe

    add-int/2addr v4, v5

    add-int/lit8 v5, v3, 0x2

    aget-byte v5, p1, v5

    and-int/lit8 v5, v5, 0x7f

    add-int/2addr v4, v5

    sub-int/2addr v4, v8

    if-nez v6, :cond_9

    sub-int v5, v7, v2

    mul-int/lit8 v4, v4, 0xa

    aput v4, v10, v5

    move v5, v2

    :goto_4
    add-int/lit8 v2, v3, 0x4

    aget-byte v12, p1, v2

    add-int/lit8 v3, v2, 0x1

    new-array v13, v12, [B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x0

    add-int v14, v12, v3

    move v4, v2

    move v2, v3

    :goto_5
    if-gt v14, v2, :cond_a

    :try_start_1
    new-instance v2, Ljava/lang/String;

    const-string v4, "UTF-16LE"

    invoke-direct {v2, v13, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    if-nez v6, :cond_b

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :goto_6
    add-int/2addr v3, v12

    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v2, v5

    move v5, v3

    goto :goto_1

    :cond_8
    if-eqz v4, :cond_d

    const/16 v2, 0x17

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v2, v2, 0x1

    move v5, v2

    goto :goto_4

    :cond_a
    :try_start_2
    aget-byte v15, p1, v2

    aput-byte v15, v13, v4
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    :try_start_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v13, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v4, v6, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v4, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_6

    :catch_0
    move-exception v2

    :try_start_4
    sget-object v3, Lldpsdksub/b/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLDBtoUNIBuffer() 00 NullPointerException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/16 v2, 0x17

    goto/16 :goto_0

    :catch_1
    move-exception v2

    sget-object v3, Lldpsdksub/b/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLDBtoUNIBuffer() 00 UnsupportedEncodingException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/16 v2, 0x17

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lldpsdksub/e/a;->a(Z)V

    invoke-static {}, Lldpsdksub/e/a;->c()V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    const/4 v2, 0x0

    goto/16 :goto_0

    :catch_2
    move-exception v2

    sget-object v3, Lldpsdksub/b/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLDBtoUNIBuffer() 11 NullPointerException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/16 v2, 0x17

    goto/16 :goto_0

    :catch_3
    move-exception v2

    sget-object v3, Lldpsdksub/b/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetLDBtoUNIBuffer() Exception:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/16 v2, 0x17

    goto/16 :goto_0

    :cond_d
    move v6, v3

    goto/16 :goto_3
.end method
