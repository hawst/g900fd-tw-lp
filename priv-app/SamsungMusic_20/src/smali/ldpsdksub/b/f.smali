.class public Lldpsdksub/b/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/b/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/b/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method private b(Lldpsdksub/g/b;I)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0xa

    if-lt p2, v1, :cond_0

    new-instance v3, Lldpsdksub/g/b;

    invoke-direct {v3}, Lldpsdksub/g/b;-><init>()V

    move v1, v0

    move v2, v0

    :goto_1
    :try_start_0
    invoke-virtual {p1}, Lldpsdksub/g/b;->c()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-virtual {p1, v1}, Lldpsdksub/g/b;->a(I)Lldpsdksub/g/a;

    move-result-object v4

    iget v5, v4, Lldpsdksub/g/a;->d:I

    add-int/2addr v5, v2

    if-lt v5, p2, :cond_3

    new-instance v2, Lldpsdksub/g/b;

    invoke-direct {v2}, Lldpsdksub/g/b;-><init>()V

    :goto_2
    invoke-virtual {p1}, Lldpsdksub/g/b;->c()I

    move-result v4

    if-lt v1, v4, :cond_2

    iget-object v1, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v1}, Lldpsdksub/g/b;->a(I)Lldpsdksub/g/a;

    move-result-object v4

    invoke-virtual {v2, v4}, Lldpsdksub/g/b;->a(Lldpsdksub/g/a;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget v5, v4, Lldpsdksub/g/a;->d:I

    add-int/2addr v2, v5

    invoke-virtual {v3, v4}, Lldpsdksub/g/b;->a(Lldpsdksub/g/a;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v2, Lldpsdksub/b/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LineSplit() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lldpsdksub/b/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LineSplit() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method private d()Lldpsdksub/g/b;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    if-gtz v0, :cond_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldpsdksub/g/b;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v2, Lldpsdksub/b/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetRemoveHead() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_2
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lldpsdksub/b/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetRemoveHead() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method public final a(Lldpsdksub/g/b;I)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lldpsdksub/b/f;->b(Lldpsdksub/g/b;I)Z

    move-result v0

    return v0
.end method

.method public final b()Lldpsdksub/g/b;
    .locals 1

    invoke-direct {p0}, Lldpsdksub/b/f;->d()Lldpsdksub/g/b;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/b/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method
