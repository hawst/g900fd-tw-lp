.class public final Lldpsdksub/c/b;
.super Ljava/lang/Thread;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Lldpsdksub/g/d;

.field private d:I

.field private e:I

.field private f:I

.field private final g:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lldpsdksub/g/d;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v1, p0, Lldpsdksub/c/b;->a:Z

    iput-boolean v1, p0, Lldpsdksub/c/b;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/c/b;->c:Lldpsdksub/g/d;

    const/16 v0, 0x14

    iput v0, p0, Lldpsdksub/c/b;->d:I

    iput v1, p0, Lldpsdksub/c/b;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lldpsdksub/c/b;->f:I

    new-instance v0, Lldpsdksub/c/c;

    invoke-direct {v0, p0}, Lldpsdksub/c/c;-><init>(Lldpsdksub/c/b;)V

    iput-object v0, p0, Lldpsdksub/c/b;->g:Landroid/os/Handler;

    iput-object p1, p0, Lldpsdksub/c/b;->c:Lldpsdksub/g/d;

    return-void
.end method

.method static synthetic a(Lldpsdksub/c/b;)Z
    .locals 1

    iget-boolean v0, p0, Lldpsdksub/c/b;->a:Z

    return v0
.end method

.method static synthetic b(Lldpsdksub/c/b;)Z
    .locals 1

    iget-boolean v0, p0, Lldpsdksub/c/b;->b:Z

    return v0
.end method

.method static synthetic c(Lldpsdksub/c/b;)Lldpsdksub/g/d;
    .locals 1

    iget-object v0, p0, Lldpsdksub/c/b;->c:Lldpsdksub/g/d;

    return-object v0
.end method

.method static synthetic d(Lldpsdksub/c/b;)I
    .locals 1

    iget v0, p0, Lldpsdksub/c/b;->e:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lldpsdksub/c/b;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lldpsdksub/c/b;->b:Z

    if-eqz v0, :cond_0

    iput p1, p0, Lldpsdksub/c/b;->e:I

    iget-object v0, p0, Lldpsdksub/c/b;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lldpsdksub/c/b;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lldpsdksub/c/b;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lldpsdksub/c/b;->f:I

    iput-boolean v2, p0, Lldpsdksub/c/b;->b:Z

    iget-object v0, p0, Lldpsdksub/c/b;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean p1, p0, Lldpsdksub/c/b;->a:Z

    iget-boolean v0, p0, Lldpsdksub/c/b;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lldpsdksub/c/b;->g:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lldpsdksub/c/b;->a:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lldpsdksub/c/b;->b:Z

    return-void
.end method

.method public final run()V
    .locals 2

    :goto_0
    iget-boolean v0, p0, Lldpsdksub/c/b;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lldpsdksub/c/b;->b:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lldpsdksub/c/b;->c:Lldpsdksub/g/d;

    invoke-virtual {v0}, Lldpsdksub/g/d;->d()I

    move-result v0

    iget v1, p0, Lldpsdksub/c/b;->f:I

    if-eq v1, v0, :cond_1

    iput v0, p0, Lldpsdksub/c/b;->f:I

    iget v0, p0, Lldpsdksub/c/b;->f:I

    iput v0, p0, Lldpsdksub/c/b;->e:I

    :cond_1
    iget-object v0, p0, Lldpsdksub/c/b;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget v0, p0, Lldpsdksub/c/b;->e:I

    iget v1, p0, Lldpsdksub/c/b;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lldpsdksub/c/b;->e:I

    :cond_2
    iget v0, p0, Lldpsdksub/c/b;->d:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lldpsdksub/c/b;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
