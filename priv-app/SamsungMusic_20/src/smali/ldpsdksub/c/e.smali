.class public Lldpsdksub/c/e;
.super Lldpsdksub/c/d;


# static fields
.field private static q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/c/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/c/e;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lldpsdksub/c/d;-><init>()V

    return-void
.end method

.method private b(IIILldpsdksub/g/f;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    iget-object v1, p0, Lldpsdksub/c/e;->p:Lldpsdksub/c/a;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lldpsdksub/c/e;->p:Lldpsdksub/c/a;

    invoke-interface {v1, p1, p2, p3}, Lldpsdksub/c/a;->SetLineNumDataForLDPView(III)V

    sub-int v1, p3, p1

    :goto_0
    add-int v2, v0, p1

    if-gt p2, v2, :cond_1

    :cond_0
    :goto_1
    const/4 v0, 0x1

    return v0

    :cond_1
    add-int v2, v0, p1

    invoke-virtual {p4, v2}, Lldpsdksub/g/f;->b(I)Lldpsdksub/g/b;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lldpsdksub/g/b;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    if-ne v1, v0, :cond_2

    iget-object v4, p0, Lldpsdksub/c/e;->l:Landroid/graphics/Canvas;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lldpsdksub/c/e;->m:Landroid/graphics/Paint;

    if-eqz v4, :cond_2

    iget-object v0, p0, Lldpsdksub/c/e;->l:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lldpsdksub/c/e;->l:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Lldpsdksub/g/b;->g()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lldpsdksub/c/d;->j:I

    int-to-float v2, v2

    iget-object v4, p0, Lldpsdksub/c/e;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/c/e;->q:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InitDrawLine() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/c/e;->q:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InitDrawLine() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a(III)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput p3, v0, Landroid/graphics/Rect;->left:I

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iput v4, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lldpsdksub/c/d;->i:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput p3, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, p1

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lldpsdksub/c/d;->g:I

    iget v3, p0, Lldpsdksub/c/d;->h:I

    mul-int/2addr v3, p2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lldpsdksub/c/d;->i:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lldpsdksub/c/e;->p:Lldpsdksub/c/a;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lldpsdksub/c/e;->p:Lldpsdksub/c/a;

    iget-object v3, p0, Lldpsdksub/c/e;->k:Landroid/graphics/Bitmap;

    invoke-interface {v2, v3, v0, v1, v4}, Lldpsdksub/c/a;->DrawWaveForLDP(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    :cond_0
    return-void
.end method

.method public final a(IIILldpsdksub/g/f;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lldpsdksub/c/e;->b(IIILldpsdksub/g/f;)Z

    move-result v0

    return v0
.end method

.method public final a(IIILldpsdksub/g/g;)Z
    .locals 1

    if-eqz p4, :cond_0

    iget-object v0, p0, Lldpsdksub/c/e;->p:Lldpsdksub/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/c/e;->p:Lldpsdksub/c/a;

    invoke-interface {v0, p1, p2, p3}, Lldpsdksub/c/a;->SetLineNumDataForLDBView(III)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
