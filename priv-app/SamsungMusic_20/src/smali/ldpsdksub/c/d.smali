.class public Lldpsdksub/c/d;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:Lldpsdksub/a/a;

.field public e:I

.field public f:I

.field g:I

.field public h:I

.field i:I

.field j:I

.field protected k:Landroid/graphics/Bitmap;

.field protected l:Landroid/graphics/Canvas;

.field protected m:Landroid/graphics/Paint;

.field public n:Landroid/graphics/Paint;

.field public o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

.field protected p:Lldpsdksub/c/a;

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lldpsdksub/c/d;->a:I

    iput v0, p0, Lldpsdksub/c/d;->b:I

    iput-boolean v0, p0, Lldpsdksub/c/d;->c:Z

    iput-object v1, p0, Lldpsdksub/c/d;->d:Lldpsdksub/a/a;

    iput v0, p0, Lldpsdksub/c/d;->e:I

    iput v0, p0, Lldpsdksub/c/d;->f:I

    iput v0, p0, Lldpsdksub/c/d;->q:I

    iput v0, p0, Lldpsdksub/c/d;->g:I

    iput v0, p0, Lldpsdksub/c/d;->h:I

    iput v0, p0, Lldpsdksub/c/d;->i:I

    iput v0, p0, Lldpsdksub/c/d;->j:I

    iput-object v1, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lldpsdksub/c/d;->l:Landroid/graphics/Canvas;

    iput-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    iput-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    iput-object v1, p0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iput-object v1, p0, Lldpsdksub/c/d;->p:Lldpsdksub/c/a;

    return-void
.end method

.method private d()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object v3, p0, Lldpsdksub/c/d;->l:Landroid/graphics/Canvas;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DestroyBitmap() m_bmpWave Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iput-object v3, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lldpsdksub/c/d;->d()V

    iput-object v0, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    iput-object v0, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    return-void
.end method

.method public final a(I)V
    .locals 0

    if-lez p1, :cond_0

    iput p1, p0, Lldpsdksub/c/d;->a:I

    :cond_0
    return-void
.end method

.method public a(III)V
    .locals 0

    return-void
.end method

.method public final a(Lldpsdksub/c/a;)V
    .locals 0

    iput-object p1, p0, Lldpsdksub/c/d;->p:Lldpsdksub/c/a;

    return-void
.end method

.method public final a(IIILandroid/graphics/Typeface;)Z
    .locals 6

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0}, Lldpsdksub/c/d;->d()V

    iget-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    :cond_0
    iget-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    if-nez v1, :cond_1

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    :cond_1
    iget-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    ushr-int/lit8 v2, p1, 0x18

    shr-int/lit8 v3, p1, 0x10

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    and-int/lit16 v5, p1, 0xff

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    ushr-int/lit8 v2, p2, 0x18

    shr-int/lit8 v3, p2, 0x10

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v4, p2, 0x8

    and-int/lit16 v4, v4, 0xff

    and-int/lit16 v5, p2, 0xff

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    int-to-float v2, p3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    int-to-float v2, p3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    if-eqz p4, :cond_2

    iget-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    :cond_2
    iget-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    iget v1, v2, Landroid/graphics/Paint$FontMetrics;->ascent:F

    const/4 v3, 0x0

    cmpl-float v3, v3, v1

    if-lez v3, :cond_3

    const/high16 v3, -0x40800000    # -1.0f

    mul-float/2addr v1, v3

    :cond_3
    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, p0, Lldpsdksub/c/d;->i:I

    float-to-int v1, v1

    iput v1, p0, Lldpsdksub/c/d;->j:I

    iget-object v1, p0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget-object v2, p0, Lldpsdksub/c/d;->n:Landroid/graphics/Paint;

    iget-object v3, p0, Lldpsdksub/c/d;->m:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetDrawDataPaint(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IIILldpsdksub/g/f;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(IIILldpsdksub/g/g;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(ILandroid/graphics/Bitmap$Config;)Z
    .locals 7

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InitDrawerCanvas() 000 m_nScreenWidth:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lldpsdksub/c/d;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InitDrawerCanvas() 000 m_nScreenHeight:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lldpsdksub/c/d;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InitDrawerCanvas() 000 nLineTotalCount:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :try_start_0
    iget v1, p0, Lldpsdksub/c/d;->a:I

    iget v2, p0, Lldpsdksub/c/d;->b:I

    if-lez v1, :cond_0

    if-gtz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v3, p0, Lldpsdksub/c/d;->f:I

    iget v4, p0, Lldpsdksub/c/d;->i:I

    iget v5, p0, Lldpsdksub/c/d;->j:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    if-le v3, v2, :cond_2

    iget v3, p0, Lldpsdksub/c/d;->i:I

    iget v4, p0, Lldpsdksub/c/d;->j:I

    add-int/2addr v3, v4

    div-int v3, v2, v3

    iput v3, p0, Lldpsdksub/c/d;->f:I

    :cond_2
    iget v3, p0, Lldpsdksub/c/d;->f:I

    div-int/2addr v2, v3

    iput v2, p0, Lldpsdksub/c/d;->h:I

    iget v2, p0, Lldpsdksub/c/d;->h:I

    iget v3, p0, Lldpsdksub/c/d;->i:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lldpsdksub/c/d;->g:I

    iget v2, p0, Lldpsdksub/c/d;->g:I

    iget v3, p0, Lldpsdksub/c/d;->j:I

    add-int/2addr v2, v3

    iput v2, p0, Lldpsdksub/c/d;->q:I

    iget-boolean v2, p0, Lldpsdksub/c/d;->c:Z

    if-eqz v2, :cond_5

    iget v2, p0, Lldpsdksub/c/d;->h:I

    mul-int/2addr v2, p1

    iput p1, p0, Lldpsdksub/c/d;->e:I

    iget-object v3, p0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget v4, p0, Lldpsdksub/c/d;->a:I

    invoke-virtual {v3, v4, v2}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetResizeValue(II)V

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitDrawerCanvas() 000 m_nOneLineHeight:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lldpsdksub/c/d;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitDrawerCanvas() 000 m_nDrawYposForText:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lldpsdksub/c/d;->q:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitDrawerCanvas() 000 m_nLineCountToScreen:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lldpsdksub/c/d;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v2, p0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    iget v3, p0, Lldpsdksub/c/d;->j:I

    iget v4, p0, Lldpsdksub/c/d;->q:I

    iget v5, p0, Lldpsdksub/c/d;->h:I

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetDrawDataYpos(IIIZ)V

    iget-object v2, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    if-nez v2, :cond_3

    iget v2, p0, Lldpsdksub/c/d;->i:I

    invoke-static {v1, v2, p2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    :cond_3
    iget-object v1, p0, Lldpsdksub/c/d;->l:Landroid/graphics/Canvas;

    if-nez v1, :cond_4

    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lldpsdksub/c/d;->k:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lldpsdksub/c/d;->l:Landroid/graphics/Canvas;

    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    iget v2, p0, Lldpsdksub/c/d;->f:I

    iput v2, p0, Lldpsdksub/c/d;->e:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InitDrawerCanvas() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lldpsdksub/c/d;->l:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/c/d;->l:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    if-lez p1, :cond_0

    iput p1, p0, Lldpsdksub/c/d;->b:I

    iget v0, p0, Lldpsdksub/c/d;->a:I

    iget v1, p0, Lldpsdksub/c/d;->b:I

    iget-object v2, p0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lldpsdksub/c/d;->o:Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;

    invoke-virtual {v2, v0, v1}, Lcom/mediacanvas/AndroidLDP/View/MC_LDPView;->SetScreenArea(II)V

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lldpsdksub/c/d;->d:Lldpsdksub/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lldpsdksub/c/d;->d:Lldpsdksub/a/a;

    iget v0, v0, Lldpsdksub/a/a;->a:I

    goto :goto_0
.end method
