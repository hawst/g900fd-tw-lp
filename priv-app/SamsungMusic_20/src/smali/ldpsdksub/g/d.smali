.class public Lldpsdksub/g/d;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

.field private c:Lldpsdksub/e/a;

.field private d:Lldpsdksub/c/b;

.field private e:Lldpsdksub/g/e;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/g/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/g/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lldpsdksub/g/d;->b:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    iput-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    iput-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    iput-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    const/4 v0, 0x0

    iput v0, p0, Lldpsdksub/g/d;->f:I

    new-instance v0, Lldpsdksub/g/e;

    invoke-direct {v0, p0}, Lldpsdksub/g/e;-><init>(Lldpsdksub/g/d;)V

    iput-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    return-void
.end method

.method private static c([BI)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v1, p1, -0x4

    if-lt v0, v1, :cond_1

    :goto_1
    const/4 v0, -0x1

    :cond_0
    return v0

    :cond_1
    :try_start_0
    aget-byte v1, p0, v0

    const/16 v2, 0x58

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p0, v1

    const/16 v2, 0x4c

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p0, v1

    const/16 v2, 0x44

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x3

    aget-byte v1, p0, v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v2, 0x42

    if-eq v1, v2, :cond_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetXLDBPos() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetXLDBPos() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private static d([BI)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v1, p1, -0x4

    if-lt v0, v1, :cond_1

    :goto_1
    const/4 v0, -0x1

    :cond_0
    return v0

    :cond_1
    :try_start_0
    aget-byte v1, p0, v0

    const/16 v2, 0x55

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p0, v1

    const/16 v2, 0x53

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p0, v1

    const/16 v2, 0x4c

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x3

    aget-byte v1, p0, v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v2, 0x54

    if-eq v1, v2, :cond_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetUSLTPos() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetUSLTPos() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a([BI)I
    .locals 3

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    iget-object v1, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-eqz v1, :cond_0

    invoke-static {p1, p2}, Lldpsdksub/g/d;->c([BI)I

    move-result v1

    if-ltz v1, :cond_0

    new-instance v2, Lldpsdksub/b/c;

    invoke-direct {v2}, Lldpsdksub/b/c;-><init>()V

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v2, v0, p1, p2, v1}, Lldpsdksub/b/c;->a(Lldpsdksub/e/a;[BII)I

    move-result v0

    invoke-virtual {v2}, Lldpsdksub/b/c;->a()V

    :cond_0
    return v0
.end method

.method public final a()V
    .locals 2

    new-instance v0, Lldpsdksub/e/b;

    new-instance v1, Lldpsdksub/c/e;

    invoke-direct {v1}, Lldpsdksub/c/e;-><init>()V

    invoke-direct {v0, v1}, Lldpsdksub/e/b;-><init>(Lldpsdksub/c/d;)V

    iput-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    return-void
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Lldpsdksub/g/d;->f:I

    return-void
.end method

.method public final a(Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;)V
    .locals 0

    iput-object p1, p0, Lldpsdksub/g/d;->b:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    return-void
.end method

.method public final a(Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;)V
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0, p1}, Lldpsdksub/e/a;->a(Lcom/mediacanvas/AndroidLDP/Mgr/LineChangeListener;)V

    return-void
.end method

.method public final a(Lldpsdksub/c/a;)V
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0, p1}, Lldpsdksub/e/a;->a(Lldpsdksub/c/a;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    invoke-virtual {v0, p1}, Lldpsdksub/c/b;->b(Z)V

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    invoke-virtual {v0, v1}, Lldpsdksub/g/e;->removeMessages(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    invoke-virtual {v0, v1}, Lldpsdksub/g/e;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final a(IIILandroid/graphics/Typeface;)Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lldpsdksub/e/a;->a(IIILandroid/graphics/Typeface;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/graphics/Bitmap$Config;)Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0, p1}, Lldpsdksub/e/a;->a(Landroid/graphics/Bitmap$Config;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/View;IIIIZ)Z
    .locals 7

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lldpsdksub/e/a;->a(Landroid/view/View;IIIIZ)Z

    move-result v0

    return v0
.end method

.method public final b([BI)I
    .locals 2

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    iget-object v1, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-eqz v1, :cond_0

    invoke-static {p1, p2}, Lldpsdksub/g/d;->d([BI)I

    move-result v1

    if-ltz v1, :cond_0

    new-instance v0, Lldpsdksub/b/b;

    invoke-direct {v0}, Lldpsdksub/b/b;-><init>()V

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-static {v0, p1, p2, v1}, Lldpsdksub/b/b;->a(Lldpsdksub/e/a;[BII)I

    move-result v0

    :cond_0
    return v0
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0}, Lldpsdksub/e/a;->a()V

    iput-object v1, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    :cond_0
    invoke-virtual {p0}, Lldpsdksub/g/d;->f()V

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    if-eqz v0, :cond_1

    iput-object v1, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    :cond_1
    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    if-eqz v0, :cond_2

    iput-object v1, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    :cond_2
    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    invoke-virtual {v0, p1}, Lldpsdksub/c/b;->a(I)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0}, Lldpsdksub/e/a;->b()V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lldpsdksub/g/d;->f:I

    return-void
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0, p1}, Lldpsdksub/e/a;->b(I)V

    :cond_0
    return-void
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lldpsdksub/g/d;->f:I

    return v0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lldpsdksub/g/e;->sendEmptyMessage(I)Z

    :cond_0
    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    if-nez v0, :cond_1

    new-instance v0, Lldpsdksub/c/b;

    invoke-direct {v0, p0}, Lldpsdksub/c/b;-><init>(Lldpsdksub/g/d;)V

    iput-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lldpsdksub/c/b;->a(Z)V

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    invoke-virtual {v0}, Lldpsdksub/c/b;->start()V

    :cond_1
    return-void
.end method

.method public final f()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/d;->e:Lldpsdksub/g/e;

    invoke-virtual {v0, v1}, Lldpsdksub/g/e;->removeMessages(I)V

    :cond_0
    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    invoke-virtual {v0}, Lldpsdksub/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    invoke-virtual {v0, v1}, Lldpsdksub/c/b;->a(Z)V

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    :cond_1
    return-void

    :cond_2
    :try_start_0
    iget-object v2, p0, Lldpsdksub/g/d;->d:Lldpsdksub/c/b;

    invoke-virtual {v2}, Lldpsdksub/c/b;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0}, Lldpsdksub/e/a;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->b:Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;

    invoke-interface {v0}, Lcom/mediacanvas/AndroidLDP/Mgr/LDPListener;->GetCurrentTime()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lldpsdksub/g/d;->c:Lldpsdksub/e/a;

    invoke-virtual {v0}, Lldpsdksub/e/a;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
