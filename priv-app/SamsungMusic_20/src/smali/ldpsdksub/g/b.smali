.class public Lldpsdksub/g/b;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/ArrayList;

.field private c:Ljava/lang/StringBuffer;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lldpsdksub/g/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lldpsdksub/g/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    iput-object v1, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    iput v0, p0, Lldpsdksub/g/b;->d:I

    iput v0, p0, Lldpsdksub/g/b;->e:I

    iput v0, p0, Lldpsdksub/g/b;->f:I

    iput v0, p0, Lldpsdksub/g/b;->g:I

    iput v0, p0, Lldpsdksub/g/b;->h:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    return-void
.end method

.method private b(II)I
    .locals 7

    const/4 v2, -0x1

    const/high16 v6, 0x428c0000    # 70.0f

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    move p2, v2

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget-object v1, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    move p2, v0

    goto :goto_0

    :cond_2
    iget v1, p0, Lldpsdksub/g/b;->h:I

    if-gtz v1, :cond_3

    move p2, v0

    goto :goto_0

    :cond_3
    if-le v1, p1, :cond_0

    move v3, v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    :goto_2
    move p2, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldpsdksub/g/a;

    if-eqz v0, :cond_9

    iget v4, v0, Lldpsdksub/g/a;->a:I

    if-eqz v4, :cond_8

    iget v4, v0, Lldpsdksub/g/a;->b:I

    if-eqz v4, :cond_8

    iget v4, v0, Lldpsdksub/g/a;->a:I

    if-gt v4, p1, :cond_6

    iget v4, v0, Lldpsdksub/g/a;->b:I

    if-ge p1, v4, :cond_6

    iget v3, v0, Lldpsdksub/g/a;->b:I

    iget v4, v0, Lldpsdksub/g/a;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v4, p1

    iget v5, v0, Lldpsdksub/g/a;->a:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    div-float v3, v4, v3

    mul-float/2addr v3, v6

    iget v0, v0, Lldpsdksub/g/a;->d:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    div-float/2addr v0, v6

    float-to-int v0, v0

    add-int/2addr v0, v1

    const/16 v1, 0xf

    sub-int v2, p2, v0

    if-lt v1, v2, :cond_5

    add-int/lit8 v0, p2, 0x2

    :cond_5
    add-int/lit8 p2, v0, 0x2

    goto :goto_0

    :cond_6
    iget v4, v0, Lldpsdksub/g/a;->a:I

    if-ge p1, v4, :cond_7

    add-int/lit8 p2, v1, 0x2

    goto :goto_0

    :cond_7
    iget v0, v0, Lldpsdksub/g/a;->d:I

    add-int/2addr v0, v1

    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_8
    iget v0, v0, Lldpsdksub/g/a;->d:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    add-int/2addr v0, v1

    goto :goto_3

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "GetCurrentWavePos() NullPointerException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_2

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "GetCurrentWavePos() Exception:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3
.end method

.method private b(Landroid/graphics/Paint;II)I
    .locals 4

    const/4 v0, 0x0

    iput v0, p0, Lldpsdksub/g/b;->e:I

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    iget v0, p0, Lldpsdksub/g/b;->e:I

    invoke-static {p2, v0, p3}, Lldpsdksub/a/a;->a(III)I

    move-result v0

    iput v0, p0, Lldpsdksub/g/b;->f:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget v0, p0, Lldpsdksub/g/b;->e:I

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldpsdksub/g/a;

    if-eqz v0, :cond_2

    iget-object v2, v0, Lldpsdksub/g/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, v0, Lldpsdksub/g/a;->d:I

    iget v3, p0, Lldpsdksub/g/b;->e:I

    add-int/2addr v2, v3

    iput v2, p0, Lldpsdksub/g/b;->e:I

    iget v2, v0, Lldpsdksub/g/a;->a:I

    if-eqz v2, :cond_1

    iget v2, v0, Lldpsdksub/g/a;->b:I

    if-eqz v2, :cond_1

    iget v2, p0, Lldpsdksub/g/b;->g:I

    if-nez v2, :cond_1

    iget v2, v0, Lldpsdksub/g/a;->a:I

    iput v2, p0, Lldpsdksub/g/b;->g:I

    :cond_1
    iget v2, v0, Lldpsdksub/g/a;->a:I

    if-eqz v2, :cond_2

    iget v2, v0, Lldpsdksub/g/a;->b:I

    if-eqz v2, :cond_2

    iget v0, v0, Lldpsdksub/g/a;->b:I

    iput v0, p0, Lldpsdksub/g/b;->h:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetAt() InitWordWidth:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetAt() InitWordWidth:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private b(Lldpsdksub/g/a;)Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    iget-object v1, p1, Lldpsdksub/g/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppendWordData() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppendWordData() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private c(I)Lldpsdksub/g/a;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    if-gez p1, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldpsdksub/g/a;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetAt() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_1
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v2, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetAt() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method private j()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    iget-object v3, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    move v2, v1

    :goto_0
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v3, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldpsdksub/g/a;

    iget-object v0, v0, Lldpsdksub/g/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ResetLineWord() NullPointerException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ResetLineWord() Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(II)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lldpsdksub/g/b;->b(II)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/graphics/Paint;II)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lldpsdksub/g/b;->b(Landroid/graphics/Paint;II)I

    move-result v0

    return v0
.end method

.method public final a(I)Lldpsdksub/g/a;
    .locals 1

    invoke-direct {p0, p1}, Lldpsdksub/g/b;->c(I)Lldpsdksub/g/a;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    iget-object v3, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    :cond_0
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v1, v2

    :goto_0
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :cond_1
    :goto_1
    iput v2, p0, Lldpsdksub/g/b;->d:I

    iput v2, p0, Lldpsdksub/g/b;->e:I

    iput v2, p0, Lldpsdksub/g/b;->f:I

    iput v2, p0, Lldpsdksub/g/b;->g:I

    iput v2, p0, Lldpsdksub/g/b;->h:I

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldpsdksub/g/a;

    if-eqz v0, :cond_3

    const/4 v3, 0x0

    iput v3, v0, Lldpsdksub/g/a;->a:I

    const/4 v3, 0x0

    iput v3, v0, Lldpsdksub/g/a;->b:I

    const-string v3, ""

    iput-object v3, v0, Lldpsdksub/g/a;->c:Ljava/lang/String;

    const/4 v3, 0x0

    iput v3, v0, Lldpsdksub/g/a;->d:I
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DestroyWordData() StringIndexOutOfBoundsException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringIndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DestroyWordData() NullPointerException:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :catch_2
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DestroyWordData() Exception:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lldpsdksub/g/a;)Z
    .locals 1

    invoke-direct {p0, p1}, Lldpsdksub/g/b;->b(Lldpsdksub/g/a;)Z

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Lldpsdksub/g/b;->d:I

    return-void
.end method

.method public final b()Z
    .locals 1

    invoke-direct {p0}, Lldpsdksub/g/b;->j()Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 3

    :try_start_0
    iget-object v0, p0, Lldpsdksub/g/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetCount() NullPointerException:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_1
    const/4 v0, -0x1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lldpsdksub/g/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetCount() Exception:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lldpsdksub/g/b;->d:I

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lldpsdksub/g/b;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lldpsdksub/g/b;->e:I

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lldpsdksub/g/b;->f:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lldpsdksub/g/b;->g:I

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lldpsdksub/g/b;->h:I

    return v0
.end method
