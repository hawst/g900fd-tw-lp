.class public final Lldpsdksub/d/e;
.super Ljava/lang/Object;


# instance fields
.field a:J


# direct methods
.method public constructor <init>(J)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lldpsdksub/d/e;->a:J

    iput-wide p1, p0, Lldpsdksub/d/e;->a:J

    return-void
.end method

.method private a(I)J
    .locals 6

    const-wide v4, 0xffffffffL

    iget-wide v0, p0, Lldpsdksub/d/e;->a:J

    shl-long/2addr v0, p1

    and-long/2addr v0, v4

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    and-long/2addr v2, v4

    rsub-int/lit8 v4, p1, 0x20

    ushr-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V
    .locals 12

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v4, p1, Lldpsdksub/d/e;->a:J

    iget-wide v6, p2, Lldpsdksub/d/e;->a:J

    iget-wide v8, p3, Lldpsdksub/d/e;->a:J

    and-long/2addr v6, v4

    const-wide/16 v10, -0x1

    xor-long/2addr v4, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v4, v10

    and-long/2addr v4, v8

    or-long/2addr v4, v6

    add-long v4, v4, p4

    add-long v4, v4, p8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    move-wide/from16 v0, p6

    long-to-int v2, v0

    invoke-direct {p0, v2}, Lldpsdksub/d/e;->a(I)J

    move-result-wide v2

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v4, p1, Lldpsdksub/d/e;->a:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    return-void
.end method

.method public final b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V
    .locals 12

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v4, p1, Lldpsdksub/d/e;->a:J

    iget-wide v6, p2, Lldpsdksub/d/e;->a:J

    iget-wide v8, p3, Lldpsdksub/d/e;->a:J

    and-long/2addr v4, v8

    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    add-long v4, v4, p4

    add-long v4, v4, p8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    move-wide/from16 v0, p6

    long-to-int v2, v0

    invoke-direct {p0, v2}, Lldpsdksub/d/e;->a(I)J

    move-result-wide v2

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v4, p1, Lldpsdksub/d/e;->a:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    return-void
.end method

.method public final c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V
    .locals 8

    iget-wide v0, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p1, Lldpsdksub/d/e;->a:J

    iget-wide v4, p2, Lldpsdksub/d/e;->a:J

    iget-wide v6, p3, Lldpsdksub/d/e;->a:J

    xor-long/2addr v2, v4

    xor-long/2addr v2, v6

    add-long/2addr v2, p4

    add-long v2, v2, p8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lldpsdksub/d/e;->a:J

    iget-wide v0, p0, Lldpsdksub/d/e;->a:J

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    iput-wide v0, p0, Lldpsdksub/d/e;->a:J

    long-to-int v0, p6

    invoke-direct {p0, v0}, Lldpsdksub/d/e;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, Lldpsdksub/d/e;->a:J

    iget-wide v0, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p1, Lldpsdksub/d/e;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lldpsdksub/d/e;->a:J

    iget-wide v0, p0, Lldpsdksub/d/e;->a:J

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    iput-wide v0, p0, Lldpsdksub/d/e;->a:J

    return-void
.end method

.method public final d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V
    .locals 12

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v4, p1, Lldpsdksub/d/e;->a:J

    iget-wide v6, p2, Lldpsdksub/d/e;->a:J

    iget-wide v8, p3, Lldpsdksub/d/e;->a:J

    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    or-long/2addr v4, v8

    xor-long/2addr v4, v6

    add-long v4, v4, p4

    add-long v4, v4, p8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    move-wide/from16 v0, p6

    long-to-int v2, v0

    invoke-direct {p0, v2}, Lldpsdksub/d/e;->a(I)J

    move-result-wide v2

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v4, p1, Lldpsdksub/d/e;->a:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    iget-wide v2, p0, Lldpsdksub/d/e;->a:J

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    iput-wide v2, p0, Lldpsdksub/d/e;->a:J

    return-void
.end method
