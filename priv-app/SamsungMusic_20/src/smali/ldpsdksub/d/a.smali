.class public final Lldpsdksub/d/a;
.super Ljava/lang/Object;


# instance fields
.field a:I

.field b:I

.field c:[B

.field d:J

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lldpsdksub/d/a;->a:I

    iput-object v1, p0, Lldpsdksub/d/a;->c:[B

    const/4 v0, 0x1

    iput v0, p0, Lldpsdksub/d/a;->b:I

    const-string v0, ""

    iput-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    iput-object v1, p0, Lldpsdksub/d/a;->c:[B

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lldpsdksub/d/a;->d:J

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    const-string v0, ""

    iput-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    const-string v0, ""

    sget-object v1, Lldpsdksub/d/d;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v3, Lldpsdksub/d/f;

    invoke-direct {v3}, Lldpsdksub/d/f;-><init>()V

    iget v1, p0, Lldpsdksub/d/a;->b:I

    sget v4, Lldpsdksub/d/b;->a:I

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lldpsdksub/d/a;->c:[B

    if-eqz v1, :cond_0

    iget-wide v4, p0, Lldpsdksub/d/a;->d:J

    cmp-long v1, v4, v8

    if-gtz v1, :cond_6

    :cond_0
    const-string v0, ""

    :cond_1
    iget v1, p0, Lldpsdksub/d/a;->a:I

    sget v3, Lldpsdksub/d/b;->b:I

    if-eq v1, v3, :cond_2

    iget v1, p0, Lldpsdksub/d/a;->a:I

    sget v3, Lldpsdksub/d/b;->d:I

    if-ne v1, v3, :cond_3

    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    move-object v0, v1

    move v1, v2

    :goto_0
    if-lt v1, v3, :cond_7

    iget-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    :cond_3
    iget v1, p0, Lldpsdksub/d/a;->a:I

    sget v2, Lldpsdksub/d/b;->c:I

    if-eq v1, v2, :cond_4

    iget v1, p0, Lldpsdksub/d/a;->a:I

    sget v2, Lldpsdksub/d/b;->d:I

    if-ne v1, v2, :cond_5

    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :cond_5
    return-object v0

    :cond_6
    iget-object v1, p0, Lldpsdksub/d/a;->c:[B

    iget-wide v4, p0, Lldpsdksub/d/a;->d:J

    invoke-static {v1, v4, v5, v3}, Lldpsdksub/d/f;->a([BJLldpsdksub/d/f;)J

    move-result-wide v4

    cmp-long v1, v4, v8

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    const/16 v4, 0x10

    if-ge v1, v4, :cond_1

    const-string v4, "%02X"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, v3, Lldpsdksub/d/f;->a:[S

    aget-short v6, v6, v1

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-char v0, v4, v1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-char v0, v4, v1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    iget-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldpsdksub/d/a;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2
.end method
