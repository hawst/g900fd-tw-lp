.class public final Lldpsdksub/d/f;
.super Ljava/lang/Object;


# static fields
.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I

.field private static n:I

.field private static o:I

.field private static p:I

.field private static q:I

.field private static r:I

.field private static s:I

.field private static t:I

.field private static u:[C


# instance fields
.field protected a:[S

.field private b:[J

.field private c:[J

.field private d:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x7

    sput v0, Lldpsdksub/d/f;->e:I

    const/16 v0, 0xc

    sput v0, Lldpsdksub/d/f;->f:I

    const/16 v0, 0x11

    sput v0, Lldpsdksub/d/f;->g:I

    const/16 v0, 0x16

    sput v0, Lldpsdksub/d/f;->h:I

    const/4 v0, 0x5

    sput v0, Lldpsdksub/d/f;->i:I

    const/16 v0, 0x9

    sput v0, Lldpsdksub/d/f;->j:I

    const/16 v0, 0xe

    sput v0, Lldpsdksub/d/f;->k:I

    const/16 v0, 0x14

    sput v0, Lldpsdksub/d/f;->l:I

    const/4 v0, 0x4

    sput v0, Lldpsdksub/d/f;->m:I

    const/16 v0, 0xb

    sput v0, Lldpsdksub/d/f;->n:I

    const/16 v0, 0x10

    sput v0, Lldpsdksub/d/f;->o:I

    const/16 v0, 0x17

    sput v0, Lldpsdksub/d/f;->p:I

    const/4 v0, 0x6

    sput v0, Lldpsdksub/d/f;->q:I

    const/16 v0, 0xa

    sput v0, Lldpsdksub/d/f;->r:I

    const/16 v0, 0xf

    sput v0, Lldpsdksub/d/f;->s:I

    const/16 v0, 0x15

    sput v0, Lldpsdksub/d/f;->t:I

    const/16 v0, 0x40

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lldpsdksub/d/f;->u:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [J

    iput-object v0, p0, Lldpsdksub/d/f;->b:[J

    const/4 v0, 0x4

    new-array v0, v0, [J

    iput-object v0, p0, Lldpsdksub/d/f;->c:[J

    const/16 v0, 0x40

    new-array v0, v0, [S

    iput-object v0, p0, Lldpsdksub/d/f;->d:[S

    const/16 v0, 0x10

    new-array v0, v0, [S

    iput-object v0, p0, Lldpsdksub/d/f;->a:[S

    return-void
.end method

.method public static a([BJLldpsdksub/d/f;)J
    .locals 21

    if-eqz p0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_1

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_1
    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->b:[J

    const/4 v3, 0x0

    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->b:[J

    const/4 v5, 0x1

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v5

    aput-wide v6, v2, v3

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->c:[J

    const/4 v3, 0x0

    const-wide/32 v4, 0x67452301

    aput-wide v4, v2, v3

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->c:[J

    const/4 v3, 0x1

    const-wide v4, 0xefcdab89L

    aput-wide v4, v2, v3

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->c:[J

    const/4 v3, 0x2

    const-wide v4, 0x98badcfeL

    aput-wide v4, v2, v3

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->c:[J

    const/4 v3, 0x3

    const-wide/32 v4, 0x10325476

    aput-wide v4, v2, v3

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x800

    move v8, v4

    move-wide/from16 v4, p1

    :goto_1
    const-wide/16 v10, 0x800

    cmp-long v2, v2, v10

    if-gez v2, :cond_3

    const/16 v2, 0x10

    new-array v11, v2, [J

    const/16 v2, 0xe

    move-object/from16 v0, p3

    iget-object v3, v0, Lldpsdksub/d/f;->b:[J

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    aput-wide v4, v11, v2

    const/16 v2, 0xf

    move-object/from16 v0, p3

    iget-object v3, v0, Lldpsdksub/d/f;->b:[J

    const/4 v4, 0x1

    aget-wide v4, v3, v4

    aput-wide v4, v11, v2

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->b:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const/4 v4, 0x3

    ushr-long/2addr v2, v4

    const-wide/16 v4, 0x3f

    and-long/2addr v2, v4

    long-to-int v2, v2

    const/16 v3, 0x38

    if-ge v2, v3, :cond_8

    rsub-int/lit8 v2, v2, 0x38

    :goto_2
    int-to-long v2, v2

    sget-object v12, Lldpsdksub/d/f;->u:[C

    const/16 v4, 0x10

    new-array v13, v4, [J

    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->b:[J

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    const/4 v8, 0x3

    ushr-long/2addr v4, v8

    const-wide/16 v8, 0x3f

    and-long/2addr v4, v8

    long-to-int v4, v4

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v8, 0x0

    aget-wide v8, v5, v8

    const/4 v5, 0x3

    shl-long v14, v2, v5

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    add-long/2addr v8, v14

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v10, 0x0

    aget-wide v14, v5, v10

    cmp-long v5, v8, v14

    if-gez v5, :cond_2

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v8, 0x1

    aget-wide v14, v5, v8

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    aput-wide v14, v5, v8

    :cond_2
    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v8, 0x0

    aget-wide v14, v5, v8

    const/4 v9, 0x3

    shl-long v16, v2, v9

    add-long v14, v14, v16

    aput-wide v14, v5, v8

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v8, 0x0

    aget-wide v14, v5, v8

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    aput-wide v14, v5, v8

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v8, 0x1

    aget-wide v14, v5, v8

    const/16 v9, 0x1d

    ushr-long v16, v2, v9

    add-long v14, v14, v16

    aput-wide v14, v5, v8

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->b:[J

    const/4 v8, 0x1

    aget-wide v14, v5, v8

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    aput-wide v14, v5, v8

    const/4 v5, 0x0

    move v8, v4

    move v9, v5

    :goto_3
    const-wide/16 v4, 0x1

    sub-long v4, v2, v4

    const-wide/16 v14, 0x0

    cmp-long v2, v2, v14

    if-gtz v2, :cond_9

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_4
    const/16 v4, 0xe

    if-lt v3, v4, :cond_b

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->c:[J

    invoke-static {v2, v11}, Lldpsdksub/d/f;->a([J[J)V

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_5
    const/4 v4, 0x4

    if-lt v3, v4, :cond_c

    move-wide v2, v6

    goto/16 :goto_0

    :cond_3
    const-wide/16 v2, 0x800

    cmp-long v2, v2, v4

    if-gtz v2, :cond_5

    const-wide/16 v2, 0x800

    const-wide/16 v10, 0x800

    sub-long/2addr v4, v10

    :goto_6
    add-long/2addr v6, v2

    const/16 v9, 0x10

    new-array v15, v9, [J

    move-object/from16 v0, p3

    iget-object v9, v0, Lldpsdksub/d/f;->b:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    const/4 v9, 0x3

    ushr-long/2addr v10, v9

    const-wide/16 v12, 0x3f

    and-long/2addr v10, v12

    long-to-int v9, v10

    move-object/from16 v0, p3

    iget-object v10, v0, Lldpsdksub/d/f;->b:[J

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    const/4 v12, 0x3

    shl-long v12, v2, v12

    const-wide v16, 0xffffffffL

    and-long v12, v12, v16

    add-long/2addr v10, v12

    move-object/from16 v0, p3

    iget-object v12, v0, Lldpsdksub/d/f;->b:[J

    const/4 v13, 0x0

    aget-wide v12, v12, v13

    cmp-long v10, v10, v12

    if-gez v10, :cond_4

    move-object/from16 v0, p3

    iget-object v10, v0, Lldpsdksub/d/f;->b:[J

    const/4 v11, 0x1

    aget-wide v12, v10, v11

    const-wide/16 v16, 0x1

    add-long v12, v12, v16

    aput-wide v12, v10, v11

    :cond_4
    move-object/from16 v0, p3

    iget-object v10, v0, Lldpsdksub/d/f;->b:[J

    const/4 v11, 0x0

    aget-wide v12, v10, v11

    const/4 v14, 0x3

    shl-long v16, v2, v14

    add-long v12, v12, v16

    aput-wide v12, v10, v11

    move-object/from16 v0, p3

    iget-object v10, v0, Lldpsdksub/d/f;->b:[J

    const/4 v11, 0x0

    aget-wide v12, v10, v11

    const-wide v16, 0xffffffffL

    and-long v12, v12, v16

    aput-wide v12, v10, v11

    move-object/from16 v0, p3

    iget-object v10, v0, Lldpsdksub/d/f;->b:[J

    const/4 v11, 0x1

    aget-wide v12, v10, v11

    const/16 v14, 0x1d

    ushr-long v16, v2, v14

    add-long v12, v12, v16

    aput-wide v12, v10, v11

    move-object/from16 v0, p3

    iget-object v10, v0, Lldpsdksub/d/f;->b:[J

    const/4 v11, 0x1

    aget-wide v12, v10, v11

    const-wide v16, 0xffffffffL

    and-long v12, v12, v16

    aput-wide v12, v10, v11

    move-wide v10, v2

    move v14, v9

    move v9, v8

    :goto_7
    const-wide/16 v12, 0x1

    sub-long v12, v10, v12

    const-wide/16 v16, 0x0

    cmp-long v10, v10, v16

    if-gtz v10, :cond_6

    long-to-int v9, v2

    add-int/2addr v8, v9

    goto/16 :goto_1

    :cond_5
    move-wide v2, v4

    goto/16 :goto_6

    :cond_6
    move-object/from16 v0, p3

    iget-object v0, v0, Lldpsdksub/d/f;->d:[S

    move-object/from16 v16, v0

    add-int/lit8 v10, v14, 0x1

    add-int/lit8 v11, v9, 0x1

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    int-to-short v9, v9

    aput-short v9, v16, v14

    const/16 v9, 0x40

    if-ne v10, v9, :cond_e

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_8
    const/16 v14, 0x10

    if-lt v9, v14, :cond_7

    move-object/from16 v0, p3

    iget-object v9, v0, Lldpsdksub/d/f;->c:[J

    invoke-static {v9, v15}, Lldpsdksub/d/f;->a([J[J)V

    const/4 v9, 0x0

    move v14, v9

    move v9, v11

    move-wide v10, v12

    goto :goto_7

    :cond_7
    move-object/from16 v0, p3

    iget-object v14, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v16, v10, 0x3

    aget-short v14, v14, v16

    shl-int/lit8 v14, v14, 0x18

    move-object/from16 v0, p3

    iget-object v0, v0, Lldpsdksub/d/f;->d:[S

    move-object/from16 v16, v0

    add-int/lit8 v17, v10, 0x2

    aget-short v16, v16, v17

    shl-int/lit8 v16, v16, 0x10

    or-int v14, v14, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lldpsdksub/d/f;->d:[S

    move-object/from16 v16, v0

    add-int/lit8 v17, v10, 0x1

    aget-short v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int v14, v14, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lldpsdksub/d/f;->d:[S

    move-object/from16 v16, v0

    aget-short v16, v16, v10

    or-int v14, v14, v16

    int-to-long v0, v14

    move-wide/from16 v16, v0

    aput-wide v16, v15, v9

    aget-wide v16, v15, v9

    const-wide v18, 0xffffffffL

    and-long v16, v16, v18

    aput-wide v16, v15, v9

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v10, v10, 0x4

    goto :goto_8

    :cond_8
    rsub-int/lit8 v2, v2, 0x78

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p3

    iget-object v3, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v2, v8, 0x1

    add-int/lit8 v10, v9, 0x1

    aget-char v9, v12, v9

    and-int/lit16 v9, v9, 0xff

    int-to-short v9, v9

    aput-short v9, v3, v8

    const/16 v3, 0x40

    if-ne v2, v3, :cond_d

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_9
    const/16 v8, 0x10

    if-lt v3, v8, :cond_a

    move-object/from16 v0, p3

    iget-object v2, v0, Lldpsdksub/d/f;->c:[J

    invoke-static {v2, v13}, Lldpsdksub/d/f;->a([J[J)V

    const/4 v2, 0x0

    move v8, v2

    move v9, v10

    move-wide v2, v4

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p3

    iget-object v8, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v9, v2, 0x3

    aget-short v8, v8, v9

    shl-int/lit8 v8, v8, 0x18

    move-object/from16 v0, p3

    iget-object v9, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v14, v2, 0x2

    aget-short v9, v9, v14

    shl-int/lit8 v9, v9, 0x10

    or-int/2addr v8, v9

    move-object/from16 v0, p3

    iget-object v9, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v14, v2, 0x1

    aget-short v9, v9, v14

    shl-int/lit8 v9, v9, 0x8

    or-int/2addr v8, v9

    move-object/from16 v0, p3

    iget-object v9, v0, Lldpsdksub/d/f;->d:[S

    aget-short v9, v9, v2

    or-int/2addr v8, v9

    int-to-long v8, v8

    aput-wide v8, v13, v3

    aget-wide v8, v13, v3

    const-wide v14, 0xffffffffL

    and-long/2addr v8, v14

    aput-wide v8, v13, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x4

    goto :goto_9

    :cond_b
    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v5, v2, 0x3

    aget-short v4, v4, v5

    shl-int/lit8 v4, v4, 0x18

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v8, v2, 0x2

    aget-short v5, v5, v8

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->d:[S

    add-int/lit8 v8, v2, 0x1

    aget-short v5, v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->d:[S

    aget-short v5, v5, v2

    or-int/2addr v4, v5

    int-to-long v4, v4

    aput-wide v4, v11, v3

    aget-wide v4, v11, v3

    const-wide v8, 0xffffffffL

    and-long/2addr v4, v8

    aput-wide v4, v11, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x4

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->a:[S

    move-object/from16 v0, p3

    iget-object v5, v0, Lldpsdksub/d/f;->c:[J

    aget-wide v8, v5, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v5, v8

    int-to-short v5, v5

    aput-short v5, v4, v2

    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->a:[S

    add-int/lit8 v5, v2, 0x1

    move-object/from16 v0, p3

    iget-object v8, v0, Lldpsdksub/d/f;->c:[J

    aget-wide v8, v8, v3

    const/16 v10, 0x8

    ushr-long/2addr v8, v10

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-short v8, v8

    aput-short v8, v4, v5

    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->a:[S

    add-int/lit8 v5, v2, 0x2

    move-object/from16 v0, p3

    iget-object v8, v0, Lldpsdksub/d/f;->c:[J

    aget-wide v8, v8, v3

    const/16 v10, 0x10

    ushr-long/2addr v8, v10

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-short v8, v8

    aput-short v8, v4, v5

    move-object/from16 v0, p3

    iget-object v4, v0, Lldpsdksub/d/f;->a:[S

    add-int/lit8 v5, v2, 0x3

    move-object/from16 v0, p3

    iget-object v8, v0, Lldpsdksub/d/f;->c:[J

    aget-wide v8, v8, v3

    const/16 v10, 0x18

    ushr-long/2addr v8, v10

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-short v8, v8

    aput-short v8, v4, v5

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x4

    goto/16 :goto_5

    :cond_d
    move v8, v2

    move v9, v10

    move-wide v2, v4

    goto/16 :goto_3

    :cond_e
    move v9, v11

    move v14, v10

    move-wide v10, v12

    goto/16 :goto_7
.end method

.method private static a([J[J)V
    .locals 13

    new-instance v0, Lldpsdksub/d/e;

    const/4 v1, 0x0

    aget-wide v2, p0, v1

    invoke-direct {v0, v2, v3}, Lldpsdksub/d/e;-><init>(J)V

    new-instance v1, Lldpsdksub/d/e;

    const/4 v2, 0x1

    aget-wide v2, p0, v2

    invoke-direct {v1, v2, v3}, Lldpsdksub/d/e;-><init>(J)V

    new-instance v2, Lldpsdksub/d/e;

    const/4 v3, 0x2

    aget-wide v4, p0, v3

    invoke-direct {v2, v4, v5}, Lldpsdksub/d/e;-><init>(J)V

    new-instance v3, Lldpsdksub/d/e;

    const/4 v4, 0x3

    aget-wide v4, p0, v4

    invoke-direct {v3, v4, v5}, Lldpsdksub/d/e;-><init>(J)V

    const/4 v4, 0x0

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->e:I

    int-to-long v6, v6

    const-wide v8, 0xd76aa478L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x1

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->f:I

    int-to-long v9, v4

    const-wide v11, 0xe8c7b756L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x2

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->g:I

    int-to-long v8, v4

    const-wide/32 v10, 0x242070db

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x3

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->h:I

    int-to-long v7, v4

    const-wide v9, 0xc1bdceeeL

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x4

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->e:I

    int-to-long v6, v6

    const-wide v8, 0xf57c0fafL

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x5

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->f:I

    int-to-long v9, v4

    const-wide/32 v11, 0x4787c62a

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x6

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->g:I

    int-to-long v8, v4

    const-wide v10, 0xa8304613L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x7

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->h:I

    int-to-long v7, v4

    const-wide v9, 0xfd469501L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x8

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->e:I

    int-to-long v6, v6

    const-wide/32 v8, 0x698098d8

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x9

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->f:I

    int-to-long v9, v4

    const-wide v11, 0x8b44f7afL

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xa

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->g:I

    int-to-long v8, v4

    const-wide v10, 0xffff5bb1L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xb

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->h:I

    int-to-long v7, v4

    const-wide v9, 0x895cd7beL

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xc

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->e:I

    int-to-long v6, v6

    const-wide/32 v8, 0x6b901122

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xd

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->f:I

    int-to-long v9, v4

    const-wide v11, 0xfd987193L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xe

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->g:I

    int-to-long v8, v4

    const-wide v10, 0xa679438eL

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xf

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->h:I

    int-to-long v7, v4

    const-wide/32 v9, 0x49b40821

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->a(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x1

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->i:I

    int-to-long v6, v6

    const-wide v8, 0xf61e2562L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x6

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->j:I

    int-to-long v9, v4

    const-wide v11, 0xc040b340L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xb

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->k:I

    int-to-long v8, v4

    const-wide/32 v10, 0x265e5a51

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x0

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->l:I

    int-to-long v7, v4

    const-wide v9, 0xe9b6c7aaL

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x5

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->i:I

    int-to-long v6, v6

    const-wide v8, 0xd62f105dL

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xa

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->j:I

    int-to-long v9, v4

    const-wide/32 v11, 0x2441453

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xf

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->k:I

    int-to-long v8, v4

    const-wide v10, 0xd8a1e681L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x4

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->l:I

    int-to-long v7, v4

    const-wide v9, 0xe7d3fbc8L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x9

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->i:I

    int-to-long v6, v6

    const-wide/32 v8, 0x21e1cde6

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xe

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->j:I

    int-to-long v9, v4

    const-wide v11, 0xc33707d6L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x3

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->k:I

    int-to-long v8, v4

    const-wide v10, 0xf4d50d87L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x8

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->l:I

    int-to-long v7, v4

    const-wide/32 v9, 0x455a14ed

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xd

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->i:I

    int-to-long v6, v6

    const-wide v8, 0xa9e3e905L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x2

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->j:I

    int-to-long v9, v4

    const-wide v11, 0xfcefa3f8L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x7

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->k:I

    int-to-long v8, v4

    const-wide/32 v10, 0x676f02d9

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xc

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->l:I

    int-to-long v7, v4

    const-wide v9, 0x8d2a4c8aL

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->b(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x5

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->m:I

    int-to-long v6, v6

    const-wide v8, 0xfffa3942L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x8

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->n:I

    int-to-long v9, v4

    const-wide v11, 0x8771f681L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xb

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->o:I

    int-to-long v8, v4

    const-wide/32 v10, 0x6d9d6122

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xe

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->p:I

    int-to-long v7, v4

    const-wide v9, 0xfde5380cL

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x1

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->m:I

    int-to-long v6, v6

    const-wide v8, 0xa4beea44L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x4

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->n:I

    int-to-long v9, v4

    const-wide/32 v11, 0x4bdecfa9

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x7

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->o:I

    int-to-long v8, v4

    const-wide v10, 0xf6bb4b60L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xa

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->p:I

    int-to-long v7, v4

    const-wide v9, 0xbebfbc70L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xd

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->m:I

    int-to-long v6, v6

    const-wide/32 v8, 0x289b7ec6

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x0

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->n:I

    int-to-long v9, v4

    const-wide v11, 0xeaa127faL

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x3

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->o:I

    int-to-long v8, v4

    const-wide v10, 0xd4ef3085L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x6

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->p:I

    int-to-long v7, v4

    const-wide/32 v9, 0x4881d05

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x9

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->m:I

    int-to-long v6, v6

    const-wide v8, 0xd9d4d039L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xc

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->n:I

    int-to-long v9, v4

    const-wide v11, 0xe6db99e5L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xf

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->o:I

    int-to-long v8, v4

    const-wide/32 v10, 0x1fa27cf8

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x2

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->p:I

    int-to-long v7, v4

    const-wide v9, 0xc4ac5665L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->c(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x0

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->q:I

    int-to-long v6, v6

    const-wide v8, 0xf4292244L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x7

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->r:I

    int-to-long v9, v4

    const-wide/32 v11, 0x432aff97

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xe

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->s:I

    int-to-long v8, v4

    const-wide v10, 0xab9423a7L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x5

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->t:I

    int-to-long v7, v4

    const-wide v9, 0xfc93a039L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xc

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->q:I

    int-to-long v6, v6

    const-wide/32 v8, 0x655b59c3

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x3

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->r:I

    int-to-long v9, v4

    const-wide v11, 0x8f0ccc92L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xa

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->s:I

    int-to-long v8, v4

    const-wide v10, 0xffeff47dL

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x1

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->t:I

    int-to-long v7, v4

    const-wide v9, 0x85845dd1L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x8

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->q:I

    int-to-long v6, v6

    const-wide/32 v8, 0x6fa87e4f

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xf

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->r:I

    int-to-long v9, v4

    const-wide v11, 0xfe2ce6e0L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x6

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->s:I

    int-to-long v8, v4

    const-wide v10, 0xa3014314L

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xd

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->t:I

    int-to-long v7, v4

    const-wide/32 v9, 0x4e0811a1

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x4

    aget-wide v4, p1, v4

    sget v6, Lldpsdksub/d/f;->q:I

    int-to-long v6, v6

    const-wide v8, 0xf7537e82L

    invoke-virtual/range {v0 .. v9}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0xb

    aget-wide v7, p1, v4

    sget v4, Lldpsdksub/d/f;->r:I

    int-to-long v9, v4

    const-wide v11, 0xbd3af235L

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v3 .. v12}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x2

    aget-wide v6, p1, v4

    sget v4, Lldpsdksub/d/f;->s:I

    int-to-long v8, v4

    const-wide/32 v10, 0x2ad7d2bb

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v11}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/16 v4, 0x9

    aget-wide v5, p1, v4

    sget v4, Lldpsdksub/d/f;->t:I

    int-to-long v7, v4

    const-wide v9, 0xeb86d391L

    move-object v4, v0

    invoke-virtual/range {v1 .. v10}, Lldpsdksub/d/e;->d(Lldpsdksub/d/e;Lldpsdksub/d/e;Lldpsdksub/d/e;JJJ)V

    const/4 v4, 0x0

    aget-wide v4, p0, v4

    iget-wide v6, v0, Lldpsdksub/d/e;->a:J

    add-long/2addr v4, v6

    long-to-double v4, v4

    const/4 v0, 0x0

    double-to-long v4, v4

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    aput-wide v4, p0, v0

    const/4 v0, 0x1

    aget-wide v4, p0, v0

    iget-wide v0, v1, Lldpsdksub/d/e;->a:J

    add-long/2addr v0, v4

    long-to-double v0, v0

    const/4 v4, 0x1

    double-to-long v0, v0

    const-wide v6, 0xffffffffL

    and-long/2addr v0, v6

    aput-wide v0, p0, v4

    const/4 v0, 0x2

    aget-wide v0, p0, v0

    iget-wide v4, v2, Lldpsdksub/d/e;->a:J

    add-long/2addr v0, v4

    long-to-double v0, v0

    const/4 v2, 0x2

    double-to-long v0, v0

    const-wide v4, 0xffffffffL

    and-long/2addr v0, v4

    aput-wide v0, p0, v2

    const/4 v0, 0x3

    aget-wide v0, p0, v0

    iget-wide v2, v3, Lldpsdksub/d/e;->a:J

    add-long/2addr v0, v2

    long-to-double v0, v0

    const/4 v2, 0x3

    double-to-long v0, v0

    const-wide v4, 0xffffffffL

    and-long/2addr v0, v4

    aput-wide v0, p0, v2

    return-void
.end method
