.class public final Lldpsdksub/d/c;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    const/4 v2, 0x0

    const/4 v12, 0x2

    const/16 v11, 0xa

    const/4 v4, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_c

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p0}, Lldpsdksub/a/e;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {p0}, Lldpsdksub/a/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const v0, 0x80001

    new-array v8, v0, [B

    new-array v9, v11, [B

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    const/16 v5, 0xa

    :try_start_1
    invoke-virtual {v3, v9, v0, v5}, Ljava/io/FileInputStream;->read([BII)I

    const/4 v0, 0x0

    aget-byte v0, v9, v0

    const/16 v5, 0x49

    if-ne v0, v5, :cond_0

    const/4 v0, 0x1

    aget-byte v0, v9, v0

    const/16 v5, 0x44

    if-ne v0, v5, :cond_0

    const/4 v0, 0x2

    aget-byte v0, v9, v0

    const/16 v5, 0x33

    if-eq v0, v5, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    const/4 v0, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    invoke-static {v9, v0, v8, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v0, 0xa

    const v5, 0x7fff6

    invoke-virtual {v3, v8, v0, v5}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    move v1, v4

    :cond_1
    :goto_2
    if-nez v1, :cond_7

    move-object v0, v2

    :goto_3
    return-object v0

    :cond_2
    const/16 v5, 0x15

    const/4 v0, 0x6

    move v6, v5

    move v5, v0

    move v0, v1

    :goto_4
    if-lt v5, v11, :cond_4

    int-to-long v6, v0

    :try_start_3
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v10

    cmp-long v5, v6, v10

    if-lez v5, :cond_3

    move v0, v1

    :cond_3
    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    :cond_4
    aget-byte v7, v9, v5

    shl-int/2addr v7, v6

    add-int/2addr v7, v0

    add-int/lit8 v6, v6, -0x7

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v7

    goto :goto_4

    :cond_5
    add-int/lit8 v0, v0, -0xa

    int-to-long v6, v0

    invoke-virtual {v3, v6, v7}, Ljava/io/FileInputStream;->skip(J)J

    const/4 v0, 0x0

    const/high16 v5, 0x80000

    invoke-virtual {v3, v8, v0, v5}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v0, v3

    :goto_5
    if-eqz v0, :cond_1

    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v3, v2

    :goto_6
    if-eqz v3, :cond_1

    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    move-object v3, v2

    :goto_7
    if-eqz v3, :cond_1

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    move-object v3, v2

    :goto_8
    if-eqz v3, :cond_1

    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_2

    :catch_7
    move-exception v0

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_9
    if-eqz v3, :cond_6

    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9

    :cond_6
    :goto_a
    throw v0

    :catch_8
    move-exception v0

    move v1, v4

    goto :goto_2

    :cond_7
    new-instance v0, Lldpsdksub/d/a;

    invoke-direct {v0}, Lldpsdksub/d/a;-><init>()V

    sget v1, Lldpsdksub/d/b;->c:I

    if-eq v1, v4, :cond_8

    if-eq v1, v12, :cond_8

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8

    const/4 v2, 0x4

    if-ne v1, v2, :cond_a

    :cond_8
    iput v1, v0, Lldpsdksub/d/a;->a:I

    :goto_b
    sget v1, Lldpsdksub/d/b;->a:I

    if-eq v1, v4, :cond_9

    if-ne v1, v12, :cond_b

    :cond_9
    iput v1, v0, Lldpsdksub/d/a;->b:I

    :goto_c
    iput-object v8, v0, Lldpsdksub/d/a;->c:[B

    const-wide/32 v2, 0x80000

    iput-wide v2, v0, Lldpsdksub/d/a;->d:J

    invoke-virtual {v0}, Lldpsdksub/d/a;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_a
    iput v4, v0, Lldpsdksub/d/a;->a:I

    goto :goto_b

    :cond_b
    iput v4, v0, Lldpsdksub/d/a;->b:I

    goto :goto_c

    :cond_c
    move-object v0, v2

    goto/16 :goto_3

    :catch_9
    move-exception v1

    goto :goto_a

    :catchall_1
    move-exception v0

    goto :goto_9

    :catch_a
    move-exception v0

    goto :goto_8

    :catch_b
    move-exception v0

    goto :goto_7

    :catch_c
    move-exception v0

    goto :goto_6

    :catch_d
    move-exception v0

    move-object v0, v2

    goto :goto_5
.end method
