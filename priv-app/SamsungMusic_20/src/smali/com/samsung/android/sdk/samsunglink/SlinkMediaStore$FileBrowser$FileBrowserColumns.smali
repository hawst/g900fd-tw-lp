.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileBrowserColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileBrowserColumns"
.end annotation


# static fields
.field public static final DISPLAY_NAME:Ljava/lang/String; = "_display_name"

.field public static final FILE_ID:Ljava/lang/String; = "document_id"

.field public static final HOME_SYNC_FLAGS:Ljava/lang/String; = "home_sync_flags"

.field public static final ICON_ID:Ljava/lang/String; = "icon"

.field public static final LAST_MODIFIED:Ljava/lang/String; = "last_modified"

.field public static final LOCAL_DATA:Ljava/lang/String; = "local_data"

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final SIZE:Ljava/lang/String; = "_size"
