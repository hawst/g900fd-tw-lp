.class public Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
.super Ljava/lang/Object;
.source "SlinkFileTransferUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$1;,
        Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    }
.end annotation


# static fields
.field public static final ACTION_CANCEL:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Cancel"

.field public static final ACTION_CHOOSE_DEVICE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.ChooseDevice"

.field public static final ACTION_DOWNLOAD:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Download"

.field public static final ACTION_DOWNLOAD_MODAL:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.DownloadModal"

.field public static final ACTION_FILE_TRANSFER_LIST:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.FileTransferList"

.field public static final ACTION_SEND_TO:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.SendTo"

.field public static final ACTION_TRANSFER:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Transfer"

.field public static final ACTION_UPLOAD:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Upload"

.field public static final BROADCAST_AUTO_UPLOAD_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.AUTO_UPLOAD_STATE_CHANGED"

.field public static final DEVICE_CHOOSER_RESULT_BROADCAST_ACTION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.DEVICE_CHOOSER_RESULT_BROADCAST_ACTION"

.field public static final DOWNLOAD_RESULT_BROADCAST_ACTION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.DOWNLOAD_RESULT_BROADCAST_ACTION"

.field public static final EXTRA_BROADCAST_RESULTS:Ljava/lang/String; = "broadcastResults"

.field public static final EXTRA_CHOOSE_STORAGE:Ljava/lang/String; = "choose_storage"

.field public static final EXTRA_DEVICE_CHOOSER_MODE:Ljava/lang/String; = "deviceChooserMode"

.field public static final EXTRA_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field public static final EXTRA_DOWNLOADING_TEXT:Ljava/lang/String; = "downloadingText"

.field public static final EXTRA_IS_CANCEL_FOR_AUTO_UPLOAD:Ljava/lang/String; = "isCancelForAutoUpload"

.field public static final EXTRA_IS_FILES_OR_DOCUMENTS_TAB_SOURCE:Ljava/lang/String; = "isFilesOrDocumentsTabSource"

.field public static final EXTRA_IS_SOURCE_SECURE:Ljava/lang/String; = "isSourceSecure"

.field public static final EXTRA_PATHS:Ljava/lang/String; = "paths"

.field public static final EXTRA_ROW_IDS:Ljava/lang/String; = "rowIds"

.field public static final EXTRA_TARGET_DEVICE_ID:Ljava/lang/String; = "targetDeviceId"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"

.field public static final EXTRA_TRANSFER_OPTIONS:Ljava/lang/String; = "transferOptions"

.field public static final EXTRA_URIS:Ljava/lang/String; = "uris"

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    .line 186
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 170
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 172
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    if-nez v0, :cond_1

    .line 173
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    .line 175
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public cancelFilesForAutoUpload()V
    .locals 4

    .prologue
    .line 823
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v2, "Enter ::cancelFilesForAutoUpload()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Cancel"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 826
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.filetransfer.FileTransferStarterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 829
    const-string v1, "isCancelForAutoUpload"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 830
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 832
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 833
    return-void
.end method

.method public createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;
    .locals 5
    .param p1, "sourceDeviceId"    # J
    .param p3, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p4, "broadcastResults"    # Z
    .param p5, "chooseStorage"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 664
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter ::createDeviceChooserActivityIntent(sourceDeviceId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mediaSet, broadcastResults:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", chooseStorage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.ChooseDevice"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 674
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.ui.SendToActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 677
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 678
    if-eqz p3, :cond_0

    .line 679
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 681
    :cond_0
    if-eqz p4, :cond_1

    .line 682
    const-string v1, "broadcastResults"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 684
    :cond_1
    if-eqz p5, :cond_2

    .line 685
    const-string v1, "choose_storage"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 688
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 690
    return-object v0
.end method

.method public createDeviceChooserActivityIntent(JZ)Landroid/content/Intent;
    .locals 7
    .param p1, "sourceDeviceId"    # J
    .param p3, "broadcastResults"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 576
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter ::createDeviceChooserActivityIntent(sourceDeviceId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", broadcastResults:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    const/4 v4, 0x0

    check-cast v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createDeviceChooserActivityIntent(J[JZ)Landroid/content/Intent;
    .locals 7
    .param p1, "sourceDeviceId"    # J
    .param p3, "samsungLinkMediaStoreRowIds"    # [J
    .param p4, "broadcastResults"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 618
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter ::createDeviceChooserActivityIntent(sourceDeviceId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", samsungLinkMediaStoreRowIds, broadcastResults:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    invoke-static {p3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v4

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createFileTransferListActivityIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 808
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v2, "Enter ::createFileTransferListActivityIntent()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.FileTransferList"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 811
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.ui.FileTransferListActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 814
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 815
    return-object v0
.end method

.method public createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 4
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "downloadingText"    # Ljava/lang/String;
    .param p4, "broadcastResults"    # Z
    .param p5, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 475
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter ::createModalDownloadActivityIntent( mediaSet, title:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", downloadingText:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", broadcastResults:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", TransferOptions)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.DownloadModal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 485
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.ui.DownloadActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 489
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 490
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 491
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 494
    const-string v1, "downloadingText"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    :cond_1
    if-eqz p4, :cond_2

    .line 497
    const-string v1, "broadcastResults"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    :cond_2
    if-eqz p5, :cond_3

    .line 500
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 503
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 505
    return-object v0
.end method

.method public createModalDownloadActivityIntent([JLjava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 6
    .param p1, "samsungLinkMediaStoreRowIds"    # [J
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "downloadingText"    # Ljava/lang/String;
    .param p4, "broadcastResults"    # Z
    .param p5, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 430
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v1, "Enter ::createModalDownloadActivityIntent( samsungLinkMediaStoreRowIds, title, downloadingText, broadcastResults, TransferOptions)"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createSendToActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 4
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 724
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v2, "Enter ::createSendToActivityIntent(mediaSet, TransferOptions)"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    if-nez p1, :cond_0

    .line 727
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "mediaSet is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 730
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.SendTo"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 732
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.ui.SendToActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 736
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 738
    if-eqz p2, :cond_1

    .line 739
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 742
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 744
    return-object v0
.end method

.method public downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 216
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v2, "Enter ::downloadFiles(mediaSet, TransferOptions)"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Download"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.filetransfer.FileTransferStarterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 222
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 223
    if-eqz p2, :cond_0

    .line 224
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 229
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 230
    return-void
.end method

.method public downloadFiles([JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2
    .param p1, "samsungLinkMediaStoreRowIds"    # [J
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 200
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v1, "Enter ::downloadFiles(samsungLinkMediaStoreRowIds, TransferOptions)"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 204
    return-void
.end method

.method public getLocalPathsFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 3
    .param p1, "resultData"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v2, "Enter ::getLocalPathsFromSuccessfulModalDownloadResult(Intent)"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const-string v1, "paths"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 547
    .local v0, "localPaths":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 548
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 550
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 8
    .param p1, "resultData"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v7, "Enter ::getLocalUrisFromSuccessfulModalDownloadResult(Intent)"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    const-string v6, "uris"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 522
    .local v3, "mediaStoreUris":[Landroid/os/Parcelable;
    if-nez v3, :cond_1

    .line 523
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 530
    :cond_0
    return-object v5

    .line 525
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    array-length v6, v3

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 526
    .local v5, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    move-object v0, v3

    .local v0, "arr$":[Landroid/os/Parcelable;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 527
    .local v4, "parcelable":Landroid/os/Parcelable;
    check-cast v4, Landroid/net/Uri;

    .end local v4    # "parcelable":Landroid/os/Parcelable;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getTargetDeviceIdFromSuccessfulDeviceChooserResult(Landroid/content/Intent;)J
    .locals 4
    .param p1, "resultData"    # Landroid/content/Intent;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 704
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v1, "Enter ::getTargetDeviceIdFromSuccessfulDeviceChooserResult(Intent)"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    const-string v0, "deviceId"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public is3boxSupported(JJ)Z
    .locals 7
    .param p1, "sourceDeviceId"    # J
    .param p3, "targetDeviceId"    # J

    .prologue
    .line 365
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter ::is3boxSupported( sourceDeviceId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", targetDeviceId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 372
    .local v1, "extras":Landroid/os/Bundle;
    const-string v3, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID"

    invoke-virtual {v1, v3, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 375
    const-string v3, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID"

    invoke-virtual {v1, v3, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 379
    const/4 v2, 0x0

    .line 381
    .local v2, "result":Landroid/os/Bundle;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.Is3boxSupported.NAME"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 390
    :goto_0
    if-nez v2, :cond_0

    .line 391
    const/4 v3, 0x0

    .line 393
    :goto_1
    return v3

    .line 386
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "slinklib"

    const-string v4, "IllegalArgumentException ::maybe platform disabled is3boxSupported"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 393
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const-string v3, "method_result"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1
.end method

.method public isAutoUploadInProgress()Z
    .locals 7

    .prologue
    .line 754
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    const-string v3, "Enter ::isAutoUploadInProgress()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    const/4 v1, 0x0

    .line 757
    .local v1, "result":Landroid/os/Bundle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsAutoUploadInProgress.NAME"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 768
    :goto_0
    if-nez v1, :cond_0

    .line 769
    const/4 v2, 0x0

    .line 771
    :goto_1
    return v2

    .line 762
    :catch_0
    move-exception v0

    .line 764
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "slinklib"

    const-string v3, "IllegalArgumentException ::maybe platform disabled isAutoUploadInProgress"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 771
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const-string v2, "method_result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    goto :goto_1
.end method

.method public isTransferInProgressForDevice(J)Z
    .locals 9
    .param p1, "deviceId"    # J

    .prologue
    const/4 v3, 0x0

    .line 783
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Enter ::isTransferInProgressForDevice(deviceId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    const/4 v2, 0x0

    .line 786
    .local v2, "result":Landroid/os/Bundle;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsFileTransferInProgress.NAME"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 799
    if-eqz v2, :cond_0

    const-string v3, "method_result"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    :cond_0
    :goto_0
    return v3

    .line 791
    :catch_0
    move-exception v0

    .line 792
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 793
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 795
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    const-string v4, "slinklib"

    const-string v5, "IllegalArgumentException ::maybe platform disabled iTPFD"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public transferFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 269
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter ::transferFiles(mediaSet, remoteDeviceId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", TransferOptions)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Transfer"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.filetransfer.FileTransferStarterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 278
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 279
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 280
    if-eqz p4, :cond_0

    .line 281
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 286
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 287
    return-void
.end method

.method public transferFiles([JJLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 4
    .param p1, "samsungLinkMediaStoreRowIds"    # [J
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 248
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter ::transferFiles(samsungLinkMediaStoreRowIds, remoteDeviceId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", TransferOptions)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->transferFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 255
    return-void
.end method

.method public uploadMediaStoreFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 332
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter ::uploadMediaStoreFiles(mediaSet, remoteDeviceId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", TransferOptions)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Upload"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 338
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.filetransfer.FileTransferStarterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 342
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 343
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 344
    if-eqz p4, :cond_0

    .line 345
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 348
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 350
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 351
    return-void
.end method

.method public uploadMediaStoreFiles([JJLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 4
    .param p1, "mediaStoreRowIds"    # [J
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 307
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter ::uploadMediaStoreFiles(mediaStoreRowIds, remoteDeviceId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", TransferOptions)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->uploadMediaStoreFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 314
    return-void
.end method
