.class public final enum Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
.super Ljava/lang/Enum;
.source "SlinkDevicePhysicalType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field private static final TAG:Ljava/lang/String;

.field public static final enum TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field public static final enum WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "DEVICE_PHONE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "DEVICE_TAB"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "PC"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 33
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "BLURAY"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 38
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "CAMERA"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 43
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "SPC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 48
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "TV"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 53
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "DEVICE_PHONE_WINDOWS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 58
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 63
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    const-string v1, "WEARABLE_DEVICE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 14
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 65
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getDevicePhysicalType(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    .locals 7
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 78
    const-string v4, "physical_type"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 80
    .local v1, "columnIndex":I
    :try_start_0
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 82
    .local v3, "value":Ljava/lang/String;
    :try_start_1
    invoke-static {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 89
    .end local v3    # "value":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 83
    .restart local v3    # "value":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 84
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unrecognized value for devicePhysicalType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    :try_end_2
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 87
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "value":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 88
    .local v0, "ce":Landroid/database/CursorIndexOutOfBoundsException;
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Column index: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    return-object v0
.end method


# virtual methods
.method public toContentValues(Landroid/content/ContentValues;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 100
    const-string v0, "physical_type"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return-void
.end method
