.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AudioUriBatch"
.end annotation


# instance fields
.field private final mHttpProxyUri:Landroid/net/Uri;

.field private final mLocalUri:Landroid/net/Uri;

.field private final mSameAccessPointUri:Landroid/net/Uri;

.field private final mScsUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0
    .param p1, "httpProxyUri"    # Landroid/net/Uri;
    .param p2, "scsUri"    # Landroid/net/Uri;
    .param p3, "sameAccessPointUri"    # Landroid/net/Uri;
    .param p4, "localUri"    # Landroid/net/Uri;

    .prologue
    .line 2901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2902
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mHttpProxyUri:Landroid/net/Uri;

    .line 2903
    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mScsUri:Landroid/net/Uri;

    .line 2904
    iput-object p3, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mSameAccessPointUri:Landroid/net/Uri;

    .line 2905
    iput-object p4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mLocalUri:Landroid/net/Uri;

    .line 2906
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/net/Uri;
    .param p2, "x1"    # Landroid/net/Uri;
    .param p3, "x2"    # Landroid/net/Uri;
    .param p4, "x3"    # Landroid/net/Uri;
    .param p5, "x4"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$1;

    .prologue
    .line 2890
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public getHttpProxyUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2909
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mHttpProxyUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getLocalUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2921
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getSameAccessPointUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2917
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mSameAccessPointUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getScsUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2913
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->mScsUri:Landroid/net/Uri;

    return-object v0
.end method
