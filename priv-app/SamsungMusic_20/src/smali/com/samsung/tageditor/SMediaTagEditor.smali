.class public Lcom/samsung/tageditor/SMediaTagEditor;
.super Ljava/lang/Object;
.source "SMediaTagEditor.java"


# static fields
.field public static final METADATA_KEY_ALBUM:I = 0x1

.field public static final METADATA_KEY_ALBUMARTIST:I = 0xd

.field public static final METADATA_KEY_ARTIST:I = 0x2

.field public static final METADATA_KEY_COMPOSER:I = 0x4

.field public static final METADATA_KEY_TITLE:I = 0x7

.field public static final RCD_EOF:I = 0x0

.field public static final RCD_ERR_FILEIO:I = -0x5

.field public static final RCD_ERR_INVALID_ARG:I = -0x1

.field public static final RCD_ERR_MEMALLOC:I = -0x4

.field public static final RCD_ERR_UNEXPECTED:I = -0x3

.field public static final RCD_ERR_UNSUPPORTED:I = -0x2

.field public static final RCD_OK:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "smte"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native closeFile()I
.end method

.method public static native getMetadata(II)[B
.end method

.method public static native openFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native saveMetadata()I
.end method

.method public static native setMetadata(ILjava/lang/String;)I
.end method
