.class public Lcom/samsung/musicplus/bigpond/BigPondFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "BigPondFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/bigpond/IBigPondController;


# static fields
.field private static final TAG:Ljava/lang/String; = "BigPondFragment"


# instance fields
.field mAdapter:Lcom/samsung/musicplus/bigpond/BigPondAdapter;

.field private mIsView:Z

.field mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mIsView:Z

    return-void
.end method

.method private initList()V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/musicplus/bigpond/BigPondModel;->getInst()Lcom/samsung/musicplus/bigpond/BigPondModel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;

    .line 45
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;

    invoke-direct {v0, v1, v2, p0}, Lcom/samsung/musicplus/bigpond/BigPondAdapter;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/bigpond/IBigPondModel;Lcom/samsung/musicplus/bigpond/IBigPondController;)V

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mAdapter:Lcom/samsung/musicplus/bigpond/BigPondAdapter;

    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;

    iget-object v1, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mAdapter:Lcom/samsung/musicplus/bigpond/BigPondAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/bigpond/BigPondModel;->registerView(Lcom/samsung/musicplus/bigpond/IBigPondView;)V

    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mAdapter:Lcom/samsung/musicplus/bigpond/BigPondAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    const v0, 0x7f100030

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->setEmptyViewText(Ljava/lang/CharSequence;)V

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->setListShown(Z)V

    .line 50
    return-void
.end method


# virtual methods
.method protected initializeList()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initializeList()V

    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->initList()V

    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;

    invoke-virtual {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->start()V

    .line 67
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 59
    const-string v0, "BigPondFragment"

    const-string v1, "onActivityCreated()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 129
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroyView()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mIsView:Z

    .line 142
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 5
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 100
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;

    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondModel;->getData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 103
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "BigPondFragment"

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 107
    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onListItemLongClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)Z
    .locals 5
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x1

    .line 114
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mModel:Lcom/samsung/musicplus/bigpond/BigPondModel;

    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondModel;->getData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 117
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return v4

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "BigPondFragment"

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 121
    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Target Activity Not Found"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 72
    iget v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 77
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onResume()V

    .line 149
    return-void
.end method

.method public onUpdate(I)V
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 82
    packed-switch p1, :pswitch_data_0

    .line 96
    :goto_0
    return-void

    .line 84
    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mIsView:Z

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;->setListShown(Z)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mAdapter:Lcom/samsung/musicplus/bigpond/BigPondAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 90
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mAdapter:Lcom/samsung/musicplus/bigpond/BigPondAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondFragment;->mIsView:Z

    .line 136
    return-void
.end method
