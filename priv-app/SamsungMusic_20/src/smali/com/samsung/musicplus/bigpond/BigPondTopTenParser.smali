.class Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;
.super Ljava/lang/Object;
.source "BigPondTopTenParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/bigpond/BigPondTopTenParser$BIGPOND_RESPONSE_RESULT;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MpBigPondTop10Parser"


# instance fields
.field private mBigPondItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mImageIndex:I


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "getList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    .line 65
    iput-object p1, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    .line 66
    return-void
.end method


# virtual methods
.method public getData(Ljava/lang/String;)I
    .locals 12
    .param p1, "urlString"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v5, 0x1

    .line 71
    .local v5, "responseCode":I
    const-string v9, "MpBigPondTop10Parser"

    const-string v10, "getData()"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v2, 0x0

    .line 74
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 75
    .local v7, "url":Ljava/net/URL;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 76
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v4

    .line 77
    .local v4, "parser":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v4}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v8

    .line 78
    .local v8, "xmlreader":Lorg/xml/sax/XMLReader;
    new-instance v6, Lcom/samsung/musicplus/bigpond/BigPondParseHandler;

    iget-object v9, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    invoke-direct {v6, v9}, Lcom/samsung/musicplus/bigpond/BigPondParseHandler;-><init>(Ljava/util/List;)V

    .line 79
    .local v6, "theRssHandler":Lcom/samsung/musicplus/bigpond/BigPondParseHandler;
    invoke-interface {v8, v6}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 80
    invoke-virtual {v7}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v2

    .line 81
    new-instance v3, Lorg/xml/sax/InputSource;

    invoke-direct {v3, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 83
    .local v3, "is":Lorg/xml/sax/InputSource;
    invoke-interface {v8, v3}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 84
    if-eqz v6, :cond_0

    .line 85
    invoke-virtual {v6}, Lcom/samsung/musicplus/bigpond/BigPondParseHandler;->getChannelItem()Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :cond_0
    const/4 v5, 0x3

    .line 95
    if-eqz v2, :cond_1

    .line 97
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 103
    .end local v1    # "factory":Ljavax/xml/parsers/SAXParserFactory;
    .end local v3    # "is":Lorg/xml/sax/InputSource;
    .end local v4    # "parser":Ljavax/xml/parsers/SAXParser;
    .end local v6    # "theRssHandler":Lcom/samsung/musicplus/bigpond/BigPondParseHandler;
    .end local v7    # "url":Ljava/net/URL;
    .end local v8    # "xmlreader":Lorg/xml/sax/XMLReader;
    :cond_1
    :goto_0
    return v5

    .line 98
    .restart local v1    # "factory":Ljavax/xml/parsers/SAXParserFactory;
    .restart local v3    # "is":Lorg/xml/sax/InputSource;
    .restart local v4    # "parser":Ljavax/xml/parsers/SAXParser;
    .restart local v6    # "theRssHandler":Lcom/samsung/musicplus/bigpond/BigPondParseHandler;
    .restart local v7    # "url":Ljava/net/URL;
    .restart local v8    # "xmlreader":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 88
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "factory":Ljavax/xml/parsers/SAXParserFactory;
    .end local v3    # "is":Lorg/xml/sax/InputSource;
    .end local v4    # "parser":Ljavax/xml/parsers/SAXParser;
    .end local v6    # "theRssHandler":Lcom/samsung/musicplus/bigpond/BigPondParseHandler;
    .end local v7    # "url":Ljava/net/URL;
    .end local v8    # "xmlreader":Lorg/xml/sax/XMLReader;
    :catch_1
    move-exception v0

    .line 89
    .local v0, "e":Ljava/net/MalformedURLException;
    :try_start_2
    const-string v9, "MpBigPondTop10Parser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getData() MalformedURLException "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 90
    const/4 v5, 0x4

    .line 95
    if-eqz v2, :cond_1

    .line 97
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 98
    :catch_2
    move-exception v0

    .line 99
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v9, "MpBigPondTop10Parser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getData() Exception "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 93
    const/4 v5, 0x4

    .line 95
    if-eqz v2, :cond_1

    .line 97
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 98
    :catch_4
    move-exception v0

    .line 99
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 95
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    if-eqz v2, :cond_2

    .line 97
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 100
    :cond_2
    :goto_1
    throw v9

    .line 98
    :catch_5
    move-exception v0

    .line 99
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public setImage()I
    .locals 11

    .prologue
    .line 107
    const/4 v6, 0x2

    .line 109
    .local v6, "responseCode":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_0
    iget-object v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_2

    .line 110
    iget-object v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    .line 111
    .local v7, "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    if-nez v7, :cond_0

    .line 112
    const/4 v8, 0x7

    .line 139
    .end local v7    # "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :goto_1
    return v8

    .line 114
    .restart local v7    # "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :cond_0
    new-instance v5, Ljava/net/URL;

    invoke-virtual {v7}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getImage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 116
    .local v5, "imageURL":Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 117
    .local v2, "conn":Ljava/net/URLConnection;
    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V

    .line 118
    const-string v8, "MpBigPondTop10Parser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setImage() Image conntect success "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 120
    .local v1, "buff":Ljava/io/BufferedInputStream;
    const-string v8, "MpBigPondTop10Parser"

    const-string v9, "setImage() BufferedInputStream success"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 122
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 123
    const-string v8, "MpBigPondTop10Parser"

    const-string v9, "setImage() Image is null "

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :goto_2
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 109
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 125
    :cond_1
    iget-object v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    invoke-virtual {v8, v0}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 126
    const-string v8, "MpBigPondTop10Parser"

    const-string v9, "setImage() setBitmap"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 132
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "buff":Ljava/io/BufferedInputStream;
    .end local v2    # "conn":Ljava/net/URLConnection;
    .end local v5    # "imageURL":Ljava/net/URL;
    .end local v7    # "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :catch_0
    move-exception v3

    .line 133
    .local v3, "e":Ljava/net/MalformedURLException;
    const-string v8, "MpBigPondTop10Parser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setImage() MalformedURLException "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v6, 0x7

    .end local v3    # "e":Ljava/net/MalformedURLException;
    :goto_3
    move v8, v6

    .line 139
    goto :goto_1

    .line 131
    :cond_2
    const/4 v6, 0x6

    goto :goto_3

    .line 135
    :catch_1
    move-exception v3

    .line 136
    .local v3, "e":Ljava/io/IOException;
    const-string v8, "MpBigPondTop10Parser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setImage() IOException "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v6, 0x7

    goto :goto_3
.end method

.method public setNextImage(Z)I
    .locals 10
    .param p1, "first"    # Z

    .prologue
    const/4 v7, 0x7

    const/4 v8, 0x0

    .line 143
    if-eqz p1, :cond_0

    .line 144
    iput v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    .line 147
    :cond_0
    const/4 v5, 0x2

    .line 149
    .local v5, "responseCode":I
    :try_start_0
    iget-object v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    iget v9, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    if-gt v8, v9, :cond_2

    .line 186
    :cond_1
    :goto_0
    return v7

    .line 153
    :cond_2
    iget-object v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    iget v9, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    .line 154
    .local v6, "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    if-eqz v6, :cond_1

    .line 157
    new-instance v4, Ljava/net/URL;

    invoke-virtual {v6}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getImage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 159
    .local v4, "imageURL":Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 160
    .local v2, "conn":Ljava/net/URLConnection;
    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V

    .line 161
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 162
    .local v1, "buff":Ljava/io/BufferedInputStream;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 163
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-nez v0, :cond_4

    .line 164
    const-string v7, "MpBigPondTop10Parser"

    const-string v8, "setImage() Image is null "

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 171
    const/4 v5, 0x5

    .line 172
    iget v7, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    .line 174
    iget v7, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    iget-object v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 175
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    .line 176
    const/4 v5, 0x6

    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "buff":Ljava/io/BufferedInputStream;
    .end local v2    # "conn":Ljava/net/URLConnection;
    .end local v4    # "imageURL":Ljava/net/URL;
    .end local v6    # "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :cond_3
    :goto_2
    move v7, v5

    .line 186
    goto :goto_0

    .line 166
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    .restart local v1    # "buff":Ljava/io/BufferedInputStream;
    .restart local v2    # "conn":Ljava/net/URLConnection;
    .restart local v4    # "imageURL":Ljava/net/URL;
    .restart local v6    # "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :cond_4
    iget-object v7, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mBigPondItemList:Ljava/util/List;

    iget v8, p0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->mImageIndex:I

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    invoke-virtual {v7, v0}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 167
    const-string v7, "MpBigPondTop10Parser"

    const-string v8, "setImage() setBitmap"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 179
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "buff":Ljava/io/BufferedInputStream;
    .end local v2    # "conn":Ljava/net/URLConnection;
    .end local v4    # "imageURL":Ljava/net/URL;
    .end local v6    # "temp":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :catch_0
    move-exception v3

    .line 180
    .local v3, "e":Ljava/net/MalformedURLException;
    const-string v7, "MpBigPondTop10Parser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setImage() MalformedURLException "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v5, 0x7

    .line 185
    goto :goto_2

    .line 182
    .end local v3    # "e":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 183
    .local v3, "e":Ljava/io/IOException;
    const-string v7, "MpBigPondTop10Parser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setImage() IOException "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const/4 v5, 0x7

    goto :goto_2
.end method
