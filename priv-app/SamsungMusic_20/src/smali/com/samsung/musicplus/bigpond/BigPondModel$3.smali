.class Lcom/samsung/musicplus/bigpond/BigPondModel$3;
.super Landroid/os/Handler;
.source "BigPondModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/bigpond/BigPondModel;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/bigpond/BigPondModel;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 147
    const-string v0, "BigPondModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Msg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 153
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$200(Lcom/samsung/musicplus/bigpond/BigPondModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$400(Lcom/samsung/musicplus/bigpond/BigPondModel;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->doRestart()V
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$500(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    goto :goto_0

    .line 161
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->swap()V
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$600(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    .line 163
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->printData()V
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$700(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    .line 166
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->downloadBigPondImage(Z)V
    invoke-static {v0, v3}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$900(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)V

    goto :goto_0

    .line 173
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->swap()V
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$600(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    .line 174
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 175
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->downloadBigPondImage(Z)V
    invoke-static {v0, v4}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$900(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)V

    goto :goto_0

    .line 181
    :sswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # invokes: Lcom/samsung/musicplus/bigpond/BigPondModel;->swap()V
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$600(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    .line 182
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    goto :goto_0

    .line 188
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    goto/16 :goto_0

    .line 194
    :sswitch_5
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    goto/16 :goto_0

    .line 150
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_4
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_5
        0x64 -> :sswitch_0
    .end sparse-switch
.end method
