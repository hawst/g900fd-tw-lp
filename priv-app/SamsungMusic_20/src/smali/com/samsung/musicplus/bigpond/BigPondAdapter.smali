.class public Lcom/samsung/musicplus/bigpond/BigPondAdapter;
.super Landroid/widget/BaseAdapter;
.source "BigPondAdapter.java"

# interfaces
.implements Lcom/samsung/musicplus/bigpond/IBigPondView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BigPondAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

.field private mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/bigpond/IBigPondModel;Lcom/samsung/musicplus/bigpond/IBigPondController;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/musicplus/bigpond/IBigPondModel;
    .param p3, "ctrl"    # Lcom/samsung/musicplus/bigpond/IBigPondController;

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    .line 44
    iput-object p1, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;

    .line 46
    iput-object p3, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

    .line 47
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;

    invoke-interface {v0}, Lcom/samsung/musicplus/bigpond/IBigPondModel;->getData()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;

    invoke-interface {v0}, Lcom/samsung/musicplus/bigpond/IBigPondModel;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;

    invoke-interface {v0}, Lcom/samsung/musicplus/bigpond/IBigPondModel;->getData()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;

    invoke-interface {v0}, Lcom/samsung/musicplus/bigpond/IBigPondModel;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 69
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "pos"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    if-nez p2, :cond_0

    .line 76
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;-><init>()V

    .line 77
    .local v0, "holder":Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;
    iget-object v5, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 78
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f04003e

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 79
    iget-object v5, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 80
    .local v3, "r":Landroid/content/res/Resources;
    const v5, 0x7f02003b

    invoke-static {v3, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mDefaultArtwork:Landroid/graphics/Bitmap;

    .line 82
    const v5, 0x7f0d00cf

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    .line 83
    const v5, 0x7f0d00d0

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 84
    const v5, 0x7f0d00d1

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mArtist:Landroid/widget/TextView;

    .line 85
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 91
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "r":Landroid/content/res/Resources;
    :goto_0
    iget-object v5, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mModel:Lcom/samsung/musicplus/bigpond/IBigPondModel;

    invoke-interface {v5}, Lcom/samsung/musicplus/bigpond/IBigPondModel;->getData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    .line 92
    .local v2, "musicinfo":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 94
    .local v4, "thumbImage":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_1

    .line 95
    iget-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 100
    :goto_1
    iget-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mArtist:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getArtist()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    return-object p2

    .line 88
    .end local v0    # "holder":Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;
    .end local v2    # "musicinfo":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    .end local v4    # "thumbImage":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;
    goto :goto_0

    .line 97
    .restart local v2    # "musicinfo":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    .restart local v4    # "thumbImage":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v5, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    iget-object v6, v0, Lcom/samsung/musicplus/bigpond/BigPondAdapter$ViewHolder;->mDefaultArtwork:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public onUpdate(I)V
    .locals 4
    .param p1, "msg"    # I

    .prologue
    const v1, 0x7f100032

    const/4 v3, 0x0

    const/4 v2, 0x6

    .line 118
    packed-switch p1, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 120
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

    invoke-interface {v0, v2}, Lcom/samsung/musicplus/bigpond/IBigPondController;->onUpdate(I)V

    goto :goto_0

    .line 124
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/bigpond/IBigPondController;->onUpdate(I)V

    goto :goto_0

    .line 128
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 130
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

    invoke-interface {v0, v2}, Lcom/samsung/musicplus/bigpond/IBigPondController;->onUpdate(I)V

    goto :goto_0

    .line 134
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f100033

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 136
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

    invoke-interface {v0, v2}, Lcom/samsung/musicplus/bigpond/IBigPondController;->onUpdate(I)V

    goto :goto_0

    .line 140
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 142
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondAdapter;->mController:Lcom/samsung/musicplus/bigpond/IBigPondController;

    invoke-interface {v0, v2}, Lcom/samsung/musicplus/bigpond/IBigPondController;->onUpdate(I)V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
