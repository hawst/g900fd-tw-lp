.class Lcom/samsung/musicplus/bigpond/BigPondModel$2;
.super Ljava/lang/Thread;
.source "BigPondModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/bigpond/BigPondModel;->downloadBigPondImage(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

.field final synthetic val$first:Z


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    iput-boolean p2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->val$first:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 99
    const-string v0, "BigPondModel"

    const-string v1, "Thread start! downloadBigPondImage()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    iget-object v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondParser:Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;
    invoke-static {v1}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$100(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->val$first:Z

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;->setNextImage(Z)I

    move-result v1

    # setter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$002(Lcom/samsung/musicplus/bigpond/BigPondModel;I)I

    .line 101
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$202(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)Z

    .line 102
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mRestart:Z
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$300(Lcom/samsung/musicplus/bigpond/BigPondModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$400(Lcom/samsung/musicplus/bigpond/BigPondModel;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->this$0:Lcom/samsung/musicplus/bigpond/BigPondModel;

    # getter for: Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I
    invoke-static {v1}, Lcom/samsung/musicplus/bigpond/BigPondModel;->access$000(Lcom/samsung/musicplus/bigpond/BigPondModel;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 105
    :cond_0
    const-string v0, "BigPondModel"

    const-string v1, "Thread end! downloadBigPondImage()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void
.end method
