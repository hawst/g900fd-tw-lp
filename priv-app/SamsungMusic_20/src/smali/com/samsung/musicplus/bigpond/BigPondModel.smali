.class public Lcom/samsung/musicplus/bigpond/BigPondModel;
.super Ljava/lang/Object;
.source "BigPondModel.java"

# interfaces
.implements Lcom/samsung/musicplus/bigpond/IBigPondModel;


# static fields
.field private static final DEBUG:Z = true

.field private static final RSS_URL:Ljava/lang/String; = "https://feeds.rss.bigpond.com/xmlfeed?aid=21&feedId=327680&useragent"

.field private static final TAG:Ljava/lang/String; = "BigPondModel"

.field private static sInst:Lcom/samsung/musicplus/bigpond/BigPondModel;


# instance fields
.field private mBigPondNetworkStatus:I

.field private mBigPondParser:Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;

.field private mDatalists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDatalists_clone:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDisableThread:Z

.field private mResponseHandler:Landroid/os/Handler;

.field private mRestart:Z

.field private mUseThread:Z

.field private mView:Lcom/samsung/musicplus/bigpond/IBigPondView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/bigpond/BigPondModel;->sInst:Lcom/samsung/musicplus/bigpond/BigPondModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    .line 39
    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists_clone:Ljava/util/List;

    .line 41
    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondParser:Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;

    .line 43
    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;

    .line 45
    iput v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    .line 47
    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    .line 49
    iput-boolean v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mRestart:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z

    .line 53
    iput-boolean v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDisableThread:Z

    .line 144
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondModel$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/bigpond/BigPondModel$3;-><init>(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists_clone:Ljava/util/List;

    .line 210
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;

    iget-object v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists_clone:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondParser:Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;

    .line 211
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/bigpond/BigPondModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/bigpond/BigPondModel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;
    .param p1, "x1"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondParser:Lcom/samsung/musicplus/bigpond/BigPondTopTenParser;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/bigpond/BigPondModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/bigpond/BigPondModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mRestart:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/bigpond/BigPondModel;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/bigpond/BigPondModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->doRestart()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/bigpond/BigPondModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->swap()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/bigpond/BigPondModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->printData()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/bigpond/BigPondModel;)Lcom/samsung/musicplus/bigpond/IBigPondView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/bigpond/BigPondModel;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/bigpond/BigPondModel;->downloadBigPondImage(Z)V

    return-void
.end method

.method private connectBigPond()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 68
    iget-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDisableThread:Z

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 72
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z

    .line 73
    iput v1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    .line 74
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondModel$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/bigpond/BigPondModel$1;-><init>(Lcom/samsung/musicplus/bigpond/BigPondModel;)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel$1;->start()V

    goto :goto_0
.end method

.method private doRestart()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 121
    const-string v0, "BigPondModel"

    const-string v1, "doRestart()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iput-boolean v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mRestart:Z

    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists_clone:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 126
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 127
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    iput v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    .line 130
    iput-boolean v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDisableThread:Z

    .line 132
    invoke-virtual {p0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->start()V

    .line 133
    return-void
.end method

.method private downloadBigPondImage(Z)V
    .locals 1
    .param p1, "first"    # Z

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDisableThread:Z

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mUseThread:Z

    .line 95
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    .line 96
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondModel$2;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/bigpond/BigPondModel$2;-><init>(Lcom/samsung/musicplus/bigpond/BigPondModel;Z)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel$2;->start()V

    goto :goto_0
.end method

.method public static getInst()Lcom/samsung/musicplus/bigpond/BigPondModel;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/samsung/musicplus/bigpond/BigPondModel;->sInst:Lcom/samsung/musicplus/bigpond/BigPondModel;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondModel;

    invoke-direct {v0}, Lcom/samsung/musicplus/bigpond/BigPondModel;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/bigpond/BigPondModel;->sInst:Lcom/samsung/musicplus/bigpond/BigPondModel;

    .line 140
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/bigpond/BigPondModel;->sInst:Lcom/samsung/musicplus/bigpond/BigPondModel;

    return-object v0
.end method

.method private printData()V
    .locals 5

    .prologue
    .line 111
    const-string v2, "BigPondModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    .line 114
    .local v1, "info":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    const-string v2, "BigPondModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Title:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v2, "BigPondModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Artist:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 118
    .end local v1    # "info":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :cond_0
    return-void
.end method

.method private swap()V
    .locals 5

    .prologue
    .line 56
    iget-object v3, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 57
    iget-object v4, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists_clone:Ljava/util/List;

    monitor-enter v4

    .line 60
    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists_clone:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    .line 61
    .local v1, "info":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    invoke-virtual {v1}, Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;->makeClone()Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;

    move-result-object v2

    .line 62
    .local v2, "info_new":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    iget-object v3, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    .end local v2    # "info_new":Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    return-void
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/musicplus/bigpond/BigPondTopTenInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDatalists:Ljava/util/List;

    return-object v0
.end method

.method public registerView(Lcom/samsung/musicplus/bigpond/IBigPondView;)V
    .locals 0
    .param p1, "view"    # Lcom/samsung/musicplus/bigpond/IBigPondView;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;

    .line 221
    return-void
.end method

.method public restart()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v2, 0x1

    .line 249
    const-string v0, "BigPondModel"

    const-string v1, "restart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 251
    iput-boolean v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mRestart:Z

    .line 252
    iput-boolean v2, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mDisableThread:Z

    .line 253
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mResponseHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 254
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 225
    iget v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mBigPondNetworkStatus:I

    packed-switch v0, :pswitch_data_0

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 227
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->connectBigPond()V

    goto :goto_0

    .line 235
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/samsung/musicplus/bigpond/BigPondModel;->mView:Lcom/samsung/musicplus/bigpond/IBigPondView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/bigpond/IBigPondView;->onUpdate(I)V

    goto :goto_0

    .line 242
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/musicplus/bigpond/BigPondModel;->connectBigPond()V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
