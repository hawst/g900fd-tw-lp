.class public Lcom/samsung/musicplus/MusicMainActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MusicMainActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.implements Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;


# static fields
.field private static final DEVICES_TAB_ID:Ljava/lang/Integer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MENU_SEARCH:I = 0x7f0d01cb

.field private static final PREF_KEY_CURRENT_TAB:Ljava/lang/String; = "music_current_tab"

.field private static final SAVED_ADDED_DMS:Ljava/lang/String; = "added_dms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final SAVED_TAB_ID:Ljava/lang/String; = "tab_id"

.field private static final TAG:Ljava/lang/String; = "MusicList"

.field public static final TRACK_LAUNCH:Ljava/lang/String; = "track_launch"

.field private static final VERIFICATION_LOG_TAG:Ljava/lang/String; = "VerificationLog"

.field public static sNeedUpdateTab:Z


# instance fields
.field private mDelayedLoading:Z

.field private mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected mIsActionModeEnabled:Z

.field private mIsDmsTabAdded:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mLaunchedFromGalaxyFinder:Z

.field private mLibraryTabFragment:Landroid/app/Fragment;

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mResId:I

.field private mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    const v0, 0x1000b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/MusicMainActivity;->DEVICES_TAB_ID:Ljava/lang/Integer;

    .line 105
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 102
    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    .line 113
    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsActionModeEnabled:Z

    .line 115
    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLaunchedFromGalaxyFinder:Z

    .line 117
    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDelayedLoading:Z

    .line 498
    const v0, 0x7f0d00f2

    iput v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mResId:I

    return-void
.end method

.method private addDmsTab()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    if-nez v0, :cond_0

    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    .line 551
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    sget-object v1, Lcom/samsung/musicplus/MusicMainActivity;->DEVICES_TAB_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->addTab(Ljava/lang/Integer;)V

    .line 553
    :cond_0
    return-void
.end method

.method private buildTabIds()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346
    iget-object v6, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    const-string v7, "tab_menu_list"

    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTab()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "listOrder":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v6, "|"

    invoke-direct {v3, v2, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    .local v3, "strToken":Ljava/util/StringTokenizer;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 351
    .local v5, "tabIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    .line 352
    .local v0, "count":I
    const/4 v4, -0x1

    .line 353
    .local v4, "tabId":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 354
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 355
    const v6, 0x1000a

    if-ne v4, v6, :cond_0

    .line 353
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 358
    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 361
    :cond_1
    iget-boolean v6, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    if-eqz v6, :cond_2

    .line 364
    sget-object v6, Lcom/samsung/musicplus/MusicMainActivity;->DEVICES_TAB_ID:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :cond_2
    return-object v5
.end method

.method private enableActionMode(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 587
    iput-boolean p1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsActionModeEnabled:Z

    .line 589
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/base/list/IActionMode;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/base/list/IActionMode;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/base/list/IActionMode;->setActionModeEnabled(Z)V

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v0, :cond_1

    .line 596
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/samsung/musicplus/widget/tab/ITabController;->setEnableTab(Z)V

    .line 598
    :cond_1
    return-void

    .line 596
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeTabs(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    .line 301
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 302
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 304
    const/4 v1, 0x0

    .line 305
    .local v1, "currentTabId":I
    if-eqz p1, :cond_2

    .line 306
    const-string v2, "tab_id"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 307
    const-string v2, "added_dms"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    .line 315
    :cond_0
    :goto_0
    new-instance v2, Lcom/samsung/musicplus/widget/tab/ActionBarTab;

    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/samsung/musicplus/widget/tab/ActionBarTab;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;Landroid/app/ActionBar;)V

    iput-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    .line 319
    iget-boolean v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDelayedLoading:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1

    .line 320
    iget-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-direct {p0}, Lcom/samsung/musicplus/MusicMainActivity;->buildTabIds()Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/widget/tab/ITabController;->initializeTabs(Ljava/util/ArrayList;)V

    .line 321
    iget-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/widget/tab/ITabController;->setTabSelected(Ljava/lang/Integer;)V

    .line 327
    :cond_1
    return-void

    .line 309
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_0

    .line 310
    iget-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    const-string v3, "music_current_tab"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method private initializeUi(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f0d0102

    const/4 v1, 0x0

    .line 266
    packed-switch v1, :pswitch_data_0

    .line 295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Do not support the selected tab type (= 0)."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :pswitch_0
    const-string v0, "music_player_pref"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/MusicMainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    .line 269
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 274
    const v0, 0x7f040059

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->setContentView(I)V

    .line 276
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/MusicMainActivity;->initializeTabs(Landroid/os/Bundle;)V

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 284
    :pswitch_1
    const v0, 0x7f040056

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->setContentView(I)V

    .line 286
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    .line 288
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    if-nez v0, :cond_0

    .line 289
    new-instance v0, Lcom/samsung/musicplus/contents/LibraryTabFragment;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    .line 290
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private launchTrackActivity(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 530
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 531
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 532
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    const-string v1, "track_title"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 534
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/MusicMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 535
    return-void
.end method

.method private reinitializeTabs()V
    .locals 4

    .prologue
    .line 331
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 336
    .local v0, "currentTabId":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-direct {p0}, Lcom/samsung/musicplus/MusicMainActivity;->buildTabIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->initializeTabs(Ljava/util/ArrayList;)V

    .line 337
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->setTabSelected(Ljava/lang/Integer;)V

    .line 338
    return-void

    .line 334
    .end local v0    # "currentTabId":I
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "music_current_tab"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .restart local v0    # "currentTabId":I
    goto :goto_0
.end method

.method private removeDmsTab()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 560
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getTabCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->removeTab(I)V

    .line 566
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    .line 568
    :cond_0
    return-void
.end method


# virtual methods
.method public actionSconnect()Z
    .locals 2

    .prologue
    .line 481
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    if-eqz v1, :cond_0

    .line 482
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v1, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;->actionSconnect()Z

    move-result v1

    .line 491
    :goto_0
    return v1

    .line 485
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v1, :cond_1

    .line 486
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentContent()Ljava/lang/Object;

    move-result-object v0

    .line 487
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    if-eqz v1, :cond_1

    .line 488
    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;->actionSconnect()Z

    move-result v1

    goto :goto_0

    .line 491
    :cond_1
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->actionSconnect()Z

    move-result v1

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 436
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 437
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 438
    .local v1, "keyCode":I
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 439
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 440
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 441
    :cond_0
    const/4 v2, 0x0

    .line 445
    :goto_0
    return v2

    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 647
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 648
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->enableActionMode(Z)V

    .line 649
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 640
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 641
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->enableActionMode(Z)V

    .line 643
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 231
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/MusicBaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 232
    iget-boolean v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLaunchedFromGalaxyFinder:Z

    if-eqz v1, :cond_0

    .line 233
    const/16 v1, 0x65

    if-ne p1, v1, :cond_0

    const/16 v1, 0x64

    if-ne p2, v1, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->finish()V

    .line 239
    :cond_0
    iput-boolean v4, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLaunchedFromGalaxyFinder:Z

    .line 240
    iget-boolean v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDelayedLoading:Z

    if-eqz v1, :cond_1

    .line 241
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "music_current_tab"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 243
    .local v0, "currentTabId":I
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-direct {p0}, Lcom/samsung/musicplus/MusicMainActivity;->buildTabIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->initializeTabs(Ljava/util/ArrayList;)V

    .line 244
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->setTabSelected(Ljava/lang/Integer;)V

    .line 245
    sput-boolean v4, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    .line 249
    iput-boolean v4, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDelayedLoading:Z

    .line 251
    .end local v0    # "currentTabId":I
    :cond_1
    return-void
.end method

.method public onAddDmsTab()V
    .locals 0

    .prologue
    .line 572
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicMainActivity;->addDmsTab()V

    .line 573
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x20001

    const/4 v5, 0x0

    .line 121
    const-string v3, "VerificationLog"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isGateEnable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    const-string v3, "GATE"

    const-string v4, "<GATE-M> MUSICPLAYER_OPENED </GATE-M>"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_0
    sget-boolean v3, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    if-eqz v3, :cond_1

    .line 134
    sput-boolean v5, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    .line 135
    const/4 p1, 0x0

    .line 137
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 139
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "extra_type"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 140
    .local v2, "type":I
    const-string v3, "intent_extra_from"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLaunchedFromGalaxyFinder:Z

    .line 141
    const-string v3, "track_launch"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 142
    if-ne v2, v6, :cond_3

    .line 143
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->launchPlayerActivity(Landroid/app/Activity;)V

    .line 148
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "track_launch"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 149
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDelayedLoading:Z

    .line 152
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/MusicMainActivity;->initializeUi(Landroid/os/Bundle;)V

    .line 159
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.samsung.musicplus.action.START_SYNC_MUSICSQUARE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 162
    return-void

    .line 145
    .end local v0    # "i":Landroid/content/Intent;
    :cond_3
    const-string v3, "extra_keyword"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "extra_name"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/MusicMainActivity;->launchTrackActivity(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 373
    invoke-static {p0}, Lcom/samsung/musicplus/util/VZCloudUtils;->hasVZCloudpkg(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const/4 v0, 0x0

    const v1, 0x7f0d0024

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 376
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 214
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    .line 215
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 216
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    const-string v1, "music_current_tab"

    iget-object v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 218
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 220
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 223
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isGateEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 224
    const-string v1, "GATE"

    const-string v2, "<GATE-M> MUSICPLAYER_CLOSED </GATE-M>"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_2
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onDestroy()V

    .line 227
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 415
    sparse-switch p1, :sswitch_data_0

    .line 430
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    .line 417
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->launchQueryBrowser()V

    goto :goto_0

    .line 420
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 421
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->openOptionsMenu()V

    move v1, v2

    .line 422
    goto :goto_0

    .line 426
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.delete"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 427
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    move v1, v2

    .line 428
    goto :goto_0

    .line 415
    nop

    :sswitch_data_0
    .sparse-switch
        0x29 -> :sswitch_1
        0x54 -> :sswitch_0
        0x70 -> :sswitch_2
    .end sparse-switch
.end method

.method public onModeChanged(Z)V
    .locals 1
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 653
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onModeChanged(Z)V

    .line 654
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_0

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 656
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/tab/ITabController;->onMultiWindowStateChanged(Z)V

    goto :goto_0

    .line 659
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->onMultiWindowModeChanged(Z)V

    goto :goto_0

    .line 654
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 399
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 410
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 401
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->launchQueryBrowser()V

    goto :goto_0

    .line 405
    :sswitch_1
    const-string v1, "com.vcast.mediamanager.ACTION_MUSIC"

    invoke-static {p0, v1}, Lcom/samsung/musicplus/util/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 399
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d0024 -> :sswitch_1
        0x7f0d01cb -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onPause()V

    .line 191
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 381
    iget-object v3, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v3}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentContent()Ljava/lang/Object;

    move-result-object v2

    .line 382
    .local v2, "o":Ljava/lang/Object;
    const/4 v0, 0x0

    .line 383
    .local v0, "curList":I
    const/4 v1, 0x0

    .line 384
    .local v1, "isEmptyList":Z
    instance-of v3, v2, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v3, :cond_0

    move-object v3, v2

    .line 385
    check-cast v3, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v3}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->getListType()I

    move-result v0

    move-object v3, v2

    .line 386
    check-cast v3, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v3}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->isEmptyList()Z

    move-result v1

    .line 387
    instance-of v3, v2, Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    if-eqz v3, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->hasNoSongs(Landroid/content/Context;)Z

    move-result v1

    .line 391
    :cond_0
    const v3, 0x7f0d01cb

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSearchMenu(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 393
    const v3, 0x7f0d0024

    invoke-static {p0, p1, v3}, Lcom/samsung/musicplus/util/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 394
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3

    .line 391
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onReceivePlayerState(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 462
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    .line 463
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v1, :cond_0

    .line 466
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v1, :cond_1

    .line 470
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 472
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.android.music.settingchanged"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 477
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 475
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1, v0}, Lcom/samsung/musicplus/widget/tab/ITabController;->receivePlayerState(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRemoveDmsTab()V
    .locals 0

    .prologue
    .line 577
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicMainActivity;->removeDmsTab()V

    .line 578
    return-void
.end method

.method public onResizerMoveEnd()V
    .locals 2

    .prologue
    .line 623
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsActionModeEnabled:Z

    if-nez v0, :cond_0

    .line 624
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_0

    .line 636
    :cond_0
    :goto_0
    return-void

    .line 626
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->setSwipable(Z)V

    goto :goto_0

    .line 629
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;->onResizerMoveEnd()V

    goto :goto_0

    .line 624
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResizerMoveStart()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 606
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsActionModeEnabled:Z

    if-nez v0, :cond_0

    .line 607
    packed-switch v1, :pswitch_data_0

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 609
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->setSwipable(Z)V

    goto :goto_0

    .line 612
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;->onResizerMoveStart()V

    goto :goto_0

    .line 607
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 184
    const-string v0, "VerificationLog"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onResume()V

    .line 186
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 255
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 256
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v0

    .line 257
    .local v0, "currentTabID":Ljava/lang/Integer;
    const-string v2, "tab_id"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 260
    const-string v1, "added_dms"

    iget-boolean v2, p0, Lcom/samsung/musicplus/MusicMainActivity;->mIsDmsTabAdded:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 261
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 263
    .end local v0    # "currentTabID":Ljava/lang/Integer;
    :cond_0
    return-void

    .line 257
    .restart local v0    # "currentTabID":Ljava/lang/Integer;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mPreferences:Landroid/content/SharedPreferences;

    const-string v3, "music_current_tab"

    const/4 v4, -0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 450
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 451
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mLibraryTabFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->onServiceConnected()V

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/tab/ITabController;->onServiceConnected()V

    .line 458
    :cond_1
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sp"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 539
    const-string v0, "tab_menu_list"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    .line 542
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onStart()V

    .line 167
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/tab/ITabController;->onStart()V

    .line 170
    :cond_0
    sget-boolean v0, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    if-eqz v0, :cond_1

    .line 171
    const-string v0, "MusicList"

    const-string v1, "Tab pref changed, reinitialize tabs!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicMainActivity;->reinitializeTabs()V

    .line 173
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    .line 175
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDelayedLoading:Z

    if-nez v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->startObserving()V

    .line 180
    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->stopObserving()V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/samsung/musicplus/MusicMainActivity;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/tab/ITabController;->onStop()V

    .line 201
    :cond_1
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onStop()V

    .line 202
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 206
    if-eqz p1, :cond_0

    .line 207
    const-string v0, "VerificationLog"

    const-string v1, "Executed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onWindowFocusChanged(Z)V

    .line 210
    return-void
.end method

.method public setMiniPlayer(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 495
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/MusicMainActivity;->setMiniPlayerLayout(I)V

    .line 496
    return-void
.end method

.method protected setMiniPlayerLayout(I)V
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 506
    iget v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mResId:I

    if-ne v1, p1, :cond_1

    .line 507
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->setMiniPlayerLayout(I)V

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    iput p1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mResId:I

    .line 512
    const/4 v0, 0x0

    .line 513
    .local v0, "isVisible":Z
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mMiniPlayerRoot:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 514
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mMiniPlayerRoot:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v0, 0x1

    .line 515
    :goto_1
    if-eqz v0, :cond_2

    .line 516
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mMiniPlayerRoot:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 519
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->releaseMiniPlayer()V

    .line 520
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->setMiniPlayerLayout(I)V

    .line 522
    iget-object v1, p0, Lcom/samsung/musicplus/MusicMainActivity;->mMiniPlayerRoot:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 525
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->makeMiniPlayer()V

    .line 526
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicMainActivity;->setMiniPlayerVisible(Z)V

    goto :goto_0

    .line 514
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
