.class Lcom/samsung/musicplus/MusicBaseActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "MusicBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/MusicBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/MusicBaseActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/MusicBaseActivity;)V
    .locals 0

    .prologue
    .line 605
    iput-object p1, p0, Lcom/samsung/musicplus/MusicBaseActivity$5;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 608
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 609
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 610
    .local v1, "data":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 611
    const-string v10, "id"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 612
    .local v6, "debugId":J
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v4

    .line 613
    .local v4, "debugCurrentId":J
    const-string v10, "MusicUi"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "action : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " id : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " currentId : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    .end local v4    # "debugCurrentId":J
    .end local v6    # "debugId":J
    :cond_0
    const-string v10, "com.samsung.cover.OPEN"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 618
    const-string v10, "coverOpen"

    const/4 v11, 0x0

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    if-nez v10, :cond_1

    .line 622
    iget-object v10, p0, Lcom/samsung/musicplus/MusicBaseActivity$5;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    new-instance v11, Landroid/content/Intent;

    const-class v12, Lcom/samsung/musicplus/DummyActivity;

    invoke-direct {v11, p1, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v10, v11}, Lcom/samsung/musicplus/MusicBaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 646
    :cond_1
    :goto_0
    return-void

    .line 626
    :cond_2
    const-string v10, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 629
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "id"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 630
    .local v8, "id":J
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    .line 631
    .local v2, "currentId":J
    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-lez v10, :cond_4

    cmp-long v10, v8, v2

    if-eqz v10, :cond_4

    .line 634
    const-string v10, "MusicUi"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Audio id is different so, ignore action : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " id : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " currentId : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 639
    .end local v2    # "currentId":J
    .end local v8    # "id":J
    :cond_4
    iget-object v10, p0, Lcom/samsung/musicplus/MusicBaseActivity$5;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    # invokes: Lcom/samsung/musicplus/MusicBaseActivity;->isStartReservation()Z
    invoke-static {v10}, Lcom/samsung/musicplus/MusicBaseActivity;->access$200(Lcom/samsung/musicplus/MusicBaseActivity;)Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v10, p0, Lcom/samsung/musicplus/MusicBaseActivity$5;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v10}, Lcom/samsung/musicplus/MusicBaseActivity;->isServiceConnected()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 640
    iget-object v10, p0, Lcom/samsung/musicplus/MusicBaseActivity$5;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v10, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    goto :goto_0

    .line 644
    :cond_5
    iget-object v10, p0, Lcom/samsung/musicplus/MusicBaseActivity$5;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    # getter for: Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/musicplus/MusicBaseActivity;->access$300(Lcom/samsung/musicplus/MusicBaseActivity;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
