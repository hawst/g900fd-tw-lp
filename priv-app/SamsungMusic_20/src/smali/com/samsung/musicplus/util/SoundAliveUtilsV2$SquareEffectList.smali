.class final enum Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
.super Ljava/lang/Enum;
.source "SoundAliveUtilsV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/SoundAliveUtilsV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SquareEffectList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum AUTO:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum CUSTOM:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum POP:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

.field public static final enum ROCK:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;


# instance fields
.field private name:I

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 134
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "AUTO"

    const/16 v2, 0x63

    const v3, 0x7f100028

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->AUTO:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 135
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "NORMAL"

    const v2, 0x7f100109

    invoke-direct {v0, v1, v6, v5, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 136
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "CLASSIC"

    const v2, 0x7f10003f

    invoke-direct {v0, v1, v7, v9, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 137
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "ROCK"

    const/4 v2, 0x3

    const v3, 0x7f100140

    invoke-direct {v0, v1, v2, v7, v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->ROCK:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 138
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "POP"

    const v2, 0x7f100128

    invoke-direct {v0, v1, v8, v6, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->POP:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 139
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "JAZZ"

    const v2, 0x7f100098

    invoke-direct {v0, v1, v9, v8, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 140
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    const-string v1, "CUSTOM"

    const/4 v2, 0x6

    const/16 v3, 0xd

    const v4, 0x7f100049

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->CUSTOM:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    .line 133
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->AUTO:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v1, v0, v7

    const/4 v1, 0x3

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->ROCK:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->POP:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->CUSTOM:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "effectValue"    # I
    .param p4, "effectName"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 147
    iput p3, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->value:I

    .line 148
    iput p4, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->name:I

    .line 149
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 133
    const-class v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    return-object v0
.end method

.method public static values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    invoke-virtual {v0}, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    return-object v0
.end method


# virtual methods
.method public getName()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->name:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->value:I

    return v0
.end method
