.class public Lcom/samsung/musicplus/util/SoundAliveUtilsV2;
.super Ljava/lang/Object;
.source "SoundAliveUtilsV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;,
        Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;,
        Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;,
        Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    }
.end annotation


# static fields
.field public static final DEFAULT_BAND_LEVEL:[I

.field public static final DEFAULT_BAND_LEVEL_STRING:Ljava/lang/String; = "0|0|0|0|0|0|0|"

.field public static final DEFAULT_SQUARE_POSITION:[I

.field public static final DEFAULT_SQUARE_POSITION_STRING:Ljava/lang/String; = "2|2|"

.field public static final DEFAULT_STRENTH:[I

.field public static final DEFAULT_STRENTH_STRING:Ljava/lang/String; = "0|0|0|"

.field public static UNDEFINED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, -0x1

    sput v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_STRENTH:[I

    .line 32
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_BAND_LEVEL:[I

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_SQUARE_POSITION:[I

    return-void

    .line 17
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 32
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 46
    :array_2
    .array-data 4
        0x2
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    return-void
.end method

.method public static getNewSoundAliveErrorMessage(II)I
    .locals 2
    .param p0, "audioPath"    # I
    .param p1, "soundAlive"    # I

    .prologue
    .line 213
    sget v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    .line 214
    .local v0, "message":I
    sparse-switch p0, :sswitch_data_0

    .line 245
    :cond_0
    :goto_0
    return v0

    .line 220
    :sswitch_0
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->isSupportSoundAliveSpeaker(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 221
    const v0, 0x7f10016a

    goto :goto_0

    .line 225
    :sswitch_1
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->isSupportSoundAliveBT(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    const v0, 0x7f100167

    goto :goto_0

    .line 230
    :sswitch_2
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->isSupportSoundAliveLineOut(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 231
    const v0, 0x7f100169

    goto :goto_0

    .line 235
    :sswitch_3
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->isSupportSoundAliveHDMI(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    const v0, 0x7f100168

    goto :goto_0

    .line 214
    :sswitch_data_0
    .sparse-switch
        -0xa -> :sswitch_0
        0x2 -> :sswitch_0
        0x380 -> :sswitch_1
        0x400 -> :sswitch_3
        0x1800 -> :sswitch_2
    .end sparse-switch
.end method

.method public static getPresetEffectName(I)I
    .locals 5
    .param p0, "mode"    # I

    .prologue
    .line 204
    invoke-static {}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 205
    .local v1, "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 206
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->getName()I

    move-result v4

    .line 209
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    :goto_1
    return v4

    .line 204
    .restart local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 209
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    :cond_1
    const v4, 0x7f100108

    goto :goto_1
.end method

.method public static getPresetEffectNameInternal(I)I
    .locals 5
    .param p0, "mode"    # I

    .prologue
    .line 125
    invoke-static {}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;->values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 126
    .local v1, "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 127
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;->getName()I

    move-result v4

    .line 130
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;
    :goto_1
    return v4

    .line 125
    .restart local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 130
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectListInternal;
    :cond_1
    const v4, 0x7f100108

    goto :goto_1
.end method

.method public static getSquareEffectName(I)I
    .locals 5
    .param p0, "mode"    # I

    .prologue
    .line 165
    invoke-static {}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 166
    .local v1, "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 167
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;->getName()I

    move-result v4

    .line 170
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    :goto_1
    return v4

    .line 165
    .restart local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 170
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectList;
    :cond_1
    const v4, 0x7f100049

    goto :goto_1
.end method

.method public static getSquareEffectNameInternal(I)I
    .locals 5
    .param p0, "mode"    # I

    .prologue
    .line 86
    invoke-static {}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 87
    .local v1, "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 88
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->getName()I

    move-result v4

    .line 91
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    :goto_1
    return v4

    .line 86
    .restart local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    :cond_1
    const v4, 0x7f100049

    goto :goto_1
.end method
