.class public Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SlinkTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1190
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1193
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->P_CLOUD_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1194
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1197
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_0

    const-string v0, "index_char, title COLLATE LOCALIZED ASC"

    :goto_0
    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1200
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_1

    const-string v0, "index_char"

    :goto_1
    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1202
    iput-object v3, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1203
    iput-object v3, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1205
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 1206
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 1207
    const-string v0, "duration"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 1208
    return-void

    .line 1197
    :cond_0
    const-string v0, "title COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 1200
    :cond_1
    const-string v0, "title"

    goto :goto_1
.end method
