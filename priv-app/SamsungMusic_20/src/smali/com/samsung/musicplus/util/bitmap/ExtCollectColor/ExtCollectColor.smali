.class public Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;
.super Ljava/lang/Object;
.source "ExtCollectColor.java"


# instance fields
.field private final ColorMask:I

.field private final TargetSize:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapSmall:Landroid/graphics/Bitmap;

.field private mFindResultIndex:I

.field private mHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHeight:I

.field private mRand:Ljava/util/Random;

.field private mResourceID:I

.field private mResult:[I

.field private mResultCount:I

.field private mSort:Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/16 v0, 0x3c

    iput v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->TargetSize:I

    .line 14
    const v0, -0x1f1f20

    iput v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->ColorMask:I

    .line 35
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    .line 36
    new-instance v0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mSort:Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

    .line 38
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    .line 39
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Random;->setSeed(J)V

    .line 40
    return-void
.end method

.method private addHash(I)V
    .locals 4
    .param p1, "key"    # I

    .prologue
    .line 120
    const v0, -0x1f1f20

    and-int/2addr p1, v0

    .line 121
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v1, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)[",
            "Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "hash":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mResultCount:I

    new-array v2, v6, [Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;

    .line 130
    .local v2, "nodes":[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;
    const/4 v4, 0x0

    .line 132
    .local v4, "nodes_index":I
    const/4 v3, 0x0

    .line 133
    .local v3, "nodesIsFull":Z
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 135
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v2

    if-ge v1, v6, :cond_0

    .line 136
    new-instance v6, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;

    invoke-direct {v6}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;-><init>()V

    aput-object v6, v2, v1

    .line 135
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 138
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 139
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 141
    .local v5, "temp_key":I
    if-eqz v3, :cond_1

    .line 142
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v5, v6, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->insert(II[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)Z

    goto :goto_1

    .line 144
    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v5, v6, v2, v4}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->insert(II[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;I)Z

    .line 145
    add-int/lit8 v4, v4, 0x1

    .line 146
    array-length v6, v2

    if-ne v4, v6, :cond_0

    .line 147
    const/4 v3, 0x1

    .line 148
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mSort:Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

    invoke-virtual {v6, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;->makeTree([Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)V

    goto :goto_1

    .line 153
    .end local v5    # "temp_key":I
    :cond_2
    iput v4, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mFindResultIndex:I

    .line 155
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mSort:Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

    invoke-virtual {v6, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;->makeTree([Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)V

    .line 156
    return-object v2
.end method

.method private decodeImage()V
    .locals 3

    .prologue
    const/16 v2, 0x3c

    .line 81
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mWidth:I

    .line 106
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHeight:I

    .line 107
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-static {v0, v2, v2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private getInt(III)I
    .locals 3
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I

    .prologue
    .line 226
    const/high16 v1, -0x1000000

    shl-int/lit8 v2, p1, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, p2, 0x8

    or-int/2addr v1, v2

    or-int v0, v1, p3

    .line 227
    .local v0, "result":I
    return v0
.end method

.method private getRGB([I)[I
    .locals 6
    .param p1, "color"    # [I

    .prologue
    .line 231
    array-length v4, p1

    mul-int/lit8 v4, v4, 0x3

    new-array v3, v4, [I

    .line 232
    .local v3, "result":[I
    const/4 v1, 0x0

    .line 233
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_0

    .line 234
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget v4, p1, v0

    const/high16 v5, 0xff0000

    and-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x10

    aput v4, v3, v1

    .line 235
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p1, v0

    const v5, 0xff00

    and-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x8

    aput v4, v3, v2

    .line 236
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p1, v0

    and-int/lit16 v4, v4, 0xff

    aput v4, v3, v1

    .line 233
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    .line 238
    :cond_0
    return-object v3
.end method

.method private getResult([Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)[I
    .locals 3
    .param p1, "nodes"    # [Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;

    .prologue
    .line 175
    iget v2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mResultCount:I

    new-array v1, v2, [I

    .line 176
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 177
    iget v2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mFindResultIndex:I

    if-gt v2, v0, :cond_0

    .line 178
    const/4 v2, 0x0

    aget v2, v1, v2

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->getSimAverage(I)I

    move-result v2

    aput v2, v1, v0

    .line 176
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;->getKey()I

    move-result v2

    aput v2, v1, v0

    goto :goto_1

    .line 184
    :cond_1
    return-object v1
.end method

.method private getSimAverage(I)I
    .locals 9
    .param p1, "color"    # I

    .prologue
    const/16 v8, 0xff

    const/4 v7, 0x1

    .line 188
    const/16 v3, 0x8

    .line 189
    .local v3, "multi":I
    const/high16 v6, 0xff0000

    and-int/2addr v6, p1

    shr-int/lit8 v4, v6, 0x10

    .line 190
    .local v4, "r":I
    const v6, 0xff00

    and-int/2addr v6, p1

    shr-int/lit8 v2, v6, 0x8

    .line 191
    .local v2, "g":I
    and-int/lit16 v1, p1, 0xff

    .line 192
    .local v1, "b":I
    add-int v6, v4, v2

    add-int/2addr v6, v1

    div-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x2

    div-int v0, v6, v3

    .line 194
    .local v0, "average":I
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextBoolean()Z

    move-result v6

    if-ne v6, v7, :cond_3

    .line 195
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    add-int/2addr v4, v6

    .line 198
    :goto_0
    if-le v4, v8, :cond_4

    .line 199
    const/16 v4, 0xff

    .line 203
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextBoolean()Z

    move-result v6

    if-ne v6, v7, :cond_5

    .line 204
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    add-int/2addr v2, v6

    .line 207
    :goto_2
    if-le v2, v8, :cond_6

    .line 208
    const/16 v2, 0xff

    .line 212
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextBoolean()Z

    move-result v6

    if-ne v6, v7, :cond_7

    .line 213
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    add-int/2addr v1, v6

    .line 216
    :goto_4
    if-le v1, v8, :cond_8

    .line 217
    const/16 v1, 0xff

    .line 221
    :cond_2
    :goto_5
    invoke-direct {p0, v4, v2, v1}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->getInt(III)I

    move-result v5

    .line 222
    .local v5, "result":I
    return v5

    .line 197
    .end local v5    # "result":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    sub-int/2addr v4, v6

    goto :goto_0

    .line 200
    :cond_4
    if-gez v4, :cond_0

    .line 201
    const/4 v4, 0x0

    goto :goto_1

    .line 206
    :cond_5
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    sub-int/2addr v2, v6

    goto :goto_2

    .line 209
    :cond_6
    if-gez v2, :cond_1

    .line 210
    const/4 v2, 0x0

    goto :goto_3

    .line 215
    :cond_7
    iget-object v6, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    sub-int/2addr v1, v6

    goto :goto_4

    .line 218
    :cond_8
    if-gez v1, :cond_2

    .line 219
    const/4 v1, 0x0

    goto :goto_5
.end method

.method private insert(II[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)Z
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I
    .param p3, "nodes"    # [Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;

    .prologue
    const/4 v0, 0x0

    .line 160
    aget-object v1, p3, v0

    invoke-virtual {v1}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;->getValue()I

    move-result v1

    if-ge v1, p2, :cond_0

    .line 161
    aget-object v0, p3, v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;->setNode(II)V

    .line 162
    iget-object v0, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mSort:Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

    invoke-virtual {v0, p3}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;->checkTree([Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)V

    .line 163
    const/4 v0, 0x1

    .line 165
    :cond_0
    return v0
.end method

.method private insert(II[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;I)Z
    .locals 1
    .param p1, "key"    # I
    .param p2, "value"    # I
    .param p3, "nodes"    # [Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;
    .param p4, "nodes_count"    # I

    .prologue
    .line 169
    aget-object v0, p3, p4

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;->setKey(I)V

    .line 170
    aget-object v0, p3, p4

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;->setValue(I)V

    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method private runDetail()V
    .locals 4

    .prologue
    .line 111
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHeight:I

    if-ge v0, v3, :cond_1

    .line 112
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v3, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mWidth:I

    if-ge v1, v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    .line 114
    .local v2, "key":I
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->addHash(I)V

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 111
    .end local v2    # "key":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    .end local v1    # "j":I
    :cond_1
    return-void
.end method


# virtual methods
.method public setImage(Landroid/graphics/Bitmap;I)[I
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "resultCount"    # I

    .prologue
    .line 62
    const/4 v2, 0x1

    if-lt p2, v2, :cond_0

    if-nez p1, :cond_1

    .line 63
    :cond_0
    const/4 v0, 0x0

    .line 77
    :goto_0
    return-object v0

    .line 65
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mFindResultIndex:I

    .line 66
    iput-object p1, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mBitmap:Landroid/graphics/Bitmap;

    .line 67
    iput p2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mResultCount:I

    .line 68
    invoke-direct {p0}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->decodeImage()V

    .line 69
    iget-object v2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    .line 70
    invoke-direct {p0}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->runDetail()V

    .line 72
    iget-object v2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mHash:Ljava/util/Hashtable;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;

    move-result-object v1

    .line 73
    .local v1, "nodes":[Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;
    iget-object v2, p0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->mSort:Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorSort;->sort([Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)V

    .line 74
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->getResult([Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/CollectColorNode;)[I

    move-result-object v0

    .line 77
    .local v0, "colors":[I
    goto :goto_0
.end method
