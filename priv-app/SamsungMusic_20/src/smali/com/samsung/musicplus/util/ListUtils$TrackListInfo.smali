.class public Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;
.super Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackListInfo"
.end annotation


# direct methods
.method public constructor <init>(IJ)V
    .locals 4
    .param p1, "list"    # I
    .param p2, "audioId"    # J

    .prologue
    .line 648
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;-><init>()V

    .line 649
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 666
    const-string v0, "MusicListUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TrackListInfo > invalid track list : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Did you miss something? audioId is < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :goto_0
    return-void

    .line 651
    :sswitch_0
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 654
    :sswitch_1
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 657
    :sswitch_2
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 660
    :sswitch_3
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 663
    :sswitch_4
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 649
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x6 -> :sswitch_3
        0x8 -> :sswitch_2
        0x11 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 3
    .param p1, "list"    # I
    .param p2, "keyWord"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 576
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;-><init>()V

    .line 577
    const v0, 0x7f100106

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->noItemTextId:I

    .line 578
    const v0, 0x7f0f000a

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->numberOfTextId:I

    .line 579
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    .line 581
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 641
    const-string v0, "MusicListUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TrackListInfo > invalid track list : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Did you miss something? keyWord is < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :goto_0
    return-void

    .line 583
    :sswitch_0
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$AllTrackQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$AllTrackQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 586
    :sswitch_1
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 589
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 592
    :sswitch_2
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 593
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 596
    :sswitch_3
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 599
    :sswitch_4
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$YearTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$YearTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 602
    :sswitch_5
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 605
    :sswitch_6
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    goto :goto_0

    .line 608
    :sswitch_7
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 609
    # invokes: Lcom/samsung/musicplus/util/ListUtils;->isFavoritePlaylistTrack(Ljava/lang/String;)Z
    invoke-static {p2}, Lcom/samsung/musicplus/util/ListUtils;->access$000(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 612
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 616
    :sswitch_8
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 617
    const v0, 0x7f100105

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->noItemTextId:I

    .line 618
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 621
    :sswitch_9
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 622
    const v0, 0x7f10005b

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->noItemTextId:I

    .line 623
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 626
    :sswitch_a
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 627
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto :goto_0

    .line 630
    :sswitch_b
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;

    invoke-direct {v0, p2}, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 631
    const v0, 0x7f100102

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->noItemTextId:I

    .line 632
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    goto/16 :goto_0

    .line 635
    :sswitch_c
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 636
    const v0, 0x7f0f0008

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->numberOfTextId:I

    .line 637
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->showIndexView:Z

    .line 638
    const v0, 0x7f100101

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;->noItemTextId:I

    goto/16 :goto_0

    .line 581
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_7
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0xa -> :sswitch_8
        0xb -> :sswitch_9
        0xd -> :sswitch_a
        0x11 -> :sswitch_c
        0x24 -> :sswitch_b
    .end sparse-switch
.end method
