.class public Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddToNowPlayingTask"
.end annotation


# instance fields
.field private mList:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;[JZ)V
    .locals 0
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "finish"    # Z

    .prologue
    .line 514
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 515
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->mList:[J

    .line 516
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 520
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->mList:[J

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->enqueue([JI)V

    .line 521
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->mList:[J

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 504
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 526
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0010

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->mMessage:Ljava/lang/String;

    .line 530
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 531
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 504
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
