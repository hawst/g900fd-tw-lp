.class public Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "YearTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1282
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1283
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->YEARS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1284
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_YEAR_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1285
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->selection:Ljava/lang/String;

    .line 1286
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1288
    const-string v0, "year_name DESC"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1289
    const-string v0, "year_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1290
    const-string v0, "year_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1292
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1293
    const-string v0, "year_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1294
    return-void
.end method
