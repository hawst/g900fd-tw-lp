.class public Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GenreTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1298
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;-><init>()V

    .line 1299
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->GENRES_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1300
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_GENRE_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1301
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->selection:Ljava/lang/String;

    .line 1302
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->GENRE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1304
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->GENRE_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1305
    const-string v0, "genre_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1307
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1308
    const-string v0, "genre_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1309
    return-void
.end method
