.class public Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RemoveTask"
.end annotation


# instance fields
.field private mAlbumId:J

.field private mAudioIds:[J

.field private mPositionIds:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;JJJZ)V
    .locals 4
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "albumId"    # J
    .param p4, "audioId"    # J
    .param p6, "positionId"    # J
    .param p8, "finish"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 236
    invoke-direct {p0, p1, p8}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 237
    iput-wide p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mAlbumId:J

    .line 238
    new-array v0, v1, [J

    aput-wide p4, v0, v2

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mAudioIds:[J

    .line 241
    new-array v0, v1, [J

    aput-wide p6, v0, v2

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mPositionIds:[J

    .line 244
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mIsSupportDelayedProgress:Z

    .line 245
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;J[J[JZ)V
    .locals 2
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "albumId"    # J
    .param p4, "audioIds"    # [J
    .param p5, "positionIds"    # [J
    .param p6, "finish"    # Z

    .prologue
    .line 228
    invoke-direct {p0, p1, p6}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 229
    iput-wide p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mAlbumId:J

    .line 230
    iput-object p4, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mAudioIds:[J

    .line 231
    iput-object p5, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mPositionIds:[J

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mIsSupportDelayedProgress:Z

    .line 233
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 249
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mAlbumId:J

    iget-object v4, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mAudioIds:[J

    iget-object v5, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mPositionIds:[J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->removePlayListItem(Landroid/content/Context;J[J[J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 211
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 254
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000e

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->mMessage:Ljava/lang/String;

    .line 258
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 259
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 211
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
