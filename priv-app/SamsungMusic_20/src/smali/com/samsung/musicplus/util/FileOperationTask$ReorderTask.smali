.class public Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReorderTask"
.end annotation


# instance fields
.field private mIsReadyToReorder:Z

.field private mKey:Ljava/lang/String;

.field private mListType:I

.field private mPlaylistId:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;ZJILjava/lang/String;Z)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "finish"    # Z
    .param p3, "playlistId"    # J
    .param p5, "listType"    # I
    .param p6, "key"    # Ljava/lang/String;
    .param p7, "isReadyToReorder"    # Z

    .prologue
    .line 283
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mIsReadyToReorder:Z

    .line 284
    iput-wide p3, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mPlaylistId:J

    .line 285
    iput p5, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mListType:I

    .line 286
    iput-object p6, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mKey:Ljava/lang/String;

    .line 287
    iput-boolean p7, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mIsReadyToReorder:Z

    .line 288
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 11
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mIsReadyToReorder:Z

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->makeReorderPlayListItem(Landroid/content/Context;)V

    .line 294
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mPlaylistId:J

    sget-wide v4, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->sReorderPlayListId:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->movePlayList(Landroid/content/Context;JJ)[J

    .line 313
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mContext:Landroid/content/Context;

    sget-wide v2, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->sReorderPlayListId:J

    iget-wide v4, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mPlaylistId:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->movePlayList(Landroid/content/Context;JJ)[J

    move-result-object v9

    .line 299
    .local v9, "list":[J
    if-eqz v9, :cond_0

    iget-wide v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mPlaylistId:J

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isCurrentPlaylist(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    const-wide/16 v6, -0x1

    .line 303
    .local v6, "audioId":J
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v6

    .line 304
    const/4 v10, -0x1

    .line 305
    .local v10, "position":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    array-length v0, v9

    if-ge v8, v0, :cond_3

    .line 306
    aget-wide v0, v9, v8

    cmp-long v0, v6, v0

    if-nez v0, :cond_2

    .line 307
    move v10, v8

    .line 305
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 310
    :cond_3
    iget v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mListType:I

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mKey:Ljava/lang/String;

    invoke-static {v0, v1, v9, v10}, Lcom/samsung/musicplus/util/ServiceUtils;->reorderQueue(ILjava/lang/String;[JI)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 262
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 318
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 319
    iget-boolean v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mIsReadyToReorder:Z

    if-eqz v1, :cond_0

    .line 320
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 321
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    const v2, 0x20004

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 322
    const-string v1, "list_mode"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 323
    const-string v1, "keyword"

    sget-wide v2, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->sReorderPlayListId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 324
    const-string v1, "playlist_id"

    iget-wide v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mPlaylistId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 325
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 326
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 328
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 262
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
