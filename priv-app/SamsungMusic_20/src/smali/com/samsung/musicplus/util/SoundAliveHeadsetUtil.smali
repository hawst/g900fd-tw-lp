.class public Lcom/samsung/musicplus/util/SoundAliveHeadsetUtil;
.super Ljava/lang/Object;
.source "SoundAliveHeadsetUtil.java"


# static fields
.field public static final BT_SOUNDALIVE_ACTIVATE_ACTION:Ljava/lang/String; = "com.sec.samsungsound.ACTION_SOUNDALIVE_CHANGED"

.field private static final MUSIC_SOUNDALIVE_ACTIVATE_ACTION:Ljava/lang/String; = "com.sec.music.ACTION_SOUNDALIVE_CHANGED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static notifySoundAliveChanged(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.music.ACTION_SOUNDALIVE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 31
    return-void
.end method
