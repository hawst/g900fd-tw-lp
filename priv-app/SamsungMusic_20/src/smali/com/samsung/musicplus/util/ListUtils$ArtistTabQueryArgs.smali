.class public Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtistTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1249
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;-><init>()V

    .line 1250
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1251
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_ARTIST_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1252
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->selection:Ljava/lang/String;

    .line 1253
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1255
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1257
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1258
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1259
    const-string v0, "artist"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1260
    return-void
.end method
