.class public Lcom/samsung/musicplus/util/AlbumArtUtils;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# static fields
.field private static final ALBUM_ART_CACHE_SIZE:I = 0x3

.field private static final DEAFULT_ART_MAINTAIN_COUNT:I = 0x4

.field public static final DEFAULT_ALBUM_ART_ARTIST:I = 0x7f020037

.field public static final DEFAULT_ALBUM_ART_FOR_SFINDER:Ljava/lang/String; = "music_library_default_medium"

.field public static final DEFAULT_ALBUM_ART_LARGE:I = 0x7f020039

.field public static final DEFAULT_ALBUM_ART_MEDIUM:I = 0x7f02003a

.field public static final DEFAULT_ALBUM_ART_SMALL:I = 0x7f02003b

.field public static final DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

.field private static DEFAULT_ART_CACHE_KEY:Ljava/lang/Object; = null

.field public static final DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

.field private static final GRID_ALBUM_ART_CACHE_SIZE:I = 0x3c

.field private static final LIST_ALBUM_ART_CACHE_SIZE:I = 0xc8

.field public static final MEDIA_PROVIDER_ALBUM_ART:Ljava/lang/String; = "content://media/external/audio/albumart"

.field public static final MUSICPLUS_ALBUM_ART:Ljava/lang/String;

.field private static final NETWORK_ALBUM_ART_CACHE_SIZE:I = 0x64

.field private static final PLAYLIST_IMAGE_VOLUMEPATH:Ljava/lang/String;

.field public static final S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "MusicAlbumUtils"

.field public static final THUMBNAIL_COLUMN:Ljava/lang/String; = "mini_thumb_data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final URI_ARTWORK:Landroid/net/Uri;

.field private static final USER_PLAYLIST_ARTWORK_URI:Landroid/net/Uri;

.field public static final USER_PLAYLIST_ARTWORK_URI_STRING:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static sArtCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sArtColorCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field private static sArtImportantColorCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sDefaultArtCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static sGridArtCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sNetworkArtChache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sSingleArtChache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sSlinkArtCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xc8

    .line 71
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->MUSICPLUS_ALBUM_ART:Ljava/lang/String;

    .line 82
    const-string v0, "content://media/external/audio/albumart"

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    .line 87
    sget-object v0, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

    .line 94
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->DUMMY_ALBUM_URI_STRING:Ljava/lang/String;

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    .line 96
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->URI_ARTWORK:Landroid/net/Uri;

    .line 98
    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI:Landroid/net/Uri;

    .line 104
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI_STRING:Ljava/lang/String;

    .line 132
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v2}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtCache:Landroid/util/LruCache;

    .line 135
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x3c

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sGridArtCache:Landroid/util/LruCache;

    .line 138
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v2}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSlinkArtCache:Landroid/util/LruCache;

    .line 141
    new-instance v0, Landroid/util/LruCache;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSingleArtChache:Landroid/util/LruCache;

    .line 144
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sNetworkArtChache:Landroid/util/LruCache;

    .line 147
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    .line 149
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtColorCache:Ljava/util/HashMap;

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtImportantColorCache:Ljava/util/HashMap;

    .line 213
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ART_CACHE_KEY:Ljava/lang/Object;

    .line 821
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/playlistImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->PLAYLIST_IMAGE_VOLUMEPATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearAlbumArtCache()V
    .locals 1

    .prologue
    .line 262
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 263
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sGridArtCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 264
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtColorCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 265
    return-void
.end method

.method public static clearCaches()V
    .locals 0

    .prologue
    .line 249
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearAlbumArtCache()V

    .line 250
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearNetworkAlbumArtCache()V

    .line 251
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearSingleArtCache()V

    .line 252
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearSlinkArtCAche()V

    .line 253
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearDefaultArtCache()V

    .line 254
    return-void
.end method

.method public static clearDefaultArtCache()V
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 281
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 282
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    .line 284
    :cond_0
    return-void
.end method

.method public static clearNetworkAlbumArtCache()V
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sNetworkArtChache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 277
    return-void
.end method

.method public static clearSingleArtCache()V
    .locals 1

    .prologue
    .line 268
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSingleArtChache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 269
    return-void
.end method

.method public static clearSlinkArtCAche()V
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSlinkArtCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 273
    return-void
.end method

.method public static convertDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 857
    if-nez p0, :cond_1

    .line 887
    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_0
    return-object v0

    .line 862
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    instance-of v6, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v6, :cond_2

    .line 863
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 864
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    instance-of v6, p0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v6, :cond_3

    .line 865
    check-cast p0, Landroid/graphics/drawable/TransitionDrawable;

    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {p0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getBitmapDrawOfTransitionDraw(Landroid/graphics/drawable/TransitionDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    .line 866
    .local v2, "d":Landroid/graphics/drawable/BitmapDrawable;
    if-eqz v2, :cond_0

    .line 867
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 877
    .end local v2    # "d":Landroid/graphics/drawable/BitmapDrawable;
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 878
    .local v4, "width":I
    if-lez v4, :cond_4

    .line 879
    :goto_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 880
    .local v3, "height":I
    if-lez v3, :cond_5

    .line 882
    :goto_2
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 883
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 884
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    invoke-virtual {p0, v7, v7, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 885
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "height":I
    :cond_4
    move v4, v5

    .line 878
    goto :goto_1

    .restart local v3    # "height":I
    :cond_5
    move v3, v5

    .line 880
    goto :goto_2
.end method

.method public static getAlbumartColor(Ljava/lang/String;)[I
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 351
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtColorCache:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method public static getArtCacheKey(ILjava/lang/Object;I)Ljava/lang/String;
    .locals 1
    .param p0, "listType"    # I
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "size"    # I

    .prologue
    .line 402
    invoke-static {p0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtWorkUri(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtCacheKey(Ljava/lang/String;Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getArtCacheKey(Ljava/lang/String;Ljava/lang/Object;I)Ljava/lang/String;
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "size"    # I

    .prologue
    .line 414
    if-nez p0, :cond_0

    .line 416
    const/4 v1, 0x0

    .line 429
    :goto_0
    return-object v1

    .line 418
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 419
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "content://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 420
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    const/16 v1, 0x5c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 422
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 427
    :goto_1
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 428
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 429
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 425
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static getArtWorkUri(I)Ljava/lang/String;
    .locals 2
    .param p0, "listType"    # I

    .prologue
    .line 379
    const v1, 0x1000b

    if-ne p0, v1, :cond_0

    .line 381
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbumArt()Ljava/lang/String;

    move-result-object v0

    .line 389
    .local v0, "artUri":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 382
    .end local v0    # "artUri":Ljava/lang/String;
    :cond_0
    const v1, 0x2000b

    if-ne p0, v1, :cond_1

    .line 383
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

    .restart local v0    # "artUri":Ljava/lang/String;
    goto :goto_0

    .line 384
    .end local v0    # "artUri":Ljava/lang/String;
    :cond_1
    const v1, 0x2000d

    if-ne p0, v1, :cond_2

    .line 385
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    .restart local v0    # "artUri":Ljava/lang/String;
    goto :goto_0

    .line 387
    .end local v0    # "artUri":Ljava/lang/String;
    :cond_2
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    .restart local v0    # "artUri":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getArtworkBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "size"    # I

    .prologue
    .line 443
    const/4 v6, 0x0

    .line 444
    .local v6, "b":Landroid/graphics/Bitmap;
    move-object v7, p1

    .line 445
    .local v7, "colorCacheKey":Ljava/lang/String;
    const-string v0, "content://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p2, :cond_5

    move-object v0, p2

    .line 448
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 450
    .local v2, "id":J
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 453
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 454
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 455
    invoke-static {p0, v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getUserPlayListArtworkPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    .line 456
    .local v8, "path":Ljava/lang/String;
    invoke-static {v8, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getLocalArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 479
    .end local v2    # "id":J
    .end local v8    # "path":Ljava/lang/String;
    .end local p2    # "key":Ljava/lang/Object;
    :cond_1
    :goto_0
    return-object v6

    .line 457
    .restart local v2    # "id":J
    .restart local p2    # "key":Ljava/lang/Object;
    :cond_2
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 458
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "key":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDlnaArtwork(Landroid/content/Context;JI)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0

    .line 459
    .restart local p2    # "key":Ljava/lang/Object;
    :cond_3
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 460
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "key":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getSlinkArtwork(Landroid/content/Context;JI)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0

    .line 462
    .restart local p2    # "key":Ljava/lang/Object;
    :cond_4
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move v4, p3

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQucikWithUri(Landroid/content/Context;Landroid/net/Uri;JII)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0

    .line 464
    .end local v2    # "id":J
    :cond_5
    const-string v0, "http:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 466
    invoke-static {p1, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getNetworkArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0

    .line 467
    :cond_6
    const-string v0, "file:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 469
    invoke-static {p1, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getLocalArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0

    .line 470
    :cond_7
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    invoke-static {p1, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getLocalArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0
.end method

.method private static getArtworkQucikWithUri(Landroid/content/Context;Landroid/net/Uri;JII)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "id"    # J
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    .line 533
    invoke-static/range {p0 .. p5}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuickInternal(Landroid/content/Context;Landroid/net/Uri;JII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 548
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->URI_ARTWORK:Landroid/net/Uri;

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuickInternal(Landroid/content/Context;Landroid/net/Uri;JII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getArtworkQuickInternal(Landroid/content/Context;Landroid/net/Uri;JII)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "albumId"    # J
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    .line 564
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkQuickInternal baseUri: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " w : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " h : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-wide/16 v8, 0x0

    cmp-long v7, p2, v8

    if-gez v7, :cond_1

    .line 568
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkQuickInternal() "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " albumId "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const/4 v0, 0x0

    .line 630
    :cond_0
    :goto_0
    return-object v0

    .line 576
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 577
    .local v4, "res":Landroid/content/ContentResolver;
    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 578
    .local v6, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_6

    .line 579
    const/4 v2, 0x0

    .line 581
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    const-string v7, "r"

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 582
    if-nez v2, :cond_2

    .line 583
    const-string v7, "MusicAlbumUtils"

    const-string v8, "getArtworkQuickInternal() : file does not exist"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 584
    const/4 v0, 0x0

    .line 622
    if-eqz v2, :cond_0

    .line 623
    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 625
    :catch_0
    move-exception v1

    .line 626
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 587
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 591
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v7, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 592
    const/4 v7, 0x0

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 593
    const/4 v7, 0x1

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 594
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 596
    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v8, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {p4, v7, v8}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getSampleSize(III)I

    move-result v7

    iput v7, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 597
    const/4 v7, 0x0

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 599
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 602
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    const/4 v7, -0x1

    if-eq p4, v7, :cond_5

    .line 604
    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v7, p4, :cond_3

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v7, p5, :cond_5

    const/4 v7, -0x1

    if-eq p4, v7, :cond_5

    .line 605
    :cond_3
    const/4 v7, 0x1

    invoke-static {v0, p4, p5, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 608
    .local v5, "tmp":Landroid/graphics/Bitmap;
    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 609
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 611
    :cond_4
    move-object v0, v5

    .line 622
    .end local v5    # "tmp":Landroid/graphics/Bitmap;
    :cond_5
    if-eqz v2, :cond_0

    .line 623
    :try_start_3
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 625
    :catch_1
    move-exception v1

    .line 626
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 615
    .end local v0    # "b":Landroid/graphics/Bitmap;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_2
    move-exception v1

    .line 616
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkQuick "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 622
    if-eqz v2, :cond_6

    .line 623
    :try_start_5
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 630
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    :cond_6
    :goto_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 625
    .restart local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    :catch_3
    move-exception v1

    .line 626
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 617
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 619
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_6
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkQuick "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 622
    if-eqz v2, :cond_6

    .line 623
    :try_start_7
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_1

    .line 625
    :catch_5
    move-exception v1

    .line 626
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 621
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 622
    if-eqz v2, :cond_7

    .line 623
    :try_start_8
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 627
    :cond_7
    :goto_2
    throw v7

    .line 625
    :catch_6
    move-exception v1

    .line 626
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private static getBitmapDrawOfTransitionDraw(Landroid/graphics/drawable/TransitionDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 6
    .param p0, "td"    # Landroid/graphics/drawable/TransitionDrawable;

    .prologue
    .line 893
    invoke-virtual {p0}, Landroid/graphics/drawable/TransitionDrawable;->getNumberOfLayers()I

    move-result v2

    .line 894
    .local v2, "total":I
    const-string v3, "CLASSNAME"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TransitionDrawable NumberOfLayers : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 898
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/TransitionDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 899
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    instance-of v3, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_0

    .line 900
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 903
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :goto_1
    return-object v0

    .line 897
    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 903
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static getBitmapOptions(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 10
    .param p0, "imageUrl"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 726
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 727
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x0

    .line 729
    .local v0, "bis":Ljava/io/BufferedInputStream;
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 730
    .local v5, "url":Ljava/net/URL;
    sget-object v6, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {v5, v6}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v2

    .line 731
    .local v2, "conn":Ljava/net/URLConnection;
    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V

    .line 732
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    const/4 v6, 0x1

    :try_start_1
    iput-boolean v6, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 736
    const/4 v6, 0x0

    invoke-static {v1, v6, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 741
    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 743
    if-eqz v1, :cond_0

    .line 744
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v0, v1

    .line 750
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v2    # "conn":Ljava/net/URLConnection;
    .end local v5    # "url":Ljava/net/URL;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    :cond_1
    :goto_0
    return-object v4

    .line 746
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "conn":Ljava/net/URLConnection;
    .restart local v5    # "url":Ljava/net/URL;
    :catch_0
    move-exception v3

    .line 747
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "MusicAlbumUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AS: getBitmapSize - IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 749
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_0

    .line 737
    .end local v2    # "conn":Ljava/net/URLConnection;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "url":Ljava/net/URL;
    :catch_1
    move-exception v3

    .line 738
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 741
    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 743
    if-eqz v0, :cond_1

    .line 744
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 746
    :catch_2
    move-exception v3

    .line 747
    const-string v6, "MusicAlbumUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AS: getBitmapSize - IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 741
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 743
    if-eqz v0, :cond_2

    .line 744
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 748
    :cond_2
    :goto_3
    throw v6

    .line 746
    :catch_3
    move-exception v3

    .line 747
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AS: getBitmapSize - IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 741
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "conn":Ljava/net/URLConnection;
    .restart local v5    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 737
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catch_4
    move-exception v3

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_1
.end method

.method public static getCachedArtwork(J)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "artIndex"    # J

    .prologue
    .line 188
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static getCachedNetworkAlbumArt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 207
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    :cond_0
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sNetworkArtChache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public static getCachedSlinkArtwork(J)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "artIndex"    # J

    .prologue
    .line 203
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSlinkArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 217
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ART_CACHE_KEY:Ljava/lang/Object;

    monitor-enter v2

    .line 218
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 219
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    .line 223
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    const v3, 0x7f02003b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    const v3, 0x7f02003a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    const v3, 0x7f020039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    const v3, 0x7f020037

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 231
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Only for valid ids, this is not allowed as cache"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 228
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 234
    :cond_1
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 236
    .local v0, "b":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 237
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 238
    sget-object v1, Lcom/samsung/musicplus/util/AlbumArtUtils;->sDefaultArtCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    :cond_2
    return-object v0
.end method

.method private static getDlnaArtwork(Landroid/content/Context;JI)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "size"    # I

    .prologue
    .line 501
    const/4 v6, 0x0

    .line 502
    .local v6, "b":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 504
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "album_art"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "album_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 508
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 510
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getNetworkArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 513
    :cond_0
    if-eqz v7, :cond_1

    .line 514
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 517
    :cond_1
    return-object v6

    .line 513
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 514
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getGridCachedArtwork(J)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "artIndex"    # J

    .prologue
    .line 192
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sGridArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private static getLocalArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "targetSize"    # I

    .prologue
    const/4 v6, 0x0

    .line 679
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getLocalArtworkQuick path: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " target size : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    if-eqz p0, :cond_0

    const-string v7, ""

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move-object v2, v6

    .line 714
    .end local p0    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v2

    .line 684
    .restart local p0    # "path":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    .line 686
    .local v0, "bis":Ljava/io/BufferedInputStream;
    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 690
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    const-string v7, "file://"

    invoke-virtual {p0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .end local p0    # "path":Ljava/lang/String;
    :goto_1
    invoke-static {p0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getBitmapOptions(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v4

    .line 692
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v8, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {p1, v7, v8}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getSampleSize(III)I

    move-result v7

    iput v7, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 693
    const/4 v7, 0x0

    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 695
    const/4 v7, 0x0

    invoke-static {v1, v7, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 696
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_4

    const/4 v7, -0x1

    if-eq p1, v7, :cond_4

    .line 697
    const/4 v7, 0x1

    invoke-static {v2, p1, p1, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 699
    .local v5, "resizeBm":Landroid/graphics/Bitmap;
    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 700
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 702
    :cond_3
    move-object v2, v5

    .line 709
    .end local v5    # "resizeBm":Landroid/graphics/Bitmap;
    :cond_4
    if-eqz v1, :cond_1

    .line 711
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 712
    :catch_0
    move-exception v3

    .line 713
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "MusicAlbumUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException occured:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 690
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local p0    # "path":Ljava/lang/String;
    :cond_5
    :try_start_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object p0

    goto :goto_1

    .line 705
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    :catch_1
    move-exception v3

    .line 706
    .end local p0    # "path":Ljava/lang/String;
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "MusicAlbumUtils"

    const-string v8, "getPlayListArtwork file not found!"

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 709
    if-eqz v0, :cond_6

    .line 711
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_3
    move-object v2, v6

    .line 714
    goto/16 :goto_0

    .line 712
    .restart local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v3

    .line 713
    .local v3, "e":Ljava/io/IOException;
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException occured:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 709
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v0, :cond_7

    .line 711
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 714
    :cond_7
    :goto_5
    throw v6

    .line 712
    :catch_3
    move-exception v3

    .line 713
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v7, "MusicAlbumUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException occured:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 709
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_4

    .line 705
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catch_4
    move-exception v3

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_2
.end method

.method public static getNetworkArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "link"    # Ljava/lang/String;
    .param p1, "targetSize"    # I

    .prologue
    const/4 v8, 0x0

    .line 777
    const-string v9, "MusicAlbumUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getNetworkArtworkQuick link: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " target size : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    if-eqz p0, :cond_0

    const-string v9, ""

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    move-object v2, v8

    .line 817
    :goto_0
    return-object v2

    .line 783
    :cond_1
    const/4 v0, 0x0

    .line 785
    .local v0, "bis":Ljava/io/BufferedInputStream;
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 786
    .local v7, "url":Ljava/net/URL;
    sget-object v9, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {v7, v9}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v3

    .line 787
    .local v3, "conn":Ljava/net/URLConnection;
    invoke-virtual {v3}, Ljava/net/URLConnection;->connect()V

    .line 789
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 792
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    invoke-static {p0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getBitmapOptions(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v5

    .line 794
    .local v5, "options":Landroid/graphics/BitmapFactory$Options;
    iget v9, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v10, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {p1, v9, v10}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getSampleSize(III)I

    move-result v9

    iput v9, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 796
    const/4 v9, 0x0

    invoke-static {v1, v9, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 797
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    const/4 v9, -0x1

    if-eq p1, v9, :cond_3

    .line 799
    const/4 v9, 0x1

    invoke-static {v2, p1, p1, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 800
    .local v6, "resizeBm":Landroid/graphics/Bitmap;
    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 801
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 803
    :cond_2
    move-object v2, v6

    .line 811
    .end local v6    # "resizeBm":Landroid/graphics/Bitmap;
    :cond_3
    if-eqz v1, :cond_4

    .line 812
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 817
    :cond_4
    :goto_1
    const-string v8, "MusicAlbumUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getNetworkArtworkQuick end link: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " target size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 814
    :catch_0
    move-exception v4

    .line 815
    .local v4, "e":Ljava/io/IOException;
    const-string v8, "MusicAlbumUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getNetworkArtworkQuick: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 806
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "conn":Ljava/net/URLConnection;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    :catch_1
    move-exception v4

    .line 807
    .restart local v4    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v9, "MusicAlbumUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getNetworkArtworkQuick: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 811
    if-eqz v0, :cond_5

    .line 812
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 817
    :cond_5
    :goto_3
    const-string v9, "MusicAlbumUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getNetworkArtworkQuick end link: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " target size : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 814
    :catch_2
    move-exception v4

    .line 815
    const-string v9, "MusicAlbumUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getNetworkArtworkQuick: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 810
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 811
    :goto_4
    if-eqz v0, :cond_6

    .line 812
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 817
    :cond_6
    :goto_5
    const-string v9, "MusicAlbumUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getNetworkArtworkQuick end link: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " target size : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 814
    :catch_3
    move-exception v4

    .line 815
    .restart local v4    # "e":Ljava/io/IOException;
    const-string v9, "MusicAlbumUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getNetworkArtworkQuick: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 810
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "conn":Ljava/net/URLConnection;
    .restart local v7    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v8

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_4

    .line 806
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catch_4
    move-exception v4

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto/16 :goto_2
.end method

.method private static getSampleSize(III)I
    .locals 8
    .param p0, "targetSize"    # I
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 762
    const/4 v0, 0x1

    .line 767
    .local v0, "sampelSize":I
    mul-int/lit8 v1, p0, 0x2

    if-ge p1, v1, :cond_0

    mul-int/lit8 v1, p0, 0x2

    if-lt p2, v1, :cond_1

    .line 768
    :cond_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    int-to-double v4, p0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-double v6, v1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v1, v4

    int-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 772
    :cond_1
    const-string v1, "MusicAlbumUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInSampleSize - sampleSize is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    return v0
.end method

.method public static getSingleArtCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 196
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    :cond_0
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSingleArtChache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private static getSlinkArtwork(Landroid/content/Context;JI)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "size"    # I

    .prologue
    .line 493
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 494
    const/4 v0, 0x0

    .line 497
    :goto_0
    return-object v0

    .line 496
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    .local v4, "threadId":J
    move-object v1, p0

    move-wide v2, p1

    move v6, p3

    .line 497
    invoke-static/range {v1 .. v6}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getAlbumArt(Landroid/content/Context;JJI)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static getUserPlayListArtwork(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pid"    # J

    .prologue
    .line 635
    invoke-static {p0, p1, p2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getUserPlayListArtworkPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 637
    .local v1, "path":Ljava/lang/String;
    const-string v4, "MusicAlbumUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getPlayListArtwork path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0138

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 641
    .local v3, "targetSize":I
    invoke-static {v1, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getLocalArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 642
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 643
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 644
    .local v2, "res":Landroid/content/res/Resources;
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 646
    .end local v2    # "res":Landroid/content/res/Resources;
    :goto_0
    return-object v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static getUserPlayListArtworkPath(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pid"    # J

    .prologue
    const/4 v8, 0x0

    .line 650
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 652
    .local v1, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 653
    .local v7, "path":Ljava/lang/String;
    const/4 v6, 0x0

    .line 655
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "mini_thumb_data"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 659
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 660
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 661
    if-nez v7, :cond_1

    .line 666
    if-eqz v6, :cond_0

    .line 667
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v8

    .line 670
    :goto_0
    return-object v0

    .line 666
    :cond_1
    if-eqz v6, :cond_2

    .line 667
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    .line 670
    goto :goto_0

    .line 666
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 667
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method static isCached(Ljava/lang/String;Ljava/lang/Object;Z)Z
    .locals 4
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "artKey"    # Ljava/lang/Object;
    .param p2, "isListView"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 287
    instance-of v2, p1, Ljava/lang/Long;

    if-eqz v2, :cond_7

    .line 289
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 290
    if-eqz p2, :cond_2

    .line 291
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtCache:Landroid/util/LruCache;

    check-cast p1, Ljava/lang/Long;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-virtual {v2, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 291
    goto :goto_0

    .line 293
    .restart local p1    # "artKey":Ljava/lang/Object;
    :cond_2
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sGridArtCache:Landroid/util/LruCache;

    check-cast p1, Ljava/lang/Long;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-virtual {v2, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 295
    .restart local p1    # "artKey":Ljava/lang/Object;
    :cond_3
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 297
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sNetworkArtChache:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 298
    :cond_4
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 299
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSlinkArtCache:Landroid/util/LruCache;

    check-cast p1, Ljava/lang/Long;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-virtual {v2, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 300
    .restart local p1    # "artKey":Ljava/lang/Object;
    :cond_5
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_6
    move v0, v1

    .line 309
    goto :goto_0

    .line 303
    :cond_7
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 304
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sNetworkArtChache:Landroid/util/LruCache;

    invoke-virtual {v2, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static makeAlbumartColorCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 327
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtColorCache:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 328
    new-instance v0, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;-><init>()V

    .line 329
    .local v0, "collectColor":Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;
    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;->setImage(Landroid/graphics/Bitmap;I)[I

    move-result-object v1

    .line 330
    .local v1, "results":[I
    if-eqz v1, :cond_0

    .line 345
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtColorCache:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    .end local v0    # "collectColor":Lcom/samsung/musicplus/util/bitmap/ExtCollectColor/ExtCollectColor;
    .end local v1    # "results":[I
    :cond_0
    return-void
.end method

.method private static makeArtCacheWithIndex(Ljava/lang/String;JLandroid/graphics/drawable/Drawable;Z)V
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "index"    # J
    .param p3, "d"    # Landroid/graphics/drawable/Drawable;
    .param p4, "isListView"    # Z

    .prologue
    .line 355
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    if-eqz p4, :cond_1

    .line 357
    invoke-static {p1, p2, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeCache(JLandroid/graphics/drawable/Drawable;)V

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-static {p1, p2, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeGridCache(JLandroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 361
    :cond_2
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 363
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeCacheNetworkArtwork(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 364
    :cond_3
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365
    invoke-static {p1, p2, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeCacheSlinkArtwork(JLandroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 366
    :cond_4
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->USER_PLAYLIST_ARTWORK_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private static makeArtCacheWithUri(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 154
    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeCacheNetworkArtwork(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    const-string v0, "file:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private static makeCache(JLandroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p0, "artIndex"    # J
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 164
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    return-void
.end method

.method static makeCache(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "artKey"    # Ljava/lang/Object;
    .param p3, "isListView"    # Z

    .prologue
    .line 313
    if-nez p0, :cond_1

    .line 324
    .end local p2    # "artKey":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 316
    .restart local p2    # "artKey":Ljava/lang/Object;
    :cond_1
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 318
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "artKey":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p1, v0, v1, p0, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeArtCacheWithIndex(Ljava/lang/String;JLandroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    .line 319
    .restart local p2    # "artKey":Ljava/lang/Object;
    :cond_2
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 320
    check-cast p2, Ljava/lang/String;

    .end local p2    # "artKey":Ljava/lang/Object;
    invoke-static {p2, p0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeArtCacheWithUri(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static makeCacheNetworkArtwork(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 178
    if-eqz p1, :cond_0

    .line 179
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sNetworkArtChache:Landroid/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    :cond_0
    return-void
.end method

.method private static makeCacheSlinkArtwork(JLandroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p0, "index"    # J
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 184
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSlinkArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    return-void
.end method

.method private static makeGridCache(JLandroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p0, "artIndex"    # J
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 168
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sGridArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    return-void
.end method

.method static makeSingleCacheArtwork(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 172
    if-eqz p1, :cond_0

    .line 173
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sSingleArtChache:Landroid/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    :cond_0
    return-void
.end method

.method public static removeCache(J)V
    .locals 2
    .param p0, "artIndex"    # J

    .prologue
    .line 257
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->sGridArtCache:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    return-void
.end method

.method public static savePlayListArtwork(Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "pid"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 825
    new-instance v1, Ljava/io/File;

    sget-object v6, Lcom/samsung/musicplus/util/AlbumArtUtils;->PLAYLIST_IMAGE_VOLUMEPATH:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 826
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    .line 827
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-nez v6, :cond_2

    .line 828
    const-string v6, "MusicAlbumUtils"

    const-string v7, "sevePlayListArtwork mkdir error"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .line 853
    :cond_1
    :goto_0
    return-object v4

    .line 832
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/musicplus/util/AlbumArtUtils;->PLAYLIST_IMAGE_VOLUMEPATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 833
    .local v4, "path":Ljava/lang/String;
    const/4 v2, 0x0

    .line 835
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 836
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x50

    invoke-virtual {p0, v6, v7, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 837
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 843
    if-eqz v3, :cond_1

    .line 845
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 846
    :catch_0
    move-exception v0

    .line 847
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 848
    const-string v5, "MusicAlbumUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sevePlayListArtwork initSoftKeyView Error 3 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    const/4 v4, 0x0

    .line 850
    goto :goto_0

    .line 838
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    .line 839
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 840
    const-string v6, "MusicAlbumUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sevePlayListArtwork initSoftKeyView Error 2 :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 843
    if-eqz v2, :cond_3

    .line 845
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_3
    :goto_2
    move-object v4, v5

    .line 850
    goto :goto_0

    .line 846
    :catch_2
    move-exception v0

    .line 847
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 848
    const-string v6, "MusicAlbumUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sevePlayListArtwork initSoftKeyView Error 3 :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    const/4 v4, 0x0

    goto :goto_2

    .line 843
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v2, :cond_4

    .line 845
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 850
    :cond_4
    :goto_4
    throw v5

    .line 846
    :catch_3
    move-exception v0

    .line 847
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 848
    const-string v6, "MusicAlbumUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sevePlayListArtwork initSoftKeyView Error 3 :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    const/4 v4, 0x0

    goto :goto_4

    .line 843
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 838
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
