.class public Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtistTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "audioId"    # J

    .prologue
    .line 1103
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(J)V

    .line 1104
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1107
    const-string v0, "album COLLATE LOCALIZED ASC, album_id, track"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1109
    const-string v0, "album"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1111
    const-string v0, "title != \'\' AND is_music=1 AND _id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1112
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1113
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 1114
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 1115
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 1116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1089
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 1090
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1093
    const-string v0, "album COLLATE LOCALIZED ASC, album_id, track"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1095
    const-string v0, "album"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1097
    const-string v0, "title != \'\' AND is_music=1 AND artist_id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1098
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 1099
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 1100
    return-void
.end method
