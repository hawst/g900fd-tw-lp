.class public Lcom/samsung/musicplus/util/OldAlbumArtLoader;
.super Ljava/lang/Object;
.source "OldAlbumArtLoader.java"

# interfaces
.implements Lcom/samsung/musicplus/util/IAlbumArtLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$1;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$BaseAlbumToken;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;,
        Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final DECODE_COMPELETE:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "MusicAlbumLoader"

.field private static final THREAD_NAME:Ljava/lang/String; = "OldAlbumArtLoader"

.field private static final TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

.field private static volatile sAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;


# instance fields
.field private mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

.field private mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

.field private mThread:Landroid/os/HandlerThread;

.field private final mUiUpdateHandler:Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

.field private final mWorkingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 475
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x106000d

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;

    .line 99
    const-string v0, "MusicAlbumLoader"

    const-string v1, "OldAlbumArtLoader start creating"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "OldAlbumArtLoader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    .line 101
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 102
    new-instance v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mUiUpdateHandler:Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

    .line 103
    const-string v0, "MusicAlbumLoader"

    const-string v1, "OldAlbumArtLoader end creating"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mUiUpdateHandler:Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

    return-object v0
.end method

.method static synthetic access$600()Landroid/graphics/drawable/ColorDrawable;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    return-object v0
.end method

.method public static getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;
    .locals 2

    .prologue
    .line 86
    const-string v0, "MusicAlbumLoader"

    const-string v1, "AlbumArtLoader start getAlbumArtLoader"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->sAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    if-nez v0, :cond_1

    .line 88
    const-class v1, Lcom/samsung/musicplus/util/AlbumArtLoader;

    monitor-enter v1

    .line 89
    :try_start_0
    sget-object v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->sAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->sAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    .line 92
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :cond_1
    const-string v0, "MusicAlbumLoader"

    const-string v1, "AlbumArtLoader end getAlbumArtLoader"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->sAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    return-object v0

    .line 92
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZ)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I
    .param p7, "needCache"    # Z
    .param p8, "isListView"    # Z

    .prologue
    .line 174
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZZ)V

    .line 175
    return-void
.end method

.method private loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I
    .param p7, "wait"    # Z
    .param p8, "needCache"    # Z
    .param p9, "isListView"    # Z

    .prologue
    .line 191
    new-instance v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Lcom/samsung/musicplus/util/OldAlbumArtLoader$1;)V

    .line 192
    .local v0, "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;
    iput-object p1, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->context:Landroid/content/Context;

    .line 193
    iput-object p2, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->uri:Ljava/lang/String;

    .line 194
    iput-object p4, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->iv:Landroid/widget/ImageView;

    .line 195
    iput-object p3, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    .line 196
    iput p5, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->size:I

    .line 197
    iput-boolean p7, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->wait:Z

    .line 198
    iput p6, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->defaultAlbumRes:I

    .line 199
    iput-boolean p8, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->needCache:Z

    .line 200
    iput-boolean p9, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->isListView:Z

    .line 201
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    if-nez v1, :cond_0

    .line 202
    new-instance v1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    .line 204
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 205
    const-string v1, "MusicAlbumLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getArtworkInBackground URI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " iv : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void
.end method


# virtual methods
.method public loadArtistGroupArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;[Landroid/widget/ImageView;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/Object;
    .param p4, "iv"    # [Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 281
    new-instance v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Lcom/samsung/musicplus/util/OldAlbumArtLoader$1;)V

    .line 282
    .local v0, "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;
    iput-object p1, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->context:Landroid/content/Context;

    .line 283
    iput-object p2, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->uri:Ljava/lang/String;

    .line 284
    iput p5, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->size:I

    .line 285
    iput-object p3, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->artKey:Ljava/lang/Object;

    .line 286
    iput-object p4, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->iv:[Landroid/widget/ImageView;

    .line 287
    iput p6, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->defaultAlbumRes:I

    .line 289
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    if-nez v1, :cond_0

    .line 290
    new-instance v1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 293
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # I
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "size"    # I
    .param p5, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 237
    invoke-static {p2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtWorkUri(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;ILandroid/os/Handler;)V

    .line 238
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;ILandroid/os/Handler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "size"    # I
    .param p5, "resultHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v3, 0x0

    .line 254
    new-instance v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;

    invoke-direct {v0, p0, v3}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Lcom/samsung/musicplus/util/OldAlbumArtLoader$1;)V

    .line 255
    .local v0, "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;
    iput-object p1, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->context:Landroid/content/Context;

    .line 256
    iput-object p2, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->uri:Ljava/lang/String;

    .line 257
    iput-object p3, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->artKey:Ljava/lang/Object;

    .line 258
    iput p4, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->size:I

    .line 259
    iput-object p5, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->handler:Landroid/os/Handler;

    .line 260
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    if-nez v1, :cond_0

    .line 261
    new-instance v1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    invoke-virtual {v1, v3}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 264
    iget-object v1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 266
    const-string v1, "MusicAlbumLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getSingleArtworkInBackground URI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    const/4 v7, 0x1

    .line 144
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZ)V

    .line 145
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I
    .param p7, "wait"    # Z

    .prologue
    .line 138
    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZZ)V

    .line 139
    return-void
.end method

.method public loadArtworkWithoutAnimation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 221
    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZ)V

    .line 222
    return-void
.end method

.method public loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 168
    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZ)V

    .line 169
    return-void
.end method

.method public loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I
    .param p7, "wait"    # Z

    .prologue
    .line 162
    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZZZ)V

    .line 163
    return-void
.end method

.method public quit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    const-string v0, "MusicAlbumLoader"

    const-string v1, "AlbumArtLoader quit"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParserSingle:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mAlbumArtParser:Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mUiUpdateHandler:Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 120
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearCaches()V

    .line 121
    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 122
    sput-object v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->sAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    .line 123
    return-void
.end method
