.class public Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaylistTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 997
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 998
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 999
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "wrong keyword for query music play list... keyword is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1003
    :cond_1
    iput-object p1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->keyWord:Ljava/lang/String;

    .line 1008
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1015
    .local v2, "playListId":J
    :goto_0
    const-wide/16 v4, -0xe

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 1018
    iget-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "limit"

    const-string v5, "900"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1019
    const-string v1, "date_added DESC"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1020
    const-string v1, "title != \'\' AND is_music=1 AND recently_added_remove_flag != 1"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1056
    :goto_1
    iput-object v6, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1058
    return-void

    .line 1009
    .end local v2    # "playListId":J
    :catch_0
    move-exception v0

    .line 1010
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 1011
    const-wide/16 v2, -0x1

    .restart local v2    # "playListId":J
    goto :goto_0

    .line 1024
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    const-wide/16 v4, -0xb

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 1025
    invoke-static {v6}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1026
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " COLLATE LOCALIZED ASC"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1027
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1028
    const-string v1, "audio_id"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 1029
    const-string v1, "title != \'\'"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->selection:Ljava/lang/String;

    goto :goto_1

    .line 1032
    :cond_3
    const-wide/16 v4, -0xc

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 1033
    iget-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "limit"

    const-string v5, "50"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1034
    const-string v1, "most_played DESC"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1035
    const-string v1, "title != \'\' AND is_music=1 AND most_played != 0"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->selection:Ljava/lang/String;

    goto :goto_1

    .line 1038
    :cond_4
    const-wide/16 v4, -0xd

    cmp-long v1, v2, v4

    if-nez v1, :cond_5

    .line 1039
    iget-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "limit"

    const-string v5, "50"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1040
    const-string v1, "recently_played DESC"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1041
    const-string v1, "title != \'\' AND is_music=1 AND recently_played != 0"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->selection:Ljava/lang/String;

    goto/16 :goto_1

    .line 1044
    :cond_5
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_6

    .line 1046
    iget-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "limit"

    const-string v5, "0"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    goto/16 :goto_1

    .line 1050
    :cond_6
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1051
    const-string v1, "play_order"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1052
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1053
    const-string v1, "audio_id"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 1054
    const-string v1, "title != \'\'"

    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;->selection:Ljava/lang/String;

    goto/16 :goto_1
.end method
