.class final Lcom/samsung/musicplus/util/UiUtils$1;
.super Ljava/lang/Object;
.source "UiUtils.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$value:Landroid/util/TypedValue;


# direct methods
.method constructor <init>(Landroid/util/TypedValue;)V
    .locals 0

    .prologue
    .line 1648
    iput-object p1, p0, Lcom/samsung/musicplus/util/UiUtils$1;->val$value:Landroid/util/TypedValue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 1651
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1667
    const/4 v4, 0x0

    :goto_0
    return v4

    .line 1653
    :pswitch_0
    const v5, 0x106000d

    invoke-virtual {p1, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 1659
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 1660
    .local v3, "paddingTop":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    .line 1661
    .local v0, "paddingBottom":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 1662
    .local v1, "paddingLeft":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 1663
    .local v2, "paddingRight":I
    iget-object v5, p0, Lcom/samsung/musicplus/util/UiUtils$1;->val$value:Landroid/util/TypedValue;

    iget v5, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1664
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 1651
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
