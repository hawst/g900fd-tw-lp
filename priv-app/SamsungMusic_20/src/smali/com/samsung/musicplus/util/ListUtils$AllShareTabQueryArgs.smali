.class public Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllShareTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1353
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1354
    sget-object v0, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1355
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_SHARE_LIST_TAB_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1357
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->selection:Ljava/lang/String;

    .line 1358
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1359
    const-string v0, "provider_name COLLATE LOCALIZED ASC"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1360
    const-string v0, "provider_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1361
    const-string v0, "provider_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1363
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1364
    const-string v0, "provider_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1365
    const-string v0, "album_art"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;->albumArtCol:Ljava/lang/String;

    .line 1366
    return-void
.end method
