.class public Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllShareTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1169
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1170
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1171
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_SHARE_LIST_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->ALLSHARE_TITLE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1173
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALLSHARE_TITLE_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1174
    const-string v0, "provider_id = ? AND media_type = ? "

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1176
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0x40000

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1180
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 1181
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 1182
    const-string v0, "artist"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 1183
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1184
    const-string v0, "duration"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 1185
    const-string v0, "album_art"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;->albumArtCol:Ljava/lang/String;

    .line 1186
    return-void
.end method
