.class public Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaylistTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1336
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;-><init>()V

    .line 1337
    # invokes: Lcom/samsung/musicplus/util/ListUtils;->getPlaylistContentUri()Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->access$100()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1338
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1340
    const-string v0, "name NOT IN (\'Reorder playlist\')"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->selection:Ljava/lang/String;

    .line 1342
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLIST_NAME_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1344
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLIST_NAME_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1345
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1347
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1348
    const-string v0, "name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1349
    return-void
.end method
