.class Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;
.super Ljava/lang/Object;
.source "SelectionAnimationUtil.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/SelectionAnimationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 13

    .prologue
    .line 59
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/AbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v9

    invoke-virtual {v9, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 61
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v1

    .line 62
    .local v1, "childCount":I
    if-nez v1, :cond_0

    .line 63
    const/4 v9, 0x0

    .line 120
    :goto_0
    return v9

    .line 65
    :cond_0
    const/4 v7, -0x2

    .line 67
    .local v7, "subContainerId":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_f

    .line 68
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 69
    .local v2, "container":Landroid/view/View;
    const v9, 0x7f0d004c

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 70
    .local v0, "chb":Landroid/view/View;
    if-nez v0, :cond_2

    .line 71
    const v9, 0x7f0d00a6

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 72
    .local v5, "stub":Landroid/view/View;
    instance-of v9, v5, Landroid/view/ViewStub;

    if-eqz v9, :cond_1

    .line 73
    check-cast v5, Landroid/view/ViewStub;

    .end local v5    # "stub":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 75
    :cond_1
    const v9, 0x7f0d004c

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_2

    .line 78
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 79
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 80
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/View;->setAlpha(F)V

    .line 87
    :cond_2
    :goto_2
    if-eqz v0, :cond_5

    .line 88
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$200(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)I

    move-result v9

    if-gez v9, :cond_3

    .line 89
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v10

    # setter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I
    invoke-static {v9, v10}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$202(Lcom/samsung/musicplus/util/SelectionAnimationUtil;I)I

    .line 91
    :cond_3
    const/4 v9, -0x2

    if-ne v7, v9, :cond_4

    .line 92
    const/4 v4, 0x0

    .local v4, "idIndex":I
    :goto_3
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimationContainerIds:[I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$300(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)[I

    move-result-object v9

    array-length v9, v9

    if-ge v4, v9, :cond_4

    .line 93
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimationContainerIds:[I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$300(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)[I

    move-result-object v9

    aget v9, v9, v4

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 94
    .local v8, "v":Landroid/view/View;
    if-eqz v8, :cond_7

    .line 95
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimationContainerIds:[I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$300(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)[I

    move-result-object v9

    aget v7, v9, v4

    .line 102
    .end local v4    # "idIndex":I
    .end local v8    # "v":Landroid/view/View;
    :cond_4
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsGridType:Z
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$400(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 103
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 104
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/View;->setAlpha(F)V

    .line 108
    :goto_4
    iget-object v10, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v9

    if-eqz v9, :cond_9

    const/high16 v9, 0x3f800000    # 1.0f

    :goto_5
    # invokes: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->prepareCheckBoxAnimate(Landroid/view/View;IIF)V
    invoke-static {v10, v0, v11, v12, v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$500(Lcom/samsung/musicplus/util/SelectionAnimationUtil;Landroid/view/View;IIF)V

    .line 67
    :cond_5
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 82
    :cond_6
    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 98
    .restart local v4    # "idIndex":I
    .restart local v8    # "v":Landroid/view/View;
    :cond_7
    const/4 v7, -0x1

    .line 92
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 106
    .end local v4    # "idIndex":I
    .end local v8    # "v":Landroid/view/View;
    :cond_8
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 108
    :cond_9
    const/4 v9, 0x0

    goto :goto_5

    .line 111
    :cond_a
    if-lez v7, :cond_b

    .line 112
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 113
    .local v6, "subContainder":Landroid/view/View;
    if-eqz v6, :cond_b

    move-object v2, v6

    .line 115
    .end local v6    # "subContainder":Landroid/view/View;
    :cond_b
    iget-object v11, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    const/4 v12, 0x0

    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v9

    if-eqz v9, :cond_c

    const/4 v9, 0x0

    :goto_7
    iget-object v10, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v10}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v10

    if-eqz v10, :cond_d

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_8
    # invokes: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->prepareCheckBoxAnimate(Landroid/view/View;IIF)V
    invoke-static {v11, v0, v12, v9, v10}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$500(Lcom/samsung/musicplus/util/SelectionAnimationUtil;Landroid/view/View;IIF)V

    .line 116
    iget-object v10, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v9

    if-eqz v9, :cond_e

    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$200(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)I

    move-result v9

    neg-int v9, v9

    :goto_9
    const/4 v11, 0x0

    # invokes: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->prepareContainerAnimate(Landroid/view/View;II)V
    invoke-static {v10, v2, v9, v11}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$600(Lcom/samsung/musicplus/util/SelectionAnimationUtil;Landroid/view/View;II)V

    goto :goto_6

    .line 115
    :cond_c
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$200(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)I

    move-result v9

    neg-int v9, v9

    goto :goto_7

    :cond_d
    const/4 v10, 0x0

    goto :goto_8

    .line 116
    :cond_e
    iget-object v9, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I
    invoke-static {v9}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$200(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)I

    move-result v9

    goto :goto_9

    .line 120
    .end local v0    # "chb":Landroid/view/View;
    .end local v2    # "container":Landroid/view/View;
    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_0
.end method
