.class final enum Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
.super Ljava/lang/Enum;
.source "SoundAliveUtilsV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/SoundAliveUtilsV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SquareEffectListInternal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

.field public static final enum CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

.field public static final enum JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

.field public static final enum NONE:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

.field public static final enum POP:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

.field public static final enum ROCK:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;


# instance fields
.field private name:I

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 57
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    const-string v1, "CLASSIC"

    const v2, 0x7f10003f

    invoke-direct {v0, v1, v5, v4, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    const-string v1, "JAZZ"

    const/16 v2, 0x10

    const v3, 0x7f100098

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    .line 58
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    const-string v1, "NONE"

    const/16 v2, 0xc

    const v3, 0x7f100109

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->NONE:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    .line 59
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    const-string v1, "POP"

    const/16 v2, 0xd

    const v3, 0x7f100128

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->POP:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    .line 60
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    const-string v1, "ROCK"

    const/16 v2, 0xa

    const v3, 0x7f100140

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->ROCK:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    .line 56
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->NONE:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->POP:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->ROCK:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    aput-object v1, v0, v8

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "effectValue"    # I
    .param p4, "effectName"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput p3, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->value:I

    .line 69
    iput p4, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->name:I

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    return-object v0
.end method

.method public static values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    invoke-virtual {v0}, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;

    return-object v0
.end method


# virtual methods
.method public getName()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->name:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$SquareEffectListInternal;->value:I

    return v0
.end method
