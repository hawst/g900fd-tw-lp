.class public interface abstract Lcom/samsung/musicplus/util/UiUtils$Defs;
.super Ljava/lang/Object;
.source "UiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/UiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Defs"
.end annotation


# static fields
.field public static final ADD_TO_FOVORITES:I = 0x7f0d01d2

.field public static final ADD_TO_NOW_PLAYING:I = 0x7f0d01f0

.field public static final ADD_TO_PERSONAL:I = 0x7f0d01d6

.field public static final ADD_TO_PLAYLIST:I = 0x7f0d01cd

.field public static final BASIC_MINI_PLAYER_RESID:I = 0x7f0d00f2

.field public static final CHANGE_ORDER:I = 0x7f0d01fb

.field public static final CHANGE_PLAYER:I = 0x7f0d01ec

.field public static final CHILD_MENU_BASE:I = 0x15

.field public static final CREATE_PLAYLIST:I = 0x7f0d01f8

.field public static final CREATE_PLAYLIST_HEADER:I = 0x7f0d0093

.field public static final DELETE_ITEM:I = 0x7f0d01d0

.field public static final DETAILS:I = 0x7f0d01d8

.field public static final DONE:I = 0x7f0d01ca

.field public static final DOWNLOAD_CONTENT:I = 0x7f0d01f3

.field public static final EDIT_ITEM:I = 0x7f0d01ce

.field public static final END:I = 0x7f0d01e8

.field public static final GO_TO_SETTING:I = 0x7f0d01e6

.field public static final GROUP_PLAY:I = 0x7f0d01f2

.field public static final MOVE_TO_KNOX:I = 0x7f0d01d4

.field public static final MUSIC_SQUARE:I = 0x7f0d01e5

.field public static final NORMAL_LIST:I = 0x13

.field public static final NOW_PLAYING_LIST_HEADER:I = 0x7f0d00a8

.field public static final REMOVE_FROM_FOVORITES:I = 0x7f0d01d3

.field public static final REMOVE_FROM_KNOX:I = 0x7f0d01d5

.field public static final REMOVE_FROM_PERSONAL:I = 0x7f0d01d7

.field public static final REMOVE_ITEM:I = 0x7f0d01cf

.field public static final SAMSUNG_MUSIC:I = 0x7f0d01df

.field public static final SAVE_AS_PLAYLIST:I = 0x7f0d01fa

.field public static final SELECT_ITEM:I = 0x7f0d01dc

.field public static final SELECT_PLAYLIST:I = 0x12

.field public static final SET_AS:I = 0x7f0d01a3

.field public static final SHARE_TRACK_VIA:I = 0x7f0d01f1

.field public static final SHARE_WITH_OTHER_DEVICES:I = 0x7f0d01ef

.field public static final SQUARE_MINI_PLAYER_RESID:I = 0x7f0d00fe

.field public static final SS_MP3_RINGTONE_KOR_LGT:I = 0x7f0d01de

.field public static final USE_SIM1:I = 0x0

.field public static final USE_SIM2:I = 0x1

.field public static final VIA_BLUETOOTH:I = 0x7f0d01e1

.field public static final VIA_PHONE:I = 0x7f0d01e2

.field public static final VIEW_AS:I = 0x7f0d01e4

.field public static final VOLUME:I = 0x7f0d01ed
