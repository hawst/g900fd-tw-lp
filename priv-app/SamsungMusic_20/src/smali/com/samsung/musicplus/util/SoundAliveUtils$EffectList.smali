.class final enum Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
.super Ljava/lang/Enum;
.source "SoundAliveUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/SoundAliveUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "EffectList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum AUTO:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum BASS_BOOST:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum CAFE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum DANCE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum EXTERNALIZATION:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum HALL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum POP:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum ROCK:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum SRS_SURROUND:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum SRS_WOW_HD:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum TREBLE_BOOST:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum TUBE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum USER:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum VIRT51:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

.field public static final enum VOCAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;


# instance fields
.field private name:I

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 13
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "AUTO"

    const/4 v2, -0x1

    const v3, 0x7f100028

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->AUTO:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "NORMAL"

    const v2, 0x7f100109

    invoke-direct {v0, v1, v6, v5, v2}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 14
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "POP"

    const v2, 0x7f100128

    invoke-direct {v0, v1, v7, v6, v2}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->POP:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 15
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "ROCK"

    const v2, 0x7f100140

    invoke-direct {v0, v1, v8, v7, v2}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->ROCK:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 16
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "DANCE"

    const v2, 0x7f10004a

    invoke-direct {v0, v1, v9, v8, v2}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->DANCE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 17
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "JAZZ"

    const/4 v2, 0x5

    const v3, 0x7f100098

    invoke-direct {v0, v1, v2, v9, v3}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 18
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "CLASSIC"

    const/4 v2, 0x6

    const/4 v3, 0x5

    const v4, 0x7f10003f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 19
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "TUBE"

    const/4 v2, 0x7

    const/16 v3, 0x11

    const v4, 0x7f1001ac

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->TUBE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 20
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "VOCAL"

    const/16 v2, 0x8

    const/4 v3, 0x6

    const v4, 0x7f1001c0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->VOCAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 21
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "BASS_BOOST"

    const/16 v2, 0x9

    const/4 v3, 0x7

    const v4, 0x7f10002e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->BASS_BOOST:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 22
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "TREBLE_BOOST"

    const/16 v2, 0xa

    const/16 v3, 0x8

    const v4, 0x7f10017e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->TREBLE_BOOST:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 23
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "VIRT51"

    const/16 v2, 0xb

    const/16 v3, 0x10

    const v4, 0x7f1001bf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->VIRT51:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 24
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "EXTERNALIZATION"

    const/16 v2, 0xc

    const/16 v3, 0xa

    const v4, 0x7f100087

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->EXTERNALIZATION:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 25
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "CAFE"

    const/16 v2, 0xd

    const/16 v3, 0xb

    const v4, 0x7f100039

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->CAFE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 26
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "HALL"

    const/16 v2, 0xe

    const/16 v3, 0xc

    const v4, 0x7f100043

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->HALL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 27
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "USER"

    const/16 v2, 0xf

    const/16 v3, 0xd

    const v4, 0x7f100049

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->USER:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 30
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "SRS_SURROUND"

    const/16 v2, 0x10

    const/16 v3, 0x12

    const v4, 0x7f10016d

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->SRS_SURROUND:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    const-string v1, "SRS_WOW_HD"

    const/16 v2, 0x11

    const/16 v3, 0x13

    const v4, 0x7f10016e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->SRS_WOW_HD:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .line 12
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->AUTO:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->POP:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->ROCK:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->DANCE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->JAZZ:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->CLASSIC:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->TUBE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->VOCAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->BASS_BOOST:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->TREBLE_BOOST:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->VIRT51:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->EXTERNALIZATION:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->CAFE:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->HALL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->USER:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->SRS_SURROUND:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->SRS_WOW_HD:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "effectValue"    # I
    .param p4, "effectName"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->value:I

    .line 39
    iput p4, p0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->name:I

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    .prologue
    .line 12
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->name:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    return-object v0
.end method

.method public static values()[Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    invoke-virtual {v0}, [Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    return-object v0
.end method


# virtual methods
.method public getName()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->name:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->value:I

    return v0
.end method
