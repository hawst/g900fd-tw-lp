.class public Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddToPlaylistTask"
.end annotation


# instance fields
.field private mIsPlaylistFull:Z

.field private mLaunchTrack:Z

.field private mList:[J

.field private mPlaylistId:J

.field private mTrackName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;[JJZ)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "playlistId"    # J
    .param p5, "finish"    # Z

    .prologue
    const/4 v0, 0x0

    .line 350
    invoke-direct {p0, p1, p5}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 337
    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mLaunchTrack:Z

    .line 341
    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mIsPlaylistFull:Z

    .line 351
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mList:[J

    .line 352
    iput-wide p3, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mPlaylistId:J

    .line 353
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;[JJZZLjava/lang/String;)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "playlistId"    # J
    .param p5, "finish"    # Z
    .param p6, "launchTrack"    # Z
    .param p7, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 357
    invoke-direct {p0, p1, p5}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 337
    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mLaunchTrack:Z

    .line 341
    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mIsPlaylistFull:Z

    .line 358
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mList:[J

    .line 359
    iput-wide p3, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mPlaylistId:J

    .line 360
    iput-boolean p6, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mLaunchTrack:Z

    .line 361
    iput-object p7, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mTrackName:Ljava/lang/String;

    .line 362
    return-void
.end method

.method public static makePlaylist(Landroid/content/Context;ILandroid/app/FragmentManager;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # I
    .param p2, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 397
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->makePlaylist(Landroid/content/Context;ILandroid/app/FragmentManager;[J)V

    .line 398
    return-void
.end method

.method public static makePlaylist(Landroid/content/Context;ILandroid/app/FragmentManager;[J)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # I
    .param p2, "fm"    # Landroid/app/FragmentManager;
    .param p3, "selectedId"    # [J

    .prologue
    const/4 v3, 0x6

    .line 402
    # invokes: Lcom/samsung/musicplus/util/FileOperationTask;->getPlaylistCount(Landroid/content/Context;)I
    invoke-static {p0}, Lcom/samsung/musicplus/util/FileOperationTask;->access$000(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_2

    .line 403
    const-string v1, "edit_title"

    invoke-virtual {p2, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 404
    .local v0, "fg":Landroid/app/Fragment;
    if-nez v0, :cond_0

    .line 405
    if-nez p3, :cond_1

    .line 406
    new-instance v1, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {v1, p1, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;-><init>(II)V

    const-string v2, "edit_title"

    invoke-virtual {v1, p2, v2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 419
    .end local v0    # "fg":Landroid/app/Fragment;
    :cond_0
    :goto_0
    return-void

    .line 409
    .restart local v0    # "fg":Landroid/app/Fragment;
    :cond_1
    new-instance v1, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {v1, p1, v3, p3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;-><init>(II[J)V

    const-string v2, "edit_title"

    invoke-virtual {v1, p2, v2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 414
    .end local v0    # "fg":Landroid/app/Fragment;
    :cond_2
    const/high16 v1, 0x7f100000

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mIsPlaylistFull:Z

    if-eqz v0, :cond_0

    .line 373
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 375
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mList:[J

    iget-wide v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mPlaylistId:J

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->addToPlaylistInternal(Landroid/content/Context;[JJ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 331
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected makeSongListToAdd(Landroid/content/Context;[JJ)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ids"    # [J
    .param p3, "playlistid"    # J

    .prologue
    const/16 v7, 0x3e8

    .line 422
    array-length v4, p2

    .line 423
    .local v4, "size":I
    invoke-static {p1, p3, p4}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->getPlaylistSongCount(Landroid/content/Context;J)I

    move-result v0

    .line 424
    .local v0, "base":I
    add-int v5, v0, v4

    if-le v5, v7, :cond_0

    .line 425
    const v5, 0x7f100001

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 428
    if-lt v0, v7, :cond_1

    .line 429
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mIsPlaylistFull:Z

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    rsub-int v2, v0, 0x3e8

    .line 435
    .local v2, "maxSize":I
    new-array v3, v2, [J

    .line 436
    .local v3, "newIds":[J
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 437
    aget-wide v6, p2, v1

    aput-wide v6, v3, v1

    .line 436
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 439
    :cond_2
    iput-object v3, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mList:[J

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 380
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_0

    .line 381
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0011

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mMessage:Ljava/lang/String;

    .line 384
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 385
    iget-boolean v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mLaunchTrack:Z

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 387
    .local v0, "i":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 388
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, "list_type"

    const v2, 0x20004

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 389
    const-string v1, "keyword"

    iget-wide v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mPlaylistId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    const-string v1, "track_title"

    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mTrackName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 392
    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 394
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 331
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 366
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mList:[J

    iget-wide v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->mPlaylistId:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->makeSongListToAdd(Landroid/content/Context;[JJ)V

    .line 367
    invoke-super {p0}, Lcom/samsung/musicplus/util/FileOperationTask;->onPreExecute()V

    .line 368
    return-void
.end method
