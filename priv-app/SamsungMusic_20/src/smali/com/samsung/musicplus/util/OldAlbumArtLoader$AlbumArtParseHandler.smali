.class Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;
.super Landroid/os/Handler;
.source "OldAlbumArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/OldAlbumArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumArtParseHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Landroid/os/Looper;)V
    .locals 0
    .param p2, "l"    # Landroid/os/Looper;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    .line 304
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 305
    return-void
.end method

.method private isAlreadyWorking(Landroid/widget/ImageView;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "iv"    # Landroid/widget/ImageView;
    .param p2, "artKey"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 416
    if-nez p1, :cond_0

    .line 437
    :goto_0
    return v1

    .line 419
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$300(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 420
    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$300(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 421
    .local v0, "key":Ljava/lang/Object;
    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 427
    const/4 v1, 0x1

    monitor-exit v2

    goto :goto_0

    .line 436
    .end local v0    # "key":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 435
    .restart local v0    # "key":Ljava/lang/Object;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$300(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private loadAlbum(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 389
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;

    .line 390
    .local v8, "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;
    iget-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->uri:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->iv:Landroid/widget/ImageView;

    iget-object v1, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->isAlreadyWorking(Landroid/widget/ImageView;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    iget-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->context:Landroid/content/Context;

    iget-object v1, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->uri:Ljava/lang/String;

    iget-object v2, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    iget v3, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->size:I

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 397
    .local v7, "b":Landroid/graphics/Bitmap;
    if-nez v7, :cond_2

    .line 398
    const/4 v0, 0x0

    iput-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    .line 405
    :goto_1
    iget-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->uri:Ljava/lang/String;

    iget-object v1, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    iget-boolean v2, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->isListView:Z

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->isCached(Ljava/lang/String;Ljava/lang/Object;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 406
    const/4 v0, 0x0

    iput-boolean v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->needCache:Z

    .line 412
    :goto_2
    iget-object v1, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->iv:Landroid/widget/ImageView;

    iget-boolean v2, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->needCache:Z

    iget-object v3, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    iget-object v4, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    iget v5, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->defaultAlbumRes:I

    iget-boolean v6, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->wait:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->sendToUiHandler(Landroid/widget/ImageView;ZLjava/lang/Object;Landroid/graphics/drawable/Drawable;IZ)V

    goto :goto_0

    .line 400
    :cond_2
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 409
    :cond_3
    iget-object v0, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    iget-object v1, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->uri:Ljava/lang/String;

    iget-object v2, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    iget-boolean v3, v8, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->isListView:Z

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeCache(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_2
.end method

.method private loadAlbumSendExternalModule(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 319
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;

    .line 320
    .local v3, "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;
    iget-object v4, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->context:Landroid/content/Context;

    iget-object v5, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->uri:Ljava/lang/String;

    iget-object v6, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->artKey:Ljava/lang/Object;

    iget v7, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->size:I

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 323
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 324
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 325
    .local v1, "d":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v4, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->uri:Ljava/lang/String;

    iget-object v5, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->artKey:Ljava/lang/Object;

    iget v6, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->size:I

    invoke-static {v4, v5, v6}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtCacheKey(Ljava/lang/String;Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, "key":Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeSingleCacheArtwork(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 328
    .end local v1    # "d":Landroid/graphics/drawable/BitmapDrawable;
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    iget-object v4, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->handler:Landroid/os/Handler;

    iget-object v5, v3, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;->handler:Landroid/os/Handler;

    const/16 v6, 0xc8

    invoke-virtual {v5, v6, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 329
    return-void
.end method

.method private loadMultipleAlbums(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 332
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;

    .line 333
    .local v7, "art":Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->uri:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    const/4 v11, 0x0

    .line 340
    .local v11, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "album_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "artist_id= \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->artKey:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\') GROUP BY ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "album_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 348
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 349
    new-instance v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;

    iget-object v0, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    const/4 v1, 0x0

    invoke-direct {v13, v0, v1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Lcom/samsung/musicplus/util/OldAlbumArtLoader$1;)V

    .line 350
    .local v13, "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;
    invoke-interface {v11}, Landroid/database/Cursor;->getPosition()I

    move-result v12

    .line 351
    .local v12, "position":I
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->iv:[Landroid/widget/ImageView;

    array-length v0, v0

    if-ge v12, v0, :cond_5

    .line 352
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->iv:[Landroid/widget/ImageView;

    aget-object v0, v0, v12

    iput-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->iv:Landroid/widget/ImageView;

    .line 358
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->artKey:Ljava/lang/Object;

    iput-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    .line 359
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->context:Landroid/content/Context;

    iput-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->context:Landroid/content/Context;

    .line 360
    iget v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->size:I

    iput v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->size:I

    .line 361
    const/4 v0, 0x0

    iput-boolean v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->needCache:Z

    .line 362
    iget v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->defaultAlbumRes:I

    iput v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->defaultAlbumRes:I

    .line 364
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 366
    .local v8, "albumId":J
    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    .line 367
    iget-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 368
    iget-object v0, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->context:Landroid/content/Context;

    iget-object v1, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->uri:Ljava/lang/String;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget v3, v7, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;->size:I

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 370
    .local v10, "b":Landroid/graphics/Bitmap;
    if-nez v10, :cond_4

    .line 371
    const/4 v0, 0x0

    iput-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    .line 375
    :goto_2
    iget-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    iget-object v1, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->uri:Ljava/lang/String;

    iget-object v2, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    iget-boolean v3, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->isListView:Z

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->makeCache(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 378
    .end local v10    # "b":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v1, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->iv:Landroid/widget/ImageView;

    iget-boolean v2, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->needCache:Z

    iget-object v3, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->artKey:Ljava/lang/Object;

    iget-object v4, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;

    iget v5, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->defaultAlbumRes:I

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->sendToUiHandler(Landroid/widget/ImageView;ZLjava/lang/Object;Landroid/graphics/drawable/Drawable;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 382
    .end local v8    # "albumId":J
    .end local v12    # "position":I
    .end local v13    # "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;
    :catchall_0
    move-exception v0

    if-eqz v11, :cond_3

    .line 383
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 373
    .restart local v8    # "albumId":J
    .restart local v10    # "b":Landroid/graphics/Bitmap;
    .restart local v12    # "position":I
    .restart local v13    # "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;
    :cond_4
    :try_start_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, v13, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;->d:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 382
    .end local v8    # "albumId":J
    .end local v10    # "b":Landroid/graphics/Bitmap;
    .end local v12    # "position":I
    .end local v13    # "tag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;
    :cond_5
    if-eqz v11, :cond_0

    .line 383
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private sendToUiHandler(Landroid/widget/ImageView;ZLjava/lang/Object;Landroid/graphics/drawable/Drawable;IZ)V
    .locals 6
    .param p1, "iv"    # Landroid/widget/ImageView;
    .param p2, "animation"    # Z
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "d"    # Landroid/graphics/drawable/Drawable;
    .param p5, "defaultRes"    # I
    .param p6, "wait"    # Z

    .prologue
    .line 442
    if-eqz p1, :cond_1

    .line 443
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 444
    .local v1, "key":Ljava/lang/Object;
    if-eqz v1, :cond_1

    invoke-virtual {v1, p3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    if-eqz p6, :cond_0

    .line 450
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Landroid/os/HandlerThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :cond_0
    :goto_0
    new-instance v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;

    iget-object v3, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;-><init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;Lcom/samsung/musicplus/util/OldAlbumArtLoader$1;)V

    .line 456
    .local v2, "uiTag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;
    iput-boolean p2, v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->showAnimation:Z

    .line 457
    iput-object p1, v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    .line 458
    iput-object p3, v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->artKey:Ljava/lang/Object;

    .line 459
    iput-object p4, v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->d:Landroid/graphics/drawable/Drawable;

    .line 460
    iput p5, v2, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->defaultAlbumRes:I

    .line 463
    iget-object v3, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mUiUpdateHandler:Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;
    invoke-static {v3}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$500(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mUiUpdateHandler:Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;
    invoke-static {v4}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$500(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;

    move-result-object v4

    const/16 v5, 0xc8

    invoke-virtual {v4, v5, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 471
    .end local v1    # "key":Ljava/lang/Object;
    .end local v2    # "uiTag":Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;
    :cond_1
    return-void

    .line 451
    .restart local v1    # "key":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 309
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumToken;

    if-eqz v0, :cond_1

    .line 310
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->loadAlbum(Landroid/os/Message;)V

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$MultipleAlbumToken;

    if-eqz v0, :cond_2

    .line 312
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->loadMultipleAlbums(Landroid/os/Message;)V

    goto :goto_0

    .line 313
    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$ExternalAlbumToken;

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$AlbumArtParseHandler;->loadAlbumSendExternalModule(Landroid/os/Message;)V

    goto :goto_0
.end method
