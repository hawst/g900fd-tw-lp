.class public interface abstract Lcom/samsung/musicplus/util/logging/LoggingFeatures;
.super Ljava/lang/Object;
.source "LoggingFeatures.java"


# static fields
.field public static final ADAPT_SOUND:Ljava/lang/String; = "ADSD"

.field public static final DLNA:Ljava/lang/String; = "DLNA"

.field public static final FIND_TAG:Ljava/lang/String; = "FDTG"

.field public static final GESTURE_LEFT_RIGHT:Ljava/lang/String; = "GEST"

.field public static final GRID_VIEW:Ljava/lang/String; = "GRID"

.field public static final LYRICS:Ljava/lang/String; = "LYRC"

.field public static final MENU_ADD_TO_PLAYLIST:Ljava/lang/String; = "MATP"

.field public static final MOVE_TO_PRIVATE:Ljava/lang/String; = "MVTP"

.field public static final MUSIC_SQUARE:Ljava/lang/String; = "MSSQ"

.field public static final PALM_TOUCH:Ljava/lang/String; = "PALM"

.field public static final SAMSUNG_LINK:Ljava/lang/String; = "SSLK"

.field public static final SAMSUNG_LINK_DEVICE_REGISTERED:J = 0x3e8L

.field public static final SAMSUNG_LINK_DEVICE_UNREGISTERED:J = 0x0L

.field public static final SELECTION_MODE_LONG_PRESS:Ljava/lang/String; = "SLLP"

.field public static final SELECTION_MODE_OPTION_DELETE:Ljava/lang/String; = "SLOD"

.field public static final SELECTION_MODE_OPTION_SELECT:Ljava/lang/String; = "SLOS"

.field public static final SELECTION_MODE_USE_DELETE:Ljava/lang/String; = "SLUD"

.field public static final SMART_VOLUME:Ljava/lang/String; = "SMVL"

.field public static final SOUND_ALIVE:Ljava/lang/String; = "SDAL"

.field public static final TURN_OVER_PAUSE:Ljava/lang/String; = "TURN"
