.class public Lcom/samsung/musicplus/util/ListUtils;
.super Ljava/lang/Object;
.source "ListUtils.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/ListUtils$MediaInfo;,
        Lcom/samsung/musicplus/util/ListUtils$SongList;,
        Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$SlinkTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$AllShareTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$YearTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$ArtistTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$BuapTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$AllTrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$QueryArgs;,
        Lcom/samsung/musicplus/util/ListUtils$TabListInfo;,
        Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;,
        Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    }
.end annotation


# static fields
.field public static final ADD_TO_NOWPLAYING_ID:J = -0x11L

.field public static final ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final ALBUM_ORDER_COLUMN:Ljava/lang/String;

.field public static final ALBUM_PROJECTION:[Ljava/lang/String;

.field public static final ALBUM_TAB:I = 0x10002

.field public static final ALBUM_TRACK:I = 0x20002

.field public static ALBUM_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final ALLSHARE_TITLE_ORDER_COLUMN:Ljava/lang/String;

.field public static final ALL_SHARE_EXTERNAL_LOCAL_TRACK:I = 0x2000e

.field public static final ALL_SHARE_EXTERNAL_REMOTE_TRACK:I = 0x2000f

.field public static ALL_SHARE_LIST_TAB_PROJECTION:[Ljava/lang/String; = null

.field public static ALL_SHARE_LIST_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final ALL_SHARE_PROVIDER:Ljava/lang/String; = "content://com.sec.music/all_share/"

.field public static final ALL_SHARE_TAB:I = 0x1000b

.field public static final ALL_SHARE_TRACK:I = 0x2000b

.field public static final ALL_TRACK:I = 0x20001

.field public static ALL_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final ARTIST_ORDER_COLUMN:Ljava/lang/String;

.field public static final ARTIST_TAB:I = 0x10003

.field public static final ARTIST_TRACK:I = 0x20003

.field public static ARTIST_TRACK_PROJECTION:[Ljava/lang/String; = null

.field private static final AUTHORITY:Ljava/lang/String; = "media"

.field public static final BIGPOND_TAB:I = 0x1000c

.field public static final BUAP_TRACK:I = 0x20010

.field public static final BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

.field public static final CATEGORY_TITLE_ID:J = -0xfL

.field public static final COMPOSERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final COMPOSER_ORDER_COLUMN:Ljava/lang/String;

.field public static final COMPOSER_PROJECTION:[Ljava/lang/String;

.field public static final COMPOSER_TAB:I = 0x10008

.field public static final COMPOSER_TRACK:I = 0x20008

.field private static final CONTENT_AUTHORITY_SLASH:Ljava/lang/String; = "content://media/"

.field public static final COUNT_COLUMN:Ljava/lang/String; = "count"

.field public static final CREATE_PLAYLIST_ID:J = -0x10L

.field private static final DEFAULT_MEDIA_INFO_COLS:[Ljava/lang/String;

.field private static final DEFAULT_MUSIC_QUERY_NO_ITEM:Ljava/lang/String; = "_id = \'\' AND is_music=1"

.field public static final DEFAULT_MUSIC_QUERY_WHERE:Ljava/lang/String; = "title != \'\' AND is_music=1"

.field public static final DEFAULT_PLAYLIST_ORDER:Ljava/lang/String; = "-11|-12|-13|-14"

.field public static final DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

.field private static final DLNA_MEDIA_INFO_COLS:[Ljava/lang/String;

.field private static final EXTERNAL:Ljava/lang/String; = "external"

.field public static final FAVORITE_LIST_ID:J = -0xbL

.field public static final FIXED_TAB:Ljava/lang/String; = "65540|131073|65538|65539"

.field public static final FIXED_TAB_BIGPOND:Ljava/lang/String; = "65540|131073|65548|65538|65539"

.field public static final FOLDERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final FOLDER_COUNT:Ljava/lang/String; = "count"

.field public static final FOLDER_PROJECTION:[Ljava/lang/String;

.field public static final FOLDER_TAB:I = 0x10007

.field public static final FOLDER_TRACK:I = 0x20007

.field public static FOLDER_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final GENRES_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final GENRE_ORDER_COLUMN:Ljava/lang/String;

.field public static final GENRE_TAB:I = 0x10006

.field public static final GENRE_TRACK:I = 0x20006

.field public static final LAST_DEFAULT_LIST_ITEM:J = -0x11L

.field public static final LAST_ITEM_DEFAULT_PLAYLIST:J = -0xeL

.field private static final LIST_CLOUD_MASK:I = 0xf00000

.field public static final LIST_CLOUD_S:I = 0x100000

.field public static final LIST_CLOUD_VZW_BUAPLUS:I = 0x200000

.field public static final LIST_CONTENT_TYPE_ALBUM:I = 0x2

.field public static final LIST_CONTENT_TYPE_ALL:I = 0x1

.field public static final LIST_CONTENT_TYPE_ALL_SHARE:I = 0xb

.field public static final LIST_CONTENT_TYPE_ALL_SHARE_EXTERNAL_LOCAL:I = 0xe

.field public static final LIST_CONTENT_TYPE_ALL_SHARE_EXTERNAL_REMOTE:I = 0xf

.field public static final LIST_CONTENT_TYPE_ARTIST:I = 0x3

.field public static final LIST_CONTENT_TYPE_BIGPOND:I = 0xc

.field public static final LIST_CONTENT_TYPE_BUAP:I = 0x10

.field public static final LIST_CONTENT_TYPE_COMPOSER:I = 0x8

.field public static final LIST_CONTENT_TYPE_FOLDER:I = 0x7

.field public static final LIST_CONTENT_TYPE_GENRE:I = 0x6

.field private static final LIST_CONTENT_TYPE_MASK:I = 0xff

.field public static final LIST_CONTENT_TYPE_MUSIC_SQUARE:I = 0xa

.field public static final LIST_CONTENT_TYPE_PLAYLIST:I = 0x4

.field public static final LIST_CONTENT_TYPE_PLAYLIST_USERMADE:I = 0x5

.field public static final LIST_CONTENT_TYPE_RECORDINGS:I = 0x11

.field public static final LIST_CONTENT_TYPE_SEARCH:I = 0x24

.field public static final LIST_CONTENT_TYPE_S_LINK:I = 0xd

.field public static final LIST_CONTENT_TYPE_YEAR:I = 0x9

.field public static final LIST_HEADER_CREATE:I = 0x2

.field public static final LIST_HEADER_EDIT:I = 0x3

.field public static final LIST_HEADER_NORMAL:I = 0x0

.field public static final LIST_HEADER_OK_CANCLE:I = 0x4

.field public static final LIST_HEADER_SELECTABLE:I = 0x1

.field public static final LIST_MODE_ADD_TO:I = 0x10

.field public static final LIST_MODE_ADD_TO_FAVORITE:I = 0x14

.field public static final LIST_MODE_ADD_TO_NOWPLAYING:I = 0x15

.field public static final LIST_MODE_ADD_TO_PLAYLIST:I = 0x1

.field public static final LIST_MODE_ADD_TO_PLAYLIST_FROM_TAB_SELECTOR:I = 0x3

.field public static final LIST_MODE_ADD_TO_THIS_PLAYLIST:I = 0x2

.field public static final LIST_MODE_CREATE_PLAYLIST:I = 0x6

.field public static final LIST_MODE_CREATE_PLAYLIST_ONLY:I = 0x7

.field public static final LIST_MODE_DELETE:I = 0x4

.field public static final LIST_MODE_DOWNLOAD:I = 0xc

.field public static final LIST_MODE_EDIT_ITEM:I = 0x9

.field public static final LIST_MODE_EDIT_PLAYLIST:I = 0x8

.field public static final LIST_MODE_MAKE_AVAILABLE_OFFLINE:I = 0xe

.field public static final LIST_MODE_MOVE_TO_KNOX:I = 0x13

.field public static final LIST_MODE_MOVE_TO_SECRETBOX:I = 0x11

.field public static final LIST_MODE_REMOVE:I = 0x5

.field public static final LIST_MODE_REMOVE_FROM_SECRETBOX:I = 0x12

.field public static final LIST_MODE_REMOVE_OFFLINE:I = 0xf

.field public static final LIST_MODE_REORDER:I = 0xb

.field public static final LIST_MODE_SAVE_AS_PLAYLIST:I = 0xa

.field public static final LIST_MODE_SHARE_VIA:I = 0xd

.field public static final LIST_TYPE_FILTER:I = -0xf0001

.field public static final LIST_TYPE_GRID_VIEW:I = 0x40000

.field private static final LIST_TYPE_MASK:I = 0x30000

.field public static final LIST_TYPE_TAB:I = 0x10000

.field public static final LIST_TYPE_TRACK:I = 0x20000

.field public static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final MOST_PLAYED_LIST_ID:J = -0xcL

.field public static final MUSIC_SQUARE_TAB:I = 0x1000a

.field public static final MUSIC_SQUARE_TRACK:I = 0x2000a

.field public static final NOMAL_MODE:I = 0x0

.field private static final NORMAL_LIST_TYPE_MASK:I = 0x3ffff

.field public static final PERSONAL_MODE:I = 0x1

.field public static final PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final PLAYLISTS_EXTERNAL_CONTENT_URI_INCLUDING_NESTED_LIST:Landroid/net/Uri;

.field public static final PLAYLISTS_EXTERNAL_CONTENT_URI_WITH_OUT_NOTI:Landroid/net/Uri;

.field public static final PLAYLIST_NAME_ORDER_COLUMN:Ljava/lang/String;

.field public static final PLAYLIST_TAB:I = 0x10004

.field public static final PLAYLIST_TAB_USER_MADE:I = 0x10005

.field public static final PLAYLIST_TRACK:I = 0x20004

.field public static final PLAY_LIST_PROJECTION:[Ljava/lang/String;

.field public static PLAY_LIST_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final P_CLOUD_MEDIA_DATA:Ljava/lang/String; = "data"

.field public static final P_CLOUD_MEDIA_DURATION:Ljava/lang/String; = "duration"

.field public static final P_CLOUD_MEDIA_SEED:Ljava/lang/String; = "seed"

.field public static final P_CLOUD_MEDIA_TITLE:Ljava/lang/String; = "title"

.field public static final P_CLOUD_PROVIDER:Ljava/lang/String; = "content://com.sec.pcw/"

.field public static final P_CLOUD_URI:Landroid/net/Uri;

.field public static final RECENTLY_ADDED_LIST_ID:J = -0xeL

.field public static final RECENTLY_ADDED_REMOVE_FLAG:Ljava/lang/String; = "recently_added_remove_flag"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field public static final RECENTLY_PLAYED_LIST_ID:J = -0xdL

.field public static final RECORDINGS:I = 0x20011

.field private static final RECORDINGS_QUERY_WHERE:Ljava/lang/String;

.field public static RECORDINGS_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final SAMSUNG_ARTIST_PROJECTION:[Ljava/lang/String;

.field public static SAMSUNG_FOLDER_PROJECTION:[Ljava/lang/String; = null

.field public static final SAMSUNG_GENRE_PROJECTION:[Ljava/lang/String;

.field public static final SAMSUNG_LINK_TAB:I = 0x1000d

.field public static final SAMSUNG_YEAR_PROJECTION:[Ljava/lang/String;

.field public static final SEARCH_TRACK:I = 0x20024

.field public static SEARCH_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final SEPERATOR:Ljava/lang/String; = "|"

.field private static final S_LINK_MEDIA_INFO_COLS:[Ljava/lang/String;

.field public static final S_LINK_TRACK:I = 0x2000d

.field private static final TAG:Ljava/lang/String; = "MusicListUtils"

.field public static final TAG_BIGPOND:Ljava/lang/String; = "music_bigpond"

.field public static final TAG_MUSIC_LIST:Ljava/lang/String; = "music_list"

.field public static final TAG_MUSIC_LIST_SPLIT_SUB:Ljava/lang/String; = "music_list_split_sub"

.field public static final TITLE_ORDER_COLUMN:Ljava/lang/String;

.field public static TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final UNDEFINDED_LIST_TYPE:I = -0x1

.field public static final UNDEFINED:I = -0x1

.field public static final UNDEFINEDL_MODE:I = -0x1

.field public static final YEARS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final YEAR_NAME:Ljava/lang/String; = "year_name"

.field public static final YEAR_TAB:I = 0x10009

.field public static final YEAR_TRACK:I = 0x20009

.field private static final sEmptyList:[J


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    const-string v0, "content://com.sec.pcw/player/music"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->P_CLOUD_URI:Landroid/net/Uri;

    .line 72
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_0

    const-string v0, "title_pinyin"

    :goto_0
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALLSHARE_TITLE_ORDER_COLUMN:Ljava/lang/String;

    .line 777
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_1

    const-string v0, "title_pinyin"

    :goto_1
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    .line 780
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_2

    const-string v0, "artist_pinyin"

    :goto_2
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    .line 783
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_3

    const-string v0, "album_pinyin"

    :goto_3
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    .line 786
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_4

    const-string v0, "genre_name_pinyin"

    :goto_4
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->GENRE_ORDER_COLUMN:Ljava/lang/String;

    .line 789
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_5

    const-string v0, "composer_pinyin"

    :goto_5
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->COMPOSER_ORDER_COLUMN:Ljava/lang/String;

    .line 792
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_6

    const-string v0, "name_pinyin"

    :goto_6
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLIST_NAME_ORDER_COLUMN:Ljava/lang/String;

    .line 795
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_7

    const-string v0, "_display_name_pinyin"

    :goto_7
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    .line 798
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_8

    const-string v0, "bucket_display_name_pinyin"

    :goto_8
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    .line 1416
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getAlbumContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1418
    const-string v0, "content://media/external/audio/media/music_years"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->YEARS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1421
    const-string v0, "content://media/external/audio/media/music_composers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->COMPOSERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1424
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getGenresContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->GENRES_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1426
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getArtistsContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1428
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getFoldersContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->FOLDERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1430
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1432
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "secFilter"

    const-string v2, "include"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_INCLUDING_NESTED_LIST:Landroid/net/Uri;

    .line 1438
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents;->getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI_WITH_OUT_NOTI:Landroid/net/Uri;

    .line 1514
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "artist_count"

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "numsongs"

    aput-object v2, v0, v1

    :goto_9
    nop

    nop

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_PROJECTION:[Ljava/lang/String;

    .line 1534
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "composer"

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->COMPOSER_ORDER_COLUMN:Ljava/lang/String;

    aput-object v1, v0, v6

    const-string v1, "count"

    aput-object v1, v0, v7

    :goto_a
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->COMPOSER_PROJECTION:[Ljava/lang/String;

    .line 1543
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "genre_name"

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->GENRE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v1, v0, v6

    const-string v1, "count"

    aput-object v1, v0, v7

    :goto_b
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_GENRE_PROJECTION:[Ljava/lang/String;

    .line 1552
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->FOLDER_PROJECTION:[Ljava/lang/String;

    .line 1557
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLIST_NAME_ORDER_COLUMN:Ljava/lang/String;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    .line 1565
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_c

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const-string v1, "bucket_id"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    :goto_c
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_FOLDER_PROJECTION:[Ljava/lang/String;

    .line 1578
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_d

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "number_of_albums"

    aput-object v1, v0, v6

    const-string v1, "number_of_tracks"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    :goto_d
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_ARTIST_PROJECTION:[Ljava/lang/String;

    .line 1591
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "year_name"

    aput-object v1, v0, v5

    const-string v1, "year"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_YEAR_PROJECTION:[Ljava/lang/String;

    .line 1599
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_e

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    :goto_e
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->TRACK_PROJECTION:[Ljava/lang/String;

    .line 1612
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_f

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "track"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    :goto_f
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1626
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "track"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "year_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1634
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_10

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "year_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    :goto_10
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ARTIST_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1649
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_11

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "_display_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "album"

    aput-object v2, v0, v1

    :goto_11
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->FOLDER_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1665
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_12

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "album_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "play_order"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "audio_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_music"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    :goto_12
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1686
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_13

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "provider_id"

    aput-object v1, v0, v5

    const-string v1, "provider_name"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->ALLSHARE_TITLE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    :goto_13
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_SHARE_LIST_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1703
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "provider_id"

    aput-object v1, v0, v4

    const-string v1, "provider_name"

    aput-object v1, v0, v5

    const-string v1, "album_art"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_SHARE_LIST_TAB_PROJECTION:[Ljava/lang/String;

    .line 1708
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "mime_type"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->SEARCH_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1714
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_14

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "date_modified"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v6

    const-string v1, "track"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    :goto_14
    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->RECORDINGS_TRACK_PROJECTION:[Ljava/lang/String;

    .line 1738
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mime_type = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Recordings;->MIME_TYPE_AMR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recordingtype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->RECORDINGS_QUERY_WHERE:Ljava/lang/String;

    .line 2070
    new-array v0, v3, [J

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    .line 2249
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v6

    const-string v1, "duration"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "genre_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->DEFAULT_MEDIA_INFO_COLS:[Ljava/lang/String;

    .line 2258
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "provider_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "extension"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "provider_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "seed"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "genre_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->DLNA_MEDIA_INFO_COLS:[Ljava/lang/String;

    .line 2268
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "device_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/ListUtils;->S_LINK_MEDIA_INFO_COLS:[Ljava/lang/String;

    return-void

    .line 72
    :cond_0
    const-string v0, "title"

    goto/16 :goto_0

    .line 777
    :cond_1
    const-string v0, "title"

    goto/16 :goto_1

    .line 780
    :cond_2
    const-string v0, "artist"

    goto/16 :goto_2

    .line 783
    :cond_3
    const-string v0, "album"

    goto/16 :goto_3

    .line 786
    :cond_4
    const-string v0, "genre_name"

    goto/16 :goto_4

    .line 789
    :cond_5
    const-string v0, "composer"

    goto/16 :goto_5

    .line 792
    :cond_6
    const-string v0, "name"

    goto/16 :goto_6

    .line 795
    :cond_7
    const-string v0, "_display_name"

    goto/16 :goto_7

    .line 798
    :cond_8
    const-string v0, "bucket_display_name"

    goto/16 :goto_8

    .line 1514
    :cond_9
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "artist_count"

    aput-object v1, v0, v6

    const-string v1, "numsongs"

    aput-object v1, v0, v7

    goto/16 :goto_9

    .line 1534
    :cond_a
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "composer"

    aput-object v1, v0, v5

    const-string v1, "count"

    aput-object v1, v0, v6

    goto/16 :goto_a

    .line 1543
    :cond_b
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "genre_name"

    aput-object v1, v0, v5

    const-string v1, "count"

    aput-object v1, v0, v6

    goto/16 :goto_b

    .line 1565
    :cond_c
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const-string v1, "bucket_id"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    goto/16 :goto_c

    .line 1578
    :cond_d
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "number_of_albums"

    aput-object v1, v0, v6

    const-string v1, "number_of_tracks"

    aput-object v1, v0, v7

    goto/16 :goto_d

    .line 1599
    :cond_e
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    goto/16 :goto_e

    .line 1612
    :cond_f
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "track"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    goto/16 :goto_f

    .line 1634
    :cond_10
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "year_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    goto/16 :goto_10

    .line 1649
    :cond_11
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "_display_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "album"

    aput-object v2, v0, v1

    goto/16 :goto_11

    .line 1665
    :cond_12
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "album_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "play_order"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "audio_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "is_music"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    goto/16 :goto_12

    .line 1686
    :cond_13
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "provider_id"

    aput-object v1, v0, v5

    const-string v1, "provider_name"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    goto/16 :goto_13

    .line 1714
    :cond_14
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "date_modified"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v6

    const-string v1, "track"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    goto/16 :goto_14
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->isFavoritePlaylistTrack(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistContentUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->RECORDINGS_QUERY_WHERE:Ljava/lang/String;

    return-object v0
.end method

.method public static convertToListType(Landroid/net/Uri;)I
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, -0x1

    .line 2304
    if-nez p0, :cond_1

    .line 2320
    :cond_0
    :goto_0
    return v1

    .line 2308
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2309
    .local v0, "uriString":Ljava/lang/String;
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2310
    const v1, 0x2000d

    goto :goto_0

    .line 2311
    :cond_2
    sget-object v2, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2312
    const v1, 0x2000f

    goto :goto_0

    .line 2313
    :cond_3
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2318
    const v1, 0x20001

    goto :goto_0
.end method

.method public static convertToUri(I)Landroid/net/Uri;
    .locals 4
    .param p0, "listType"    # I

    .prologue
    .line 2284
    packed-switch p0, :pswitch_data_0

    .line 2296
    :pswitch_0
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    .line 2299
    .local v0, "uri":Landroid/net/Uri;
    :goto_0
    const-string v1, "MusicListUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convertToUri() Uri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2300
    return-object v0

    .line 2286
    .end local v0    # "uri":Landroid/net/Uri;
    :pswitch_1
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI:Landroid/net/Uri;

    .line 2287
    .restart local v0    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 2290
    .end local v0    # "uri":Landroid/net/Uri;
    :pswitch_2
    sget-object v0, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->CONTENT_URI:Landroid/net/Uri;

    .line 2291
    .restart local v0    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 2293
    .end local v0    # "uri":Landroid/net/Uri;
    :pswitch_3
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    .line 2294
    .restart local v0    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 2284
    nop

    :pswitch_data_0
    .packed-switch 0x2000b
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static final getAlbumContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1458
    const-string v1, "content://media/external/audio/media/music_albums"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1460
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method private static final getArtistsContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1469
    const-string v1, "content://media/external/audio/media/music_artists_album_id"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1472
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method public static getCloudType(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 173
    const/high16 v0, 0xf00000

    and-int/2addr v0, p0

    return v0
.end method

.method public static getDefaultPlayListPrefValue(J)Ljava/lang/String;
    .locals 2
    .param p0, "plid"    # J

    .prologue
    .line 1938
    long-to-int v1, p0

    .line 1940
    .local v1, "type":I
    packed-switch v1, :pswitch_data_0

    .line 1954
    const/4 v0, 0x0

    .line 1957
    .local v0, "pref":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1942
    .end local v0    # "pref":Ljava/lang/String;
    :pswitch_0
    const-string v0, "favourite_played"

    .line 1943
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1945
    .end local v0    # "pref":Ljava/lang/String;
    :pswitch_1
    const-string v0, "most_played"

    .line 1946
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1948
    .end local v0    # "pref":Ljava/lang/String;
    :pswitch_2
    const-string v0, "recently_played"

    .line 1949
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1951
    .end local v0    # "pref":Ljava/lang/String;
    :pswitch_3
    const-string v0, "recently_added"

    .line 1952
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1940
    :pswitch_data_0
    .packed-switch -0xe
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getDefaultPlaylistName(J)I
    .locals 6
    .param p0, "plid"    # J

    .prologue
    .line 542
    const/4 v0, 0x0

    .line 543
    .local v0, "plName":I
    long-to-int v1, p0

    .line 545
    .local v1, "type":I
    packed-switch v1, :pswitch_data_0

    .line 559
    const-string v2, "MusicListUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Really? playlist id is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    :goto_0
    return v0

    .line 547
    :pswitch_0
    const v0, 0x7f10008a

    .line 548
    goto :goto_0

    .line 550
    :pswitch_1
    const v0, 0x7f1000cc

    .line 551
    goto :goto_0

    .line 553
    :pswitch_2
    const v0, 0x7f10012c

    .line 554
    goto :goto_0

    .line 556
    :pswitch_3
    const v0, 0x7f10012b

    .line 557
    goto :goto_0

    .line 545
    :pswitch_data_0
    .packed-switch -0xe
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getDefaultTab()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1774
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_BIGPOND:Z

    if-eqz v1, :cond_0

    .line 1775
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "65540|131073|65548|65538|65539"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1779
    .local v0, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getExtraTabItems()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1777
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "65540|131073|65538|65539"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .restart local v0    # "sb":Ljava/lang/StringBuilder;
    goto :goto_0
.end method

.method public static getDefaultTabName(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 489
    const/4 v0, -0x1

    .line 490
    .local v0, "tabNameId":I
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result p0

    .line 491
    sparse-switch p0, :sswitch_data_0

    .line 529
    const v0, 0x7f10017d

    .line 532
    :goto_0
    return v0

    .line 493
    :sswitch_0
    const v0, 0x7f10017d

    .line 494
    goto :goto_0

    .line 496
    :sswitch_1
    const v0, 0x7f10001f

    .line 497
    goto :goto_0

    .line 499
    :sswitch_2
    const v0, 0x7f100027

    .line 500
    goto :goto_0

    .line 502
    :sswitch_3
    const v0, 0x7f100127

    .line 503
    goto :goto_0

    .line 505
    :sswitch_4
    const v0, 0x7f100091

    .line 506
    goto :goto_0

    .line 508
    :sswitch_5
    const v0, 0x7f10008e

    .line 509
    goto :goto_0

    .line 511
    :sswitch_6
    const v0, 0x7f100042

    .line 512
    goto :goto_0

    .line 514
    :sswitch_7
    const v0, 0x7f1001d8

    .line 515
    goto :goto_0

    .line 517
    :sswitch_8
    const v0, 0x7f100057

    .line 518
    goto :goto_0

    .line 520
    :sswitch_9
    const v0, 0x7f1000d7

    .line 521
    goto :goto_0

    .line 523
    :sswitch_a
    const v0, 0x7f100031

    .line 524
    goto :goto_0

    .line 526
    :sswitch_b
    const v0, 0x7f10012e

    .line 527
    goto :goto_0

    .line 491
    :sswitch_data_0
    .sparse-switch
        0x10002 -> :sswitch_1
        0x10003 -> :sswitch_2
        0x10004 -> :sswitch_3
        0x10006 -> :sswitch_4
        0x10007 -> :sswitch_5
        0x10008 -> :sswitch_6
        0x10009 -> :sswitch_7
        0x1000a -> :sswitch_9
        0x1000b -> :sswitch_8
        0x1000c -> :sswitch_a
        0x20001 -> :sswitch_0
        0x20011 -> :sswitch_b
    .end sparse-switch
.end method

.method public static getDefaultTabOrder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1800
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_BIGPOND:Z

    if-eqz v1, :cond_0

    .line 1801
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "65540|131073|65548|65538|65539"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1805
    .local v0, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getExtraTabOrder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1806
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1803
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "65540|131073|65538|65539"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .restart local v0    # "sb":Ljava/lang/StringBuilder;
    goto :goto_0
.end method

.method public static getDefaultTabPrefValue(I)Ljava/lang/String;
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 1857
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1892
    const/4 v0, 0x0

    .line 1895
    .local v0, "pref":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1859
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_0
    const-string v0, "album"

    .line 1860
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1862
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_1
    const-string v0, "artist"

    .line 1863
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1865
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_2
    const-string v0, "playlist"

    .line 1866
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1868
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_3
    const-string v0, "genre"

    .line 1869
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1871
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_4
    const-string v0, "folder"

    .line 1872
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1874
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_5
    const-string v0, "composer"

    .line 1875
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1877
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_6
    const-string v0, "year"

    .line 1878
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1880
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_7
    const-string v0, "music_square"

    .line 1881
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1883
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_8
    const-string v0, "music_bigpond"

    .line 1884
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1886
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_9
    const-string v0, "music_all_share"

    .line 1887
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1889
    .end local v0    # "pref":Ljava/lang/String;
    :sswitch_a
    const-string v0, "recordings"

    .line 1890
    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 1857
    :sswitch_data_0
    .sparse-switch
        0x10002 -> :sswitch_0
        0x10003 -> :sswitch_1
        0x10004 -> :sswitch_2
        0x10006 -> :sswitch_3
        0x10007 -> :sswitch_4
        0x10008 -> :sswitch_5
        0x10009 -> :sswitch_6
        0x1000a -> :sswitch_7
        0x1000b -> :sswitch_9
        0x1000c -> :sswitch_8
        0x20011 -> :sswitch_a
    .end sparse-switch
.end method

.method private static getDlnaMusicInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2465
    const/4 v7, 0x0

    .line 2466
    .local v7, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    const/4 v6, 0x0

    .line 2468
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->DLNA_MEDIA_INFO_COLS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2469
    if-eqz v6, :cond_3

    .line 2470
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2471
    new-instance v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    invoke-direct {v8}, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2472
    .end local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .local v8, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :try_start_1
    const-string v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    .line 2473
    const-string v0, "album"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    .line 2474
    const-string v0, "artist"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    .line 2475
    const-string v0, "album_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->albumId:J

    .line 2476
    const-string v0, "duration"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 2477
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    .line 2478
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->filePath:Ljava/lang/String;

    .line 2479
    const-string v0, "extension"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->extendtion:Ljava/lang/String;

    .line 2480
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->id:J

    .line 2481
    const-string v0, "_size"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->fileSize:J

    .line 2482
    const-string v0, "provider_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->dmsName:Ljava/lang/String;

    .line 2483
    const-string v0, "provider_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->dmsId:Ljava/lang/String;

    .line 2484
    const-string v0, "seed"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->seed:Ljava/lang/String;

    .line 2485
    const-string v0, "genre_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->genre:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v7, v8

    .line 2493
    .end local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :goto_0
    if-eqz v6, :cond_0

    .line 2494
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2497
    :cond_0
    const-string v1, "MusicListUtils"

    const-string v2, "AS: MusicAlbumInfo : Title: %s, Artist: %s, Album: %s, Duration: %s, MimeType: %s"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-nez v7, :cond_4

    const-string v0, "null"

    :goto_1
    aput-object v0, v3, v4

    const/4 v4, 0x1

    if-nez v7, :cond_5

    const-string v0, "null"

    :goto_2
    aput-object v0, v3, v4

    const/4 v4, 0x2

    if-nez v7, :cond_6

    const-string v0, "null"

    :goto_3
    aput-object v0, v3, v4

    const/4 v4, 0x3

    if-nez v7, :cond_7

    const-string v0, "null"

    :goto_4
    aput-object v0, v3, v4

    const/4 v4, 0x4

    if-nez v7, :cond_8

    const-string v0, "null"

    :goto_5
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2503
    return-object v7

    .line 2487
    :cond_1
    :try_start_2
    const-string v0, "MusicListUtils"

    const-string v1, "AS: getAlbumInfo: cursor.moveToFirst() FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2493
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v6, :cond_2

    .line 2494
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2490
    :cond_3
    :try_start_3
    const-string v0, "MusicListUtils"

    const-string v1, "AS: getAlbumInfo: cursor==null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2497
    :cond_4
    iget-object v0, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v0, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    goto :goto_2

    :cond_6
    iget-object v0, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    goto :goto_3

    :cond_7
    iget-wide v10, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_4

    :cond_8
    iget-object v0, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    goto :goto_5

    .line 2493
    .end local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    goto :goto_6
.end method

.method public static getDownloadableListForCursor(Landroid/database/Cursor;)[J
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 2079
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;Z)[J

    move-result-object v0

    return-object v0
.end method

.method private static getExtraTabItems()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1784
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-eqz v0, :cond_0

    .line 1785
    const-string v0, "|65543"

    .line 1787
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "|65543|65547"

    goto :goto_0
.end method

.method private static getExtraTabOrder()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1810
    const-string v0, ""

    .line 1811
    .local v0, "tabs":Ljava/lang/String;
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-eqz v1, :cond_1

    .line 1812
    const-string v0, "|65542|65543|65544"

    .line 1820
    :goto_0
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_RECORDINGS_TAB:Z

    if-eqz v1, :cond_0

    .line 1821
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|131089"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1823
    :cond_0
    return-object v0

    .line 1814
    :cond_1
    const-string v0, "|65542|65543|65544|65547"

    goto :goto_0
.end method

.method private static getFileNameFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 2563
    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 2564
    .local v1, "lastSlash":I
    if-ltz v1, :cond_0

    .line 2565
    add-int/lit8 v1, v1, 0x1

    .line 2566
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2567
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 2571
    :cond_0
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2572
    .local v0, "lastDot":I
    if-lez v0, :cond_1

    .line 2573
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 2575
    :cond_1
    return-object p0
.end method

.method private static final getFoldersContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1481
    const-string v1, "content://media/external/audio/media/music_folders"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1483
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method private static final getGenresContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1492
    const-string v1, "content://media/external/audio/media/music_genres"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1495
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method public static getKeyWordColforSelection(I)Ljava/lang/String;
    .locals 4
    .param p0, "list"    # I

    .prologue
    .line 1387
    const/4 v0, 0x0

    .line 1388
    .local v0, "returnvalue":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1402
    :pswitch_0
    const-string v1, "MusicListUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getKeyWordColforSelection > invalid track list : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    :goto_0
    return-object v0

    .line 1390
    :pswitch_1
    const-string v0, "album_id"

    .line 1391
    goto :goto_0

    .line 1393
    :pswitch_2
    const-string v0, "artist_id"

    .line 1394
    goto :goto_0

    .line 1396
    :pswitch_3
    const-string v0, "composer"

    .line 1397
    goto :goto_0

    .line 1399
    :pswitch_4
    const-string v0, "genre_name"

    .line 1400
    goto :goto_0

    .line 1388
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getListByPref(Ljava/lang/String;)I
    .locals 2
    .param p0, "pref"    # Ljava/lang/String;

    .prologue
    .line 1905
    const/4 v0, -0x1

    .line 1906
    .local v0, "list":I
    const-string v1, "album"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1907
    const v0, 0x10002

    .line 1927
    :cond_0
    :goto_0
    return v0

    .line 1908
    :cond_1
    const-string v1, "artist"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1909
    const v0, 0x10003

    goto :goto_0

    .line 1910
    :cond_2
    const-string v1, "playlist"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1911
    const v0, 0x10004

    goto :goto_0

    .line 1912
    :cond_3
    const-string v1, "genre"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1913
    const v0, 0x10006

    goto :goto_0

    .line 1914
    :cond_4
    const-string v1, "folder"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1915
    const v0, 0x10007

    goto :goto_0

    .line 1916
    :cond_5
    const-string v1, "composer"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1917
    const v0, 0x10008

    goto :goto_0

    .line 1918
    :cond_6
    const-string v1, "year"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1919
    const v0, 0x10009

    goto :goto_0

    .line 1920
    :cond_7
    const-string v1, "music_square"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1921
    const v0, 0x1000a

    goto :goto_0

    .line 1922
    :cond_8
    const-string v1, "music_bigpond"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1923
    const v0, 0x1000c

    goto :goto_0

    .line 1924
    :cond_9
    const-string v1, "recordings"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1925
    const v0, 0x20011

    goto :goto_0
.end method

.method public static getListContentType(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 308
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static getListIdForCursor(Landroid/database/Cursor;J)Lcom/samsung/musicplus/util/ListUtils$SongList;
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "selectedId"    # J

    .prologue
    .line 2246
    const/4 v0, -0x1

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/musicplus/util/ListUtils;->getListIdForCursor(Landroid/database/Cursor;JI)Lcom/samsung/musicplus/util/ListUtils$SongList;

    move-result-object v0

    return-object v0
.end method

.method public static getListIdForCursor(Landroid/database/Cursor;JI)Lcom/samsung/musicplus/util/ListUtils$SongList;
    .locals 15
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "selectedId"    # J
    .param p3, "position"    # I

    .prologue
    .line 2172
    const-string v12, "MusicListUtils"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getSongListForCursor "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173
    new-instance v11, Lcom/samsung/musicplus/util/ListUtils$SongList;

    invoke-direct {v11}, Lcom/samsung/musicplus/util/ListUtils$SongList;-><init>()V

    .line 2174
    .local v11, "song":Lcom/samsung/musicplus/util/ListUtils$SongList;
    if-nez p0, :cond_0

    .line 2175
    sget-object v12, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    iput-object v12, v11, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    .line 2232
    :goto_0
    return-object v11

    .line 2179
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 2180
    .local v9, "len":I
    if-nez v9, :cond_1

    .line 2181
    sget-object v12, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    iput-object v12, v11, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    goto :goto_0

    .line 2186
    :cond_1
    instance-of v12, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;

    if-eqz v12, :cond_6

    move-object v12, p0

    .line 2187
    check-cast v12, Lcom/samsung/musicplus/widget/database/MusicCursor;

    invoke-virtual {v12}, Lcom/samsung/musicplus/widget/database/MusicCursor;->getRealCursorCount()I

    move-result v2

    .local v2, "dataSize":I
    move-object v12, p0

    .line 2190
    check-cast v12, Lcom/samsung/musicplus/widget/database/MusicCursor;

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/samsung/musicplus/widget/database/MusicCursor;->getSongStartPosition(I)I

    move-result p3

    .line 2194
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2196
    new-array v10, v2, [J

    .line 2197
    .local v10, "list":[J
    const/4 v8, 0x0

    .line 2200
    .local v8, "index":I
    :try_start_0
    const-string v12, "audio_id"

    invoke-interface {p0, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 2209
    :goto_2
    const/4 v4, 0x0

    .line 2212
    .local v4, "i":I
    :cond_2
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 2213
    .local v6, "id":J
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-lez v12, :cond_4

    .line 2217
    cmp-long v12, p1, v6

    if-nez v12, :cond_3

    .line 2218
    iput v4, v11, Lcom/samsung/musicplus/util/ListUtils$SongList;->position:I

    .line 2219
    const-string v12, "MusicListUtils"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Found song that matched with music provider. id : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " position : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2222
    :cond_3
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    aput-wide v6, v10, v4

    move v4, v5

    .line 2224
    .end local v5    # "i":I
    .restart local v4    # "i":I
    :cond_4
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-nez v12, :cond_2

    .line 2226
    const/4 v12, -0x1

    move/from16 v0, p3

    if-eq v0, v12, :cond_5

    .line 2228
    move/from16 v0, p3

    iput v0, v11, Lcom/samsung/musicplus/util/ListUtils$SongList;->position:I

    .line 2230
    :cond_5
    iput-object v10, v11, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    .line 2231
    const-string v12, "MusicListUtils"

    const-string v13, "getSongListForCursor end"

    invoke-static {v12, v13}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2192
    .end local v2    # "dataSize":I
    .end local v4    # "i":I
    .end local v6    # "id":J
    .end local v8    # "index":I
    .end local v10    # "list":[J
    :cond_6
    move v2, v9

    .restart local v2    # "dataSize":I
    goto :goto_1

    .line 2201
    .restart local v8    # "index":I
    .restart local v10    # "list":[J
    :catch_0
    move-exception v3

    .line 2202
    .local v3, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v12, "_id"

    invoke-interface {p0, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v8

    goto :goto_2

    .line 2204
    :catch_1
    move-exception v3

    .line 2205
    sget-object v12, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    iput-object v12, v11, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    goto/16 :goto_0
.end method

.method public static getListType(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 193
    const/high16 v0, 0x30000

    and-int/2addr v0, p0

    return v0
.end method

.method public static getLocalFilePath(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 2400
    const/4 v7, 0x0

    .line 2401
    .local v7, "path":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2403
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2407
    if-eqz v6, :cond_3

    .line 2408
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2409
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 2417
    :goto_0
    if-eqz v6, :cond_0

    .line 2418
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2421
    :cond_0
    return-object v7

    .line 2411
    :cond_1
    :try_start_1
    const-string v0, "MusicListUtils"

    const-string v1, "getLocalFilePath: cursor.moveToFirst() FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2417
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 2418
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2414
    :cond_3
    :try_start_2
    const-string v0, "MusicListUtils"

    const-string v1, "getLocalFilePath: cursor==null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static getMediaInfo(Landroid/content/Context;ILandroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2379
    packed-switch p1, :pswitch_data_0

    .line 2387
    :pswitch_0
    invoke-static {p0, p2}, Lcom/samsung/musicplus/util/ListUtils;->getMediaInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2381
    :pswitch_1
    invoke-static {p0, p2}, Lcom/samsung/musicplus/util/ListUtils;->getSlinkMediaInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v0

    goto :goto_0

    .line 2385
    :pswitch_2
    invoke-static {p0, p2}, Lcom/samsung/musicplus/util/ListUtils;->getDlnaMusicInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v0

    goto :goto_0

    .line 2379
    :pswitch_data_0
    .packed-switch 0x2000b
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static getMediaInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2425
    const-string v0, "MusicListUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMediaInfo uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426
    const/4 v7, 0x0

    .line 2427
    .local v7, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    const/4 v6, 0x0

    .line 2429
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->DEFAULT_MEDIA_INFO_COLS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2430
    if-eqz v6, :cond_4

    .line 2431
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2432
    new-instance v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    invoke-direct {v8}, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2433
    .end local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .local v8, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :try_start_1
    const-string v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    .line 2434
    const-string v0, "album"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    .line 2435
    const-string v0, "artist"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    .line 2436
    const-string v0, "album_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->albumId:J

    .line 2437
    const-string v0, "duration"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 2438
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    .line 2439
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->filePath:Ljava/lang/String;

    .line 2440
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2441
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->getFileNameFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    .line 2443
    :cond_0
    const-string v0, "genre_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->genre:Ljava/lang/String;

    .line 2444
    const-string v0, "bit_depth"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->bitDepth:I

    .line 2445
    const-string v0, "sampling_rate"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->samplingRate:I

    .line 2446
    const-string v0, "is_secretbox"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->personalMode:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v7, v8

    .line 2454
    .end local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :goto_0
    if-eqz v6, :cond_1

    .line 2455
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2458
    :cond_1
    const-string v1, "MusicListUtils"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMediaInfo uri : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_5

    const-string v0, "null"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " title : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v7, :cond_6

    const-string v0, "null"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " album id : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v7, :cond_7

    const-string v0, "null"

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461
    return-object v7

    .line 2448
    :cond_2
    :try_start_2
    const-string v0, "MusicListUtils"

    const-string v1, "getAlbumInfo: cursor.moveToFirst() FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2454
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v6, :cond_3

    .line 2455
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2451
    :cond_4
    :try_start_3
    const-string v0, "MusicListUtils"

    const-string v1, "getAlbumInfo: cursor==null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2458
    :cond_5
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    iget-object v0, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_7
    iget-wide v4, v7, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->albumId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    .line 2454
    .end local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v7    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    goto :goto_4
.end method

.method public static getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    .locals 4
    .param p0, "list"    # I
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 739
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getListType(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 747
    const-string v1, "MusicListUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicListInfo > invalid list type : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Did you miss something?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    const/4 v0, 0x0

    .line 752
    .local v0, "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    :goto_0
    const-string v1, "MusicListUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicListInfo > list type : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keyWord : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    return-object v0

    .line 741
    .end local v0    # "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    :sswitch_0
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;-><init>(I)V

    .line 742
    .restart local v0    # "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    goto :goto_0

    .line 744
    .end local v0    # "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    :sswitch_1
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;-><init>(ILjava/lang/String;)V

    .line 745
    .restart local v0    # "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    goto :goto_0

    .line 739
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getMusicListInfoFromAudioId(IJ)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    .locals 5
    .param p0, "list"    # I
    .param p1, "audioId"    # J

    .prologue
    .line 760
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getListType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 765
    const-string v1, "MusicListUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicListInfoFromAudioId > invalid list type : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Did you miss something?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const/4 v0, 0x0

    .line 771
    .local v0, "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    :goto_0
    const-string v1, "MusicListUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicListInfoFromAudioId > list type : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keyWord : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    return-object v0

    .line 762
    .end local v0    # "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    :pswitch_0
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/musicplus/util/ListUtils$TrackListInfo;-><init>(IJ)V

    .line 763
    .restart local v0    # "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    goto :goto_0

    .line 760
    nop

    :pswitch_data_0
    .packed-switch 0x20000
        :pswitch_0
    .end packed-switch
.end method

.method public static getPersonalModeInList(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$QueryArgs;[JZ)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "args"    # Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .param p2, "list"    # [J
    .param p3, "userPlaylist"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2025
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "is_secretbox"

    aput-object v0, v2, v1

    .line 2029
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 2030
    .local v6, "c":Landroid/database/Cursor;
    const/4 v9, 0x1

    .line 2031
    .local v9, "mode":I
    const/4 v10, 0x0

    .line 2032
    .local v10, "totalCount":I
    const/4 v8, 0x0

    .line 2033
    .local v8, "countPersonal":I
    const/4 v7, 0x0

    .line 2036
    .local v7, "countNormal":I
    if-eqz p1, :cond_3

    .line 2037
    if-eqz p3, :cond_1

    :try_start_0
    const-string v0, "audio_id"

    :goto_0
    invoke-static {v0, p2}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v3

    .line 2040
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2041
    if-eqz v6, :cond_3

    .line 2042
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 2043
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2044
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2045
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v4, 0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 2046
    add-int/lit8 v8, v8, 0x1

    .line 2050
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2056
    .end local v3    # "where":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 2057
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 2037
    :cond_1
    :try_start_1
    const-string v0, "_id"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2048
    .restart local v3    # "where":Ljava/lang/String;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 2056
    .end local v3    # "where":Ljava/lang/String;
    :cond_3
    if-eqz v6, :cond_4

    .line 2057
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2061
    :cond_4
    if-ne v8, v10, :cond_6

    .line 2062
    const/4 v9, 0x0

    .line 2067
    :cond_5
    :goto_3
    return v9

    .line 2063
    :cond_6
    if-ne v7, v10, :cond_5

    .line 2064
    const/4 v9, 0x1

    goto :goto_3
.end method

.method private static final getPlaylistContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1447
    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1449
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method public static final getPlaylistMembersContentUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "pId"    # J

    .prologue
    .line 1503
    const-string v1, "external"

    invoke-static {v1, p0, p1}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    .line 1505
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method public static getPreDefinedList(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 324
    const v0, 0x3ffff

    and-int/2addr v0, p0

    return v0
.end method

.method public static getSlinkMediaInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2507
    const-string v0, "MusicListUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSlinkMediaInfo URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2508
    const/4 v6, 0x0

    .line 2509
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 2513
    .local v8, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    .line 2514
    .local v7, "id":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getContentUri(J)Landroid/net/Uri;

    move-result-object p1

    .line 2515
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/util/ListUtils;->S_LINK_MEDIA_INFO_COLS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2516
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2517
    new-instance v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    invoke-direct {v9}, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2518
    .end local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .local v9, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->id:J

    .line 2521
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->filePath:Ljava/lang/String;

    .line 2523
    const-string v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    .line 2524
    const-string v0, "album"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    .line 2525
    const-string v0, "artist"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    .line 2526
    const-string v0, "album_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->albumId:J

    .line 2527
    const-string v0, "duration"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 2528
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    .line 2529
    const-string v0, "device_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->deviceId:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v8, v9

    .line 2532
    .end local v9    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :cond_0
    if-eqz v6, :cond_1

    .line 2533
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2537
    :cond_1
    const-string v1, "MusicListUtils"

    const-string v2, "Slink: MusicAlbumInfo : Title: %s, Artist: %s, Album: %s, Duration: %s, MimeType: %s FilePath: %s"

    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-nez v8, :cond_3

    const-string v0, "null"

    :goto_0
    aput-object v0, v3, v4

    const/4 v4, 0x1

    if-nez v8, :cond_4

    const-string v0, "null"

    :goto_1
    aput-object v0, v3, v4

    const/4 v4, 0x2

    if-nez v8, :cond_5

    const-string v0, "null"

    :goto_2
    aput-object v0, v3, v4

    const/4 v4, 0x3

    if-nez v8, :cond_6

    const-string v0, "null"

    :goto_3
    aput-object v0, v3, v4

    const/4 v4, 0x4

    if-nez v8, :cond_7

    const-string v0, "null"

    :goto_4
    aput-object v0, v3, v4

    const/4 v4, 0x5

    if-nez v8, :cond_8

    const-string v0, "null"

    :goto_5
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544
    return-object v8

    .line 2532
    .end local v7    # "id":Ljava/lang/String;
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v6, :cond_2

    .line 2533
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2537
    .restart local v7    # "id":Ljava/lang/String;
    :cond_3
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    goto :goto_2

    :cond_6
    iget-wide v10, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    :cond_7
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    goto :goto_4

    :cond_8
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->filePath:Ljava/lang/String;

    goto :goto_5

    .line 2532
    .end local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v9    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :catchall_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .restart local v8    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    goto :goto_6
.end method

.method public static getSongCountInList(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$QueryArgs;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "args"    # Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .prologue
    const/4 v1, 0x0

    .line 1986
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(*)"

    aput-object v0, v2, v1

    .line 1989
    .local v2, "cols":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1990
    .local v7, "songCount":I
    const/4 v6, 0x0

    .line 1992
    .local v6, "c":Landroid/database/Cursor;
    if-eqz p1, :cond_0

    .line 1993
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, p1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1995
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1996
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 2000
    :cond_0
    if-eqz v6, :cond_1

    .line 2001
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2004
    :cond_1
    return v7

    .line 2000
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 2001
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getSongListForCursor(Landroid/database/Cursor;)[J
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 2089
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;Z)[J

    move-result-object v0

    return-object v0
.end method

.method private static getSongListForCursor(Landroid/database/Cursor;Z)[J
    .locals 13
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "needLocalOnly"    # Z

    .prologue
    const/4 v12, 0x0

    .line 2099
    const-string v9, "MusicListUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getSongListForCursor "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2100
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2101
    :cond_0
    sget-object v7, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    .line 2145
    :goto_0
    return-object v7

    .line 2104
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 2105
    .local v4, "len":I
    if-nez v4, :cond_2

    .line 2106
    sget-object v7, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    goto :goto_0

    .line 2108
    :cond_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2110
    const/4 v1, 0x0

    .line 2112
    .local v1, "index":I
    :try_start_0
    const-string v9, "audio_id"

    invoke-interface {p0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2117
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v9

    new-array v7, v9, [J

    .line 2118
    .local v7, "list":[J
    const/4 v5, 0x0

    .local v5, "length":I
    move v6, v5

    .line 2122
    .end local v5    # "length":I
    .local v6, "length":I
    :goto_2
    if-lt v6, v4, :cond_4

    move v5, v6

    .line 2134
    .end local v6    # "length":I
    .restart local v5    # "length":I
    :cond_3
    if-nez v5, :cond_6

    .line 2135
    sget-object v7, Lcom/samsung/musicplus/util/ListUtils;->sEmptyList:[J

    goto :goto_0

    .line 2113
    .end local v5    # "length":I
    .end local v7    # "list":[J
    :catch_0
    move-exception v0

    .line 2114
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v9, "_id"

    invoke-interface {p0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2125
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v6    # "length":I
    .restart local v7    # "list":[J
    :cond_4
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2126
    .local v2, "id":J
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-lez v9, :cond_8

    .line 2127
    if-eqz p1, :cond_5

    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->isCloud(Landroid/database/Cursor;)Z

    move-result v9

    if-eqz v9, :cond_5

    move v5, v6

    .line 2132
    .end local v6    # "length":I
    .restart local v5    # "length":I
    :goto_3
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_3

    move v6, v5

    .end local v5    # "length":I
    .restart local v6    # "length":I
    goto :goto_2

    .line 2130
    :cond_5
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "length":I
    .restart local v5    # "length":I
    aput-wide v2, v7, v6

    goto :goto_3

    .line 2138
    .end local v2    # "id":J
    :cond_6
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-eq v5, v9, :cond_7

    .line 2139
    new-array v8, v5, [J

    .line 2140
    .local v8, "temp":[J
    invoke-static {v7, v12, v8, v12, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141
    move-object v7, v8

    .line 2144
    .end local v8    # "temp":[J
    :cond_7
    const-string v9, "MusicListUtils"

    const-string v10, "getSongListForCursor end"

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v5    # "length":I
    .restart local v2    # "id":J
    .restart local v6    # "length":I
    :cond_8
    move v5, v6

    .end local v6    # "length":I
    .restart local v5    # "length":I
    goto :goto_3
.end method

.method public static getSubTrackList(I)I
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 181
    const v0, -0xf0001

    and-int/2addr v0, p0

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    return v0
.end method

.method public static hasNoSongs(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1961
    const/4 v8, 0x0

    .line 1962
    .local v8, "noSongs":Z
    new-instance v6, Lcom/samsung/musicplus/util/ListUtils$AllTrackQueryArgs;

    invoke-direct {v6}, Lcom/samsung/musicplus/util/ListUtils$AllTrackQueryArgs;-><init>()V

    .line 1963
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(*)"

    aput-object v0, v2, v1

    .line 1966
    .local v2, "cols":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 1967
    .local v9, "songCount":I
    const/4 v7, 0x0

    .line 1969
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1971
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1972
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1975
    :cond_0
    if-eqz v7, :cond_1

    .line 1976
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1979
    :cond_1
    if-gtz v9, :cond_2

    .line 1980
    const/4 v8, 0x1

    .line 1982
    :cond_2
    return v8

    .line 1975
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 1976
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static isCheckNoListItem(I)Z
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 351
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result p0

    .line 352
    const v0, 0x20002

    if-eq p0, v0, :cond_0

    const v0, 0x20003

    if-eq p0, v0, :cond_0

    const v0, 0x20006

    if-eq p0, v0, :cond_0

    const v0, 0x20008

    if-eq p0, v0, :cond_0

    const v0, 0x20009

    if-eq p0, v0, :cond_0

    const v0, 0x20007

    if-eq p0, v0, :cond_0

    const v0, 0x2000a

    if-ne p0, v0, :cond_1

    .line 355
    :cond_0
    const/4 v0, 0x1

    .line 357
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCloud(Landroid/database/Cursor;)Z
    .locals 4
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 225
    const-string v3, "media_type"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 226
    .local v0, "idx":I
    if-lez v0, :cond_0

    .line 227
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 228
    .local v1, "mediaType":I
    const v3, 0x20002

    if-ne v1, v3, :cond_0

    const/4 v2, 0x1

    .line 230
    .end local v1    # "mediaType":I
    :cond_0
    return v2
.end method

.method public static isCurrentPlaylist(J)Z
    .locals 8
    .param p0, "plid"    # J

    .prologue
    .line 400
    const/4 v4, 0x0

    .line 402
    .local v4, "isCurrentPlaylist":Z
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    .line 403
    .local v1, "currentPlayingTab":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "currentPlayingKeyWord":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v5

    const v6, 0x20004

    if-ne v5, v6, :cond_1

    if-eqz v0, :cond_1

    .line 406
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 407
    .local v2, "currentPlaylistId":J
    cmp-long v5, p0, v2

    if-eqz v5, :cond_0

    const-wide/16 v6, -0xb

    cmp-long v5, v2, v6

    if-nez v5, :cond_1

    .line 408
    :cond_0
    const/4 v4, 0x1

    .line 411
    .end local v2    # "currentPlaylistId":J
    :cond_1
    return v4
.end method

.method public static isDefaultMusicContent(I)Z
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 335
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result p0

    .line 336
    const v0, 0x2000d

    if-eq p0, v0, :cond_0

    const v0, 0x2000b

    if-eq p0, v0, :cond_0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const v0, 0x2000e

    if-eq p0, v0, :cond_0

    const v0, 0x2000f

    if-ne p0, v0, :cond_1

    .line 339
    :cond_0
    const/4 v0, 0x0

    .line 341
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isDisabledItem(I)Z
    .locals 4
    .param p0, "list"    # I

    .prologue
    .line 1842
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v0

    if-nez v0, :cond_0

    int-to-long v0, p0

    const-wide/16 v2, -0xb

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1843
    :cond_0
    const/4 v0, 0x1

    .line 1845
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEasyMode(I)Z
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method

.method private static isFavoritePlaylistTrack(Ljava/lang/String;)Z
    .locals 6
    .param p0, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 389
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 394
    .local v2, "id":J
    :goto_0
    const-wide/16 v4, -0xb

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 390
    .end local v2    # "id":J
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 392
    const-wide/16 v2, -0x1

    .restart local v2    # "id":J
    goto :goto_0

    .line 394
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isFixedList(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 1827
    const/4 v0, 0x0

    .line 1828
    .local v0, "isFixed":Z
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1838
    :goto_0
    :pswitch_0
    return v0

    .line 1830
    :pswitch_1
    const/4 v0, 0x1

    .line 1831
    goto :goto_0

    .line 1833
    :pswitch_2
    const/4 v0, 0x1

    .line 1834
    goto :goto_0

    .line 1828
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static isGridType(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    const/high16 v1, 0x40000

    .line 197
    and-int v0, p0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlaylistTabContent(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 415
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportAddPlayList(Landroid/content/Context;I)Z
    .locals 1
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "listType"    # I

    .prologue
    .line 2548
    const v0, 0x2000d

    if-eq p1, v0, :cond_0

    const v0, 0x2000b

    if-eq p1, v0, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportDownloadList(I)Z
    .locals 1
    .param p0, "listContent"    # I

    .prologue
    .line 213
    packed-switch p0, :pswitch_data_0

    .line 220
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 218
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isSupportSearchMenu(I)Z
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 2579
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2583
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2581
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2579
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method public static isSupportSplitSub(Landroid/content/Context;I)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 234
    const-string v2, "MusicListUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSupportSplitSub() listType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v2, :cond_0

    .line 236
    instance-of v2, p0, Lcom/samsung/musicplus/MusicMainActivity;

    if-nez v2, :cond_1

    .line 264
    .end local p0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return v0

    .line 239
    .restart local p0    # "context":Landroid/content/Context;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v1, :cond_0

    .line 242
    check-cast p0, Lcom/samsung/musicplus/MusicMainActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicMainActivity;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_0

    .line 245
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 248
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 259
    goto :goto_0

    .line 252
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static isTab(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 201
    const/high16 v0, 0x30000

    and-int/2addr v0, p0

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTrack(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 205
    const/high16 v0, 0x30000

    and-int/2addr v0, p0

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUserMadePlayList(J)Z
    .locals 2
    .param p0, "plid"    # J

    .prologue
    .line 367
    const-wide/16 v0, -0xb

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0xc

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0xd

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0xe

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    .line 369
    const/4 v0, 0x1

    .line 371
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUserPlaylistTrack(ILjava/lang/String;)Z
    .locals 4
    .param p0, "list"    # I
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 376
    :try_start_0
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v2

    const v3, 0x20004

    if-ne v2, v3, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/ListUtils;->isUserMadePlayList(J)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 378
    const/4 v1, 0x1

    .line 383
    :cond_0
    :goto_0
    return v1

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method
