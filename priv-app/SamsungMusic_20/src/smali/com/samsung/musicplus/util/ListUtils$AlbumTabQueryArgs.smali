.class public Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1233
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;-><init>()V

    .line 1234
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1235
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1236
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->selection:Ljava/lang/String;

    .line 1237
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1239
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1241
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1242
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1243
    const-string v0, "album"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1244
    const-string v0, "artist"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;->text2Col:Ljava/lang/String;

    .line 1245
    return-void
.end method
