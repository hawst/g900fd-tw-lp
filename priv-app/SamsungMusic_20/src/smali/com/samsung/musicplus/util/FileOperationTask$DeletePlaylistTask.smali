.class public Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeletePlaylistTask"
.end annotation


# instance fields
.field private mList:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;JZ)V
    .locals 4
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # J
    .param p4, "finish"    # Z

    .prologue
    const/4 v2, 0x1

    .line 483
    invoke-direct {p0, p1, p4}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 484
    new-array v0, v2, [J

    const/4 v1, 0x0

    aput-wide p2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mList:[J

    .line 487
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mIsSupportDelayedProgress:Z

    .line 488
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;[JZ)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "finish"    # Z

    .prologue
    .line 477
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 478
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mList:[J

    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mIsSupportDelayedProgress:Z

    .line 480
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 492
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mList:[J

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->deletePlaylists(Landroid/content/Context;[J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 467
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 497
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1000dd

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->mMessage:Ljava/lang/String;

    .line 500
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 501
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 467
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
