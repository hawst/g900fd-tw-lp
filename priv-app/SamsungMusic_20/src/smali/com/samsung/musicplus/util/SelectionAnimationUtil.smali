.class public Lcom/samsung/musicplus/util/SelectionAnimationUtil;
.super Ljava/lang/Object;
.source "SelectionAnimationUtil.java"


# static fields
.field private static final NOT_EXIST:I = -0x1

.field private static final UNKNOWN:I = -0x2


# instance fields
.field private mAbsListView:Landroid/widget/AbsListView;

.field private mAniDuration:I

.field private mAnimationContainerIds:[I

.field private mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

.field private mCheckBoxWidth:I

.field private mContext:Landroid/content/Context;

.field private mDummyTouchListener:Landroid/view/View$OnTouchListener;

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mIsGridType:Z

.field private mIsShowMode:Z

.field mPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v2, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I

    .line 39
    new-instance v0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/SelectionAnimationUtil$1;-><init>(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mDummyTouchListener:Landroid/view/View$OnTouchListener;

    .line 46
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x7f0d00cd

    aput v1, v0, v2

    iput-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimationContainerIds:[I

    .line 56
    new-instance v0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/SelectionAnimationUtil$2;-><init>(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 161
    new-instance v0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;-><init>(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

    .line 51
    iput-object p1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mContext:Landroid/content/Context;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAniDuration:I

    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mContext:Landroid/content/Context;

    const v1, 0x10c003d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/util/SelectionAnimationUtil;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mCheckBoxWidth:I

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimationContainerIds:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsGridType:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/util/SelectionAnimationUtil;Landroid/view/View;IIF)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # F

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->prepareCheckBoxAnimate(Landroid/view/View;IIF)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/util/SelectionAnimationUtil;Landroid/view/View;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/util/SelectionAnimationUtil;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->prepareContainerAnimate(Landroid/view/View;II)V

    return-void
.end method

.method private prepareCheckBoxAnimate(Landroid/view/View;IIF)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fromX"    # I
    .param p3, "toX"    # I
    .param p4, "toAlpha"    # F

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsGridType:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 142
    :cond_0
    int-to-float v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 143
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAniDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 149
    return-void
.end method

.method private prepareContainerAnimate(Landroid/view/View;II)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fromX"    # I
    .param p3, "toX"    # I

    .prologue
    .line 152
    int-to-float v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 153
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAniDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 159
    return-void
.end method


# virtual methods
.method public setCheckBoxAnimation(Landroid/widget/AbsListView;ZZ)V
    .locals 2
    .param p1, "lv"    # Landroid/widget/AbsListView;
    .param p2, "isShowMode"    # Z
    .param p3, "isGridType"    # Z

    .prologue
    .line 125
    if-nez p1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z

    if-nez v0, :cond_2

    if-eqz p2, :cond_0

    .line 131
    :cond_2
    iput-boolean p2, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z

    .line 132
    iput-boolean p3, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsGridType:Z

    .line 133
    iput-object p1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;

    .line 134
    iget-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mDummyTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method
