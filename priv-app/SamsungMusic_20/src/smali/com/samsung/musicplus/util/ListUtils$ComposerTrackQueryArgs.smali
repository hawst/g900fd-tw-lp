.class public Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComposerTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "audioId"    # J

    .prologue
    .line 1127
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(J)V

    .line 1128
    const-string v0, "title != \'\' AND is_music=1 AND _id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1129
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1121
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 1122
    const-string v0, "title != \'\' AND is_music=1 AND composer=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1124
    return-void
.end method
