.class Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;
.super Landroid/os/AsyncTask;
.source "AlbumArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/AlbumArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadArtistGroupTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mArtistId:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mIv:[Landroid/widget/ImageView;

.field private mResId:I

.field private mSize:I

.field private mUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;[Landroid/widget/ImageView;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/Object;
    .param p4, "iv"    # [Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 296
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mAlbums:Ljava/util/ArrayList;

    .line 297
    iput-object p1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mContext:Landroid/content/Context;

    .line 298
    iput-object p2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mUri:Ljava/lang/String;

    .line 299
    iput-object p3, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mArtistId:Ljava/lang/Object;

    .line 300
    iput-object p4, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mIv:[Landroid/widget/ImageView;

    .line 301
    iput p5, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mSize:I

    .line 302
    iput p6, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mResId:I

    .line 303
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 280
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x0

    .line 307
    const-string v0, "MusicAlbumLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getting albums for artist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mArtistId:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const/4 v7, 0x0

    .line 311
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "album_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "artist_id= \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mArtistId:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\') GROUP BY ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "album_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 318
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-interface {v7}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    .line 320
    .local v9, "position":I
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mIv:[Landroid/widget/ImageView;

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v9, v0, :cond_3

    .line 327
    .end local v9    # "position":I
    :cond_0
    if-eqz v7, :cond_1

    .line 328
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_1
    const-string v0, "MusicAlbumLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getting albums for artist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mArtistId:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " finished count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    # getter for: Lcom/samsung/musicplus/util/AlbumArtLoader;->mArtistAlbums:Landroid/support/v4/util/LongSparseArray;
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->access$200()Landroid/support/v4/util/LongSparseArray;

    move-result-object v1

    monitor-enter v1

    .line 336
    :try_start_1
    const-string v0, "MusicAlbumLoader"

    const-string v2, "put"

    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v8, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mArtistId:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Long;

    .line 338
    .local v8, "key":Ljava/lang/Long;
    # getter for: Lcom/samsung/musicplus/util/AlbumArtLoader;->mArtistAlbums:Landroid/support/v4/util/LongSparseArray;
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->access$200()Landroid/support/v4/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v6, v0, [Ljava/lang/Long;

    .line 340
    .local v6, "albums":[Ljava/lang/Long;
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 341
    # getter for: Lcom/samsung/musicplus/util/AlbumArtLoader;->mArtistAlbums:Landroid/support/v4/util/LongSparseArray;
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->access$200()Landroid/support/v4/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 343
    .end local v6    # "albums":[Ljava/lang/Long;
    :cond_2
    const-string v0, "MusicAlbumLoader"

    const-string v2, "end put"

    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 345
    return-object v10

    .line 324
    .end local v8    # "key":Ljava/lang/Long;
    .restart local v9    # "position":I
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mAlbums:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 327
    .end local v9    # "position":I
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 328
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 344
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 280
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 11
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 350
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 351
    .local v7, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mIv:[Landroid/widget/ImageView;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    .line 352
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v8

    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iget v10, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mSize:I

    new-instance v0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;

    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mIv:[Landroid/widget/ImageView;

    aget-object v2, v2, v6

    iget-object v3, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mArtistId:Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->mResId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;IZ)V

    invoke-virtual {v8, v9, v10, v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 351
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 356
    :cond_0
    return-void
.end method
