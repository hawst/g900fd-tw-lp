.class final enum Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
.super Ljava/lang/Enum;
.source "SoundAliveUtilsV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/SoundAliveUtilsV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PresetEffectList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

.field public static final enum CLUB:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

.field public static final enum CONCERTHALL:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

.field public static final enum NONE:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

.field public static final enum STUDIO:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

.field public static final enum TUBE_AMP_EFFECT:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

.field public static final enum VIRT71:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;


# instance fields
.field private name:I

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 174
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    const-string v1, "NONE"

    const v2, 0x7f100108

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->NONE:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    .line 175
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    const-string v1, "TUBE_AMP_EFFECT"

    const v2, 0x7f1001ac

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->TUBE_AMP_EFFECT:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    .line 176
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    const-string v1, "VIRT71"

    const v2, 0x7f1001bf

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->VIRT71:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    .line 177
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    const-string v1, "STUDIO"

    const v2, 0x7f100171

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->STUDIO:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    .line 178
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    const-string v1, "CLUB"

    const v2, 0x7f100041

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->CLUB:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    .line 179
    new-instance v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    const-string v1, "CONCERTHALL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const v4, 0x7f100043

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->CONCERTHALL:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    .line 173
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->NONE:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->TUBE_AMP_EFFECT:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->VIRT71:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->STUDIO:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->CLUB:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->CONCERTHALL:Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "effectValue"    # I
    .param p4, "effectName"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 186
    iput p3, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->value:I

    .line 187
    iput p4, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->name:I

    .line 188
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 173
    const-class v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    return-object v0
.end method

.method public static values()[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->$VALUES:[Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    invoke-virtual {v0}, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;

    return-object v0
.end method


# virtual methods
.method public getName()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->name:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2$PresetEffectList;->value:I

    return v0
.end method
