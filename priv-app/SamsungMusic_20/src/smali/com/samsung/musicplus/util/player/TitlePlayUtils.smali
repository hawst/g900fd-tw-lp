.class public Lcom/samsung/musicplus/util/player/TitlePlayUtils;
.super Ljava/lang/Object;
.source "TitlePlayUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/player/TitlePlayUtils$1;,
        Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;
    }
.end annotation


# static fields
.field private static final sEmptyList:Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;-><init>(Lcom/samsung/musicplus/util/player/TitlePlayUtils$1;)V

    sput-object v0, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->sEmptyList:Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    .line 45
    sget-object v0, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->sEmptyList:Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    new-array v1, v2, [J

    iput-object v1, v0, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;->list:[J

    .line 46
    sget-object v0, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->sEmptyList:Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    iput v2, v0, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;->position:I

    .line 47
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method private static getSongList(Landroid/database/Cursor;Ljava/lang/String;)Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;
    .locals 10
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 50
    if-nez p0, :cond_0

    .line 51
    sget-object v6, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->sEmptyList:Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    .line 85
    :goto_0
    return-object v6

    .line 54
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 55
    .local v3, "len":I
    if-nez v3, :cond_1

    .line 56
    sget-object v6, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->sEmptyList:Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    goto :goto_0

    .line 59
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 61
    const/4 v2, 0x0

    .line 63
    .local v2, "index":I
    :try_start_0
    const-string v8, "audio_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 67
    :goto_1
    const-string v8, "title"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 69
    .local v7, "titleIndex":I
    new-array v4, v3, [J

    .line 70
    .local v4, "list":[J
    const/4 v5, 0x0

    .line 71
    .local v5, "position":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v3, :cond_3

    .line 72
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 76
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 77
    move v5, v1

    .line 79
    :cond_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 64
    .end local v1    # "i":I
    .end local v4    # "list":[J
    .end local v5    # "position":I
    .end local v7    # "titleIndex":I
    :catch_0
    move-exception v0

    .line 65
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v8, "_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 82
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "i":I
    .restart local v4    # "list":[J
    .restart local v5    # "position":I
    .restart local v7    # "titleIndex":I
    :cond_3
    new-instance v6, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;

    const/4 v8, 0x0

    invoke-direct {v6, v8}, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;-><init>(Lcom/samsung/musicplus/util/player/TitlePlayUtils$1;)V

    .line 83
    .local v6, "songList":Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;
    iput-object v4, v6, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;->list:[J

    .line 84
    iput v5, v6, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;->position:I

    goto :goto_0
.end method

.method public static play(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const v9, 0x20001

    .line 18
    invoke-static {v9, v10}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iget-object v6, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 20
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v7, 0x0

    .line 22
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 24
    invoke-static {v7, p1}, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->getSongList(Landroid/database/Cursor;Ljava/lang/String;)Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 26
    .local v8, "songList":Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;
    if-eqz v7, :cond_0

    .line 27
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 31
    :cond_0
    iget-object v0, v8, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;->list:[J

    iget v1, v8, Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;->position:I

    invoke-static {p0, v9, v10, v0, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JI)V

    .line 32
    return-void

    .line 26
    .end local v8    # "songList":Lcom/samsung/musicplus/util/player/TitlePlayUtils$SongList;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_1

    .line 27
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public static play(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "genre"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 102
    const/4 v6, 0x0

    .line 105
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "title LIKE \"%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p2, :cond_2

    const-string v3, ""

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p3, :cond_3

    const-string v3, ""

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p4, :cond_4

    const-string v3, ""

    :goto_2
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "title != \'\' AND is_music=1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 121
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    .line 126
    :cond_0
    if-eqz v6, :cond_1

    .line 127
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v9

    .line 131
    :goto_3
    return v0

    .line 105
    :cond_2
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND album LIKE \"%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%\""

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND artist LIKE \"%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%\""

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND genre_name LIKE \"%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%\""

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 124
    :cond_5
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 126
    .local v7, "list":[J
    if-eqz v6, :cond_6

    .line 127
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 130
    :cond_6
    const v0, 0x20001

    invoke-static {p0, v0, v10, v7, v9}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JI)V

    move v0, v8

    .line 131
    goto :goto_3

    .line 126
    .end local v7    # "list":[J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_7

    .line 127
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0
.end method
