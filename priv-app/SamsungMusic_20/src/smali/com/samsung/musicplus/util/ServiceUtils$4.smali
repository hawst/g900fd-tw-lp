.class final Lcom/samsung/musicplus/util/ServiceUtils$4;
.super Ljava/lang/Thread;
.source "ServiceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/util/ServiceUtils;->playPrev(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$force:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 831
    iput-boolean p1, p0, Lcom/samsung/musicplus/util/ServiceUtils$4;->val$force:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 834
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 843
    :goto_0
    return-void

    .line 837
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    iget-boolean v2, p0, Lcom/samsung/musicplus/util/ServiceUtils$4;->val$force:Z

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/service/IPlayerService;->prev(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 838
    :catch_0
    move-exception v0

    .line 839
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 840
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 841
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
