.class public Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteTask"
.end annotation


# instance fields
.field private mCount:I

.field private mIsTrack:Z

.field private mList:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;[JIZZ)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "count"    # I
    .param p4, "isTrack"    # Z
    .param p5, "finish"    # Z

    .prologue
    .line 178
    invoke-direct {p0, p1, p5}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 179
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mList:[J

    .line 180
    iput p3, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mCount:I

    .line 181
    iput-boolean p4, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mIsTrack:Z

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mIsSupportDelayedProgress:Z

    .line 183
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mList:[J

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->deleteTracks(Landroid/content/Context;[J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 162
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 199
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mIsTrack:Z

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000b

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mMessage:Ljava/lang/String;

    .line 207
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 208
    return-void

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1000dd

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 162
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->mActivity:Landroid/app/Activity;

    const v1, 0x7f100129

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->showLoading(Landroid/app/Activity;I)V

    .line 190
    :cond_0
    return-void
.end method
