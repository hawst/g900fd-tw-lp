.class public Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SmartFavoriteTask"
.end annotation


# instance fields
.field private mFavoriteSelection:[J

.field private mIsRemovable:Z

.field private mItemIds:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;[J[JZZ)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "itemIds"    # [J
    .param p3, "favoriteSelection"    # [J
    .param p4, "isRemovable"    # Z
    .param p5, "finish"    # Z

    .prologue
    .line 595
    invoke-direct {p0, p1, p5}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 596
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mItemIds:[J

    .line 597
    iput-object p3, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mFavoriteSelection:[J

    .line 598
    iput-boolean p4, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mIsRemovable:Z

    .line 602
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mIsSupportProgress:Z

    .line 603
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 607
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mItemIds:[J

    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mFavoriteSelection:[J

    iget-boolean v3, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mIsRemovable:Z

    # invokes: Lcom/samsung/musicplus/util/FileOperationTask;->setSmartFavorites(Landroid/content/Context;[J[JZ)I
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask;->access$200(Landroid/content/Context;[J[JZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 578
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 612
    iget-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mIsRemovable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mFavoriteSelection:[J

    if-nez v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000c

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mMessage:Ljava/lang/String;

    .line 624
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 625
    return-void

    .line 616
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mItemIds:[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 617
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0009

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mMessage:Ljava/lang/String;

    goto :goto_0

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000f

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->mMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 578
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
