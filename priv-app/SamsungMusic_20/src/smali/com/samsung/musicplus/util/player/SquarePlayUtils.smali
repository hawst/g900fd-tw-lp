.class public Lcom/samsung/musicplus/util/player/SquarePlayUtils;
.super Ljava/lang/Object;
.source "SquarePlayUtils.java"

# interfaces
.implements Lcom/samsung/musicplus/service/MediaCommandAction$CommandMood;


# static fields
.field private static final CALM_CELLS:[Ljava/lang/String;

.field private static final CALM_CELLS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final EXCITING_CELLS:[Ljava/lang/String;

.field private static final EXCITING_CELLS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final JOY_CELLS:[Ljava/lang/String;

.field private static final JOY_CELLS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PASSIONATE_CELLS:[Ljava/lang/String;

.field private static final PASSIONATE_CELLS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "15"

    aput-object v1, v0, v3

    const-string v1, "16"

    aput-object v1, v0, v4

    const-string v1, "17"

    aput-object v1, v0, v5

    const-string v1, "18"

    aput-object v1, v0, v6

    const-string v1, "19"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "20"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "21"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "22"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "23"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "24"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->CALM_CELLS:[Ljava/lang/String;

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "3"

    aput-object v1, v0, v3

    const-string v1, "4"

    aput-object v1, v0, v4

    const-string v1, "8"

    aput-object v1, v0, v5

    const-string v1, "9"

    aput-object v1, v0, v6

    const-string v1, "13"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "14"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "18"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "19"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "23"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "24"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->JOY_CELLS:[Ljava/lang/String;

    .line 42
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v3

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "5"

    aput-object v1, v0, v5

    const-string v1, "6"

    aput-object v1, v0, v6

    const-string v1, "10"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "11"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "15"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "16"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "20"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "21"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->PASSIONATE_CELLS:[Ljava/lang/String;

    .line 49
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v3

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "2"

    aput-object v1, v0, v5

    const-string v1, "3"

    aput-object v1, v0, v6

    const-string v1, "4"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "9"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->EXCITING_CELLS:[Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->JOY_CELLS:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->JOY_CELLS_SET:Ljava/util/Set;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->EXCITING_CELLS:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->EXCITING_CELLS_SET:Ljava/util/Set;

    .line 58
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->PASSIONATE_CELLS:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->PASSIONATE_CELLS_SET:Ljava/util/Set;

    .line 61
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->CALM_CELLS:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->CALM_CELLS_SET:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method private static getSongList(Landroid/content/Context;Ljava/lang/String;)[J
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mood"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "calm"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCalmUri()Landroid/net/Uri;

    move-result-object v1

    .line 84
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 85
    const/4 v6, 0x0

    .line 87
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "source_id AS _id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "mood_cell"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "year_cell"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "title_key"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 96
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 98
    if-eqz v6, :cond_1

    .line 99
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 103
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    return-object v0

    .line 76
    :cond_2
    const-string v2, "exciting"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 77
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareExcitingUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 78
    :cond_3
    const-string v2, "joyful"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 79
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareJoyfulUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 80
    :cond_4
    const-string v2, "passionate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquarePassionateUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 98
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 99
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public static play(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mood"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->savePrefSelectSquareCells(Landroid/content/Context;Ljava/lang/String;)V

    .line 65
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->getSongList(Landroid/content/Context;Ljava/lang/String;)[J

    move-result-object v0

    .line 66
    .local v0, "songIds":[J
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 67
    const v1, 0x2000a

    const-string v2, "_id"

    invoke-static {v2, v0}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v0, v3}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JI)V

    .line 70
    :cond_0
    return-void
.end method

.method private static savePrefSelectSquareCells(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v1, 0x0

    .line 109
    .local v1, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "exciting"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->EXCITING_CELLS_SET:Ljava/util/Set;

    .line 119
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 123
    const-string v2, "music_player_pref"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "savedSquare"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 127
    .end local v0    # "sp":Landroid/content/SharedPreferences;
    :cond_1
    return-void

    .line 111
    :cond_2
    const-string v2, "calm"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 112
    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->CALM_CELLS_SET:Ljava/util/Set;

    goto :goto_0

    .line 113
    :cond_3
    const-string v2, "passionate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 114
    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->PASSIONATE_CELLS_SET:Ljava/util/Set;

    goto :goto_0

    .line 115
    :cond_4
    const-string v2, "joyful"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    sget-object v1, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->JOY_CELLS_SET:Ljava/util/Set;

    goto :goto_0
.end method
