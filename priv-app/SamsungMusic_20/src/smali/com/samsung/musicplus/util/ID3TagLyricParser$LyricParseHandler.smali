.class Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;
.super Landroid/os/Handler;
.source "ID3TagLyricParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ID3TagLyricParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LyricParseHandler"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/util/ID3TagLyricParser;Landroid/os/Looper;Landroid/content/Context;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "c"    # Landroid/content/Context;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    .line 153
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 154
    iput-object p3, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->context:Landroid/content/Context;

    .line 155
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0x12c

    .line 159
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x190

    if-ne v2, v3, :cond_3

    .line 160
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    # getter for: Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->access$000(Lcom/samsung/musicplus/util/ID3TagLyricParser;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    # getter for: Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->access$000(Lcom/samsung/musicplus/util/ID3TagLyricParser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 163
    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 165
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 166
    const-string v2, "file%3A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 169
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->context:Landroid/content/Context;

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->convertToStoragePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    :cond_1
    :goto_0
    const-string v2, "MusicLyric"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LyricHandler path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    # invokes: Lcom/samsung/musicplus/util/ID3TagLyricParser;->getLyricFromFile(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->access$100(Lcom/samsung/musicplus/util/ID3TagLyricParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "lyric":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    # getter for: Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->access$000(Lcom/samsung/musicplus/util/ID3TagLyricParser;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 180
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    # getter for: Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->access$000(Lcom/samsung/musicplus/util/ID3TagLyricParser;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->this$0:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    # getter for: Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->access$000(Lcom/samsung/musicplus/util/ID3TagLyricParser;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 184
    .end local v0    # "lyric":Ljava/lang/String;
    .end local v1    # "path":Ljava/lang/String;
    :cond_3
    return-void

    .line 170
    .restart local v1    # "path":Ljava/lang/String;
    :cond_4
    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 171
    iget-object v2, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->context:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/samsung/musicplus/util/UiUtils;->convertToStoragePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 172
    :cond_5
    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    const/4 v1, 0x0

    goto :goto_0
.end method
