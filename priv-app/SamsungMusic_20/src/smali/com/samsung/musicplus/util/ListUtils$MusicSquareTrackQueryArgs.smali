.class public Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicSquareTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 978
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 979
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 980
    :cond_0
    const-string v0, "_id = \'\' AND is_music=1"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;->selection:Ljava/lang/String;

    .line 984
    :goto_0
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 985
    const-string v0, "title_key"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 986
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 987
    return-void

    .line 982
    :cond_1
    iput-object p1, p0, Lcom/samsung/musicplus/util/ListUtils$MusicSquareTrackQueryArgs;->selection:Ljava/lang/String;

    goto :goto_0
.end method
