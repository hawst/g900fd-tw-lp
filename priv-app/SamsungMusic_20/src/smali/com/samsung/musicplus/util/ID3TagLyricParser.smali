.class public Lcom/samsung/musicplus/util/ID3TagLyricParser;
.super Ljava/lang/Object;
.source "ID3TagLyricParser.java"

# interfaces
.implements Lcom/samsung/musicplus/util/LyricsLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/ID3TagLyricParser$1;,
        Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;,
        Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;,
        Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;,
        Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;
    }
.end annotation


# static fields
.field private static final ID3_HEADER_IDENTIFIER:Ljava/lang/String; = "494433"

.field private static final SHOW_ID3TAG_LOG:Z = false

.field private static final TAG:Ljava/lang/String; = "MusicLyric"

.field private static sAllTagSize:I

.field private static sFile:Ljava/io/RandomAccessFile;

.field private static sIsCancel:Z

.field private static sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

.field private static sMajorVersion:B


# instance fields
.field private mLyricParserHandler:Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

.field private mLyricThread:Landroid/os/HandlerThread;

.field private mUiUpdateHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sIsCancel:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/util/ID3TagLyricParser;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/ID3TagLyricParser;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/util/ID3TagLyricParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/util/ID3TagLyricParser;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->getLyricFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/util/ID3TagLyricParser;
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 108
    sget-object v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    if-nez v0, :cond_1

    .line 109
    const-class v1, Lcom/samsung/musicplus/util/ID3TagLyricParser;

    monitor-enter v1

    .line 110
    :try_start_0
    sget-object v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ID3TagLyricParser;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    .line 112
    sget-object v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->start(Landroid/content/Context;)V

    .line 114
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    return-object v0

    .line 114
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getLyric()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 273
    new-instance v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;

    invoke-direct {v3, v4}, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;-><init>(Lcom/samsung/musicplus/util/ID3TagLyricParser$1;)V

    .line 275
    .local v3, "tag":Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;
    :goto_0
    sget-object v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v6, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->type:[B

    invoke-virtual {v5, v6}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 276
    sget-boolean v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sIsCancel:Z

    if-eqz v5, :cond_1

    .line 323
    :cond_0
    :goto_1
    return-object v4

    .line 283
    :cond_1
    sget-object v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v6, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->size:[B

    invoke-virtual {v5, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 285
    const/4 v2, 0x0

    .line 287
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v5, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->size:[B

    array-length v5, v5

    if-ge v0, v5, :cond_4

    .line 288
    sget-boolean v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sIsCancel:Z

    if-nez v5, :cond_0

    .line 294
    sget-byte v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sMajorVersion:B

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    .line 295
    iget-object v5, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->size:[B

    aget-byte v5, v5, v0

    and-int/lit16 v5, v5, 0xff

    iget-object v6, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->size:[B

    array-length v6, v6

    add-int/lit8 v7, v0, 0x1

    sub-int/2addr v6, v7

    mul-int/lit8 v6, v6, 0x8

    shl-int/2addr v5, v6

    or-int/2addr v2, v5

    .line 287
    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 296
    :cond_3
    sget-byte v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sMajorVersion:B

    const/4 v6, 0x4

    if-ne v5, v6, :cond_2

    .line 298
    iget-object v5, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->size:[B

    aget-byte v5, v5, v0

    and-int/lit8 v5, v5, 0x7f

    iget-object v6, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->size:[B

    array-length v6, v6

    add-int/lit8 v7, v0, 0x1

    sub-int/2addr v6, v7

    mul-int/lit8 v6, v6, 0x7

    shl-int/2addr v5, v6

    or-int/2addr v2, v5

    goto :goto_3

    .line 302
    :cond_4
    iget-object v5, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->type:[B

    const-string v6, "ISO-8859-1"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toString([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 308
    .local v1, "id3tag":Ljava/lang/String;
    if-nez v2, :cond_5

    .line 309
    const/16 v2, 0x8

    .line 312
    :cond_5
    sget v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sAllTagSize:I

    sub-int/2addr v5, v2

    sput v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sAllTagSize:I

    const/4 v6, 0x1

    if-lt v5, v6, :cond_0

    .line 316
    sget-object v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v6, v3, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagFields;->flag:[B

    invoke-virtual {v5, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 318
    const-string v5, "USLT"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 319
    invoke-static {v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->parsingLyric(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 321
    :cond_6
    sget-object v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v5, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_0
.end method

.method private declared-synchronized getLyricFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 211
    monitor-enter p0

    if-nez p1, :cond_1

    .line 212
    :try_start_0
    const-string v2, "MusicLyric"

    const-string v3, "getLyricFromFile() - path is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 217
    :cond_1
    :try_start_1
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "r"

    invoke-direct {v2, p1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    .line 218
    invoke-static {}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->isId3Tag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 220
    const/4 v2, 0x0

    sput-boolean v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sIsCancel:Z

    .line 221
    invoke-static {}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->getLyric()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 227
    :try_start_2
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    if-eqz v2, :cond_0

    .line 228
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string v2, "MusicLyric"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException when closing file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 211
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 227
    :cond_2
    :try_start_4
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    if-eqz v2, :cond_0

    .line 228
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 230
    :catch_1
    move-exception v0

    .line 231
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v2, "MusicLyric"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException when closing file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 223
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 224
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_6
    const-string v2, "MusicLyric"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException when get lyric from file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 227
    :try_start_7
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    if-eqz v2, :cond_0

    .line 228
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 230
    :catch_3
    move-exception v0

    .line 231
    :try_start_8
    const-string v2, "MusicLyric"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException when closing file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 226
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v1

    .line 227
    :try_start_9
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    if-eqz v2, :cond_3

    .line 228
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 232
    :cond_3
    :goto_1
    :try_start_a
    throw v1

    .line 230
    :catch_4
    move-exception v0

    .line 231
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v2, "MusicLyric"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException when closing file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_1
.end method

.method private static isDescripto([B)Z
    .locals 2
    .param p0, "input"    # [B

    .prologue
    .line 398
    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "descripto":Ljava/lang/String;
    const-string v1, "FFFE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FEFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isId3Tag()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 240
    const/4 v2, 0x0

    .line 241
    .local v2, "isId3Tag":Z
    new-instance v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;-><init>(Lcom/samsung/musicplus/util/ID3TagLyricParser$1;)V

    .line 242
    .local v0, "header":Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;
    sput-byte v7, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sMajorVersion:B

    .line 243
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 245
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v4, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->identifier:[B

    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->read([B)I

    .line 246
    const-string v3, "494433"

    iget-object v4, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->identifier:[B

    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 248
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v4, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->version:[B

    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->read([B)I

    .line 249
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v4, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->flags:[B

    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->read([B)I

    .line 250
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v4, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->size:[B

    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->read([B)I

    .line 252
    sput v7, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sAllTagSize:I

    .line 253
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->size:[B

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 254
    sget v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sAllTagSize:I

    iget-object v4, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->size:[B

    aget-byte v4, v4, v1

    and-int/lit8 v4, v4, 0x7f

    iget-object v5, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->size:[B

    array-length v5, v5

    add-int/lit8 v6, v1, 0x1

    sub-int/2addr v5, v6

    mul-int/lit8 v5, v5, 0x7

    shl-int/2addr v4, v5

    or-int/2addr v3, v4

    sput v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sAllTagSize:I

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 256
    :cond_0
    iget-object v3, v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagHeader;->version:[B

    aget-byte v3, v3, v7

    sput-byte v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sMajorVersion:B

    .line 264
    const/4 v2, 0x1

    .line 269
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method private static isKiesLyric([B)Z
    .locals 11
    .param p0, "content"    # [B

    .prologue
    const/16 v10, 0x10

    const/16 v9, 0x8

    const/4 v8, 0x2

    .line 440
    const/4 v3, 0x0

    .line 442
    .local v3, "isKiesLyric":Z
    array-length v6, p0

    if-le v6, v9, :cond_0

    const/4 v6, 0x0

    aget-byte v6, p0, v6

    if-nez v6, :cond_0

    const/4 v6, 0x1

    aget-byte v6, p0, v6

    const/16 v7, 0x5b

    if-ne v6, v7, :cond_0

    aget-byte v6, p0, v8

    const/16 v7, 0x4d

    if-ne v6, v7, :cond_0

    const/4 v6, 0x3

    aget-byte v6, p0, v6

    const/16 v7, 0x5f

    if-ne v6, v7, :cond_0

    const/4 v6, 0x4

    aget-byte v6, p0, v6

    const/16 v7, 0x43

    if-ne v6, v7, :cond_0

    const/4 v6, 0x5

    aget-byte v6, p0, v6

    const/16 v7, 0x49

    if-ne v6, v7, :cond_0

    const/4 v6, 0x6

    aget-byte v6, p0, v6

    const/16 v7, 0x54

    if-ne v6, v7, :cond_0

    const/4 v6, 0x7

    aget-byte v6, p0, v6

    const/16 v7, 0x59

    if-ne v6, v7, :cond_0

    aget-byte v6, p0, v9

    const/16 v7, 0x5d

    if-ne v6, v7, :cond_0

    .line 445
    const/4 v3, 0x1

    .line 449
    :cond_0
    array-length v6, p0

    if-le v6, v10, :cond_2

    .line 451
    :try_start_0
    const-string v6, "EUC-KR"

    invoke-static {p0, v6}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toString([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 452
    .local v5, "strContent":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 453
    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 454
    .local v4, "start":I
    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 455
    .local v1, "end":I
    if-lez v4, :cond_2

    if-lez v1, :cond_2

    if-ge v4, v1, :cond_2

    sub-int v6, v1, v4

    if-lt v6, v8, :cond_2

    .line 456
    add-int/lit8 v2, v1, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v10, :cond_2

    .line 457
    aget-byte v6, p0, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v6, :cond_1

    .line 458
    const/4 v3, 0x1

    .line 456
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 460
    :cond_1
    const/4 v3, 0x0

    .line 472
    .end local v1    # "end":I
    .end local v2    # "i":I
    .end local v4    # "start":I
    .end local v5    # "strContent":Ljava/lang/String;
    :cond_2
    :goto_1
    return v3

    .line 466
    .restart local v5    # "strContent":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 468
    .end local v5    # "strContent":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 469
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private static isNull([B)Z
    .locals 2
    .param p0, "input"    # [B

    .prologue
    .line 404
    const-string v0, "0000"

    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static nonUnicodeParsing(Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;I)Ljava/lang/String;
    .locals 5
    .param p0, "tag"    # Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 409
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v4, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->nonUnicodDescripto:[B

    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->read([B)I

    .line 411
    iget-object v3, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->nonUnicodDescripto:[B

    array-length v3, v3

    sub-int v1, p1, v3

    .line 412
    .local v1, "contentSize":I
    if-gtz v1, :cond_1

    .line 413
    const/4 v2, 0x0

    .line 436
    :cond_0
    :goto_0
    return-object v2

    .line 416
    :cond_1
    new-array v0, v1, [B

    .line 418
    .local v0, "content":[B
    sget-object v3, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->read([B)I

    .line 420
    const/4 v2, 0x0

    .line 421
    .local v2, "lyric":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->isKiesLyric([B)Z

    move-result v3

    if-nez v3, :cond_0

    .line 422
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_JPN_LYRIC:Z

    if-eqz v3, :cond_2

    .line 423
    const-string v3, "SHIFT-JIS"

    invoke-static {v0, v3}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toStringWithNewLine([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 424
    :cond_2
    const-string v3, "Korea"

    sget-object v4, Lcom/samsung/musicplus/library/MusicFeatures;->SYSTEM_CONTRY_CODE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 425
    const-string v3, "EUC-KR"

    invoke-static {v0, v3}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toStringWithNewLine([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 427
    :cond_3
    const-string v3, "ISO-8859-1"

    invoke-static {v0, v3}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toStringWithNewLine([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private parserCancel()V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sIsCancel:Z

    .line 204
    return-void
.end method

.method private static parsingLyric(I)Ljava/lang/String;
    .locals 4
    .param p0, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 327
    new-instance v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;-><init>(Lcom/samsung/musicplus/util/ID3TagLyricParser$1;)V

    .line 329
    .local v1, "tag":Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v3, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->encoding:[B

    invoke-virtual {v2, v3}, Ljava/io/RandomAccessFile;->read([B)I

    .line 330
    sget-object v2, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    iget-object v3, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->language:[B

    invoke-virtual {v2, v3}, Ljava/io/RandomAccessFile;->read([B)I

    .line 331
    const/4 v0, 0x0

    .line 333
    .local v0, "lyric":Ljava/lang/String;
    const-string v2, "01"

    iget-object v3, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->encoding:[B

    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 334
    iget-object v2, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->encoding:[B

    array-length v2, v2

    iget-object v3, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->language:[B

    array-length v3, v3

    add-int/2addr v2, v3

    sub-int v2, p0, v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->unicodeParsing(Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;I)Ljava/lang/String;

    move-result-object v0

    .line 338
    :cond_0
    :goto_0
    return-object v0

    .line 335
    :cond_1
    const-string v2, "00"

    iget-object v3, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->encoding:[B

    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 336
    iget-object v2, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->encoding:[B

    array-length v2, v2

    iget-object v3, v1, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->language:[B

    array-length v3, v3

    add-int/2addr v2, v3

    sub-int v2, p0, v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->nonUnicodeParsing(Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private start(Landroid/content/Context;)V
    .locals 3
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 123
    const-string v0, "MusicLyric"

    const-string v1, "ID3TagParser start creating"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "lyric worker"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricThread:Landroid/os/HandlerThread;

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 126
    new-instance v0, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

    iget-object v1, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;-><init>(Lcom/samsung/musicplus/util/ID3TagLyricParser;Landroid/os/Looper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricParserHandler:Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

    .line 127
    const-string v0, "MusicLyric"

    const-string v1, "ID3TagParser end creating"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method private static unicodeParsing(Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;I)Ljava/lang/String;
    .locals 10
    .param p0, "tag"    # Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 342
    const/4 v4, 0x2

    new-array v3, v4, [B

    .line 343
    .local v3, "tempDescripto":[B
    :cond_0
    :goto_0
    sget-object v4, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v4, v3}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    .line 344
    sget-boolean v4, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sIsCancel:Z

    if-eqz v4, :cond_2

    .line 394
    :cond_1
    :goto_1
    return-object v2

    .line 350
    :cond_2
    array-length v4, v3

    sub-int/2addr p1, v4

    .line 354
    invoke-static {v3}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->isDescripto([B)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 355
    invoke-virtual {v3}, [B->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    iput-object v4, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->unicodDescripto:[B

    goto :goto_0

    .line 358
    :cond_3
    invoke-static {v3}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->isNull([B)Z

    move-result v4

    if-nez v4, :cond_0

    .line 363
    array-length v4, v3

    add-int/2addr p1, v4

    .line 364
    sget-object v4, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    sget-object v5, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    array-length v5, v3

    int-to-long v8, v5

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 368
    :cond_4
    if-lez p1, :cond_1

    .line 372
    new-array v0, p1, [B

    .line 374
    .local v0, "content":[B
    sget-object v4, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v4, v0}, Ljava/io/RandomAccessFile;->read([B)I

    .line 376
    const/4 v2, 0x0

    .line 382
    .local v2, "lyric":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser$ID3TagUSLT;->unicodDescripto:[B

    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v1

    .line 383
    .local v1, "descripto":Ljava/lang/String;
    const-string v4, "FFFE"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 384
    const-string v4, "UTF-16LE"

    invoke-static {v0, v4}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toStringWithNewLine([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 385
    :cond_5
    const-string v4, "FEFF"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 386
    const-string v4, "UTF-16BE"

    invoke-static {v0, v4}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toStringWithNewLine([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public getLyricInBackgroud(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x190

    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricParserHandler:Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->removeMessages(I)V

    .line 145
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->parserCancel()V

    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricParserHandler:Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

    iget-object v1, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricParserHandler:Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

    invoke-virtual {v1, v2, p1}, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->sendMessage(Landroid/os/Message;)Z

    .line 147
    return-void
.end method

.method public loadLyrics(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 0
    .param p1, "uiHandler"    # Landroid/os/Handler;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 482
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->setCallbackHandler(Landroid/os/Handler;)V

    .line 483
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->getLyricInBackgroud(Ljava/lang/String;)V

    .line 484
    return-void
.end method

.method public quit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    iput-object v1, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;

    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricParserHandler:Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/ID3TagLyricParser$LyricParseHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 194
    iget-object v0, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mLyricThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 195
    sput-object v1, Lcom/samsung/musicplus/util/ID3TagLyricParser;->sLyricParser:Lcom/samsung/musicplus/util/ID3TagLyricParser;

    .line 196
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 477
    invoke-virtual {p0}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->quit()V

    .line 478
    return-void
.end method

.method public setCallbackHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "h"    # Landroid/os/Handler;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/samsung/musicplus/util/ID3TagLyricParser;->mUiUpdateHandler:Landroid/os/Handler;

    .line 136
    return-void
.end method
