.class public Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComposerTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1264
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1265
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->COMPOSERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1266
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->COMPOSER_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1269
    const-string v0, "composer IS NOT NULL AND composer != \'\'"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->selection:Ljava/lang/String;

    .line 1271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->COMPOSER_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1273
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->COMPOSER_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1274
    const-string v0, "composer"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1276
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1277
    const-string v0, "composer"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1278
    return-void
.end method
