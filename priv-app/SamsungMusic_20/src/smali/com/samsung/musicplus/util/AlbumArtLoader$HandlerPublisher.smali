.class Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;
.super Ljava/lang/Object;
.source "AlbumArtLoader.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/AlbumArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HandlerPublisher"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    iput-object p1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;->mHandler:Landroid/os/Handler;

    .line 266
    return-void
.end method


# virtual methods
.method public onFailPublishImage(Landroid/net/Uri;J)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "elapsedTime"    # J

    .prologue
    .line 277
    return-void
.end method

.method public onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 270
    const-string v0, "handler"

    # invokes: Lcom/samsung/musicplus/util/AlbumArtLoader;->debugBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V
    invoke-static {v0, p2, p1, p3, p4}, Lcom/samsung/musicplus/util/AlbumArtLoader;->access$000(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V

    .line 271
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xc8

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 272
    return-void
.end method
