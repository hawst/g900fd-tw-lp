.class public Lcom/samsung/musicplus/util/Version;
.super Ljava/lang/Object;
.source "Version.java"


# static fields
.field public static final ANDROID_UNDER_KITKAT:Z

.field public static final LIBRARY_API_1:Z

.field public static final LIBRARY_OVER_API_1:Z

.field public static final LIBRARY_OVER_API_2:Z

.field public static final PLAYER_VERSION:D = 2.0


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 10
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/samsung/musicplus/util/Version;->ANDROID_UNDER_KITKAT:Z

    .line 18
    sget v0, Lcom/samsung/musicplus/library/Build$VERSION;->CURRENT:I

    const/4 v3, 0x2

    if-le v0, v3, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_2:Z

    .line 26
    sget v0, Lcom/samsung/musicplus/library/Build$VERSION;->CURRENT:I

    if-le v0, v1, :cond_2

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    .line 31
    sget v0, Lcom/samsung/musicplus/library/Build$VERSION;->CURRENT:I

    if-ne v0, v1, :cond_3

    :goto_3
    sput-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    return-void

    :cond_0
    move v0, v2

    .line 10
    goto :goto_0

    :cond_1
    move v0, v2

    .line 18
    goto :goto_1

    :cond_2
    move v0, v2

    .line 26
    goto :goto_2

    :cond_3
    move v1, v2

    .line 31
    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
