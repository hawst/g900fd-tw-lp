.class public Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;
.super Lcom/samsung/musicplus/util/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddToFavoriteTask"
.end annotation


# instance fields
.field private mList:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;JZZ)V
    .locals 2
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # J
    .param p4, "isAddedFavorite"    # Z
    .param p5, "finish"    # Z

    .prologue
    const/4 v1, 0x0

    .line 549
    invoke-direct {p0, p1, p5}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 550
    const/4 v0, 0x1

    new-array v0, v0, [J

    aput-wide p2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mList:[J

    .line 553
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mIsSupportProgress:Z

    .line 554
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;[JZZ)V
    .locals 0
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "isAddedFavorite"    # Z
    .param p4, "finish"    # Z

    .prologue
    .line 544
    invoke-direct {p0, p1, p4}, Lcom/samsung/musicplus/util/FileOperationTask;-><init>(Landroid/app/Activity;Z)V

    .line 545
    iput-object p2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mList:[J

    .line 546
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 558
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 559
    .local v1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mList:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 560
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mList:[J

    aget-wide v2, v2, v0

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 563
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v4, 0x0

    # invokes: Lcom/samsung/musicplus/util/FileOperationTask;->setFavorites(Landroid/content/Context;Ljava/util/ArrayList;ZZ)V
    invoke-static {v2, v1, v3, v4}, Lcom/samsung/musicplus/util/FileOperationTask;->access$100(Landroid/content/Context;Ljava/util/ArrayList;ZZ)V

    .line 564
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mList:[J

    array-length v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 534
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 569
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000f

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->mMessage:Ljava/lang/String;

    .line 574
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 575
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 534
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
