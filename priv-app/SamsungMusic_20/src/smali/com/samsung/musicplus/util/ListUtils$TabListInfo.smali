.class public Lcom/samsung/musicplus/util/ListUtils$TabListInfo;
.super Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TabListInfo"
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "list"    # I

    .prologue
    const/4 v1, 0x1

    .line 675
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;-><init>()V

    .line 676
    const v0, 0x7f100106

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 677
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 730
    :pswitch_0
    const-string v0, "MusicListUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TabListInfo > invalid tab list : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Did you miss something?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    :goto_0
    return-void

    .line 679
    :pswitch_1
    const v0, 0x7f1000f4

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 680
    const/high16 v0, 0x7f0f0000

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 681
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$AlbumTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 682
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto :goto_0

    .line 685
    :pswitch_2
    const v0, 0x7f1000f5

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 686
    const v0, 0x7f0f0001

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 687
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$ArtistTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 688
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto :goto_0

    .line 691
    :pswitch_3
    const v0, 0x7f1000f6

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 692
    const v0, 0x7f0f0002

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 693
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$ComposerTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 694
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto :goto_0

    .line 697
    :pswitch_4
    const v0, 0x7f100107

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 698
    const v0, 0x7f0f0012

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 699
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$YearTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 702
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto :goto_0

    .line 705
    :pswitch_5
    const v0, 0x7f1000fd

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 706
    const v0, 0x7f0f0005

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 707
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$GenreTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 708
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto :goto_0

    .line 711
    :pswitch_6
    const v0, 0x7f1000fc

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 712
    const v0, 0x7f0f0004

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 713
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 714
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto :goto_0

    .line 718
    :pswitch_7
    const v0, 0x7f100100

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 719
    const v0, 0x7f0f0007

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 720
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$PlaylistTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 721
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto/16 :goto_0

    .line 724
    :pswitch_8
    const v0, 0x7f1000fe

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->noItemTextId:I

    .line 725
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$AllShareTabQueryArgs;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 726
    const v0, 0x7f0f0003

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->numberOfTextId:I

    .line 727
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/ListUtils$TabListInfo;->showIndexView:Z

    goto/16 :goto_0

    .line 677
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
