.class public Lcom/samsung/musicplus/util/KnoxGateManager;
.super Ljava/lang/Object;
.source "KnoxGateManager.java"

# interfaces
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "KnoxModeManager"


# instance fields
.field private mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->findKnox10Method()Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    .line 61
    :cond_0
    return-void
.end method

.method private activeKnox20MoveMenu(Landroid/content/Context;I)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # I

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "active":Z
    const v1, 0x20004

    if-eq p2, v1, :cond_0

    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->availableKnoxPersonalMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isSupportKnoxMove(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    const/4 v0, 0x1

    .line 228
    :cond_0
    return v0
.end method

.method private ensureKnoxServiceConnected(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isServiceConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->startContainerService(Landroid/content/Context;)V

    .line 257
    :cond_0
    return-void
.end method

.method public static isKnoxModeOn(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_2:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setKnoxMenuVisibility(Landroid/content/Context;Landroid/view/Menu;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 125
    sget-boolean v2, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v2, :cond_0

    .line 126
    const v2, 0x7f0d01d4

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 127
    .local v0, "moveToKnox":Landroid/view/MenuItem;
    const v2, 0x7f0d01d5

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 128
    .local v1, "removeFromKnox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->availableKnoxPersonalMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isSupportKnoxMove(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    invoke-static {p0, v0, v1}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnox20Menu(Landroid/content/Context;Landroid/view/MenuItem;Landroid/view/MenuItem;)V

    .line 135
    .end local v0    # "moveToKnox":Landroid/view/MenuItem;
    .end local v1    # "removeFromKnox":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method private static showContainerSelectDialog(Landroid/app/Activity;[JI)V
    .locals 4
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "items"    # [J
    .param p2, "mode"    # I

    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getPersonaList(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;

    move-result-object v1

    .line 96
    .local v1, "list":Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;
    if-eqz v1, :cond_1

    .line 97
    const v2, 0x7f0d01d4

    if-ne p2, v2, :cond_0

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->getSize()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 98
    invoke-static {p1}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->getInstance([J)Landroid/app/DialogFragment;

    move-result-object v0

    .line 99
    .local v0, "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "knoxSelectDialog"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 106
    .end local v0    # "dialog":Landroid/app/DialogFragment;
    :goto_0
    return-void

    .line 101
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->getId(I)I

    move-result v2

    invoke-static {p0, p1, v2}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxMoveDialog(Landroid/app/Activity;[JI)V

    goto :goto_0

    .line 104
    :cond_1
    const-string v2, "KnoxModeManager"

    const-string v3, "PersonaInfoList is null..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static showKnox20Menu(Landroid/content/Context;Landroid/view/MenuItem;Landroid/view/MenuItem;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "move"    # Landroid/view/MenuItem;
    .param p2, "remove"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 204
    invoke-static {p0}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 206
    invoke-interface {p2, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 211
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 209
    invoke-interface {p2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static showKnoxDialog(Landroid/app/Activity;[JI)V
    .locals 3
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "items"    # [J
    .param p2, "mode"    # I

    .prologue
    .line 74
    if-nez p0, :cond_0

    .line 75
    const-string v1, "KnoxModeManager"

    const-string v2, "showKnoxDialog() - Activity is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 80
    .local v0, "context":Landroid/content/Context;
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isOverVersion2(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    invoke-static {v0, p1}, Lcom/samsung/musicplus/util/knox/KnoxUtils;->move(Landroid/content/Context;[J)V

    goto :goto_0

    .line 89
    :cond_1
    const/4 v1, -0x1

    invoke-static {p0, p1, v1}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxMoveDialog(Landroid/app/Activity;[JI)V

    goto :goto_0
.end method

.method private static showKnoxMoveDialog(Landroid/app/Activity;[JI)V
    .locals 4
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "items"    # [J
    .param p2, "destContainerId"    # I

    .prologue
    .line 109
    new-instance v0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    int-to-long v2, p2

    invoke-direct {v0, p1, v2, v3}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;-><init>([JJ)V

    .line 110
    .local v0, "dialog":Landroid/app/DialogFragment;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/DialogFragment;->setCancelable(Z)V

    .line 111
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "knoxMoveDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 112
    return-void
.end method


# virtual methods
.method public activeKnox10MoveMenu(Landroid/content/Context;I)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # I

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "active":Z
    const v1, 0x20004

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    # invokes: Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->availableKnoxPersonalMode(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;)Z
    invoke-static {v1}, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->access$000(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/KnoxGateManager;->ensureKnoxServiceConnected(Landroid/content/Context;)V

    .line 247
    const/4 v0, 0x1

    .line 250
    :cond_0
    return v0
.end method

.method public disableKnoxMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 184
    const v2, 0x7f0d01d4

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 185
    .local v0, "moveToKnox":Landroid/view/MenuItem;
    const v2, 0x7f0d01d5

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 187
    .local v1, "removeFromKnox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 188
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 190
    :cond_0
    if-eqz v1, :cond_1

    .line 191
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 193
    :cond_1
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/samsung/musicplus/util/KnoxGateManager;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->unbindService()V

    .line 270
    :cond_0
    return-void
.end method

.method public setKnoxMenuVisibility(Landroid/content/Context;Landroid/view/Menu;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "menu"    # Landroid/view/Menu;
    .param p3, "listType"    # I

    .prologue
    const/4 v3, 0x1

    .line 149
    if-nez p2, :cond_1

    .line 150
    const-string v2, "KnoxModeManager"

    const-string v3, "handleKnoxModeMenu() from CommonListFragment- Menu is null, Something\'s wrong!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    const v2, 0x7f0d01d4

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 156
    .local v0, "moveToKnox":Landroid/view/MenuItem;
    sget-boolean v2, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v2, :cond_4

    .line 157
    const v2, 0x7f0d01d5

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 158
    .local v1, "removeFromKnox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 160
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/util/KnoxGateManager;->activeKnox20MoveMenu(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 161
    invoke-static {p1, v0, v1}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnox20Menu(Landroid/content/Context;Landroid/view/MenuItem;Landroid/view/MenuItem;)V

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {p0, p1, p3}, Lcom/samsung/musicplus/util/KnoxGateManager;->activeKnox10MoveMenu(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 165
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 167
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 172
    .end local v1    # "removeFromKnox":Landroid/view/MenuItem;
    :cond_4
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p3}, Lcom/samsung/musicplus/util/KnoxGateManager;->activeKnox10MoveMenu(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    const v2, 0x7f02000c

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 174
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
