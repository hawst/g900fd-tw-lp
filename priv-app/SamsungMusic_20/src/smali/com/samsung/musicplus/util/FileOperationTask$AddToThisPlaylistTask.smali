.class public Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;
.super Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddToThisPlaylistTask"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;[JJ)V
    .locals 7
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "list"    # [J
    .param p3, "playlistId"    # J

    .prologue
    const/4 v6, 0x0

    .line 452
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;-><init>(Landroid/app/Activity;[JJZ)V

    .line 453
    iput-boolean v6, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->mIsSupportProgress:Z

    .line 454
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 458
    invoke-super {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 459
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->mActivity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/samsung/musicplus/player/PlayerActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerActivity;->hideAddToPlaylist()V

    .line 464
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 444
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
