.class public Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(J)V
    .locals 3
    .param p1, "audioId"    # J

    .prologue
    .line 929
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 930
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->uri:Landroid/net/Uri;

    .line 931
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->projection:[Ljava/lang/String;

    .line 932
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 933
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 934
    const-string v0, "title != \'\' AND is_music=1"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->selection:Ljava/lang/String;

    .line 936
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    long-to-int v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 942
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 943
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 944
    const-string v0, "artist"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 945
    const-string v0, "album"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->text3Col:Ljava/lang/String;

    .line 946
    const-string v0, "duration"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 947
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 948
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 949
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 950
    const-string v0, "is_secretbox"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->secretBoxCol:Ljava/lang/String;

    .line 951
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 903
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 904
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->uri:Landroid/net/Uri;

    .line 905
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALL_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->projection:[Ljava/lang/String;

    .line 906
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 907
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 908
    const-string v0, "title != \'\' AND is_music=1"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->selection:Ljava/lang/String;

    .line 909
    if-nez p1, :cond_0

    .line 910
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 918
    :goto_0
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 919
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 920
    const-string v0, "artist"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 921
    const-string v0, "album"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->text3Col:Ljava/lang/String;

    .line 922
    const-string v0, "duration"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 923
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 924
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 925
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 926
    const-string v0, "is_secretbox"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->secretBoxCol:Ljava/lang/String;

    .line 927
    return-void

    .line 912
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    goto :goto_0
.end method
