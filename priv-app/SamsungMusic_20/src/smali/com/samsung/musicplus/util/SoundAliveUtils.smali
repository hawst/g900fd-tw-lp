.class public Lcom/samsung/musicplus/util/SoundAliveUtils;
.super Ljava/lang/Object;
.source "SoundAliveUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    }
.end annotation


# static fields
.field public static UNDEFINED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, -0x1

    sput v0, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static getEffectName(I)I
    .locals 5
    .param p0, "mode"    # I

    .prologue
    .line 56
    invoke-static {}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->values()[Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 57
    .local v1, "el":Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 58
    invoke-virtual {v1}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->getName()I

    move-result v4

    .line 61
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    :goto_1
    return v4

    .line 56
    .restart local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "el":Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;
    :cond_1
    sget-object v4, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->NORMAL:Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;

    # getter for: Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->name:I
    invoke-static {v4}, Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;->access$000(Lcom/samsung/musicplus/util/SoundAliveUtils$EffectList;)I

    move-result v4

    goto :goto_1
.end method

.method public static getSoundAliveErrorMessage(II)I
    .locals 2
    .param p0, "audioPath"    # I
    .param p1, "soundAlive"    # I

    .prologue
    .line 105
    sget v0, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    .line 106
    .local v0, "message":I
    sparse-switch p0, :sswitch_data_0

    .line 137
    :cond_0
    :goto_0
    return v0

    .line 112
    :sswitch_0
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveSpeaker(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    const v0, 0x7f10016a

    goto :goto_0

    .line 117
    :sswitch_1
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveBT(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    const v0, 0x7f100167

    goto :goto_0

    .line 122
    :sswitch_2
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveLineOut(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    const v0, 0x7f100169

    goto :goto_0

    .line 127
    :sswitch_3
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveHDMI(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    const v0, 0x7f100169

    goto :goto_0

    .line 106
    :sswitch_data_0
    .sparse-switch
        -0xa -> :sswitch_0
        0x2 -> :sswitch_0
        0x380 -> :sswitch_1
        0x400 -> :sswitch_3
        0x1800 -> :sswitch_2
    .end sparse-switch
.end method
