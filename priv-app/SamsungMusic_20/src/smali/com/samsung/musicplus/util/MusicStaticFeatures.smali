.class public Lcom/samsung/musicplus/util/MusicStaticFeatures;
.super Ljava/lang/Object;
.source "MusicStaticFeatures.java"


# static fields
.field public static final FLAG_CHECK_IMEI_WHEN_HANDLE_ACTION_MEDIA_BUTTON:Z

.field public static final FLAG_CHECK_KOR_LGT:Z

.field public static final FLAG_CHECK_KOR_SKT:Z

.field public static final FLAG_CHECK_MTPACTIVITY:Z

.field public static final FLAG_K2HD:Z

.field public static final FLAG_MASS_PROJECT:Z

.field public static final FLAG_MUSIC_LDB:Z

.field public static final FLAG_MUSIC_MID_VOLUME_CONTROL:Z

.field public static final FLAG_MUSIC_REPEAT_AB:Z

.field public static final FLAG_MUSIC_SUPPORT_SRS_SOUNDALIVE:Z

.field public static final FLAG_QC_LPA_ENABLE:Z

.field public static final FLAG_SMART_VOLUME:Z

.field public static final FLAG_SUPPORT_ADAPT_SOUND:Z

.field public static final FLAG_SUPPORT_ALLSHARE:Z

.field public static final FLAG_SUPPORT_AUTO_RECOMMENDATION:Z

.field public static final FLAG_SUPPORT_CALL_RINGTONE_DUOS_CDMAGSM:Z

.field public static final FLAG_SUPPORT_CALL_RINGTONE_DUOS_CGG:Z

.field public static final FLAG_SUPPORT_DLNA_DMC_ONLY:Z

.field public static final FLAG_SUPPORT_DLNA_MULTI_SPEAKER:Z

.field public static final FLAG_SUPPORT_DSDS:Z

.field public static final FLAG_SUPPORT_DUALMODE:Z

.field public static final FLAG_SUPPORT_FEATURE_NTT_RINGTONE_DRM_CHECK:Z

.field public static final FLAG_SUPPORT_FINGER_AIR_VIEW:Z

.field public static final FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

.field public static final FLAG_SUPPORT_MIRROR_CALL:Z

.field public static final FLAG_SUPPORT_MOTION_TURN_OVER:Z

.field public static final FLAG_SUPPORT_MOVE_TO_KNOX:Z

.field public static final FLAG_SUPPORT_MULTI_SIM:Z

.field public static final FLAG_SUPPORT_MULTI_SPEAKER:Z

.field public static final FLAG_SUPPORT_NEW_SOUNDALIVE:Z

.field public static final FLAG_SUPPORT_NUMERIC_KEYPAD:Z

.field public static final FLAG_SUPPORT_SAMSUNG_MUSIC:Z

.field public static final FLAG_SUPPORT_SECRET_BOX:Z

.field public static final FLAG_SUPPORT_SET_BATTERY_ADC:Z

.field public static final FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

.field public static final FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION:Z

.field public static final FLAG_SUPPORT_SVOICE_FOR_GEAR:Z

.field public static final FLAG_SUPPORT_SWEEP_ON_TABS:Z

.field public static final FLAG_SUPPORT_UHQA:Z

.field public static final FLAG_SUPPORT_WIFIDISPLAY_OLD:Z

.field public static final FLAG_SUPPORT_WIFI_DISPLAY:Z

.field public static final FLAG_SUPPROT_ENHANCED_PLAY_SPEED:Z

.field public static final TAG:Ljava/lang/String; = "MusicFeatures"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "FLAG_SUPPORT_SET_BATTERY_ADC"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SET_BATTERY_ADC:Z

    .line 19
    const-string v0, "FLAG_SUPPORT_MIRROR_CALL"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_MIRROR_CALL:Z

    .line 21
    const-string v0, "FLAG_SUPPORT_FEATURE_NTT_RINGTONE_DRM_CHECK"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_FEATURE_NTT_RINGTONE_DRM_CHECK:Z

    .line 23
    const-string v0, "FLAG_SUPPORT_SPLIT_LIST_VIEW"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    .line 25
    const-string v0, "FLAG_SUPPORT_MOTION_TURN_OVER"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_MOTION_TURN_OVER:Z

    .line 27
    const-string v0, "FLAG_SUPPROT_ENHANCED_PLAY_SPEED"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPROT_ENHANCED_PLAY_SPEED:Z

    .line 29
    const-string v0, "FLAG_QC_LPA_ENABLE"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_QC_LPA_ENABLE:Z

    .line 31
    const-string v0, "FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION:Z

    .line 33
    const-string v0, "FLAG_SMART_VOLUME"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SMART_VOLUME:Z

    .line 35
    const-string v0, "FLAG_K2HD"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    .line 37
    const-string v0, "FLAG_MUSIC_REPEAT_AB"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MUSIC_REPEAT_AB:Z

    .line 39
    const-string v0, "FLAG_MUSIC_LDB"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MUSIC_LDB:Z

    .line 41
    const-string v0, "FLAG_MUSIC_MID_VOLUME_CONTROL"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MUSIC_MID_VOLUME_CONTROL:Z

    .line 43
    const-string v0, "FLAG_MUSIC_SUPPORT_SRS_SOUNDALIVE"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MUSIC_SUPPORT_SRS_SOUNDALIVE:Z

    .line 45
    const-string v0, "FLAG_SUPPORT_CALL_RINGTONE_DUOS_CDMAGSM"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_CALL_RINGTONE_DUOS_CDMAGSM:Z

    .line 47
    const-string v0, "FLAG_SUPPORT_CALL_RINGTONE_DUOS_CGG"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_CALL_RINGTONE_DUOS_CGG:Z

    .line 49
    const-string v0, "FLAG_SUPPORT_DUALMODE"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_DUALMODE:Z

    .line 51
    const-string v0, "FLAG_SUPPORT_DSDS"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_DSDS:Z

    .line 53
    const-string v0, "FLAG_SUPPORT_MULTI_SIM"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_MULTI_SIM:Z

    .line 55
    const-string v0, "FLAG_SUPPORT_GESTURE_AIR_MOTION"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

    .line 57
    const-string v0, "FLAG_SUPPORT_FINGER_AIR_VIEW"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    .line 59
    const-string v0, "FLAG_SUPPORT_ADAPT_SOUND"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_ADAPT_SOUND:Z

    .line 61
    const-string v0, "FLAG_SUPPORT_MULTI_SPEAKER"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_MULTI_SPEAKER:Z

    .line 63
    const-string v0, "FLAG_SUPPORT_AUTO_RECOMMENDATION"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_AUTO_RECOMMENDATION:Z

    .line 65
    const-string v0, "FLAG_SUPPORT_DLNA_MULTI_SPEAKER"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_DLNA_MULTI_SPEAKER:Z

    .line 67
    const-string v0, "FLAG_SUPPORT_WIFI_DISPLAY"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    .line 69
    const-string v0, "FLAG_SUPPORT_NEW_SOUNDALIVE"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_NEW_SOUNDALIVE:Z

    .line 71
    const-string v0, "FLAG_SUPPORT_SECRET_BOX"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SECRET_BOX:Z

    .line 73
    const-string v0, "FLAG_SUPPORT_NUMERIC_KEYPAD"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_NUMERIC_KEYPAD:Z

    .line 75
    const-string v0, "FLAG_SUPPORT_WIFIDISPLAY_OLD"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFIDISPLAY_OLD:Z

    .line 77
    const-string v0, "FLAG_SUPPORT_MOVE_TO_KNOX"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_MOVE_TO_KNOX:Z

    .line 79
    const-string v0, "FLAG_SUPPORT_UHQA"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_UHQA:Z

    .line 81
    const-string v0, "FLAG_CHECK_IMEI_WHEN_HANDLE_ACTION_MEDIA_BUTTON"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_IMEI_WHEN_HANDLE_ACTION_MEDIA_BUTTON:Z

    .line 83
    const-string v0, "FLAG_SUPPORT_SAMSUNG_MUSIC"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SAMSUNG_MUSIC:Z

    .line 85
    const-string v0, "FLAG_CHECK_MTPACTIVITY"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_MTPACTIVITY:Z

    .line 87
    const-string v0, "FLAG_CHECK_KOR_SKT"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_SKT:Z

    .line 89
    const-string v0, "FLAG_CHECK_KOR_LGT"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    .line 91
    const-string v0, "FLAG_SUPPORT_DLNA_DMC_ONLY"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_DLNA_DMC_ONLY:Z

    .line 93
    const-string v0, "FLAG_SUPPORT_SVOICE_FOR_GEAR"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SVOICE_FOR_GEAR:Z

    .line 95
    const-string v0, "FLAG_SUPPORT_ALLSHARE"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_ALLSHARE:Z

    .line 97
    const-string v0, "IS_MASS_PROJECT"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    .line 99
    const-string v0, "FLAG_SUPPORT_SWEEP_ON_TABS"

    invoke-static {v0}, Lcom/samsung/musicplus/util/MusicStaticFeatures;->getMusicFeatures(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SWEEP_ON_TABS:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final getMusicFeatures(Ljava/lang/String;)Z
    .locals 6
    .param p0, "feature"    # Ljava/lang/String;

    .prologue
    .line 102
    const/4 v2, 0x0

    .line 104
    .local v2, "on":Z
    :try_start_0
    const-class v3, Lcom/samsung/musicplus/library/MusicFeatures;

    invoke-virtual {v3, p0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 106
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 115
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return v2

    .line 107
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    const-string v3, "MusicFeatures"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not supported, illegal argument"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 112
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v3, "MusicFeatures"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not supported, no filed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 109
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :try_start_3
    const-string v3, "MusicFeatures"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not supported, illegal access"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method
