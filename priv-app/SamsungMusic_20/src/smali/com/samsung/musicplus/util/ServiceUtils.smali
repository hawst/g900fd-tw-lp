.class public Lcom/samsung/musicplus/util/ServiceUtils;
.super Ljava/lang/Object;
.source "ServiceUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;,
        Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final UNDEFINED:I = -0x1

.field private static sConnectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;",
            ">;"
        }
    .end annotation
.end field

.field public static sService:Lcom/samsung/musicplus/service/IPlayerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/samsung/musicplus/util/ServiceUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->sConnectionMap:Ljava/util/HashMap;

    .line 68
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method public static bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 79
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 81
    .local v0, "cw":Landroid/content/ContextWrapper;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 82
    new-instance v1, Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;

    invoke-direct {v1, p1}, Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;-><init>(Landroid/content/ServiceConnection;)V

    .line 83
    .local v1, "sb":Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    new-instance v2, Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    invoke-direct {v2, v0}, Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;-><init>(Landroid/content/ContextWrapper;)V

    .line 88
    :goto_0
    return-object v2

    .line 87
    :cond_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v3, "Failed to bind to service"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static changeListWithoutPlaying(Landroid/content/Context;ILjava/lang/String;[JIZ)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "playCurrentSong"    # Z

    .prologue
    const/4 v0, 0x0

    .line 184
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 187
    :cond_1
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-eqz v1, :cond_2

    .line 188
    invoke-static {p0, p3, p4}, Lcom/samsung/musicplus/util/ServiceUtils;->checkLgtDrm(Landroid/content/Context;[JI)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    :cond_2
    new-instance v0, Lcom/samsung/musicplus/util/ServiceUtils$2;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/util/ServiceUtils$2;-><init>(ILjava/lang/String;[JIZ)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/util/ServiceUtils$2;->start()V

    .line 209
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static changeToDefaultPlayer()V
    .locals 2

    .prologue
    .line 1259
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1267
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1263
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->changeToDefaultPlayer()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1264
    :catch_0
    move-exception v0

    .line 1265
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static changeToDmrPlayer(Ljava/lang/String;)V
    .locals 2
    .param p0, "dmrId"    # Ljava/lang/String;

    .prologue
    .line 1248
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1256
    :goto_0
    return-void

    .line 1252
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->changeToDmrPlayer(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1253
    :catch_0
    move-exception v0

    .line 1254
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static changeToWfdDevice()V
    .locals 2

    .prologue
    .line 1270
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1278
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1274
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->changeToWfdDevice()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1275
    :catch_0
    move-exception v0

    .line 1276
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private static checkLgtDrm(Landroid/content/Context;[JI)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # [J
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x1

    .line 221
    if-nez p0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v4

    .line 224
    :cond_1
    if-ltz p2, :cond_0

    .line 227
    invoke-static {p0}, Lcom/samsung/musicplus/library/drm/DrmManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/drm/DrmManager;

    move-result-object v0

    .line 229
    .local v0, "dm":Lcom/samsung/musicplus/library/drm/DrmManager;
    :try_start_0
    aget-wide v2, p1, p2

    invoke-static {p0, v2, v3}, Lcom/samsung/musicplus/util/ListUtils;->getLocalFilePath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, "filePath":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/drm/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 231
    const v2, 0x7f10009a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :cond_2
    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->release()V

    goto :goto_0

    .line 236
    .end local v1    # "filePath":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 237
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->release()V

    :cond_3
    throw v2
.end method

.method public static dlnaDmrVolumeDown()V
    .locals 2

    .prologue
    .line 1292
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1300
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1296
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->dlnaDmrVolumeDown()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1297
    :catch_0
    move-exception v0

    .line 1298
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static dlnaDmrVolumeUp()V
    .locals 2

    .prologue
    .line 1303
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1311
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1307
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->dlnaDmrVolumeUp()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1308
    :catch_0
    move-exception v0

    .line 1309
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static doShuffle(Landroid/content/Context;ILjava/lang/String;[J)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J

    .prologue
    .line 141
    const/4 v0, -0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->openList(Landroid/content/Context;ILjava/lang/String;[JI)Z

    .line 142
    return-void
.end method

.method public static enqueue([JI)V
    .locals 2
    .param p0, "list"    # [J
    .param p1, "action"    # I

    .prologue
    .line 860
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 868
    :goto_0
    return-void

    .line 864
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->enqueue([JI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 865
    :catch_0
    move-exception v0

    .line 866
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAlbum()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1109
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1110
    const/4 v1, 0x0

    .line 1118
    .local v1, "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1112
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1114
    .restart local v1    # "name":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getAlbumName()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1115
    :catch_0
    move-exception v0

    .line 1116
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAlbumArt()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1161
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1162
    const/4 v1, 0x0

    .line 1170
    .local v1, "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1164
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1166
    .restart local v1    # "name":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getAlbumArt()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1167
    :catch_0
    move-exception v0

    .line 1168
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAlbumId()J
    .locals 4

    .prologue
    .line 1122
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1123
    const-wide/16 v2, -0x1

    .line 1131
    .local v2, "id":J
    :goto_0
    return-wide v2

    .line 1125
    .end local v2    # "id":J
    :cond_0
    const-wide/16 v2, -0x1

    .line 1127
    .restart local v2    # "id":J
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getAlbumId()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 1128
    :catch_0
    move-exception v0

    .line 1129
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getArtist()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1096
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1097
    const/4 v1, 0x0

    .line 1105
    .local v1, "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1099
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1101
    .restart local v1    # "name":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getArtistName()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1102
    :catch_0
    move-exception v0

    .line 1103
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAudioSessionId()I
    .locals 3

    .prologue
    .line 267
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 268
    const/4 v1, -0x1

    .line 276
    .local v1, "sessionId":I
    :goto_0
    return v1

    .line 270
    .end local v1    # "sessionId":I
    :cond_0
    const/4 v1, -0x1

    .line 272
    .restart local v1    # "sessionId":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getAudioSessionId()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 273
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getBitDepth()I
    .locals 3

    .prologue
    .line 1057
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1058
    const/4 v0, -0x1

    .line 1066
    .local v0, "bit":I
    :goto_0
    return v0

    .line 1060
    .end local v0    # "bit":I
    :cond_0
    const/4 v0, -0x1

    .line 1062
    .restart local v0    # "bit":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getBitDepth()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1063
    :catch_0
    move-exception v1

    .line 1064
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getBuffering()I
    .locals 3

    .prologue
    .line 994
    const/4 v0, -0x1

    .line 996
    .local v0, "buffering":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 997
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->buffering()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1002
    :cond_0
    :goto_0
    return v0

    .line 999
    :catch_0
    move-exception v1

    .line 1000
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCurrentAudioId()J
    .locals 5

    .prologue
    .line 581
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v4, "before getCurrentAudioId()"

    invoke-static {v1, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    const-wide/16 v2, -0x1

    .line 583
    .local v2, "id":J
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 585
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getAudioId()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 590
    :cond_0
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v4, "after getCurrentAudioId()"

    invoke-static {v1, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    return-wide v2

    .line 586
    :catch_0
    move-exception v0

    .line 587
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCurrentBaseUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 935
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v3, :cond_1

    .line 936
    const/4 v1, 0x0

    .line 947
    .local v1, "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-object v1

    .line 938
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_1
    const/4 v1, 0x0

    .line 940
    .restart local v1    # "uri":Landroid/net/Uri;
    :try_start_0
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v3}, Lcom/samsung/musicplus/service/IPlayerService;->getCurrentBaseUri()Ljava/lang/String;

    move-result-object v2

    .line 941
    .local v2, "uriString":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 942
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 944
    .end local v2    # "uriString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 945
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCurrentPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1018
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1019
    const/4 v1, 0x0

    .line 1027
    .local v1, "path":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1021
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1023
    .restart local v1    # "path":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getCurrentPath()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1024
    :catch_0
    move-exception v0

    .line 1025
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCurrentUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 922
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 923
    const/4 v1, 0x0

    .line 931
    .local v1, "uri":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 925
    .end local v1    # "uri":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 927
    .restart local v1    # "uri":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getCurrentUri()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 928
    :catch_0
    move-exception v0

    .line 929
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDeviceId()J
    .locals 4

    .prologue
    .line 1148
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v3, :cond_0

    .line 1149
    const-wide/16 v0, -0x1

    .line 1157
    .local v0, "deviceId":J
    :goto_0
    return-wide v0

    .line 1151
    .end local v0    # "deviceId":J
    :cond_0
    const-wide/16 v0, -0x1

    .line 1153
    .restart local v0    # "deviceId":J
    :try_start_0
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v3}, Lcom/samsung/musicplus/service/IPlayerService;->getDeviceId()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 1154
    :catch_0
    move-exception v2

    .line 1155
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDlnaPlayingDmrId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 663
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 665
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getDlnaPlayingDmrId()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 670
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-object v1

    .line 666
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 667
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 670
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getDlnaPlayingNic()Ljava/lang/String;
    .locals 2

    .prologue
    .line 646
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 648
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getDlnaPlayingNic()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 653
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-object v1

    .line 649
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 650
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 653
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getDuration()J
    .locals 4

    .prologue
    .line 1006
    const-wide/16 v0, -0x1

    .line 1008
    .local v0, "duration":J
    :try_start_0
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v3, :cond_0

    .line 1009
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v3}, Lcom/samsung/musicplus/service/IPlayerService;->duration()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1014
    :cond_0
    :goto_0
    return-wide v0

    .line 1011
    :catch_0
    move-exception v2

    .line 1012
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getGenre()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1135
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1136
    const/4 v1, 0x0

    .line 1144
    .local v1, "genre":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1138
    .end local v1    # "genre":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1140
    .restart local v1    # "genre":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getGenre()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1141
    :catch_0
    move-exception v0

    .line 1142
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 630
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 632
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getKeyWord()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 637
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-object v1

    .line 633
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 634
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 637
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getListItemCount()I
    .locals 3

    .prologue
    .line 595
    const/4 v0, 0x0

    .line 596
    .local v0, "count":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 598
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getListItemCount()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 603
    :cond_0
    :goto_0
    return v0

    .line 599
    :catch_0
    move-exception v1

    .line 600
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getListPosition()I
    .locals 3

    .prologue
    .line 607
    const/4 v1, -0x1

    .line 608
    .local v1, "position":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 610
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getListPosition()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 615
    :cond_0
    :goto_0
    return v1

    .line 611
    :catch_0
    move-exception v0

    .line 612
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getListType()I
    .locals 2

    .prologue
    .line 619
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 621
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getListType()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 626
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 622
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 623
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 626
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static getMediaType()I
    .locals 3

    .prologue
    .line 1031
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1032
    const/4 v1, 0x0

    .line 1040
    .local v1, "type":I
    :goto_0
    return v1

    .line 1034
    .end local v1    # "type":I
    :cond_0
    const/4 v1, 0x0

    .line 1036
    .restart local v1    # "type":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getMediaType()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1037
    :catch_0
    move-exception v0

    .line 1038
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getMimeType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1044
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1045
    const/4 v1, 0x0

    .line 1053
    .local v1, "mime":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1047
    .end local v1    # "mime":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1049
    .restart local v1    # "mime":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getMimeType()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1050
    :catch_0
    move-exception v0

    .line 1051
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getMoodValue()I
    .locals 3

    .prologue
    .line 1394
    const/4 v1, -0x1

    .line 1395
    .local v1, "mood":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 1397
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getMoodValue()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1402
    :cond_0
    :goto_0
    return v1

    .line 1398
    :catch_0
    move-exception v0

    .line 1399
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getNewSoundAliveBandLevel(I)I
    .locals 4
    .param p0, "bandLevel"    # I

    .prologue
    .line 422
    const/4 v1, 0x0

    .line 423
    .local v1, "level":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 425
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2, p0}, Lcom/samsung/musicplus/service/IPlayerService;->getNewSoundAliveBandLevel(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 433
    :cond_0
    :goto_0
    return v1

    .line 426
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 428
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v3, "getNewSoundAliveBandLevel : getParameter() called on uninitialized AudioEffect."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getNewSoundAliveCurrentPreset()I
    .locals 3

    .prologue
    .line 396
    const/4 v1, 0x0

    .line 397
    .local v1, "preset":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 405
    :goto_0
    return v1

    .line 401
    :cond_0
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getNewSoundAliveCurrentPreset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 402
    :catch_0
    move-exception v0

    .line 403
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getNewSoundAliveRoundedStrength(I)I
    .locals 3
    .param p0, "effect"    # I

    .prologue
    .line 437
    const/4 v1, 0x0

    .line 438
    .local v1, "strength":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 440
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2, p0}, Lcom/samsung/musicplus/service/IPlayerService;->getNewSoundAliveRoundedStrength(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 445
    :cond_0
    :goto_0
    return v1

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getNewSoundAliveSquarePosition()[I
    .locals 3

    .prologue
    .line 409
    const/4 v1, 0x0

    .line 410
    .local v1, "position":[I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 418
    :goto_0
    return-object v1

    .line 414
    :cond_0
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getNewSoundAliveSquarePosition()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getNextUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 964
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 965
    const/4 v1, 0x0

    .line 973
    .local v1, "uri":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 967
    .end local v1    # "uri":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 969
    .restart local v1    # "uri":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getNextUri()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 970
    :catch_0
    move-exception v0

    .line 971
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getPersonalMode()I
    .locals 3

    .prologue
    .line 1406
    const/4 v1, -0x1

    .line 1407
    .local v1, "mode":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 1409
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getPersonalMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1414
    :cond_0
    :goto_0
    return v1

    .line 1410
    :catch_0
    move-exception v0

    .line 1411
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getPlaySpeed()F
    .locals 3

    .prologue
    .line 524
    const/high16 v1, 0x3f800000    # 1.0f

    .line 525
    .local v1, "playSpeed":F
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 527
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getPlaySpeed()F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 532
    :cond_0
    :goto_0
    return v1

    .line 528
    :catch_0
    move-exception v0

    .line 529
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getPosition()J
    .locals 4

    .prologue
    .line 977
    const-wide/16 v2, -0x1

    .line 979
    .local v2, "position":J
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 980
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->position()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 985
    :cond_0
    :goto_0
    return-wide v2

    .line 982
    :catch_0
    move-exception v0

    .line 983
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getPreferencesFloat(Ljava/lang/String;F)F
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # F

    .prologue
    .line 1370
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1378
    .end local p1    # "value":F
    :goto_0
    return p1

    .line 1374
    .restart local p1    # "value":F
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->getPreferencesFloat(Ljava/lang/String;F)F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    goto :goto_0

    .line 1375
    :catch_0
    move-exception v0

    .line 1376
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getPrevUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 951
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 952
    const/4 v1, 0x0

    .line 960
    .local v1, "uri":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 954
    .end local v1    # "uri":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 956
    .restart local v1    # "uri":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getPrevUri()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 957
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getRepeat()I
    .locals 3

    .prologue
    .line 1174
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1175
    const/4 v1, 0x0

    .line 1184
    .local v1, "repeat":I
    :goto_0
    return v1

    .line 1178
    .end local v1    # "repeat":I
    :cond_0
    const/4 v1, 0x0

    .line 1180
    .restart local v1    # "repeat":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getRepeat()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1181
    :catch_0
    move-exception v0

    .line 1182
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSamplingRate()I
    .locals 3

    .prologue
    .line 1070
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1071
    const/4 v1, -0x1

    .line 1079
    .local v1, "sample":I
    :goto_0
    return v1

    .line 1073
    .end local v1    # "sample":I
    :cond_0
    const/4 v1, -0x1

    .line 1075
    .restart local v1    # "sample":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getSamplingRate()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1076
    :catch_0
    move-exception v0

    .line 1077
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getShuffle()I
    .locals 3

    .prologue
    .line 1188
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1189
    const/4 v1, 0x0

    .line 1198
    .local v1, "shuffle":I
    :goto_0
    return v1

    .line 1192
    .end local v1    # "shuffle":I
    :cond_0
    const/4 v1, 0x0

    .line 1194
    .restart local v1    # "shuffle":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getShuffle()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1195
    :catch_0
    move-exception v0

    .line 1196
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSoundAlive()I
    .locals 3

    .prologue
    .line 291
    const/4 v1, 0x0

    .line 292
    .local v1, "eq":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 300
    :goto_0
    return v1

    .line 296
    :cond_0
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getSoundAlive()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSoundAliveUserEQ()[I
    .locals 3

    .prologue
    .line 304
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 305
    const/4 v1, 0x0

    .line 313
    .local v1, "eq":[I
    :goto_0
    return-object v1

    .line 307
    .end local v1    # "eq":[I
    :cond_0
    const/4 v1, 0x0

    .line 309
    .restart local v1    # "eq":[I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getSoundAliveUserEQ()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSoundAliveUserExt()[I
    .locals 3

    .prologue
    .line 317
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 318
    const/4 v1, 0x0

    .line 326
    .local v1, "extended":[I
    :goto_0
    return-object v1

    .line 320
    .end local v1    # "extended":[I
    :cond_0
    const/4 v1, 0x0

    .line 322
    .restart local v1    # "extended":[I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getSoundAliveUserExt()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1083
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 1084
    const/4 v1, 0x0

    .line 1092
    .local v1, "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1086
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 1088
    .restart local v1    # "name":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->getTitleName()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1089
    :catch_0
    move-exception v0

    .line 1090
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isActiveSmartVolume()Z
    .locals 3

    .prologue
    .line 1382
    const/4 v1, 0x0

    .line 1383
    .local v1, "isActiveSmartVolume":Z
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 1385
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->isActiveSmartVolume()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1390
    :cond_0
    :goto_0
    return v1

    .line 1386
    :catch_0
    move-exception v0

    .line 1387
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isAudioShockWarningEnabled()Z
    .locals 2

    .prologue
    .line 1449
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 1451
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isAudioShockWarningEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1456
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 1452
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 1453
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1456
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isBtConnected()Z
    .locals 2

    .prologue
    .line 741
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 743
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isBtConnected()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 748
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 744
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 745
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 748
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isConnectingWfd()Z
    .locals 2

    .prologue
    .line 730
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 732
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isConnectingWfd()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 737
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 733
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 734
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 737
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDlnaDmsList()Z
    .locals 3

    .prologue
    .line 688
    const/4 v0, 0x0

    .line 689
    .local v0, "isDMSList":Z
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v1

    const v2, 0x2000b

    if-ne v1, v2, :cond_0

    .line 690
    const/4 v0, 0x1

    .line 692
    :cond_0
    return v0
.end method

.method public static isDmrPlaying()Z
    .locals 1

    .prologue
    .line 679
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEnableToPlay()Z
    .locals 2

    .prologue
    .line 1438
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 1440
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isEnableToPlaying()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1445
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 1441
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 1442
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1445
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLocalTrack()Z
    .locals 2

    .prologue
    .line 696
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 698
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isLocalTrack()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 704
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 699
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 700
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 704
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isPlaying()Z
    .locals 2

    .prologue
    .line 708
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 710
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isPlaying()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 715
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 711
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 712
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 715
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isPlayingAudioId(J)Z
    .locals 6
    .param p0, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 765
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v3, :cond_0

    .line 774
    :goto_0
    return v2

    .line 768
    :cond_0
    const/4 v1, 0x0

    .line 770
    .local v1, "isSame":Z
    :try_start_0
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v3}, Lcom/samsung/musicplus/service/IPlayerService;->getAudioId()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    cmp-long v3, v4, p0

    if-nez v3, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v2, v1

    .line 774
    goto :goto_0

    :cond_1
    move v1, v2

    .line 770
    goto :goto_1

    .line 771
    :catch_0
    move-exception v0

    .line 772
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public static isPreparing()Z
    .locals 2

    .prologue
    .line 719
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 721
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isPreparing()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 726
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 722
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 723
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 726
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSameSong(IJ)Z
    .locals 7
    .param p0, "list"    # I
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 752
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v3, :cond_0

    .line 761
    :goto_0
    return v2

    .line 755
    :cond_0
    const/4 v1, 0x0

    .line 757
    .local v1, "isSame":Z
    :try_start_0
    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v3}, Lcom/samsung/musicplus/service/IPlayerService;->getAudioId()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_1

    sget-object v3, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v3}, Lcom/samsung/musicplus/service/IPlayerService;->getListType()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, p0, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v2, v1

    .line 761
    goto :goto_0

    :cond_1
    move v1, v2

    .line 757
    goto :goto_1

    .line 758
    :catch_0
    move-exception v0

    .line 759
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public static isSupportPlaySpeed()Z
    .locals 3

    .prologue
    .line 536
    const/4 v1, 0x0

    .line 537
    .local v1, "isSupport":Z
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 539
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->isSupportPlaySpeed()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 544
    :cond_0
    :goto_0
    return v1

    .line 540
    :catch_0
    move-exception v0

    .line 541
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static killMusicProcess()V
    .locals 2

    .prologue
    .line 1314
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1323
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1317
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->quit()V

    .line 1319
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->stopMusicPackage()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1320
    :catch_0
    move-exception v0

    .line 1321
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static loadNewSoundAliveAuto()Z
    .locals 3

    .prologue
    .line 499
    const/4 v1, 0x0

    .line 500
    .local v1, "isAuto":Z
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 502
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->loadNewSoundAliveAuto()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 507
    :cond_0
    :goto_0
    return v1

    .line 503
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static loadNewSoundAliveBandLevel()[I
    .locals 3

    .prologue
    .line 475
    const/4 v1, 0x0

    .line 476
    .local v1, "level":[I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 478
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->loadNewSoundAliveBandLevel()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 483
    :cond_0
    :goto_0
    return-object v1

    .line 479
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static loadNewSoundAliveSquarePosition()[I
    .locals 3

    .prologue
    .line 462
    const/4 v1, 0x0

    .line 463
    .local v1, "position":[I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 471
    :goto_0
    return-object v1

    .line 467
    :cond_0
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->loadNewSoundAliveSquarePosition()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 468
    :catch_0
    move-exception v0

    .line 469
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static loadNewSoundAliveStrength()[I
    .locals 3

    .prologue
    .line 487
    const/4 v1, 0x0

    .line 488
    .local v1, "strength":[I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 490
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->loadNewSoundAliveStrength()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 495
    :cond_0
    :goto_0
    return-object v1

    .line 491
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static loadNewSoundAliveUsePreset()I
    .locals 3

    .prologue
    .line 449
    const/4 v1, 0x0

    .line 450
    .local v1, "preset":I
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 458
    :goto_0
    return v1

    .line 454
    :cond_0
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->loadNewSoundAliveUsePreset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static openList(Landroid/content/Context;ILjava/lang/String;[JI)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 156
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-eqz v1, :cond_2

    .line 160
    invoke-static {p0, p3, p4}, Lcom/samsung/musicplus/util/ServiceUtils;->checkLgtDrm(Landroid/content/Context;[JI)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    :cond_2
    new-instance v0, Lcom/samsung/musicplus/util/ServiceUtils$1;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/util/ServiceUtils$1;-><init>(ILjava/lang/String;[JI)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/util/ServiceUtils$1;->start()V

    .line 179
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static play()V
    .locals 2

    .prologue
    .line 793
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_1

    .line 803
    .local v0, "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 797
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 798
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->play()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 800
    :catch_0
    move-exception v0

    .line 801
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static playNext()V
    .locals 1

    .prologue
    .line 806
    sget-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v0, :cond_0

    .line 824
    :goto_0
    return-void

    .line 810
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/util/ServiceUtils$3;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ServiceUtils$3;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/musicplus/util/ServiceUtils$3;->start()V

    goto :goto_0
.end method

.method public static playPrev(Z)V
    .locals 1
    .param p0, "force"    # Z

    .prologue
    .line 827
    sget-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v0, :cond_0

    .line 845
    :goto_0
    return-void

    .line 831
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/util/ServiceUtils$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/util/ServiceUtils$4;-><init>(Z)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/util/ServiceUtils$4;->start()V

    goto :goto_0
.end method

.method public static refreshDlna()V
    .locals 2

    .prologue
    .line 1226
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1234
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1230
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->refreshDlna()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1231
    :catch_0
    move-exception v0

    .line 1232
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static removeTrack(J)I
    .locals 4
    .param p0, "id"    # J

    .prologue
    .line 883
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 884
    const/4 v1, 0x0

    .line 892
    :goto_0
    return v1

    .line 886
    :cond_0
    const/4 v1, 0x0

    .line 888
    .local v1, "numremoved":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->removeTrack(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 889
    :catch_0
    move-exception v0

    .line 890
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static removeTracks(II)I
    .locals 3
    .param p0, "first"    # I
    .param p1, "last"    # I

    .prologue
    .line 909
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 910
    const/4 v1, 0x0

    .line 918
    :goto_0
    return v1

    .line 912
    :cond_0
    const/4 v1, 0x0

    .line 914
    .local v1, "numremoved":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->removeTracks(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 915
    :catch_0
    move-exception v0

    .line 916
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static removeTracks([J)I
    .locals 3
    .param p0, "ids"    # [J

    .prologue
    .line 896
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_0

    .line 897
    const/4 v1, 0x0

    .line 905
    :goto_0
    return v1

    .line 899
    :cond_0
    const/4 v1, 0x0

    .line 901
    .local v1, "numremoved":I
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2, p0}, Lcom/samsung/musicplus/service/IPlayerService;->removeTracksAudioIds([J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 902
    :catch_0
    move-exception v0

    .line 903
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static reorderQueue(ILjava/lang/String;[JI)V
    .locals 2
    .param p0, "listType"    # I
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "list"    # [J
    .param p3, "position"    # I

    .prologue
    .line 871
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 879
    :goto_0
    return-void

    .line 875
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1, p2, p3}, Lcom/samsung/musicplus/service/IPlayerService;->reorderQueue(ILjava/lang/String;[JI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 876
    :catch_0
    move-exception v0

    .line 877
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static savePreferencesBoolean(Ljava/lang/String;Z)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Z

    .prologue
    .line 1348
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1356
    :goto_0
    return-void

    .line 1352
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->savePreferencesBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1353
    :catch_0
    move-exception v0

    .line 1354
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static savePreferencesFloat(Ljava/lang/String;F)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # F

    .prologue
    .line 1337
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1345
    :goto_0
    return-void

    .line 1341
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->savePreferencesFloat(Ljava/lang/String;F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1342
    :catch_0
    move-exception v0

    .line 1343
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static savePreferencesInt(Ljava/lang/String;I)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 1326
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1334
    :goto_0
    return-void

    .line 1330
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->savePreferencesInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1331
    :catch_0
    move-exception v0

    .line 1332
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1359
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1367
    :goto_0
    return-void

    .line 1363
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1364
    :catch_0
    move-exception v0

    .line 1365
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static seek(J)J
    .locals 2
    .param p0, "position"    # J

    .prologue
    .line 848
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 849
    const-wide/16 p0, 0x0

    .line 856
    :goto_0
    return-wide p0

    .line 852
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->seek(J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p0

    goto :goto_0

    .line 853
    :catch_0
    move-exception v0

    .line 854
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static selectDlnaDms(Ljava/lang/String;)V
    .locals 2
    .param p0, "dmsId"    # Ljava/lang/String;

    .prologue
    .line 1237
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1245
    :goto_0
    return-void

    .line 1241
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->selectDlnaDms(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1242
    :catch_0
    move-exception v0

    .line 1243
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setAdaptSound(Z)V
    .locals 2
    .param p0, "isOn"    # Z

    .prologue
    .line 570
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 578
    :goto_0
    return-void

    .line 574
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setAdaptSound(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setDlnaDmrMute()V
    .locals 2

    .prologue
    .line 1281
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1289
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1285
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->setDlnaDmrMute()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1286
    :catch_0
    move-exception v0

    .line 1287
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setK2HD(Z)V
    .locals 2
    .param p0, "isOn"    # Z

    .prologue
    .line 559
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 567
    :goto_0
    return-void

    .line 563
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setK2HD(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 564
    :catch_0
    move-exception v0

    .line 565
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setLimitVolume(FF)V
    .locals 2
    .param p0, "l"    # F
    .param p1, "r"    # F

    .prologue
    .line 1428
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 1430
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->setLimitVolume(FF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1435
    :cond_0
    :goto_0
    return-void

    .line 1431
    :catch_0
    move-exception v0

    .line 1432
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setNewSoundAliveAuto(Z)V
    .locals 2
    .param p0, "auto"    # Z

    .prologue
    .line 330
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 338
    :goto_0
    return-void

    .line 334
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setNewSoundAliveAuto(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setNewSoundAliveBandLevel([I)V
    .locals 2
    .param p0, "band"    # [I

    .prologue
    .line 363
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 371
    :goto_0
    return-void

    .line 367
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setNewSoundAliveBandLevel([I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setNewSoundAliveEachStrength(II)V
    .locals 2
    .param p0, "effect"    # I
    .param p1, "mode"    # I

    .prologue
    .line 385
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 393
    :goto_0
    return-void

    .line 389
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->setNewSoundAliveEachStrength(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setNewSoundAliveSquarePosition([I)V
    .locals 2
    .param p0, "position"    # [I

    .prologue
    .line 352
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 360
    :goto_0
    return-void

    .line 356
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setNewSoundAliveSquarePosition([I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setNewSoundAliveStrength([I)V
    .locals 2
    .param p0, "mode"    # [I

    .prologue
    .line 374
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 382
    :goto_0
    return-void

    .line 378
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setNewSoundAliveStrength([I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setNewSoundAliveUsePreset(I)V
    .locals 2
    .param p0, "effect"    # I

    .prologue
    .line 341
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 349
    :goto_0
    return-void

    .line 345
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setNewSoundAliveUsePreset(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 346
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setPlaySpeed(F)V
    .locals 2
    .param p0, "playSpeed"    # F

    .prologue
    .line 511
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 521
    :goto_0
    return-void

    .line 515
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setPlaySpeed(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 516
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 518
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 519
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setQueuePosition(IZ)V
    .locals 2
    .param p0, "index"    # I
    .param p1, "play"    # Z

    .prologue
    .line 244
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->setQueuePosition(IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSlinkWakeLock()V
    .locals 2

    .prologue
    .line 1418
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 1420
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->setSlinkWakeLock()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1425
    .local v0, "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 1421
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 1422
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSmartVolume(Z)V
    .locals 2
    .param p0, "isOn"    # Z

    .prologue
    .line 548
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 556
    :goto_0
    return-void

    .line 552
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setSmartVolume(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 553
    :catch_0
    move-exception v0

    .line 554
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSoundAlive(I)V
    .locals 2
    .param p0, "effect"    # I

    .prologue
    .line 255
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 263
    :goto_0
    return-void

    .line 259
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0}, Lcom/samsung/musicplus/service/IPlayerService;->setSoundAlive(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSoundAliveUser([I[I)V
    .locals 2
    .param p0, "eq"    # [I
    .param p1, "effect"    # [I

    .prologue
    .line 280
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 288
    :goto_0
    return-void

    .line 284
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p0, p1}, Lcom/samsung/musicplus/service/IPlayerService;->setSoundAliveUser([I[I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static togglePlay()V
    .locals 2

    .prologue
    .line 778
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 790
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 782
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 783
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->pause()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 787
    :catch_0
    move-exception v0

    .line 788
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 785
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->play()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static toggleRepeat()V
    .locals 2

    .prologue
    .line 1202
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1211
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1207
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->toggleRepeat()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1208
    :catch_0
    move-exception v0

    .line 1209
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static toggleShuffle()V
    .locals 2

    .prologue
    .line 1214
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_0

    .line 1223
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 1219
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->toggleShuffle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1220
    :catch_0
    move-exception v0

    .line 1221
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static unbindFromService(Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;)V
    .locals 4
    .param p0, "token"    # Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    .prologue
    .line 92
    if-nez p0, :cond_1

    .line 93
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v3, " Trying to unbind with null token"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;->mWrappedContext:Landroid/content/ContextWrapper;

    .line 97
    .local v0, "cw":Landroid/content/ContextWrapper;
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;

    .line 98
    .local v1, "sb":Lcom/samsung/musicplus/util/ServiceUtils$ServiceBinder;
    if-nez v1, :cond_2

    .line 99
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v3, " Trying to unbind for unknown Context"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    .line 103
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    .line 107
    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->CLASSNAME:Ljava/lang/String;

    const-string v3, " unbindFromService sService = null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
