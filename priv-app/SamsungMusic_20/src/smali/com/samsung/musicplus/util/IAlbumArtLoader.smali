.class public interface abstract Lcom/samsung/musicplus/util/IAlbumArtLoader;
.super Ljava/lang/Object;
.source "IAlbumArtLoader.java"


# static fields
.field public static final DECODE_COMPELETE:I = 0xc8


# virtual methods
.method public abstract loadArtistGroupArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;[Landroid/widget/ImageView;II)V
.end method

.method public abstract loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V
.end method

.method public abstract loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;ILandroid/os/Handler;)V
.end method

.method public abstract loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
.end method

.method public abstract loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V
.end method

.method public abstract loadArtworkWithoutAnimation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
.end method

.method public abstract loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
.end method

.method public abstract loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V
.end method

.method public abstract quit()V
.end method
