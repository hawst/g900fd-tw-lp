.class public Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GenreTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "audioId"    # J

    .prologue
    .line 1140
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(J)V

    .line 1141
    const-string v0, "title != \'\' AND is_music=1 AND _id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1142
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1134
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 1135
    const-string v0, "title != \'\' AND is_music=1 AND genre_name=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$GenreTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1137
    return-void
.end method
