.class public Lcom/samsung/musicplus/util/FileOperationTask;
.super Landroid/os/AsyncTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$AddToFavoriteTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;,
        Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final MAX_PLAYLIST_COUNT:I = 0x64

.field private static final MAX_PLAYLIST_ITEM:I = 0x3e8

.field private static final REORDER_NAME:Ljava/lang/String; = "Reorder playlist"

.field public static final REORDER_NAME_FOR_SEARCH:Ljava/lang/String; = "\'Reorder playlist\'"

.field private static sContentValuesCache:[Landroid/content/ContentValues;

.field public static sReorderPlayListId:J


# instance fields
.field protected final NO_NEED_TOAST:I

.field protected mActivity:Landroid/app/Activity;

.field protected mContext:Landroid/content/Context;

.field protected mFinish:Z

.field protected mIsSupportDelayedProgress:Z

.field protected mIsSupportProgress:Z

.field protected mLoadingProgress:Landroid/app/ProgressDialog;

.field protected mMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const-class v0, Lcom/samsung/musicplus/util/FileOperationTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/FileOperationTask;->CLASSNAME:Ljava/lang/String;

    .line 860
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    .line 958
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/samsung/musicplus/util/FileOperationTask;->sReorderPlayListId:J

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 2
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "finish"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mFinish:Z

    .line 66
    iput v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->NO_NEED_TOAST:I

    .line 70
    iput-boolean v1, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mIsSupportProgress:Z

    .line 72
    iput-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mIsSupportDelayedProgress:Z

    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mActivity:Landroid/app/Activity;

    .line 86
    iput-boolean p2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mFinish:Z

    .line 87
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mContext:Landroid/content/Context;

    .line 88
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)I
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/samsung/musicplus/util/FileOperationTask;->getPlaylistCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Landroid/content/Context;Ljava/util/ArrayList;ZZ)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/musicplus/util/FileOperationTask;->setFavorites(Landroid/content/Context;Ljava/util/ArrayList;ZZ)V

    return-void
.end method

.method static synthetic access$200(Landroid/content/Context;[J[JZ)I
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # [J
    .param p2, "x2"    # [J
    .param p3, "x3"    # Z

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/musicplus/util/FileOperationTask;->setSmartFavorites(Landroid/content/Context;[J[JZ)I

    move-result v0

    return v0
.end method

.method public static addToPlaylistInternal(Landroid/content/Context;[JJ)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ids"    # [J
    .param p2, "playlistid"    # J

    .prologue
    .line 629
    sget-object v6, Lcom/samsung/musicplus/util/FileOperationTask;->CLASSNAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addToPlaylistInternal play list id : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    const/4 v2, 0x0

    .line 631
    .local v2, "numinserted":I
    if-nez p1, :cond_1

    .line 634
    const-string v6, "MusicBase"

    const-string v7, "ListSelection null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_0
    return v2

    .line 636
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 637
    .local v3, "resolver":Landroid/content/ContentResolver;
    invoke-static {p2, p3}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v5

    .line 638
    .local v5, "uri":Landroid/net/Uri;
    array-length v4, p1

    .line 639
    .local v4, "size":I
    invoke-static {p0, p2, p3}, Lcom/samsung/musicplus/util/FileOperationTask;->getPlaylistSongCount(Landroid/content/Context;J)I

    move-result v0

    .line 640
    .local v0, "base":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 641
    const/16 v6, 0x3e8

    invoke-static {p1, v1, v6, v0}, Lcom/samsung/musicplus/util/FileOperationTask;->makeInsertItems([JIII)V

    .line 642
    sget-object v6, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v6

    add-int/2addr v2, v6

    .line 640
    add-int/lit16 v1, v1, 0x3e8

    goto :goto_0
.end method

.method private clearPlaylist(Landroid/content/Context;J)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "plid"    # J

    .prologue
    const/4 v4, 0x0

    .line 727
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearPlaylist play list id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    invoke-static {p2, p3}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 729
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 730
    return-void
.end method

.method private doResult()V
    .locals 5

    .prologue
    .line 141
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mMessage:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mMessage:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 144
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 145
    .local v1, "v":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 146
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 148
    :cond_0
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    .end local v0    # "toast":Landroid/widget/Toast;
    .end local v1    # "v":Landroid/widget/TextView;
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mIsSupportProgress:Z

    if-eqz v2, :cond_2

    .line 151
    iget-boolean v2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mIsSupportDelayedProgress:Z

    if-eqz v2, :cond_4

    .line 152
    invoke-direct {p0}, Lcom/samsung/musicplus/util/FileOperationTask;->hideLoadingDelayed()V

    .line 157
    :cond_2
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mFinish:Z

    if-eqz v2, :cond_3

    .line 158
    iget-object v2, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 160
    :cond_3
    return-void

    .line 154
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/util/FileOperationTask;->hideLoading()V

    goto :goto_0
.end method

.method private static getCount(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 842
    const/4 v7, 0x0

    .line 843
    .local v7, "count":I
    const/4 v6, 0x0

    .line 845
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "count(*)"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 849
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 850
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 853
    :cond_0
    if-eqz v6, :cond_1

    .line 854
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 857
    :cond_1
    return v7

    .line 853
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 854
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private static getPlaylistCount(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 825
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/FileOperationTask;->getCount(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method protected static getPlaylistSongCount(Landroid/content/Context;J)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistid"    # J

    .prologue
    .line 831
    invoke-static {p1, p2}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/FileOperationTask;->getCount(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method private hideLoadingDelayed()V
    .locals 4

    .prologue
    .line 128
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/musicplus/util/FileOperationTask$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/util/FileOperationTask$1;-><init>(Lcom/samsung/musicplus/util/FileOperationTask;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 135
    return-void
.end method

.method private static makeInsertItems([JIII)V
    .locals 6
    .param p0, "ids"    # [J
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "base"    # I

    .prologue
    .line 871
    add-int v1, p1, p2

    array-length v2, p0

    if-le v1, v2, :cond_0

    .line 872
    array-length v1, p0

    sub-int p2, v1, p1

    .line 876
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    array-length v1, v1

    if-eq v1, p2, :cond_2

    .line 877
    :cond_1
    new-array v1, p2, [Landroid/content/ContentValues;

    sput-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    .line 880
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_4

    .line 881
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    aget-object v1, v1, v0

    if-nez v1, :cond_3

    .line 882
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    aput-object v2, v1, v0

    .line 885
    :cond_3
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    aget-object v1, v1, v0

    const-string v2, "play_order"

    add-int v3, p3, p1

    add-int/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 887
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->sContentValuesCache:[Landroid/content/ContentValues;

    aget-object v1, v1, v0

    const-string v2, "audio_id"

    add-int v3, p1, v0

    aget-wide v4, p0, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 880
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 890
    :cond_4
    return-void
.end method

.method public static moveItem(Landroid/content/Context;Landroid/content/ContentResolver;JII)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "res"    # Landroid/content/ContentResolver;
    .param p2, "playlistId"    # J
    .param p4, "from"    # I
    .param p5, "to"    # I

    .prologue
    .line 1002
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-wide v2, Lcom/samsung/musicplus/util/FileOperationTask;->sReorderPlayListId:J

    invoke-static {v0, v2, v3, p4, p5}, Landroid/provider/MediaStore$Audio$Playlists$Members;->moveItem(Landroid/content/ContentResolver;JII)Z

    .line 1005
    return-void
.end method

.method private removeTracksFromService([J)V
    .locals 4
    .param p1, "audioIds"    # [J

    .prologue
    .line 898
    array-length v1, p1

    .line 899
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 900
    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/ServiceUtils;->removeTrack(J)I

    .line 899
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 902
    :cond_0
    return-void
.end method

.method private static setFavorite(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "favorite"    # Z
    .param p3, "showToast"    # Z

    .prologue
    .line 1033
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1034
    .local v0, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1035
    invoke-static {p0, v0, p2, p3}, Lcom/samsung/musicplus/util/FileOperationTask;->setFavorites(Landroid/content/Context;Ljava/util/ArrayList;ZZ)V

    .line 1036
    return-void
.end method

.method private static setFavorites(Landroid/content/Context;Ljava/util/ArrayList;ZZ)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "favorite"    # Z
    .param p3, "showToast"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1042
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    move-result-wide v0

    .line 1044
    .local v0, "favoritePlid":J
    if-nez p1, :cond_1

    .line 1047
    const-string v7, "MusicBase"

    const-string v8, "ListSelection null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    :cond_0
    :goto_0
    return-void

    .line 1048
    :cond_1
    const-wide/16 v8, -0x1

    cmp-long v7, v0, v8

    if-eqz v7, :cond_0

    .line 1049
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v3, v7, [J

    .line 1050
    .local v3, "idss":[J
    if-eqz p2, :cond_3

    .line 1051
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 1052
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-long v8, v7

    aput-wide v8, v3, v2

    .line 1051
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1054
    :cond_2
    invoke-static {p0, v3, v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask;->addToPlaylistInternal(Landroid/content/Context;[JJ)I

    .line 1062
    .end local v2    # "i":I
    :goto_2
    if-eqz p3, :cond_0

    .line 1064
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1065
    .local v6, "size":I
    if-eqz p2, :cond_4

    .line 1066
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f000f

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1072
    .local v4, "msg":Ljava/lang/String;
    :goto_3
    invoke-static {p0, v4, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1056
    .end local v4    # "msg":Ljava/lang/String;
    .end local v6    # "size":I
    :cond_3
    const-string v7, "audio_id"

    invoke-static {p1, v7}, Lcom/samsung/musicplus/util/UiUtils;->formatSelectionForIds(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1058
    .local v5, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v5, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    .line 1069
    .end local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "size":I
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f000c

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "msg":Ljava/lang/String;
    goto :goto_3
.end method

.method private static setSmartFavorites(Landroid/content/Context;[J[JZ)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "itmeIds"    # [J
    .param p2, "favoriteSelectionIds"    # [J
    .param p3, "isRemovable"    # Z

    .prologue
    .line 1079
    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    .line 1080
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1081
    .local v1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    array-length v2, p1

    .line 1082
    .local v2, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1083
    aget-wide v4, p1, v0

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1086
    :cond_0
    const-string v4, "audio_id"

    invoke-static {v1, v4}, Lcom/samsung/musicplus/util/UiUtils;->formatSelectionForIds(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1088
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1092
    .end local v0    # "i":I
    .end local v1    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v2    # "length":I
    .end local v3    # "selection":Ljava/lang/String;
    :goto_1
    return v4

    :cond_1
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {p0, p2, v4, v5}, Lcom/samsung/musicplus/util/FileOperationTask;->addToPlaylistInternal(Landroid/content/Context;[JJ)I

    move-result v4

    goto :goto_1
.end method

.method public static toggleFavorites(Landroid/content/Context;Ljava/lang/String;JZZ)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "audioId"    # J
    .param p4, "isAddedFavorite"    # Z
    .param p5, "showToast"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1018
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1019
    .local v0, "id":Ljava/lang/String;
    if-nez p4, :cond_0

    move v1, v2

    :goto_0
    invoke-static {p0, v0, v1, p5}, Lcom/samsung/musicplus/util/FileOperationTask;->setFavorite(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 1020
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.musicplus.favouritechanged"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1021
    if-nez p4, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v3

    .line 1019
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1021
    goto :goto_1
.end method


# virtual methods
.method protected deletePlaylists(Landroid/content/Context;[J)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # [J

    .prologue
    .line 709
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 710
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_2

    .line 712
    aget-wide v2, p2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 710
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 715
    :cond_1
    aget-wide v2, p2, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 716
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 717
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 720
    :cond_2
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 721
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id IN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 723
    array-length v2, p2

    return v2
.end method

.method protected deleteTracks(Landroid/content/Context;[J)I
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # [J

    .prologue
    .line 905
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v3

    const/4 v3, 0x1

    const-string v6, "_data"

    aput-object v6, v4, v3

    const/4 v3, 0x2

    const-string v6, "album_id"

    aput-object v6, v4, v3

    .line 910
    .local v4, "cols":[Ljava/lang/String;
    const-string v3, "_id"

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v5

    .line 911
    .local v5, "where":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 912
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 914
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v10, :cond_3

    .line 915
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v13

    .line 916
    .local v13, "listType":I
    invoke-static {v13}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v3

    const v6, 0x2000b

    if-eq v3, v6, :cond_0

    .line 917
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 918
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 920
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 921
    .local v14, "id":J
    invoke-static {v14, v15}, Lcom/samsung/musicplus/util/ServiceUtils;->removeTrack(J)I

    .line 924
    const/4 v3, 0x2

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 925
    .local v8, "artIndex":J
    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/AlbumArtUtils;->removeCache(J)V

    .line 926
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 930
    .end local v8    # "artIndex":J
    .end local v14    # "id":J
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v6, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 934
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 935
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_2

    .line 936
    const/4 v3, 0x1

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 937
    .local v16, "name":Ljava/lang/String;
    if-eqz v16, :cond_1

    .line 938
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 940
    .local v12, "f":Ljava/io/File;
    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 944
    sget-object v3, Lcom/samsung/musicplus/util/FileOperationTask;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to delete file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 950
    .end local v12    # "f":Ljava/io/File;
    :cond_1
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 946
    .restart local v12    # "f":Ljava/io/File;
    :catch_0
    move-exception v11

    .line 947
    .local v11, "ex":Ljava/lang/SecurityException;
    invoke-virtual {v11}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_2

    .line 952
    .end local v11    # "ex":Ljava/lang/SecurityException;
    .end local v12    # "f":Ljava/io/File;
    .end local v16    # "name":Ljava/lang/String;
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 954
    .end local v13    # "listType":I
    :cond_3
    move-object/from16 v0, p2

    array-length v3, v0

    return v3
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 54
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public hideLoading()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 123
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 125
    :cond_1
    return-void
.end method

.method protected makeReorderPlayListItem(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 967
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 968
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "count(*)"

    aput-object v3, v2, v8

    const-string v3, "name=\'Reorder playlist\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 971
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 972
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTabChanged & Playlist but there are no quick list. So make it. the number of Quick list : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7, v9}, Landroid/content/ContentValues;-><init>(I)V

    .line 978
    .local v7, "value":Landroid/content/ContentValues;
    const-string v1, "name"

    const-string v2, "Reorder playlist"

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 981
    .end local v7    # "value":Landroid/content/ContentValues;
    :cond_0
    if-eqz v6, :cond_1

    .line 982
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 984
    :cond_1
    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    const-string v3, "name=\'Reorder playlist\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 987
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 988
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    sput-wide v2, Lcom/samsung/musicplus/util/FileOperationTask;->sReorderPlayListId:J

    .line 989
    sget-object v1, Lcom/samsung/musicplus/util/FileOperationTask;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   sReorderPlayListId List\'s playlist ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v4, Lcom/samsung/musicplus/util/FileOperationTask;->sReorderPlayListId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 993
    sget-wide v2, Lcom/samsung/musicplus/util/FileOperationTask;->sReorderPlayListId:J

    invoke-direct {p0, p1, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask;->clearPlaylist(Landroid/content/Context;J)V

    .line 994
    return-void
.end method

.method protected final movePlayList(Landroid/content/Context;JJ)[J
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "from"    # J
    .param p4, "to"    # J

    .prologue
    .line 649
    const-wide/16 v4, -0x1

    cmp-long v4, p2, v4

    if-nez v4, :cond_0

    .line 651
    const/16 v20, 0x0

    .line 705
    :goto_0
    return-object v20

    .line 655
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 660
    .local v11, "audioCursor":Landroid/database/Cursor;
    if-nez v11, :cond_1

    .line 661
    const/16 v20, 0x0

    goto :goto_0

    .line 664
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 665
    .local v17, "numTotalAudio":I
    move/from16 v0, v17

    new-array v10, v0, [J

    .line 666
    .local v10, "arrTotalAudio":[J
    const/4 v14, 0x0

    .line 667
    .local v14, "cur":I
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 668
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "cur":I
    .local v15, "cur":I
    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v10, v14

    move v14, v15

    .end local v15    # "cur":I
    .restart local v14    # "cur":I
    goto :goto_1

    .line 670
    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 673
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static/range {p2 .. p3}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "audio_id"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "play_order"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 678
    .local v18, "playlistCursor":Landroid/database/Cursor;
    if-nez v18, :cond_3

    .line 679
    const/16 v20, 0x0

    goto :goto_0

    .line 682
    :cond_3
    const/4 v14, 0x0

    .line 683
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v21, v0

    .line 685
    .local v21, "tempList":[J
    :cond_4
    :goto_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 686
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 687
    .local v12, "audioID":J
    invoke-static {v10, v12, v13}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v19

    .line 688
    .local v19, "result":I
    if-ltz v19, :cond_4

    .line 690
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "cur":I
    .restart local v15    # "cur":I
    aput-wide v12, v21, v14

    move v14, v15

    .end local v15    # "cur":I
    .restart local v14    # "cur":I
    goto :goto_2

    .line 693
    .end local v12    # "audioID":J
    .end local v19    # "result":I
    :cond_5
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 696
    new-array v0, v14, [J

    move-object/from16 v20, v0

    .line 697
    .local v20, "resultList":[J
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_3
    move/from16 v0, v16

    if-ge v0, v14, :cond_6

    .line 698
    aget-wide v4, v21, v16

    aput-wide v4, v20, v16

    .line 697
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 702
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask;->clearPlaylist(Landroid/content/Context;J)V

    .line 703
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-wide/from16 v2, p4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask;->addToPlaylistInternal(Landroid/content/Context;[JJ)I

    goto/16 :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/samsung/musicplus/util/FileOperationTask;->doResult()V

    .line 106
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 54
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/util/FileOperationTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mIsSupportProgress:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mActivity:Landroid/app/Activity;

    const v1, 0x7f1000a1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask;->showLoading(Landroid/app/Activity;I)V

    .line 95
    :cond_0
    return-void
.end method

.method protected removePlayListItem(Landroid/content/Context;J[J[J)I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "plid"    # J
    .param p4, "audioIds"    # [J
    .param p5, "positions"    # [J

    .prologue
    .line 745
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "("

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 746
    .local v3, "sb":Ljava/lang/StringBuilder;
    array-length v2, p4

    .line 747
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 748
    aget-wide v6, p4, v1

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 749
    array-length v6, p4

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_0

    .line 750
    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 753
    :cond_1
    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 754
    const-wide/16 v6, -0xb

    cmp-long v6, p2, v6

    if-nez v6, :cond_2

    .line 760
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id IN "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 821
    :goto_1
    array-length v6, p4

    return v6

    .line 764
    :cond_2
    const-wide/16 v6, -0xc

    cmp-long v6, p2, v6

    if-nez v6, :cond_3

    .line 769
    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    .line 770
    .local v4, "uri":Landroid/net/Uri;
    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 771
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "most_played"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 772
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id IN "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 774
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    .line 776
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_3
    const-wide/16 v6, -0xd

    cmp-long v6, p2, v6

    if-nez v6, :cond_4

    .line 781
    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    .line 782
    .restart local v4    # "uri":Landroid/net/Uri;
    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 783
    .restart local v5    # "values":Landroid/content/ContentValues;
    const-string v6, "recently_played"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 784
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id IN "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 786
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_1

    .line 788
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_4
    const-wide/16 v6, -0xe

    cmp-long v6, p2, v6

    if-nez v6, :cond_5

    .line 793
    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    .line 794
    .restart local v4    # "uri":Landroid/net/Uri;
    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 795
    .restart local v5    # "values":Landroid/content/ContentValues;
    const-string v6, "recently_added_remove_flag"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 796
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id IN "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 798
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_1

    .line 802
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_5
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v4

    .line 803
    .restart local v4    # "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 804
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id IN "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public showLoading(Landroid/app/Activity;I)V
    .locals 2
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "resId"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 111
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/util/FileOperationTask;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 115
    return-void
.end method
