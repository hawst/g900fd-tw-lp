.class Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;
.super Landroid/os/Handler;
.source "OldAlbumArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/OldAlbumArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UiUpdateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)V
    .locals 1

    .prologue
    .line 482
    iput-object p1, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    .line 483
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 484
    return-void
.end method

.method private attachDecodedAlbum(Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;)V
    .locals 5
    .param p1, "tag"    # Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;

    .prologue
    .line 494
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 495
    .local v0, "key":Ljava/lang/Object;
    if-eqz v0, :cond_0

    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->artKey:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 496
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->d:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_1

    .line 497
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->forceLayout(Landroid/widget/ImageView;)V

    .line 498
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    iget v3, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->defaultAlbumRes:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 515
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$300(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Ljava/util/HashMap;

    move-result-object v3

    monitor-enter v3

    .line 516
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->this$0:Lcom/samsung/musicplus/util/OldAlbumArtLoader;

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->mWorkingMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$300(Lcom/samsung/musicplus/util/OldAlbumArtLoader;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v4, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    return-void

    .line 500
    :cond_1
    iget-boolean v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->showAnimation:Z

    if-eqz v2, :cond_2

    .line 501
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    # getter for: Lcom/samsung/musicplus/util/OldAlbumArtLoader;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;
    invoke-static {}, Lcom/samsung/musicplus/util/OldAlbumArtLoader;->access$600()Landroid/graphics/drawable/ColorDrawable;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->d:Landroid/graphics/drawable/Drawable;

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 505
    .local v1, "td":Landroid/graphics/drawable/TransitionDrawable;
    const/16 v2, 0xc8

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 506
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->forceLayout(Landroid/widget/ImageView;)V

    .line 507
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 509
    .end local v1    # "td":Landroid/graphics/drawable/TransitionDrawable;
    :cond_2
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->forceLayout(Landroid/widget/ImageView;)V

    .line 510
    iget-object v2, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->iv:Landroid/widget/ImageView;

    iget-object v3, p1, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 521
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private forceLayout(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 534
    invoke-virtual {p1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    .line 535
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 488
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateHandler;->attachDecodedAlbum(Lcom/samsung/musicplus/util/OldAlbumArtLoader$UiUpdateTag;)V

    .line 491
    :cond_0
    return-void
.end method
