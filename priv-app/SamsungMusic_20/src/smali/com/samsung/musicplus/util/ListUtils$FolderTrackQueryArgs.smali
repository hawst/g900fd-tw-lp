.class public Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FolderTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1154
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 1155
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->FOLDER_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1156
    const-string v0, "title != \'\' AND is_music=1 AND bucket_id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1160
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1162
    const-string v0, "_display_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 1163
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 1164
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 1165
    return-void
.end method
