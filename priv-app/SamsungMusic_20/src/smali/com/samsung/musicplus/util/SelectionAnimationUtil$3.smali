.class Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SelectionAnimationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/SelectionAnimationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 164
    iget-object v3, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v3}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v1

    .line 165
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 166
    iget-object v3, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v3}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0d004c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 167
    .local v0, "chb":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 168
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 169
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 170
    iget-object v3, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mIsShowMode:Z
    invoke-static {v3}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$100(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 171
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 165
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 176
    .end local v0    # "chb":Landroid/view/View;
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/util/SelectionAnimationUtil$3;->this$0:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    # getter for: Lcom/samsung/musicplus/util/SelectionAnimationUtil;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v3}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->access$000(Lcom/samsung/musicplus/util/SelectionAnimationUtil;)Landroid/widget/AbsListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 177
    return-void
.end method
