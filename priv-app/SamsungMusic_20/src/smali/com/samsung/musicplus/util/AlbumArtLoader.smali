.class public Lcom/samsung/musicplus/util/AlbumArtLoader;
.super Ljava/lang/Object;
.source "AlbumArtLoader.java"

# interfaces
.implements Lcom/samsung/musicplus/util/IAlbumArtLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;,
        Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;,
        Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;,
        Lcom/samsung/musicplus/util/AlbumArtLoader$Holder;
    }
.end annotation


# static fields
.field public static final ACTIVATE_ONE_CASHE:Z = false

.field private static final TAG:Ljava/lang/String; = "MusicAlbumLoader"

.field private static final TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

.field private static final mArtistAlbums:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<[",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 359
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x106000d

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtLoader;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    .line 362
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/util/AlbumArtLoader;->mArtistAlbums:Landroid/support/v4/util/LongSparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # J

    .prologue
    .line 25
    invoke-static {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/util/AlbumArtLoader;->debugBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V

    return-void
.end method

.method static synthetic access$100()Landroid/graphics/drawable/ColorDrawable;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtLoader;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    return-object v0
.end method

.method static synthetic access$200()Landroid/support/v4/util/LongSparseArray;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtLoader;->mArtistAlbums:Landroid/support/v4/util/LongSparseArray;

    return-object v0
.end method

.method private static debugBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V
    .locals 5
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 208
    const-string v0, "MusicAlbumLoader"

    const-string v1, "%s Uri: %s time: %d memsize: %d Config: %s height: %d width: %d"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap$Config;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method public static getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtLoader$Holder;->mInstance:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    return-object v0
.end method


# virtual methods
.method public loadArtistGroupArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;[Landroid/widget/ImageView;II)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/Object;
    .param p4, "iv"    # [Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 187
    const-string v2, "MusicAlbumLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadArtistGroupArtwork uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " artistId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " iv.size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    sget-object v3, Lcom/samsung/musicplus/util/AlbumArtLoader;->mArtistAlbums:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v2, p3

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Long;

    .line 192
    .local v9, "albums":[Ljava/lang/Long;
    if-nez v9, :cond_1

    .line 193
    new-instance v2, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;[Landroid/widget/ImageView;II)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/util/AlbumArtLoader$LoadArtistGroupTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 205
    :cond_0
    return-void

    .line 197
    :cond_1
    array-length v2, v9

    move-object/from16 v0, p4

    array-length v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 198
    .local v11, "length":I
    const-string v2, "MusicAlbumLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Known albums, length: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v11, :cond_0

    .line 200
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v8

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    aget-object v3, v9, v10

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    new-instance v2, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;

    aget-object v4, p4, v10

    const/4 v7, 0x0

    move-object v3, p1

    move-object/from16 v5, p3

    move/from16 v6, p6

    invoke-direct/range {v2 .. v7}, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;IZ)V

    move/from16 v0, p5

    invoke-virtual {v8, v12, v0, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 199
    add-int/lit8 v10, v10, 0x1

    goto :goto_0
.end method

.method public loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # I
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "size"    # I
    .param p5, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 150
    invoke-static {p2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtWorkUri(I)Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "MusicAlbumLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadArtwork handler, listtype + "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " artKey: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    check-cast p3, Ljava/lang/Long;

    .end local p3    # "artKey":Ljava/lang/Object;
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 154
    .local v0, "albuArtUri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;

    invoke-direct {v3, p5}, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v2, v0, p4, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 155
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;ILandroid/os/Handler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "size"    # I
    .param p5, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 170
    const-string v1, "MusicAlbumLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadArtwork handler uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " artKey: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    check-cast p3, Ljava/lang/Long;

    .end local p3    # "artKey":Ljava/lang/Object;
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 172
    .local v0, "albuArtUri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;

    invoke-direct {v2, p5}, Lcom/samsung/musicplus/util/AlbumArtLoader$HandlerPublisher;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v1, v0, p4, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 173
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 74
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/musicplus/util/AlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V

    .line 75
    return-void
.end method

.method public loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I
    .param p7, "wait"    # Z

    .prologue
    .line 80
    const-string v2, "MusicAlbumLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadArtwork uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " artKey: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    if-nez p2, :cond_0

    .line 92
    :goto_0
    return-void

    .line 84
    :cond_0
    const/4 v8, 0x0

    .line 86
    .local v8, "albuArtUri":Landroid/net/Uri;
    :try_start_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v0, p3

    check-cast v0, Ljava/lang/Long;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 90
    :goto_1
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v10

    new-instance v2, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;

    const/4 v7, 0x1

    move-object v3, p1

    move-object v4, p4

    move-object v5, p3

    move/from16 v6, p6

    invoke-direct/range {v2 .. v7}, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;IZ)V

    move/from16 v0, p5

    invoke-virtual {v10, v8, v0, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    goto :goto_0

    .line 87
    :catch_0
    move-exception v9

    .line 88
    .local v9, "e":Ljava/lang/ClassCastException;
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    goto :goto_1
.end method

.method public loadArtworkWithoutAnimation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 129
    const-string v0, "MusicAlbumLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadArtwork no animation uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " artKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {p4, p6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p3

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 133
    .local v6, "albuArtUri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v7

    new-instance v0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;IZ)V

    invoke-virtual {v7, v6, p5, v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 135
    return-void
.end method

.method public loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I

    .prologue
    .line 97
    invoke-virtual/range {p0 .. p6}, Lcom/samsung/musicplus/util/AlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 98
    return-void
.end method

.method public loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "iv"    # Landroid/widget/ImageView;
    .param p5, "size"    # I
    .param p6, "resId"    # I
    .param p7, "wait"    # Z

    .prologue
    .line 113
    invoke-virtual/range {p0 .. p6}, Lcom/samsung/musicplus/util/AlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 114
    return-void
.end method

.method public quit()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method
