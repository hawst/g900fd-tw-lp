.class public Lcom/samsung/musicplus/util/knox/KnoxUtils;
.super Ljava/lang/Object;
.source "KnoxUtils.java"


# static fields
.field private static final KNOX_MOVE_IN_BACKGROUND:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method private static getRealFilePath(Landroid/content/Context;[J)Ljava/util/ArrayList;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[J)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v7, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 56
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v3

    const-string v3, "_id"

    invoke-static {v3, p1}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 59
    if-nez v6, :cond_1

    .line 60
    const-string v0, "MusicKnox"

    const-string v1, "requestMoveToKnox fail cause c is null. This should not happened. Please check input list and provider"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    if-eqz v6, :cond_0

    .line 76
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 79
    :cond_0
    :goto_0
    return-object v7

    .line 65
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 66
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_4

    .line 68
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 69
    .local v8, "srcPath":Ljava/lang/String;
    if-eqz v8, :cond_2

    const-string v0, ""

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 75
    .end local v8    # "srcPath":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 76
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 75
    :cond_4
    if-eqz v6, :cond_0

    .line 76
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static move(Landroid/content/Context;[J)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # [J

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/knox/KnoxUtils;->moveInternal(Landroid/content/Context;[J)V

    .line 41
    return-void
.end method

.method private static moveInternal(Landroid/content/Context;[J)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # [J

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/knox/KnoxUtils;->getRealFilePath(Landroid/content/Context;[J)Ljava/util/ArrayList;

    move-result-object v0

    .line 45
    .local v0, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    move-result-object v1

    .line 46
    .local v1, "m":Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;
    invoke-virtual {v1, p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->makeSession(Landroid/content/Context;)Z

    .line 47
    invoke-virtual {v1, v0, v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->move(Ljava/util/List;Ljava/util/List;)J

    .line 48
    invoke-virtual {v1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->releaseSession()V

    .line 49
    return-void
.end method
