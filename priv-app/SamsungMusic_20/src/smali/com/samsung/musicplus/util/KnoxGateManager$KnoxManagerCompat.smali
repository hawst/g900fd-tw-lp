.class public Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;
.super Ljava/lang/Object;
.source "KnoxGateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/KnoxGateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KnoxManagerCompat"
.end annotation


# static fields
.field private static final sKnoxMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->findKnox10Method()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->sKnoxMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    .prologue
    .line 279
    invoke-static {p0}, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->availableKnoxPersonalMode(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;)Z

    move-result v0

    return v0
.end method

.method private static availableKnoxPersonalMode(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;)Z
    .locals 5
    .param p0, "knoxManager"    # Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    .prologue
    .line 299
    const/4 v0, 0x0

    .line 300
    .local v0, "active":Z
    sget-object v3, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->sKnoxMethod:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_0

    .line 302
    :try_start_0
    const-string v3, "KnoxModeManager"

    const-string v4, "availableKnoxPersonalMode() - invoke"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    sget-object v3, Lcom/samsung/musicplus/util/KnoxGateManager$KnoxManagerCompat;->sKnoxMethod:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :cond_0
    move v1, v0

    .line 312
    .end local v0    # "active":Z
    .local v1, "active":I
    :goto_0
    return v1

    .line 304
    .end local v1    # "active":I
    .restart local v0    # "active":Z
    :catch_0
    move-exception v2

    .line 305
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "KnoxModeManager"

    const-string v4, "availableKnoxPersonalMode() - InvocationTargetException!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 306
    .restart local v1    # "active":I
    goto :goto_0

    .line 307
    .end local v1    # "active":I
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v2

    .line 308
    .local v2, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "KnoxModeManager"

    const-string v4, "availableKnoxPersonalMode() - IllegalAccessException!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 309
    .restart local v1    # "active":I
    goto :goto_0
.end method

.method public static findKnox10Method()Ljava/lang/reflect/Method;
    .locals 4

    .prologue
    .line 289
    :try_start_0
    const-string v2, "KnoxModeManager"

    const-string v3, "findKnox10Method()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const-class v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    .line 291
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;>;"
    const-string v2, "availableKnoxPersonalMode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 294
    :goto_0
    return-object v2

    .line 292
    :catch_0
    move-exception v1

    .line 293
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "KnoxModeManager"

    const-string v3, "findKnox10Method() - NoSuchMethodException!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v2, 0x0

    goto :goto_0
.end method
