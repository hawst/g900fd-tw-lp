.class public Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1212
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->SEARCH_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1214
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->SEARCH_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1216
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1221
    const-string v0, "album COLLATE LOCALIZED ASC, album_id, track"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1223
    const-string v0, "album"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1224
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 1225
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 1226
    const-string v0, "artist"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 1227
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$SearchTrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1228
    return-void
.end method
