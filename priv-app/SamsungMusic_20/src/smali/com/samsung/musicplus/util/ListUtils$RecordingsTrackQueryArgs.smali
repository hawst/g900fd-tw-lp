.class public Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecordingsTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1370
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 1371
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->uri:Landroid/net/Uri;

    .line 1372
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->RECORDINGS_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1374
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 1375
    # getter for: Lcom/samsung/musicplus/util/ListUtils;->RECORDINGS_QUERY_WHERE:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->access$200()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1377
    const-string v0, "_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 1378
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 1379
    const-string v0, "date_modified"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 1380
    const-string v0, "duration"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 1381
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1382
    const-string v0, "is_secretbox"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$RecordingsTrackQueryArgs;->secretBoxCol:Ljava/lang/String;

    .line 1383
    return-void
.end method
