.class public final Lcom/samsung/musicplus/util/player/PlayUtils;
.super Ljava/lang/Object;
.source "PlayUtils.java"

# interfaces
.implements Lcom/samsung/musicplus/service/MediaCommandAction;


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicPlayUtils"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static play(Landroid/content/Context;ILjava/lang/String;[JI)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    .line 71
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JILjava/lang/String;)V

    .line 72
    return-void
.end method

.method public static play(Landroid/content/Context;ILjava/lang/String;[JILjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string v1, "MusicPlayUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play() list type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.action.SERVICE_COMMAND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    const-string v1, "command"

    const-string v2, "openList"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v1, "listType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    const-string v1, "listQueryKey"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string v1, "list"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 86
    const-string v1, "listPosition"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 87
    const-string v1, "dmr_device"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 89
    return-void
.end method

.method public static playAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "genre"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/musicplus/util/player/AlbumPlayUtils;->play(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method public static playArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artist"    # Ljava/lang/String;
    .param p2, "genre"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-static {p0, p1, p2}, Lcom/samsung/musicplus/util/player/ArtistPlayUtils;->play(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public static playByMood(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 26
    const-string v1, "extra_type"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 27
    .local v0, "mood":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/player/SquarePlayUtils;->play(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public static playContents(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    const-string v3, "base_uri"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 52
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    const-string v3, "list"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    .line 59
    .local v0, "list":[J
    if-eqz v0, :cond_0

    array-length v3, v0

    if-eqz v3, :cond_0

    .line 64
    const-string v3, "listPosition"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 67
    .local v1, "position":I
    const v3, 0x20001

    const/4 v4, 0x0

    invoke-static {p0, v3, v4, v0, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JI)V

    goto :goto_0
.end method

.method public static playGenre(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/player/GenrePlayUtils;->play(Landroid/content/Context;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public static playPlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlist"    # Ljava/lang/String;
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "genre"    # Ljava/lang/String;
    .param p5, "title"    # Ljava/lang/String;

    .prologue
    .line 190
    invoke-static/range {p0 .. p5}, Lcom/samsung/musicplus/util/player/PlaylistPlayUtils;->play(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public static playRadioChannel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "genre"    # Ljava/lang/String;
    .param p4, "rchannel"    # Ljava/lang/String;
    .param p5, "title"    # Ljava/lang/String;

    .prologue
    .line 186
    return-void
.end method

.method public static playResumeLastPlaylist(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    const-string v0, "play"

    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public static playSong(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "genre"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-static {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->play(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static playUnstructuredSearch(Landroid/content/Context;Ljava/lang/String;)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents;->SEARCH_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 120
    .local v3, "uri":Landroid/net/Uri;
    const/4 v12, 0x0

    .line 122
    .local v12, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "mime_type"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 126
    if-eqz v12, :cond_0

    .line 127
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 128
    .local v13, "count":I
    if-lez v13, :cond_0

    .line 129
    new-instance v15, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v15, v4, v5}, Ljava/util/Random;-><init>(J)V

    .line 131
    .local v15, "rand":Ljava/util/Random;
    invoke-virtual {v15, v13}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    const-string v2, "mime_type"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 134
    .local v14, "mimeType":Ljava/lang/String;
    const-string v2, "artist"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135
    const-string v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 137
    .local v10, "artistId":J
    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, Lcom/samsung/musicplus/util/player/ArtistPlayUtils;->play(Landroid/content/Context;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    .end local v10    # "artistId":J
    .end local v13    # "count":I
    .end local v14    # "mimeType":Ljava/lang/String;
    .end local v15    # "rand":Ljava/util/Random;
    :cond_0
    :goto_0
    if-eqz v12, :cond_1

    .line 154
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 157
    :cond_1
    return-void

    .line 138
    .restart local v13    # "count":I
    .restart local v14    # "mimeType":Ljava/lang/String;
    .restart local v15    # "rand":Ljava/util/Random;
    :cond_2
    :try_start_1
    const-string v2, "album"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 139
    const-string v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 141
    .local v8, "albumId":J
    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, Lcom/samsung/musicplus/util/player/AlbumPlayUtils;->play(Landroid/content/Context;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 153
    .end local v8    # "albumId":J
    .end local v13    # "count":I
    .end local v14    # "mimeType":Ljava/lang/String;
    .end local v15    # "rand":Ljava/util/Random;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_3

    .line 154
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    .line 143
    .restart local v13    # "count":I
    .restart local v14    # "mimeType":Ljava/lang/String;
    .restart local v15    # "rand":Ljava/util/Random;
    :cond_4
    :try_start_2
    const-string v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 145
    .local v16, "songId":J
    const v2, 0x20001

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [J

    const/4 v6, 0x0

    aput-wide v16, v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4, v5, v6}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static playVia(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 31
    const-string v2, "extra_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "type":Ljava/lang/String;
    const-string v2, "extra_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "search":Ljava/lang/String;
    const-string v2, "MusicPlayUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playVia() - type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " search: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v2, "title"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/player/TitlePlayUtils;->play(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    const-string v2, "album"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 38
    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/player/AlbumPlayUtils;->play(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_2
    const-string v2, "artist"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 40
    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/player/ArtistPlayUtils;->play(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_3
    const-string v2, "playlist"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    invoke-static {p0, v0}, Lcom/samsung/musicplus/util/player/PlaylistPlayUtils;->play(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static startCommand(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 93
    return-void
.end method

.method public static startCommand(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "command"    # Ljava/lang/String;
    .param p3, "force"    # Z

    .prologue
    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .local v0, "i":Landroid/content/Intent;
    if-nez p1, :cond_0

    const-string p1, "com.samsung.musicplus.action.SERVICE_COMMAND"

    .end local p1    # "action":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v1, "command"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "force"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 114
    return-void
.end method

.method public static startCommand(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "force"    # Z

    .prologue
    .line 105
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 106
    return-void
.end method
