.class public Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FolderTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1313
    invoke-direct {p0}, Lcom/samsung/musicplus/util/ListUtils$MusicPlusTabQueryArgs;-><init>()V

    .line 1314
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->FOLDERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->uri:Landroid/net/Uri;

    .line 1315
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->SAMSUNG_FOLDER_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->projection:[Ljava/lang/String;

    .line 1316
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->selection:Ljava/lang/String;

    .line 1317
    iput-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 1322
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/util/ListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 1325
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 1326
    const-string v0, "bucket_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 1328
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1329
    const-string v0, "bucket_display_name"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 1330
    const-string v0, "_data"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->text2Col:Ljava/lang/String;

    .line 1331
    const-string v0, "is_secretbox"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$FolderTabQueryArgs;->secretBoxCol:Ljava/lang/String;

    .line 1332
    return-void
.end method
