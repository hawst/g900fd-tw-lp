.class public Lcom/samsung/musicplus/util/Features;
.super Ljava/lang/Object;
.source "Features.java"


# static fields
.field public static final AGING_LOG_WRITER:Z = false

.field public static final FLAG_ENABLE_HEADER_IN_SELECTION_MODE:Z = true

.field public static final FLAG_KNOX_DAR:Z = true

.field public static final FLAG_NOW_PLAYING_OPERATION_OPTION:Z = true

.field public static final FLAG_PERSONAL_FOLDER_SELECTION:Z = true

.field public static final FLAG_PERSONAL_RENAME_OPERATION:Z = true

.field public static final FLAG_SELECT_ALL_ACTIONBAR:Z = true

.field public static final FLAG_SHOW_REMOVE_OPTION:Z = true

.field public static final FLAG_SPINNER_CAB:Z = false

.field public static final FLAG_SUPPORT_ANIMATED_PLAY_PAUSE_BUTTON:Z = false

.field public static final FLAG_SUPPORT_BIG_NOTIFICATION:Z = true

.field public static final FLAG_SUPPORT_BUFFERING:Z = false

.field public static final FLAG_SUPPORT_CHINA_WLAN:Z

.field public static final FLAG_SUPPORT_DELETE_MENU_IN_LIST:Z

.field public static final FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

.field public static final FLAG_SUPPORT_FULL_PLAYER_ANIMATION:Z = false

.field public static final FLAG_SUPPORT_GALLERY_VOLUME_EFFECT:Z

.field public static final FLAG_SUPPORT_LAUNCH_PLAYER_FROM_LIST:Z = true

.field public static final FLAG_SUPPORT_MUSIC_PROVIDER:Z = false

.field public static final FLAG_SUPPORT_MUSIC_SQUARE_MENU_ITEM:Z = true

.field public static final FLAG_SUPPORT_PARTICLE_EFFECT:Z

.field public static final FLAG_SUPPORT_POPUP_WINDOW_VOLUME:Z = true

.field public static final FLAG_SUPPORT_RECORDINGS_TAB:Z

.field public static final FLAG_SUPPORT_SHIFTJIS_CHATSET:Z = false

.field public static final FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

.field public static final FLAG_SUPPORT_SWIPE:Z = false

.field public static final FLAG_SUPPORT_VOLUME_SEEKBAR_EARSHOCK:Z = true

.field public static final FLAG_USE_CACHED_SPECTRUM_COLOR:Z = false

.field public static final NATIVE_MUSIC_MODE:Z = true

.field public static final PRODUCT_NAME:Ljava/lang/String;

.field public static final SERVER_TIMESTAMP:Z = true

.field private static T_BASE_MODEL:Z

.field public static final USE_CURRENT_PLAYLIST_FOR_REORDER:Z

.field private static final sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/Features;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    .line 15
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    .line 17
    sget-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tr3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/util/Features;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "muscat3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->T_BASE_MODEL:Z

    .line 112
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    .line 117
    const-string v0, "CHINA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_CHINA_WLAN:Z

    .line 128
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->T_BASE_MODEL:Z

    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_GALLERY_VOLUME_EFFECT:Z

    .line 154
    sget-object v0, Lcom/samsung/musicplus/util/Features;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_PARTICLE_EFFECT"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_PARTICLE_EFFECT:Z

    .line 164
    sget-object v0, Lcom/samsung/musicplus/util/Features;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_INTERNAL_SOUNDALIVE"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    sput-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    .line 169
    sget-object v0, Lcom/samsung/musicplus/util/Features;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v1, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_RECORDING_TAB"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_RECORDINGS_TAB:Z

    .line 174
    sget-object v0, Lcom/samsung/musicplus/util/Features;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v1, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_LIST_DELETE_MENU"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_DELETE_MENU_IN_LIST:Z

    return-void

    :cond_1
    move v0, v1

    .line 17
    goto :goto_0

    :cond_2
    move v2, v1

    .line 164
    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
