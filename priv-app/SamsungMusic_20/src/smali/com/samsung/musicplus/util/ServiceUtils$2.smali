.class final Lcom/samsung/musicplus/util/ServiceUtils$2;
.super Ljava/lang/Thread;
.source "ServiceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/util/ServiceUtils;->changeListWithoutPlaying(Landroid/content/Context;ILjava/lang/String;[JIZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$keyWord:Ljava/lang/String;

.field final synthetic val$list:[J

.field final synthetic val$listType:I

.field final synthetic val$playCurrentSong:Z

.field final synthetic val$position:I


# direct methods
.method constructor <init>(ILjava/lang/String;[JIZ)V
    .locals 0

    .prologue
    .line 194
    iput p1, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$listType:I

    iput-object p2, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$keyWord:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$list:[J

    iput p4, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$position:I

    iput-boolean p5, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$playCurrentSong:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 196
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v1, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    iget v2, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$listType:I

    iget-object v3, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$keyWord:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$list:[J

    iget v5, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$position:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/musicplus/service/IPlayerService;->changeListWithoutPlaying(ILjava/lang/String;[JI)V

    .line 201
    iget-boolean v1, p0, Lcom/samsung/musicplus/util/ServiceUtils$2;->val$playCurrentSong:Z

    if-eqz v1, :cond_0

    .line 202
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->play()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
