.class public Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;
.super Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "audioId"    # J

    .prologue
    .line 1074
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(J)V

    .line 1075
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1077
    const-string v0, "track"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1079
    const-string v0, "title != \'\' AND is_music=1 AND _id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1080
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 1081
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 1082
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 1083
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 1084
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/util/ListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;)V

    .line 1064
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->ALBUM_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 1066
    const-string v0, "track"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 1068
    const-string v0, "title != \'\' AND is_music=1 AND album_id=?"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->selection:Ljava/lang/String;

    .line 1069
    const-string v0, "sampling_rate"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 1070
    const-string v0, "bit_depth"

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$AlbumTrackQueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 1071
    return-void
.end method
