.class Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;
.super Ljava/lang/Object;
.source "AlbumArtLoader.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/AlbumArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageViewPublisher"
.end annotation


# instance fields
.field private mAnimation:Z

.field private mArtKey:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mIv:Landroid/widget/ImageView;

.field private mResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;IZ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I
    .param p5, "animation"    # Z

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    iput-object p1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mContext:Landroid/content/Context;

    .line 228
    iput-object p2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    .line 229
    iput-object p3, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mArtKey:Ljava/lang/Object;

    .line 230
    iput p4, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mResId:I

    .line 231
    iput-boolean p5, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mAnimation:Z

    .line 232
    return-void
.end method


# virtual methods
.method public onFailPublishImage(Landroid/net/Uri;J)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "elapsedTime"    # J

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mArtKey:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mArtKey:Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 257
    :cond_0
    return-void
.end method

.method public onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 236
    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mArtKey:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mArtKey:Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    const-string v1, "ImageView"

    # invokes: Lcom/samsung/musicplus/util/AlbumArtLoader;->debugBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V
    invoke-static {v1, p2, p1, p3, p4}, Lcom/samsung/musicplus/util/AlbumArtLoader;->access$000(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/net/Uri;J)V

    .line 238
    iget-boolean v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mAnimation:Z

    if-eqz v1, :cond_1

    const-wide/16 v2, 0x32

    cmp-long v1, p3, v2

    if-lez v1, :cond_1

    .line 239
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    # getter for: Lcom/samsung/musicplus/util/AlbumArtLoader;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->access$100()Landroid/graphics/drawable/ColorDrawable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 244
    .local v0, "td":Landroid/graphics/drawable/TransitionDrawable;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 245
    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 250
    .end local v0    # "td":Landroid/graphics/drawable/TransitionDrawable;
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/util/AlbumArtLoader$ImageViewPublisher;->mIv:Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
