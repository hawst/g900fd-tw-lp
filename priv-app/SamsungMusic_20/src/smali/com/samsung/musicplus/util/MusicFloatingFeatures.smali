.class public Lcom/samsung/musicplus/util/MusicFloatingFeatures;
.super Ljava/lang/Object;
.source "MusicFloatingFeatures.java"


# static fields
.field public static final FLAG_ENABLE_SURVEY_MODE:Z

.field public static final FLAG_SUPPORT_IMAGE_STARTING_WINDOW:Z

.field private static final sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/util/MusicFloatingFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    .line 18
    sget-object v0, Lcom/samsung/musicplus/util/MusicFloatingFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v1, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/util/MusicFloatingFeatures;->FLAG_ENABLE_SURVEY_MODE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
