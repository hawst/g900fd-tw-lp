.class public Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.super Ljava/lang/Object;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QueryArgs"
.end annotation


# instance fields
.field public albumArtCol:Ljava/lang/String;

.field public albumIdCol:Ljava/lang/String;

.field public audioIdCol:Ljava/lang/String;

.field public bitDepthCol:Ljava/lang/String;

.field public durationCol:Ljava/lang/String;

.field public indexBy:Ljava/lang/String;

.field public keyWord:Ljava/lang/String;

.field public orderBy:Ljava/lang/String;

.field public projection:[Ljava/lang/String;

.field public samplingRateCol:Ljava/lang/String;

.field public secretBoxCol:Ljava/lang/String;

.field public selection:Ljava/lang/String;

.field public selectionArgs:[Ljava/lang/String;

.field public text1Col:Ljava/lang/String;

.field public text2Col:Ljava/lang/String;

.field public text3Col:Ljava/lang/String;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 840
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->keyWord:Ljava/lang/String;

    .line 845
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    .line 850
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->durationCol:Ljava/lang/String;

    .line 855
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    .line 860
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    .line 865
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text3Col:Ljava/lang/String;

    .line 871
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumIdCol:Ljava/lang/String;

    .line 877
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumArtCol:Ljava/lang/String;

    .line 882
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 887
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 892
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->secretBoxCol:Ljava/lang/String;

    return-void
.end method
