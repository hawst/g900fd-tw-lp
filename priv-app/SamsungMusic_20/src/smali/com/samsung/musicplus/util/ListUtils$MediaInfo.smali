.class public Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
.super Ljava/lang/Object;
.source "ListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/util/ListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaInfo"
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public albumArt:Ljava/lang/String;

.field public albumId:J

.field public artist:Ljava/lang/String;

.field public bitDepth:I

.field public deviceId:J

.field public dmsId:Ljava/lang/String;

.field public dmsName:Ljava/lang/String;

.field public duration:J

.field public extendtion:Ljava/lang/String;

.field public filePath:Ljava/lang/String;

.field public fileSize:J

.field public genre:Ljava/lang/String;

.field public id:J

.field public mediaType:I

.field public mimeType:Ljava/lang/String;

.field public moodValue:I

.field public personalMode:I

.field public samplingRate:I

.field public seed:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2362
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->moodValue:I

    .line 2373
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mediaType:I

    return-void
.end method
