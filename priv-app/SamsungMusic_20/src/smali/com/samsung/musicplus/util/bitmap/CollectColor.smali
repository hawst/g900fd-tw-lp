.class public Lcom/samsung/musicplus/util/bitmap/CollectColor;
.super Ljava/lang/Object;
.source "CollectColor.java"


# static fields
.field private static final COLOR_MASK:I = -0x1f1f20

.field private static final TARGET_SIZE:I = 0x3c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAverageColor(Landroid/graphics/Bitmap;)I
    .locals 15
    .param p0, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 91
    if-nez p0, :cond_0

    .line 114
    :goto_0
    return v2

    .line 95
    :cond_0
    const/16 v8, 0x28

    .line 96
    .local v8, "TARGET_SIZE":I
    const/4 v3, 0x1

    invoke-static {p0, v8, v8, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 97
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v14, 0x0

    .line 98
    .local v14, "redBucket":I
    const/4 v11, 0x0

    .line 99
    .local v11, "greenBucket":I
    const/4 v9, 0x0

    .line 100
    .local v9, "blueBucket":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    mul-int v13, v3, v4

    .line 103
    .local v13, "pixelCount":I
    new-array v1, v13, [I

    .line 105
    .local v1, "mBitmapBuffer":[I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 107
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    if-ge v12, v13, :cond_1

    .line 108
    aget v10, v1, v12

    .line 109
    .local v10, "c":I
    invoke-static {v10}, Landroid/graphics/Color;->red(I)I

    move-result v2

    add-int/2addr v14, v2

    .line 110
    invoke-static {v10}, Landroid/graphics/Color;->green(I)I

    move-result v2

    add-int/2addr v11, v2

    .line 111
    invoke-static {v10}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    add-int/2addr v9, v2

    .line 107
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 114
    .end local v10    # "c":I
    :cond_1
    div-int v2, v14, v13

    div-int v3, v11, v13

    div-int v4, v9, v13

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    goto :goto_0
.end method

.method private static getMaxOcurrenceColor([I)I
    .locals 6
    .param p0, "bitmapBuffer"    # [I

    .prologue
    const/4 v5, 0x0

    .line 47
    const/4 v2, 0x1

    .line 48
    .local v2, "maxOcr":I
    aget v3, p0, v5

    .line 49
    .local v3, "maxOcrColor":I
    const/4 v4, 0x1

    .line 50
    .local v4, "ocr":I
    aget v0, p0, v5

    .line 51
    .local v0, "color":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v5, p0

    if-ge v1, v5, :cond_2

    .line 52
    aget v5, p0, v1

    if-ne v0, v5, :cond_0

    .line 53
    add-int/lit8 v4, v4, 0x1

    .line 51
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    :cond_0
    if-le v4, v2, :cond_1

    .line 56
    move v2, v4

    .line 57
    move v3, v0

    .line 59
    :cond_1
    const/4 v4, 0x1

    .line 60
    aget v0, p0, v1

    goto :goto_1

    .line 63
    :cond_2
    const/high16 v5, -0x1000000

    or-int/2addr v5, v3

    return v5
.end method

.method public static getMostImportantColor(Landroid/graphics/Bitmap;)I
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v2, 0x0

    .line 37
    :goto_0
    return v2

    .line 32
    :cond_0
    invoke-static {p0}, Lcom/samsung/musicplus/util/bitmap/CollectColor;->getPixelsFromScaledBitmap(Landroid/graphics/Bitmap;)[I

    move-result-object v0

    .line 34
    .local v0, "bitmapBuffer":[I
    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    .line 35
    invoke-static {v0}, Lcom/samsung/musicplus/util/bitmap/CollectColor;->getMaxOcurrenceColor([I)I

    move-result v1

    .line 37
    .local v1, "color":I
    const v2, -0x1f1f20

    and-int/2addr v2, v1

    goto :goto_0
.end method

.method private static getPixelsFromScaledBitmap(Landroid/graphics/Bitmap;)[I
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v5, 0x3c

    const/4 v2, 0x0

    .line 73
    const/4 v4, 0x1

    invoke-static {p0, v5, v5, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 75
    .local v0, "bitmapSmall":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 76
    .local v3, "width":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 78
    .local v7, "height":I
    mul-int v4, v3, v7

    new-array v1, v4, [I

    .local v1, "bitmapBuffer":[I
    move v4, v2

    move v5, v2

    move v6, v3

    .line 79
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 81
    return-object v1
.end method
