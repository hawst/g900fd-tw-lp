.class public Lcom/samsung/musicplus/util/player/GenrePlayUtils;
.super Ljava/lang/Object;
.source "GenrePlayUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private static getSongList(Landroid/content/Context;Ljava/lang/String;)[J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreName"    # Ljava/lang/String;

    .prologue
    .line 28
    const/4 v6, 0x0

    .line 30
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "genre_name LIKE \"%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "title != \'\' AND is_music=1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 37
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 39
    .local v7, "list":[J
    if-eqz v6, :cond_0

    .line 40
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 43
    :cond_0
    return-object v7

    .line 39
    .end local v7    # "list":[J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 40
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public static play(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreName"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/player/GenrePlayUtils;->playInternal(Landroid/content/Context;Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method private static playInternal(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreName"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-static {p0, p1}, Lcom/samsung/musicplus/util/player/GenrePlayUtils;->getSongList(Landroid/content/Context;Ljava/lang/String;)[J

    move-result-object v0

    .line 22
    .local v0, "list":[J
    const v1, 0x20006

    const/4 v2, 0x0

    invoke-static {p0, v1, p1, v0, v2}, Lcom/samsung/musicplus/util/player/PlayUtils;->play(Landroid/content/Context;ILjava/lang/String;[JI)V

    .line 23
    return-void
.end method
