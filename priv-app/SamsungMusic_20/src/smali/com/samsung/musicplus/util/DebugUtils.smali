.class public Lcom/samsung/musicplus/util/DebugUtils;
.super Ljava/lang/Object;
.source "DebugUtils.java"


# static fields
.field private static final TSP_SERVICE_TAG:Ljava/lang/String; = "MusicTspService"

.field public static final TSP_SERVICE_TEST:Z = false

.field private static final TSP_TAG:Ljava/lang/String; = "MusicTsp"

.field public static final TSP_TEST:Z

.field private static sTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/samsung/musicplus/util/DebugUtils;->sTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFieldsStringValueNameForDebugging(Ljava/lang/Object;I)Ljava/lang/String;
    .locals 6
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "stringType"    # I

    .prologue
    .line 93
    if-nez p0, :cond_1

    .line 94
    const/4 v3, 0x0

    .line 110
    :cond_0
    :goto_0
    return-object v3

    .line 96
    :cond_1
    const/4 v3, 0x0

    .line 97
    .local v3, "stringTypeName":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 99
    .local v1, "fields":[Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    array-length v5, v1

    if-ge v2, v5, :cond_0

    .line 100
    aget-object v5, v1, v2

    invoke-virtual {v5, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    .line 101
    .local v4, "type":I
    if-ne v4, p1, :cond_2

    .line 102
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 99
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 105
    .end local v4    # "type":I
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 107
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public static printTspLog(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 14
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 55
    .local v2, "currentTime":J
    const/4 v4, 0x0

    .line 56
    .local v4, "isServiceProcess":Z
    instance-of v8, p0, Landroid/content/Context;

    if-eqz v8, :cond_0

    move-object v8, p0

    .line 57
    check-cast v8, Landroid/content/Context;

    const-string v9, "activity"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 60
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v8, 0x2710

    invoke-virtual {v0, v8}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v6

    .line 61
    .local v6, "s":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 62
    .local v7, "size":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v7, :cond_0

    .line 63
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v5, v8, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    .line 64
    .local v5, "name":Ljava/lang/String;
    const-string v8, "com.samsung.musicplus:service"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 65
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v9

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningServiceInfo;

    iget v8, v8, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    if-ne v9, v8, :cond_0

    .line 66
    const/4 v4, 0x1

    .line 73
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "i":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "s":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    .end local v7    # "size":I
    :cond_0
    if-eqz v4, :cond_3

    .line 82
    :goto_2
    sput-wide v2, Lcom/samsung/musicplus/util/DebugUtils;->sTime:J

    .line 83
    return-void

    .line 61
    .restart local v0    # "am":Landroid/app/ActivityManager;
    .restart local v6    # "s":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 62
    .restart local v1    # "i":I
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v7    # "size":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 79
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "i":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "s":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    .end local v7    # "size":I
    :cond_3
    const-string v10, "MusicTsp"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-wide v8, Lcom/samsung/musicplus/util/DebugUtils;->sTime:J

    const-wide/16 v12, 0x0

    cmp-long v8, v8, v12

    if-nez v8, :cond_4

    sget-wide v8, Lcom/samsung/musicplus/util/DebugUtils;->sTime:J

    :goto_3
    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    sget-wide v8, Lcom/samsung/musicplus/util/DebugUtils;->sTime:J

    sub-long v8, v2, v8

    goto :goto_3
.end method

.method public static resetTime()V
    .locals 2

    .prologue
    .line 42
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/samsung/musicplus/util/DebugUtils;->sTime:J

    .line 43
    return-void
.end method
