.class Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "PalmTouchTutorialActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/PalmTouchTutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 232
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.PALM_DOWN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 234
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    iget-object v3, v3, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 235
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    # setter for: Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlaySound:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->access$002(Lcom/samsung/musicplus/PalmTouchTutorialActivity;Z)Z

    .line 236
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    # invokes: Lcom/samsung/musicplus/PalmTouchTutorialActivity;->stopSound()V
    invoke-static {v3}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->access$100(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V

    .line 237
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    # getter for: Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mPlayPauseBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->access$200(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f02006e

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 238
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    # getter for: Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mTouchHelpText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->access$300(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f10011c

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 243
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    invoke-virtual {v3}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "PalmMotionTest"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 244
    .local v2, "value":Z
    if-eqz v2, :cond_0

    .line 245
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    invoke-static {v3, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 247
    .local v1, "t":Landroid/widget/Toast;
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    const v4, 0x7f040092

    invoke-static {v3, v4, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 249
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 251
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    iget-object v3, v3, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    iget-object v4, v4, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mWork:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 252
    iget-object v3, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    iget-object v3, v3, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$2;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    iget-object v4, v4, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->mWork:Ljava/lang/Runnable;

    const-wide/16 v6, 0xbb8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 257
    .end local v1    # "t":Landroid/widget/Toast;
    .end local v2    # "value":Z
    :cond_0
    return-void
.end method
