.class Lcom/samsung/musicplus/player/PlayerActivity$8;
.super Landroid/content/BroadcastReceiver;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerActivity;)V
    .locals 0

    .prologue
    .line 1058
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerActivity$8;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1061
    const/4 v1, 0x0

    .line 1062
    .local v1, "isConnected":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1063
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.android.sidesync.source.SIDESYNC_CONNECTED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1064
    const/4 v1, 0x1

    .line 1068
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity$8;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v2}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v3

    if-nez v1, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItemEnabled(Z)V

    .line 1069
    return-void

    .line 1065
    :cond_1
    const-string v2, "com.sec.android.sidesync.source.SIDESYNC_DISCONNECT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1066
    const/4 v1, 0x0

    goto :goto_0

    .line 1068
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
