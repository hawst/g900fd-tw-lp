.class Lcom/samsung/musicplus/player/FullPlayerView$22;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 3017
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$22;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromTouch"    # Z

    .prologue
    .line 3021
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$22;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-boolean v0, v0, Lcom/samsung/musicplus/player/FullPlayerView;->mPause:Z

    if-eqz v0, :cond_1

    .line 3028
    :cond_0
    :goto_0
    return-void

    .line 3025
    :cond_1
    if-eqz p3, :cond_0

    .line 3026
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$22;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setVolume(I)V
    invoke-static {v0, p2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4800(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 3032
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 3036
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$22;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->showSmartVolumeToast()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4900(Lcom/samsung/musicplus/player/FullPlayerView;)V

    .line 3037
    return-void
.end method
