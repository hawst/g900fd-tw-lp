.class Lcom/samsung/musicplus/player/FullPlayerView$6;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 605
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNowPlayingQueryComplete(Lcom/samsung/musicplus/player/NowPlayingCursor;)V
    .locals 6
    .param p1, "c"    # Lcom/samsung/musicplus/player/NowPlayingCursor;

    .prologue
    .line 608
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 609
    const/4 v0, 0x0

    .line 610
    .local v0, "old":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 611
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 613
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    new-instance v2, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v3, v3, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c003f

    const v5, 0x7f0c0040

    invoke-direct {v2, v3, p1, v4, v5}, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;II)V

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1702(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/GLAlbumArtAdapter;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    .line 616
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtUriListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1800(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->setAlbArtUriListener(Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;)V

    .line 617
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 620
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1902(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 622
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->updateGLAlbumartPosition()V
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2000(Lcom/samsung/musicplus/player/FullPlayerView;)V

    .line 623
    if-eqz v0, :cond_1

    .line 624
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 631
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$6;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateAlbumArt(I)V

    .line 633
    .end local v0    # "old":Landroid/database/Cursor;
    :cond_2
    return-void
.end method
