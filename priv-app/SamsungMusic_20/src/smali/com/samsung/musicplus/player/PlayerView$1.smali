.class Lcom/samsung/musicplus/player/PlayerView$1;
.super Landroid/os/Handler;
.source "PlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerView;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$1;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 205
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 216
    :goto_0
    return-void

    .line 207
    :sswitch_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView$1;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/PlayerView;->refreshNow()J

    move-result-wide v0

    .line 208
    .local v0, "next":J
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView$1;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    const/4 v3, 0x0

    # invokes: Lcom/samsung/musicplus/player/PlayerView;->queueNextRefresh(JZ)V
    invoke-static {v2, v0, v1, v3}, Lcom/samsung/musicplus/player/PlayerView;->access$100(Lcom/samsung/musicplus/player/PlayerView;JZ)V

    goto :goto_0

    .line 211
    .end local v0    # "next":J
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$1;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Bitmap;

    # invokes: Lcom/samsung/musicplus/player/PlayerView;->onAlbumLoadFinished(Landroid/graphics/Bitmap;)V
    invoke-static {v3, v2}, Lcom/samsung/musicplus/player/PlayerView;->access$200(Lcom/samsung/musicplus/player/PlayerView;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 205
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method
