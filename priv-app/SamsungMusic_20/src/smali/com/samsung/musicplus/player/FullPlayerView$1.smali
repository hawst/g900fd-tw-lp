.class Lcom/samsung/musicplus/player/FullPlayerView$1;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView;->handleAlbumFlingEvent(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;

.field final synthetic val$direction:I

.field final synthetic val$in:Landroid/view/animation/Animation;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;ILandroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iput p2, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->val$direction:I

    iput-object p3, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->val$in:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$000(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 398
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 402
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 406
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumArtView()Landroid/widget/ImageView;

    move-result-object v0

    .line 407
    .local v0, "albumArt":Landroid/widget/ImageView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 408
    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->val$direction:I

    const/16 v2, 0x67

    if-ne v1, v2, :cond_0

    .line 409
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->playNext()V

    .line 413
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$1;->val$in:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 414
    return-void

    .line 411
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->playPrev(Z)V

    goto :goto_0
.end method
