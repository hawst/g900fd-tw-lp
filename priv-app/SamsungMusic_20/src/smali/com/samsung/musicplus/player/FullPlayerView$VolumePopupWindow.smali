.class Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;
.super Ljava/lang/Object;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumePopupWindow"
.end annotation


# static fields
.field private static final NO_EAR_SHOCK:I = -0x1


# instance fields
.field private mPopupWindowDmr:Landroid/widget/PopupWindow;

.field private mPopupWindowNormal:Landroid/widget/PopupWindow;

.field private mRootView:Landroid/view/View;

.field private mSoundAliveButton:Landroid/view/View;

.field private mVolumePopupSeekBar:Landroid/widget/SeekBar;

.field private mVolumeText:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;

.field private x:I

.field private y:I


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/Context;Landroid/view/View;I)V
    .locals 8
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "root"    # Landroid/view/View;
    .param p4, "maxVolume"    # I

    .prologue
    const v7, 0x7f0c0113

    .line 3239
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3240
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 3241
    .local v2, "r":Landroid/content/res/Resources;
    iput-object p3, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mRootView:Landroid/view/View;

    .line 3244
    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 3245
    .local v4, "width":I
    const v5, 0x7f0c0109

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 3248
    .local v1, "height":I
    iget-object v5, p1, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3250
    .local v0, "display":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 3251
    .local v3, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 3253
    iget v5, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    const v6, 0x7f0c010b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    .line 3255
    const v5, 0x7f0c010c

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->y:I

    .line 3257
    invoke-direct {p0, v4, v1, p4}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->initNormal(III)V

    .line 3258
    invoke-direct {p0, v4, v1}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->initDmr(II)V

    .line 3259
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/Context;Landroid/view/View;ILcom/samsung/musicplus/player/FullPlayerView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Landroid/view/View;
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/samsung/musicplus/player/FullPlayerView$1;

    .prologue
    .line 3221
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/Context;Landroid/view/View;I)V

    return-void
.end method

.method private initDmr(II)V
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const v6, 0x7f0d0132

    const v5, 0x7f0d0131

    const v4, 0x7f0d0130

    .line 3305
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, v1, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04006a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 3307
    .local v0, "v":Landroid/view/View;
    new-instance v1, Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, p2, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowDmr:Landroid/widget/PopupWindow;

    .line 3310
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f1001ab

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3312
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$2;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$2;-><init>(Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3320
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f1001a9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3322
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$3;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$3;-><init>(Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3331
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f1001aa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3333
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$4;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$4;-><init>(Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3341
    return-void
.end method

.method private initNormal(III)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "maxVolume"    # I

    .prologue
    .line 3262
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, v1, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040069

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 3264
    .local v0, "v":Landroid/view/View;
    new-instance v1, Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, p2, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowNormal:Landroid/widget/PopupWindow;

    .line 3267
    const v1, 0x7f0d012d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mSoundAliveButton:Landroid/view/View;

    .line 3268
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mSoundAliveButton:Landroid/view/View;

    new-instance v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;-><init>(Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3293
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mSoundAliveButton:Landroid/view/View;

    const/16 v3, 0x3031

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    .line 3296
    const v1, 0x7f0d012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    .line 3297
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5100(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 3298
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 3299
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 3301
    const v1, 0x7f0d012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumeText:Landroid/widget/TextView;

    .line 3302
    return-void
.end method


# virtual methods
.method public getSoundAliveButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 3457
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mSoundAliveButton:Landroid/view/View;

    return-object v0
.end method

.method public getVolumeSeekBar()Landroid/view/View;
    .locals 1

    .prologue
    .line 3461
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 3418
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->isVisibleNormalVolumePanel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3419
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowNormal:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 3421
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowDmr:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3422
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowDmr:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 3424
    :cond_1
    return-void
.end method

.method public isVisibleNormalVolumePanel()Z
    .locals 1

    .prologue
    .line 3465
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowNormal:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public setVolume(I)V
    .locals 6
    .param p1, "volume"    # I

    .prologue
    const/4 v5, 0x0

    .line 3344
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mFormatBuilder:Ljava/lang/StringBuilder;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5200(Lcom/samsung/musicplus/player/FullPlayerView;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3345
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, v1, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f1001c7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3346
    .local v0, "arg":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 3347
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumeText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mFormatter:Ljava/util/Formatter;
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5300(Lcom/samsung/musicplus/player/FullPlayerView;)Ljava/util/Formatter;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3348
    return-void
.end method

.method public showDmr()V
    .locals 5

    .prologue
    .line 3414
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowDmr:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mRootView:Landroid/view/View;

    const/16 v2, 0x33

    iget v3, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    iget v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->y:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 3415
    return-void
.end method

.method public showNormal()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f0c0113

    const v8, 0x7f0c010b

    .line 3351
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v6, v6, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3354
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$200(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7, v10, v10}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->adjustStreamVolume(III)V

    .line 3411
    :goto_0
    return-void

    .line 3359
    :cond_0
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$200(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathEarjack()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$200(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3360
    :cond_1
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    iget-object v7, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v7}, Lcom/samsung/musicplus/player/FullPlayerView;->access$200(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getEarProtectLimitIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setOverlapPointForDualColor(I)V

    .line 3365
    :goto_1
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v6, v6, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3366
    .local v4, "r":Landroid/content/res/Resources;
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v6, v6, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v7, "window"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3368
    .local v0, "display":Landroid/view/Display;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 3369
    .local v5, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 3370
    const/16 v2, 0x33

    .line 3371
    .local v2, "gravity":I
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v6, v6, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.feature.cocktailbar"

    invoke-static {v6, v7}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 3372
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3376
    iget v6, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    mul-int/lit8 v7, v7, 0xc

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    .line 3387
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v6, v6, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-static {v6}, Lcom/samsung/musicplus/util/UiUtils;->isMultiWindowActive(Landroid/app/Activity;)Z

    move-result v3

    .line 3388
    .local v3, "isMultiwindow":Z
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v6

    if-eqz v6, :cond_7

    if-eqz v3, :cond_7

    .line 3389
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, v5, Landroid/graphics/Point;->y:I

    div-int/lit8 v7, v7, 0x2

    if-le v6, v7, :cond_6

    .line 3390
    const/16 v2, 0x53

    .line 3391
    iput v10, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->y:I

    .line 3396
    :goto_3
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget v7, v5, Landroid/graphics/Point;->x:I

    if-ge v6, v7, :cond_3

    .line 3397
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$5400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    .line 3407
    :cond_3
    :goto_4
    :try_start_0
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowNormal:Landroid/widget/PopupWindow;

    iget-object v7, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mRootView:Landroid/view/View;

    iget v8, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    iget v9, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->y:I

    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3408
    :catch_0
    move-exception v1

    .line 3409
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 3362
    .end local v0    # "display":Landroid/view/Display;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "gravity":I
    .end local v3    # "isMultiwindow":Z
    .end local v4    # "r":Landroid/content/res/Resources;
    .end local v5    # "size":Landroid/graphics/Point;
    :cond_4
    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mVolumePopupSeekBar:Landroid/widget/SeekBar;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setOverlapPointForDualColor(I)V

    goto/16 :goto_1

    .line 3381
    .restart local v0    # "display":Landroid/view/Display;
    .restart local v2    # "gravity":I
    .restart local v4    # "r":Landroid/content/res/Resources;
    .restart local v5    # "size":Landroid/graphics/Point;
    :cond_5
    iget v6, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    goto :goto_2

    .line 3393
    .restart local v3    # "isMultiwindow":Z
    :cond_6
    const/16 v2, 0x33

    .line 3394
    const v6, 0x7f0c010c

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->y:I

    goto :goto_3

    .line 3401
    :cond_7
    if-nez v3, :cond_3

    .line 3402
    iget v6, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->x:I

    goto :goto_4
.end method

.method public toggleDmr()V
    .locals 1

    .prologue
    .line 3449
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowDmr:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3450
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowDmr:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 3454
    :goto_0
    return-void

    .line 3452
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->showDmr()V

    goto :goto_0
.end method

.method public toggleNormal()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3427
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->isVisibleNormalVolumePanel()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3428
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->getSoundAliveButton()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3429
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3430
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "android.media.extra.AUDIO_SESSION"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAudioSessionId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3432
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3440
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z
    invoke-static {v2, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3802(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 3441
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->mPopupWindowNormal:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    .line 3446
    :goto_1
    return-void

    .line 3433
    .restart local v1    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 3434
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MusicUi"

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3435
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 3436
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3443
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z
    invoke-static {v2, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3802(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 3444
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->showNormal()V

    goto :goto_1
.end method
