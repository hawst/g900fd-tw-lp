.class Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->initNormal(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;)V
    .locals 0

    .prologue
    .line 3268
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3271
    sget-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-eqz v2, :cond_0

    .line 3272
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3273
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "android.media.extra.AUDIO_SESSION"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAudioSessionId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3276
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3290
    .end local v1    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 3277
    .restart local v1    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 3278
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MusicUi"

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3279
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 3280
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3284
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    sget-boolean v2, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_NEW_SOUNDALIVE:Z

    if-eqz v2, :cond_1

    .line 3285
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v4, v4, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v4, v4, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-class v5, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 3287
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, v2, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow$1;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v4, v4, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v4, v4, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-class v5, Lcom/samsung/musicplus/settings/SoundAliveSetting;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
