.class Lcom/samsung/musicplus/player/FullPlayerView$18;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView;->initGLGallery(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 2317
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$18;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2320
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v4, "MusicUi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mRecommendedMusicAdapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$18;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cursor: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$18;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2322
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$18;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v0

    .line 2325
    .local v0, "audioids":[J
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2326
    .local v3, "str":Ljava/lang/StringBuilder;
    array-length v2, v0

    .line 2327
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 2328
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2329
    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2327
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2331
    :cond_0
    const-string v4, "MusicUi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mRecommendedMusicAdapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$18;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v5, 0x1

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z
    invoke-static {v4, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1902(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 2336
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$18;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v4, v4, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v5, 0x2000a

    const-string v6, "_id"

    invoke-static {v6, v0}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v0, p3}, Lcom/samsung/musicplus/util/ServiceUtils;->openList(Landroid/content/Context;ILjava/lang/String;[JI)Z

    .line 2339
    return-void
.end method
