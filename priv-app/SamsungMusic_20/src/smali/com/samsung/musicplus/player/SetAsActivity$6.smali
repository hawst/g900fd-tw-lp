.class Lcom/samsung/musicplus/player/SetAsActivity$6;
.super Ljava/lang/Object;
.source "SetAsActivity.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/SetAsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/SetAsActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0

    .prologue
    .line 790
    iput-object p1, p0, Lcom/samsung/musicplus/player/SetAsActivity$6;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 793
    packed-switch p1, :pswitch_data_0

    .line 803
    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/player/SetAsActivity;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    :cond_0
    :goto_0
    return-void

    .line 797
    :pswitch_0
    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/player/SetAsActivity;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioFocus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$6;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$900(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$6;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$900(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$6;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # invokes: Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$1000(Lcom/samsung/musicplus/player/SetAsActivity;)V

    goto :goto_0

    .line 793
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
