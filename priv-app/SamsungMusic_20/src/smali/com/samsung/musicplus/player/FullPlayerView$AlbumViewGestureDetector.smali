.class Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumViewGestureDetector"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/FullPlayerView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p2, "x1"    # Lcom/samsung/musicplus/player/FullPlayerView$1;

    .prologue
    .line 545
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 548
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->handleMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1200(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    .line 549
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$300(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleLyricView()Z

    .line 557
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
