.class Lcom/samsung/musicplus/player/FullPlayerView$14;
.super Landroid/os/AsyncTask;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView;->updateAlbumArt(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field albumArtUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;

.field final synthetic val$listType:I


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;I)V
    .locals 1

    .prologue
    .line 1549
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iput p2, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->val$listType:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1550
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->albumArtUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 3
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 1554
    const/4 v0, 0x0

    .line 1555
    .local v0, "color":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1900(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1556
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1557
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1500(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/util/SparseArray;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mPosition:I
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4100(Lcom/samsung/musicplus/player/FullPlayerView;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->albumArtUri:Landroid/net/Uri;

    .line 1565
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->albumArtUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "color":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .line 1569
    .restart local v0    # "color":Ljava/lang/Integer;
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1549
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$14;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "color"    # Ljava/lang/Integer;

    .prologue
    .line 1574
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1900(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1575
    if-nez p1, :cond_1

    .line 1576
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->val$listType:I

    # invokes: Lcom/samsung/musicplus/player/PlayerView;->updateAlbumArt(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4201(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    .line 1584
    :cond_0
    :goto_0
    return-void

    .line 1579
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateAlbumLoading(Z)V

    .line 1580
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2600(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    .line 1581
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$14;->albumArtUri:Landroid/net/Uri;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->updateButtonThumbnailImage(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4300(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1549
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$14;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
