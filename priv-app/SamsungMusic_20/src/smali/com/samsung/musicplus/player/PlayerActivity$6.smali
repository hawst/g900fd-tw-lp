.class Lcom/samsung/musicplus/player/PlayerActivity$6;
.super Landroid/content/BroadcastReceiver;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerActivity;)V
    .locals 0

    .prologue
    .line 956
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerActivity$6;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 959
    const-string v1, "command"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 960
    .local v0, "cmd":I
    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "air gesture, cmd : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    const-string v1, "com.samsung.musicplus.GESTURE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 962
    const/4 v1, 0x1

    if-ne v1, v0, :cond_1

    .line 963
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity$6;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v1

    const/16 v2, 0x67

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->handleAlbumFling(I)V

    .line 964
    const-string v1, "GEST"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 970
    :cond_0
    :goto_0
    return-void

    .line 965
    :cond_1
    if-nez v0, :cond_0

    .line 966
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity$6;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->handleAlbumFling(I)V

    .line 967
    const-string v1, "GEST"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
