.class Lcom/samsung/musicplus/player/NowPlayingCursorLoader$1;
.super Landroid/os/Handler;
.source "NowPlayingCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/NowPlayingCursorLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$1;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$1;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    # getter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;
    invoke-static {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$200(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$1;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    # getter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;
    invoke-static {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$200(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/player/NowPlayingCursor;

    invoke-interface {v1, v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;->onNowPlayingQueryComplete(Lcom/samsung/musicplus/player/NowPlayingCursor;)V

    .line 94
    :cond_0
    return-void
.end method
