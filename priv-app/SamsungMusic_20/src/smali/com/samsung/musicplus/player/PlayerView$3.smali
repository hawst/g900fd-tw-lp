.class Lcom/samsung/musicplus/player/PlayerView$3;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/PlayerView;->setOnClickListener(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerView;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$3;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 452
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 454
    .local v0, "viewId":I
    sparse-switch v0, :sswitch_data_0

    .line 474
    :goto_0
    return-void

    .line 458
    :sswitch_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->togglePlay()V

    .line 459
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$3;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/PlayerView;->updatePlayState(Z)V

    goto :goto_0

    .line 463
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$3;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/PlayerView;->togglePlayerView()V

    goto :goto_0

    .line 466
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$3;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/PlayerView;->toggleShuffle()V

    goto :goto_0

    .line 469
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$3;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/PlayerView;->toggleRepeat()V

    goto :goto_0

    .line 454
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d00f7 -> :sswitch_2
        0x7f0d00f8 -> :sswitch_3
        0x7f0d00fa -> :sswitch_0
        0x7f0d00fb -> :sswitch_0
        0x7f0d0116 -> :sswitch_1
        0x7f0d0119 -> :sswitch_1
        0x7f0d011c -> :sswitch_0
    .end sparse-switch
.end method
