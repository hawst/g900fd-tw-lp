.class Lcom/samsung/musicplus/player/PlayerActivity$10;
.super Landroid/content/BroadcastReceiver;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerActivity;)V
    .locals 0

    .prologue
    .line 1101
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerActivity$10;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1104
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mEasyModeReceiver - isEasyMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity$10;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity$10;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->refreshEasyModeView()V

    .line 1110
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity$10;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerActivity;->invalidateOptionsMenu()V

    .line 1111
    return-void
.end method
