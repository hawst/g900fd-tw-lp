.class public Lcom/samsung/musicplus/player/SetAsActivity;
.super Landroid/app/Activity;
.source "SetAsActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;
    }
.end annotation


# static fields
.field private static final ALARM_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.clockpackage"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final COTACT_PACKAGE_NAME:Ljava/lang/String; = "com.android.contacts"

.field private static final PHONE_PACKAGE_NAME:Ljava/lang/String; = "com.android.phone"

.field public static final SHOW_PROGRESS:I = 0x0

.field public static final SIMSLOT1:I = 0x0

.field public static final SIMSLOT2:I = 0x1

.field private static final SIM_SLOT:Ljava/lang/String; = "gsm.sim.state"

.field private static final SIM_SLOT_1:Ljava/lang/String; = "gsm.sim.state"

.field private static final SIM_SLOT_2:Ljava/lang/String; = "gsm.sim.state2"

.field private static final SIM_SLOT_DSDS:Ljava/lang/String; = "gsm.sim.state_1"

.field private static final SIM_SLOT_NUM:Ljava/lang/String; = "ril.MSIMM"

.field private static final SIM_STATE_ABSENT:Ljava/lang/String; = "ABSENT"

.field private static final SIM_STATE_READY:Ljava/lang/String; = "READY"

.field public static final UPDATE_PROGRESS:I = 0x1


# instance fields
.field private final DISABLE_OPACITY:F

.field private mAlarmtoneRadioButton:Landroid/widget/RadioButton;

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

.field private mCustomDone:Landroid/widget/TextView;

.field private mFilePath:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mIsError:Z

.field private mIsMultiSIMSetAsMode:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mNormalRadioButton:Landroid/widget/RadioButton;

.field private mOffset:I

.field private mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressInfoView:Landroid/view/View;

.field private mRecommendRadioButton:Landroid/widget/RadioButton;

.field private mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

.field private final mResultListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

.field private mSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private mUri:Landroid/net/Uri;

.field private simCardDialog:Landroid/app/AlertDialog$Builder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/samsung/musicplus/player/SetAsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    .line 99
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    .line 133
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsError:Z

    .line 299
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->DISABLE_OPACITY:F

    .line 771
    new-instance v0, Lcom/samsung/musicplus/player/SetAsActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/SetAsActivity$5;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mResultListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    .line 790
    new-instance v0, Lcom/samsung/musicplus/player/SetAsActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/SetAsActivity$6;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 809
    new-instance v0, Lcom/samsung/musicplus/player/SetAsActivity$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/SetAsActivity$7;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mHandler:Landroid/os/Handler;

    .line 910
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->updateProgressInfoView()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->handleMenuOk()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->setDataSource()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->setProgressView()V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->play()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/player/SetAsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/musicplus/player/SetAsActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;
    .param p1, "x1"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    return p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/SetAsActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private checkRadioButtonState()Z
    .locals 1

    .prologue
    .line 538
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsError:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private doSetAs()V
    .locals 9

    .prologue
    const v8, 0x7f100024

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 543
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v4, "doSetAs()"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    iput-boolean v7, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    .line 545
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 552
    const-string v3, "gsm.sim.state"

    const-string v4, "ABSENT"

    invoke-static {v3, v7, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->getSystemProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 553
    .local v1, "sim1State":Ljava/lang/String;
    const-string v3, "gsm.sim.state"

    const-string v4, "ABSENT"

    invoke-static {v3, v6, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->getSystemProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 554
    .local v2, "sim2State":Ljava/lang/String;
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSetAs() - sim1State="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sim2State="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string v3, "READY"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "READY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 556
    iput-boolean v6, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    .line 557
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->showChooseSimCardDialog()V

    .line 617
    .end local v1    # "sim1State":Ljava/lang/String;
    .end local v2    # "sim2State":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 561
    .restart local v1    # "sim1State":Ljava/lang/String;
    .restart local v2    # "sim2State":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/player/SetAsActivity;->isSIMInsertedOnlyInSlot2()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "READY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 562
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-static {p0, v3, v4}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 563
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 564
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "recommendation_time_2"

    iget v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 572
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "DEBUG_RINGTONE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MusicPlayer : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 574
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSetAs() - PhoneRingtone - recommendation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v5}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mUri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mOffset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const v3, 0x7f10013f

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/player/SetAsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 567
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-static {p0, v6, v3}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 568
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 569
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "recommendation_time"

    iget v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 579
    .end local v1    # "sim1State":Ljava/lang/String;
    .end local v2    # "sim2State":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 580
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 581
    .local v0, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 582
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "highlight_offset"

    iget v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    .line 586
    :cond_6
    const-string v3, "ringtone_uri"

    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const-string v3, "vnd.android.cursor.item/contact"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 590
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/SetAsActivity;->startActivity(Landroid/content/Intent;)V

    .line 595
    :goto_2
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSetAs() - CallerRingtone - recommendation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v5}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mUri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mOffset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 592
    :cond_7
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v4, "doSetAs() - Not found contact application"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 597
    .end local v0    # "i":Landroid/content/Intent;
    :cond_8
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 599
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    const-string v4, "alarm://com.sec.android.app.clockpackage/alarmlist/"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 602
    .restart local v0    # "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 603
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "highlight_offset"

    iget v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    .line 606
    :cond_9
    const-string v3, "alarm_uri"

    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 607
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 608
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/SetAsActivity;->startActivity(Landroid/content/Intent;)V

    .line 613
    :goto_3
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSetAs() - AlarmTone - recommendation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v5}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mUri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 610
    :cond_a
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v4, "doSetAs() - Not found contact application"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_3
.end method

.method private getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 261
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 263
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_0

    .line 264
    :try_start_0
    invoke-virtual {v3, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 275
    :cond_0
    :goto_0
    return-object v2

    .line 266
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "com.sec.android.app.clockpackage"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 270
    const v4, 0x7f0d01ab

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 271
    .local v0, "alarmTone":Landroid/view/View;
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 273
    .end local v0    # "alarmTone":Landroid/view/View;
    :cond_1
    sget-object v4, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getAppIcon() - not found exception!! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getMaxVolumeRatio()F
    .locals 2

    .prologue
    .line 374
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->isMidi()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 375
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathEarjack()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->getMidiVolumeRatio(Z)F

    move-result v0

    .line 379
    .local v0, "volume":F
    :goto_0
    return v0

    .line 377
    .end local v0    # "volume":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .restart local v0    # "volume":F
    goto :goto_0
.end method

.method private getMidiVolumeRatio(Z)F
    .locals 4
    .param p1, "isEarJackConnected"    # Z

    .prologue
    .line 384
    if-eqz p1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getMidiHeadsetVolume()F

    move-result v0

    .line 389
    .local v0, "volume":F
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMidiVolume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isEarJackConnected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    return v0

    .line 387
    .end local v0    # "volume":F
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getMidiSpeakerVolume()F

    move-result v0

    .restart local v0    # "volume":F
    goto :goto_0
.end method

.method private getSongInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 681
    new-instance v7, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;

    invoke-direct {v7, v0}, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;-><init>(Lcom/samsung/musicplus/player/SetAsActivity$1;)V

    .line 682
    .local v7, "info":Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;
    const/4 v6, 0x0

    .line 684
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "title"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "mime_type"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 687
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;->filePath:Ljava/lang/String;

    .line 689
    const-string v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;->title:Ljava/lang/String;

    .line 690
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;->mimeType:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    :goto_0
    if-eqz v6, :cond_0

    .line 698
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 701
    :cond_0
    return-object v7

    .line 692
    :cond_1
    const v0, 0x7f1001b1

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 694
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 697
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 698
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private static getSystemProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "slot"    # I
    .param p2, "defaultVal"    # Ljava/lang/String;

    .prologue
    .line 945
    const/4 v2, 0x0

    .line 946
    .local v2, "propVal":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/telephony/MultiSimManager;->getSubId(I)[J

    move-result-object v3

    .line 947
    .local v3, "subId":[J
    if-eqz v3, :cond_0

    .line 948
    const/4 v5, 0x0

    aget-wide v6, v3, v5

    invoke-static {v6, v7}, Lcom/samsung/android/telephony/MultiSimManager;->getPhoneId(J)I

    move-result v0

    .line 949
    .local v0, "phoneId":I
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 950
    .local v1, "prop":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 951
    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 952
    .local v4, "values":[Ljava/lang/String;
    if-ltz v0, :cond_0

    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget-object v5, v4, v0

    if-eqz v5, :cond_0

    .line 953
    aget-object v2, v4, v0

    .line 957
    .end local v0    # "phoneId":I
    .end local v1    # "prop":Ljava/lang/String;
    .end local v4    # "values":[Ljava/lang/String;
    :cond_0
    if-nez v2, :cond_1

    .end local p2    # "defaultVal":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "defaultVal":Ljava/lang/String;
    :cond_1
    move-object p2, v2

    goto :goto_0
.end method

.method private getTimeString(J)Ljava/lang/String;
    .locals 9
    .param p1, "msec"    # J

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 880
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gtz v3, :cond_0

    .line 881
    const/4 v2, 0x0

    .line 882
    .local v2, "sec":I
    const/4 v1, 0x0

    .line 883
    .local v1, "min":I
    const/4 v0, 0x0

    .line 898
    .local v0, "hr":I
    :goto_0
    if-nez v0, :cond_2

    .line 899
    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 901
    :goto_1
    return-object v3

    .line 886
    .end local v0    # "hr":I
    .end local v1    # "min":I
    .end local v2    # "sec":I
    :cond_0
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v2, v4

    .line 887
    .restart local v2    # "sec":I
    if-nez v2, :cond_1

    .line 888
    const/4 v1, 0x0

    .line 889
    .restart local v1    # "min":I
    const/4 v0, 0x0

    .line 893
    :goto_2
    rem-int/lit8 v2, v2, 0x3c

    .line 894
    div-int/lit8 v0, v1, 0x3c

    .line 895
    .restart local v0    # "hr":I
    rem-int/lit8 v1, v1, 0x3c

    goto :goto_0

    .line 891
    .end local v0    # "hr":I
    .end local v1    # "min":I
    :cond_1
    div-int/lit8 v1, v2, 0x3c

    .restart local v1    # "min":I
    goto :goto_2

    .line 901
    .restart local v0    # "hr":I
    :cond_2
    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private handleMenuOk()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 346
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 347
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 348
    const v3, 0x7f1001b1

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/player/SetAsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 350
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->finish()V

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 355
    .local v2, "values":Landroid/content/ContentValues;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 356
    :cond_2
    const-string v3, "is_ringtone"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 365
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 366
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->doSetAs()V

    .line 367
    iget-boolean v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    if-nez v3, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->finish()V

    goto :goto_0

    .line 357
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 358
    const-string v3, "is_alarm"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalArgumentException occured :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 362
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 363
    .local v0, "ex":Ljava/lang/UnsupportedOperationException;
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UnsupportedOperationException occured :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private isMidi()Z
    .locals 6

    .prologue
    .line 394
    iget-object v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/player/SetAsActivity;->getSongInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;

    move-result-object v1

    .line 396
    .local v1, "info":Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;
    const/4 v2, 0x0

    .line 397
    .local v2, "isMidi":Z
    iget-object v3, v1, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;->mimeType:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/MediaFileCompat;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 398
    .local v0, "fileType":I
    const/16 v3, 0xe

    if-eq v0, v3, :cond_0

    const/16 v3, 0xf

    if-eq v0, v3, :cond_0

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 401
    :cond_0
    const/4 v2, 0x1

    .line 403
    :cond_1
    sget-object v3, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isMidi() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " fileType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return v2
.end method

.method private static isSIMInsertedOnlyInSlot2()Z
    .locals 3

    .prologue
    .line 620
    const-string v2, "ril.MSIMM"

    invoke-static {v2}, Lcom/samsung/musicplus/library/os/SystemPropertiesCompat;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 621
    .local v1, "simm":Ljava/lang/String;
    const/4 v0, 0x1

    .line 622
    .local v0, "onlyInSlot2":Z
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 623
    const/4 v0, 0x1

    .line 627
    :goto_0
    return v0

    .line 625
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private play()V
    .locals 3

    .prologue
    .line 705
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYING_MUSIC_DURING_CALL:Z

    if-nez v1, :cond_0

    .line 706
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 707
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 708
    const v1, 0x7f1001b3

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 715
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    return-void

    .line 713
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->requestAudioFocus()V

    .line 714
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method

.method private releaseRecommander()V
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->close()Z

    .line 760
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    .line 762
    :cond_0
    return-void
.end method

.method private requestAudioFocus()V
    .locals 4

    .prologue
    .line 765
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-nez v0, :cond_0

    .line 767
    sget-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "requestAudioFocus fail. it may be during call"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :cond_0
    return-void
.end method

.method private setAppIconImage()V
    .locals 5

    .prologue
    .line 248
    const v4, 0x7f0d01a5

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 249
    .local v3, "phone":Landroid/widget/ImageView;
    const-string v4, "com.android.phone"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 250
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 252
    const v4, 0x7f0d01a9

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 253
    .local v1, "contact":Landroid/widget/ImageView;
    const-string v4, "com.android.contacts"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 255
    const v4, 0x7f0d01ad

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 256
    .local v0, "alarm":Landroid/widget/ImageView;
    const-string v4, "com.sec.android.app.clockpackage"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/player/SetAsActivity;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 257
    return-void
.end method

.method private setCustomCancelDone()V
    .locals 4

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 316
    .local v1, "customView":Landroid/view/View;
    const v2, 0x7f0d0043

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    .line 317
    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 318
    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 319
    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    new-instance v3, Lcom/samsung/musicplus/player/SetAsActivity$2;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/player/SetAsActivity$2;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    const v2, 0x7f0d0042

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 328
    .local v0, "customCancel":Landroid/widget/TextView;
    new-instance v2, Lcom/samsung/musicplus/player/SetAsActivity$3;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/player/SetAsActivity$3;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    return-void
.end method

.method private setDataSource()V
    .locals 3

    .prologue
    .line 718
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 720
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 721
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 722
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 723
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 731
    :goto_0
    return-void

    .line 724
    :catch_0
    move-exception v0

    .line 725
    .local v0, "e":Ljava/io/IOException;
    const v1, 0x7f100124

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 728
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsError:Z

    .line 729
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->finish()V

    goto :goto_0
.end method

.method private setProgressView()V
    .locals 18

    .prologue
    .line 833
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v14, :cond_0

    .line 875
    :goto_0
    return-void

    .line 837
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 838
    const v14, 0x7f0d0194

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 839
    .local v8, "setAsActivity":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v15, 0x7f0d00c3

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 840
    .local v7, "progress":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v15, 0x7f0d01a0

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 842
    .local v9, "startingTimeText":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v15, 0x7f0d019e

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 843
    .local v5, "offsetTimeText":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v15, 0x7f0d01a1

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 846
    .local v4, "durationTimeText":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    int-to-float v14, v14

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v15

    int-to-float v15, v15

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v15}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v15

    int-to-float v15, v15

    div-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 850
    .local v6, "paddingValue":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 852
    .local v3, "context":Landroid/content/Context;
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v6, v14, v15, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 853
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/musicplus/player/SetAsActivity;->getTimeString(J)Ljava/lang/String;

    move-result-object v13

    .line 854
    .local v13, "timeStart":Ljava/lang/String;
    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 855
    invoke-static {v3, v13}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 857
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    int-to-long v14, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/musicplus/player/SetAsActivity;->getTimeString(J)Ljava/lang/String;

    move-result-object v11

    .line 858
    .local v11, "timeOffset":Ljava/lang/String;
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 859
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 860
    .local v2, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v14

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v15

    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/lang/CharSequence;->length()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v14, v15, v0, v1, v2}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 861
    invoke-static {v3, v11}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 863
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    sub-int v12, v6, v14

    .line 864
    .local v12, "timeOffsetPadding":I
    if-gez v12, :cond_1

    .line 865
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v14, v15, v0, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 872
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v14}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v14

    int-to-long v14, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/musicplus/player/SetAsActivity;->getTimeString(J)Ljava/lang/String;

    move-result-object v10

    .line 873
    .local v10, "timeDuration":Ljava/lang/String;
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 874
    invoke-static {v3, v10}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 866
    .end local v10    # "timeDuration":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v14

    add-int/2addr v14, v6

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v15

    if-le v14, v15, :cond_2

    .line 867
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v14

    sub-int v14, v6, v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v14, v15, v0, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 869
    :cond_2
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v5, v12, v14, v15, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method private showChooseSimCardDialog()V
    .locals 6

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "select_name_1"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 632
    .local v2, "strSim1":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "select_name_2"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 634
    .local v3, "strSim2":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v1, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v4, 0x1

    aput-object v3, v1, v4

    .line 638
    .local v1, "items":[Ljava/lang/CharSequence;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    .line 639
    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    const v5, 0x7f100153

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 640
    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    new-instance v5, Lcom/samsung/musicplus/player/SetAsActivity$4;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/player/SetAsActivity$4;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    invoke-virtual {v4, v1, v5}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 676
    iget-object v4, p0, Lcom/samsung/musicplus/player/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 677
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 678
    return-void
.end method

.method private stopMediaPlayer()V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 752
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 753
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 755
    :cond_0
    return-void
.end method

.method private updateDoneButtonState()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 302
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 312
    :goto_0
    return-void

    .line 305
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->checkRadioButtonState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 307
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method

.method private updateProgressInfoView()V
    .locals 4

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 245
    :cond_0
    return-void
.end method

.method private updateRadioButon(Landroid/widget/RadioButton;)V
    .locals 3
    .param p1, "selectedRadioButton"    # Landroid/widget/RadioButton;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 515
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_3

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 518
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 527
    :cond_1
    :goto_0
    invoke-virtual {p1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 529
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->checkRadioButtonState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 530
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 531
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 532
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCustomDone:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 535
    :cond_2
    return-void

    .line 519
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_1

    .line 522
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 523
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 524
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 409
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 504
    :cond_0
    :goto_0
    return-void

    .line 411
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 414
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 427
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    if-eqz v1, :cond_0

    .line 428
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 429
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 416
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 417
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->setDataSource()V

    .line 419
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getMaxVolumeRatio()F

    move-result v0

    .line 420
    .local v0, "maxVolume":F
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 424
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->play()V

    goto :goto_1

    .line 433
    .end local v0    # "maxVolume":F
    :sswitch_1
    iget v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    if-lez v1, :cond_4

    .line 435
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 438
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 474
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    if-eqz v1, :cond_0

    .line 475
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 476
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 440
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 441
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->setDataSource()V

    .line 442
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 443
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->play()V

    goto :goto_2

    .line 447
    :cond_4
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    if-nez v1, :cond_5

    .line 448
    new-instance v1, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    .line 449
    :cond_5
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    .line 450
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->open(Ljava/lang/String;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 466
    :pswitch_0
    const v1, 0x7f100080

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 469
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsError:Z

    .line 470
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->finish()V

    goto :goto_2

    .line 452
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommender:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mResultListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->doExtract(Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;)Z

    .line 453
    const/4 v1, 0x0

    const v2, 0x7f10012a

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/player/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {p0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_2

    .line 457
    :pswitch_2
    const v1, 0x7f10007e

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 460
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsError:Z

    .line 461
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->finish()V

    goto :goto_2

    .line 481
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eqz v1, :cond_0

    .line 482
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 483
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    .line 488
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eqz v1, :cond_0

    .line 489
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 490
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    .line 495
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    if-eqz v1, :cond_0

    .line 496
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 497
    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    .line 409
    :sswitch_data_0
    .sparse-switch
        0x7f0d0027 -> :sswitch_0
        0x7f0d0198 -> :sswitch_1
        0x7f0d0199 -> :sswitch_2
        0x7f0d01a7 -> :sswitch_3
        0x7f0d01ab -> :sswitch_4
    .end sparse-switch

    .line 450
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 907
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 908
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 339
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 340
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 343
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    sget-object v9, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v10, "onCreate()"

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const v9, 0x7f040088

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->setContentView(I)V

    .line 141
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->setCustomCancelDone()V

    .line 143
    const-string v9, "audio"

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/media/AudioManager;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 144
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 146
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 147
    .local v3, "data":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 148
    const-string v9, "extra_uri"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    .line 149
    iget-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    if-nez v9, :cond_0

    .line 150
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string v10, "Did you miss MusicIntent.EXTRA_URI as extra value?"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 154
    :cond_0
    iget-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->getSongInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;

    move-result-object v4

    .line 155
    .local v4, "info":Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;
    iget-object v9, v4, Lcom/samsung/musicplus/player/SetAsActivity$SongInfo;->filePath:Ljava/lang/String;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mFilePath:Ljava/lang/String;

    .line 157
    const v9, 0x7f0d0199

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 158
    .local v6, "phoneRingtone":Landroid/view/View;
    const v9, 0x7f0d01a7

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 159
    .local v1, "callerRingtone":Landroid/view/View;
    const v9, 0x7f0d01ab

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 160
    .local v0, "alarmTone":Landroid/view/View;
    const v9, 0x7f0d0027

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 161
    .local v5, "normal":Landroid/view/View;
    const v9, 0x7f0d0198

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 162
    .local v7, "recommend":Landroid/view/View;
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->setAppIconImage()V

    .line 165
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 166
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 168
    const v9, 0x7f0d01a3

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 171
    const v9, 0x7f0d01a3

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f1000bc

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f10018b

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 187
    .local v2, "context":Landroid/content/Context;
    invoke-static {v2, v5}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 188
    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 189
    invoke-static {v2, v6}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 190
    invoke-static {v2, v1}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 191
    invoke-static {v2, v0}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 193
    const v9, 0x7f0d0195

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    .line 194
    const v9, 0x7f0d019a

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    .line 195
    const v9, 0x7f0d01a4

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    .line 196
    const v9, 0x7f0d01a8

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    .line 197
    const v9, 0x7f0d01ac

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    .line 199
    const-string v9, "phone"

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 200
    .local v8, "telephony":Landroid/telephony/TelephonyManager;
    invoke-static {v8}, Lcom/samsung/musicplus/library/telephony/TelephonyManagerCompat;->isVoiceCapable(Landroid/telephony/TelephonyManager;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 201
    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 202
    const/16 v9, 0x8

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 206
    :cond_1
    const v9, 0x7f0d019d

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/player/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    .line 208
    if-eqz p1, :cond_2

    .line 209
    const-string v9, "offset"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    .line 212
    :cond_2
    new-instance v9, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v9, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 213
    iget-object v9, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    new-instance v10, Lcom/samsung/musicplus/player/SetAsActivity$1;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/player/SetAsActivity$1;-><init>(Lcom/samsung/musicplus/player/SetAsActivity;)V

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 230
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 292
    sget-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->releaseRecommander()V

    .line 294
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 295
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 296
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 297
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 941
    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 280
    sget-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->releaseRecommander()V

    .line 282
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 286
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V

    .line 287
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 288
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 235
    sget-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mIsError:Z

    .line 237
    invoke-direct {p0}, Lcom/samsung/musicplus/player/SetAsActivity;->updateDoneButtonState()V

    .line 238
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 508
    sget-object v0, Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v0, "offset"

    iget v1, p0, Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 510
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 511
    return-void
.end method
