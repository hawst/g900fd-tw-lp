.class public Lcom/samsung/musicplus/player/NowPlayingListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "NowPlayingListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;
.implements Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;


# static fields
.field private static final DEBUG:Z = false

.field private static final ENABLE_REORDER:Z = false

.field public static final FRAGMENT_TAG:Ljava/lang/String; = "now_playing_list"

.field private static final LIST_ANIMATION_DURATION:I = 0x64

.field private static final USE_SERVICE_LIST:Z = true


# instance fields
.field private mCountView:Landroid/widget/TextView;

.field private mListUpdateHandler:Landroid/os/Handler;

.field private mListUpdater:Landroid/os/Handler;

.field private mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

.field private mMoveToCurrentSong:Z

.field private final mQueueReceiver:Landroid/content/BroadcastReceiver;

.field private mTotalView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mMoveToCurrentSong:Z

    .line 102
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment$1;-><init>(Lcom/samsung/musicplus/player/NowPlayingListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mQueueReceiver:Landroid/content/BroadcastReceiver;

    .line 267
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment$2;-><init>(Lcom/samsung/musicplus/player/NowPlayingListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mListUpdater:Landroid/os/Handler;

    .line 410
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment$3;-><init>(Lcom/samsung/musicplus/player/NowPlayingListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mListUpdateHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/player/NowPlayingListFragment;)Lcom/samsung/musicplus/player/NowPlayingCursorLoader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingListFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/player/NowPlayingListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingListFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->updateListWhenQueryComplete()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/player/NowPlayingListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingListFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->updateTotalCount()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/player/NowPlayingListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingListFragment;

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->invalidateAllViews()V

    return-void
.end method

.method private disableAddToFavorite(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 373
    const v1, 0x7f0d01d2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 374
    .local v0, "addToFavorite":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 375
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 377
    :cond_0
    return-void
.end method

.method private disableAddToPlaylistOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 352
    const v1, 0x7f0d01cd

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 353
    .local v0, "addToPlaylist":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 354
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 356
    :cond_0
    return-void
.end method

.method private disableDeleteOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 359
    const v1, 0x7f0d01d0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 360
    .local v0, "delete":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 361
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 363
    :cond_0
    return-void
.end method

.method private disableRemoveOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 366
    const v1, 0x7f0d01cf

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 367
    .local v0, "remove":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 368
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 370
    :cond_0
    return-void
.end method

.method private disableSetAsOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 380
    const v1, 0x7f0d01a3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 381
    .local v0, "setAs":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 382
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 384
    :cond_0
    return-void
.end method

.method private ensureHeaderView(Landroid/app/Activity;)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mTotalView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 307
    const v0, 0x7f0d0061

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mTotalView:Landroid/widget/TextView;

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mCountView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 310
    const v0, 0x7f0d0046

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mCountView:Landroid/widget/TextView;

    .line 312
    :cond_1
    return-void
.end method

.method public static getInstance(ILjava/lang/String;)Landroid/app/Fragment;
    .locals 4
    .param p0, "listType"    # I
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v1, Lcom/samsung/musicplus/player/NowPlayingListFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;-><init>()V

    .line 114
    .local v1, "fg":Landroid/app/Fragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 115
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    const/4 v3, -0x1

    if-eq p0, v3, :cond_0

    .end local p0    # "listType":I
    :goto_0
    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v2, "key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 119
    return-object v1

    .line 115
    .restart local p0    # "listType":I
    :cond_0
    const p0, 0x20001

    goto :goto_0
.end method

.method private setCurrentPosition()V
    .locals 3

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 459
    .local v1, "lv":Landroid/widget/ListView;
    if-eqz v1, :cond_0

    .line 460
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v0

    .line 461
    .local v0, "current":I
    if-ltz v0, :cond_0

    .line 462
    new-instance v2, Lcom/samsung/musicplus/player/NowPlayingListFragment$4;

    invoke-direct {v2, p0, v1, v0}, Lcom/samsung/musicplus/player/NowPlayingListFragment$4;-><init>(Lcom/samsung/musicplus/player/NowPlayingListFragment;Landroid/widget/ListView;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 473
    .end local v0    # "current":I
    :cond_0
    return-void
.end method

.method private updateListWhenQueryComplete()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 279
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    const-string v0, "MusicUiList"

    const-string v1, "Already detached so ignore it"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->setListShown(Z)V

    .line 290
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 291
    iget v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mNoItemTextId:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->setEmptyView(I)V

    goto :goto_0

    .line 287
    :cond_2
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->setListShownNoAnimation(Z)V

    goto :goto_1

    .line 293
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mMoveToCurrentSong:Z

    if-eqz v0, :cond_0

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mMoveToCurrentSong:Z

    .line 295
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->setCurrentPosition()V

    goto :goto_0
.end method

.method private updateTotalCount()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 421
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 422
    .local v0, "a":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->ensureHeaderView(Landroid/app/Activity;)V

    .line 429
    iget-object v5, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mTotalView:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mCountView:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 436
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v4

    .line 437
    .local v4, "songCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 441
    .local v3, "sb":Ljava/lang/StringBuilder;
    if-eqz v4, :cond_2

    .line 442
    const-string v5, "%d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 443
    .local v1, "front":Ljava/lang/String;
    const-string v5, "%d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 444
    .local v2, "rear":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    const-string v5, " / "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    .end local v1    # "front":Ljava/lang/String;
    .end local v2    # "rear":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mTotalView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f000a

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v4, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    iget-object v5, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mCountView:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 5

    .prologue
    .line 207
    new-instance v0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f040038

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v0
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 322
    new-instance v0, Lcom/samsung/musicplus/contents/menu/NowPlayingListMenus;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/menu/NowPlayingListMenus;-><init>()V

    return-object v0
.end method

.method public drop(II)V
    .locals 3
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 139
    .local v1, "lv":Landroid/widget/ListView;
    instance-of v2, v1, Lcom/samsung/musicplus/widget/list/ReorderListView;

    if-eqz v2, :cond_0

    .line 140
    const/4 v2, -0x1

    if-le p1, v2, :cond_1

    if-eq p1, p2, :cond_1

    .line 143
    iget-object v2, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/player/NowPlayingCursor;

    .line 144
    .local v0, "c":Lcom/samsung/musicplus/player/NowPlayingCursor;
    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/player/NowPlayingCursor;->moveItem(II)V

    .line 145
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->notifyDataSetChanged()V

    .line 146
    invoke-virtual {v1}, Landroid/widget/ListView;->invalidateViews()V

    .line 150
    check-cast v1, Lcom/samsung/musicplus/widget/list/ReorderListView;

    .end local v1    # "lv":Landroid/widget/ListView;
    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/ReorderListView;->removeDragView()V

    .line 155
    .end local v0    # "c":Lcom/samsung/musicplus/player/NowPlayingCursor;
    :cond_0
    :goto_0
    return-void

    .line 152
    .restart local v1    # "lv":Landroid/widget/ListView;
    :cond_1
    check-cast v1, Lcom/samsung/musicplus/widget/list/ReorderListView;

    .end local v1    # "lv":Landroid/widget/ListView;
    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/ReorderListView;->removeDragView()V

    goto :goto_0
.end method

.method public getList()I
    .locals 1

    .prologue
    .line 317
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    return v0
.end method

.method protected initIndexView(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 303
    return-void
.end method

.method protected initLoader()V
    .locals 3

    .prologue
    .line 213
    invoke-static {}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getNowPlayingArgs()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 214
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-direct {v0, v1, v2, p0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;-><init>(Landroid/content/ContentResolver;Lcom/samsung/musicplus/service/IPlayerService;Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .line 219
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 133
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 327
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 328
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 159
    const v0, 0x7f04001c

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 202
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroy()V

    .line 203
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mListUpdater:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->release()V

    .line 195
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 197
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroyView()V

    .line 198
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 1
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 223
    const/4 v0, 0x1

    invoke-static {p3, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setQueuePosition(IZ)V

    .line 224
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->updateTotalCount()V

    .line 232
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 233
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 235
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->updateListWhenQueryComplete()V

    .line 238
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mText1Idx:I

    .line 239
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onNowPlayingQueryComplete(Lcom/samsung/musicplus/player/NowPlayingCursor;)V
    .locals 4
    .param p1, "c"    # Lcom/samsung/musicplus/player/NowPlayingCursor;

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    invoke-virtual {p1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->close()V

    .line 265
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->updateTotalCount()V

    .line 258
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 259
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 264
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mListUpdater:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 173
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-eqz v1, :cond_0

    .line 174
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 175
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 176
    const-string v1, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mQueueReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 179
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStart()V

    .line 180
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mQueueReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 187
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStop()V

    .line 188
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 164
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 166
    const v0, 0x7f0d008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 168
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f02009e

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(I)V

    .line 169
    return-void
.end method

.method protected prepareContextMenu(Landroid/view/Menu;JI)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableDeleteOption(Landroid/view/Menu;)V

    .line 337
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDlnaDmsList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    const v1, 0x2000d

    if-ne v0, v1, :cond_2

    .line 340
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableDeleteOption(Landroid/view/Menu;)V

    .line 341
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableAddToFavorite(Landroid/view/Menu;)V

    .line 342
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableSetAsOption(Landroid/view/Menu;)V

    .line 343
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableAddToPlaylistOption(Landroid/view/Menu;)V

    .line 349
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableSetAsOption(Landroid/view/Menu;)V

    goto :goto_0

    .line 346
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/samsung/musicplus/util/UiUtils;->isPersonalModeSong(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->disableSetAsOption(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public updateNowPlayingTitle(Landroid/content/Intent;)V
    .locals 5
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 394
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mListUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 401
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingListFragment;->mListUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 403
    :cond_1
    return-void
.end method
