.class Lcom/samsung/musicplus/player/FullPlayerView$19;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView;->initSmartClip(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 2354
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$19;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExtractSmartClipData(Landroid/view/View;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "element"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;
    .param p3, "croppedArea"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;

    .prologue
    .line 2360
    const/4 v1, 0x0

    .line 2361
    .local v1, "matched":Z
    const/4 v0, 0x1

    .line 2363
    .local v0, "enableClipData":Z
    instance-of v3, p1, Lcom/samsung/musicplus/library/metadata/ISmartClipView;

    if-eqz v3, :cond_1

    .line 2364
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .local v2, "r":Landroid/graphics/Rect;
    move-object v3, p1

    .line 2365
    check-cast v3, Lcom/samsung/musicplus/library/metadata/ISmartClipView;

    invoke-interface {v3, v2}, Lcom/samsung/musicplus/library/metadata/ISmartClipView;->getRealViewArea(Landroid/graphics/Rect;)V

    move-object v3, p1

    .line 2366
    check-cast v3, Lcom/samsung/musicplus/library/metadata/ISmartClipView;

    invoke-interface {v3}, Lcom/samsung/musicplus/library/metadata/ISmartClipView;->enableMetaDataClip()Z

    move-result v0

    .line 2367
    invoke-interface {p3, v2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->intersects(Landroid/graphics/Rect;)Z

    move-result v1

    .line 2372
    .end local v2    # "r":Landroid/graphics/Rect;
    :goto_0
    if-eqz v1, :cond_2

    .line 2373
    if-eqz v0, :cond_0

    .line 2374
    invoke-static {p1, p2, p3}, Lcom/samsung/android/smartclip/SmartClipMetaUtils;->extractDefaultSmartClipData(Landroid/view/View;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I

    .line 2375
    new-instance v3, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    const-string v4, "file_path_audio"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 2377
    new-instance v3, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    const-string v4, "title"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 2380
    :cond_0
    const/4 v3, 0x1

    .line 2382
    :goto_1
    return v3

    .line 2369
    :cond_1
    invoke-interface {p3, p1}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->intersects(Landroid/view/View;)Z

    move-result v1

    goto :goto_0

    .line 2382
    :cond_2
    const/16 v3, 0x100

    goto :goto_1
.end method
