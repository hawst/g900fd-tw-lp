.class Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomSeekBarListener"
.end annotation


# static fields
.field private static final SKIP_INTERVAL:I = 0x4


# instance fields
.field private mKeyPressed:Z

.field private mProgressPoint:I

.field private mStartTracking:Z

.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 821
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823
    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    .line 825
    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mStartTracking:Z

    .line 861
    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    return-void
.end method

.method private seek()V
    .locals 4

    .prologue
    .line 857
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$900(Lcom/samsung/musicplus/player/PlayerView;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->seek(J)J

    .line 858
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    const-wide/16 v2, -0x1

    # setter for: Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/player/PlayerView;->access$902(Lcom/samsung/musicplus/player/PlayerView;J)J

    .line 859
    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 867
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 869
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 881
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    .line 882
    iput v2, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    .line 885
    :cond_0
    :goto_0
    return v2

    .line 871
    :pswitch_0
    const/16 v1, 0x15

    if-ne p2, v1, :cond_1

    .line 872
    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    .line 873
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    goto :goto_0

    .line 874
    :cond_1
    const/16 v1, 0x16

    if-ne p2, v1, :cond_0

    .line 875
    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    add-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    .line 876
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    goto :goto_0

    .line 869
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 835
    if-nez p3, :cond_1

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 838
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mStartTracking:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    if-eqz v0, :cond_3

    .line 839
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mDuration:J
    invoke-static {v1}, Lcom/samsung/musicplus/player/PlayerView;->access$1000(Lcom/samsung/musicplus/player/PlayerView;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mScrubbingBar:Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;
    invoke-static {v1}, Lcom/samsung/musicplus/player/PlayerView;->access$1100(Lcom/samsung/musicplus/player/PlayerView;)Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    move-result-object v1

    iget v4, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    add-int/2addr v4, p2

    invoke-virtual {v1, v4}, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->reprocessProgress(I)I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    # setter for: Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/player/PlayerView;->access$902(Lcom/samsung/musicplus/player/PlayerView;J)J

    .line 841
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mProgressPoint:I

    .line 842
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerView;->refreshNow()J

    .line 844
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    if-eqz v0, :cond_0

    .line 845
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->seek()V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 829
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mStartTracking:Z

    .line 830
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mKeyPressed:Z

    .line 831
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 852
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->seek()V

    .line 853
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;->mStartTracking:Z

    .line 854
    return-void
.end method
