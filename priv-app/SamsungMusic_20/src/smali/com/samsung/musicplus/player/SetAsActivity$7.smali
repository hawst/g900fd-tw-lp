.class Lcom/samsung/musicplus/player/SetAsActivity$7;
.super Landroid/os/Handler;
.source "SetAsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/SetAsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/SetAsActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0

    .prologue
    .line 809
    iput-object p1, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 812
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 826
    :goto_0
    return-void

    .line 814
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # invokes: Lcom/samsung/musicplus/player/SetAsActivity;->stopMediaPlayer()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$1000(Lcom/samsung/musicplus/player/SetAsActivity;)V

    .line 815
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # invokes: Lcom/samsung/musicplus/player/SetAsActivity;->setDataSource()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$1100(Lcom/samsung/musicplus/player/SetAsActivity;)V

    .line 816
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$900(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I
    invoke-static {v1}, Lcom/samsung/musicplus/player/SetAsActivity;->access$400(Lcom/samsung/musicplus/player/SetAsActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 817
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # invokes: Lcom/samsung/musicplus/player/SetAsActivity;->setProgressView()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$1200(Lcom/samsung/musicplus/player/SetAsActivity;)V

    .line 818
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # invokes: Lcom/samsung/musicplus/player/SetAsActivity;->play()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$1300(Lcom/samsung/musicplus/player/SetAsActivity;)V

    goto :goto_0

    .line 821
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$7;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # invokes: Lcom/samsung/musicplus/player/SetAsActivity;->setProgressView()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$1200(Lcom/samsung/musicplus/player/SetAsActivity;)V

    goto :goto_0

    .line 812
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
