.class Lcom/samsung/musicplus/player/PlayerActivity$9;
.super Landroid/content/BroadcastReceiver;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerActivity;)V
    .locals 0

    .prologue
    .line 1079
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerActivity$9;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1082
    const-string v1, "state"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1083
    .local v0, "isHdmiConnected":Z
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity$9;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setHdmiConnected(Z)V

    .line 1084
    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHdmiReceiver() - isHdmiConnected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity$9;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/PlayerActivity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1086
    new-instance v1, Lcom/samsung/musicplus/dialog/NotiDialog;

    const v2, 0x7f100046

    const v3, 0x7f100045

    invoke-direct {v1, v2, v3}, Lcom/samsung/musicplus/dialog/NotiDialog;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity$9;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/PlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/dialog/NotiDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1088
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity$9;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateChangePlayerItem()V

    .line 1089
    return-void
.end method
