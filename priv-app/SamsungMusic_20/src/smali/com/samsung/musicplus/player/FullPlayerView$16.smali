.class Lcom/samsung/musicplus/player/FullPlayerView$16;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 2231
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$16;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2234
    .local p1, "parent":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$16;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastAdapter:Lcom/samsung/musicplus/player/PlayerSideCastAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v0

    .line 2236
    .local v0, "audioids":[J
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$16;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, v1, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x2000a

    const-string v3, "_id"

    invoke-static {v3, v0}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0, p3}, Lcom/samsung/musicplus/util/ServiceUtils;->openList(Landroid/content/Context;ILjava/lang/String;[JI)Z

    .line 2239
    return-void
.end method
