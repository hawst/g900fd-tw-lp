.class public Lcom/samsung/musicplus/player/PlayerActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "PlayerActivity.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# static fields
.field public static final LAUNCH_SET_AS:Ljava/lang/String; = "com.sec.android.music.intent.action.LAUNCH_SET_AS"

.field private static final MAX_STREAM_VOLUME:I = 0xf

.field private static final MIN_STREAM_VOLUME:I = 0x0

.field protected static final MSG_UPDATE_WAIT:I = 0x0

.field private static final SELECTED_ID:Ljava/lang/String; = "selected_id"

.field private static final TAG:Ljava/lang/String; = "MusicUi"


# instance fields
.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private final mAudioReceiver:Landroid/content/BroadcastReceiver;

.field private final mEasyModeReceiver:Landroid/content/BroadcastReceiver;

.field private mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

.field private final mGestureReceiver:Landroid/content/BroadcastReceiver;

.field private final mHandleVolumeKeyBlockHandler:Landroid/os/Handler;

.field private final mHdmiReceiver:Landroid/content/BroadcastReceiver;

.field private mIsGestureRegistered:Z

.field private mIsHandleVolumeKeyBlock:Z

.field private mIsVolumeMute:Z

.field private final mMainHandler:Landroid/os/Handler;

.field private mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

.field private mSelectedId:J

.field private final mSideSyncReceiver:Landroid/content/BroadcastReceiver;

.field private mVolumeUpdateBlock:Z

.field private final mVolumeUpdateBlockHandler:Landroid/os/Handler;

.field private mWfdStatusChangeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 261
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/player/PlayerActivity$1;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mMainHandler:Landroid/os/Handler;

    .line 276
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$2;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mWfdStatusChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 721
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlock:Z

    .line 723
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$4;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlockHandler:Landroid/os/Handler;

    .line 876
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsHandleVolumeKeyBlock:Z

    .line 878
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$5;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mHandleVolumeKeyBlockHandler:Landroid/os/Handler;

    .line 885
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsGestureRegistered:Z

    .line 956
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$6;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureReceiver:Landroid/content/BroadcastReceiver;

    .line 998
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsVolumeMute:Z

    .line 1000
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$7;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    .line 1058
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$8;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$8;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    .line 1079
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$9;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$9;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    .line 1101
    new-instance v0, Lcom/samsung/musicplus/player/PlayerActivity$10;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerActivity$10;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/player/PlayerActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/player/PlayerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlock:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/player/PlayerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlock:Z

    return p1
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/player/PlayerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsHandleVolumeKeyBlock:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/library/audio/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/player/PlayerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsVolumeMute:Z

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/musicplus/player/PlayerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsVolumeMute:Z

    return p1
.end method

.method private ensureGestureManager()V
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    if-eqz v0, :cond_0

    .line 910
    :goto_0
    return-void

    .line 909
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    goto :goto_0
.end method

.method private handleVolumeKey(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 847
    const/4 v0, 0x0

    .line 853
    .local v0, "consumed":Z
    iput-boolean v5, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlock:Z

    .line 854
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlockHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 855
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlockHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 857
    iget-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsHandleVolumeKeyBlock:Z

    if-eqz v1, :cond_0

    .line 858
    const/4 v0, 0x1

    .line 873
    :goto_0
    return v0

    .line 860
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 861
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->volumeUp()V

    .line 862
    const/4 v0, 0x1

    .line 870
    :cond_1
    :goto_1
    iput-boolean v5, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsHandleVolumeKeyBlock:Z

    .line 871
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mHandleVolumeKeyBlockHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 863
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 864
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->volumeDown()V

    .line 865
    const/4 v0, 0x1

    goto :goto_1

    .line 866
    :cond_3
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeMute(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 867
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleMute()V

    .line 868
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private initializeViews(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 694
    const v1, 0x7f0d0106

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 695
    .local v0, "root":Landroid/view/View;
    new-instance v1, Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {v1, p0, v0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V

    iput-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    .line 696
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setCurrentSongPosition(I)V

    .line 697
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateLyricsView()V

    .line 698
    return-void
.end method

.method private isEnableGesture()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 918
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

    if-eqz v3, :cond_0

    .line 923
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v3

    if-gt v3, v2, :cond_1

    .line 941
    :cond_0
    :goto_0
    return v1

    .line 927
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->hasWindowFocus()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 928
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 929
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->isEnableAirBrowse(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 930
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->isEnableAirBrowseOnNowPlayingScreen(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 931
    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isLockScreenOn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 934
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->ensureGestureManager()V

    move v1, v2

    .line 935
    goto :goto_0
.end method

.method private isSlinkList()Z
    .locals 2

    .prologue
    .line 392
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    const v1, 0x2000d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSupportedFileTypeForShareMusic()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 474
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_KOR_SKT:Z

    if-eqz v3, :cond_1

    .line 476
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v2

    .line 477
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_2

    const-string v3, ".dcf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 490
    .end local v2    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 480
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSamplingRate()I

    move-result v3

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getBitDepth()I

    move-result v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 485
    :cond_2
    const/4 v0, 0x0

    .line 486
    .local v0, "isSupport":Z
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 487
    .local v1, "mimeType":Ljava/lang/String;
    const-string v3, "audio/mpeg"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "audio/x-wav"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 488
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isVolumeDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 830
    const/4 v0, 0x0

    .line 831
    .local v0, "down":Z
    const/16 v1, 0x19

    if-eq p1, v1, :cond_0

    const/16 v1, 0x117

    if-ne p1, v1, :cond_2

    .line 832
    :cond_0
    const/4 v0, 0x1

    .line 839
    :cond_1
    :goto_0
    return v0

    .line 833
    :cond_2
    const/16 v1, 0xa9

    if-ne p1, v1, :cond_1

    .line 835
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v1

    const/16 v2, 0x222

    if-ne v1, v2, :cond_1

    .line 836
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isVolumeEvent(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 813
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeMute(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVolumeMute(I)Z
    .locals 1
    .param p1, "keyCode"    # I

    .prologue
    .line 843
    const/16 v0, 0x5b

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa4

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVolumeUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 817
    const/4 v0, 0x0

    .line 818
    .local v0, "up":Z
    const/16 v1, 0x18

    if-eq p1, v1, :cond_0

    const/16 v1, 0x118

    if-ne p1, v1, :cond_2

    .line 819
    :cond_0
    const/4 v0, 0x1

    .line 826
    :cond_1
    :goto_0
    return v0

    .line 820
    :cond_2
    const/16 v1, 0xa8

    if-ne p1, v1, :cond_1

    .line 822
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v1

    const/16 v2, 0x221

    if-ne v1, v2, :cond_1

    .line 823
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private launchGroupPlay()V
    .locals 6

    .prologue
    .line 669
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.groupcast.action.SEND_MUSIC_FOR_DJ_MODE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 670
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "audio/*"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 672
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 680
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 681
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 682
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->startActivity(Landroid/content/Intent;)V

    .line 691
    :goto_0
    return-void

    .line 684
    :cond_0
    const-string v3, "MusicUi"

    const-string v4, "GroupPlay - ActivityNotFoundException "

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f100024

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 688
    :catch_0
    move-exception v0

    .line 689
    .local v0, "e1":Ljava/lang/NullPointerException;
    const-string v3, "MusicUi"

    const-string v4, "GroupPlay - NullPointerException, uri is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private launchSetAs()V
    .locals 7

    .prologue
    .line 632
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentUri()Ljava/lang/String;

    move-result-object v3

    .line 633
    .local v3, "url":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 634
    const-string v4, "MusicUi"

    const-string v5, "launchSetAs - getCurrentUri == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    :goto_0
    return-void

    .line 637
    :cond_0
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 639
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 640
    const-string v4, "MusicUi"

    const-string v5, "launchSetAs - uri == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 649
    :cond_1
    sget-boolean v4, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v4, :cond_2

    .line 650
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/samsung/musicplus/player/SetAsActivity;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 655
    .local v1, "i":Landroid/content/Intent;
    :goto_1
    :try_start_0
    const-string v4, "extra_uri"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 656
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 657
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 662
    :catch_0
    move-exception v0

    .line 663
    .local v0, "e1":Ljava/lang/NullPointerException;
    const-string v4, "MusicUi"

    const-string v5, "SetAs - NullPointerException, uri is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 652
    .end local v0    # "e1":Ljava/lang/NullPointerException;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.sec.android.music.intent.action.LAUNCH_SET_AS"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v1    # "i":Landroid/content/Intent;
    goto :goto_1

    .line 659
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f100024

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private prepareLocalOptions(Landroid/view/Menu;Ljava/lang/String;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 396
    const/4 v6, 0x0

    .line 397
    .local v6, "isDrm":Z
    const/4 v4, 0x1

    .line 398
    .local v4, "enableRingtone":Z
    const/4 v3, 0x0

    .line 400
    .local v3, "drmManager":Lcom/samsung/musicplus/library/drm/DrmManager;
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 401
    .local v2, "context":Landroid/content/Context;
    sget-boolean v7, Lcom/samsung/musicplus/library/MusicFeatures;->MUSIC_FEATURE_ENABLE_DRM_RINGTONE_CHECK:Z

    if-eqz v7, :cond_0

    .line 402
    invoke-static {v2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/drm/DrmManager;

    move-result-object v3

    .line 403
    invoke-virtual {v3, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v6

    .line 406
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v0

    .line 407
    .local v0, "audioId":J
    const-wide/16 v8, 0x0

    cmp-long v7, v0, v8

    if-lez v7, :cond_3

    .line 408
    const v7, 0x7f0d01d8

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 413
    :goto_0
    if-eqz v6, :cond_1

    .line 414
    invoke-virtual {v3, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getOptionInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;

    move-result-object v5

    .line 415
    .local v5, "info":Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;
    iget-boolean v4, v5, Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;->ringtone:Z

    .line 418
    .end local v5    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;
    :cond_1
    if-eqz v4, :cond_2

    .line 419
    invoke-static {v2, p2}, Lcom/samsung/musicplus/util/UiUtils;->isPossibleSetAsRingtone(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    .line 421
    if-eqz v4, :cond_6

    .line 422
    invoke-static {v2}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 423
    const v7, 0x7f0d01a3

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 434
    :cond_2
    :goto_1
    if-nez v6, :cond_7

    invoke-static {v2}, Lcom/samsung/musicplus/util/UiUtils;->isSupportShareMusic(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isSupportedFileTypeForShareMusic()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 435
    const v7, 0x7f0d01f2

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 441
    :goto_2
    const v7, 0x7f0d01d0

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 442
    const v7, 0x7f0d01f0

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 445
    invoke-static {v2, p1}, Lcom/samsung/musicplus/util/KnoxGateManager;->setKnoxMenuVisibility(Landroid/content/Context;Landroid/view/Menu;)V

    .line 446
    return-void

    .line 410
    :cond_3
    const v7, 0x7f0d01d8

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 424
    :cond_4
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getPersonalMode()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    .line 425
    const v7, 0x7f0d01a3

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 427
    :cond_5
    const v7, 0x7f0d01a3

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 430
    :cond_6
    const v7, 0x7f0d01a3

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 437
    :cond_7
    const v7, 0x7f0d01f2

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method private prepareOptionMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v4, 0x7f0d01d8

    const/4 v3, 0x0

    .line 375
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "filePath":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v1

    .line 378
    .local v1, "isDmrPlaying":Z
    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isSlinkList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 379
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->prepareLocalOptions(Landroid/view/Menu;Ljava/lang/String;)V

    .line 389
    :goto_0
    return-void

    .line 381
    :cond_0
    const v2, 0x7f0d01a3

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 382
    const v2, 0x7f0d01f2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 383
    if-nez v1, :cond_1

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDlnaDmsList()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isSlinkList()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 384
    :cond_1
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 386
    :cond_2
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private prepareSlinkOption(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 449
    const v2, 0x7f0d01ef

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 450
    .local v1, "m":Landroid/view/MenuItem;
    if-nez v1, :cond_0

    .line 465
    :goto_0
    return-void

    .line 453
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isSlinkList()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 454
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 457
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 458
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isLocalTrack()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->isNetworkInitialized(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 460
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 462
    :cond_2
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private registerAudioReceiver()V
    .locals 2

    .prologue
    .line 987
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 988
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 989
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 990
    const-string v1, "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 991
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 992
    return-void
.end method

.method private registerEasyModeReceiver()V
    .locals 3

    .prologue
    .line 1093
    const-string v1, "MusicUi"

    const-string v2, "registerEasyModeReceiver()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1096
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1097
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE_MUSIC"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1098
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1099
    return-void
.end method

.method private registerGestureListener(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 888
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 889
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isEnableGesture()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsGestureRegistered:Z

    if-nez v0, :cond_0

    .line 893
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->registerGestureListener(Z)V

    .line 894
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsGestureRegistered:Z

    .line 897
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", register gesture listner"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    :cond_0
    return-void
.end method

.method private registerGestureReceiver()V
    .locals 3

    .prologue
    .line 974
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

    if-eqz v0, :cond_0

    .line 975
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.musicplus.GESTURE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 978
    :cond_0
    return-void
.end method

.method private registerHdmiReceiver()V
    .locals 3

    .prologue
    .line 1073
    const-string v0, "MusicUi"

    const-string v1, "registerHdmiReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.HDMI_PLUGGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1075
    return-void
.end method

.method private registerSideSyncReceiver()V
    .locals 2

    .prologue
    .line 1052
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1053
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.sidesync.source.SIDESYNC_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1054
    const-string v1, "com.sec.android.sidesync.source.SIDESYNC_DISCONNECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1055
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1056
    return-void
.end method

.method private registerWfdStatusChangeReceiver()V
    .locals 2

    .prologue
    .line 287
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 288
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.action.AV_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 290
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mWfdStatusChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 291
    return-void
.end method

.method private setFakeCustomView()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 702
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 703
    .local v1, "v":Landroid/view/View;
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 705
    new-instance v2, Lcom/samsung/musicplus/player/PlayerActivity$3;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/player/PlayerActivity$3;-><init>(Lcom/samsung/musicplus/player/PlayerActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 712
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 713
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 714
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 715
    return-void
.end method

.method private unregisterGestureListener(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 945
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    if-eqz v0, :cond_0

    .line 946
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsGestureRegistered:Z

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->releaseGestureListener()V

    .line 948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mIsGestureRegistered:Z

    .line 951
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", unregister gesture listner"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    :cond_0
    return-void
.end method

.method private unregisterGestureReceiver()V
    .locals 1

    .prologue
    .line 981
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mGestureReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 984
    :cond_0
    return-void
.end method

.method private unregisterWfdStatusChangeReceiver()V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mWfdStatusChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 295
    return-void
.end method


# virtual methods
.method public doAllShareButton()V
    .locals 1

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->doAllShareButton()V

    .line 1127
    return-void
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 320
    const-string v0, "MusicUi"

    const-string v1, "PlayerActivity finish()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    .line 322
    return-void
.end method

.method public hideAddToPlaylist()V
    .locals 2

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->hideAddToPlaylist(Z)V

    .line 1116
    return-void
.end method

.method public hideVolumePanel()V
    .locals 1

    .prologue
    .line 1119
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    if-eqz v0, :cond_0

    .line 1120
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    .line 1122
    :cond_0
    return-void
.end method

.method protected launchDetailsInfo(I)V
    .locals 5
    .param p1, "listType"    # I

    .prologue
    .line 619
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDlnaDmsList()Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x2000d

    if-ne p1, v3, :cond_1

    .line 620
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 621
    .local v2, "manager":Landroid/app/FragmentManager;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v4

    invoke-static {v3, p1, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getInstance(Ljava/lang/String;II)Landroid/app/DialogFragment;

    move-result-object v0

    .line 623
    .local v0, "fg":Landroid/app/DialogFragment;
    const-string v3, "dlnaDms_detail_info"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 629
    .end local v0    # "fg":Landroid/app/DialogFragment;
    .end local v2    # "manager":Landroid/app/FragmentManager;
    :goto_0
    return-void

    .line 625
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 626
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "uri"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 627
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 597
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    packed-switch p1, :pswitch_data_0

    .line 610
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/MusicBaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 611
    return-void

    .line 600
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 598
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->isNowPlayingListShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->togglePlayerView()V

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    move-result v0

    if-nez v0, :cond_2

    .line 308
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->setResult(I)V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->finish()V

    .line 310
    const/4 v0, 0x0

    const v1, 0x7f05000f

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerActivity;->overridePendingTransition(II)V

    .line 313
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    if-eqz p1, :cond_0

    .line 124
    const-string v0, "selected_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    .line 126
    :cond_0
    const v0, 0x7f04005d

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->setContentView(I)V

    .line 127
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->setVolumeControlStream(I)V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 131
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerActivity;->initializeViews(Landroid/os/Bundle;)V

    .line 132
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->setFakeCustomView()V

    .line 134
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_1

    .line 135
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerEasyModeReceiver()V

    .line 137
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 340
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    const v1, 0x7f0d01ec

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItem(Landroid/view/MenuItem;)V

    .line 341
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    const v1, 0x7f0d01ed

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->setVolumeItem(Landroid/view/MenuItem;)V

    .line 342
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlockHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 194
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->dismissVolumePanel()V

    .line 195
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 199
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onDestroy()V

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->onDestroy()V

    .line 201
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 732
    const-string v2, "MusicUi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyDown keyCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    sparse-switch p1, :sswitch_data_0

    .line 777
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeEvent(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 778
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->handleVolumeKey(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 789
    :cond_0
    :goto_0
    return v0

    .line 735
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleShuffle()V

    goto :goto_0

    .line 738
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 740
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 742
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    goto :goto_0

    .line 745
    :sswitch_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 746
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->openOptionsMenu()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 749
    goto :goto_0

    .line 752
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->isVisibleNormalVolumePanel()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 753
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1, v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v1

    if-eqz v1, :cond_3

    .line 754
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->volumeDown()V

    .line 755
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->getVolumeSeekBar()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 757
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->getSoundAliveButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 765
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->isVisibleNormalVolumePanel()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 766
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1, v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v1

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    .line 767
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->getSoundAliveButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-nez v1, :cond_4

    .line 768
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->volumeUp()V

    .line 770
    :cond_4
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->getVolumeSeekBar()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :cond_5
    move v0, v1

    .line 784
    goto :goto_0

    .line 789
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 733
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_4
        0x14 -> :sswitch_3
        0x29 -> :sswitch_2
        0x2f -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 794
    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyUp keyCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    packed-switch p1, :pswitch_data_0

    .line 803
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerActivity;->isVolumeEvent(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 809
    :cond_0
    :goto_0
    return v0

    .line 797
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 799
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->openOptionsMenu()V

    goto :goto_0

    .line 809
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 795
    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
    .end packed-switch
.end method

.method public onModeChanged(Z)V
    .locals 2
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 1131
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onModeChanged(Z)V

    .line 1132
    if-nez p1, :cond_0

    .line 1133
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->setSelectedRecommendPosition(I)V

    .line 1134
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->hideVolumePanel()V

    .line 1136
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 495
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 572
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    .line 497
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->isDataHelpDlna(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 499
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->showDataHelpDlnaDialog(Landroid/content/Context;Landroid/app/FragmentManager;)V

    goto :goto_0

    .line 503
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->doAllShareButton()V

    goto :goto_0

    .line 506
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v3}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleVolumePannel()V

    goto :goto_0

    .line 509
    :sswitch_2
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/player/PlayerActivity;->moveTaskToBack(Z)Z

    .line 510
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->killMusicProcess()V

    goto :goto_0

    .line 513
    :sswitch_3
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->launchGroupPlay()V

    goto :goto_0

    .line 516
    :sswitch_4
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->launchSetAs()V

    goto :goto_0

    .line 519
    :sswitch_5
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/player/PlayerActivity;->launchDetailsInfo(I)V

    goto :goto_0

    .line 524
    :sswitch_6
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    .line 527
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isSlinkList()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 528
    const/4 v2, 0x2

    .line 532
    .local v2, "type":I
    :goto_1
    new-array v3, v6, [J

    iget-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    aput-wide v4, v3, v7

    invoke-static {p0, v2, v3, v6}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->startDeviceSelectActivityForResult(Landroid/app/Activity;I[JI)V

    goto :goto_0

    .line 530
    .end local v2    # "type":I
    :cond_2
    const/4 v2, 0x1

    .restart local v2    # "type":I
    goto :goto_1

    .line 538
    .end local v2    # "type":I
    :sswitch_7
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    .line 539
    new-array v1, v6, [J

    iget-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    aput-wide v4, v1, v7

    .line 542
    .local v1, "id":[J
    const v3, 0x7f0d01d4

    invoke-static {p0, v1, v3}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxDialog(Landroid/app/Activity;[JI)V

    goto :goto_0

    .line 545
    .end local v1    # "id":[J
    :sswitch_8
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    .line 546
    new-array v1, v6, [J

    iget-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    aput-wide v4, v1, v7

    .line 549
    .restart local v1    # "id":[J
    const v3, 0x7f0d01d5

    invoke-static {p0, v1, v3}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxDialog(Landroid/app/Activity;[JI)V

    goto :goto_0

    .line 552
    .end local v1    # "id":[J
    :sswitch_9
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    .line 553
    new-array v1, v6, [J

    iget-wide v4, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    aput-wide v4, v1, v7

    .line 556
    .restart local v1    # "id":[J
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 557
    new-instance v3, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    array-length v4, v1

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v5

    invoke-direct {v3, v1, v4, v5, v7}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;-><init>([JIIZ)V

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "deleteDialog"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 562
    .end local v1    # "id":[J
    :sswitch_a
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "MATP"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 563
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 564
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "list_mode"

    const/16 v4, 0x15

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 566
    const-string v3, "header_mode"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 567
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 495
    :sswitch_data_0
    .sparse-switch
        0x7f0d01a3 -> :sswitch_4
        0x7f0d01d0 -> :sswitch_9
        0x7f0d01d4 -> :sswitch_7
        0x7f0d01d5 -> :sswitch_8
        0x7f0d01d8 -> :sswitch_5
        0x7f0d01e8 -> :sswitch_2
        0x7f0d01ec -> :sswitch_0
        0x7f0d01ed -> :sswitch_1
        0x7f0d01ef -> :sswitch_6
        0x7f0d01f0 -> :sswitch_a
        0x7f0d01f2 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onPause()V

    .line 172
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->onPause()V

    .line 173
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const v0, 0x7f0d01ee

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 349
    const/4 v0, 0x1

    .line 359
    :goto_0
    return v0

    .line 357
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerActivity;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 358
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerActivity;->prepareSlinkOption(Landroid/view/Menu;)V

    .line 359
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onReceivePlayerState(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 205
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    .line 207
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.android.music.metachanged"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 209
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 212
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->finish()V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isLocalTrack()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->showPrepareLoading(Z)V

    .line 216
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateTrackAllInfo()V

    .line 217
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateStarredIcon()V

    .line 220
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    .line 221
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateNowPlayingList(Landroid/content/Intent;)V

    .line 224
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->startRequery()V

    .line 225
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v2

    if-nez v2, :cond_2

    .line 228
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->updatePrepareLoading(Z)V

    .line 229
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateNowPlayingList(Landroid/content/Intent;)V

    .line 231
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setCurrentSongPosition(I)V

    goto :goto_0

    .line 232
    :cond_3
    const-string v2, "com.android.music.playstatechanged"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 233
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v1

    .line 234
    .local v1, "isPlaying":Z
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updatePlayState(Z)V

    .line 235
    if-eqz v1, :cond_4

    .line 236
    const-string v2, "playing"

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/PlayerActivity;->registerGestureListener(Ljava/lang/String;)V

    .line 237
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    .line 242
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateNowPlayingList(Landroid/content/Intent;)V

    goto :goto_0

    .line 240
    :cond_4
    const-string v2, "pause"

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterGestureListener(Ljava/lang/String;)V

    goto :goto_1

    .line 243
    .end local v1    # "isPlaying":Z
    :cond_5
    const-string v2, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 244
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->updatePrepareLoading(Z)V

    .line 245
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateNowPlayingList(Landroid/content/Intent;)V

    goto :goto_0

    .line 246
    :cond_6
    const-string v2, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 247
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateSongInfo()V

    .line 248
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateNowPlayingList(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 249
    :cond_7
    const-string v2, "com.samsung.musicplus.favouritechanged"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 250
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateStarredIcon()V

    goto/16 :goto_0

    .line 251
    :cond_8
    const-string v2, "com.android.music.settingchanged"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 252
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateRepeatIcon()V

    .line 253
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateShuffleIcon()V

    goto/16 :goto_0

    .line 254
    :cond_9
    const-string v2, "com.samsung.musicplus.action.PLAYER_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 255
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateChangePlayerItem()V

    goto/16 :goto_0

    .line 256
    :cond_a
    const-string v2, "com.samsung.musicplus.action.QUEUE_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->cancelFfRew()V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onResume()V

    .line 166
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->onResume()V

    .line 167
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 364
    const-string v1, "selected_id"

    iget-wide v2, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSelectedId:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 365
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    if-eqz v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->appendSavedInstanceState(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 367
    .local v0, "playerViewState":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 368
    move-object p1, v0

    .line 371
    .end local v0    # "playerViewState":Landroid/os/Bundle;
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 372
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 141
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateControllerView(I)V

    .line 145
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setNowPlayingCursorLoader()V

    .line 146
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->invalidateOptionsMenu()V

    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updatePlayerOptionButtons()V

    .line 148
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1140
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSizeChanged(Landroid/graphics/Rect;)V

    .line 1141
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 1142
    .local v0, "orientation":I
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->isMultiWindowActive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-nez v1, :cond_0

    .line 1143
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->hideVolumePanel()V

    .line 1145
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerGestureReceiver()V

    .line 153
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerAudioReceiver()V

    .line 154
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerSideSyncReceiver()V

    .line 155
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerHdmiReceiver()V

    .line 156
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerWfdStatusChangeReceiver()V

    .line 158
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onStart()V

    .line 159
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->onStart()V

    .line 160
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart in music end "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterGestureReceiver()V

    .line 182
    const-string v0, "onStop"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterGestureListener(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 184
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 185
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 186
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterWfdStatusChangeReceiver()V

    .line 187
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onStop()V

    .line 188
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->onStop()V

    .line 189
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 326
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged hasFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    if-eqz p1, :cond_0

    .line 328
    const-string v0, "focus"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->registerGestureListener(Ljava/lang/String;)V

    .line 333
    :goto_0
    return-void

    .line 330
    :cond_0
    const-string v0, "focus"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerActivity;->unregisterGestureListener(Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    goto :goto_0
.end method

.method public viaBluetooth()V
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    .line 580
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItemWithDealy(I)V

    .line 581
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->viaBluetooth()V

    .line 583
    return-void
.end method

.method public viaDevice()V
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    .line 590
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItemWithDealy(I)V

    .line 591
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->viaDevice()V

    .line 593
    return-void
.end method
