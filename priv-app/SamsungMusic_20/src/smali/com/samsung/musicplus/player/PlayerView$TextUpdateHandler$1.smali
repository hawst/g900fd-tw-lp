.class Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$300(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$300(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$400(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$400(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getArtist()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$500(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 238
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler$1;->this$1:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$500(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbum()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    :cond_2
    return-void
.end method
