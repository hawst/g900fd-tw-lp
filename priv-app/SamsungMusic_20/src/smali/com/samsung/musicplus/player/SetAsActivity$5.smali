.class Lcom/samsung/musicplus/player/SetAsActivity$5;
.super Ljava/lang/Object;
.source "SetAsActivity.java"

# interfaces
.implements Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/SetAsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/SetAsActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/SetAsActivity;)V
    .locals 0

    .prologue
    .line 771
    iput-object p1, p0, Lcom/samsung/musicplus/player/SetAsActivity$5;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(II)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "offset"    # I

    .prologue
    .line 774
    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/player/SetAsActivity;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mResultListener() - status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " offset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$5;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$700(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$5;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$700(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 779
    :cond_0
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    .line 780
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$5;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # setter for: Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I
    invoke-static {v0, p2}, Lcom/samsung/musicplus/player/SetAsActivity;->access$402(Lcom/samsung/musicplus/player/SetAsActivity;I)I

    .line 781
    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/player/SetAsActivity;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mResultListener() - offset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/SetAsActivity$5;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mOffset:I
    invoke-static {v2}, Lcom/samsung/musicplus/player/SetAsActivity;->access$400(Lcom/samsung/musicplus/player/SetAsActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    iget-object v0, p0, Lcom/samsung/musicplus/player/SetAsActivity$5;->this$0:Lcom/samsung/musicplus/player/SetAsActivity;

    # getter for: Lcom/samsung/musicplus/player/SetAsActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/player/SetAsActivity;->access$800(Lcom/samsung/musicplus/player/SetAsActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 787
    :cond_1
    return-void
.end method
